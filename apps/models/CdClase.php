<?php
namespace Modules\Models;

class CdClase extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    protected $clasid;

    /**
     *
     * @var string
     */
    protected $date_end;

    /**
     *
     * @var string
     */
    protected $name;

    /**
     *
     * @var string
     */
    protected $status;

    /**
     *
     * @var string
     */
    protected $date_creation;

    /**
     *
     * @var integer
     */
    protected $uid;

    /**
     *
     * @var integer
     */
    protected $pid;

    /**
     *
     * @var integer
     */
    protected $generation;

    /**
     *
     * @var integer
     */
    protected $carrera;

    /**
     *
     * @var integer
     */
    protected $file;

    /**
     *
     * @var string
     */
    protected $chat;

    /**
     *
     * @var string
     */
    protected $calendario;

    /**
     * Method to set the value of field clasid
     *
     * @param integer $clasid
     * @return $this
     */
    public function setClasid($clasid)
    {
        $this->clasid = $clasid;

        return $this;
    }

    /**
     * Method to set the value of field date_end
     *
     * @param string $date_end
     * @return $this
     */
    public function setDateEnd($date_end)
    {
        $this->date_end = $date_end;

        return $this;
    }

    /**
     * Method to set the value of field name
     *
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Method to set the value of field status
     *
     * @param string $status
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Method to set the value of field date_creation
     *
     * @param string $date_creation
     * @return $this
     */
    public function setDateCreation($date_creation)
    {
        $this->date_creation = $date_creation;

        return $this;
    }

    /**
     * Method to set the value of field uid
     *
     * @param integer $uid
     * @return $this
     */
    public function setUid($uid)
    {
        $this->uid = $uid;

        return $this;
    }

    /**
     * Method to set the value of field pid
     *
     * @param integer $pid
     * @return $this
     */
    public function setPid($pid)
    {
        $this->pid = $pid;

        return $this;
    }

    /**
     * Method to set the value of field generation
     *
     * @param integer $generation
     * @return $this
     */
    public function setGeneration($generation)
    {
        $this->generation = $generation;

        return $this;
    }

    /**
     * Method to set the value of field carrera
     *
     * @param integer $carrera
     * @return $this
     */
    public function setCarrera($carrera)
    {
        $this->carrera = $carrera;

        return $this;
    }

    /**
     * Method to set the value of field file
     *
     * @param string $file
     * @return $this
     */
    public function setFile($file)
    {
        $this->file = $file;

        return $this;
    }

    /**
     * Method to set the value of field chat
     *
     * @param string $chat
     * @return $this
     */
    public function setChat($chat)
    {
        $this->chat = $chat;

        return $this;
    }

    /**
     * Method to set the value of field calendario
     *
     * @param string $calendario
     * @return $this
     */
    public function setCalendario($calendario)
    {
        $this->calendario = $calendario;

        return $this;
    }

    /**
     * Returns the value of field clasid
     *
     * @return integer
     */
    public function getClasid()
    {
        return $this->clasid;
    }

    /**
     * Returns the value of field date_end
     *
     * @return string
     */
    public function getDateEnd()
    {
        return $this->date_end;
    }

    /**
     * Returns the value of field name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Returns the value of field status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Returns the value of field date_creation
     *
     * @return string
     */
    public function getDateCreation()
    {
        return $this->date_creation;
    }

    /**
     * Returns the value of field uid
     *
     * @return integer
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * Returns the value of field pid
     *
     * @return integer
     */
    public function getPid()
    {
        return $this->pid;
    }

    /**
     * Returns the value of field generation
     *
     * @return integer
     */
    public function getGeneration()
    {
        return $this->generation;
    }

    /**
     * Returns the value of field carrera
     *
     * @return integer
     */
    public function getCarrera()
    {
        return $this->carrera;
    }

    /**
     * Returns the value of field file
     *
     * @return string
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Returns the value of field chat
     *
     * @return string
     */
    public function getChat()
    {
        return $this->chat;
    }

    /**
     * Returns the value of field calendario
     *
     * @return string
     */
    public function getCalendario()
    {
        return $this->calendario;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->hasMany('clasid', 'CdClaseStudent', 'clasid', array('alias' => 'CdClaseStudent'));
        /*$this->hasMany('clasid', 'CdDirection', 'clasid', array('alias' => 'CdDirection'));
        $this->hasMany('clasid', 'CdSale', 'clasid', array('alias' => 'CdSale'));
        $this->belongsTo('uid', 'CdUser', 'uid', array('alias' => 'CdUser'));*/
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'cd_clase';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return CdClase[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return CdClase
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
