<?php
namespace Modules\Models;
class CdLogUserSubservices extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    protected $lusid;

    /**
     *
     * @var integer
     */
    protected $uid;

    /**
     *
     * @var integer
     */
    protected $uid_update;

    protected $usid;
    /**
     *
     * @var string
     */
    protected $percentage;

    /**
     *
     * @var string
     */
    protected $date_creation;

    /**
     * Method to set the value of field lusid
     *
     * @param integer $lusid
     * @return $this
     */
    public function setLusid($lusid)
    {
        $this->lusid = $lusid;

        return $this;
    }

    /**
     * Method to set the value of field uid
     *
     * @param integer $uid
     * @return $this
     */
    public function setUid($uid)
    {
        $this->uid = $uid;

        return $this;
    }

    /**
     * Method to set the value of field uid_update
     *
     * @param integer $uid_update
     * @return $this
     */
    public function setUidUpdate($uid_update)
    {
        $this->uid_update = $uid_update;

        return $this;
    }
    public function setUsid($usid)
    {
        $this->usid = $usid;

        return $this;
    }

    /**
     * Method to set the value of field percentage
     *
     * @param string $percentage
     * @return $this
     */
    public function setPercentage($percentage)
    {
        $this->percentage = $percentage;

        return $this;
    }

    /**
     * Method to set the value of field date_creation
     *
     * @param string $date_creation
     * @return $this
     */
    public function setDateCreation($date_creation)
    {
        $this->date_creation = $date_creation;

        return $this;
    }

    /**
     * Returns the value of field lusid
     *
     * @return integer
     */
    public function getLusid()
    {
        return $this->lusid;
    }

    /**
     * Returns the value of field uid
     *
     * @return integer
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * Returns the value of field uid_update
     *
     * @return integer
     */
    public function getUidUpdate()
    {
        return $this->uid_update;
    }

    public function getUsid()
    {
        return $this->usid;
    }

    /**
     * Returns the value of field percentage
     *
     * @return string
     */
    public function getPercentage()
    {
        return $this->percentage;
    }

    /**
     * Returns the value of field date_creation
     *
     * @return string
     */
    public function getDateCreation()
    {
        return $this->date_creation;
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'cd_log_user_subservices';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return CdLogUserSubservices[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return CdLogUserSubservices
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
