<?php
namespace Modules\Models;
class CdLogStock extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    protected $lstid;

    /**
     *
     * @var integer
     */
    protected $add;

    /**
     *
     * @var integer
     */
    protected $delete;

    /**
     *
     * @var integer
     */
    protected $stock;

    /**
     *
     * @var integer
     */
    protected $stock_new;

    /**
     *
     * @var string
     */
    protected $quantity;

    /**
     *
     * @var string
     */
    protected $quantity_new;

    /**
     *
     * @var integer
     */
    protected $stid;

    /**
     *
     * @var string
     */
    protected $date_creation;

    /**
     * Method to set the value of field lstid
     *
     * @param integer $lstid
     * @return $this
     */
    public function setLstid($lstid)
    {
        $this->lstid = $lstid;

        return $this;
    }

    /**
     * Method to set the value of field add
     *
     * @param integer $add
     * @return $this
     */
    public function setAdd($add)
    {
        $this->add = $add;

        return $this;
    }

    /**
     * Method to set the value of field delete
     *
     * @param integer $delete
     * @return $this
     */
    public function setDelete($delete)
    {
        $this->delete = $delete;

        return $this;
    }

    /**
     * Method to set the value of field stock
     *
     * @param integer $stock
     * @return $this
     */
    public function setStock($stock)
    {
        $this->stock = $stock;

        return $this;
    }

    /**
     * Method to set the value of field stock_new
     *
     * @param integer $stock_new
     * @return $this
     */
    public function setStockNew($stock_new)
    {
        $this->stock_new = $stock_new;

        return $this;
    }

    /**
     * Method to set the value of field quantity
     *
     * @param string $quantity
     * @return $this
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Method to set the value of field quantity_new
     *
     * @param string $quantity_new
     * @return $this
     */
    public function setQuantityNew($quantity_new)
    {
        $this->quantity_new = $quantity_new;

        return $this;
    }

    /**
     * Method to set the value of field stid
     *
     * @param integer $stid
     * @return $this
     */
    public function setStid($stid)
    {
        $this->stid = $stid;

        return $this;
    }

    /**
     * Method to set the value of field date_creation
     *
     * @param string $date_creation
     * @return $this
     */
    public function setDateCreation($date_creation)
    {
        $this->date_creation = $date_creation;

        return $this;
    }

    /**
     * Returns the value of field lstid
     *
     * @return integer
     */
    public function getLstid()
    {
        return $this->lstid;
    }

    /**
     * Returns the value of field add
     *
     * @return integer
     */
    public function getAdd()
    {
        return $this->add;
    }

    /**
     * Returns the value of field delete
     *
     * @return integer
     */
    public function getDelete()
    {
        return $this->delete;
    }

    /**
     * Returns the value of field stock
     *
     * @return integer
     */
    public function getStock()
    {
        return $this->stock;
    }

    /**
     * Returns the value of field stock_new
     *
     * @return integer
     */
    public function getStockNew()
    {
        return $this->stock_new;
    }

    /**
     * Returns the value of field quantity
     *
     * @return string
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Returns the value of field quantity_new
     *
     * @return string
     */
    public function getQuantityNew()
    {
        return $this->quantity_new;
    }

    /**
     * Returns the value of field stid
     *
     * @return integer
     */
    public function getStid()
    {
        return $this->stid;
    }

    /**
     * Returns the value of field date_creation
     *
     * @return string
     */
    public function getDateCreation()
    {
        return $this->date_creation;
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'cd_log_stock';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return CdLogStock[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return CdLogStock
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
