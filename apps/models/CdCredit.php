<?php
namespace Modules\Models;
class CdCredit extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    protected $crid;

    /**
     *
     * @var integer
     */
    protected $credit;

    /**
     *
     * @var integer
     */
    protected $salid;

    /**
     *
     * @var string
     */
    protected $status;

    /**
     *
     * @var string
     */
    protected $date_creation;

    /**
     * Method to set the value of field crid
     *
     * @param integer $crid
     * @return $this
     */
    public function setCrid($crid)
    {
        $this->crid = $crid;

        return $this;
    }

    /**
     * Method to set the value of field credit
     *
     * @param integer $credit
     * @return $this
     */
    public function setCredit($credit)
    {
        $this->credit = $credit;

        return $this;
    }

    /**
     * Method to set the value of field salid
     *
     * @param integer $salid
     * @return $this
     */
    public function setSalid($salid)
    {
        $this->salid = $salid;

        return $this;
    }

    /**
     * Method to set the value of field status
     *
     * @param string $status
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Method to set the value of field date_creation
     *
     * @param string $date_creation
     * @return $this
     */
    public function setDateCreation($date_creation)
    {
        $this->date_creation = $date_creation;

        return $this;
    }

    /**
     * Returns the value of field crid
     *
     * @return integer
     */
    public function getCrid()
    {
        return $this->crid;
    }

    /**
     * Returns the value of field credit
     *
     * @return integer
     */
    public function getCredit()
    {
        return $this->credit;
    }

    /**
     * Returns the value of field salid
     *
     * @return integer
     */
    public function getSalid()
    {
        return $this->salid;
    }

    /**
     * Returns the value of field status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Returns the value of field date_creation
     *
     * @return string
     */
    public function getDateCreation()
    {
        return $this->date_creation;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->belongsTo('salid', 'CdSale', 'salid', array('alias' => 'CdSale'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'cd_credit';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return CdCredit[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return CdCredit
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}