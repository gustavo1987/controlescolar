<?php
namespace Modules\Models;

class CdPago extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    protected $pagid;

    /**
     *
     * @var string
     */
    protected $name;

    /**
     *
     * @var string
     */
    protected $cantidad;

    /**
     *
     * @var string
     */
    protected $status;

    /**
     *
     * @var string
     */
    protected $date_creation;

    /**
     *
     * @var integer
     */
    protected $clid;

    /**
     *
     * @var integer
     */
    protected $vencimiento;

    /**
     *
     * @var string
     */
    protected $relationship;

    /**
     *
     * @var string
     */
    protected $type;

    /**
     *
     * @var string
     */
    protected $view;

    /**
     * Method to set the value of field pagid
     *
     * @param integer $pagid
     * @return $this
     */
    public function setPagid($pagid)
    {
        $this->pagid = $pagid;

        return $this;
    }

    /**
     * Method to set the value of field name
     *
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Method to set the value of field cantidad
     *
     * @param string $cantidad
     * @return $this
     */
    public function setCantidad($cantidad)
    {
        $this->cantidad = $cantidad;

        return $this;
    }

    /**
     * Method to set the value of field status
     *
     * @param string $status
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Method to set the value of field date_creation
     *
     * @param string $date_creation
     * @return $this
     */
    public function setDateCreation($date_creation)
    {
        $this->date_creation = $date_creation;

        return $this;
    }

    /**
     * Method to set the value of field clid
     *
     * @param integer $clid
     * @return $this
     */
    public function setClid($clid)
    {
        $this->clid = $clid;

        return $this;
    }

    /**
     * Method to set the value of field vencimiento
     *
     * @param integer $vencimiento
     * @return $this
     */
    public function setVencimiento($vencimiento)
    {
        $this->vencimiento = $vencimiento;

        return $this;
    }

    /**
     * Method to set the value of field relationship
     *
     * @param string $relationship
     * @return $this
     */
    public function setRelationship($relationship)
    {
        $this->relationship = $relationship;

        return $this;
    }

    /**
     * Method to set the value of field type
     *
     * @param string $type
     * @return $this
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Method to set the value of field view
     *
     * @param string $view
     * @return $this
     */
    public function setView($view)
    {
        $this->view = $view;

        return $this;
    }

    /**
     * Returns the value of field pagid
     *
     * @return integer
     */
    public function getPagid()
    {
        return $this->pagid;
    }

    /**
     * Returns the value of field name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Returns the value of field cantidad
     *
     * @return string
     */
    public function getCantidad()
    {
        return $this->cantidad;
    }

    /**
     * Returns the value of field status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Returns the value of field date_creation
     *
     * @return string
     */
    public function getDateCreation()
    {
        return $this->date_creation;
    }

    /**
     * Returns the value of field clid
     *
     * @return integer
     */
    public function getClid()
    {
        return $this->clid;
    }

    /**
     * Returns the value of field relationship
     *
     * @return string
     */
    public function getRelationship()
    {
        return $this->relationship;
    }

    /**
     * Returns the value of field vencimiento
     *
     * @return integer
     */
    public function getVencimiento()
    {
        return $this->vencimiento;
    }

    /**
     * Returns the value of field type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Returns the value of field view
     *
     * @return string
     */
    public function getView()
    {
        return $this->view;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        /*$this->hasMany('pagid', 'CdDirection', 'pagid', array('alias' => 'CdDirection'));
        $this->hasMany('pagid', 'CdSale', 'pagid', array('alias' => 'CdSale'));*/
        $this->belongsTo('clid', 'CdClient', 'clid', array('alias' => 'CdClient'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'cd_pago';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return CdPago[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return CdPago
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
