<?php
namespace Modules\Models;

class CdClaseStudent extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var string
     */
    protected $clstid;

    /**
     *
     * @var integer
     */
    protected $clasid;

    /**
     *
     * @var string
     */
    protected $clid;

    /**
     *
     * @var string
     */
    protected $status;

    /**
     *
     * @var integer
     */
    protected $calificacion;

    /**
     *
     * @var integer
     */
    protected $begin_sesion;

    /**
     * Method to set the value of field clstid
     *
     * @param string $clstid
     * @return $this
     */
    public function setClstid($clstid)
    {
        $this->clstid = $clstid;

        return $this;
    }

    /**
     * Method to set the value of field clasid
     *
     * @param integer $clasid
     * @return $this
     */
    public function setClasid($clasid)
    {
        $this->clasid = $clasid;

        return $this;
    }

    /**
     * Method to set the value of field clid
     *
     * @param string $clid
     * @return $this
     */
    public function setClid($clid)
    {
        $this->clid = $clid;

        return $this;
    }

    /**
     * Method to set the value of field status
     *
     * @param string $status
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Method to set the value of field calificacion
     *
     * @param integer $calificacion
     * @return $this
     */
    public function setCalificacion($calificacion)
    {
        $this->calificacion = $calificacion;

        return $this;
    }

    /**
     * Method to set the value of field begin_sesion
     *
     * @param string $begin_sesion
     * @return $this
     */
    public function setBeginSesion($begin_sesion)
    {
        $this->begin_sesion = $begin_sesion;

        return $this;
    }

    /**
     * Returns the value of field clstid
     *
     * @return string
     */
    public function getClstid()
    {
        return $this->clstid;
    }

    /**
     * Returns the value of field clasid
     *
     * @return integer
     */
    public function getClasid()
    {
        return $this->clasid;
    }

    /**
     * Returns the value of field clid
     *
     * @return string
     */
    public function getClid()
    {
        return $this->clid;
    }

    /**
     * Returns the value of field status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Returns the value of field calificacion
     *
     * @return integer
     */
    public function getCalificacion()
    {
        return $this->calificacion;
    }

    /**
     * Returns the value of field begin_sesion
     *
     * @return string
     */
    public function getBeginSesion()
    {
        return $this->begin_sesion;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        //$this->hasMany('clasid', 'CdClaseStudent', 'clasid', array('alias' => 'CdClaseStudent'));
        /*$this->hasMany('clasid', 'CdDirection', 'clasid', array('alias' => 'CdDirection'));
        $this->hasMany('clasid', 'CdSale', 'clasid', array('alias' => 'CdSale'));*/
        $this->belongsTo('clid', 'CdClase', 'clid', array('alias' => 'CdClase'));
    }

    /**
     * Returns table clid mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'cd_clase_student';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return CdClaseStudent[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return CdClaseStudent
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
