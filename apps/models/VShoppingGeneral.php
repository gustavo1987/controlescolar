<?php
namespace Modules\Models;
use Phalcon\Mvc\Model\Validator\Email as Email;

class VShoppingGeneral extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    protected $salid;

    /**
     *
     * @var integer
     */
    protected $uid;

    /**
     *
     * @var string
     */
    protected $total;

    /**
     *
     * @var string
     */
    protected $discount;

    /**
     *
     * @var string
     */
    protected $received;

    /**
     *
     * @var string
     */
    protected $status;

    /**
     *
     * @var string
     */
    protected $send_email;

    /**
     *
     * @var string
     */
    protected $email;

    /**
     *
     * @var string
     */
    protected $date_creation_sale;

    /**
     *
     * @var string
     */
    protected $name;

    /**
     *
     * @var string
     */
    protected $phone;

    /**
     *
     * @var integer
     */
    protected $clid;

    /**
     *
     * @var string
     */
    protected $identifier;

    /**
     *
     * @var string
     */
    protected $name_client;

    /**
     *
     * @var string
     */
    protected $last_name_client;

    /**
     *
     * @var string
     */
    protected $second_name_client;

    /**
     *
     * @var string
     */
    protected $email_client;

    /**
     *
     * @var integer
     */
    protected $uid_attend;

    /**
     *
     * @var string
     */
    protected $name_attend;

    /**
     *
     * @var string
     */
    protected $last_name_attend;

    /**
     *
     * @var string
     */
    protected $second_name_attend;

    /**
     * Method to set the value of field salid
     *
     * @param integer $salid
     * @return $this
     */
    public function setSalid($salid)
    {
        $this->salid = $salid;

        return $this;
    }

    /**
     * Method to set the value of field uid
     *
     * @param integer $uid
     * @return $this
     */
    public function setUid($uid)
    {
        $this->uid = $uid;

        return $this;
    }

    /**
     * Method to set the value of field total
     *
     * @param string $total
     * @return $this
     */
    public function setTotal($total)
    {
        $this->total = $total;

        return $this;
    }

    /**
     * Method to set the value of field discount
     *
     * @param string $discount
     * @return $this
     */
    public function setDiscount($discount)
    {
        $this->discount = $discount;

        return $this;
    }

    /**
     * Method to set the value of field received
     *
     * @param string $received
     * @return $this
     */
    public function setReceived($received)
    {
        $this->received = $received;

        return $this;
    }

    /**
     * Method to set the value of field status
     *
     * @param string $status
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Method to set the value of field send_email
     *
     * @param string $send_email
     * @return $this
     */
    public function setSendEmail($send_email)
    {
        $this->send_email = $send_email;

        return $this;
    }

    /**
     * Method to set the value of field email
     *
     * @param string $email
     * @return $this
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Method to set the value of field date_creation_sale
     *
     * @param string $date_creation_sale
     * @return $this
     */
    public function setDateCreationSale($date_creation_sale)
    {
        $this->date_creation_sale = $date_creation_sale;

        return $this;
    }

    /**
     * Method to set the value of field name
     *
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Method to set the value of field phone
     *
     * @param string $phone
     * @return $this
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Method to set the value of field clid
     *
     * @param integer $clid
     * @return $this
     */
    public function setClid($clid)
    {
        $this->clid = $clid;

        return $this;
    }

    /**
     * Method to set the value of field identifier
     *
     * @param string $identifier
     * @return $this
     */
    public function setIdentifier($identifier)
    {
        $this->identifier = $identifier;

        return $this;
    }

    /**
     * Method to set the value of field name_client
     *
     * @param string $name_client
     * @return $this
     */
    public function setNameClient($name_client)
    {
        $this->name_client = $name_client;

        return $this;
    }

    /**
     * Method to set the value of field last_name_client
     *
     * @param string $last_name_client
     * @return $this
     */
    public function setLastNameClient($last_name_client)
    {
        $this->last_name_client = $last_name_client;

        return $this;
    }

    /**
     * Method to set the value of field second_name_client
     *
     * @param string $second_name_client
     * @return $this
     */
    public function setSecondNameClient($second_name_client)
    {
        $this->second_name_client = $second_name_client;

        return $this;
    }

    /**
     * Method to set the value of field email_client
     *
     * @param string $email_client
     * @return $this
     */
    public function setEmailClient($email_client)
    {
        $this->email_client = $email_client;

        return $this;
    }

    /**
     * Method to set the value of field uid_attend
     *
     * @param integer $uid_attend
     * @return $this
     */
    public function setUidAttend($uid_attend)
    {
        $this->uid_attend = $uid_attend;

        return $this;
    }

    /**
     * Method to set the value of field name_attend
     *
     * @param string $name_attend
     * @return $this
     */
    public function setNameAttend($name_attend)
    {
        $this->name_attend = $name_attend;

        return $this;
    }

    /**
     * Method to set the value of field last_name_attend
     *
     * @param string $last_name_attend
     * @return $this
     */
    public function setLastNameAttend($last_name_attend)
    {
        $this->last_name_attend = $last_name_attend;

        return $this;
    }

    /**
     * Method to set the value of field second_name_attend
     *
     * @param string $second_name_attend
     * @return $this
     */
    public function setSecondNameAttend($second_name_attend)
    {
        $this->second_name_attend = $second_name_attend;

        return $this;
    }

    /**
     * Returns the value of field salid
     *
     * @return integer
     */
    public function getSalid()
    {
        return $this->salid;
    }

    /**
     * Returns the value of field uid
     *
     * @return integer
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * Returns the value of field total
     *
     * @return string
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * Returns the value of field discount
     *
     * @return string
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * Returns the value of field received
     *
     * @return string
     */
    public function getReceived()
    {
        return $this->received;
    }

    /**
     * Returns the value of field status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Returns the value of field send_email
     *
     * @return string
     */
    public function getSendEmail()
    {
        return $this->send_email;
    }

    /**
     * Returns the value of field email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Returns the value of field date_creation_sale
     *
     * @return string
     */
    public function getDateCreationSale()
    {
        return $this->date_creation_sale;
    }

    /**
     * Returns the value of field name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Returns the value of field phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Returns the value of field clid
     *
     * @return integer
     */
    public function getClid()
    {
        return $this->clid;
    }

    /**
     * Returns the value of field identifier
     *
     * @return string
     */
    public function getIdentifier()
    {
        return $this->identifier;
    }

    /**
     * Returns the value of field name_client
     *
     * @return string
     */
    public function getNameClient()
    {
        return $this->name_client;
    }

    /**
     * Returns the value of field last_name_client
     *
     * @return string
     */
    public function getLastNameClient()
    {
        return $this->last_name_client;
    }

    /**
     * Returns the value of field second_name_client
     *
     * @return string
     */
    public function getSecondNameClient()
    {
        return $this->second_name_client;
    }

    /**
     * Returns the value of field email_client
     *
     * @return string
     */
    public function getEmailClient()
    {
        return $this->email_client;
    }

    /**
     * Returns the value of field uid_attend
     *
     * @return integer
     */
    public function getUidAttend()
    {
        return $this->uid_attend;
    }

    /**
     * Returns the value of field name_attend
     *
     * @return string
     */
    public function getNameAttend()
    {
        return $this->name_attend;
    }

    /**
     * Returns the value of field last_name_attend
     *
     * @return string
     */
    public function getLastNameAttend()
    {
        return $this->last_name_attend;
    }

    /**
     * Returns the value of field second_name_attend
     *
     * @return string
     */
    public function getSecondNameAttend()
    {
        return $this->second_name_attend;
    }

    /**
     * Validations and business logic
     *
     * @return boolean
     */
    public function validation()
    {
        $this->validate(
            new Email(
                array(
                    'field'    => 'email',
                    'required' => true,
                )
            )
        );

        if ($this->validationHasFailed() == true) {
            return false;
        }

        return true;
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'v_shopping_general';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return VShoppingGeneral[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return VShoppingGeneral
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
