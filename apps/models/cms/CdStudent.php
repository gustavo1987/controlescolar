<?php
namespace Modules\Models\Cms;
use Phalcon\Validation;
use Phalcon\Validation\Validator\Email as EmailValidator;

class CdStudent extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(column="stuid", type="integer", length=11, nullable=false)
     */
    protected $stuid;

    /**
     *
     * @var string
     * @Column(column="name", type="string", length=50, nullable=true)
     */
    protected $name;

    /**
     *
     * @var string
     * @Column(column="lastname", type="string", length=50, nullable=true)
     */
    protected $lastname;

    /**
     *
     * @var string
     * @Column(column="secondname", type="string", length=50, nullable=true)
     */
    protected $secondname;

    /**
     *
     * @var string
     * @Column(column="email", type="string", length=50, nullable=true)
     */
    protected $email;

    /**
     *
     * @var string
     * @Column(column="phone", type="string", length=50, nullable=true)
     */
    protected $phone;

    /**
     *
     * @var string
     * @Column(column="sex", type="string", length=1, nullable=true)
     */
    protected $sex;

    /**
     *
     * @var string
     * @Column(column="calle", type="string", length=50, nullable=true)
     */
    protected $calle;

    /**
     *
     * @var string
     * @Column(column="numExt", type="string", length=10, nullable=true)
     */
    protected $numExt;

    /**
     *
     * @var string
     * @Column(column="numInt", type="string", length=10, nullable=true)
     */
    protected $numInt;

    /**
     *
     * @var string
     * @Column(column="cp", type="string", length=10, nullable=true)
     */
    protected $cp;

    /**
     *
     * @var string
     * @Column(column="ciudad", type="string", length=50, nullable=true)
     */
    protected $ciudad;

    /**
     *
     * @var string
     * @Column(column="estado", type="string", length=50, nullable=true)
     */
    protected $estado;

    /**
     *
     * @var string
     * @Column(column="pais", type="string", length=50, nullable=true)
     */
    protected $pais;

    /**
     *
     * @var string
     * @Column(column="datecreation", type="string", nullable=true)
     */
    protected $datecreation;

    /**
     *
     * @var string
     * @Column(column="comproba", type="string", length=50, nullable=true)
     */
    protected $comproba;

    /**
     *
     * @var string
     * @Column(column="acta", type="string", length=50, nullable=true)
     */
    protected $acta;

    /**
     *
     * @var string
     * @Column(column="username", type="string", length=100, nullable=true)
     */
    protected $username;

    /**
     *
     * @var string
     * @Column(column="password", type="string", length=200, nullable=true)
     */
    protected $password;

    /**
     *
     * @var string
     * @Column(column="status", type="string", length=30, nullable=true)
     */
    protected $status;

    /**
     *
     * @var string
     * @Column(column="career", type="string", nullable=true)
     */
    protected $postgrado;

    /**
     *
     * @var string
     * @Column(column="career", type="string", nullable=true)
     */
    protected $career;

    /**
     * Method to set the value of field stuid
     *
     * @param integer $stuid
     * @return $this
     */
    public function setStuid($stuid)
    {
        $this->stuid = $stuid;

        return $this;
    }

    /**
     * Method to set the value of field name
     *
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Method to set the value of field lastname
     *
     * @param string $lastname
     * @return $this
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * Method to set the value of field secondname
     *
     * @param string $secondname
     * @return $this
     */
    public function setSecondname($secondname)
    {
        $this->secondname = $secondname;

        return $this;
    }

    /**
     * Method to set the value of field email
     *
     * @param string $email
     * @return $this
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Method to set the value of field phone
     *
     * @param string $phone
     * @return $this
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Method to set the value of field sex
     *
     * @param string $sex
     * @return $this
     */
    public function setSex($sex)
    {
        $this->sex = $sex;

        return $this;
    }

    /**
     * Method to set the value of field calle
     *
     * @param string $calle
     * @return $this
     */
    public function setCalle($calle)
    {
        $this->calle = $calle;

        return $this;
    }

    /**
     * Method to set the value of field numExt
     *
     * @param string $numExt
     * @return $this
     */
    public function setNumExt($numExt)
    {
        $this->numExt = $numExt;

        return $this;
    }

    /**
     * Method to set the value of field numInt
     *
     * @param string $numInt
     * @return $this
     */
    public function setNumInt($numInt)
    {
        $this->numInt = $numInt;

        return $this;
    }

    /**
     * Method to set the value of field cp
     *
     * @param string $cp
     * @return $this
     */
    public function setCp($cp)
    {
        $this->cp = $cp;

        return $this;
    }

    /**
     * Method to set the value of field ciudad
     *
     * @param string $ciudad
     * @return $this
     */
    public function setCiudad($ciudad)
    {
        $this->ciudad = $ciudad;

        return $this;
    }

    /**
     * Method to set the value of field estado
     *
     * @param string $estado
     * @return $this
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Method to set the value of field pais
     *
     * @param string $pais
     * @return $this
     */
    public function setPais($pais)
    {
        $this->pais = $pais;

        return $this;
    }

    /**
     * Method to set the value of field datecreation
     *
     * @param string $datecreation
     * @return $this
     */
    public function setDatecreation($datecreation)
    {
        $this->datecreation = $datecreation;

        return $this;
    }

    /**
     * Method to set the value of field comproba
     *
     * @param string $comproba
     * @return $this
     */
    public function setComproba($comproba)
    {
        $this->comproba = $comproba;

        return $this;
    }

    /**
     * Method to set the value of field acta
     *
     * @param string $acta
     * @return $this
     */
    public function setActa($acta)
    {
        $this->acta = $acta;

        return $this;
    }

    /**
     * Method to set the value of field username
     *
     * @param string $username
     * @return $this
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Method to set the value of field password
     *
     * @param string $password
     * @return $this
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Method to set the value of field status
     *
     * @param string $status
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Method to set the value of field postgrado
     *
     * @param string $postgrado
     * @return $this
     */
    public function setPostgrado($postgrado)
    {
        $this->postgrado = $postgrado;

        return $this;
    }

    /**
     * Method to set the value of field career
     *
     * @param string $career
     * @return $this
     */
    public function setCareer($career)
    {
        $this->career = $career;

        return $this;
    }

    /**
     * Returns the value of field stuid
     *
     * @return integer
     */
    public function getStuid()
    {
        return $this->stuid;
    }

    /**
     * Returns the value of field name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Returns the value of field lastname
     *
     * @return string
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Returns the value of field secondname
     *
     * @return string
     */
    public function getSecondname()
    {
        return $this->secondname;
    }

    /**
     * Returns the value of field email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Returns the value of field phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Returns the value of field sex
     *
     * @return string
     */
    public function getSex()
    {
        return $this->sex;
    }

    /**
     * Returns the value of field calle
     *
     * @return string
     */
    public function getCalle()
    {
        return $this->calle;
    }

    /**
     * Returns the value of field numExt
     *
     * @return string
     */
    public function getNumExt()
    {
        return $this->numExt;
    }

    /**
     * Returns the value of field numInt
     *
     * @return string
     */
    public function getNumInt()
    {
        return $this->numInt;
    }

    /**
     * Returns the value of field cp
     *
     * @return string
     */
    public function getCp()
    {
        return $this->cp;
    }

    /**
     * Returns the value of field ciudad
     *
     * @return string
     */
    public function getCiudad()
    {
        return $this->ciudad;
    }

    /**
     * Returns the value of field estado
     *
     * @return string
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Returns the value of field pais
     *
     * @return string
     */
    public function getPais()
    {
        return $this->pais;
    }

    /**
     * Returns the value of field datecreation
     *
     * @return string
     */
    public function getDatecreation()
    {
        return $this->datecreation;
    }

    /**
     * Returns the value of field comproba
     *
     * @return string
     */
    public function getComproba()
    {
        return $this->comproba;
    }

    /**
     * Returns the value of field acta
     *
     * @return string
     */
    public function getActa()
    {
        return $this->acta;
    }

    /**
     * Returns the value of field username
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Returns the value of field password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Returns the value of field status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Returns the value of field postgrado
     *
     * @return string
     */
    public function getPostgrado()
    {
        return $this->postgrado;
    }

    /**
     * Returns the value of field career
     *
     * @return string
     */
    public function getCareer()
    {
        return $this->career;
    }

    /**
     * Validations and business logic
     *
     * @return boolean
     */
    /*
    public function validation()
    {
        $validator = new Validation();

        $validator->add(
            'email',
            new EmailValidator(
                [
                    'model'   => $this,
                    'message' => 'Please enter a correct email address',
                ]
            )
        );

        return $this->validate($validator);
    }
    */
    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("cd_ibedel");
        $this->setSource("cd_student");
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'cd_student';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return CdStudent[]|CdStudent|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return CdStudent|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
