<?php
namespace Modules\Models\Cms;
use Phalcon\Validation;
use Phalcon\Validation\Validator\Email as EmailValidator;
class CdInscripciones extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(column="insid", type="integer", length=11, nullable=false)
     */
    protected $insid;

    /**
     *
     * @var integer
     * @Column(column="catid", type="integer", length=11, nullable=true)
     */
    protected $catid;

    /**
     *
     * @var integer
     * @Column(column="estudio", type="integer", length=11, nullable=true)
     */
    protected $estudio;

    /**
     *
     * @var string
     * @Column(column="periodo", type="string", length=100, nullable=true)
     */
    protected $periodo;

    /**
     *
     * @var string
     * @Column(column="acepto", type="string", nullable=true)
     */
    protected $acepto;

    /**
     *
     * @var integer
     * @Column(column="stuid", type="integer", length=11, nullable=true)
     */
    protected $stuid;

    /**
     *
     * @var string
     * @Column(column="nacionalidad", type="string", length=150, nullable=true)
     */
    protected $nacionalidad;

    /**
     *
     * @var string
     * @Column(column="documento_acreditacion", type="string", length=150, nullable=true)
     */
    protected $documento_acreditacion;

    /**
     *
     * @var string
     * @Column(column="numero_documento_acreditacion", type="string", length=150, nullable=true)
     */
    protected $numero_documento_acreditacion;

    /**
     *
     * @var string
     * @Column(column="fecha_nacimiento", type="string", nullable=true)
     */
    protected $fecha_nacimiento;

    /**
     *
     * @var string
     * @Column(column="pais_nacimiento", type="string", length=150, nullable=true)
     */
    protected $pais_nacimiento;

    /**
     *
     * @var string
     * @Column(column="domicilio_nacimiento", type="string", length=150, nullable=true)
     */
    protected $domicilio_nacimiento;

    /**
     *
     * @var string
     * @Column(column="cp_nacimiento", type="string", length=150, nullable=true)
     */
    protected $cp_nacimiento;

    /**
     *
     * @var string
     * @Column(column="ciudad_nacimiento", type="string", length=150, nullable=true)
     */
    protected $ciudad_nacimiento;

    /**
     *
     * @var string
     * @Column(column="telefono_residencia", type="string", length=150, nullable=true)
     */
    protected $telefono_residencia;

    /**
     *
     * @var string
     * @Column(column="fijo_residencia", type="string", length=150, nullable=true)
     */
    protected $fijo_residencia;

    /**
     *
     * @var string
     * @Column(column="difusion", type="string", nullable=true)
     */
    protected $difusion;

    /**
     *
     * @var string
     * @Column(column="estado_nacimiento", type="string", length=150, nullable=true)
     */
    protected $estado_nacimiento;

    /**
     *
     * @var string
     * @Column(column="universidad", type="string", length=150, nullable=true)
     */
    protected $universidad;

    /**
     *
     * @var string
     * @Column(column="pais_estudios", type="string", length=150, nullable=true)
     */
    protected $pais_estudios;

    /**
     *
     * @var string
     * @Column(column="nombre_titulo", type="string", length=150, nullable=true)
     */
    protected $nombre_titulo;

    /**
     *
     * @var string
     * @Column(column="tipo_titulo", type="string", length=150, nullable=true)
     */
    protected $tipo_titulo;

    /**
     *
     * @var string
     * @Column(column="estado_estudios", type="string", length=150, nullable=true)
     */
    protected $estado_estudios;

    /**
     *
     * @var string
     * @Column(column="anio_cursado", type="string", length=150, nullable=true)
     */
    protected $anio_cursado;

    /**
     *
     * @var string
     * @Column(column="fecha_finalizacion", type="string", length=150, nullable=true)
     */
    protected $fecha_finalizacion;

    /**
     *
     * @var string
     * @Column(column="tel_personal", type="string", length=150, nullable=true)
     */
    protected $tel_personal;

    /**
     *
     * @var string
     * @Column(column="fijo_personal", type="string", nullable=true)
     */
    protected $fijo_personal;

    /**
     *
     * @var string
     * @Column(column="is_over", type="string", nullable=true)
     */
    protected $is_over;

    /**
     *
     * @var string
     * @Column(column="estatus", type="string", nullable=true)
     */
    protected $estatus;

    /**
     *
     * @var string
     * @Column(column="beca", type="string", nullable=true)
     */
    protected $beca;

    /**
     * Method to set the value of field insid
     *
     * @param integer $insid
     * @return $this
     */
    public function setInsid($insid)
    {
        $this->insid = $insid;

        return $this;
    }

    /**
     * Method to set the value of field catid
     *
     * @param integer $catid
     * @return $this
     */
    public function setCatid($catid)
    {
        $this->catid = $catid;

        return $this;
    }

    /**
     * Method to set the value of field estudio
     *
     * @param integer $estudio
     * @return $this
     */
    public function setEstudio($estudio)
    {
        $this->estudio = $estudio;

        return $this;
    }

    /**
     * Method to set the value of field periodo
     *
     * @param string $periodo
     * @return $this
     */
    public function setPeriodo($periodo)
    {
        $this->periodo = $periodo;

        return $this;
    }

    /**
     * Method to set the value of field acepto
     *
     * @param string $acepto
     * @return $this
     */
    public function setAcepto($acepto)
    {
        $this->acepto = $acepto;

        return $this;
    }

    /**
     * Method to set the value of field stuid
     *
     * @param integer $stuid
     * @return $this
     */
    public function setStuid($stuid)
    {
        $this->stuid = $stuid;

        return $this;
    }

    /**
     * Method to set the value of field nacionalidad
     *
     * @param string $nacionalidad
     * @return $this
     */
    public function setNacionalidad($nacionalidad)
    {
        $this->nacionalidad = $nacionalidad;

        return $this;
    }

    /**
     * Method to set the value of field documento_acreditacion
     *
     * @param string $documento_acreditacion
     * @return $this
     */
    public function setDocumentoAcreditacion($documento_acreditacion)
    {
        $this->documento_acreditacion = $documento_acreditacion;

        return $this;
    }

    /**
     * Method to set the value of field numero_documento_acreditacion
     *
     * @param string $numero_documento_acreditacion
     * @return $this
     */
    public function setNumeroDocumentoAcreditacion($numero_documento_acreditacion)
    {
        $this->numero_documento_acreditacion = $numero_documento_acreditacion;

        return $this;
    }

    /**
     * Method to set the value of field fecha_nacimiento
     *
     * @param string $fecha_nacimiento
     * @return $this
     */
    public function setFechaNacimiento($fecha_nacimiento)
    {
        $this->fecha_nacimiento = $fecha_nacimiento;

        return $this;
    }

    /**
     * Method to set the value of field pais_nacimiento
     *
     * @param string $pais_nacimiento
     * @return $this
     */
    public function setPaisNacimiento($pais_nacimiento)
    {
        $this->pais_nacimiento = $pais_nacimiento;

        return $this;
    }

    /**
     * Method to set the value of field domicilio_nacimiento
     *
     * @param string $domicilio_nacimiento
     * @return $this
     */
    public function setDomicilioNacimiento($domicilio_nacimiento)
    {
        $this->domicilio_nacimiento = $domicilio_nacimiento;

        return $this;
    }

    /**
     * Method to set the value of field cp_nacimiento
     *
     * @param string $cp_nacimiento
     * @return $this
     */
    public function setCpNacimiento($cp_nacimiento)
    {
        $this->cp_nacimiento = $cp_nacimiento;

        return $this;
    }

    /**
     * Method to set the value of field ciudad_nacimiento
     *
     * @param string $ciudad_nacimiento
     * @return $this
     */
    public function setCiudadNacimiento($ciudad_nacimiento)
    {
        $this->ciudad_nacimiento = $ciudad_nacimiento;

        return $this;
    }

    /**
     * Method to set the value of field telefono_residencia
     *
     * @param string $telefono_residencia
     * @return $this
     */
    public function setTelefonoResidencia($telefono_residencia)
    {
        $this->telefono_residencia = $telefono_residencia;

        return $this;
    }

    /**
     * Method to set the value of field fijo_residencia
     *
     * @param string $fijo_residencia
     * @return $this
     */
    public function setFijoResidencia($fijo_residencia)
    {
        $this->fijo_residencia = $fijo_residencia;

        return $this;
    }

    /**
     * Method to set the value of field difusion
     *
     * @param string $difusion
     * @return $this
     */
    public function setDifusion($difusion)
    {
        $this->difusion = $difusion;

        return $this;
    }

    /**
     * Method to set the value of field estado_nacimiento
     *
     * @param string $estado_nacimiento
     * @return $this
     */
    public function setEstadoNacimiento($estado_nacimiento)
    {
        $this->estado_nacimiento = $estado_nacimiento;

        return $this;
    }

    /**
     * Method to set the value of field universidad
     *
     * @param string $universidad
     * @return $this
     */
    public function setUniversidad($universidad)
    {
        $this->universidad = $universidad;

        return $this;
    }

    /**
     * Method to set the value of field pais_estudios
     *
     * @param string $pais_estudios
     * @return $this
     */
    public function setPaisEstudios($pais_estudios)
    {
        $this->pais_estudios = $pais_estudios;

        return $this;
    }

    /**
     * Method to set the value of field nombre_titulo
     *
     * @param string $nombre_titulo
     * @return $this
     */
    public function setNombreTitulo($nombre_titulo)
    {
        $this->nombre_titulo = $nombre_titulo;

        return $this;
    }

    /**
     * Method to set the value of field tipo_titulo
     *
     * @param string $tipo_titulo
     * @return $this
     */
    public function setTipoTitulo($tipo_titulo)
    {
        $this->tipo_titulo = $tipo_titulo;

        return $this;
    }

    /**
     * Method to set the value of field estado_estudios
     *
     * @param string $estado_estudios
     * @return $this
     */
    public function setEstadoEstudios($estado_estudios)
    {
        $this->estado_estudios = $estado_estudios;

        return $this;
    }

    /**
     * Method to set the value of field anio_cursado
     *
     * @param string $anio_cursado
     * @return $this
     */
    public function setAnioCursado($anio_cursado)
    {
        $this->anio_cursado = $anio_cursado;

        return $this;
    }

    /**
     * Method to set the value of field fecha_finalizacion
     *
     * @param string $fecha_finalizacion
     * @return $this
     */
    public function setFechaFinalizacion($fecha_finalizacion)
    {
        $this->fecha_finalizacion = $fecha_finalizacion;

        return $this;
    }

    /**
     * Method to set the value of field tel_personal
     *
     * @param string $tel_personal
     * @return $this
     */
    public function setTelPersonal($tel_personal)
    {
        $this->tel_personal = $tel_personal;

        return $this;
    }

    /**
     * Method to set the value of field fijo_personal
     *
     * @param string $fijo_personal
     * @return $this
     */
    public function setFijoPersonal($fijo_personal)
    {
        $this->fijo_personal = $fijo_personal;

        return $this;
    }

    /**
     * Method to set the value of field is_over
     *
     * @param string $is_over
     * @return $this
     */
    public function setIsOver($is_over)
    {
        $this->is_over = $is_over;

        return $this;
    }

    /**
     * Method to set the value of field estatus
     *
     * @param string $estatus
     * @return $this
     */
    public function setEstatus($estatus)
    {
        $this->estatus = $estatus;

        return $this;
    }

    /**
     * Method to set the value of field beca
     *
     * @param string $beca
     * @return $this
     */
    public function setBeca($beca)
    {
        $this->beca = $beca;

        return $this;
    }

    /**
     * Returns the value of field insid
     *
     * @return integer
     */
    public function getInsid()
    {
        return $this->insid;
    }

    /**
     * Returns the value of field catid
     *
     * @return integer
     */
    public function getCatid()
    {
        return $this->catid;
    }

    /**
     * Returns the value of field estudio
     *
     * @return integer
     */
    public function getEstudio()
    {
        return $this->estudio;
    }

    /**
     * Returns the value of field periodo
     *
     * @return string
     */
    public function getPeriodo()
    {
        return $this->periodo;
    }

    /**
     * Returns the value of field acepto
     *
     * @return string
     */
    public function getAcepto()
    {
        return $this->acepto;
    }

    /**
     * Returns the value of field stuid
     *
     * @return integer
     */
    public function getStuid()
    {
        return $this->stuid;
    }

    /**
     * Returns the value of field nacionalidad
     *
     * @return string
     */
    public function getNacionalidad()
    {
        return $this->nacionalidad;
    }

    /**
     * Returns the value of field documento_acreditacion
     *
     * @return string
     */
    public function getDocumentoAcreditacion()
    {
        return $this->documento_acreditacion;
    }

    /**
     * Returns the value of field numero_documento_acreditacion
     *
     * @return string
     */
    public function getNumeroDocumentoAcreditacion()
    {
        return $this->numero_documento_acreditacion;
    }

    /**
     * Returns the value of field fecha_nacimiento
     *
     * @return string
     */
    public function getFechaNacimiento()
    {
        return $this->fecha_nacimiento;
    }

    /**
     * Returns the value of field pais_nacimiento
     *
     * @return string
     */
    public function getPaisNacimiento()
    {
        return $this->pais_nacimiento;
    }

    /**
     * Returns the value of field domicilio_nacimiento
     *
     * @return string
     */
    public function getDomicilioNacimiento()
    {
        return $this->domicilio_nacimiento;
    }

    /**
     * Returns the value of field cp_nacimiento
     *
     * @return string
     */
    public function getCpNacimiento()
    {
        return $this->cp_nacimiento;
    }

    /**
     * Returns the value of field ciudad_nacimiento
     *
     * @return string
     */
    public function getCiudadNacimiento()
    {
        return $this->ciudad_nacimiento;
    }

    /**
     * Returns the value of field telefono_residencia
     *
     * @return string
     */
    public function getTelefonoResidencia()
    {
        return $this->telefono_residencia;
    }

    /**
     * Returns the value of field fijo_residencia
     *
     * @return string
     */
    public function getFijoResidencia()
    {
        return $this->fijo_residencia;
    }

    /**
     * Returns the value of field difusion
     *
     * @return string
     */
    public function getDifusion()
    {
        return $this->difusion;
    }

    /**
     * Returns the value of field estado_nacimiento
     *
     * @return string
     */
    public function getEstadoNacimiento()
    {
        return $this->estado_nacimiento;
    }

    /**
     * Returns the value of field universidad
     *
     * @return string
     */
    public function getUniversidad()
    {
        return $this->universidad;
    }

    /**
     * Returns the value of field pais_estudios
     *
     * @return string
     */
    public function getPaisEstudios()
    {
        return $this->pais_estudios;
    }

    /**
     * Returns the value of field nombre_titulo
     *
     * @return string
     */
    public function getNombreTitulo()
    {
        return $this->nombre_titulo;
    }

    /**
     * Returns the value of field tipo_titulo
     *
     * @return string
     */
    public function getTipoTitulo()
    {
        return $this->tipo_titulo;
    }

    /**
     * Returns the value of field estado_estudios
     *
     * @return string
     */
    public function getEstadoEstudios()
    {
        return $this->estado_estudios;
    }

    /**
     * Returns the value of field anio_cursado
     *
     * @return string
     */
    public function getAnioCursado()
    {
        return $this->anio_cursado;
    }

    /**
     * Returns the value of field fecha_finalizacion
     *
     * @return string
     */
    public function getFechaFinalizacion()
    {
        return $this->fecha_finalizacion;
    }

    /**
     * Returns the value of field tel_personal
     *
     * @return string
     */
    public function getTelPersonal()
    {
        return $this->tel_personal;
    }

    /**
     * Returns the value of field fijo_personal
     *
     * @return string
     */
    public function getFijoPersonal()
    {
        return $this->fijo_personal;
    }

    /**
     * Returns the value of field is_over
     *
     * @return string
     */
    public function getIsOver()
    {
        return $this->is_over;
    }

    /**
     * Returns the value of field estatus
     *
     * @return string
     */
    public function getEstatus()
    {
        return $this->estatus;
    }

    /**
     * Returns the value of field beca
     *
     * @return string
     */
    public function getBeca()
    {
        return $this->beca;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("cd_ibedel");
        $this->setSource("cd_inscripciones");
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'cd_inscripciones';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return CdInscripciones[]|CdInscripciones|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return CdInscripciones|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
