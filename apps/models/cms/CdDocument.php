<?php
namespace Modules\Models\Cms;
use Phalcon\Validation;
use Phalcon\Validation\Validator\Email as EmailValidator;

class CdDocument extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     * @Primary
     * @Column(column="docid", type="integer", length=11, nullable=false)
     */
    protected $docid;

    /**
     *
     * @var string
     * @Column(column="name", type="string", length=150, nullable=true)
     */
    protected $name;

    /**
     *
     * @var string
     * @Column(column="type", type="string", nullable=true)
     */
    protected $type;

    /**
     *
     * @var integer
     * @Column(column="stuid", type="integer", length=11, nullable=true)
     */
    protected $date_creation;

    /**
     *
     * @var integer
     * @Column(column="stuid", type="integer", length=11, nullable=true)
     */
    protected $stuid;

    /**
     * Method to set the value of field docid
     *
     * @param integer $docid
     * @return $this
     */
    public function setDocid($docid)
    {
        $this->docid = $docid;

        return $this;
    }

    /**
     * Method to set the value of field name
     *
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Method to set the value of field type
     *
     * @param string $type
     * @return $this
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Method to set the value of field date_creation
     *
     * @param string $date_creation
     * @return $this
     */
    public function setDateCreation($date_creation)
    {
        $this->date_creation = $date_creation;

        return $this;
    }

    /**
     * Method to set the value of field stuid
     *
     * @param integer $stuid
     * @return $this
     */
    public function setStuid($stuid)
    {
        $this->stuid = $stuid;

        return $this;
    }

    /**
     * Returns the value of field docid
     *
     * @return integer
     */
    public function getDocid()
    {
        return $this->docid;
    }

    /**
     * Returns the value of field name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Returns the value of field type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Returns the value of field date_creation
     *
     * @return string
     */
    public function getDateCreation()
    {
        return $this->date_creation;
    }

    /**
     * Returns the value of field stuid
     *
     * @return integer
     */
    public function getStuid()
    {
        return $this->stuid;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("cd_ibedel");
        $this->setSource("cd_documents");
        $this->belongsTo('stuid', '\CdStudent', 'stuid', ['alias' => 'CdStudent']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'cd_documents';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return CdDocuments[]|CdDocuments|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return CdDocuments|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
