<?php
namespace Modules\Models;


class VWorkingUserClase extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    protected $workid;

    /**
     *
     * @var string
     */
    protected $status;

    /**
     *
     * @var string
     */
    protected $name;

    /**
     *
     * @var string
     */
    protected $date_creation;

    /**
     *
     * @var string
     */
    protected $date_limit;

    /**
     *
     * @var string
     */
    protected $fileteacher;

    /**
     *
     * @var string
     */
    protected $filestudent;

    /**
     *
     * @var string
     */
    protected $type;

    /**
     *
     * @var string
     */
    protected $calificacion;

    /**
     *
     * @var integer
     */
    protected $uid;

    /**
     *
     * @var string
     */
    protected $name_user;

    /**
     *
     * @var string
     */
    protected $last_name;

    /**
     *
     * @var string
     */
    protected $second_name;

    /**
     *
     * @var string
     */
    protected $sex;

    /**
     *
     * @var string
     */
    protected $phone;

    /**
     *
     * @var string
     */
    protected $email;

    /**
     *
     * @var string
     */
    protected $status_user;

    /**
     *
     * @var integer
     */
    protected $clasid;

    /**
     *
     * @var integer
     */
    protected $pid;

    /**
     *
     * @var string
     */
    protected $name_clase;

    /**
     *
     * @var string
     */
    protected $status_clase;

    /**
     *
     * @var integer
     */
    protected $carrera;

    /**
     * Method to set the value of field workid
     *
     * @param integer $workid
     * @return $this
     */
    public function setworkid($workid)
    {
        $this->workid = $workid;

        return $this;
    }

    /**
     * Method to set the value of field status
     *
     * @param string $status
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Method to set the value of field name
     *
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Method to set the value of field date_creation
     *
     * @param string $date_creation
     * @return $this
     */
    public function setDateCreation($date_creation)
    {
        $this->date_creation = $date_creation;

        return $this;
    }

    /**
     * Method to set the value of field date_limit
     *
     * @param string $date_limit
     * @return $this
     */
    public function setDateLimit($date_limit)
    {
        $this->date_limit = $date_limit;

        return $this;
    }

    /**
     * Method to set the value of field fileteacher
     *
     * @param integer $fileteacher
     * @return $this
     */
    public function setFileteacher($fileteacher)
    {
        $this->fileteacher = $fileteacher;

        return $this;
    }

    /**
     * Method to set the value of field type
     *
     * @param integer $type
     * @return $this
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Method to set the value of field calificacion
     *
     * @param integer $calificacion
     * @return $this
     */
    public function setCalificacion($calificacion)
    {
        $this->calificacion = $calificacion;

        return $this;
    }

    /**
     * Method to set the value of field uid
     *
     * @param integer $uid
     * @return $this
     */
    public function setUid($uid)
    {
        $this->uid = $uid;

        return $this;
    }

    /**
     * Method to set the value of field name_user
     *
     * @param string $name_user
     * @return $this
     */
    public function setNameUser($name_user)
    {
        $this->name_user = $name_user;

        return $this;
    }

    /**
     * Method to set the value of field last_name
     *
     * @param string $last_name
     * @return $this
     */
    public function setLastName($last_name)
    {
        $this->last_name = $last_name;

        return $this;
    }

    /**
     * Method to set the value of field second_name
     *
     * @param string $second_name
     * @return $this
     */
    public function setSecondName($second_name)
    {
        $this->second_name = $second_name;

        return $this;
    }

    /**
     * Method to set the value of field sex
     *
     * @param string $sex
     * @return $this
     */
    public function setSex($sex)
    {
        $this->sex = $sex;

        return $this;
    }

    /**
     * Method to set the value of field phone
     *
     * @param string $phone
     * @return $this
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Method to set the value of field email
     *
     * @param string $email
     * @return $this
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Method to set the value of field status_user
     *
     * @param string $status_user
     * @return $this
     */
    public function setStatusUser($status_user)
    {
        $this->status_user = $status_user;

        return $this;
    }

    /**
     * Method to set the value of field clasid
     *
     * @param integer $clasid
     * @return $this
     */
    public function setClasid($clasid)
    {
        $this->clasid = $clasid;

        return $this;
    }

    /**
     * Method to set the value of field pid
     *
     * @param integer $pid
     * @return $this
     */
    public function setPid($pid)
    {
        $this->pid = $pid;

        return $this;
    }

    /**
     * Method to set the value of field name_clase
     *
     * @param string $name_clase
     * @return $this
     */
    public function setNameClase($name_clase)
    {
        $this->name_clase = $name_clase;

        return $this;
    }

    /**
     * Method to set the value of field status_clase
     *
     * @param string $status_clase
     * @return $this
     */
    public function setStatusClase($status_clase)
    {
        $this->status_clase = $status_clase;

        return $this;
    }

    /**
     * Method to set the value of field carrera
     *
     * @param integer $carrera
     * @return $this
     */
    public function setCarrera($carrera)
    {
        $this->carrera = $carrera;

        return $this;
    }

    /**
     * Returns the value of field workid
     *
     * @return integer
     */
    public function getworkid()
    {
        return $this->workid;
    }

    /**
     * Returns the value of field status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Returns the value of field name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Returns the value of field date_creation
     *
     * @return string
     */
    public function getDateCreation()
    {
        return $this->date_creation;
    }

    /**
     * Returns the value of field date_limit
     *
     * @return string
     */
    public function getDateLimit()
    {
        return $this->date_limit;
    }

    /**
     * Returns the value of field fileteacher
     *
     * @return integer
     */
    public function getFileteacher()
    {
        return $this->fileteacher;
    }

    /**
     * Returns the value of field filestudent
     *
     * @return integer
     */
    public function getFilestudent()
    {
        return $this->filestudent;
    }

    /**
     * Returns the value of field type
     *
     * @return integer
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Returns the value of field calificacion
     *
     * @return integer
     */
    public function getCalificacion()
    {
        return $this->calificacion;
    }

    /**
     * Returns the value of field uid
     *
     * @return integer
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * Returns the value of field name_user
     *
     * @return string
     */
    public function getNameUser()
    {
        return $this->name_user;
    }

    /**
     * Returns the value of field last_name
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->last_name;
    }

    /**
     * Returns the value of field second_name
     *
     * @return string
     */
    public function getSecondName()
    {
        return $this->second_name;
    }

    /**
     * Returns the value of field sex
     *
     * @return string
     */
    public function getSex()
    {
        return $this->sex;
    }

    /**
     * Returns the value of field phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Returns the value of field email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Returns the value of field status_user
     *
     * @return string
     */
    public function getStatusUser()
    {
        return $this->status_user;
    }

    /**
     * Returns the value of field clasid
     *
     * @return integer
     */
    public function getClasid()
    {
        return $this->clasid;
    }

    /**
     * Returns the value of field pid
     *
     * @return integer
     */
    public function getPid()
    {
        return $this->pid;
    }

    /**
     * Returns the value of field name_clase
     *
     * @return string
     */
    public function getNameClase()
    {
        return $this->name_clase;
    }

    /**
     * Returns the value of field status_clase
     *
     * @return string
     */
    public function getStatusClase()
    {
        return $this->status_clase;
    }

    /**
     * Returns the value of field carrera
     *
     * @return integer
     */
    public function getCarrera()
    {
        return $this->carrera;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->hasMany('uid', 'CdUser', 'uid', array('alias' => 'CdUser'));
        $this->hasMany('clasid', 'CdClase', 'clasid', array('alias' => 'CdClase'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'v_working_user_clase';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return VWorkingUserClase[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return VWorkingUserClase
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }


}