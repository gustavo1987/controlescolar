<?php
namespace Modules\Models;
class VProductBrand extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    protected $brid;

    /**
     *
     * @var string
     */
    protected $name_brand;

    /**
     *
     * @var string
     */
    protected $name_permalink;

    /**
     *
     * @var integer
     */
    protected $pid;

    /**
     *
     * @var string
     */
    protected $name;

    /**
     *
     * @var string
     */
    protected $permalink;

    /**
     *
     * @var string
     */
    protected $purchase_price;

    /**
     *
     * @var string
     */
    protected $sale_price;

    /**
     *
     * @var string
     */
    protected $image;

    /**
     *
     * @var string
     */
    protected $measure;

    /**
     *
     * @var string
     */
    protected $quantity;

    /**
     *
     * @var string
     */
    protected $percentage;

    /**
     *
     * @var string
     */
    protected $status;

    /**
     *
     * @var string
     */
    protected $ticket;

    /**
     *
     * @var string
     */
    protected $description;

    /**
     *
     * @var integer
     */
    protected $stid;

    /**
     *
     * @var integer
     */
    protected $stock;

    /**
     *
     * @var integer
     */
    protected $stock_max;

    /**
     *
     * @var integer
     */
    protected $stock_min;

    /**
     *
     * @var integer
     */
    protected $quantity_product;

    /**
     * Method to set the value of field brid
     *
     * @param integer $brid
     * @return $this
     */
    public function setBrid($brid)
    {
        $this->brid = $brid;

        return $this;
    }

    /**
     * Method to set the value of field name_brand
     *
     * @param string $name_brand
     * @return $this
     */
    public function setNameBrand($name_brand)
    {
        $this->name_brand = $name_brand;

        return $this;
    }

    /**
     * Method to set the value of field name_permalink
     *
     * @param string $name_permalink
     * @return $this
     */
    public function setNamePermalink($name_permalink)
    {
        $this->name_permalink = $name_permalink;

        return $this;
    }

    /**
     * Method to set the value of field pid
     *
     * @param integer $pid
     * @return $this
     */
    public function setPid($pid)
    {
        $this->pid = $pid;

        return $this;
    }

    /**
     * Method to set the value of field name
     *
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Method to set the value of field permalink
     *
     * @param string $permalink
     * @return $this
     */
    public function setPermalink($permalink)
    {
        $this->permalink = $permalink;

        return $this;
    }

    /**
     * Method to set the value of field purchase_price
     *
     * @param string $purchase_price
     * @return $this
     */
    public function setPurchasePrice($purchase_price)
    {
        $this->purchase_price = $purchase_price;

        return $this;
    }

    /**
     * Method to set the value of field sale_price
     *
     * @param string $sale_price
     * @return $this
     */
    public function setSalePrice($sale_price)
    {
        $this->sale_price = $sale_price;

        return $this;
    }

    /**
     * Method to set the value of field image
     *
     * @param string $image
     * @return $this
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Method to set the value of field measure
     *
     * @param string $measure
     * @return $this
     */
    public function setMeasure($measure)
    {
        $this->measure = $measure;

        return $this;
    }

    /**
     * Method to set the value of field quantity
     *
     * @param string $quantity
     * @return $this
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Method to set the value of field percentage
     *
     * @param string $percentage
     * @return $this
     */
    public function setPercentage($percentage)
    {
        $this->percentage = $percentage;

        return $this;
    }

    /**
     * Method to set the value of field status
     *
     * @param string $status
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Method to set the value of field ticket
     *
     * @param string $ticket
     * @return $this
     */
    public function setTicket($ticket)
    {
        $this->ticket = $ticket;

        return $this;
    }

    /**
     * Method to set the value of field description
     *
     * @param string $description
     * @return $this
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Method to set the value of field stid
     *
     * @param integer $stid
     * @return $this
     */
    public function setStid($stid)
    {
        $this->stid = $stid;

        return $this;
    }

    /**
     * Method to set the value of field stock
     *
     * @param integer $stock
     * @return $this
     */
    public function setStock($stock)
    {
        $this->stock = $stock;

        return $this;
    }

    /**
     * Method to set the value of field stock_max
     *
     * @param integer $stock_max
     * @return $this
     */
    public function setStockMax($stock_max)
    {
        $this->stock_max = $stock_max;

        return $this;
    }

    /**
     * Method to set the value of field stock_min
     *
     * @param integer $stock_min
     * @return $this
     */
    public function setStockMin($stock_min)
    {
        $this->stock_min = $stock_min;

        return $this;
    }

    /**
     * Method to set the value of field quantity_product
     *
     * @param integer $quantity_product
     * @return $this
     */
    public function setQuantityProduct($quantity_product)
    {
        $this->quantity_product = $quantity_product;

        return $this;
    }

    /**
     * Returns the value of field brid
     *
     * @return integer
     */
    public function getBrid()
    {
        return $this->brid;
    }

    /**
     * Returns the value of field name_brand
     *
     * @return string
     */
    public function getNameBrand()
    {
        return $this->name_brand;
    }

    /**
     * Returns the value of field name_permalink
     *
     * @return string
     */
    public function getNamePermalink()
    {
        return $this->name_permalink;
    }

    /**
     * Returns the value of field pid
     *
     * @return integer
     */
    public function getPid()
    {
        return $this->pid;
    }

    /**
     * Returns the value of field name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Returns the value of field permalink
     *
     * @return string
     */
    public function getPermalink()
    {
        return $this->permalink;
    }

    /**
     * Returns the value of field purchase_price
     *
     * @return string
     */
    public function getPurchasePrice()
    {
        return $this->purchase_price;
    }

    /**
     * Returns the value of field sale_price
     *
     * @return string
     */
    public function getSalePrice()
    {
        return $this->sale_price;
    }

    /**
     * Returns the value of field image
     *
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Returns the value of field measure
     *
     * @return string
     */
    public function getMeasure()
    {
        return $this->measure;
    }

    /**
     * Returns the value of field quantity
     *
     * @return string
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Returns the value of field percentage
     *
     * @return string
     */
    public function getPercentage()
    {
        return $this->percentage;
    }

    /**
     * Returns the value of field status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Returns the value of field ticket
     *
     * @return string
     */
    public function getTicket()
    {
        return $this->ticket;
    }

    /**
     * Returns the value of field description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }


    /**
     * Returns the value of field stid
     *
     * @return integer
     */
    public function getStid()
    {
        return $this->stid;
    }

    /**
     * Returns the value of field stock
     *
     * @return integer
     */
    public function getStock()
    {
        return $this->stock;
    }

    /**
     * Returns the value of field stock_max
     *
     * @return integer
     */
    public function getStockMax()
    {
        return $this->stock_max;
    }

    /**
     * Returns the value of field stock_min
     *
     * @return integer
     */
    public function getStockMin()
    {
        return $this->stock_min;
    }

    /**
     * Returns the value of field quantity_product
     *
     * @return integer
     */
    public function getQuantityProduct()
    {
        return $this->quantity_product;
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'v_product_brand';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return VProductBrand[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return VProductBrand
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
