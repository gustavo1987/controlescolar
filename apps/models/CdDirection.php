<?php
namespace Modules\Models;
class CdDirection extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    protected $did;

    /**
     *
     * @var string
     */
    protected $street;

    /**
     *
     * @var string
     */
    protected $colony;

    /**
     *
     * @var string
     */
    protected $city;

    /**
     *
     * @var string
     */
    protected $references;

    /**
     *
     * @var string
     */
    protected $cp;

    /**
     *
     * @var integer
     */
    protected $clid;

    /**
     *
     * @var integer
     */
    protected $mpid;

    /**
     * Method to set the value of field did
     *
     * @param integer $did
     * @return $this
     */
    public function setDid($did)
    {
        $this->did = $did;

        return $this;
    }

    /**
     * Method to set the value of field street
     *
     * @param string $street
     * @return $this
     */
    public function setStreet($street)
    {
        $this->street = $street;

        return $this;
    }

    /**
     * Method to set the value of field colony
     *
     * @param string $colony
     * @return $this
     */
    public function setColony($colony)
    {
        $this->colony = $colony;

        return $this;
    }

    /**
     * Method to set the value of field city
     *
     * @param string $city
     * @return $this
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Method to set the value of field references
     *
     * @param string $references
     * @return $this
     */
    public function setReferences($references)
    {
        $this->references = $references;

        return $this;
    }

    /**
     * Method to set the value of field cp
     *
     * @param string $cp
     * @return $this
     */
    public function setCp($cp)
    {
        $this->cp = $cp;

        return $this;
    }

    /**
     * Method to set the value of field clid
     *
     * @param integer $clid
     * @return $this
     */
    public function setClid($clid)
    {
        $this->clid = $clid;

        return $this;
    }

    /**
     * Method to set the value of field mpid
     *
     * @param integer $mpid
     * @return $this
     */
    public function setMpid($mpid)
    {
        $this->mpid = $mpid;

        return $this;
    }

    /**
     * Returns the value of field did
     *
     * @return integer
     */
    public function getDid()
    {
        return $this->did;
    }

    /**
     * Returns the value of field street
     *
     * @return string
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * Returns the value of field colony
     *
     * @return string
     */
    public function getColony()
    {
        return $this->colony;
    }

    /**
     * Returns the value of field city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Returns the value of field references
     *
     * @return string
     */
    public function getReferences()
    {
        return $this->references;
    }

    /**
     * Returns the value of field cp
     *
     * @return string
     */
    public function getCp()
    {
        return $this->cp;
    }

    /**
     * Returns the value of field clid
     *
     * @return integer
     */
    public function getClid()
    {
        return $this->clid;
    }

    /**
     * Returns the value of field mpid
     *
     * @return integer
     */
    public function getMpid()
    {
        return $this->mpid;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->belongsTo('mpid', 'CdMunicipios', 'mpid', array('alias' => 'CdMunicipios'));
        $this->belongsTo('clid', 'CdClient', 'clid', array('alias' => 'CdClient'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'cd_direction';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return CdDirection[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return CdDirection
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
