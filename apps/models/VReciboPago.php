<?php
namespace Modules\Models;
class VReciboPago extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    protected $recid;

    /**
     *
     * @var integer
     */
    protected $name;

    /**
     *
     * @var integer
     */
    protected $document;

    /**
     *
     * @var integer
     */
    protected $uid;

    /**
     *
     * @var string
     */
    protected $status;

    /**
     *
     * @var string
     */
    protected $date_creation;

    /**
     *
     * @var integer
     */
    protected $pagid;

    /**
     *
     * @var string
     */
    protected $namepago;

    /**
     *
     * @var integer
     */
    protected $cantidad;

    /**
     *
     * @var string
     */
    protected $date_creation_pago;

    /**
     *
     * @var string
     */
    protected $statuspago;

    /**
     *
     * @var string
     */
    protected $vencimiento;

    /**
     *
     * @var string
     */
    protected $nameuser;

    /**
     *
     * @var string
     */
    protected $last_name;

    /**
     *
     * @var string
     */
    protected $second_name;

    /**
     * Method to set the value of field recid
     *
     * @param integer $recid
     * @return $this
     */
    public function setRecid($recid)
    {
        $this->recid = $recid;

        return $this;
    }

    /**
     * Method to set the value of field name
     *
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Method to set the value of field document
     *
     * @param string $document
     * @return $this
     */
    public function setDocument($document)
    {
        $this->document = $document;

        return $this;
    }

    /**
     * Method to set the value of field uid
     *
     * @param integer $uid
     * @return $this
     */
    public function setUid($uid)
    {
        $this->uid = $uid;

        return $this;
    }

    /**
     * Method to set the value of field status
     *
     * @param string $status
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Method to set the value of field date_creation
     *
     * @param string $date_creation
     * @return $this
     */
    public function setDateCreation($date_creation)
    {
        $this->date_creation = $date_creation;

        return $this;
    }

    /**
     * Method to set the value of field pagid
     *
     * @param integer $pagid
     * @return $this
     */
    public function setPagid($pagid)
    {
        $this->pagid = $pagid;

        return $this;
    }

    /**
     * Method to set the value of field namepago
     *
     * @param string $namepago
     * @return $this
     */
    public function setNamepago($namepago)
    {
        $this->namepago = $namepago;

        return $this;
    }

    /**
     * Method to set the value of field cantidad
     *
     * @param integer $cantidad
     * @return $this
     */
    public function setCantidad($cantidad)
    {
        $this->cantidad = $cantidad;

        return $this;
    }

    /**
     * Method to set the value of field date_creation_pago
     *
     * @param string $date_creation_pago
     * @return $this
     */
    public function setDateCreationPago($date_creation_pago)
    {
        $this->date_creation_pago = $date_creation_pago;

        return $this;
    }

    /**
     * Method to set the value of field statuspago
     *
     * @param string $statuspago
     * @return $this
     */
    public function setStatuspago($statuspago)
    {
        $this->statuspago = $statuspago;

        return $this;
    }

    /**
     * Method to set the value of field vencimiento
     *
     * @param string $vencimiento
     * @return $this
     */
    public function setVencimiento($vencimiento)
    {
        $this->vencimiento = $vencimiento;

        return $this;
    }

    /**
     * Method to set the value of field nameuser
     *
     * @param string $nameuser
     * @return $this
     */
    public function setNameuser($nameuser)
    {
        $this->nameuser = $nameuser;

        return $this;
    }

    /**
     * Method to set the value of field last_name
     *
     * @param string $last_name
     * @return $this
     */
    public function setLastName($last_name)
    {
        $this->last_name = $last_name;

        return $this;
    }

    /**
     * Method to set the value of field second_name
     *
     * @param string $second_name
     * @return $this
     */
    public function setSecondName($second_name)
    {
        $this->second_name = $second_name;

        return $this;
    }

    /**
     * Returns the value of field recid
     *
     * @return integer
     */
    public function getRecid()
    {
        return $this->recid;
    }

    /**
     * Returns the value of field name
     *
     * @return integer
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Returns the value of field document
     *
     * @return integer
     */
    public function getDocument()
    {
        return $this->document;
    }

    /**
     * Returns the value of field uid
     *
     * @return integer
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * Returns the value of field status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Returns the value of field date_creation
     *
     * @return string
     */
    public function getDateCreation()
    {
        return $this->date_creation;
    }

    /**
     * Returns the value of field pagid
     *
     * @return integer
     */
    public function getPagid()
    {
        return $this->pagid;
    }

    /**
     * Returns the value of field namepago
     *
     * @return string
     */
    public function getNamepago()
    {
        return $this->namepago;
    }

    /**
     * Returns the value of field cantidad
     *
     * @return integer
     */
    public function getCantidad()
    {
        return $this->cantidad;
    }

    /**
     * Returns the value of field date_creation_pago
     *
     * @return string
     */
    public function getDateCreationPago()
    {
        return $this->date_creation_pago;
    }

    /**
     * Returns the value of field statuspago
     *
     * @return string
     */
    public function getStatuspago()
    {
        return $this->statuspago;
    }

    /**
     * Returns the value of field vencimiento
     *
     * @return string
     */
    public function getVencimiento()
    {
        return $this->vencimiento;
    }

    /**
     * Returns the value of field nameuser
     *
     * @return string
     */
    public function getNameuser()
    {
        return $this->nameuser;
    }

    /**
     * Returns the value of field second_name
     *
     * @return string
     */
    public function getSecondName()
    {
        return $this->second_name;
    }

    /**
     * Returns the value of field last_name
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->last_name;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->belongsTo('uid', 'CdUser', 'uid', array('alias' => 'CdUser'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'v_recibo_pago';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return VReciboPago[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return VReciboPago
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}