<?php
namespace Modules\Models;
class CdSubservices extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    protected $subsid;

    /**
     *
     * @var string
     */
    protected $name;

    /**
     *
     * @var string
     */
    protected $permalink;

    /**
     *
     * @var string
     */
    protected $price;

    /**
     *
     * @var string
     */
    protected $discount;

    /**
     *
     * @var integer
     */
    protected $order_subservices;

    /**
     *
     * @var string
     */
    protected $image;

    /**
     *
     * @var string
     */
    protected $date_creation;

    /**
     *
     * @var integer
     */
    protected $status;

    protected $servid;

    /**
     *
     * @var integer
     */
    protected $uid;

    /**
     * Method to set the value of field subsid
     *
     * @param integer $subsid
     * @return $this
     */
    public function setSubsid($subsid)
    {
        $this->subsid = $subsid;

        return $this;
    }

    /**
     * Method to set the value of field name
     *
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Method to set the value of field permalink
     *
     * @param string $permalink
     * @return $this
     */
    public function setPermalink($permalink)
    {
        $this->permalink = $permalink;

        return $this;
    }

    /**
     * Method to set the value of field price
     *
     * @param string $price
     * @return $this
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Method to set the value of field discount
     *
     * @param string $discount
     * @return $this
     */
    public function setDiscount($discount)
    {
        $this->discount = $discount;

        return $this;
    }

    /**
     * Method to set the value of field order_subservices
     *
     * @param integer $order_subservices
     * @return $this
     */
    public function setOrderSubservices($order_subservices)
    {
        $this->order_subservices = $order_subservices;

        return $this;
    }

    /**
     * Method to set the value of field image
     *
     * @param string $image
     * @return $this
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Method to set the value of field date_creation
     *
     * @param string $date_creation
     * @return $this
     */
    public function setDateCreation($date_creation)
    {
        $this->date_creation = $date_creation;

        return $this;
    }

    /**
     * Method to set the value of field servid
     *
     * @param integer $servid
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    public function setServid($servid)
    {
        $this->servid = $servid;

        return $this;
    }

    /**
     * Method to set the value of field uid
     *
     * @param integer $uid
     * @return $this
     */
    public function setUid($uid)
    {
        $this->uid = $uid;

        return $this;
    }

    /**
     * Returns the value of field subsid
     *
     * @return integer
     */
    public function getSubsid()
    {
        return $this->subsid;
    }

    /**
     * Returns the value of field name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Returns the value of field permalink
     *
     * @return string
     */
    public function getPermalink()
    {
        return $this->permalink;
    }

    /**
     * Returns the value of field price
     *
     * @return string
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Returns the value of field discount
     *
     * @return string
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * Returns the value of field order_subservices
     *
     * @return integer
     */
    public function getOrderSubservices()
    {
        return $this->order_subservices;
    }

    /**
     * Returns the value of field image
     *
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Returns the value of field date_creation
     *
     * @return string
     */
    public function getDateCreation()
    {
        return $this->date_creation;
    }

    /**
     * Returns the value of field servid
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }

    public function getServid()
    {
        return $this->servid;
    }

    /**
     * Returns the value of field uid
     *
     * @return integer
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->hasMany('subsid', 'CdSubservicesSale', 'subsid', array('alias' => 'CdSubservicesSale'));
        $this->hasMany('subsid', 'CdUserSubservices', 'subsid', array('alias' => 'CdUserSubservices'));
        $this->belongsTo('servid', 'CdServices', 'servid', array('alias' => 'CdServices'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'cd_subservices';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return CdSubservices[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return CdSubservices
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
