<?php
namespace Modules\Models;

class CdBilling extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    protected $bllid;

    /**
     *
     * @var string
     */
    protected $rfc;

    /**
     *
     * @var string
     */
    protected $bussiness_name;

    /**
     *
     * @var string
     */
    protected $street;

    /**
     *
     * @var string
     */
    protected $number_ext;

    /**
     *
     * @var string
     */
    protected $number_int;

    /**
     *
     * @var string
     */
    protected $colony;

    /**
     *
     * @var string
     */
    protected $location;

    /**
     *
     * @var string
     */
    protected $postal_code;

    /**
     *
     * @var string
     */
    protected $email;

    /**
     *
     * @var string
     */
    protected $iva;

    /**
     *
     * @var string
     */
    protected $date_creation;

    /**
     *
     * @var integer
     */
    protected $mpid;

    /**
     *
     * @var integer
     */
    protected $clid;

    /**
     * Method to set the value of field bllid
     *
     * @param integer $bllid
     * @return $this
     */
    public function setBllid($bllid)
    {
        $this->bllid = $bllid;

        return $this;
    }

    /**
     * Method to set the value of field rfc
     *
     * @param string $rfc
     * @return $this
     */
    public function setRfc($rfc)
    {
        $this->rfc = $rfc;

        return $this;
    }

    /**
     * Method to set the value of field bussiness_name
     *
     * @param string $bussiness_name
     * @return $this
     */
    public function setBussinessName($bussiness_name)
    {
        $this->bussiness_name = $bussiness_name;

        return $this;
    }

    /**
     * Method to set the value of field street
     *
     * @param string $street
     * @return $this
     */
    public function setStreet($street)
    {
        $this->street = $street;

        return $this;
    }

    /**
     * Method to set the value of field number_ext
     *
     * @param string $number_ext
     * @return $this
     */
    public function setNumberExt($number_ext)
    {
        $this->number_ext = $number_ext;

        return $this;
    }

    /**
     * Method to set the value of field number_int
     *
     * @param string $number_int
     * @return $this
     */
    public function setNumberInt($number_int)
    {
        $this->number_int = $number_int;

        return $this;
    }

    /**
     * Method to set the value of field colony
     *
     * @param string $colony
     * @return $this
     */
    public function setColony($colony)
    {
        $this->colony = $colony;

        return $this;
    }

    /**
     * Method to set the value of field location
     *
     * @param string $location
     * @return $this
     */
    public function setLocation($location)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * Method to set the value of field postal_code
     *
     * @param string $postal_code
     * @return $this
     */
    public function setPostalCode($postal_code)
    {
        $this->postal_code = $postal_code;

        return $this;
    }

    /**
     * Method to set the value of field email
     *
     * @param string $email
     * @return $this
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Method to set the value of field iva
     *
     * @param string $iva
     * @return $this
     */
    public function setIva($iva)
    {
        $this->iva = $iva;

        return $this;
    }

    /**
     * Method to set the value of field date_creation
     *
     * @param string $date_creation
     * @return $this
     */
    public function setDateCreation($date_creation)
    {
        $this->date_creation = $date_creation;

        return $this;
    }

    /**
     * Method to set the value of field mpid
     *
     * @param integer $mpid
     * @return $this
     */
    public function setMpid($mpid)
    {
        $this->mpid = $mpid;

        return $this;
    }

    /**
     * Method to set the value of field clid
     *
     * @param integer $clid
     * @return $this
     */
    public function setClid($clid)
    {
        $this->clid = $clid;

        return $this;
    }

    /**
     * Returns the value of field bllid
     *
     * @return integer
     */
    public function getBllid()
    {
        return $this->bllid;
    }

    /**
     * Returns the value of field rfc
     *
     * @return string
     */
    public function getRfc()
    {
        return $this->rfc;
    }

    /**
     * Returns the value of field bussiness_name
     *
     * @return string
     */
    public function getBussinessName()
    {
        return $this->bussiness_name;
    }

    /**
     * Returns the value of field street
     *
     * @return string
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * Returns the value of field number_ext
     *
     * @return string
     */
    public function getNumberExt()
    {
        return $this->number_ext;
    }

    /**
     * Returns the value of field number_int
     *
     * @return string
     */
    public function getNumberInt()
    {
        return $this->number_int;
    }

    /**
     * Returns the value of field colony
     *
     * @return string
     */
    public function getColony()
    {
        return $this->colony;
    }

    /**
     * Returns the value of field location
     *
     * @return string
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Returns the value of field postal_code
     *
     * @return string
     */
    public function getPostalCode()
    {
        return $this->postal_code;
    }

    /**
     * Returns the value of field email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Returns the value of field iva
     *
     * @return string
     */
    public function getIva()
    {
        return $this->iva;
    }

    /**
     * Returns the value of field date_creation
     *
     * @return string
     */
    public function getDateCreation()
    {
        return $this->date_creation;
    }

    /**
     * Returns the value of field mpid
     *
     * @return integer
     */
    public function getMpid()
    {
        return $this->mpid;
    }

    /**
     * Returns the value of field clid
     *
     * @return integer
     */
    public function getClid()
    {
        return $this->clid;
    }


    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->belongsTo('clid', 'CdClient', 'clid', array('alias' => 'CdClient'));
        $this->belongsTo('mpid', 'CdMunicipios', 'mpid', array('alias' => 'CdMunicipios'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'cd_billing';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return CdBilling[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return CdBilling
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
