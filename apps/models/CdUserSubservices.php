<?php
namespace Modules\Models;
class CdUserSubservices extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    protected $usid;

    /**
     *
     * @var integer
     */
    protected $uid;

    /**
     *
     * @var integer
     */
    protected $subsid;

    /**
     *
     * @var string
     */
    protected $percentage;

    /**
     *
     * @var string
     */
    protected $date_creation;

    /**
     * Method to set the value of field usid
     *
     * @param integer $usid
     * @return $this
     */
    public function setUsid($usid)
    {
        $this->usid = $usid;

        return $this;
    }

    /**
     * Method to set the value of field uid
     *
     * @param integer $uid
     * @return $this
     */
    public function setUid($uid)
    {
        $this->uid = $uid;

        return $this;
    }

    /**
     * Method to set the value of field subsid
     *
     * @param integer $subsid
     * @return $this
     */
    public function setSubsid($subsid)
    {
        $this->subsid = $subsid;

        return $this;
    }

    /**
     * Method to set the value of field percentage
     *
     * @param string $percentage
     * @return $this
     */
    public function setPercentage($percentage)
    {
        $this->percentage = $percentage;

        return $this;
    }

    /**
     * Method to set the value of field date_creation
     *
     * @param string $date_creation
     * @return $this
     */
    public function setDateCreation($date_creation)
    {
        $this->date_creation = $date_creation;

        return $this;
    }

    /**
     * Returns the value of field usid
     *
     * @return integer
     */
    public function getUsid()
    {
        return $this->usid;
    }

    /**
     * Returns the value of field uid
     *
     * @return integer
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * Returns the value of field subsid
     *
     * @return integer
     */
    public function getSubsid()
    {
        return $this->subsid;
    }

    /**
     * Returns the value of field percentage
     *
     * @return string
     */
    public function getPercentage()
    {
        return $this->percentage;
    }

    /**
     * Returns the value of field date_creation
     *
     * @return string
     */
    public function getDateCreation()
    {
        return $this->date_creation;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->belongsTo('subsid', 'CdSubservices', 'subsid', array('alias' => 'CdSubservices'));
        $this->belongsTo('uid', 'CdUser', 'uid', array('alias' => 'CdUser'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'cd_user_subservices';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return CdUserSubservices[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return CdUserSubservices
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
