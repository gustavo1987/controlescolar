<?php
namespace Modules\Models;

class CdWorking extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    protected $workid;

    /**
     *
     * @var string
     */
    protected $status;

    /**
     *
     * @var string
     */
    protected $name;

    /**
     *
     * @var string
     */
    protected $date_creation;

    /**
     *
     * @var string
     */
    protected $date_limit;

    /**
     *
     * @var integer
     */
    protected $uid;

    /**
     *
     * @var integer
     */
    protected $clasid;

    /**
     *
     * @var string
     */
    protected $fileteacher;

    /**
     *
     * @var string
     */
    protected $filestudent;

    /**
     *
     * @var string
     */
    protected $type;

    /**
     *
     * @var string
     */
    protected $calificacion;

    /**
     *
     * @var string
     */
    protected $confid;

    /**
     * Method to set the value of field workid
     *
     * @param integer $workid
     * @return $this
     */
    public function setworkid($workid)
    {
        $this->workid = $workid;

        return $this;
    }

    /**
     * Method to set the value of field status
     *
     * @param string $status
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Method to set the value of field name
     *
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Method to set the value of field date_creation
     *
     * @param string $date_creation
     * @return $this
     */
    public function setDateCreation($date_creation)
    {
        $this->date_creation = $date_creation;

        return $this;
    }

    /**
     * Method to set the value of field date_limit
     *
     * @param string $date_limit
     * @return $this
     */
    public function setDateLimit($date_limit)
    {
        $this->date_limit = $date_limit;

        return $this;
    }

    /**
     * Method to set the value of field uid
     *
     * @param integer $uid
     * @return $this
     */
    public function setUid($uid)
    {
        $this->uid = $uid;

        return $this;
    }

    /**
     * Method to set the value of field clasid
     *
     * @param integer $clasid
     * @return $this
     */
    public function setClasid($clasid)
    {
        $this->clasid = $clasid;

        return $this;
    }

    /**
     * Method to set the value of field fileteacher
     *
     * @param integer $fileteacher
     * @return $this
     */
    public function setFileteacher($fileteacher)
    {
        $this->fileteacher = $fileteacher;

        return $this;
    }

    /**
     * Method to set the value of field filestudent
     *
     * @param integer $filestudent
     * @return $this
     */
    public function setFilestudent($filestudent)
    {
        $this->filestudent = $filestudent;

        return $this;
    }

    /**
     * Method to set the value of field type
     *
     * @param integer $type
     * @return $this
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Method to set the value of field calificacion
     *
     * @param integer $calificacion
     * @return $this
     */
    public function setCalificacion($calificacion)
    {
        $this->calificacion = $calificacion;

        return $this;
    }

    /**
     * Method to set the value of field confid
     *
     * @param integer $confid
     * @return $this
     */
    public function setConfid($confid)
    {
        $this->confid = $confid;

        return $this;
    }

    /**
     * Returns the value of field workid
     *
     * @return integer
     */
    public function getworkid()
    {
        return $this->workid;
    }

    /**
     * Returns the value of field status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Returns the value of field name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Returns the value of field date_creation
     *
     * @return string
     */
    public function getDateCreation()
    {
        return $this->date_creation;
    }

    /**
     * Returns the value of field date_limit
     *
     * @return string
     */
    public function getDateLimit()
    {
        return $this->date_limit;
    }

    /**
     * Returns the value of field uid
     *
     * @return integer
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * Returns the value of field clasid
     *
     * @return integer
     */
    public function getClasid()
    {
        return $this->clasid;
    }

    /**
     * Returns the value of field fileteacher
     *
     * @return integer
     */
    public function getFileteacher()
    {
        return $this->fileteacher;
    }

    /**
     * Returns the value of field filestudent
     *
     * @return integer
     */
    public function getFilestudent()
    {
        return $this->filestudent;
    }

    /**
     * Returns the value of field type
     *
     * @return integer
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Returns the value of field calificacion
     *
     * @return integer
     */
    public function getCalificacion()
    {
        return $this->calificacion;
    }

    /**
     * Returns the value of field confid
     *
     * @return integer
     */
    public function getConfid()
    {
        return $this->confid;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->belongsTo('uid', 'CdUser', 'uid', array('alias' => 'CdUser'));
        $this->belongsTo('clasid', 'CdClase', 'clasid', array('alias' => 'CdClase'));
        /*$this->hasMany('workid', 'CdDirection', 'workid', array('alias' => 'CdDirection'));
        $this->hasMany('workid', 'CdSale', 'workid', array('alias' => 'CdSale'));
        $this->belongsTo('uid', 'CdUser', 'uid', array('alias' => 'CdUser'));*/
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'cd_working';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return CdWorking[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return CdWorking
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
