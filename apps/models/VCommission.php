<?php
namespace Modules\Models;
class VCommission extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var string
     */
    protected $type;

    /**
     *
     * @var string
     */
    protected $date_creation;

    /**
     *
     * @var string
     */
    protected $quantity_ss;

    /**
     *
     * @var string
     */
    protected $commission;

    /**
     *
     * @var string
     */
    protected $name_attend;

    /**
     *
     * @var string
     */
    protected $last_name_attend;

    /**
     *
     * @var string
     */
    protected $second_name_attend;

    /**
     *
     * @var string
     */
    protected $status;

    /**
     *
     * @var integer
     */
    protected $uid;

    /**
     * Method to set the value of field type
     *
     * @param string $type
     * @return $this
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Method to set the value of field date_creation
     *
     * @param string $date_creation
     * @return $this
     */
    public function setDateCreation($date_creation)
    {
        $this->date_creation = $date_creation;

        return $this;
    }

    /**
     * Method to set the value of field quantity_ss
     *
     * @param string $quantity_ss
     * @return $this
     */
    public function setQuantitySs($quantity_ss)
    {
        $this->quantity_ss = $quantity_ss;

        return $this;
    }

    /**
     * Method to set the value of field commission
     *
     * @param string $commission
     * @return $this
     */
    public function setCommission($commission)
    {
        $this->commission = $commission;

        return $this;
    }

    /**
     * Method to set the value of field name_attend
     *
     * @param string $name_attend
     * @return $this
     */
    public function setNameAttend($name_attend)
    {
        $this->name_attend = $name_attend;

        return $this;
    }

    /**
     * Method to set the value of field last_name_attend
     *
     * @param string $last_name_attend
     * @return $this
     */
    public function setLastNameAttend($last_name_attend)
    {
        $this->last_name_attend = $last_name_attend;

        return $this;
    }

    /**
     * Method to set the value of field second_name_attend
     *
     * @param string $second_name_attend
     * @return $this
     */
    public function setSecondNameAttend($second_name_attend)
    {
        $this->second_name_attend = $second_name_attend;

        return $this;
    }

    /**
     * Method to set the value of field status
     *
     * @param string $status
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Method to set the value of field uid
     *
     * @param integer $uid
     * @return $this
     */
    public function setUid($uid)
    {
        $this->uid = $uid;

        return $this;
    }

    /**
     * Returns the value of field type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Returns the value of field date_creation
     *
     * @return string
     */
    public function getDateCreation()
    {
        return $this->date_creation;
    }

    /**
     * Returns the value of field quantity_ss
     *
     * @return string
     */
    public function getQuantitySs()
    {
        return $this->quantity_ss;
    }

    /**
     * Returns the value of field commission
     *
     * @return string
     */
    public function getCommission()
    {
        return $this->commission;
    }

    /**
     * Returns the value of field name_attend
     *
     * @return string
     */
    public function getNameAttend()
    {
        return $this->name_attend;
    }

    /**
     * Returns the value of field last_name_attend
     *
     * @return string
     */
    public function getLastNameAttend()
    {
        return $this->last_name_attend;
    }

    /**
     * Returns the value of field second_name_attend
     *
     * @return string
     */
    public function getSecondNameAttend()
    {
        return $this->second_name_attend;
    }

    /**
     * Returns the value of field status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Returns the value of field uid
     *
     * @return integer
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'v_commission';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return VCommission[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return VCommission
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
