<?php
namespace Modules\Models;

class ChatRooms extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    protected $id;

    /**
     *
     * @var string
     */
    protected $name;

    /**
     *
     * @var string
     */
    protected $numofuser;

    /**
     *
     * @var integer
     */
    protected $file;

    /**
     * Method to set the value of field id
     *
     * @param integer $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Method to set the value of field name
     *
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Method to set the value of field numofuser
     *
     * @param string $numofuser
     * @return $this
     */
    public function setNumofuser($numofuser)
    {
        $this->numofuser = $numofuser;

        return $this;
    }

    /**
     * Method to set the value of field file
     *
     * @param integer $file
     * @return $this
     */
    public function setFile($file)
    {
        $this->file = $file;

        return $this;
    }

    /**
     * Returns the value of field id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Returns the value of field name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Returns the value of field numofuser
     *
     * @return string
     */
    public function getNumofuser()
    {
        return $this->numofuser;
    }

    /**
     * Returns the value of field file
     *
     * @return integer
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'chat_rooms';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return chat_rooms[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return chat_rooms
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}