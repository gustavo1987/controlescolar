<?php
namespace Modules\Models;
class CdRecibo extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    protected $recid;

    /**
     *
     * @var integer
     */
    protected $name;

    /**
     *
     * @var integer
     */
    protected $document;

    /**
     *
     * @var integer
     */
    protected $uid;

    /**
     *
     * @var string
     */
    protected $status;

    /**
     *
     * @var string
     */
    protected $date_creation;

    /**
     *
     * @var integer
     */
    protected $pagid;

    /**
     * Method to set the value of field recid
     *
     * @param integer $recid
     * @return $this
     */
    public function setRecid($recid)
    {
        $this->recid = $recid;

        return $this;
    }

    /**
     * Method to set the value of field name
     *
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Method to set the value of field document
     *
     * @param string $document
     * @return $this
     */
    public function setDocument($document)
    {
        $this->document = $document;

        return $this;
    }

    /**
     * Method to set the value of field uid
     *
     * @param integer $uid
     * @return $this
     */
    public function setUid($uid)
    {
        $this->uid = $uid;

        return $this;
    }

    /**
     * Method to set the value of field status
     *
     * @param string $status
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Method to set the value of field date_creation
     *
     * @param string $date_creation
     * @return $this
     */
    public function setDateCreation($date_creation)
    {
        $this->date_creation = $date_creation;

        return $this;
    }

    /**
     * Method to set the value of field pagid
     *
     * @param integer $pagid
     * @return $this
     */
    public function setPagid($pagid)
    {
        $this->pagid = $pagid;

        return $this;
    }

    /**
     * Returns the value of field recid
     *
     * @return integer
     */
    public function getRecid()
    {
        return $this->recid;
    }

    /**
     * Returns the value of field name
     *
     * @return integer
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Returns the value of field document
     *
     * @return integer
     */
    public function getDocument()
    {
        return $this->document;
    }

    /**
     * Returns the value of field uid
     *
     * @return integer
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * Returns the value of field status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Returns the value of field date_creation
     *
     * @return string
     */
    public function getDateCreation()
    {
        return $this->date_creation;
    }

    /**
     * Returns the value of field pagid
     *
     * @return integer
     */
    public function getPagid()
    {
        return $this->pagid;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->belongsTo('uid', 'CdUser', 'uid', array('alias' => 'CdUser'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'cd_recibo';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return CdRecibo[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return CdRecibo
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}