<?php
namespace Modules\Models;
class CdBrand extends \Phalcon\Mvc\Model
{
    /**
     *
     * @var integer
     */
    protected $brid;

    /**
     *
     * @var string
     */
    protected $name;

    /**
     *
     * @var string
     */
    protected $permalink;

    /**
     *
     * @var string
     */
    protected $date_creation;

    /**
     *
     * @var string
     */
    protected $image;

    /**
     *
     * @var string
     */
    protected $status;

    /**
     *
     * @var integer
     */
    protected $uid;

    /**
     *
     * @var integer
     */
    protected $postgrado;

    /**
     *
     * @var string
     */
    protected $mensualidad;

    /**
     *
     * @var integer
     */
    protected $inscripcion;

    /**
     *
     * @var integer
     */
    protected $reinscripcion;


    /**
     * Method to set the value of field brid
     *
     * @param integer $brid
     * @return $this
     */
    public function setBrid($brid)
    {
        $this->brid = $brid;

        return $this;
    }

    /**
     * Method to set the value of field name
     *
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Method to set the value of field permalink
     *
     * @param string $permalink
     * @return $this
     */
    public function setPermalink($permalink)
    {
        $this->permalink = $permalink;

        return $this;
    }

    /**
     * Method to set the value of field date_creation
     *
     * @param string $date_creation
     * @return $this
     */
    public function setDateCreation($date_creation)
    {
        $this->date_creation = $date_creation;

        return $this;
    }

    /**
     * Method to set the value of field image
     *
     * @param string $image
     * @return $this
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Method to set the value of field status
     *
     * @param string $status
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    public function setUid($uid)
    {
        $this->uid = $uid;

        return $this;
    }

    /**
     * Method to set the value of field brid
     *
     * @param integer $brid
     * @return $this
     */
    public function setPostgrado($postgrado)
    {
        $this->postgrado = $postgrado;

        return $this;
    }

    /**
     * Method to set the value of field mensualidad
     *
     * @param integer $mensualidad
     * @return $this
     */
    public function setMensualidad($mensualidad)
    {
        $this->mensualidad = $mensualidad;

        return $this;
    }

    /**
     * Method to set the value of field inscripcion
     *
     * @param integer $inscripcion
     * @return $this
     */
    public function setInscripcion($inscripcion)
    {
        $this->inscripcion = $inscripcion;

        return $this;
    }

    /**
     * Method to set the value of field reinscripcion
     *
     * @param integer $reinscripcion
     * @return $this
     */
    public function setReinscripcion($reinscripcion)
    {
        $this->reinscripcion = $reinscripcion;

        return $this;
    }

    /**
     * Returns the value of field brid
     *
     * @return integer
     */
    public function getBrid()
    {
        return $this->brid;
    }

    /**
     * Returns the value of field name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Returns the value of field permalink
     *
     * @return string
     */
    public function getPermalink()
    {
        return $this->permalink;
    }

    /**
     * Returns the value of field date_creation
     *
     * @return string
     */
    public function getDateCreation()
    {
        return $this->date_creation;
    }

    /**
     * Returns the value of field image
     *
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Returns the value of field status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }


    /**
     * Returns the value of field uid
     *
     * @return integer
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * Returns the value of field postgrado
     *
     * @return integer
     */
    public function getPostgrado()
    {
        return $this->postgrado;
    }

    /**
     * Returns the value of field mensualidad
     *
     * @return integer
     */
    public function getMensualidad()
    {
        return $this->mensualidad;
    }

    /**
     * Returns the value of field inscripcion
     *
     * @return integer
     */
    public function getInscripcion()
    {
        return $this->inscripcion;
    }

    /**
     * Returns the value of field reinscripcion
     *
     * @return integer
     */
    public function getReinscripcion()
    {
        return $this->reinscripcion;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->hasMany('brid', 'CdProduct', 'brid', array('alias' => 'CdProduct'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'cd_brand';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return CdBrand[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return CdBrand
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
