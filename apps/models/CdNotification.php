<?php
namespace Modules\Models;

class CdNotification extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    protected $notid;

    /**
     *
     * @var string
     */
    protected $notification;

    /**
     *
     * @var string
     */
    protected $status;

    /**
     *
     * @var string
     */
    protected $date_creation;

    /**
     *
     * @var string
     */
    protected $uid;

    /**
     *
     * @var string
     */
    protected $status_view;

    /**
     *
     * @var integer
     */
    protected $date;

    /**
     * Method to set the value of field notid
     *
     * @param integer $notid
     * @return $this
     */
    public function setNotid($notid)
    {
        $this->notid = $notid;

        return $this;
    }

    /**
     * Method to set the value of field notification
     *
     * @param string $notification
     * @return $this
     */
    public function setNotification($notification)
    {
        $this->notification = $notification;

        return $this;
    }

    /**
     * Method to set the value of field status
     *
     * @param string $status
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Method to set the value of field date_creation
     *
     * @param string $date_creation
     * @return $this
     */
    public function setDateCreation($date_creation)
    {
        $this->date_creation = $date_creation;

        return $this;
    }

    /**
     * Method to set the value of field uid
     *
     * @param string $uid
     * @return $this
     */
    public function setUid($uid)
    {
        $this->uid = $uid;

        return $this;
    }

    /**
     * Method to set the value of field status_view
     *
     * @param string $status_view
     * @return $this
     */
    public function setStatusView($status_view)
    {
        $this->status_view = $status_view;

        return $this;
    }

    /**
     * Method to set the value of field date
     *
     * @param integer $date
     * @return $this
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Returns the value of field notid
     *
     * @return integer
     */
    public function getNotid()
    {
        return $this->notid;
    }

    /**
     * Returns the value of field notification
     *
     * @return string
     */
    public function getNotification()
    {
        return $this->notification;
    }

    /**
     * Returns the value of field status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Returns the value of field date_creation
     *
     * @return string
     */
    public function getDateCreation()
    {
        return $this->date_creation;
    }

    /**
     * Returns the value of field uid
     *
     * @return string
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * Returns the value of field status_view
     *
     * @return string
     */
    public function getStatusView()
    {
        return $this->status_view;
    }

    /**
     * Returns the value of field date
     *
     * @return integer
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->belongsTo('uid', 'CdUser', 'uid', array('alias' => 'CdUser'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'cd_notification';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return cd_notification[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return cd_notification
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
