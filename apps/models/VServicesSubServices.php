<?php
namespace Modules\Models;
class VServicesSubServices extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    protected $servid;

    /**
     *
     * @var string
     */
    protected $name_services;

    /**
     *
     * @var string
     */
    protected $permalink_services;

    /**
     *
     * @var integer
     */
    protected $subsid;

    /**
     *
     * @var string
     */
    protected $name_sub_service;

    /**
     *
     * @var string
     */
    protected $permalink_sub_service;

    /**
     *
     * @var string
     */
    protected $price;

    /**
     *
     * @var string
     */
    protected $discount;

    /**
     *
     * @var string
     */
    protected $status;

    /**
     * Method to set the value of field servid
     *
     * @param integer $servid
     * @return $this
     */
    public function setServid($servid)
    {
        $this->servid = $servid;

        return $this;
    }

    /**
     * Method to set the value of field name_services
     *
     * @param string $name_services
     * @return $this
     */
    public function setNameServices($name_services)
    {
        $this->name_services = $name_services;

        return $this;
    }

    /**
     * Method to set the value of field permalink_services
     *
     * @param string $permalink_services
     * @return $this
     */
    public function setPermalinkServices($permalink_services)
    {
        $this->permalink_services = $permalink_services;

        return $this;
    }

    /**
     * Method to set the value of field subsid
     *
     * @param integer $subsid
     * @return $this
     */
    public function setSubsid($subsid)
    {
        $this->subsid = $subsid;

        return $this;
    }

    /**
     * Method to set the value of field name_sub_service
     *
     * @param string $name_sub_service
     * @return $this
     */
    public function setNameSubService($name_sub_service)
    {
        $this->name_sub_service = $name_sub_service;

        return $this;
    }

    /**
     * Method to set the value of field permalink_sub_service
     *
     * @param string $permalink_sub_service
     * @return $this
     */
    public function setPermalinkSubService($permalink_sub_service)
    {
        $this->permalink_sub_service = $permalink_sub_service;

        return $this;
    }

    /**
     * Method to set the value of field price
     *
     * @param string $price
     * @return $this
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Method to set the value of field discount
     *
     * @param string $discount
     * @return $this
     */
    public function setDiscount($discount)
    {
        $this->discount = $discount;

        return $this;
    }

    /**
     * Method to set the value of field status
     *
     * @param string $status
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Returns the value of field servid
     *
     * @return integer
     */
    public function getServid()
    {
        return $this->servid;
    }

    /**
     * Returns the value of field name_services
     *
     * @return string
     */
    public function getNameServices()
    {
        return $this->name_services;
    }

    /**
     * Returns the value of field permalink_services
     *
     * @return string
     */
    public function getPermalinkServices()
    {
        return $this->permalink_services;
    }

    /**
     * Returns the value of field subsid
     *
     * @return integer
     */
    public function getSubsid()
    {
        return $this->subsid;
    }

    /**
     * Returns the value of field name_sub_service
     *
     * @return string
     */
    public function getNameSubService()
    {
        return $this->name_sub_service;
    }

    /**
     * Returns the value of field permalink_sub_service
     *
     * @return string
     */
    public function getPermalinkSubService()
    {
        return $this->permalink_sub_service;
    }

    /**
     * Returns the value of field price
     *
     * @return string
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Returns the value of field discount
     *
     * @return string
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * Returns the value of field status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'v_services_sub_services';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return VServicesSubServices[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return VServicesSubServices
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
