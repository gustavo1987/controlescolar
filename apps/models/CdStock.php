<?php
namespace Modules\Models;

class CdStock extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    protected $stid;

    /**
     *
     * @var integer
     */
    protected $stock;

    /**
     *
     * @var integer
     */
    protected $stock_max;

    /**
     *
     * @var integer
     */
    protected $stock_min;

    /**
     *
     * @var integer
     */
    protected $quantity;

    /**
     *
     * @var string
     */
    protected $date_creation;

    /**
     *
     * @var integer
     */
    protected $pid;

    /**
     * Method to set the value of field stid
     *
     * @param integer $stid
     * @return $this
     */
    public function setStid($stid)
    {
        $this->stid = $stid;

        return $this;
    }

    /**
     * Method to set the value of field stock
     *
     * @param integer $stock
     * @return $this
     */
    public function setStock($stock)
    {
        $this->stock = $stock;

        return $this;
    }

    /**
     * Method to set the value of field stock_max
     *
     * @param integer $stock_max
     * @return $this
     */
    public function setStockMax($stock_max)
    {
        $this->stock_max = $stock_max;

        return $this;
    }

    /**
     * Method to set the value of field stock_min
     *
     * @param integer $stock_min
     * @return $this
     */
    public function setStockMin($stock_min)
    {
        $this->stock_min = $stock_min;

        return $this;
    }

    /**
     * Method to set the value of field quantity
     *
     * @param integer $quantity
     * @return $this
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Method to set the value of field date_creation
     *
     * @param string $date_creation
     * @return $this
     */
    public function setDateCreation($date_creation)
    {
        $this->date_creation = $date_creation;

        return $this;
    }

    /**
     * Method to set the value of field pid
     *
     * @param integer $pid
     * @return $this
     */
    public function setPid($pid)
    {
        $this->pid = $pid;

        return $this;
    }

    /**
     * Returns the value of field stid
     *
     * @return integer
     */
    public function getStid()
    {
        return $this->stid;
    }

    /**
     * Returns the value of field stock
     *
     * @return integer
     */
    public function getStock()
    {
        return $this->stock;
    }

    /**
     * Returns the value of field stock_max
     *
     * @return integer
     */
    public function getStockMax()
    {
        return $this->stock_max;
    }

    /**
     * Returns the value of field stock_min
     *
     * @return integer
     */
    public function getStockMin()
    {
        return $this->stock_min;
    }

    /**
     * Returns the value of field quantity
     *
     * @return integer
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Returns the value of field date_creation
     *
     * @return string
     */
    public function getDateCreation()
    {
        return $this->date_creation;
    }

    /**
     * Returns the value of field pid
     *
     * @return integer
     */
    public function getPid()
    {
        return $this->pid;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->belongsTo('pid', 'CdProduct', 'pid', array('alias' => 'CdProduct'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'cd_stock';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return CdStock[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return CdStock
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }
}
