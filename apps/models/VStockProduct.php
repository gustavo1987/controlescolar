<?php
namespace Modules\Models;
class VStockProduct extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    protected $brid;

    /**
     *
     * @var string
     */
    protected $name_brand;

    /**
     *
     * @var string
     */
    protected $permalink_brand;

    /**
     *
     * @var string
     */
    protected $status_brand;

    /**
     *
     * @var integer
     */
    protected $pid;

    /**
     *
     * @var string
     */
    protected $name_product;

    /**
     *
     * @var string
     */
    protected $permalink_product;

    /**
     *
     * @var string
     */
    protected $purchase_price;

    /**
     *
     * @var string
     */
    protected $sale_price;

    /**
     *
     * @var string
     */
    protected $measure;

    /**
     *
     * @var string
     */
    protected $quantity;

    /**
     *
     * @var string
     */
    protected $percentage;

    /**
     *
     * @var string
     */
    protected $status_product;

    /**
     *
     * @var string
     */
    protected $ticket;

    /**
     *
     * @var integer
     */
    protected $stid;

    /**
     *
     * @var integer
     */
    protected $stock;

    /**
     *
     * @var integer
     */
    protected $stock_max;

    /**
     *
     * @var integer
     */
    protected $stock_min;

    /**
     *
     * @var integer
     */
    protected $quantity_stock;

    /**
     * Method to set the value of field brid
     *
     * @param integer $brid
     * @return $this
     */
    public function setBrid($brid)
    {
        $this->brid = $brid;

        return $this;
    }

    /**
     * Method to set the value of field name_brand
     *
     * @param string $name_brand
     * @return $this
     */
    public function setNameBrand($name_brand)
    {
        $this->name_brand = $name_brand;

        return $this;
    }

    /**
     * Method to set the value of field permalink_brand
     *
     * @param string $permalink_brand
     * @return $this
     */
    public function setPermalinkBrand($permalink_brand)
    {
        $this->permalink_brand = $permalink_brand;

        return $this;
    }

    /**
     * Method to set the value of field status_brand
     *
     * @param string $status_brand
     * @return $this
     */
    public function setStatusBrand($status_brand)
    {
        $this->status_brand = $status_brand;

        return $this;
    }

    /**
     * Method to set the value of field pid
     *
     * @param integer $pid
     * @return $this
     */
    public function setPid($pid)
    {
        $this->pid = $pid;

        return $this;
    }

    /**
     * Method to set the value of field name_product
     *
     * @param string $name_product
     * @return $this
     */
    public function setNameProduct($name_product)
    {
        $this->name_product = $name_product;

        return $this;
    }

    /**
     * Method to set the value of field permalink_product
     *
     * @param string $permalink_product
     * @return $this
     */
    public function setPermalinkProduct($permalink_product)
    {
        $this->permalink_product = $permalink_product;

        return $this;
    }

    /**
     * Method to set the value of field purchase_price
     *
     * @param string $purchase_price
     * @return $this
     */
    public function setPurchasePrice($purchase_price)
    {
        $this->purchase_price = $purchase_price;

        return $this;
    }

    /**
     * Method to set the value of field sale_price
     *
     * @param string $sale_price
     * @return $this
     */
    public function setSalePrice($sale_price)
    {
        $this->sale_price = $sale_price;

        return $this;
    }

    /**
     * Method to set the value of field measure
     *
     * @param string $measure
     * @return $this
     */
    public function setMeasure($measure)
    {
        $this->measure = $measure;

        return $this;
    }

    /**
     * Method to set the value of field quantity
     *
     * @param string $quantity
     * @return $this
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Method to set the value of field percentage
     *
     * @param string $percentage
     * @return $this
     */
    public function setPercentage($percentage)
    {
        $this->percentage = $percentage;

        return $this;
    }

    /**
     * Method to set the value of field status_product
     *
     * @param string $status_product
     * @return $this
     */
    public function setStatusProduct($status_product)
    {
        $this->status_product = $status_product;

        return $this;
    }

    /**
     * Method to set the value of field ticket
     *
     * @param string $ticket
     * @return $this
     */
    public function setTicket($ticket)
    {
        $this->ticket = $ticket;

        return $this;
    }

    /**
     * Method to set the value of field stid
     *
     * @param integer $stid
     * @return $this
     */
    public function setStid($stid)
    {
        $this->stid = $stid;

        return $this;
    }

    /**
     * Method to set the value of field stock
     *
     * @param integer $stock
     * @return $this
     */
    public function setStock($stock)
    {
        $this->stock = $stock;

        return $this;
    }

    /**
     * Method to set the value of field stock_max
     *
     * @param integer $stock_max
     * @return $this
     */
    public function setStockMax($stock_max)
    {
        $this->stock_max = $stock_max;

        return $this;
    }

    /**
     * Method to set the value of field stock_min
     *
     * @param integer $stock_min
     * @return $this
     */
    public function setStockMin($stock_min)
    {
        $this->stock_min = $stock_min;

        return $this;
    }

    /**
     * Method to set the value of field quantity_stock
     *
     * @param integer $quantity_stock
     * @return $this
     */
    public function setQuantityStock($quantity_stock)
    {
        $this->quantity_stock = $quantity_stock;

        return $this;
    }

    /**
     * Returns the value of field brid
     *
     * @return integer
     */
    public function getBrid()
    {
        return $this->brid;
    }

    /**
     * Returns the value of field name_brand
     *
     * @return string
     */
    public function getNameBrand()
    {
        return $this->name_brand;
    }

    /**
     * Returns the value of field permalink_brand
     *
     * @return string
     */
    public function getPermalinkBrand()
    {
        return $this->permalink_brand;
    }

    /**
     * Returns the value of field status_brand
     *
     * @return string
     */
    public function getStatusBrand()
    {
        return $this->status_brand;
    }

    /**
     * Returns the value of field pid
     *
     * @return integer
     */
    public function getPid()
    {
        return $this->pid;
    }

    /**
     * Returns the value of field name_product
     *
     * @return string
     */
    public function getNameProduct()
    {
        return $this->name_product;
    }

    /**
     * Returns the value of field permalink_product
     *
     * @return string
     */
    public function getPermalinkProduct()
    {
        return $this->permalink_product;
    }

    /**
     * Returns the value of field purchase_price
     *
     * @return string
     */
    public function getPurchasePrice()
    {
        return $this->purchase_price;
    }

    /**
     * Returns the value of field sale_price
     *
     * @return string
     */
    public function getSalePrice()
    {
        return $this->sale_price;
    }

    /**
     * Returns the value of field measure
     *
     * @return string
     */
    public function getMeasure()
    {
        return $this->measure;
    }

    /**
     * Returns the value of field quantity
     *
     * @return string
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Returns the value of field percentage
     *
     * @return string
     */
    public function getPercentage()
    {
        return $this->percentage;
    }

    /**
     * Returns the value of field status_product
     *
     * @return string
     */
    public function getStatusProduct()
    {
        return $this->status_product;
    }

    /**
     * Returns the value of field ticket
     *
     * @return string
     */
    public function getTicket()
    {
        return $this->ticket;
    }

    /**
     * Returns the value of field stid
     *
     * @return integer
     */
    public function getStid()
    {
        return $this->stid;
    }

    /**
     * Returns the value of field stock
     *
     * @return integer
     */
    public function getStock()
    {
        return $this->stock;
    }

    /**
     * Returns the value of field stock_max
     *
     * @return integer
     */
    public function getStockMax()
    {
        return $this->stock_max;
    }

    /**
     * Returns the value of field stock_min
     *
     * @return integer
     */
    public function getStockMin()
    {
        return $this->stock_min;
    }

    /**
     * Returns the value of field quantity_stock
     *
     * @return integer
     */
    public function getQuantityStock()
    {
        return $this->quantity_stock;
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'v_stock_product';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return VStockProduct[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return VStockProduct
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
