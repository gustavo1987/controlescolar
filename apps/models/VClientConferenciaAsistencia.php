<?php

namespace Modules\Models;

class VClientConferenciaAsistencia extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    protected $clid;

    /**
     *
     * @var string
     */
    protected $identifier;

    /**
     *
     * @var string
     */
    protected $name;

    /**
     *
     * @var string
     */
    protected $last_name;

    /**
     *
     * @var string
     */
    protected $second_name;

    /**
     *
     * @var string
     */
    protected $sex;

    /**
     *
     * @var string
     */
    protected $birthdate;

    /**
     *
     * @var string
     */
    protected $email;

    /**
     *
     * @var string
     */
    protected $tel;

    /**
     *
     * @var string
     */
    protected $institution;

    /**
     *
     * @var string
     */
    protected $discount;

    /**
     *
     * @var string
     */
    protected $status;

    /**
     *
     * @var integer
     */
    protected $asisid;

    /**
     *
     * @var string
     */
    protected $status_asistencia;
    /**
     *
     * @var integer
     */
    protected $confid;

    /**
     *
     * @var string
     */
    protected $url;

    /**
     *
     * @var string
     */
    protected $name_conferencia;

    /**
     *
     * @var string
     */
    protected $status_conferencia;

    /**
     *
     * @var string
     */
    protected $date_begin;

    /**
     *
     * @var string
     */
    protected $clasid;

    /**
     * Method to set the value of field clid
     *
     * @param integer $clid
     * @return $this
     */
    public function setClid($clid)
    {
        $this->clid = $clid;

        return $this;
    }

    /**
     * Method to set the value of field identifier
     *
     * @param string $identifier
     * @return $this
     */
    public function setIdentifier($identifier)
    {
        $this->identifier = $identifier;

        return $this;
    }

    /**
     * Method to set the value of field name
     *
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Method to set the value of field last_name
     *
     * @param string $last_name
     * @return $this
     */
    public function setLastName($last_name)
    {
        $this->last_name = $last_name;

        return $this;
    }

    /**
     * Method to set the value of field second_name
     *
     * @param string $second_name
     * @return $this
     */
    public function setSecondName($second_name)
    {
        $this->second_name = $second_name;

        return $this;
    }

    /**
     * Method to set the value of field sex
     *
     * @param string $sex
     * @return $this
     */
    public function setSex($sex)
    {
        $this->sex = $sex;

        return $this;
    }

    /**
     * Method to set the value of field birthdate
     *
     * @param string $birthdate
     * @return $this
     */
    public function setBirthdate($birthdate)
    {
        $this->birthdate = $birthdate;

        return $this;
    }

    /**
     * Method to set the value of field email
     *
     * @param string $email
     * @return $this
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Method to set the value of field tel
     *
     * @param string $tel
     * @return $this
     */
    public function setTel($tel)
    {
        $this->tel = $tel;

        return $this;
    }

    /**
     * Method to set the value of field institution
     *
     * @param string $institution
     * @return $this
     */
    public function setInstitution($institution)
    {
        $this->institution = $institution;

        return $this;
    }

    /**
     * Method to set the value of field discount
     *
     * @param string $discount
     * @return $this
     */
    public function setDiscount($discount)
    {
        $this->discount = $discount;

        return $this;
    }

    /**
     * Method to set the value of field status
     *
     * @param string $status
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Method to set the value of field asisid
     *
     * @param integer $asisid
     * @return $this
     */
    public function setAsisid($asisid)
    {
        $this->asisid = $asisid;

        return $this;
    }

    /**
     * Method to set the value of field status_asistencia
     *
     * @param string $status_asistencia
     * @return $this
     */
    public function setStatusAsistencia($status_asistencia)
    {
        $this->status_asistencia = $status_asistencia;

        return $this;
    }

    /**
     * Method to set the value of field confid
     *
     * @param integer $confid
     * @return $this
     */
    public function setConfid($confid)
    {
        $this->confid = $confid;

        return $this;
    }

    /**
     * Method to set the value of field url
     *
     * @param string $url
     * @return $this
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Method to set the value of field name_conferencia
     *
     * @param string $name_conferencia
     * @return $this
     */
    public function setNameConferencia($name_conferencia)
    {
        $this->name_conferencia = $name_conferencia;

        return $this;
    }

    /**
     * Method to set the value of field status_conferencia
     *
     * @param string $status_conferencia
     * @return $this
     */
    public function setStatusConferencia($status_conferencia)
    {
        $this->status_conferencia = $status_conferencia;

        return $this;
    }

    /**
     * Method to set the value of field date_begin
     *
     * @param string $date_begin
     * @return $this
     */
    public function setDateBegin($date_begin)
    {
        $this->date_begin = $date_begin;

        return $this;
    }

    /**
     * Method to set the value of field clasid
     *
     * @param string $clasid
     * @return $this
     */
    public function setClasid($clasid)
    {
        $this->clasid = $clasid;

        return $this;
    }

    /**
     * Returns the value of field clid
     *
     * @return integer
     */
    public function getClid()
    {
        return $this->clid;
    }

    /**
     * Returns the value of field identifier
     *
     * @return string
     */
    public function getIdentifier()
    {
        return $this->identifier;
    }

    /**
     * Returns the value of field name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Returns the value of field last_name
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->last_name;
    }

    /**
     * Returns the value of field second_name
     *
     * @return string
     */
    public function getSecondName()
    {
        return $this->second_name;
    }

    /**
     * Returns the value of field sex
     *
     * @return string
     */
    public function getSex()
    {
        return $this->sex;
    }

    /**
     * Returns the value of field birthdate
     *
     * @return string
     */
    public function getBirthdate()
    {
        return $this->birthdate;
    }

    /**
     * Returns the value of field email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Returns the value of field tel
     *
     * @return string
     */
    public function getTel()
    {
        return $this->tel;
    }

    /**
     * Returns the value of field institution
     *
     * @return string
     */
    public function getInstitution()
    {
        return $this->institution;
    }

    /**
     * Returns the value of field discount
     *
     * @return string
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * Returns the value of field status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Returns the value of field asisid
     *
     * @return integer
     */
    public function getAsisid()
    {
        return $this->asisid;
    }

    /**
     * Returns the value of field status_asistencia
     *
     * @return string
     */
    public function getStatusAsistencia()
    {
        return $this->status_asistencia;
    }

    /**
     * Returns the value of field confid
     *
     * @return integer
     */
    public function getConfid()
    {
        return $this->confid;
    }

    /**
     * Returns the value of field url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Returns the value of field name_conferencia
     *
     * @return string
     */
    public function getNameConferencia()
    {
        return $this->name_conferencia;
    }

    /**
     * Returns the value of field status_conferencia
     *
     * @return string
     */
    public function getStatusConferencia()
    {
        return $this->status_conferencia;
    }

    /**
     * Returns the value of field date_begin
     *
     * @return string
     */
    public function getDateBegin()
    {
        return $this->date_begin;
    }

    /**
     * Returns the value of field clasid
     *
     * @return string
     */
    public function getClasid()
    {
        return $this->clasid;
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'v_client_conferencia_asistencia';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return VClientConferenciaAsistencia[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return VClientConferenciaAsistencia
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
