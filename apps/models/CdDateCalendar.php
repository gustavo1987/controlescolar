<?php
namespace Modules\Models;
class CdDateCalendar extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    protected $dacaid;

    /**
     *
     * @var string
     */
    protected $name;

    /**
     *
     * @var string
     */
    protected $datestart;

    /**
     *
     * @var string
     */
    protected $dateend;

    /**
     *
     * @var string
     */
    protected $status;

    /**
     *
     * @var string
     */
    protected $alldatestart;

    /**
     *
     * @var integer
     */
    protected $clasid;

    /**
     * Method to set the value of field dacaid
     *
     * @param integer $dacaid
     * @return $this
     */
    public function setDacaid($dacaid)
    {
        $this->dacaid = $dacaid;

        return $this;
    }

    /**
     * Method to set the value of field name
     *
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Method to set the value of field datestart
     *
     * @param string $datestart
     * @return $this
     */
    public function setDatestart($datestart)
    {
        $this->datestart = $datestart;

        return $this;
    }

    /**
     * Method to set the value of field dateend
     *
     * @param string $dateend
     * @return $this
     */
    public function setDateend($dateend)
    {
        $this->dateend = $dateend;

        return $this;
    }

    /**
     * Method to set the value of field status
     *
     * @param string $status
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Method to set the value of field allday
     *
     * @param string $allday
     * @return $this
     */
    public function setAllday($allday)
    {
        $this->allday = $allday;

        return $this;
    }

    /**
     * Method to set the value of field clasid
     *
     * @param string $clasid
     * @return $this
     */
    public function setClasid($clasid)
    {
        $this->clasid = $clasid;

        return $this;
    }

    /**
     * Returns the value of field dacaid
     *
     * @return integer
     */
    public function getDacaid()
    {
        return $this->dacaid;
    }

    /**
     * Returns the value of field name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Returns the value of field datestart
     *
     * @return string
     */
    public function getDatestart()
    {
        return $this->datestart;
    }

    /**
     * Returns the value of field dateend
     *
     * @return string
     */
    public function getDateend()
    {
        return $this->dateend;
    }

    /**
     * Returns the value of field status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Returns the value of field allday
     *
     * @return string
     */
    public function getAllday()
    {
        return $this->allday;
    }


    /**
     * Returns the value of field clasid
     *
     * @return integer
     */
    public function getClasid()
    {
        return $this->clasid;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->belongsTo('clasid', 'CdClase', 'clasid', ['alias' => 'CdClase']);
    }
    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'cd_date_calendar';
    }
    
    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return CdDateCalendar[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }
    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return CdDateCalendar
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }
}