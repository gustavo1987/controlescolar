<?php
namespace Modules\Models;
use Phalcon\Mvc\Model\Validator\Email as Email;

class VClientDB extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    protected $clid;

    /**
     *
     * @var string
     */
    protected $identifier;

    /**
     *
     * @var string
     */
    protected $name;

    /**
     *
     * @var string
     */
    protected $last_name;

    /**
     *
     * @var string
     */
    protected $second_name;

    /**
     *
     * @var string
     */
    protected $sex;

    /**
     *
     * @var string
     */
    protected $birthdate;

    /**
     *
     * @var string
     */
    protected $email;

    /**
     *
     * @var string
     */
    protected $tel;

    /**
     *
     * @var string
     */
    protected $institution;

    /**
     *
     * @var string
     */
    protected $discount;

    /**
     *
     * @var string
     */
    protected $status;

    /**
     *
     * @var integer
     */
    protected $did;

    /**
     *
     * @var string
     */
    protected $street;

    /**
     *
     * @var string
     */
    protected $colony;

    /**
     *
     * @var string
     */
    protected $city;

    /**
     *
     * @var string
     */
    protected $references;

    /**
     *
     * @var string
     */
    protected $cp;

    /**
     *
     * @var integer
     */
    protected $mpid;

    /**
     *
     * @var string
     */
    protected $nombre_municipio;

    /**
     *
     * @var integer
     */
    protected $eid;

    /**
     *
     * @var string
     */
    protected $nombre_estado;

    /**
     *
     * @var integer
     */
    protected $bllid;

    /**
     *
     * @var string
     */
    protected $rfc;

    /**
     *
     * @var string
     */
    protected $bussiness_name;

    /**
     *
     * @var string
     */
    protected $street_fiscal;

    /**
     *
     * @var string
     */
    protected $number_ext;

    /**
     *
     * @var string
     */
    protected $number_int;

    /**
     *
     * @var string
     */
    protected $colony_fiscal;

    /**
     *
     * @var string
     */
    protected $location;

    /**
     *
     * @var string
     */
    protected $postal_code;

    /**
     *
     * @var string
     */
    protected $email_fiscal;

    /**
     *
     * @var string
     */
    protected $iva;

    /**
     *
     * @var integer
     */
    protected $mpid_fiscal;

    /**
     *
     * @var string
     */
    protected $nombre_municipio_fiscal;

    /**
     *
     * @var integer
     */
    protected $eid_fiscal;

    /**
     *
     * @var string
     */
    protected $nombre_estado_fiscal;

    /**
     * Method to set the value of field clid
     *
     * @param integer $clid
     * @return $this
     */
    public function setClid($clid)
    {
        $this->clid = $clid;

        return $this;
    }

    /**
     * Method to set the value of field identifier
     *
     * @param string $identifier
     * @return $this
     */
    public function setIdentifier($identifier)
    {
        $this->identifier = $identifier;

        return $this;
    }

    /**
     * Method to set the value of field name
     *
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Method to set the value of field last_name
     *
     * @param string $last_name
     * @return $this
     */
    public function setLastName($last_name)
    {
        $this->last_name = $last_name;

        return $this;
    }

    /**
     * Method to set the value of field second_name
     *
     * @param string $second_name
     * @return $this
     */
    public function setSecondName($second_name)
    {
        $this->second_name = $second_name;

        return $this;
    }

    /**
     * Method to set the value of field sex
     *
     * @param string $sex
     * @return $this
     */
    public function setSex($sex)
    {
        $this->sex = $sex;

        return $this;
    }

    /**
     * Method to set the value of field birthdate
     *
     * @param string $birthdate
     * @return $this
     */
    public function setBirthdate($birthdate)
    {
        $this->birthdate = $birthdate;

        return $this;
    }

    /**
     * Method to set the value of field email
     *
     * @param string $email
     * @return $this
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Method to set the value of field tel
     *
     * @param string $tel
     * @return $this
     */
    public function setTel($tel)
    {
        $this->tel = $tel;

        return $this;
    }

    /**
     * Method to set the value of field institution
     *
     * @param string $institution
     * @return $this
     */
    public function setInstitution($institution)
    {
        $this->institution = $institution;

        return $this;
    }

    /**
     * Method to set the value of field discount
     *
     * @param string $discount
     * @return $this
     */
    public function setDiscount($discount)
    {
        $this->discount = $discount;

        return $this;
    }

    /**
     * Method to set the value of field status
     *
     * @param string $status
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Method to set the value of field did
     *
     * @param integer $did
     * @return $this
     */
    public function setDid($did)
    {
        $this->did = $did;

        return $this;
    }

    /**
     * Method to set the value of field street
     *
     * @param string $street
     * @return $this
     */
    public function setStreet($street)
    {
        $this->street = $street;

        return $this;
    }

    /**
     * Method to set the value of field colony
     *
     * @param string $colony
     * @return $this
     */
    public function setColony($colony)
    {
        $this->colony = $colony;

        return $this;
    }

    /**
     * Method to set the value of field city
     *
     * @param string $city
     * @return $this
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Method to set the value of field references
     *
     * @param string $references
     * @return $this
     */
    public function setReferences($references)
    {
        $this->references = $references;

        return $this;
    }

    /**
     * Method to set the value of field cp
     *
     * @param string $cp
     * @return $this
     */
    public function setCp($cp)
    {
        $this->cp = $cp;

        return $this;
    }

    /**
     * Method to set the value of field mpid
     *
     * @param integer $mpid
     * @return $this
     */
    public function setMpid($mpid)
    {
        $this->mpid = $mpid;

        return $this;
    }

    /**
     * Method to set the value of field nombre_municipio
     *
     * @param string $nombre_municipio
     * @return $this
     */
    public function setNombreMunicipio($nombre_municipio)
    {
        $this->nombre_municipio = $nombre_municipio;

        return $this;
    }

    /**
     * Method to set the value of field eid
     *
     * @param integer $eid
     * @return $this
     */
    public function setEid($eid)
    {
        $this->eid = $eid;

        return $this;
    }

    /**
     * Method to set the value of field nombre_estado
     *
     * @param string $nombre_estado
     * @return $this
     */
    public function setNombreEstado($nombre_estado)
    {
        $this->nombre_estado = $nombre_estado;

        return $this;
    }

    /**
     * Method to set the value of field bllid
     *
     * @param integer $bllid
     * @return $this
     */
    public function setBllid($bllid)
    {
        $this->bllid = $bllid;

        return $this;
    }

    /**
     * Method to set the value of field rfc
     *
     * @param string $rfc
     * @return $this
     */
    public function setRfc($rfc)
    {
        $this->rfc = $rfc;

        return $this;
    }

    /**
     * Method to set the value of field bussiness_name
     *
     * @param string $bussiness_name
     * @return $this
     */
    public function setBussinessName($bussiness_name)
    {
        $this->bussiness_name = $bussiness_name;

        return $this;
    }

    /**
     * Method to set the value of field street_fiscal
     *
     * @param string $street_fiscal
     * @return $this
     */
    public function setStreetFiscal($street_fiscal)
    {
        $this->street_fiscal = $street_fiscal;

        return $this;
    }

    /**
     * Method to set the value of field number_ext
     *
     * @param string $number_ext
     * @return $this
     */
    public function setNumberExt($number_ext)
    {
        $this->number_ext = $number_ext;

        return $this;
    }

    /**
     * Method to set the value of field number_int
     *
     * @param string $number_int
     * @return $this
     */
    public function setNumberInt($number_int)
    {
        $this->number_int = $number_int;

        return $this;
    }

    /**
     * Method to set the value of field colony_fiscal
     *
     * @param string $colony_fiscal
     * @return $this
     */
    public function setColonyFiscal($colony_fiscal)
    {
        $this->colony_fiscal = $colony_fiscal;

        return $this;
    }

    /**
     * Method to set the value of field location
     *
     * @param string $location
     * @return $this
     */
    public function setLocation($location)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * Method to set the value of field postal_code
     *
     * @param string $postal_code
     * @return $this
     */
    public function setPostalCode($postal_code)
    {
        $this->postal_code = $postal_code;

        return $this;
    }

    /**
     * Method to set the value of field email_fiscal
     *
     * @param string $email_fiscal
     * @return $this
     */
    public function setEmailFiscal($email_fiscal)
    {
        $this->email_fiscal = $email_fiscal;

        return $this;
    }

    /**
     * Method to set the value of field iva
     *
     * @param string $iva
     * @return $this
     */
    public function setIva($iva)
    {
        $this->iva = $iva;

        return $this;
    }

    /**
     * Method to set the value of field mpid_fiscal
     *
     * @param integer $mpid_fiscal
     * @return $this
     */
    public function setMpidFiscal($mpid_fiscal)
    {
        $this->mpid_fiscal = $mpid_fiscal;

        return $this;
    }

    /**
     * Method to set the value of field nombre_municipio_fiscal
     *
     * @param string $nombre_municipio_fiscal
     * @return $this
     */
    public function setNombreMunicipioFiscal($nombre_municipio_fiscal)
    {
        $this->nombre_municipio_fiscal = $nombre_municipio_fiscal;

        return $this;
    }

    /**
     * Method to set the value of field eid_fiscal
     *
     * @param integer $eid_fiscal
     * @return $this
     */
    public function setEidFiscal($eid_fiscal)
    {
        $this->eid_fiscal = $eid_fiscal;

        return $this;
    }

    /**
     * Method to set the value of field nombre_estado_fiscal
     *
     * @param string $nombre_estado_fiscal
     * @return $this
     */
    public function setNombreEstadoFiscal($nombre_estado_fiscal)
    {
        $this->nombre_estado_fiscal = $nombre_estado_fiscal;

        return $this;
    }

    /**
     * Returns the value of field clid
     *
     * @return integer
     */
    public function getClid()
    {
        return $this->clid;
    }

    /**
     * Returns the value of field identifier
     *
     * @return string
     */
    public function getIdentifier()
    {
        return $this->identifier;
    }

    /**
     * Returns the value of field name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Returns the value of field last_name
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->last_name;
    }

    /**
     * Returns the value of field second_name
     *
     * @return string
     */
    public function getSecondName()
    {
        return $this->second_name;
    }

    /**
     * Returns the value of field sex
     *
     * @return string
     */
    public function getSex()
    {
        return $this->sex;
    }

    /**
     * Returns the value of field birthdate
     *
     * @return string
     */
    public function getBirthdate()
    {
        return $this->birthdate;
    }

    /**
     * Returns the value of field email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Returns the value of field tel
     *
     * @return string
     */
    public function getTel()
    {
        return $this->tel;
    }

    /**
     * Returns the value of field institution
     *
     * @return string
     */
    public function getInstitution()
    {
        return $this->institution;
    }

    /**
     * Returns the value of field discount
     *
     * @return string
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * Returns the value of field status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Returns the value of field did
     *
     * @return integer
     */
    public function getDid()
    {
        return $this->did;
    }

    /**
     * Returns the value of field street
     *
     * @return string
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * Returns the value of field colony
     *
     * @return string
     */
    public function getColony()
    {
        return $this->colony;
    }

    /**
     * Returns the value of field city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Returns the value of field references
     *
     * @return string
     */
    public function getReferences()
    {
        return $this->references;
    }

    /**
     * Returns the value of field cp
     *
     * @return string
     */
    public function getCp()
    {
        return $this->cp;
    }

    /**
     * Returns the value of field mpid
     *
     * @return integer
     */
    public function getMpid()
    {
        return $this->mpid;
    }

    /**
     * Returns the value of field nombre_municipio
     *
     * @return string
     */
    public function getNombreMunicipio()
    {
        return $this->nombre_municipio;
    }

    /**
     * Returns the value of field eid
     *
     * @return integer
     */
    public function getEid()
    {
        return $this->eid;
    }

    /**
     * Returns the value of field nombre_estado
     *
     * @return string
     */
    public function getNombreEstado()
    {
        return $this->nombre_estado;
    }

    /**
     * Returns the value of field bllid
     *
     * @return integer
     */
    public function getBllid()
    {
        return $this->bllid;
    }

    /**
     * Returns the value of field rfc
     *
     * @return string
     */
    public function getRfc()
    {
        return $this->rfc;
    }

    /**
     * Returns the value of field bussiness_name
     *
     * @return string
     */
    public function getBussinessName()
    {
        return $this->bussiness_name;
    }

    /**
     * Returns the value of field street_fiscal
     *
     * @return string
     */
    public function getStreetFiscal()
    {
        return $this->street_fiscal;
    }

    /**
     * Returns the value of field number_ext
     *
     * @return string
     */
    public function getNumberExt()
    {
        return $this->number_ext;
    }

    /**
     * Returns the value of field number_int
     *
     * @return string
     */
    public function getNumberInt()
    {
        return $this->number_int;
    }

    /**
     * Returns the value of field colony_fiscal
     *
     * @return string
     */
    public function getColonyFiscal()
    {
        return $this->colony_fiscal;
    }

    /**
     * Returns the value of field location
     *
     * @return string
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Returns the value of field postal_code
     *
     * @return string
     */
    public function getPostalCode()
    {
        return $this->postal_code;
    }

    /**
     * Returns the value of field email_fiscal
     *
     * @return string
     */
    public function getEmailFiscal()
    {
        return $this->email_fiscal;
    }

    /**
     * Returns the value of field iva
     *
     * @return string
     */
    public function getIva()
    {
        return $this->iva;
    }

    /**
     * Returns the value of field mpid_fiscal
     *
     * @return integer
     */
    public function getMpidFiscal()
    {
        return $this->mpid_fiscal;
    }

    /**
     * Returns the value of field nombre_municipio_fiscal
     *
     * @return string
     */
    public function getNombreMunicipioFiscal()
    {
        return $this->nombre_municipio_fiscal;
    }

    /**
     * Returns the value of field eid_fiscal
     *
     * @return integer
     */
    public function getEidFiscal()
    {
        return $this->eid_fiscal;
    }

    /**
     * Returns the value of field nombre_estado_fiscal
     *
     * @return string
     */
    public function getNombreEstadoFiscal()
    {
        return $this->nombre_estado_fiscal;
    }

    /**
     * Validations and business logic
     *
     * @return boolean
     */
    public function validation()
    {
        $this->validate(
            new Email(
                array(
                    'field'    => 'email',
                    'required' => true,
                )
            )
        );

        if ($this->validationHasFailed() == true) {
            return false;
        }

        return true;
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'v_client_d_b';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return VClientDB[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return VClientDB
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
