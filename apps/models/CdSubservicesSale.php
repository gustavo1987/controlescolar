<?php
namespace Modules\Models;
class CdSubservicesSale extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    protected $subsalid;

    /**
     *
     * @var integer
     */
    protected $salid;

    /**
     *
     * @var integer
     */
    protected $subsid;

    /**
     *
     * @var integer
     */
    protected $pid;

    /**
     *
     * @var string
     */
    protected $quantity;

    /**
     *
     * @var string
     */
    protected $date_creation;

    /**
     *
     * @var string
     */
    protected $commission;

    /**
     *
     * @var string
     */
    protected $type;

    /**
     *
     * @var string
     */
    protected $payout;

    /**
     *
     * @var integer
     */
    protected $uid;

    /**
     *
     * @var string
     */
    protected $price_general;

    /**
     *
     * @var string
     */
    protected $status;

    /**
     *
     * @var integer
     */
    protected $discount;

    /**
     * Method to set the value of field subsalid
     *
     * @param integer $subsalid
     * @return $this
     */
    public function setSubsalid($subsalid)
    {
        $this->subsalid = $subsalid;

        return $this;
    }

    /**
     * Method to set the value of field salid
     *
     * @param integer $salid
     * @return $this
     */
    public function setSalid($salid)
    {
        $this->salid = $salid;

        return $this;
    }

    /**
     * Method to set the value of field subsid
     *
     * @param integer $subsid
     * @return $this
     */
    public function setSubsid($subsid)
    {
        $this->subsid = $subsid;

        return $this;
    }

    /**
     * Method to set the value of field pid
     *
     * @param integer $pid
     * @return $this
     */
    public function setPid($pid)
    {
        $this->pid = $pid;

        return $this;
    }

    /**
     * Method to set the value of field quantity
     *
     * @param string $quantity
     * @return $this
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Method to set the value of field date_creation
     *
     * @param string $date_creation
     * @return $this
     */
    public function setDateCreation($date_creation)
    {
        $this->date_creation = $date_creation;

        return $this;
    }

    /**
     * Method to set the value of field commission
     *
     * @param string $commission
     * @return $this
     */
    public function setCommission($commission)
    {
        $this->commission = $commission;

        return $this;
    }

    /**
     * Method to set the value of field type
     *
     * @param string $type
     * @return $this
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Method to set the value of field payout
     *
     * @param string $payout
     * @return $this
     */
    public function setPayout($payout)
    {
        $this->payout = $payout;

        return $this;
    }

    /**
     * Method to set the value of field uid
     *
     * @param integer $uid
     * @return $this
     */
    public function setUid($uid)
    {
        $this->uid = $uid;

        return $this;
    }

    /**
     * Method to set the value of field price_general
     *
     * @param string $price_general
     * @return $this
     */
    public function setPriceGeneral($price_general)
    {
        $this->price_general = $price_general;

        return $this;
    }

    /**
     * Method to set the value of field status
     *
     * @param string $status
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Method to set the value of field discount
     *
     * @param integer $discount
     * @return $this
     */
    public function setDiscount($discount)
    {
        $this->discount = $discount;

        return $this;
    }

    /**
     * Returns the value of field subsalid
     *
     * @return integer
     */
    public function getSubsalid()
    {
        return $this->subsalid;
    }

    /**
     * Returns the value of field salid
     *
     * @return integer
     */
    public function getSalid()
    {
        return $this->salid;
    }

    /**
     * Returns the value of field subsid
     *
     * @return integer
     */
    public function getSubsid()
    {
        return $this->subsid;
    }

    /**
     * Returns the value of field pid
     *
     * @return integer
     */
    public function getPid()
    {
        return $this->pid;
    }

    /**
     * Returns the value of field quantity
     *
     * @return string
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Returns the value of field date_creation
     *
     * @return string
     */
    public function getDateCreation()
    {
        return $this->date_creation;
    }

    /**
     * Returns the value of field commission
     *
     * @return string
     */
    public function getCommission()
    {
        return $this->commission;
    }

    /**
     * Returns the value of field type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Returns the value of field payout
     *
     * @return string
     */
    public function getPayout()
    {
        return $this->payout;
    }

    /**
     * Returns the value of field uid
     *
     * @return integer
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * Returns the value of field price_general
     *
     * @return string
     */
    public function getPriceGeneral()
    {
        return $this->price_general;
    }

    /**
     * Returns the value of field status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Returns the value of field discount
     *
     * @return integer
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->belongsTo('pid', 'CdProduct', 'pid', array('alias' => 'CdProduct'));
        $this->belongsTo('subsid', 'CdSubservices', 'subsid', array('alias' => 'CdSubservices'));
        $this->belongsTo('uid', 'CdUser', 'uid', array('alias' => 'CdUser'));
        $this->belongsTo('salid', 'CdSale', 'salid', array('alias' => 'CdSale'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'cd_subservices_sale';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return CdSubservicesSale[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return CdSubservicesSale
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
