<?php
namespace Modules\Models;
class CdBoxCut extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    protected $boxcid;

    /**
     *
     * @var string
     */
    protected $total;

    /**
     *
     * @var string
     */
    protected $date_creation;

    /**
     *
     * @var integer
     */
    protected $uid;

    /**
     *
     * @var string
     */
    protected $status;

    /**
     * Method to set the value of field boxcid
     *
     * @param integer $boxcid
     * @return $this
     */
    public function setBoxcid($boxcid)
    {
        $this->boxcid = $boxcid;

        return $this;
    }

    /**
     * Method to set the value of field total
     *
     * @param string $total
     * @return $this
     */
    public function setTotal($total)
    {
        $this->total = $total;

        return $this;
    }

    /**
     * Method to set the value of field date_time
     *
     * @param string $date_time
     * @return $this
     */
    public function setDateCreation($date_creation)
    {
        $this->date_creation = $date_creation;

        return $this;
    }

    /**
     * Method to set the value of field uid
     *
     * @param integer $uid
     * @return $this
     */
    public function setUid($uid)
    {
        $this->uid = $uid;

        return $this;
    }

    /**
     * Method to set the value of field status
     *
     * @param string $status
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Returns the value of field boxcid
     *
     * @return integer
     */
    public function getBoxcid()
    {
        return $this->boxcid;
    }

    /**
     * Returns the value of field total
     *
     * @return string
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * Returns the value of field date_time
     *
     * @return string
     */
    public function getDateCreation()
    {
        return $this->date_creation;
    }

    /**
     * Returns the value of field uid
     *
     * @return integer
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * Returns the value of field status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->belongsTo('uid', 'CdUser', 'uid', array('alias' => 'CdUser'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'cd_box_cut';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return CdBoxCut[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return CdBoxCut
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
