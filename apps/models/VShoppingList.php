<?php
namespace Modules\Models;
class VShoppingList extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    protected $salid;

    /**
     *
     * @var integer
     */
    protected $subsalid;

    /**
     *
     * @var string
     */
    protected $quantity_ss;

    /**
     *
     * @var string
     */
    protected $date_creation;

    /**
     *
     * @var string
     */
    protected $commission;

    /**
     *
     * @var string
     */
    protected $type;

    /**
     *
     * @var string
     */
    protected $typesalid;

    /**
     *
     * @var string
     */
    protected $statussalid;

    /**
     *
     * @var string
     */
    protected $status;

    /**
     *
     * @var string
     */
    protected $payout;

    /**
     *
     * @var integer
     */
    protected $uid_ss;

    /**
     *
     * @var string
     */
    protected $price_general;

    /**
     *
     * @var integer
     */
    protected $uid_attend;

    /**
     *
     * @var string
     */
    protected $name_attend;

    /**
     *
     * @var string
     */
    protected $last_name_attend;

    /**
     *
     * @var string
     */
    protected $second_name_attend;

    /**
     *
     * @var integer
     */
    protected $subsid;

    /**
     *
     * @var string
     */
    protected $name_service;

    /**
     *
     * @var string
     */
    protected $name_subservice;

    /**
     *
     * @var string
     */
    protected $price;

    /**
     *
     * @var string
     */
    protected $discount;

    /**
     *
     * @var integer
     */
    protected $pid;

    /**
     *
     * @var integer
     */
    protected $brid;

    /**
     *
     * @var string
     */
    protected $name_brand;

    /**
     *
     * @var string
     */
    protected $postgrado_bra;

    /**
     *
     * @var string
     */
    protected $name_product;

    /**
     *
     * @var string
     */
    protected $sale_price;

    /**
     *
     * @var string
     */
    protected $percentage;

    /**
     *
     * @var string
     */
    protected $ticket;

    /**
     *
     * @var string
     */
    protected $namegene;

    /**
     *
     * @var integer
     */
    protected $genid;

    /**
     * Method to set the value of field salid
     *
     * @param integer $salid
     * @return $this
     */
    public function setSalid($salid)
    {
        $this->salid = $salid;

        return $this;
    }

    /**
     * Method to set the value of field subsalid
     *
     * @param integer $subsalid
     * @return $this
     */
    public function setSubsalid($subsalid)
    {
        $this->subsalid = $subsalid;

        return $this;
    }

    /**
     * Method to set the value of field quantity_ss
     *
     * @param string $quantity_ss
     * @return $this
     */
    public function setQuantitySs($quantity_ss)
    {
        $this->quantity_ss = $quantity_ss;

        return $this;
    }

    /**
     * Method to set the value of field date_creation
     *
     * @param string $date_creation
     * @return $this
     */
    public function setDateCreation($date_creation)
    {
        $this->date_creation = $date_creation;

        return $this;
    }

    /**
     * Method to set the value of field commission
     *
     * @param string $commission
     * @return $this
     */
    public function setCommission($commission)
    {
        $this->commission = $commission;

        return $this;
    }

    /**
     * Method to set the value of field type
     *
     * @param string $type
     * @return $this
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Method to set the value of field typesalid
     *
     * @param string $typesalid
     * @return $this
     */
    public function setTypesalid($typesalid)
    {
        $this->typesalid = $typesalid;

        return $this;
    }

    /**
     * Method to set the value of field statussalid
     *
     * @param string $statussalid
     * @return $this
     */
    public function setStatussalid($statussalid)
    {
        $this->statussalid = $statussalid;

        return $this;
    }

    /**
     * Method to set the value of field status
     *
     * @param string $status
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Method to set the value of field payout
     *
     * @param string $payout
     * @return $this
     */
    public function setPayout($payout)
    {
        $this->payout = $payout;

        return $this;
    }

    /**
     * Method to set the value of field uid_ss
     *
     * @param integer $uid_ss
     * @return $this
     */
    public function setUidSs($uid_ss)
    {
        $this->uid_ss = $uid_ss;

        return $this;
    }

    /**
     * Method to set the value of field price_general
     *
     * @param string $price_general
     * @return $this
     */
    public function setPriceGeneral($price_general)
    {
        $this->price_general = $price_general;

        return $this;
    }

    /**
     * Method to set the value of field uid_attend
     *
     * @param integer $uid_attend
     * @return $this
     */
    public function setUidAttend($uid_attend)
    {
        $this->uid_attend = $uid_attend;

        return $this;
    }

    /**
     * Method to set the value of field name_attend
     *
     * @param string $name_attend
     * @return $this
     */
    public function setNameAttend($name_attend)
    {
        $this->name_attend = $name_attend;

        return $this;
    }

    /**
     * Method to set the value of field last_name_attend
     *
     * @param string $last_name_attend
     * @return $this
     */
    public function setLastNameAttend($last_name_attend)
    {
        $this->last_name_attend = $last_name_attend;

        return $this;
    }

    /**
     * Method to set the value of field second_name_attend
     *
     * @param string $second_name_attend
     * @return $this
     */
    public function setSecondNameAttend($second_name_attend)
    {
        $this->second_name_attend = $second_name_attend;

        return $this;
    }

    /**
     * Method to set the value of field subsid
     *
     * @param integer $subsid
     * @return $this
     */
    public function setSubsid($subsid)
    {
        $this->subsid = $subsid;

        return $this;
    }

    /**
     * Method to set the value of field name_service
     *
     * @param string $name_service
     * @return $this
     */
    public function setNameService($name_service)
    {
        $this->name_service = $name_service;

        return $this;
    }

    /**
     * Method to set the value of field name_subservice
     *
     * @param string $name_subservice
     * @return $this
     */
    public function setNameSubservice($name_subservice)
    {
        $this->name_subservice = $name_subservice;

        return $this;
    }

    /**
     * Method to set the value of field price
     *
     * @param string $price
     * @return $this
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Method to set the value of field discount
     *
     * @param string $discount
     * @return $this
     */
    public function setDiscount($discount)
    {
        $this->discount = $discount;

        return $this;
    }

    /**
     * Method to set the value of field pid
     *
     * @param integer $pid
     * @return $this
     */
    public function setPid($pid)
    {
        $this->pid = $pid;

        return $this;
    }

    /**
     * Method to set the value of field brid
     *
     * @param integer $brid
     * @return $this
     */
    public function setBrid($brid)
    {
        $this->brid = $brid;

        return $this;
    }

    /**
     * Method to set the value of field name_brand
     *
     * @param string $name_brand
     * @return $this
     */
    public function setNameBrand($name_brand)
    {
        $this->name_brand = $name_brand;

        return $this;
    }

    /**
     * Method to set the value of field postgrado_brand
     *
     * @param string $postgrado_brand
     * @return $this
     */
    public function setPostgradoBrand($postgrado_brand)
    {
        $this->postgrado_brand = $postgrado_brand;

        return $this;
    }

    /**
     * Method to set the value of field name_product
     *
     * @param string $name_product
     * @return $this
     */
    public function setNameProduct($name_product)
    {
        $this->name_product = $name_product;

        return $this;
    }

    /**
     * Method to set the value of field sale_price
     *
     * @param string $sale_price
     * @return $this
     */
    public function setSalePrice($sale_price)
    {
        $this->sale_price = $sale_price;

        return $this;
    }

    /**
     * Method to set the value of field percentage
     *
     * @param string $percentage
     * @return $this
     */
    public function setPercentage($percentage)
    {
        $this->percentage = $percentage;

        return $this;
    }

    /**
     * Method to set the value of field ticket
     *
     * @param string $ticket
     * @return $this
     */
    public function setTicket($ticket)
    {
        $this->ticket = $ticket;

        return $this;
    }

    /**
     * Method to set the value of field namegene
     *
     * @param string $namegene
     * @return $this
     */
    public function setNamegene($namegene)
    {
        $this->namegene = $namegene;

        return $this;
    }

    /**
     * Method to set the value of field genid
     *
     * @param integer $genid
     * @return $this
     */
    public function setGenid($genid)
    {
        $this->genid = $genid;

        return $this;
    }

    /**
     * Returns the value of field salid
     *
     * @return integer
     */
    public function getSalid()
    {
        return $this->salid;
    }

    /**
     * Returns the value of field subsalid
     *
     * @return integer
     */
    public function getSubsalid()
    {
        return $this->subsalid;
    }

    /**
     * Returns the value of field quantity_ss
     *
     * @return string
     */
    public function getQuantitySs()
    {
        return $this->quantity_ss;
    }

    /**
     * Returns the value of field date_creation
     *
     * @return string
     */
    public function getDateCreation()
    {
        return $this->date_creation;
    }

    /**
     * Returns the value of field commission
     *
     * @return string
     */
    public function getCommission()
    {
        return $this->commission;
    }

    /**
     * Returns the value of field type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Returns the value of field typesalid
     *
     * @return string
     */
    public function getTypesalid()
    {
        return $this->typesalid;
    }

    /**
     * Returns the value of field statussalid
     *
     * @return string
     */
    public function getStatussalid()
    {
        return $this->statussalid;
    }

    /**
     * Returns the value of field status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Returns the value of field payout
     *
     * @return string
     */
    public function getPayout()
    {
        return $this->payout;
    }

    /**
     * Returns the value of field uid_ss
     *
     * @return integer
     */
    public function getUidSs()
    {
        return $this->uid_ss;
    }

    /**
     * Returns the value of field price_general
     *
     * @return string
     */
    public function getPriceGeneral()
    {
        return $this->price_general;
    }

    /**
     * Returns the value of field uid_attend
     *
     * @return integer
     */
    public function getUidAttend()
    {
        return $this->uid_attend;
    }

    /**
     * Returns the value of field name_attend
     *
     * @return string
     */
    public function getNameAttend()
    {
        return $this->name_attend;
    }

    /**
     * Returns the value of field last_name_attend
     *
     * @return string
     */
    public function getLastNameAttend()
    {
        return $this->last_name_attend;
    }

    /**
     * Returns the value of field second_name_attend
     *
     * @return string
     */
    public function getSecondNameAttend()
    {
        return $this->second_name_attend;
    }

    /**
     * Returns the value of field subsid
     *
     * @return integer
     */
    public function getSubsid()
    {
        return $this->subsid;
    }

    /**
     * Returns the value of field name_service
     *
     * @return string
     */
    public function getNameService()
    {
        return $this->name_service;
    }

    /**
     * Returns the value of field name_subservice
     *
     * @return string
     */
    public function getNameSubservice()
    {
        return $this->name_subservice;
    }

    /**
     * Returns the value of field price
     *
     * @return string
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Returns the value of field discount
     *
     * @return string
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * Returns the value of field pid
     *
     * @return integer
     */
    public function getPid()
    {
        return $this->pid;
    }

    /**
     * Returns the value of field brid
     *
     * @return integer
     */
    public function getBrid()
    {
        return $this->brid;
    }

    /**
     * Returns the value of field name_brand
     *
     * @return string
     */
    public function getNameBrand()
    {
        return $this->name_brand;
    }

    /**
     * Returns the value of field postgrado_bra
     *
     * @return string
     */
    public function getPostBrand()
    {
        return $this->postgrado_bra;
    }

    /**
     * Returns the value of field name_product
     *
     * @return string
     */
    public function getNameProduct()
    {
        return $this->name_product;
    }

    /**
     * Returns the value of field sale_price
     *
     * @return string
     */
    public function getSalePrice()
    {
        return $this->sale_price;
    }

    /**
     * Returns the value of field percentage
     *
     * @return string
     */
    public function getPercentage()
    {
        return $this->percentage;
    }

    /**
     * Returns the value of field ticket
     *
     * @return string
     */
    public function getTicket()
    {
        return $this->ticket;
    }

    /**
     * Returns the value of field namegene
     *
     * @return string
     */
    public function getNamegene()
    {
        return $this->namegene;
    }

    /**
     * Returns the value of field genid
     *
     * @return integer
     */
    public function getGenid()
    {
        return $this->genid;
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'v_shopping_list';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return VShoppingList[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return VShoppingList
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
