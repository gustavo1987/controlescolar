<?php
namespace Modules\Models;
class CdSaleClient extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    protected $saclid;

    /**
     *
     * @var integer
     */
    protected $clid;

    /**
     *
     * @var integer
     */
    protected $salid;

    /**
     *
     * @var string
     */
    protected $status;

    /**
     *
     * @var string
     */
    protected $date_creation;

    /**
     *
     * @var integer
     */
    protected $calificacion;

    /**
     *
     * @var string
     */
    protected $observacion;

    /**
     *
     * @var string
     */
    protected $asistencia;

    /**
     * Method to set the value of field saclid
     *
     * @param integer $saclid
     * @return $this
     */
    public function setSaclid($saclid)
    {
        $this->saclid = $saclid;

        return $this;
    }

    /**
     * Method to set the value of field clid
     *
     * @param integer $clid
     * @return $this
     */
    public function setClid($clid)
    {
        $this->clid = $clid;

        return $this;
    }

    /**
     * Method to set the value of field salid
     *
     * @param integer $salid
     * @return $this
     */
    public function setSalid($salid)
    {
        $this->salid = $salid;

        return $this;
    }

    /**
     * Method to set the value of field status
     *
     * @param string $status
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Method to set the value of field date_creation
     *
     * @param string $date_creation
     * @return $this
     */
    public function setDateCreation($date_creation)
    {
        $this->date_creation = $date_creation;

        return $this;
    }

    /**
     * Method to set the value of field calificacion
     *
     * @param integer $calificacion
     * @return $this
     */
    public function setCalificacion($calificacion)
    {
        $this->calificacion = $calificacion;

        return $this;
    }

    /**
     * Method to set the value of field observacion
     *
     * @param string $observacion
     * @return $this
     */
    public function setObservacion($observacion)
    {
        $this->observacion = $observacion;

        return $this;
    }

    /**
     * Method to set the value of field asistencia
     *
     * @param integer $asistencia
     * @return $this
     */
    public function setAsistencia($asistencia)
    {
        $this->asistencia = $asistencia;

        return $this;
    }

    /**
     * Returns the value of field saclid
     *
     * @return integer
     */
    public function getSaclid()
    {
        return $this->saclid;
    }

    /**
     * Returns the value of field clid
     *
     * @return integer
     */
    public function getClid()
    {
        return $this->clid;
    }

    /**
     * Returns the value of field salid
     *
     * @return integer
     */
    public function getSalid()
    {
        return $this->salid;
    }

    /**
     * Returns the value of field status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Returns the value of field date_creation
     *
     * @return string
     */
    public function getDateCreation()
    {
        return $this->date_creation;
    }

    /**
     * Returns the value of field calificacion
     *
     * @return integer
     */
    public function getCalificacion()
    {
        return $this->calificacion;
    }

    /**
     * Returns the value of field observacion
     *
     * @return string
     */
    public function getObservacion()
    {
        return $this->observacion;
    }

    /**
     * Returns the value of field asistencia
     *
     * @return integer
     */
    public function getAsistencia()
    {
        return $this->asistencia;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->belongsTo('salid', 'CdSale', 'salid', array('alias' => 'CdSale'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'cd_sale_client';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return CdSaleClient[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return CdSaleClient
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}