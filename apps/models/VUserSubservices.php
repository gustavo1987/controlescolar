<?php
namespace Modules\Models;
class VUserSubservices extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    protected $uid;

    /**
     *
     * @var string
     */
    protected $name;

    /**
     *
     * @var string
     */
    protected $last_name;

    /**
     *
     * @var string
     */
    protected $photo;

    /**
     *
     * @var string
     */
    protected $status;

    /**
     *
     * @var integer
     */
    protected $usid;

    /**
     *
     * @var string
     */
    protected $percentage;

    /**
     *
     * @var integer
     */
    protected $subsid;

    /**
     *
     * @var string
     */
    protected $name_subservice;

    /**
     *
     * @var string
     */
    protected $permalink_subservice;

    /**
     *
     * @var string
     */
    protected $status_subservice;

    /**
     *
     * @var integer
     */
    protected $servid;

    /**
     *
     * @var string
     */
    protected $name_service;

    /**
     *
     * @var string
     */
    protected $permalink_service;

    /**
     * Method to set the value of field uid
     *
     * @param integer $uid
     * @return $this
     */
    public function setUid($uid)
    {
        $this->uid = $uid;

        return $this;
    }

    /**
     * Method to set the value of field name
     *
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Method to set the value of field last_name
     *
     * @param string $last_name
     * @return $this
     */
    public function setLastName($last_name)
    {
        $this->last_name = $last_name;

        return $this;
    }

    /**
     * Method to set the value of field photo
     *
     * @param string $photo
     * @return $this
     */
    public function setPhoto($photo)
    {
        $this->photo = $photo;

        return $this;
    }

    /**
     * Method to set the value of field status
     *
     * @param string $status
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Method to set the value of field usid
     *
     * @param integer $usid
     * @return $this
     */
    public function setUsid($usid)
    {
        $this->usid = $usid;

        return $this;
    }

    /**
     * Method to set the value of field percentage
     *
     * @param string $percentage
     * @return $this
     */
    public function setPercentage($percentage)
    {
        $this->percentage = $percentage;

        return $this;
    }

    /**
     * Method to set the value of field subsid
     *
     * @param integer $subsid
     * @return $this
     */
    public function setSubsid($subsid)
    {
        $this->subsid = $subsid;

        return $this;
    }

    /**
     * Method to set the value of field name_subservice
     *
     * @param string $name_subservice
     * @return $this
     */
    public function setNameSubservice($name_subservice)
    {
        $this->name_subservice = $name_subservice;

        return $this;
    }

    /**
     * Method to set the value of field permalink_subservice
     *
     * @param string $permalink_subservice
     * @return $this
     */
    public function setPermalinkSubservice($permalink_subservice)
    {
        $this->permalink_subservice = $permalink_subservice;

        return $this;
    }

    /**
     * Method to set the value of field status_subservice
     *
     * @param string $status_subservice
     * @return $this
     */
    public function setStatusSubservice($status_subservice)
    {
        $this->status_subservice = $status_subservice;

        return $this;
    }

    /**
     * Method to set the value of field servid
     *
     * @param integer $servid
     * @return $this
     */
    public function setServid($servid)
    {
        $this->servid = $servid;

        return $this;
    }

    /**
     * Method to set the value of field name_service
     *
     * @param string $name_service
     * @return $this
     */
    public function setNameService($name_service)
    {
        $this->name_service = $name_service;

        return $this;
    }

    /**
     * Method to set the value of field permalink_service
     *
     * @param string $permalink_service
     * @return $this
     */
    public function setPermalinkService($permalink_service)
    {
        $this->permalink_service = $permalink_service;

        return $this;
    }

    /**
     * Returns the value of field uid
     *
     * @return integer
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * Returns the value of field name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Returns the value of field last_name
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->last_name;
    }

    /**
     * Returns the value of field photo
     *
     * @return string
     */
    public function getPhoto()
    {
        return $this->photo;
    }

    /**
     * Returns the value of field status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Returns the value of field usid
     *
     * @return integer
     */
    public function getUsid()
    {
        return $this->usid;
    }

    /**
     * Returns the value of field percentage
     *
     * @return string
     */
    public function getPercentage()
    {
        return $this->percentage;
    }

    /**
     * Returns the value of field subsid
     *
     * @return integer
     */
    public function getSubsid()
    {
        return $this->subsid;
    }

    /**
     * Returns the value of field name_subservice
     *
     * @return string
     */
    public function getNameSubservice()
    {
        return $this->name_subservice;
    }

    /**
     * Returns the value of field permalink_subservice
     *
     * @return string
     */
    public function getPermalinkSubservice()
    {
        return $this->permalink_subservice;
    }

    /**
     * Returns the value of field status_subservice
     *
     * @return string
     */
    public function getStatusSubservice()
    {
        return $this->status_subservice;
    }

    /**
     * Returns the value of field servid
     *
     * @return integer
     */
    public function getServid()
    {
        return $this->servid;
    }

    /**
     * Returns the value of field name_service
     *
     * @return string
     */
    public function getNameService()
    {
        return $this->name_service;
    }

    /**
     * Returns the value of field permalink_service
     *
     * @return string
     */
    public function getPermalinkService()
    {
        return $this->permalink_service;
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'v_user_subservices';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return VUserSubservices[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return VUserSubservices
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
