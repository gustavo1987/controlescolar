<?php
namespace Modules\Models;
use Phalcon\Mvc\Model\Validator\date_creation as date_creation;

class VClaseUser extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    protected $uid;

    /**
     *
     * @var string
     */
    protected $name;

    /**
     *
     * @var string
     */
    protected $last_name;

    /**
     *
     * @var string
     */
    protected $second_name;

    /**
     *
     * @var string
     */
    protected $rol;

    /**
     *
     * @var string
     */
    protected $clasid;

    /**
     *
     * @var string
     */
    protected $status_clase;

    /**
     *
     * @var string
     */
    protected $date_creation;

    /**
     *
     * @var string
     */
    protected $name_clase;

    /**
     *
     * @var string
     */
    protected $carrera;

    /**
     * Method to set the value of field uid
     *
     * @param integer $uid
     * @return $this
     */
    public function setUid($uid)
    {
        $this->uid = $uid;

        return $this;
    }

    /**
     * Method to set the value of field name
     *
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Method to set the value of field last_name
     *
     * @param string $last_name
     * @return $this
     */
    public function setLastName($last_name)
    {
        $this->last_name = $last_name;

        return $this;
    }

    /**
     * Method to set the value of field second_name
     *
     * @param string $second_name
     * @return $this
     */
    public function setSecondName($second_name)
    {
        $this->second_name = $second_name;

        return $this;
    }

    /**
     * Method to set the value of field rol
     *
     * @param string $rol
     * @return $this
     */
    public function setRol($rol)
    {
        $this->rol = $rol;

        return $this;
    }

    /**
     * Method to set the value of field clasid
     *
     * @param string $clasid
     * @return $this
     */
    public function setClasid($clasid)
    {
        $this->clasid = $clasid;

        return $this;
    }

    /**
     * Method to set the value of field status_clase
     *
     * @param string $status_clase
     * @return $this
     */
    public function setStatusClase($status_clase)
    {
        $this->status_clase = $status_clase;

        return $this;
    }

    /**
     * Method to set the value of field date_creation
     *
     * @param string $date_creation
     * @return $this
     */
    public function setDateCreation($date_creation)
    {
        $this->date_creation = $date_creation;

        return $this;
    }

    /**
     * Method to set the value of field name_clase
     *
     * @param string $name_clase
     * @return $this
     */
    public function setNameClase($name_clase)
    {
        $this->name_clase = $name_clase;

        return $this;
    }

    /**
     * Method to set the value of field carrera
     *
     * @param string $carrera
     * @return $this
     */
    public function setCarrera($carrera)
    {
        $this->carrera = $carrera;

        return $this;
    }

    /**
     * Returns the value of field uid
     *
     * @return integer
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * Returns the value of field name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Returns the value of field last_name
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->last_name;
    }

    /**
     * Returns the value of field second_name
     *
     * @return string
     */
    public function getSecondName()
    {
        return $this->second_name;
    }

    /**
     * Returns the value of field rol
     *
     * @return string
     */
    public function getRol()
    {
        return $this->rol;
    }

    /**
     * Returns the value of field clasid
     *
     * @return string
     */
    public function getClasid()
    {
        return $this->clasid;
    }

    /**
     * Returns the value of field status_clase
     *
     * @return string
     */
    public function getStatusClase()
    {
        return $this->status_clase;
    }

    /**
     * Returns the value of field date_creation
     *
     * @return string
     */
    public function getDateCreation()
    {
        return $this->date_creation;
    }

    /**
     * Returns the value of field name_clase
     *
     * @return string
     */
    public function getNameClase()
    {
        return $this->name_clase;
    }

    /**
     * Returns the value of field carrera
     *
     * @return string
     */
    public function getCarrera()
    {
        return $this->carrera;
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'v_clase_user';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return VClaseUser[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return VClaseUser
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
