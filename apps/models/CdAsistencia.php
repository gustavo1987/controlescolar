<?php
namespace Modules\Models;

class CdAsistencia extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    protected $asisid;

    /**
     *
     * @var string
     */
    protected $status;

    /**
     *
     * @var string
     */
    protected $clid;

    /**
     *
     * @var integer
     */
    protected $confid;

    /**
     * Method to set the value of field asisid
     *
     * @param integer $asisid
     * @return $this
     */
    public function setAsisid($asisid)
    {
        $this->asisid = $asisid;

        return $this;
    }

    /**
     * Method to set the value of field status
     *
     * @param string $status
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Method to set the value of field clid
     *
     * @param string $clid
     * @return $this
     */
    public function setClid($clid)
    {
        $this->clid = $clid;

        return $this;
    }

    /**
     * Method to set the value of field confid
     *
     * @param integer $confid
     * @return $this
     */
    public function setConfid($confid)
    {
        $this->confid = $confid;

        return $this;
    }

    /**
     * Returns the value of field asisid
     *
     * @return integer
     */
    public function getAsisid()
    {
        return $this->asisid;
    }

    /**
     * Returns the value of field status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Returns the value of field clid
     *
     * @return string
     */
    public function getClid()
    {
        return $this->clid;
    }

    /**
     * Returns the value of field confid
     *
     * @return integer
     */
    public function getConfid()
    {
        return $this->confid;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->belongsTo('clid', 'CdClient', 'clid', array('alias' => 'CdClient'));
        $this->belongsTo('confid', 'CdConferencia', 'confid', array('alias' => 'CdConferencia'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'cd_asistencia';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return cd_asistencia[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return cd_asistencia
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
