<?php
usernamespace Modules\Models;

class ChatUsersRooms extends \Phalcon\Mvc\Model
{
    /**
     *
     * @var integer
     */
    protected $id;
    /**
     *
     * @var string
     */
    protected $username;
    /**
     *
     * @var string
     */
    protected $room;
    /**
     *
     * @var integer
     */
    protected $mod_time;
    /**
     * Method to set the value of field id
     *
     * @param integer $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }
    /**
     * Method to set the value of field username
     *
     * @param string $username
     * @return $this
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }
    /**
     * Method to set the value of field room
     *
     * @param string $room
     * @return $this
     */
    public function setRoom($room)
    {
        $this->room = $room;

        return $this;
    }
    /**
     * Method to set the value of field mod_time
     *
     * @param integer $mod_time
     * @return $this
     */
    public function setModTime($mod_time)
    {
        $this->mod_time = $mod_time;

        return $this;
    }
    /**
     * Returns the value of field id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * Returns the value of field username
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }
    /**
     * Returns the value of field room
     *
     * @return string
     */
    public function getRoom()
    {
        return $this->room;
    }
    /**
     * Returns the value of field mod_time
     *
     * @return integer
     */
    public function getModTime()
    {
        return $this->mod_time;
    }
    /**
     * Initialize method for model.
     */
    public function initialize()
    {
    }
    /**
     * Returns table username mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'chat_users_rooms';
    }
    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return chat_users_rooms[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }
    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return chat_users_rooms
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }
}