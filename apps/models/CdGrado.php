<?php
namespace Modules\Models;

class CdGrado extends \Phalcon\Mvc\Model
{
    /**
     *
     * @var integer
     */
    protected $gradid;

    /**
     *
     * @var string
     */
    protected $brid;

    /**
     *
     * @var string
     */
    protected $status;

    /**
     *
     * @var integer
     */
    protected $date;

    /**
     *
     * @var integer
     */
    protected $uid;

    /**
     *
     * @var integer
     */
    protected $number;

    /**
     * Method to set the value of field gradid
     *
     * @param integer $gradid
     * @return $this
     */
    public function setGradid($gradid)
    {
        $this->gradid = $gradid;

        return $this;
    }

    /**
     * Method to set the value of field status
     *
     * @param string $status
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Method to set the value of field brid
     *
     * @param string $brid
     * @return $this
     */
    public function setBrid($brid)
    {
        $this->brid = $brid;

        return $this;
    }

    /**
     * Method to set the value of field date
     *
     * @param integer $date
     * @return $this
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Method to set the value of field uid
     *
     * @param integer $uid
     * @return $this
     */
    public function setUid($uid)
    {
        $this->uid = $uid;

        return $this;
    }

    /**
     * Method to set the value of field number
     *
     * @param integer $number
     * @return $this
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Returns the value of field gradid
     *
     * @return integer
     */
    public function getGradid()
    {
        return $this->gradid;
    }

    /**
     * Returns the value of field status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Returns the value of field brid
     *
     * @return string
     */
    public function getBrid()
    {
        return $this->brid;
    }

    /**
     * Returns the value of field date
     *
     * @return integer
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Returns the value of field uid
     *
     * @return integer
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * Returns the value of field number
     *
     * @return integer
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->belongsTo('brid', 'CdBrand', 'brid', array('alias' => 'CdBrand'));
        $this->belongsTo('uid', 'CdUser', 'uid', array('alias' => 'CdUser'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'cd_grado';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return cd_grado[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return cd_grado
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
