<?php
namespace Modules\Models;

class CdConferencia extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    protected $confid;

    /**
     *
     * @var string
     */
    protected $url;

    /**
     *
     * @var string
     */
    protected $urlteacher;

    /**
     *
     * @var string
     */
    protected $name;

    /**
     *
     * @var string
     */
    protected $status;

    /**
     *
     * @var string
     */
    protected $date_begin;

    /**
     *
     * @var integer
     */
    protected $uid;

    /**
     *
     * @var integer
     */
    protected $clasid;

    /**
     * Method to set the value of field confid
     *
     * @param integer $confid
     * @return $this
     */
    public function setConfid($confid)
    {
        $this->confid = $confid;

        return $this;
    }

    /**
     * Method to set the value of field url
     *
     * @param string $url
     * @return $this
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Method to set the value of field urlteacher
     *
     * @param string $urlteacher
     * @return $this
     */
    public function setUrlteacher($urlteacher)
    {
        $this->urlteacher = $urlteacher;

        return $this;
    }

    /**
     * Method to set the value of field name
     *
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Method to set the value of field status
     *
     * @param string $status
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Method to set the value of field date_begin
     *
     * @param string $date_begin
     * @return $this
     */
    public function setDateBegin($date_begin)
    {
        $this->date_begin = $date_begin;

        return $this;
    }

    /**
     * Method to set the value of field uid
     *
     * @param integer $uid
     * @return $this
     */
    public function setUid($uid)
    {
        $this->uid = $uid;

        return $this;
    }

    /**
     * Method to set the value of field clasid
     *
     * @param integer $clasid
     * @return $this
     */
    public function setClasid($clasid)
    {
        $this->clasid = $clasid;

        return $this;
    }

    /**
     * Returns the value of field confid
     *
     * @return integer
     */
    public function getConfid()
    {
        return $this->confid;
    }

    /**
     * Returns the value of field url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Returns the value of field urlteacher
     *
     * @return string
     */
    public function getUrlteacher()
    {
        return $this->urlteacher;
    }

    /**
     * Returns the value of field name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Returns the value of field status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Returns the value of field date_begin
     *
     * @return string
     */
    public function getDateBegin()
    {
        return $this->date_begin;
    }

    /**
     * Returns the value of field uid
     *
     * @return integer
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * Returns the value of field clasid
     *
     * @return integer
     */
    public function getClasid()
    {
        return $this->clasid;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        /*$this->hasMany('confid', 'CdDirection', 'confid', array('alias' => 'CdDirection'));
        $this->hasMany('confid', 'CdSale', 'confid', array('alias' => 'CdSale'));
        $this->belongsTo('uid', 'CdUser', 'uid', array('alias' => 'CdUser'));*/
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'cd_conferencia';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return CdConferencia[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return CdConferencia
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
