<?php
namespace Modules\Models;
use Phalcon\Mvc\Model\Validator\calificacion as calificacion;

class VClaseClient extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    protected $clid;

    /**
     *
     * @var string
     */
    protected $name;

    /**
     *
     * @var string
     */
    protected $last_name;

    /**
     *
     * @var string
     */
    protected $second_name;

    /**
     *
     * @var string
     */
    protected $clasid;

    /**
     *
     * @var string
     */
    protected $status_clase_student;

    /**
     *
     * @var string
     */
    protected $calificacion;

    /**
     *
     * @var string
     */
    protected $name_clase;

    /**
     * Method to set the value of field clid
     *
     * @param integer $clid
     * @return $this
     */
    public function setClid($clid)
    {
        $this->clid = $clid;

        return $this;
    }

    /**
     * Method to set the value of field name
     *
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Method to set the value of field last_name
     *
     * @param string $last_name
     * @return $this
     */
    public function setLastName($last_name)
    {
        $this->last_name = $last_name;

        return $this;
    }

    /**
     * Method to set the value of field second_name
     *
     * @param string $second_name
     * @return $this
     */
    public function setSecondName($second_name)
    {
        $this->second_name = $second_name;

        return $this;
    }

    /**
     * Method to set the value of field clasid
     *
     * @param string $clasid
     * @return $this
     */
    public function setClasid($clasid)
    {
        $this->clasid = $clasid;

        return $this;
    }

    /**
     * Method to set the value of field status_clase_student
     *
     * @param string $status_clase_student
     * @return $this
     */
    public function setStatusClaseStudent($status_clase_student)
    {
        $this->status_clase_student = $status_clase_student;

        return $this;
    }

    /**
     * Method to set the value of field calificacion
     *
     * @param string $calificacion
     * @return $this
     */
    public function setCalificacion($calificacion)
    {
        $this->calificacion = $calificacion;

        return $this;
    }

    /**
     * Method to set the value of field name_clase
     *
     * @param string $name_clase
     * @return $this
     */
    public function setNameClase($name_clase)
    {
        $this->name_clase = $name_clase;

        return $this;
    }

    /**
     * Returns the value of field clid
     *
     * @return integer
     */
    public function getClid()
    {
        return $this->clid;
    }

    /**
     * Returns the value of field name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Returns the value of field last_name
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->last_name;
    }

    /**
     * Returns the value of field second_name
     *
     * @return string
     */
    public function getSecondName()
    {
        return $this->second_name;
    }

    /**
     * Returns the value of field clasid
     *
     * @return string
     */
    public function getClasid()
    {
        return $this->clasid;
    }

    /**
     * Returns the value of field status_clase_student
     *
     * @return string
     */
    public function getStatusClaseStudent()
    {
        return $this->status_clase_student;
    }

    /**
     * Returns the value of field calificacion
     *
     * @return string
     */
    public function getCalificacion()
    {
        return $this->calificacion;
    }

    /**
     * Returns the value of field name_clase
     *
     * @return string
     */
    public function getNameClase()
    {
        return $this->name_clase;
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'v_clase_client';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return VClaseClient[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return VClaseClient
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
