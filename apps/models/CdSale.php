<?php
namespace Modules\Models;
class CdSale extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=11, nullable=false)
     */
    protected $salid;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    protected $clid;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    protected $uid;

    /**
     *
     * @var string
     * @Column(type="string", length=15, nullable=false)
     */
    protected $total;

    /**
     *
     * @var string
     * @Column(type="string", length=10, nullable=true)
     */
    protected $discount;

    /**
     *
     * @var string
     * @Column(type="string", length=10, nullable=true)
     */
    protected $received;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $status;

    /**
     *
     * @var string
     * @Column(type="string", length=1, nullable=true)
     */
    protected $send_email;

    /**
     *
     * @var string
     * @Column(type="string", length=75, nullable=true)
     */
    protected $email;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $date_creation;

    /**
     *
     * @var string
     * @Column(type="string", length=250, nullable=true)
     */
    protected $name;

    /**
     *
     * @var string
     * @Column(type="string", length=15, nullable=true)
     */
    protected $phone;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $type;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=true)
     */
    protected $pullet_apart;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=true)
     */
    protected $genid;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $start_date;

    /**
     * Method to set the value of field salid
     *
     * @param integer $salid
     * @return $this
     */
    public function setSalid($salid)
    {
        $this->salid = $salid;

        return $this;
    }

    /**
     * Method to set the value of field clid
     *
     * @param integer $clid
     * @return $this
     */
    public function setClid($clid)
    {
        $this->clid = $clid;

        return $this;
    }

    /**
     * Method to set the value of field uid
     *
     * @param integer $uid
     * @return $this
     */
    public function setUid($uid)
    {
        $this->uid = $uid;

        return $this;
    }

    /**
     * Method to set the value of field total
     *
     * @param string $total
     * @return $this
     */
    public function setTotal($total)
    {
        $this->total = $total;

        return $this;
    }

    /**
     * Method to set the value of field discount
     *
     * @param string $discount
     * @return $this
     */
    public function setDiscount($discount)
    {
        $this->discount = $discount;

        return $this;
    }

    /**
     * Method to set the value of field received
     *
     * @param string $received
     * @return $this
     */
    public function setReceived($received)
    {
        $this->received = $received;

        return $this;
    }

    /**
     * Method to set the value of field status
     *
     * @param string $status
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Method to set the value of field send_email
     *
     * @param string $send_email
     * @return $this
     */
    public function setSendEmail($send_email)
    {
        $this->send_email = $send_email;

        return $this;
    }

    /**
     * Method to set the value of field email
     *
     * @param string $email
     * @return $this
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Method to set the value of field date_creation
     *
     * @param string $date_creation
     * @return $this
     */
    public function setDateCreation($date_creation)
    {
        $this->date_creation = $date_creation;

        return $this;
    }

    /**
     * Method to set the value of field name
     *
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Method to set the value of field phone
     *
     * @param string $phone
     * @return $this
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Method to set the value of field type
     *
     * @param string $type
     * @return $this
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Method to set the value of field pullet_apart
     *
     * @param integer $pullet_apart
     * @return $this
     */
    public function setPulletApart($pullet_apart)
    {
        $this->pullet_apart = $pullet_apart;

        return $this;
    }

    /**
     * Method to set the value of field genid
     *
     * @param integer $genid
     * @return $this
     */
    public function setGenid($genid)
    {
        $this->genid = $genid;

        return $this;
    }

    /**
     * Method to set the value of field start_date
     *
     * @param integer $start_date
     * @return $this
     */
    public function setStartDate($start_date)
    {
        $this->start_date = $start_date;

        return $this;
    }

    /**
     * Returns the value of field salid
     *
     * @return integer
     */
    public function getSalid()
    {
        return $this->salid;
    }

    /**
     * Returns the value of field clid
     *
     * @return integer
     */
    public function getClid()
    {
        return $this->clid;
    }

    /**
     * Returns the value of field uid
     *
     * @return integer
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * Returns the value of field total
     *
     * @return string
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * Returns the value of field discount
     *
     * @return string
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * Returns the value of field received
     *
     * @return string
     */
    public function getReceived()
    {
        return $this->received;
    }

    /**
     * Returns the value of field status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Returns the value of field send_email
     *
     * @return string
     */
    public function getSendEmail()
    {
        return $this->send_email;
    }

    /**
     * Returns the value of field email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Returns the value of field date_creation
     *
     * @return string
     */
    public function getDateCreation()
    {
        return $this->date_creation;
    }

    /**
     * Returns the value of field name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Returns the value of field phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Returns the value of field type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Returns the value of field pullet_apart
     *
     * @return integer
     */
    public function getPulletApart()
    {
        return $this->pullet_apart;
    }

    /**
     * Returns the value of field genid
     *
     * @return integer
     */
    public function getGenid()
    {
        return $this->genid;
    }

    /**
     * Returns the value of field start_date
     *
     * @return integer
     */
    public function getStartDate()
    {
        return $this->start_date;
    }

    /**
     * Validations and business logic
     *
     * @return boolean
     */

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->hasMany('salid', 'CdCredit', 'salid', ['alias' => 'CdCredit']);
        $this->hasMany('salid', 'CdSubservicesSale', 'salid', ['alias' => 'CdSubservicesSale']);
        $this->belongsTo('clid', 'CdClient', 'clid', ['alias' => 'CdClient']);
        $this->belongsTo('uid', 'CdUser', 'uid', ['alias' => 'CdUser']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'cd_sale';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return CdSale[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return CdSale
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
