<?php
namespace Modules\Models;
class CdGeneracion extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    protected $genid;

    /**
     *
     * @var integer
     */
    protected $name;

    /**
     *
     * @var string
     */
    protected $status;

    /**
     * Method to set the value of field genid
     *
     * @param integer $genid
     * @return $this
     */
    public function setGenid($genid)
    {
        $this->genid = $genid;

        return $this;
    }

    /**
     * Method to set the value of field name
     *
     * @param integer $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Method to set the value of field status
     *
     * @param string $status
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Returns the value of field genid
     *
     * @return integer
     */
    public function getGenid()
    {
        return $this->genid;
    }

    /**
     * Returns the value of field name
     *
     * @return integer
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Returns the value of field status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'cd_generacion';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return CdGeneracion[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return CdGeneracion
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}