<?php
namespace Modules\Models;
class CdCostos extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    protected $cosid;

    /**
     *
     * @var string
     */
    protected $name;

    /**
     *
     * @var string
     */
    protected $count;

    /**
     *
     * @var integer
     */
    protected $brid;

    /**
     *
     * @var string
     */
    protected $status;

    /**
     * Method to set the value of field cosid
     *
     * @param integer $cosid
     * @return $this
     */
    public function setCosid($cosid)
    {
        $this->cosid = $cosid;

        return $this;
    }

    /**
     * Method to set the value of field name
     *
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Method to set the value of field date_time
     *
     * @param string $date_time
     * @return $this
     */
    public function setCount($count)
    {
        $this->count = $count;

        return $this;
    }

    /**
     * Method to set the value of field brid
     *
     * @param integer $brid
     * @return $this
     */
    public function setBrid($brid)
    {
        $this->brid = $brid;

        return $this;
    }

    /**
     * Method to set the value of field status
     *
     * @param string $status
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Returns the value of field cosid
     *
     * @return integer
     */
    public function getCosid()
    {
        return $this->cosid;
    }

    /**
     * Returns the value of field name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Returns the value of field date_time
     *
     * @return string
     */
    public function getCount()
    {
        return $this->count;
    }

    /**
     * Returns the value of field brid
     *
     * @return integer
     */
    public function getBrid()
    {
        return $this->brid;
    }

    /**
     * Returns the value of field status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->belongsTo('brid', 'CdBrand', 'brid', array('alias' => 'CdBrand'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'cd_costos';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return CdCostos[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return CdCostos
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
