CREATE TABLE `cd_cupon` (`cupid` int(11) NOT NULL,`salid` int(11) NOT NULL,`clid` int(11) NOT NULL,`uid` int(11) NOT NULL,`total` varchar(15) NOT NULL,`status` varchar(15) DEFAULT 'CANCEL',`date_creation` datetime DEFAULT NULL) ENGINE=InnoDB DEFAULT CHARSET=latin1;
ALTER TABLE `cd_cupon` ADD PRIMARY KEY (`cupid`),ADD KEY `fk_cd_cupon_cd_client1_idx` (`clid`),ADD KEY `fk_cd_cupon_cd_user1_idx` (`uid`);ADD KEY `fk_cd_cupon_cd_sale1_idx` (`salid`);
ALTER TABLE `cd_cupon` MODIFY `cupid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
ALTER TABLE `cd_sale` CHANGE `status` `status` ENUM('PAY','CANCEL','CHANGE','PULLEDAPART','CUPON') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT 'CANCEL';




CREATE VIEW v_sale_cupon AS SELECT `cd_sale`.`salid`, `cd_sale`.`name`,`cd_cupon`.`cupid`,`cd_cupon`.`clid`,`cd_cupon`.`uid`, `cd_cupon`.`total`, `cd_cupon`.`status`, `cd_cupon`.`date_creation` FROM `cd_sale` JOIN `cd_cupon` on `cd_sale`.`salid`=`cd_cupon`.`salid`;







/*nuevo martes*/


alter view v_shopping_list as select `cs`.`salid` AS `salid`,`css`.`subsalid` AS `subsalid`,`css`.`quantity` AS `quantity_ss`,`css`.`date_creation` AS `date_creation`,`css`.`commission` AS `commission`,`css`.`type` AS `type`,`cs`.`type` AS `typesalid`,`cs`.`status` AS `statussalid`,`css`.`status` AS `status`,`css`.`payout` AS `payout`,`css`.`uid` AS `uid_ss`,`css`.`price_general` AS `price_general`,`cu`.`uid` AS `uid_attend`,`cu`.`name` AS `name_attend`,`cu`.`last_name` AS `last_name_attend`,`cu`.`second_name` AS `second_name_attend`,`cds`.`subsid` AS `subsid`,`cdse`.`name` AS `name_service`,`cds`.`name` AS `name_subservice`,`cds`.`price` AS `price`,`cds`.`discount` AS `discount`,`cp`.`pid` AS `pid`,`cb`.`name` AS `name_brand`,`cp`.`name` AS `name_product`,`cp`.`sale_price` AS `sale_price`,`css`.`discount` AS `percentage`,`cp`.`ticket` AS `ticket` from ((((((`cd_sale` `cs` left join `cd_subservices_sale` `css` on(`cs`.`salid` = `css`.`salid`)) left join `cd_user` `cu` on(`cu`.`uid` = `css`.`uid`)) left join `cd_subservices` `cds` on(`css`.`subsid` = `cds`.`subsid`)) left join `cd_services` `cdse` on(`cds`.`servid` = `cdse`.`servid`)) left join `cd_product` `cp` on(`cp`.`pid` = `css`.`pid`)) left join `cd_brand` `cb` on(`cb`.`brid` = `cp`.`brid`));