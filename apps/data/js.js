var salid = $("#salid").attr("data-value");
var uid = $("#uid").attr("data-value");
var clid = $("#clid").attr("data-value");
$(document).ready(function(){
    var sales = $("#sales");
    var formSales = $("#formSales");
    if($(".dataTable").length>=1){
        var t = getDataTable();
    }
    if(sales.length>=1){
        getSection($(".select_service"),"service",clid);
        getSection($(".select_product"),"product",uid);
        getSection($(".select_client"),"client",uid);
        getSection($(".select_employed"),"employed",uid);
        getSection($(".select_product_sale"),"product_sale",uid);
        $(document.body).on("change","table#tableSales tbody tr td input",function(){
            $this = $(this);
            id = $this.parent().parent().attr("data-value");
            type = $this.parent().parent().attr("data-type");
            var val = parseInt($this.val());
            if(val==0 || val<=0){
                val = 1;
                $this.val(1);
            }
            $.ajax({
                data : {id:id,type:type,val:val},
                url : "/dashboard/sales/update-elements",
                type : "POST",
                dataType : "json",
                success:function(response){
                    if(response.code==200){
                        parent = $this.parent().parent();
                        price = parseFloat(parent.find(".price_service").attr("data-value"));
                        discount = -(parseFloat(parent.find(".total_service").attr("data-discount")))*val;
                        subtotal = (price * val).toFixed(2);
                        $new_val = (price * val)-(-discount);

                        parent.find(".subtotal_service p").text("$"+subtotal);
                        parent.find(".discount_service strong").text("$"+discount.toFixed(2));
                        parent.find(".total_service").text("$"+$new_val.toFixed(2)).attr("data-value",$new_val.toFixed(2));

                        calculo();

                    }else{
                        messages(2)
                    }
                },error:function(){
                    messages(3)
                }
            });
            return false;
        });
        deleteRow("deleteElement","/dashboard/sales/delete","service");

        $("#checkout").click(function(e){
            e.preventDefault();
            amount_to_pay = $("#amount_to_pay").attr("data-value");
            if(amount_to_pay!=0){
                $("#modalPay").modal("show");
                $("#debt").text("$"+amount_to_pay).attr("data-pay",amount_to_pay);
            }
        });
        /* Apartado */
        $("#pulledApart").click(function(e){
            e.preventDefault();
            amount_to_pay = $("#amount_to_pay").attr("data-value");
            if(amount_to_pay!=0){
                $("#modalPulledApart").modal("show");
                $("#debt_apart").text("$"+amount_to_pay).attr("data-pay",amount_to_pay);
            }
        });

        /* Abonar a Apartado */
        $(".abonarPulledApart").click(function(e){
            e.preventDefault();
            amount_to_pay = $("#amount_to_pay").attr("data-value");
            if(amount_to_pay>0){
                $("#modalAbonarPulledApart").modal("show");
                $("#debt_apart").text("$"+amount_to_pay).attr("data-pay",amount_to_pay);
            }else{
                alert("Esta cuenta ya esta saldada.")
            }
        });


        /* Cancel Sale */
        $("#cancel_checkout").click(function(e){
            e.preventDefault();
            amount_to_pay = $("#amount_to_pay").attr("data-value");
            $("#buttons_cancel").removeClass("hidden").addClass("in");
            if(amount_to_pay!=0){
                $("#message-box-cancel").toggleClass("open");
            }
        });
        $("#cancel_sale").click(function(e){
            e.preventDefault();
            $("#buttons_cancel").removeClass("in").addClass("hidden");
            $("#message_cancel").removeClass("hidden").addClass("in");
            $.ajax({
                url:"/dashboard/sales/cancel",
                type:"POST",
                data:{salid:salid,pay:$("#amount_to_pay").attr("data-value")},
                dataType:"JSON",
                success: function(response){
                    if(response.code==200){
                        window.location.href = response.url;
                    }else{
                        messages(2);
                        $("#buttons_cancel").removeClass("hidden").addClass("in");
                        $("#message_cancel").removeClass("in").addClass("hidden");
                    }
                },error: function(){
                    messages(3);
                }
            });
        });
        /* Cancel Sale */

        $("#received_apart").on("keyup",function(){
            $this = $(this);
            value = $this.val();
            amount_to_pay = $("#debt_apart").attr("data-pay");
            calculate_apart(1,amount_to_pay,value);
        });

        $("#received").on("keyup",function(){
            $this = $(this);
            value = $this.val();
            amount_to_pay = $("#debt").attr("data-pay");
            amount_discount = $("#discount_ext").val();
            vuelto_cliente = (amount_to_pay-amount_discount);
            calculate(1,value,vuelto_cliente);
        });
        $("#discount_ext").dblclick(function(){
            $this = $(this);
            $this.removeAttr("readonly").focus();
        }).keyup(function(){
            value = parseFloat($(this).val());
            amount_to_pay = $("#debt").attr("data-pay");
            received = $("#received").val()==""?0:parseFloat($("#received").val());
            if(isNaN(value)){
                calculate(2,received,amount_to_pay);
            }
            else if(value>=0){
                new_amount_to_pay = (amount_to_pay-value).toFixed(2);
                if(value>0 && value<=amount_to_pay){
                    $("#debt").addClass("text-decoration");
                    $("#discount_debt").text("$"+(new_amount_to_pay));
                }else if(value>amount_to_pay){
                    alert("No se puede colocar una cantidad igual o mayor a la del pago");
                    $(this).val("");
                    calculate(2,received,amount_to_pay);
                    return false;
                }
                else{
                    $("#debt").removeClass("text-decoration");
                    $("#discount_debt").text("");
                }
                vuelto_cliente = (received-new_amount_to_pay).toFixed(2);
                $this.attr("data-discount",value);
                $("#vuelto_cliente").text("$"+vuelto_cliente);
                vueltoCliente(vuelto_cliente);
            }
        }).blur(function(){
            $(this).attr("readonly",true);
        });
        $("#modalPay").on('hidden.bs.modal', function (e) {
            $("#received").val(0);
            $("#vuelto_cliente").text("$0");
            $("#discount_ext").val(0);
            $("#debt").removeClass("text-decoration");
            $("#discount_debt").text("");
            $("#mail_ticket").attr("checked",false);
            // $("#send_email").removeClass("in").addClass("hidden").removeAttr("required",false);
        });

        /*Assigning users of the services*/
        $(document.body).on("click","table#tableSales tbody tr td.add_stylist button",function(e){
            e.preventDefault();
            $this = $(this);
            $type_id = $this.parent().parent().attr("data-value");
            $type_status = $this.parent().parent().attr("data-type");
            $.ajax({
                data : $(this).serialize(),
                url : "/dashboard/sales/get-users",
                type : "POST",
                dataType : "json",
                success:function(response){
                    if(response.code==200){
                        data = [];
                        $.each(response.result,function(i,item){
                            /*append($("<option class='users_sales'>").attr('value',item.id).text(item.text));*/
                            data[i] = {id:item.id,text:item.text};
                        });
                        $("#type_id").val($type_id);
                        $("#type_status").val($type_status);
                        $("#user_services").select2({maximumSelectionLength:1,data:data});
                    }
                },
                error : function(){
                    alert("Ha ocurrido un error intente nuevamente")
                }
            });
            $("#modalStylist").modal("show");
            return false;
        });
        $("#formServices").submit(function(e){
            e.preventDefault();
            $.ajax({
                data : $(this).serialize(),
                url : "/dashboard/sales/change-stylist",
                type : "POST",
                dataType : "json",
                success:function(response){
                    if(response.code==200){
                        $("#modalStylist").modal("hide");
                        $("table#tableSales tr."+response.result.id+" td.add_stylist button").text(response.result.user);
                    }else{
                        alert("Ha ocurrido un error, prueba nuevamente");
                    }
                },
                error : function(){
                    alert("Ha ocurrido un error intente nuevamente")
                }
            });
        });
        $("#modalStylist").on("hidden.bs.modal",function(){
            $("select#user_services option").remove();
            $("#user_services").select2("destroy");
            $("#modalStylist").modal("hide");
        });
        /*End Assigning users of the services*/

        /* Checked send to email and pay account */
        $("#pay_form_action").submit(function(e){
            e.preventDefault();
            $("#message-box-process").toggleClass("open");
            values = {
                total : $("#debt").attr("data-pay"),
                salid : salid,
                type:"pay"
            };
            data  = $(this).serialize()+"&"+jQuery.param(values);
            $.ajax({
                data : data,
                url : "/dashboard/sales/pay-services",
                type : "POST",
                dataType : "json",
                success:function(response){
                    if(response.code==200){
                        messages(4);
                        setTimeout(function(){
                            window.location.href="";
                        },2500);
                    }else{
                        alert("Ha ocurrido un error y no se ha podido realizar su pago, pruebe nuevamente");
                    }
                },
                error : function(){
                    alert("Ha ocurrido un error y no se ha podido realizar su pago, pruebe nuevamente");
                }
            });

        });
        /*$("#mail_ticket").click(function(){
         if($(this).is(":checked")){
         $("#send_email").removeClass("hidden").addClass("in").val($("#email").attr("data-value")).attr("required",true);
         }else{
         $("#send_email").removeClass("in").addClass("hidden").removeAttr("required");
         }
         });*/
        /* End Checked send to email and pay account*/

        /* Checked send to email and pulledApart account */
        $("#pulled_apart_form").submit(function(e){
            e.preventDefault();
            $("#message-box-process-pulled-apart").toggleClass("open");
            values = {
                total : $("#debt_apart").attr("data-pay"),
                salid : salid,
                type:"apart"
            };
            data  = $(this).serialize()+"&"+jQuery.param(values);
            $.ajax({
                data : data,
                url : "/dashboard/sales/pay-services",
                type : "POST",
                dataType : "json",
                success:function(response){
                    if(response.code==200){
                        messages(4);
                        setTimeout(function(){
                            window.location.href="";
                        },2500);
                    }else{
                        alert("Ha ocurrido un error y no se ha podido realizar su pago, pruebe nuevamente");
                    }
                },
                error : function(){
                    alert("Ha ocurrido un error y no se ha podido realizar su pago, pruebe nuevamente");
                }
            });

        });
        /*Abono*/
        $("#pulled_apart_abono_form").submit(function(e){
            e.preventDefault();
            $("#message-box-process-pulled-apart").toggleClass("open");
            values = {
                salid : salid,
                type: "abono"
            };
            data  = $(this).serialize()+"&"+jQuery.param(values);
            $.ajax({
                data : data,
                url : "/dashboard/sales/pay-pulled-apart",
                type : "POST",
                dataType : "json",
                success:function(response){
                    if(response.code==200){
                        messages(4);
                        setTimeout(function(){
                            window.location.href="";
                        },2500);
                    }else{
                        alert("Ha ocurrido un error y no se ha podido realizar su pago, pruebe nuevamente");
                    }
                },
                error : function(){
                    alert("Ha ocurrido un error y no se ha podido realizar su pago, pruebe nuevamente");
                }
            });

        });
        /* $("#mail_ticket").click(function(){
         if($(this).is(":checked")){
         $("#send_email").removeClass("hidden").addClass("in").val($("#email").attr("data-value")).attr("required",true);
         }else{
         $("#send_email").removeClass("in").addClass("hidden").removeAttr("required");
         }
         });*/
        /* End Checked send to email and pulledApart account*/

        function calculate(type,received,amount_to_pay){
            if(type==2){
                $("#debt").removeClass("text-decoration");
                $("#discount_debt").text("");
            }
            vuelto_cliente = (received-amount_to_pay).toFixed(2);
            vueltoCliente(vuelto_cliente);
        }
        function vueltoCliente(vuelto_cliente){
            if(isNaN(vuelto_cliente)){
                $("#vuelto_cliente").text("$"+0);
                $("#buttonPay").attr("disabled",true).addClass("disabled");
            }else if(vuelto_cliente < 0){
                $("#buttonPay").attr("disabled",true).addClass("disabled");
                $("#vuelto_cliente").text("$"+vuelto_cliente);
            }else{
                $("#vuelto_cliente").text("$"+vuelto_cliente);
                $("#buttonPay").removeAttr("disabled").removeClass("disabled");
            }
        }
        /* ***por que dos funciones*** */
        function vueltoCliente(vuelto_cliente){
            if(isNaN(vuelto_cliente)){
                $("#vuelto_cliente").text("$"+0);
                $("#buttonPay").attr("disabled",true).addClass("disabled");
            }else if(vuelto_cliente < 0){
                $("#buttonPay").attr("disabled",true).addClass("disabled");
                $("#vuelto_cliente").text("$"+vuelto_cliente);
            }else{
                $("#vuelto_cliente").text("$"+vuelto_cliente);
                $("#buttonPay").removeAttr("disabled").removeClass("disabled");
            }
        }

        function calculate_apart(type,received,amount_to_pay){
            if(type==2){
                $("#debt_apart").removeClass("text-decoration");
                $("#discount_debt_apart").text("");
            }
            vuelto_cliente = (received-amount_to_pay).toFixed(2);
            vueltoClienteApart(vuelto_cliente);
        }
        function vueltoClienteApart(vuelto_cliente){
            if(isNaN(vuelto_cliente)){
                $("#vuelto_cliente_apart").text("$"+0);
                $("#buttonPay_apart").attr("disabled",true).addClass("disabled");
            }else if(vuelto_cliente < 0){
                $("#buttonPay_apart").attr("disabled",true).addClass("disabled");
                $("#vuelto_cliente_apart").text("$"+vuelto_cliente);
            }else{
                $("#vuelto_cliente_apart").text("$"+vuelto_cliente);
                $("#buttonPay_apart").removeAttr("disabled").removeClass("disabled");
            }
        }

        /*Abono a apartado*/
        function vueltoClienteApart(vuelto_cliente){
            if(isNaN(vuelto_cliente)){
                $("#vuelto_cliente_apart_abono").text("$"+0);
                $("#buttonPay_apart_abono").attr("disabled",true).addClass("disabled");
            }else if(vuelto_cliente < 0){
                $("#buttonPay_apart_abono").attr("disabled",true).addClass("disabled");
                $("#vuelto_cliente_apart_abono").text("$"+vuelto_cliente);
            }else{
                $("#vuelto_cliente_apart_abono").text("$"+vuelto_cliente);
                $("#buttonPay_apart_abono").removeAttr("disabled").removeClass("disabled");
            }
        }
    }
    if(formSales.length>=1){
        (function($){
            $.fn.datepicker.dates['es'] = {
                days: ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo"],
                daysShort: ["Dom", "Lun", "Mar", "Mié", "Jue", "Vie", "Sáb", "Dom"],
                daysMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa", "Do"],
                months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
                monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
                today: "Hoy",
                clear: "Borrar",
                weekStart: 1,
                format: "dd/mm/yyyy"
            };
        }(jQuery));
        $('.getDatepicker').datepicker({
            language : "es",
            autoclose:true
        }).on('changeDate', function(e) {
            $this = $(this);
            start_date = $("#start_date").val();
            cutoff_date_val = $("#cutoff_date");
            if(cutoff_date_val.val()<start_date && cutoff_date_val.val().length>0){
                cutoff_date_val.val("");
                cutoff_date_val.popover('show');
                setTimeout(function(){
                    cutoff_date_val.popover('destroy')
                },2000);
            }
        });
        $("#formSales").submit(function (e) {
            submitSales = $("#submitSales");
            submitSales.addClass("disabled").attr("disabled",true);
            $("#exportReports").addClass("disabled").attr("disabled",true);
            t.row().remove().draw( false );
            e.preventDefault();

            $.ajax({
                data : $(this).serialize(),
                url : "/dashboard/sales/get-sales",
                type : "POST",
                dataType : "json",
                success:function(response){
                    $("table#tableSales tbody tr").remove();
                    var class_tr = "";
                    if(response.code==200){
                        $.each(response.result,function(k,val){
                            discount = val.discount?val.discount:0;
                            var table = $("table#tableSales tbody");
                            switch (val.status){
                                case "Pagado":
                                    class_tr  = t.row.add( [val.salid, val.total, discount, val.user, val.date,val.status,
                                        "<a onclick="+"printPage('"+val.url_print+"')"+" data-toggle='tooltip' data-placement='top' title='Imprimir comprobante de venta'><span class='fa fa-print fa-2x'></span></a>&nbsp;<a target='_blank' href='"+val.url_list_print+"' data-toggle='tooltip' data-placement='top' title='Ver comprobante de venta'><span class='fa fa-eye fa-2x'></span></a>&nbsp;<a target='_blank' href='"+val.url_view+"' data-toggle='tooltip' data-placement='top' title='Información general de venta'><span class='fa fa-folder-open fa-2x'></span></a>"
                                    ] ).draw( false).node();
                                    $(class_tr).addClass( 'success' );
                                    break;
                                case "Cancelada":
                                    status_color = val.total<=0?"info":"warning";
                                    status_message = val.total<=0?"En proceso":val.status;
                                    if(val.total<=0){
                                        class_tr  = t.row.add( [val.salid, val.total, discount, val.user, val.date,status_message, "<a target='_blank' href='"+val.url_list_print+"' data-toggle='tooltip' data-placement='top' title='Editar venta'><span class='fa fa-edit fa-2x'></span></a>"] ).draw( false).node();
                                        $(class_tr).addClass(status_color);
                                    }else{
                                        class_tr  = t.row.add( [val.salid, val.total, discount, val.user, val.date,status_message, "<a target='_blank' href='"+val.url_view+"' data-toggle='tooltip' data-placement='top' title='Información general de venta'><span class='fa fa-folder-open fa-2x'></span></a>"] ).draw( false).node();
                                        $(class_tr).addClass(status_color);
                                    }
                                    break;
                            }
                            t.columns.adjust().draw();
                        });
                        $("#subtotal").text("$ "+response.breakdown.subtotal);
                        $("#iva_general").text("$ "+response.breakdown.iva_general);
                        $("#amount_to_pay").text("$ "+response.breakdown.total);
                    }else if(response.code==300){
                        submitSales.popover('show');
                        setTimeout(function(){
                            submitSales.popover('destroy')
                        },2000);
                        $("#subtotal").text("$0");
                        $("#iva_general").text("$0");
                        $("#amount_to_pay").text("$0");
                    }else{
                        messages(2);
                    }
                },error:function(){
                    messages(3);
                },complete:function(){
                    submitSales.removeClass("disabled").attr("disabled",false);
                    $("#exportReports").removeClass("disabled").attr("disabled",false);
                }
            });
        });
        $("#exportReports").click(function(){
            user = $("#user").val();
            start_date = $("#start_date").val();
            cutoff_date = $("#cutoff_date").val();
            status = $("#status").val();
            window.open('/dashboard/sales/export-general?user='+user+'&start_date='+start_date+'&cutoff_date='+cutoff_date+'&status='+status+'','_blank');
        });
    }
});
function getDataTable(){
    t = $("#tableSales").DataTable({"language": {
        "paginate": {"first":"Primero","last":"Ultimo","next":"Siguiente","previous":"Anterior"},
        "lengthMenu": "Mostrar _MENU_ registros por página",
        "zeroRecords": "Nada por el momento -Lo lamento",
        "info": "Mostrando del _START_ al _END_ de _TOTAL_",
        "infoEmpty": "No hay registros disponibles",
        "infoFiltered": "(Filtrado de _MAX_ registros en total)"
    },
        fnPreDrawCallback: function(oSettings, json) {
            $('.dataTables_filter input').attr('placeholder', 'Buscar.');
        }
    });
    return t;
}
function getSection($selector,$type,id){
    $selector.select2({
        ajax: {
            url: "/dashboard/sales/feed",
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    q:params.term,
                    type : $type,
                    id : id
                };
            },
            processResults: function (data, page) {
                return {
                    results: $.map(data, function (item,key) {
                        return {
                            text: item,
                            id: key
                        }

                    })
                };
            }
        },
        escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
        minimumInputLength: 1
    }).on("select2:select", function(response) {
        type_id = response.params.data.id;
        uidEmployed = $('select#employed option:selected').val()?$('select#employed option:selected').val():0;
        $.ajax({
            data : {type_id:type_id,salid:salid,type:$type,uidUser:uid,uidEmployed:uidEmployed},
            url : "/dashboard/sales/feed-sub-services",
            type : "POST",
            dataType : "json",
            success:function(response){
                if(response.code==200){
                    /* Adding tr for type 'status' or type of sale */
                    table = $("table#tableSales tbody");
                    switch (response.result.type){
                        case "SERVICIO":
                            /* Creating variables, price = el precio del servicio o producto, percentage = Porcentaje para el descuento,
                             * discount = obtengo el porcentaje con la formula (price*percentage)/100, htmlDT es para agregar el porcentaje del descuento, mediante texto
                             * htmlD agregar el descuento en valor, price_discount = mostrar precio con descuento aplicado
                             */
                            price = parseFloat(response.result.price);
                            percentage = parseFloat(response.result.discount);
                            discount = (price*percentage)/100;
                            htmlDT = discount>0?"<strong class='color-success'>"+percentage+"% de descuento</strong>":"";
                            htmlD = discount>0?"<strong>$-"+(discount.toFixed(2))+"</strong>":"";
                            price_discount  = (price-discount).toFixed(2);
                            table.append('<tr data-type="service" data-value="'+response.result.subsalid+'" class="'+response.result.subsalid+'"><td class="text-center"><span class="deleteElement cursor_pointer" data-toggle="tooltip" data-placement="top" title="" data-original-title="Eliminar servicio"><i class="fa fa-remove fa-2x"></i></span></td><td class="description"><strong class="name_service">'+response.result.name_services+'</strong><p class="name_sub_service">'+response.result.name_sub_service+' '+htmlDT+'</p></td><!--td class="add_stylist"><button class="btn btn-success">Asignar Vendedora</button></td--><td class="text-center price_service" data-value="'+price+'">$'+price.toFixed(2)+'</td><td class="text-center count_subtotal"><input type="number" min="1" max="100" value="'+response.result.quantity+'" class="form-control comission"/></td><td class="text-center subtotal_service" data-value="'+price+'"><p>$'+price.toFixed(2)+'</p></td><td class="text-center discount_service">'+htmlD+'</td><td class="text-center total_service" data-discount="'+discount+'" data-value="'+price_discount+'">$'+price_discount+'</td></tr>');
                            break;
                        case "CONSUMO": table.append('<tr data-type="product" data-value="'+response.result.subsalid+'" class="'+response.result.subsalid+' warning"><td class="text-center"><span class="deleteElement cursor_pointer" data-toggle="tooltip" data-placement="top" title="" data-original-title="Eliminar consumo de producto"><i class="fa fa-remove fa-2x"></i></span></td><td class="description"><strong class="name_service">'+response.result.name_brand+'&nbsp;<span class="fa fa-info consumer-product" data-toggle="tooltip" data-placement="top" data-original-title="Consumo exclusivo de la tienda, por servicio."></span></strong><p class="name_sub_service">'+response.result.name_product+'</p></td><td class="add_stylist"><button class="btn btn-success">Agregar Vendedora</button></td><td class="text-center price_service" data-value="0">$0</td><td class="text-center count_subtotal"><input type="number" min="1" max="10000" value="'+response.result.quantity+'" class="form-control comission" /><span>mL&nbsp;o&nbsp;Pieza</span></td><td class="text-center subtotal_service" data-value="0"><p>$0</p></td><td class="text-center discount_service">$0</td><td class="text-center total_service" data-discount="0" data-value="0">$0</td></tr>');
                            break;
                        case "VENTAS":
                            price = parseFloat(response.result.sale_price);
                            percentage = parseFloat(response.result.percentage);
                            discount = (price*percentage)/100;
                            htmlDT = discount>0?"<strong class='color-success'>"+percentage+"% de descuento</strong>":"";
                            htmlD = discount>0?"<strong>$-"+(discount.toFixed(2))+"</strong>":"";
                            price_discount  = (price-discount).toFixed(2);
                            table.append('<tr data-type="product_sale" data-value="'+response.result.subsalid+'" class="'+response.result.subsalid+' info"><td class="text-center"><span class="deleteElement cursor_pointer" data-toggle="tooltip" data-placement="top" title="" data-original-title="Eliminar venta de producto"><i class="fa fa-remove fa-2x"></i></span></td><td class="description"><strong class="name_service">'+response.result.name_brand+'&nbsp;<span class="fa fa-info consumer-product" data-toggle="tooltip" data-placement="top" data-original-title="Venta de producto."></span></strong><p class="name_sub_service">'+response.result.name_product+' '+htmlDT+'</p></td><!--td class="add_stylist"><button class="btn btn-success">Agregar Vendedora</button></td--><td class="text-center price_service" data-value="'+price+'">$'+price.toFixed(2)+'</td><td class="text-center count_subtotal"><input type="number" min="1" max="10000" value="1" class="form-control comission" /><span>Pieza</span></td><td class="text-center subtotal_service" data-value="'+price+'"><p>$'+price.toFixed(2)+'</p></td><td class="text-center discount_service">'+htmlD+'</td><td class="text-center total_service" data-discount="'+discount+'" data-value="'+price_discount+'">$'+price_discount+'</td></tr>');
                            break;
                        default :
                            if(response.type!=="employed"){
                                if(response.result.identifier){
                                    $("#email").attr("data-value",response.result.email);
                                    $(".name_user_active").text(response.result.name+" "+response.result.last_name+" "+response.result.second_name);
                                }
                            }
                            break
                    }
                    /* subtotal_services = buscar la clase que contenga los subtotales de cada servicio, para recorrerlos con each y agregar el subtotal general */
                    calculo();
                    switch ($type){
                        case "service":$(".select_service").select2('val', '');
                            break;
                        case "product":$(".select_product").select2('val', '');
                            break;
                        case "product_sale":$(".select_product_sale").select2('val', '');
                            break;
                    }
                    return false;
                }
            },
            error : function(){
                messages(3);
            }
        });

    }).trigger('change');
    return false;
}
function deleteRow(selector,url){
    var box = $("#mb-remove-row");
    var id = "0";
    var type = "0";
    $(document.body).on("click","table tbody tr td span."+selector,function(){
        id = $(this).parent().parent().attr("data-value");
        type = $(this).parent().parent().attr("data-type");
        box.addClass("open");
    });
    $("div#mb-remove-row .mb-control-yes").on("click",function(){
        $("#status_delete").removeClass("in").addClass("hidden");
        $("#message_delete").removeClass("hidden").addClass("in");
        $.ajax({
            url : url,
            type : "POST",
            data : {id:id,type:type},
            dataType : "json",
            success : function(response){
                box.removeClass("open");
                if(response.code==200){
                    $("."+id).hide("slow",function(){
                        $(this).remove();
                        calculo();
                    });
                }else{
                    $("#message-box-danger-deleted").toggleClass("open");
                    setTimeout(function(){
                        $("#message-box-danger-deleted").removeClass("open");
                    },2000);
                }
            },error : function(){
                alert("Ha ocurrido un error intente nuevamente.");
            },complete:function(){
                $("#message_delete").removeClass("in").addClass("hidden");
                $("#status_delete").removeClass("hidden").addClass("in");
            }
        });
    });
}
function calculo(){
    value_iva = parseFloat(16);
    //Add total pay
    total_services = $(document.body).find("table#tableSales .total_service");
    var total = 0;
    total_services.each(function(){
        total = (parseFloat(total) + parseFloat($(this).attr("data-value")));
    });

    selector_iva = $("#iva");
    iva_general = (total/1.16)*.16;
    subtotal = (total-iva_general).toFixed(2);
    selector_iva.find(".iva_general").text("$"+iva_general.toFixed(2));

    $("#subtotal").text("$"+subtotal);

    /* Iva y Total a pagar */

    $("#amount_to_pay").text("$"+total.toFixed(2)).attr("data-value",total);
}
function messages($type){
    if($type==1){
        setTimeout(function(){
            $("#message-box-info").removeClass("open");
            $("#message-box-success").toggleClass("open");
        },1500);
        setTimeout(function(){
            $("#message-box-success").removeClass("open");
        },2000);
    }
    else if($type==2){
        setTimeout(function(){
            $("#message-box-info").removeClass("open");
            $("#message-box-warning").toggleClass("open");
        },1500);
        setTimeout(function(){
            $("#message-box-warning").removeClass("open");
        },3000);
    }else if($type==3){
        setTimeout(function(){
            $("#message-box-info").removeClass("open");
            $("#message-box-warning").toggleClass("open");
        },1500);
        setTimeout(function(){
            $("#message-box-warning").removeClass("open");
        },3000);
    }
    else if($type==4){
        setTimeout(function(){
            $("#message-box-process").removeClass("open");
            $("#message-box-pay").toggleClass("open");
        },1500);
        setTimeout(function(){
            $("#message-box-pay").removeClass("open");
        },3000);
    }
}

/*table.append("<tr class='success'><td>"+val.salid+"</td><td class='text-right'>$ "+val.total+"</td><td class='text-right'>"+discount+"</td><td>"+val.user+"</td><td>"+val.date+"</td><td>"+val.status+"</td><td><a onclick="+"printPage('"+val.url_print+"')"+" data-toggle='tooltip' data-placement='top' title='Imprimir comprobante de venta'><span class='fa fa-print fa-2x'></span></a>&nbsp;<a target='_blank' href='"+val.url_list_print+"' data-toggle='tooltip' data-placement='top' title='Ver comprobante de venta'><span class='fa fa-eye fa-2x'></span></a>&nbsp;<a target='_blank' href='"+val.url_view+"' data-toggle='tooltip' data-placement='top' title='Información general de venta'><span class='fa fa-folder-open fa-2x'></span></a></td></tr>");*/
/*table.append("<tr class='"+status_color+"'><td>"+val.salid+"</td><td class='text-right'>$ "+val.total+"</td><td class='text-right'>"+discount+"</td><td>"+val.user+"</td><td>"+val.date+"</td><td>"+status_message+"</td><td><a target='_blank' href='"+val.url_list_print+"' data-toggle='tooltip' data-placement='top' title='Editar venta'><span class='fa fa-edit fa-2x'></span></a></td></tr>");*/
/*table.append("<tr class='"+status_color+"'><td>"+val.salid+"</td><td class='text-right'>$ "+val.total+"</td><td class='text-right'>"+discount+"</td><td>"+val.user+"</td><td>"+val.date+"</td><td>"+status_message+"</td><td><a target='_blank' href='"+val.url_view+"' data-toggle='tooltip' data-placement='top' title='Información general de venta'><span class='fa fa-folder-open fa-2x'></span></a></td></tr>");*/