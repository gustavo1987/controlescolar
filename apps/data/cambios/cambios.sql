USE `cd_beauty_room`;
CREATE
     OR REPLACE ALGORITHM = UNDEFINED
    DEFINER = `root`@`localhost`
    SQL SECURITY DEFINER
VIEW `v_shopping_list` AS
    SELECT
        `cs`.`salid` AS `salid`,
        `css`.`subsalid` AS `subsalid`,
        `css`.`quantity` AS `quantity_ss`,
        `css`.`date_creation` AS `date_creation`,
        `css`.`commission` AS `commission`,
        `css`.`type` AS `type`,
        `css`.`status` AS `status`,
        `css`.`payout` AS `payout`,
        `css`.`uid` AS `uid_ss`,
        `css`.`price_general` AS `price_general`,
        `cu`.`uid` AS `uid_attend`,
        `cu`.`name` AS `name_attend`,
        `cu`.`last_name` AS `last_name_attend`,
        `cu`.`second_name` AS `second_name_attend`,
        `cds`.`subsid` AS `subsid`,
        `cdse`.`name` AS `name_service`,
        `cds`.`name` AS `name_subservice`,
        `cds`.`price` AS `price`,
        `cds`.`discount` AS `discount`,
        `cp`.`pid` AS `pid`,
        `cb`.`name` AS `name_brand`,
        `cp`.`name` AS `name_product`,
        `cp`.`sale_price` AS `sale_price`,
        `css`.`discount` AS `percentage`,
        `cp`.`ticket` AS `ticket`
    FROM
        ((((((`cd_sale` `cs`
        LEFT JOIN `cd_subservices_sale` `css` ON ((`cs`.`salid` = `css`.`salid`)))
        LEFT JOIN `cd_user` `cu` ON ((`cu`.`uid` = `css`.`uid`)))
        LEFT JOIN `cd_subservices` `cds` ON ((`css`.`subsid` = `cds`.`subsid`)))
        LEFT JOIN `cd_services` `cdse` ON ((`cds`.`servid` = `cdse`.`servid`)))
        LEFT JOIN `cd_product` `cp` ON ((`cp`.`pid` = `css`.`pid`)))
        LEFT JOIN `cd_brand` `cb` ON ((`cb`.`brid` = `cp`.`brid`)));
