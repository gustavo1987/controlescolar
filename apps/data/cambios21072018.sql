/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  gustavoperezjavier
 * Created: 21-jul-2018
 */

ALTER TABLE `cd_sale_client` ADD `asistencia` INT NULL AFTER `observacion`;


select `clid`.`clid` AS `clid`,`clid`.`identifier` AS `identifier`,`clid`.`name` AS `name`,`clid`.`last_name` AS `last_name`,`clid`.`second_name` AS `second_name`,`clid`.`sex` AS `sex`,`clid`.`birthdate` AS `birthdate`,`clid`.`email` AS `email`,`clid`.`tel` AS `tel`,`clid`.`institution` AS `institution`,`clid`.`discount` AS `discount`,`clid`.`status` AS `status`,`saclid`.`status` AS `statuscla`,`saclid`.`date_creation` AS `date_creationcla`,`saclid`.`calificacion` AS `calificacion`,`saclid`.`observacion` AS `observacion`,`saclid`.`asistencia` AS `asistencia`,`salid`.`salid` AS `salid` from ((`cd_controlescolar`.`cd_client` `clid` join `cd_controlescolar`.`cd_sale_client` `saclid` on(`clid`.`clid` = `saclid`.`clid`)) join `cd_controlescolar`.`cd_sale` `salid` on(`saclid`.`salid` = `salid`.`salid`))


SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

CREATE TABLE IF NOT EXISTS `cd_controlescolar`.`cd_work` (
  `workid` INT(11) NOT NULL AUTO_INCREMENT,
  `subsalid` INT(11) NOT NULL,
  `work` TEXT NOT NULL,
  `fechavencimiento` DATETIME NOT NULL,
  `status` VARCHAR(45) NOT NULL DEFAULT 'ACTIVO',
  INDEX `fk_table1_cd_subservices_sale1_idx` (`subsalid` ASC),
  PRIMARY KEY (`workid`),
  CONSTRAINT `fk_table1_cd_subservices_sale1`
    FOREIGN KEY (`subsalid`)
    REFERENCES `cd_controlescolar`.`cd_subservices_sale` (`subsalid`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;