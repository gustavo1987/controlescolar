<?php
namespace Modules\Dashboard\Plugins;
use \Phalcon\Events\Event,
    \Phalcon\Mvc\User\Plugin,
    \Phalcon\Mvc\Dispatcher,
    \Phalcon\Acl;
/**
 * Security
 *
 * This is the security plugin which controls that users only have access to the modules they're assigned to
 */
class Security extends Plugin {
    public function __construct($dependencyInjector){
        $this->_dependencyInjector = $dependencyInjector;
    }
    public function getAcl(){
        if(!isset($this->persistent->acl)){ /* update values here */
            $acl = new \Phalcon\Acl\Adapter\Memory();
            $acl->setDefaultAction(Acl::DENY);
            $roles = array(
                "GUEST" => new Acl\Role("GUEST"),
                "USER" => new Acl\Role("USER"),
                "STUDENT"=> new Acl\Role("STUDENT"),
                "TEACHER"=> new Acl\Role("TEACHER"),
                "COORDINATOR"=> new Acl\Role("COORDINATOR"),
                "ADMIN" => new Acl\Role("ADMIN"),
                "SUPERADMIN" => new Acl\Role("SUPERADMIN")
            );
            foreach($roles as $key => $role){
                switch($key){
                    case "GUEST" : $acl->addRole($role);
                        break;
                    case "USER" : $acl->addRole($role,$roles['GUEST']);
                        break;
                    case "STUDENT" : $acl->addRole($role,$roles['USER']);
                        break;
                    case "TEACHER" : $acl->addRole($role,$roles['STUDENT']);
                        break;
                    case "COORDINATOR" : $acl->addRole($role,$roles['TEACHER']);
                        break;
                    case "ADMIN" : $acl->addRole($role,$roles['COORDINATOR']);
                        break;
                    case "SUPERADMIN" : $acl->addRole($role,$roles['ADMIN']);
                        break;
                }
            }
            //Resources of admin (cms)
            $superadminResources = array(
                "reports"=>array("index","gethistory","exportgeneral","commission","getcommission","exportcommission"),
                "sales"=>array("createcupon","cupones","printcupon","deletecupon","cuponview"),
                "user"=>array('index',"profile","updateuser","updatepassword","updateuserimage","uploadimage","socialmedia","validateemail","validateusername","editnote"),

            );
            foreach($superadminResources as $resource => $actions){
                $acl->addResource(new \Phalcon\Acl\Resource($resource),$actions);
            };

            $adminResources = array(
                "index"=>array("index","index","new","slider","edit","uploadfile","save","update","uploadmultipleimages"),
                "config"=>array('index',"saveorder"),
                "teacher"=>array('index','inactive'),
                "sales"=>array("index","feed","getusers","cuponverificar","getclients","number","active","feedsubservices","updateelements","changestylist","delete","deleteabono","payservices","print","getsales","completed","completedapart","day","dayapart","endapart","cancel","exportgeneral","paypulledapart"),
                "user"=>array("deleteuser","newuser","index","saveuser","edit","inactive","percentage","savepercentage"),
                "sections"=>array("index","home","feedpost","updatesection","orderpostsections"),
                "services"=>array("index","save","update","delete","validate","permalink","subservices","savesubservice","permalinksubservice","deletesubservice"),
                "brands"=>array("index","save","update","delete","validate","permalink","postgrado"),
                "products"=>array("index","new","save","edit","update","delete","validate","permalink","inactive","get","stock","printp","export"),
                "mail"=>array("index","allsubscribers","edit","inactive","update","send","sendmail"),
                "client" =>array("admision","recibo","inscripcion","editadmision","editrecibo","convertirestudiante","admisioncreate","saveadmisioncreate"),
                "student" =>array("saveuser","new","edit","updateuser","all","todos","addpay","viewpay","savepay","deletepay","blockstudent","pos","updateuser","hiddenpaystudent"),
                "clases" =>array("index","new","edit","update","save","list","pos","deleteuserclass","modulonew","uploaddocumentcalendario","savemodulo","editmodulo","deletemodulo","uploaddocumentplan","viewmodule","catedras","newcatedra","editcatedra","savecatedra","updatecatedra","deletecatedra"),
                "notification" =>array("index","new","edit","update","save","delete"),
            );
            foreach($adminResources as $resource => $actions){
                $acl->addResource(new \Phalcon\Acl\Resource($resource),$actions);
            };
            $coordinatorResources = array(
                "client"=>array("emailaceptado22","index","new","delete","save","edit","inactive","update","validateemail","validaterfc","control","savecontrol","uploadfile","deletedfile","sendmail","validateemailedit","signedup","savescholarshipcontrol","getmunicipios","all","viewone"),
                "sales"=>array("open","close","clases","addstudent","list","saveaddstudent","updateaddstudent"),
                "products"=>array("index"),
                "controlescolar"=>array("index","pagos","gradonew","pos"),
                "maestro"=>array("index","viewclass"),
            );
            foreach($coordinatorResources as $resource => $actions){
                $acl->addResource(new \Phalcon\Acl\Resource($resource),$actions);
            };
            $teacherResources = array(
                "index"=>array("all"),
                "client"=>array("index","new","clasestudiante","work","aula","generacion","delete","save","edit","inactive","update","validateemail","validaterfc","control","savecontrol","uploadfile","deletedfile","sendmail","validateemailedit","signedup","savescholarshipcontrol","getmunicipios"),
                "sales"=>array("open","close","salestudentclase"),
                "products"=>array("index"),
                "student"=>array("allstudentclass","updatescorestudent"),
                "maestro"=>array("materias","estudiantes","estudiante","calificar","clase","aula","newwork","asistencia","newasistencia","saveasistencia","updateasistencia","uploadfile","savenote","uploadfilecalificacion"),
            );
            foreach($teacherResources as $resource => $actions){
                $acl->addResource(new \Phalcon\Acl\Resource($resource),$actions);
            };
            $studentResources = array(
                "index"=>array("calendar","comunidad","generarpago","uploaddocumentsrecibo","validaterecibo","loadmessage"),
                "client"=>array("index","new","delete","save","edit","inactive","update","validateemail","validaterfc","control","savecontrol","uploadfile","deletedfile","sendmail","validateemailedit","signedup","savescholarshipcontrol","getmunicipios"),
                "sales"=>array("open","close"),
                "student"=>array("index","materias","pagos","video","videos","materia","horario","aula","clase","calificaciones","asistencias","pagos","clasesanteriores","chat","calendario","sitios","servicios","notas","trabajos","getcalendar","planestudio","uploadfile","updatediferido","asistenciavivo"),
                "products"=>array("index"),
                "email"=>array("worksendstudent"),
                "chat"=>array("index","process","update","userlist"),
                "user"=>array('index',"profile","updateuser","updatepassword","updateuserimage","uploadimage","socialmedia","validateemail","validateusername","editnote"),
                "clases"=>array('plan'),
                "recibos"=>array("index","update","new"),
            );
            foreach($studentResources as $resource => $actions){
                $acl->addResource(new \Phalcon\Acl\Resource($resource),$actions);
            };
            $userResources = array(
                "index"=>array("index"),
            );
            foreach($userResources as $resource => $actions){
                $acl->addResource(new \Phalcon\Acl\Resource($resource),$actions);
            };
            $publicResources = array(
                "login"=>array('index',"logout","session")
            );
            foreach($publicResources as $resource => $actions){
                $acl->addResource(new \Phalcon\Acl\Resource($resource),$actions);
            }
            foreach($publicResources as $resource => $actions){
                foreach($actions as $action){
                    $acl->allow("GUEST",$resource,$action);
                }
            };
            foreach($userResources as $resource => $actions){
                foreach($actions as $action){
                    $acl->allow("USER",$resource,$action);
                    $acl->allow("STUDENT",$resource,$action);
                    $acl->allow("TEACHER",$resource,$action);
                    $acl->allow("COORDINATOR",$resource,$action);
                    $acl->allow("ADMIN",$resource,$action);
                    $acl->allow("SUPERADMIN",$resource,$action);
                    $acl->deny("USER","login","index");
                }
            };
            foreach($studentResources as $resource => $actions){
                foreach($actions as $action){
                    $acl->allow("STUDENT",$resource,$action);
                    $acl->allow("TEACHER",$resource,$action);
                    $acl->allow("COORDINATOR",$resource,$action);
                    $acl->allow("ADMIN",$resource,$action);
                    $acl->allow("SUPERADMIN",$resource,$action);
                    $acl->deny("STUDENT","login","index");
                }
            };
            foreach($teacherResources as $resource => $actions){
                foreach($actions as $action){
                    $acl->allow("TEACHER",$resource,$action);
                    $acl->allow("COORDINATOR",$resource,$action);
                    $acl->allow("ADMIN",$resource,$action);
                    $acl->allow("SUPERADMIN",$resource,$action);
                    $acl->deny("TEACHER","login","index");
                }
            };
            foreach($coordinatorResources as $resource => $actions){
                foreach($actions as $action){
                    $acl->allow("COORDINATOR",$resource,$action);
                    $acl->allow("ADMIN",$resource,$action);
                    $acl->allow("SUPERADMIN",$resource,$action);
                    $acl->deny("COORDINATOR","login","index");
                }
            };
            //Grant acess to adminResources area to role ADMIN
            foreach($adminResources as $resource => $actions){
                foreach($actions as $action){
                    $acl->allow("ADMIN",$resource,$action);
                    $acl->allow("SUPERADMIN",$resource,$action);
                    $acl->deny("ADMIN","login","index");
                }
            };
            //Grant acess to adminResources area to role ADMIN
            foreach($superadminResources as $resource => $actions){
                foreach($actions as $action){
                    $acl->allow("SUPERADMIN",$resource,$action);
                }
            };
            //The acl is stored in session, APC would be useful here too
            $this->persistent->acl = $acl;
        }
        return $this->persistent->acl;
    }

    /**
     * This action is executed before execute any action in the application
     */
    public function beforeDispatch(Event $event, Dispatcher $dispatcher)
    {
        $auth = $this->session->get('auth');
        switch($auth["rol"]){
            case "SUPERADMIN" : $role = 'SUPERADMIN';
                break;
            case "ADMIN" : $role = 'ADMIN';
                break;
            case "USER" : $role = 'USER';
                break;
            case "COORDINATOR" : $role = 'COORDINATOR';
                break;
            case "TEACHER" : $role = 'TEACHER';
                break;
            case "STUDENT" : $role = 'STUDENT';
                break;
            default : $role = 'GUEST';
            break;
        }
        $controller = $dispatcher->getControllerName();
        $action = $dispatcher->getActionName();
        $acl = $this->getAcl();
        $allowed = $acl->isAllowed($role, $controller, $action);
        if ($allowed != Acl::ALLOW) {
            $this->flash->error("You don't have access to this module");
            if($role==="GUEST"){
                $this->response->redirect("login");
            }
            else{
                $this->response->redirect("dashboard");
            }
            return false;
        }

    }
}
