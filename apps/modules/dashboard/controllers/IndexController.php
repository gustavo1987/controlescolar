<?php
namespace Modules\Dashboard\Controllers;
include dirname(dirname(dirname(dirname(__FILE__))))."/library/wideimage/WideImage.php";
//require dirname(dirname(dirname(dirname(__FILE__))))."/library/PHPMailer/PHPMailerAutoload.php";
use Modules\Models\CdSale;
use Modules\Models\CdSlider;
use Modules\Models\CdClient;
use Modules\Models\CdUser;
use Modules\Models\CdBrand;
use Modules\Models\CdPago;
use Modules\Models\CdNotification;
use Modules\Models\CdRecibo;
use Modules\Models\cms\CdStudent;
use Modules\Models\VProductBrand;
use Phalcon\Http\Request;

class IndexController extends ControllerBase
{
    public function indexAction(){
        $auth = $this->auth();
        $rol = $auth["rol"];
        $title = "Campus Virtual";
        $url ="/";
        $description = "Campus Virtual";
        $image = "dash/img/logo-100.png";
        $this->metaHome($title,$url,$description,$image);

        $this->backView("Bienvenido","/dashboard");
        if($auth){
            $this->dataTable();
            $this->scripts();
            $date = date("Y-m-d");
            $find = VProductBrand::find("status='ACTIVE' and stock<=stock_min order by pid desc");
            $sales = CdSale::find("date(date_creation)='$date' and status='PAY'");
            $pulledapart = CdSale::find("date(date_creation)='$date' and status='PULLEDAPART'");
            $pulledapartAll = CdSale::find("status='PULLEDAPART'");
            $sales_cancel = CdSale::find("date(date_creation)='$date' and status='CANCEL'");
            $notification = CdNotification::find("uid=".$auth["uid"]." and status_view='WAIT'");
            $user = CdUser::findFirst($auth['uid']);
            $this->view->setVar("rol",$rol);
            $this->view->setVar("products",$find);
            $this->view->setVar("sales",count($sales));
            $this->view->setVar("pulledapart",count($pulledapart));
            $this->view->setVar("pulledapartAll",count($pulledapartAll));
            $this->view->setVar("sales_cancel",count($sales_cancel));
            $this->view->setVar("notification",count($notification));
            $this->view->setVar("user",$user);
        }else{
            $this->response(array("code"=>404,"message"=>"You do not have permission"),404);
        }
    }
    public function calendarAction(){
        $auth = $this->auth();
        $title = "CALENDARIO";
        $url ="/";
        $description = "IDE Iberoamerica";
        $image = "dash/img/logo-100.png";
        $this->metaHome($title,$url,$description,$image);
        $this->backView("Campus Virtual","/dashboard");
        $student = CdClient::findFirst("email='".$auth["email"]."'");
        if($student->getCarrera()==17){ 
                $modulo="Cuatrimestre";
        }
        elseif($student->getCarrera()==18){
            $modulo="Cuatrimestre";
        }
        elseif($student->getCarrera()==19){
            $modulo="Semestre";
        }elseif($student->getCarrera()==20){
            $modulo="Semestre";
        }
        $this->view->setVar("modulo",$modulo);
    }
    public function comunidadAction(){

        $title = "COMUNIDAD UNIVERSITARIA";
        $url ="/";
        $description = "IDE Iberoamerica";
        $image = "dash/img/logo-100.png";
        $this->metaHome($title,$url,$description,$image);
        $this->backView("Campus Virtual","/dashboard");
    }
    public function allAction(){
    }
    public function uploadMultipleImagesAction(){
        $request = new Request();
        $auth = $this->auth();
        if($auth){
            if($request->hasFiles()==true){
                foreach($request->getUploadedFiles() as $file){
                    $image_replace = preg_replace('/[^A-Za-z0-9áéíóúÁÉÍÓÚñÑ\_\.!¡¿?]/', '',$file->getName());
                    $new_image = uniqid()."_".$image_replace;
                    if($file->moveTo($this->public."dash/img/mail/".$new_image)){
                        $url = $this->url->getBaseUri()."dash/img/mail/"."$new_image";
                        $funcNum = $_GET['CKEditorFuncNum'] ;
                        echo "<script type='text/javascript'>window.parent.CKEDITOR.tools.callFunction('".$funcNum."','".$url."', 'Guardado correctamente');</script>";
                        exit();
                        //$this->response(array("name"=>$new_image,"message"=>"SUCCESS","code"=>"200"),200);
                    }
                    else{
                        $this->response(array("name"=>$new_image,"message"=>"error try again","code"=>"404"),200);
                    }
                }
            }
        }else{
            exit();
        }
    }
    public function newAction(){
        $auth = $this->auth();
        if($auth){
            $this->view->setVar("image",uniqid()."-".date("Y").".png");
        }else{
            $this->response(array("message"=>"error"),404);
        }
    }
    public function uploadFileAction(){
        $request = $this->request;
        $auth = $this->auth();
        if($request->isPost() && $request->isAjax() && $auth){
            if($request->hasFiles()==true){
                foreach($request->getUploadedFiles() as $file){
                    $new_file = $request->getPost("name-image");
                    if($file->moveTo($this->public."front/src/images/slider/".$new_file)){
                        imagepng(imagecreatefromstring(file_get_contents($this->public."front/src/images/slider/".$new_file)),$this->public."front/src/images/slider/".$new_file);
                        $this->response(array("name"=>$new_file,"message"=>"SUCCESS","code"=>"200","type"=>"image"),200);
                    }
                }
            }
        }else{
            $this->response(array("message"=>"Error 404, try again."),404);
        }
    }


    /*Pago via paypal*/

    public function generarpagoAction(){
        $auth = $this->auth();
        
        //print_r("hola");exit;
        $request = new Request();
        $status = "";
        if($request->isAjax()){

            $email = $auth["email"];
            //$client = CdStudent::findFirst("email='$email'");
            $client = CdClient::findFirst("email='$email'");
            
            //$carrera = CdPost::findFirst($inscripcion->getEstudio());
            $carrera = CdBrand::findFirst($client->getCarrera());
            if($request->getPost("pago")==1){
                if($request->getPost("valor")=="reinscripcion"){
                    $costocobrar = $carrera->getReinscripcion();
                }
                elseif($request->getPost("valor")=="mensualidad"){
                    $costocobrar = $carrera->getMensualidad();
                }
            }
            elseif($request->getPost("pago")==2){
                    $costocobrar = $carrera->getReinscripcion()+$carrera->getMensualidad();
            }
            if($client->getBeca()!=null){
                $descuento = $costocobrar*$client->getBeca();
                $inscripcionnum = $costocobrar-$descuento;
                $comision = ($inscripcionnum*0.04)+4;
                $total2 = $comision+$inscripcionnum;
                $total = number_format($total2, 2, ".","");
            }
            /*elseif($mes==10){
                $descuento = $costocobrar*0.50;
                $inscripcionnum = $costocobrar-$descuento;
                $comision = ($inscripcionnum*0.04)+4;
                $total2 = $comision+$inscripcionnum;
                $total = number_format($total2, 2, ".","");
            }
            elseif($mes==11){
                $descuento = $costocobrar*0.60;
                $inscripcionnum = $costocobrar-$descuento;
                $comision = ($inscripcionnum*0.04)+4;
                $total2 = $comision+$inscripcionnum;
                $total = number_format($total2, 2, ".","");

            }*/
            else{
                $inscripcionnum = $costocobrar;
                $comision = ($inscripcionnum*0.04)+4;
                $total2 = $comision+$inscripcionnum;
                $total = number_format($total2, 2, ".","");
            }
            $user = CdUser::findFirst($client->getUid());
            /*$comision = $carrera->getPrecioInscripcion()*0.0395;
            $total = $comision+$carrera->getPrecioInscripcion()+4;*/
            // if(!$inscripcion)$this->response(array("message"=>"No existe ningun proceso de inscripción","code"=>404),404);
            if($user->getStatus()=="ACTIVE"){
                $proceso = "SI";
                $buy = (double)$total;
                $item_number = 1;
                $payer_email = $client->getEmail();
                $custom = $client->getUid();
                $return_url = $this->url->getBaseUri().'pagos/pagado?entrada='.$buy.'&id='.$custom;
                $cancel_url = $this->url->getBaseUri().'pagos/cancelado?id='.$custom;
                $notify_url = $this->url->getBaseUri().'paypal';
                $paypal_email = "ideiberoamerica2018@gmail.com";
                $item_name = "Inscripcion";
                $item_amount = $buy;
                $querystring = '';
                $querystring .= "?business=".urlencode($paypal_email)."&";
                $querystring .= "item_name=".urlencode($item_name)."&";
                $querystring .= "amount=".urlencode($item_amount)."&";
                $querystring .= "no_note=".urlencode(1)."&";
                $querystring .= "cmd=".urlencode("_xclick")."&";
                $querystring .= "lc=".urlencode("MX")."&";
                $querystring .= "currency_code=".urlencode("MXN")."&";
                $querystring .= "bn=".urlencode("PP-BuyNowBF:btn_buynow_LG.gif:NonHostedGuest")."&";
                $querystring .= "item_number=".urlencode($custom)."&";
                $querystring .= "payer_email=".urlencode($payer_email)."&";
                $querystring .= "charset=".urlencode("utf-8")."&";
                $querystring .= "custom=".urlencode($custom)."&";
                foreach($_POST as $key => $value){
                    $value = urlencode(stripslashes($value));
                    $querystring .= "$key=$value&";
                }
                $querystring .= "return=".urlencode(stripslashes($return_url))."&";
                $querystring .= "cancel_return=".urlencode(stripslashes($cancel_url))."&";
                $querystring .= "notify_url=".urlencode($notify_url);
                $url = "https://www.paypal.com/cgi-bin/webscr".$querystring;
    //                header('location:https://www.sandbox.paypal.com/cgi-bin/webscr'.$querystring);
    //                exit();
                $this->view->setVar("url",$url);
                $this->response(array("valid"=>true,"url"=>$url,"message"=>"correcto","code"=>200),200);
            }
            else{
                $proceso = "NO";
                $this->response(array("valid"=>false,"message"=>"correcto","code"=>200),200);
            } 
            /*$student = CdStudent::findFirst("email='".$email."'");
            if($student){
                $inscripcion = CdInscripciones::findFirst("stuid=".$student);
                if($inscripcion->getIsOver()==false)$status="no";
                else $status="si";
                print_r("4");

                $this->response(array("status"=>$status,"code"=>200,"message"=>"data-saved"),200);
            }
            else{
                $this->response(array("message"=>"Estudiante no existe"),404);
            }*/
        }
        else{
            $this->response(array("message"=>"error","code"=>404),404);
        }
    }
    /*Cargar recibo del alumno en servicios educativos*/
    public function validatereciboAction(){
        $request = new Request();
        if(!($request->isPost() and $request->isAjax()))$this->response(array("message"=>"error"),404);
        $email = $request->getPost("email");
        $status = CdStudent::findFirst("email='".$email."'");
        if($status->getStatus()=="ADMITIDO"){
            $this->response(array("code"=>200,"message"=>"data-saved"),200);
        }
        elseif($status->getStatus()=="RECIBO"){
            $this->response(array("code"=>500,"message"=>"data-saved"),200);
        }
        else{
            $this->response(array("code"=>404,"message"=>"data-error"),200);
        }
    }
    /*Cargar documentos en sección de admisión para alumno en servicios educativos*/
    public function uploaddocumentsreciboAction(){
        $request = new Request();
        if(!($request->isPost() and $request->isAjax()))$this->response(array("message"=>"error"),404);
        if($request->hasFiles()==true){
            foreach($request->getUploadedFiles() as $file){
                $type = explode("/",$file->getType());
                $image_replace = $this->url_clean($file->getName());
                $new_image = uniqid()."_".$image_replace;
                if($file->moveTo(dirname(dirname(dirname(dirname(__DIR__))))."/public/documents/".$request->getPost("type")."/".$new_image)){
                    $this->response(array("message"=>"correcto","document"=>$new_image),200);
                }else{
                    $this->response(array("message"=>"error try again","code"=>"404"),200);
                }
            }
        }
        $this->response(array("message"=>"correcto"));
    }

    private function sendEmailRecibo($values){
        $email = new \PHPMailer();
        $email->isSMTP();
        $email->Host = "smtp.gmail.com";
        $email->Port=587;
        $email->CharSet = 'UTF-8';
        $email->SMTPSecure="tls";
        $email->SMTPAuth =true;
        $email->Username = "enviodecorreoscdevelopers@gmail.com";
        $email->Password = "cdevelopers2018";
        $email->setFrom("contactanos@ideiberoamerica.com",'IDE Iberoamerica');
        $email->addReplyTo("contactanos@ideiberoamerica.com",'IDE Iberoamerica');
        $email->addAddress("serviciosadministrativos@ideiberoamerica.com");
        $email->WordWrap =100;
        $email->isHTML(true);
        $email->Subject = "Trabajo cargado";
        $file = dirname(__DIR__)."/views/email/loadrecibo.html";
        $file = str_replace("*|recibo_student|*", $value, $file);
        $email->msgHTML(file_get_contents($file));
        $email->AltBody = "Instituto Iberoamericano de Derecho";
        
        if(!$email->send()) {
            echo 'Message could not be sent.';
            echo 'Mailer Error: ' . $email->ErrorInfo;
        } else {
            return true;
        }
    }
    public function loadmessageAction(){
        $auth = $this->auth();
        if($auth){
            $now = date("Y-m-d h:i:s");
            $notification = CdNotification::find("status_view='WAIT' and date<='".$now."' and uid=".$auth['uid']." order by date asc limit 1");
            if($notification){
                foreach ($notification as $key => $value) {
                $value->setStatusView('VIEWED')
                            ->update();
                }
                $this->response(array("code"=>200,"message"=>"SUCCESS","result"=>$notification,"leght"=>count($notification)),200);
            }
            

        }else{
            $this->response(array("code"=>404,"message"=>"You do not have permission"),404);
        }
    }
    private function scripts(){
        $this->assets->collection('jsPlugins')
            ->setTargetPath("dash/js/general.products.min.js")
            ->setTargetUri("dash/js/general.products.min.js")
            ->addJs("dash/js/products/products.js")
            ->join(true)
            ->addFilter(new \Phalcon\Assets\Filters\Jsmin());
    }
}