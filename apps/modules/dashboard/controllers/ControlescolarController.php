<?php
namespace Modules\Dashboard\Controllers;
use Modules\Models\CdClient;
use Modules\Models\CdUser;
use Modules\Models\CdPago;
use Modules\Models\CdBrand;
use Modules\Models\VClientUser;
use Phalcon\Http\Request;

class ControlescolarController extends ControllerBase{
    public function indexAction(){
    }
    public function gradonewAction(){
        $this->scripts();
    }
    public function posAction(){
        $auth = $this->auth();
        $this->backView("Campus Virtual","/dashboard");
        if($auth){
            $posgrados = CdBrand::find("status='ACTIVE'");
            $this->view->setVar("posgrados",$posgrados);
        }else{
            $this->response(array("code"=>404,"message"=>"You do not have permission"),404);
        }

    }
    public function pagosAction(){
    	$auth = $this->auth();
        $title = "Pagos";
        $this->scripts();
        $url ="/";
        $description = "IDE Iberoamerica";
        $image = "dash/img/logo-100.png";
        $this->metaHome($title,$url,$description,$image);
        $this->backView("Posgrados","/dashboard/controlescolar/pos");
        $client = CdClient::findFirst("uid=".$auth['uid']);
        $pagosrealizados = CdPago::find("status='PAGADO' and clid=".$client->getClid());
        $pagospendiente = CdPago::findFirst("status='ACTIVO' and clid=".$client->getClid());
        if($auth['rol']=="ADMIN"){
            $id = $this->dispatcher->getParam("id");
            $student = VClientUser::find("status='STUDENT' and carrera=".$id);
            $this->view->setVar("student",$student);
        }
        $this->view->setVar("emailuser",$auth['uid']);
        $this->view->setVar("pagospendiente",$pagospendiente);
        $this->view->setVar("pagosrealizados",$pagosrealizados);
    }
    private function scripts(){
        $this->assets->collection('jsPlugins')
            ->setTargetPath("dash/js/general.controlescolar.min.js")
            ->setTargetUri("dash/js/general.controlescolar.min.js")
            ->addJs("dash/js/plugins/BootstrapDP/bootstrap-datepicker.min.js")
            ->addJs("dash/js/plugins/BootstrapDP/bootstrap-datepicker.es.min.js")
            ->addJs("dash/js/controlescolar/controlescolar.js")
            ->join(true)
            ->addFilter(new \Phalcon\Assets\Filters\Jsmin());
    }
}