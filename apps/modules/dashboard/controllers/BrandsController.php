<?php
namespace Modules\Dashboard\Controllers;
use Modules\Models\CdBrand;
use Modules\Models\VShoppingList;
use Modules\Models\VProductBrand;

class BrandsController extends ControllerBase{
    /* Actions Services */
    public function indexAction(){
        $auth = $this->auth();
        if($auth){

            $post = $this->dispatcher->getParam("id");
            $this->validationJs();
            $this->scripts();
            $brands = CdBrand::find("postgrado=".$post." and status='ACTIVE'");
            //$brands  = CdBrand::find(array("order"=>"brid asc"));
            $this->view->setVar("brands",$brands);
        }else{
            $this->response(array("code"=>404,"message"=>"You do not have permission"),404);
        }
    }
    public function postgradoAction(){
        $auth = $this->auth();
        if($auth){
            $post = $this->dispatcher->getParam("id");
            $this->validationJs();
            $this->scripts();
            $postgrado = VShoppingList::findFirst("brid=".$post);
            $primero = VProductBrand::find("brid=".$post." and stock_min=1 and status='ACTIVE'");
            $segundo = VProductBrand::find("brid=".$post." and stock_min=2 and status='ACTIVE'");
            $tercero = VProductBrand::find("brid=".$post." and stock_min=3 and status='ACTIVE'");
            $cuarto = VProductBrand::find("brid=".$post." and stock_min=4 and status='ACTIVE'");
            $quinto = VProductBrand::find("brid=".$post." and stock_min=5 and status='ACTIVE'");

            $brands = VShoppingList::find("brid=".$post." and statussalid='PAY' and status='ACTIVO'");
            //$brands  = CdBrand::find(array("order"=>"brid asc"));
            $this->view->setVar("brands",$brands);
            $this->view->setVar("primero",$primero);
            $this->view->setVar("segundo",$segundo);
            $this->view->setVar("tercero",$tercero);
            $this->view->setVar("cuarto",$cuarto);
            $this->view->setVar("quinto",$quinto);
            $this->view->setVar("postgrado",$postgrado);
        }else{
            $this->response(array("code"=>404,"message"=>"You do not have permission"),404);
        }
    }
    public function saveAction(){
        $request = $this->request;
        $auth = $this->auth();
        if($request->isPost() && $request->isAjax() && $auth ){
            $values = $request->getPost();
            if($values['brid']){
                $find = CdBrand::findFirst($values['brid']);
                $find->setName($values['name'])
                    ->setPermalink($values['permalink'])
                    ->setDateCreation(date("Y-m-d H:i:s"))
                    ->setUid($auth['uid']);
                if(!$find->update())$this->response(array("code"=>404,"message"=>"Error to update cd_services"),200);
                $type = 2;
            }else{
                $find = new CdBrand();
                $find->setName($values['name'])
                    ->setPermalink($values['permalink'])
                    ->setDateCreation(date("Y-m-d H:i:s"))
                    ->setStatus("ACTIVE")
                    ->setUid($auth['uid']);
                if(!$find->save())$this->response(array("code"=>404,"message"=>"Error to save cd_brand"),200);
                $type = 1;
            }
            $this->getBrandsJson(2);
            $this->response(array("code"=>200,"message"=>"SUCCESS","type"=>$type,"result"=>array("id"=>$find->getBrid(),"name"=>$find->getName(),"permalink"=>$find->getPermalink())),200);
        }else{
            $this->response(array("code"=>404,"message"=>"You do not have permission"),404);
        }
    }
    public function deleteAction(){
        $request = $this->request;
        $auth = $this->auth();
        if($request->isPost() && $request->isAjax() && $auth ){
            $id = $request->getPost("id");
            $find = CdBrand::findFirst($id);
            try{
                if($find->delete()){
                    $this->getBrandsJson(2);
                    $this->response(array("message"=>"SUCCESS","code"=>"200"),200);
                }
            }catch (\Exception $e){
                $this->response(array("message"=>"SUCCESS","code"=>"300"),200);
            }
        }else{
            $this->response(array("code"=>404,"message"=>"You do not have permission"),404);
        }
    }
    public function permalinkAction(){
        $request = $this->request;
        $auth = $this->auth();
        if($request->isPost() && $request->isAjax() && $auth ){
            $post = new CdBrand();
            $name = $request->getPost("name");
            $new_url = $this->url_clean($name);
            $check_url = $post->find("permalink = '$new_url'");
            $count = 1;
            while(count($check_url)){
                $generate_url = $new_url."-".$count;
                $check_url = $post->find("permalink = '$generate_url'");
                if(count($check_url)==0){
                    $new_url = $generate_url;
                }
                $count++;
            }
            $this->response(array("message"=>"SUCCESS","permalink"=>$new_url,"code"=>"200","data"=>"url generated"),200);
        }else{
            $this->response(array("code"=>404,"message"=>"You do not have permission"),404);
        }
    }
    /* End Actions Services */


    /* Other functions  */
    private function scripts(){
        $this->assets->collection('jsPlugins')
            ->setTargetPath("dash/js/general.brands.min.js")
            ->setTargetUri("dash/js/general.brands.min.js")
            ->addJs("dash/js/brands/brands.js")
            ->join(true)
            ->addFilter(new \Phalcon\Assets\Filters\Jsmin());
    }
}