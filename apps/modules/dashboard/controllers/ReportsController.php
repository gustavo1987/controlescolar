<?php
namespace Modules\Dashboard\Controllers;
require dirname(dirname(dirname(dirname(__FILE__))))."/library/Classes/PHPExcel.php";
use Modules\Models\CdUser;
use Modules\Models\VCommission;
use Modules\Models\VShoppingList;
use Phalcon\Http\Request;

class ReportsController extends ControllerBase{

    public function indexAction(){
        $auth = $this->auth();
        if($auth){
            $this->dataTable();
            $this->scripts();
            $user = CdUser::find("status='ACTIVE'");
            $this->view->setVar("users",$user);
        }else{
            $this->response(array("code"=>404,"message"=>"You do not have permission"),404);
        }
    }
    public function getHistoryAction(){
        $auth = $this->auth();
        $request = new Request();
        if($auth && $request->getPost() && $request->isAjax()){
            $values = $request->getPost();
            $date_init = $values['start_date']?$this->getFormatDate($values['start_date']):null;
            $date_finish = $values['cutoff_date']?$this->getFormatDate($values['cutoff_date']):null;


            $user = $values['user']=="general"?null:" and uid_ss={$values['user']}";
            $start_date = $values['start_date']?" date(date_creation)>='$date_init'":null;
            $cutoff_date = $values['cutoff_date']?" date(date_creation)<='$date_finish'":null;
            $status = $values['status']=="TODAS"?" and (type='SERVICIO' or type='VENTAS' or type='CONSUMO')":" and status='{$values['status']}'";
            $statusSalid = " and (statussalid='PAY')";
            if($values['start_date']){
                $cutoff_date = $values['cutoff_date']?" and date(date_creation)<='$date_finish'":null;
                $status = $values['status']=="TODAS"?" and (type='SERVICIO' or type='VENTAS' or type='CONSUMO')":" and status='{$values['status']}'";
            }
            if($values['cutoff_date']) $status = $values['status']=="TODAS"?" and (type='SERVICIO' or type='VENTAS' or type='CONSUMO')":" and status='{$values['status']}'";

            $query = $start_date.$cutoff_date.$status.$user.$statusSalid;
            $cd_sale = VShoppingList::find("$query");
            if(count($cd_sale)>=1){
                $content = array();
                $total = 0;
                $total_secondary = 0;
                foreach($cd_sale as $key => $values){
                    $count = $key+1;
                    if($values->getStatus()=="ACTIVO" and ($values->getType()=="SERVICIO" or $values->getType()=="VENTAS")){
                        $total+=(double)$values->getPayout();
                        $total_secondary+=(double)$values->getPriceGeneral();
                    }
                    $subtotal = $values->getType()=="SERVICIO"?$values->getQuantitySs()*$values->getPrice():$values->getQuantitySs()*$values->getSalePrice();
                    $discount = ($subtotal*$values->getDiscount())/100;
                    $content[$count] = array(
                        "date"=>date("d-m-Y H:i",strtotime($values->getDateCreation())),
                        "name_service"=>$values->getNameService(),
                        "name_sub_service"=>$values->getNameSubservice(),
                        "percentage_discount"=>$values->getDiscount(),
                        "attend"=>$values->getNameAttend()." ".$values->getLastNameAttend(),
                        "price"=>number_format($values->getPrice(),"2",".",","),
                        "type"=>$values->getType(),
                        "status"=>$values->getStatus(),
                        "quantity"=>$values->getQuantitySs(),
                        "subtotal"=>number_format($subtotal,"2",".",','),
                        "discount"=>number_format($discount,"2",".",',')."<br>({$values->getDiscount()}%)",
                        "total"=>number_format($subtotal-$discount,"2",".",',')
                    );
                    if($values->getType()=="VENTAS"){
                        $discount = ($subtotal*$values->getPercentage())/100;
                        $content[$count]['price'] = number_format($values->getSalePrice(),"2",".",",");
                        $content[$count]['name_service'] = $values->getNameBrand();
                        $content[$count]['name_sub_service'] = $values->getNameProduct();
                        $content[$count]['percentage_discount'] = $values->getPercentage();
                        $content[$count]['discount'] =number_format($discount,"2",".",',')."<br>({$values->getPercentage()}%)";
                        $content[$count]['total'] = number_format($subtotal-$discount,"2",".",',');
                    }else if($values->getType()=="CONSUMO"){
                        $content[$count]['name_service'] = $values->getNameBrand();
                        $content[$count]['name_sub_service'] = $values->getNameProduct();
                        $content[$count]['price'] = "0";
                        $content[$count]['percentage_discount'] = "0";
                        $content[$count]['discount'] = "0";
                        $content[$count]['total'] = "0";
                    }
                }
                $breakdown = array("iva_general"=>0,"subtotal"=>0,"total"=>0);
                if($total>0){
                    $iva = 16;
                    $iva_general = ($total * $iva)/100;
                    $subtotal = $total;
                    $antes = $subtotal / 1.16;
                    $ivareal = $subtotal-$antes;
                    //$breakdown = array("iva_general"=>number_format($iva_general,'2','.',','),"subtotal"=>number_format($subtotal,'2','.',','),"total"=>number_format($iva_general+$subtotal,'2','.',','));
                    $breakdown = array("iva_general"=>number_format($ivareal,'2','.',','),"subtotal"=>number_format($antes,'2','.',','),"total"=>number_format($subtotal,'2','.',','));
                }
                $this->response(array("code"=>200,"message"=>"success","result"=>$content,"breakdown"=>$breakdown),200);
            }
            $this->response(array("code"=>300,"message"=>"O values"),200);
        }else{
            $this->response(array("code"=>404,"messag
e"=>"You do not have permission"),404);
        }
    }
    public function commissionAction(){
        $auth = $this->auth();
        if($auth){
            $this->dataTable();
            $this->scripts();
            $user = CdUser::find("status='ACTIVE'");
            $this->view->setVar("users",$user);
        }else{
            $this->response(array("code"=>404,"message"=>"You do not have permission"),404);
        }
    }
    public function getCommissionAction(){
        $auth = $this->auth();
        $request = new Request();
        if($auth && $request->getPost() && $request->isAjax()){
            $values = $request->getPost();
            $date_init = $values['start_date']?$this->getFormatDate($values['start_date']):null;
            $date_finish = $values['cutoff_date']?$this->getFormatDate($values['cutoff_date']):null;

            $user = $values['user']=="general"?null:" and uid={$values['user']}";
            $start_date = $values['start_date']?" date(date_creation)>='$date_init'":null;
            $cutoff_date = $values['cutoff_date']?" date(date_creation)<='$date_finish'":null;
            $status = $values['status']=="TODAS"?" (type='SERVICIO' or type='VENTAS')":" type='{$values['status']}'";
            if($values['start_date']){
                $cutoff_date = $values['cutoff_date']?" and date(date_creation)<='$date_finish'":null;
                $status = $values['status']=="TODAS"?" and (type='SERVICIO' or type='VENTAS')":" and type='{$values['status']}'";
            }
            if($values['cutoff_date']) $status = $values['status']=="TODAS"?" and (type='SERVICIO' or type='VENTAS')":" and type='{$values['status']}'";

            $query = $start_date.$cutoff_date.$status.$user." and status='ACTIVO'";
            //print_r($query);exit();
            $cd_sale = VCommission::find("$query");
            if(count($cd_sale)>=1){
                $content = array();
                foreach($cd_sale as $key => $values){
                    $uid = $values->getUid();
                    $type=$values->getType();
                    $content[$uid][$type]["date"]=date("d-m-Y H:i",strtotime($values->getDateCreation()));
                    $content[$uid][$type]['type']=$values->getType();
                    $content[$uid][$type]['quantity']+=(int)$values->getQuantitySs();
                    $content[$uid][$type]['commission']+=(int)$values->getCommission();
                    $content[$uid][$type]['attend']=$values->getNameAttend()." ".$values->getLastNameAttend()." ".$values->getSecondNameAttend();
                    $content[$uid][$type]['status']=$values->getStatus();
                }
                $this->response(array("code"=>200,"message"=>"success","result"=>$content),200);
            }
            $this->response(array("code"=>300,"message"=>"O values"),200);
        }else{
            $this->response(array("code"=>404,"message"=>"You do not have permission"),404);
        }
    }
/* Export Commission users */
    public function exportCommissionAction(){
        $auth = $this->auth();
        $values = $this->request->get();

        if($auth && $values){

            $user = $this->request->get("user");
            $start_date = $values['start_date']?$this->getFormatDate($values['start_date']):null;
            $cutoff_date = $values['cutoff_date']?$this->getFormatDate($values['cutoff_date']):null;
            $status = $values["status"];

            $objPHPExcel = new \PHPExcel();
            $objPHPExcel->getProperties()->setCreator($auth['name'])
                ->setLastModifiedBy($auth['email'])
                ->setTitle("Reporte de comisiones Beauty Room")
                ->setSubject("Reporte de comisiones")
                ->setDescription("eporte de comisiones")
                ->setKeywords("XLS Microsoft")
                ->setCategory("Estetica");
            $this->xlsCommission($user,$start_date,$cutoff_date,$status,$objPHPExcel,$values);
        }else{
            exit();
        }
    }
    private function xlsCommission($type_user,$date_init,$date_finish,$type_status,$objectXls,$val){
        $cont = 0;
        $objectXls->setActiveSheetIndex(0)
            ->setCellValue('A2', 'No.')
            ->setCellValue('B2', 'Tipo')
            ->setCellValue('C2', 'Fecha inicial')
            ->setCellValue('D2', 'Fecha de corte')
            ->setCellValue('E2', 'Nombre estilista')
            ->setCellValue('F2', 'Cantidad')
            ->setCellValue('G2', 'Comisión ganada');

        $user = $type_user=="general"?null:" and uid=$type_user";
        $start_date = $date_init?" date(date_creation)>='$date_init'":null;
        $cutoff_date = $date_finish?" date(date_creation)<='$date_finish'":null;
        $status = $type_status=="TODAS"?" (type='SERVICIO' or type='VENTAS')":" type='$type_status'";

        if($date_init){
            $cutoff_date = $date_finish?" and date(date_creation)<='$date_finish'":null;
            $status = $type_status=="TODAS"?" and (type='SERVICIO' or type='VENTAS')":" and type='$type_status'";
        }
        if($date_finish) $status = $type_status=="TODAS"?" and (type='SERVICIO' or type='VENTAS')":" and type='$type_status'";

        $query = $start_date.$cutoff_date.$status.$user." and status='ACTIVO'";
        //print_r($query);exit();
        $cd_sale = VCommission::find("$query");

        if(count($cd_sale)>=1){
            $content = array();
            foreach($cd_sale as $key => $values){
                $uid = $values->getUid();
                $type=$values->getType();
                $content[$uid][$type]["date"]=date("d-m-Y H:i",strtotime($values->getDateCreation()));
                $content[$uid][$type]['type']=$values->getType();
                $content[$uid][$type]['quantity']+=(int)$values->getQuantitySs();
                $content[$uid][$type]['commission']+=(int)$values->getCommission();
                $content[$uid][$type]['attend']=$values->getNameAttend()." ".$values->getLastNameAttend()." ".$values->getSecondNameAttend();
                $content[$uid][$type]['status']=$values->getStatus();
            }
            $cont=2;
            foreach($content as $key => $value){
                foreach($value as $j => $vl){
                    $cont+=1;
                    $objectXls->setActiveSheetIndex(0)
                        ->setCellValue("A".$cont, $key)
                        ->setCellValue("B".$cont, $j)
                        ->setCellValue("C".$cont, $val['start_date'])
                        ->setCellValue("D".$cont, $val['cutoff_date'])
                        ->setCellValue("E".$cont, $vl['attend'])
                        ->setCellValue("F".$cont, $vl['quantity'])
                        ->setCellValue("G".$cont, $vl['commission']);
                }
            }
        }else{
            $cont = 3;
            $objectXls->setActiveSheetIndex(0)
                ->setCellValue("A3".$cont, "Sin resultados")
                ->setCellValue("B3".$cont, "")
                ->setCellValue("C3".$cont, "")
                ->setCellValue("D3".$cont, "")
                ->setCellValue("E3".$cont, "")
                ->setCellValue("F3"."")->setCellValue("G3"."");
        }

        $this->autoWidthCommission($objectXls,$cont);
        $objectXls->getActiveSheet()->setTitle("Ventas Beauty Room");
        $objectXls->setActiveSheetIndex(0);
        $objWriter = \PHPExcel_IOFactory::createWriter($objectXls,'Excel2007');
        $xls = date('m-d-Y_H.i.s')."_$type_user.xlsx";
        $objWriter->save(dirname(dirname(dirname(dirname(__DIR__))))."/public/reports/".$xls);
        $this->download($xls);
    }

    private function autoWidthCommission($objectXls,$cont){
        $this->style($objectXls,"A2:G2");
        $this->logo($objectXls,"A1:G1");
        $this->styleContent($objectXls,"A3:G$cont");
        $letter = array("A","B","C","D","E","F","G");
        foreach($letter as $values){
            $objectXls->getActiveSheet()->getColumnDimension("$values")->setAutoSize(true);
        }
    }
/* End Export Commission users */

/* Export History Sales */
    public function exportGeneralAction(){
        $auth = $this->auth();
        $user = $this->request->get("user");
        $start_date = $this->request->get("start_date")?$this->getFormatDate($this->request->get("start_date")):null;
        $cutoff_date = $this->request->get("cutoff_date")?$this->getFormatDate($this->request->get("cutoff_date")):null;
        $status = $this->request->get("status");
        if($auth && $status){
            $objPHPExcel = new \PHPExcel();
            $objPHPExcel->getProperties()->setCreator($auth['name'])
                ->setLastModifiedBy($auth['email'])
                ->setTitle("Reporte de ventas Beauty Room")
                ->setSubject("Información de control de ventas")
                ->setDescription("Información de venta")
                ->setKeywords("XLS Microsoft")
                ->setCategory("Estetica");
            $this->xlsGeneral($user,$start_date,$cutoff_date,$status,$objPHPExcel);
        }else{
            exit();
        }
    }
    private function xlsGeneral($type_user,$date_init,$date_finish,$type_status,$objectXls){
        $cont = 0;
        $objectXls->setActiveSheetIndex(0)
            ->setCellValue('A2', 'No.')
            ->setCellValue('B2', 'Tipo')
            ->setCellValue('C2', 'Estatus')
            ->setCellValue('D2', 'Fecha Proceso')
            ->setCellValue('E2', 'Atendio')
            ->setCellValue('F2', 'Comisión ganada')
            ->setCellValue('G2', 'Venta o Servicio')
            ->setCellValue('H2', 'Producto o Subservicio')
            ->setCellValue('I2', 'Descuento(%)')
            ->setCellValue('J2', 'Precio')
            ->setCellValue('K2', 'Cantidad(Pz,ml)')
            ->setCellValue('L2', 'Subtotal')
            ->setCellValue('M2', 'Descuento aplicado')
            ->setCellValue('N2', 'Total')
            ->setCellValue('O2', '----')
            ->setCellValue('P2', 'Ganancia Total Generada');

        $user = $type_user=="general"?null:" and uid_ss=$type_user";
        $start_date = $date_init?" date(date_creation)>='$date_init'":null;
        $cutoff_date = $date_finish?" date(date_creation)<='$date_finish'":null;
        $status = $type_status=="TODAS"?" (type='SERVICIO' or type='VENTAS' or type='CONSUMO')":" status='$type_status'";
        if($date_init){
            $cutoff_date = $date_finish?" and date(date_creation)<='$date_finish'":null;
            $status = $type_status=="TODAS"?" and (type='SERVICIO' or type='VENTAS' or type='CONSUMO')":" and status='$type_status'";
        }
        if($date_finish) $status = $type_status=="TODAS"?" and (type='SERVICIO' or type='VENTAS' or type='CONSUMO')":" and status='$type_status'";

        $query = $start_date.$cutoff_date.$status.$user;
        $cd_sale = VShoppingList::find("$query");
        if(count($cd_sale)>=1){
            $content = array();
            $total = 0;
            $total_secondary = 0;
            foreach($cd_sale as $key => $values){
                $count = $key+1;
                if($values->getStatus()=="ACTIVO" and ($values->getType()=="SERVICIO" or $values->getType()=="VENTAS")){
                    $total+=(double)$values->getPayout();
                    $total_secondary+=(double)$values->getPriceGeneral();
                }
                $subtotal = $values->getType()=="SERVICIO"?$values->getQuantitySs()*$values->getPrice():$values->getQuantitySs()*$values->getSalePrice();
                $discount = ($subtotal*$values->getDiscount())/100;
                $content[$count] = array(
                    "date"=>date("d-m-Y H:i",strtotime($values->getDateCreation())),
                    "name_service"=>$values->getNameService(),
                    "name_sub_service"=>$values->getNameSubservice(),
                    "percentage_discount"=>$values->getDiscount(),
                    "attend"=>$values->getNameAttend()." ".$values->getLastNameAttend(),
                    "price"=>number_format($values->getPrice(),"2",".",","),
                    "type"=>$values->getType(),
                    "status"=>$values->getStatus(),
                    "quantity"=>$values->getQuantitySs(),
                    "commission"=>$values->getCommission(),
                    "subtotal"=>number_format($subtotal,"2",".",','),
                    "discount"=>number_format($discount,"2",".",','),
                    "total"=>number_format($subtotal-$discount,"2",".",',')
                );
                if($values->getType()=="VENTAS"){
                    $discount = ($subtotal*$values->getPercentage())/100;
                    $content[$count]['price'] = number_format($values->getSalePrice(),"2",".",",");
                    $content[$count]['name_service'] = $values->getNameBrand();
                    $content[$count]['name_sub_service'] = $values->getNameProduct();
                    $content[$count]['percentage_discount'] = $values->getPercentage();
                    $content[$count]['discount'] =number_format($discount,"2",".",',');
                    $content[$count]['total'] = number_format($subtotal-$discount,"2",".",',');
                }else if($values->getType()=="CONSUMO"){
                    $content[$count]['name_service'] = $values->getNameBrand();
                    $content[$count]['name_sub_service'] = $values->getNameProduct();
                    $content[$count]['price'] = "0";
                    $content[$count]['percentage_discount'] = "0";
                    $content[$count]['discount'] = "0";
                    $content[$count]['total'] = "0";
                }
            }
            foreach($content as $key => $value){
                $cont =$key+2;
                $objectXls->setActiveSheetIndex(0)
                    ->setCellValue("A".$cont, $key)
                    ->setCellValue("B".$cont, $value['type'])
                    ->setCellValue("C".$cont, $value['status'])
                    ->setCellValue("D".$cont, $value['date'])
                    ->setCellValue("E".$cont, $value['attend'])
                    ->setCellValue("F".$cont, $value['commission'])
                    ->setCellValue("G".$cont, $value['name_service'])
                    ->setCellValue("H".$cont, $value['name_sub_service'])
                    ->setCellValue("I".$cont, $value['percentage_discount'])
                    ->setCellValue("J".$cont, $value['price'])
                    ->setCellValue("K".$cont, $value['quantity'])
                    ->setCellValue("L".$cont, $value['subtotal'])
                    ->setCellValue("M".$cont, $value['discount'])
                    ->setCellValue("N".$cont, $value['total']);
            }
            if($total>0){
                $iva = 16;
                $iva_general = 0;
                $objectXls->setActiveSheetIndex(0)
                    ->setCellValue("O3", "")
                    ->setCellValue("P3", "$".number_format($iva_general+$total,'2','.',','));
            }
        }else{
            $cont = 3;
            $objectXls->setActiveSheetIndex(0)
                ->setCellValue("A3".$cont, "Sin resultados")
                ->setCellValue("B3".$cont, "")
                ->setCellValue("C3".$cont, "")
                ->setCellValue("D3".$cont, "")
                ->setCellValue("E3".$cont, "")
                ->setCellValue("F3"."")->setCellValue("G3"."")->setCellValue("H3"."")->setCellValue("I3"."")
                ->setCellValue("J3"."")->setCellValue("K3"."")->setCellValue("L3"."")->setCellValue("M3"."")
                ->setCellValue("N3"."")->setCellValue("O3"."")->setCellValue("P3"."");
        }

        $this->autoWidth($objectXls,$cont);
        $objectXls->getActiveSheet()->setTitle("Ventas Beauty Room");
        $objectXls->setActiveSheetIndex(0);
        $objWriter = \PHPExcel_IOFactory::createWriter($objectXls,'Excel2007');
        $xls = date('m-d-Y_H.i.s')."_$type_user.xlsx";
        $objWriter->save(dirname(dirname(dirname(dirname(__DIR__))))."/public/reports/".$xls);
        $this->download($xls);
    }
    private function autoWidth($objectXls,$cont){
        $this->style($objectXls,"A2:P2");
        $this->logo($objectXls,"A1:P1");
        $this->styleContent($objectXls,"A3:P$cont");
        $letter = array("A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P");
        foreach($letter as $values){
            $objectXls->getActiveSheet()->getColumnDimension("$values")->setAutoSize(true);
        }
    }
/* End Export History Sales */

    private function scripts(){
        $this->assets->collection('jsPlugins')
            ->setTargetPath("dash/js/general.reports.min.js")
            ->setTargetUri("dash/js/general.reports.min.js")
            ->addJs("dash/js/plugins/BootstrapDP/bootstrap-datepicker.min.js")
            ->addJs("dash/js/plugins/BootstrapDP/bootstrap-datepicker.es.min.js")
            ->addJs("dash/js/reports/reports.js")
            ->join(true)
            ->addFilter(new \Phalcon\Assets\Filters\Jsmin());

        $this->assets->collection('cssPlugins')
            ->setTargetPath("dash/css/general.sales.min.css")
            ->setTargetUri("dash/css/general.sales.min.css")
            ->addCss("dash/css/BDP/bootstrap-datepicker3.css")
            ->join(true)
            ->addFilter(new \Phalcon\Assets\Filters\Cssmin());
    }
}