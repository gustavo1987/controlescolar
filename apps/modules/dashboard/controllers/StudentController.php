<?php
namespace Modules\Dashboard\Controllers;
include dirname(dirname(dirname(dirname(__FILE__))))."/library/wideimage/WideImage.php";
require dirname(dirname(dirname(dirname(__FILE__))))."/library/PHPMailer/PHPMailerAutoload.php";

use Modules\Models\CdAsistencia;
use Modules\Models\CdBrand;
use Modules\Models\CdClase;
use Modules\Models\CdClaseStudent;
use Modules\Models\CdClient;
use Modules\Models\CdConferencia;
use Modules\Models\CdDateCalendar;
use Modules\Models\CdPago;
use Modules\Models\cms\CdPost;
use Modules\Models\CdProduct;
use Modules\Models\CdNotification;
use Modules\Models\CdUser;
use Modules\Models\CdSubservices;
use Modules\Models\cms\CdStudent;
use Modules\Models\CdWorking;
use Modules\Models\VClaseClient;
use Modules\Models\VClientUser;
use Modules\Models\VClientConferenciaAsistencia;
use Modules\Models\VProductBrand;
use Modules\Models\VProductBrandClase;
use Modules\Models\VShoppingList;
use Modules\Models\VStockProduct;
use Modules\Models\VWorkingUserClase;
use Modules\Models\VClaseStudentScore;
use Modules\Models\VReciboPago;
use Modules\Models\VReciboUser;

use Phalcon\Http\Request;

class StudentController extends ControllerBase{
    public function indexAction(){
        $auth = $this->auth();
        $title = "AULA VIRTUAL";
        $url ="/";
        $description = "IDE Iberoamerica";
        $image = "dash/img/logo-100.png";
        $this->metaHome($title,$url,$description,$image);
        if($auth){
            $this->dataTable();
            $clients = null;
            if($auth['rol']!=="ADMIN")
                $clients = CdClient::find("status='ACTIVO' and uid={$auth['uid']}");
            else if($auth['rol']=="ADMIN"){
                $clients = CdClient::find("status='ACTIVO'");
            }
            $this->view->setVar("clients",$clients);
        }else{
            $this->response(array("message"=>"error"),404);
        }
    }
    public function newAction(){
        $auth = $this->auth();
        if($auth){
            $this->scripts();
        }
    }
    public function editAction(){
        $auth = $this->auth();
        $id = $this->dispatcher->getParam("id");
        $this->backView("Estudiantes","/dashboard/estudiantes");
        if($auth){
            $this->scripts();
            $user = VClientUser::findFirst("clid=".$id);
            $postgrados = CdBrand::find("status='ACTIVE'");
            $this->view->setVar("user",$user);
            $this->view->setVar("postgrados",$postgrados);
            $this->backView("Posgrados","/dashboard/student/all/".$user->getCarrera());
        }
    }
    public function todosAction(){
        $auth = $this->auth();
        if($auth){

        $this->backView("Campus Virtual","/dashboard");
        }
    }
    public function planestudioAction(){
        $auth = $this->auth();
        if($auth){
        }
    }
    public function allAction(){
        $auth = $this->auth();
        $this->scripts();
        $id = $this->dispatcher->getParam("id");
        $title = "ESTUDIANTES";
        $url ="/";
        $description = "IDE Iberoamerica";
        $image = "dash/img/logo-100.png";
        $this->metaHome($title,$url,$description,$image);
        $this->backView("Posgrados","/dashboard/student/pos");
        if($auth){
            $this->dataTable();
            $student = VClientUser::find("status='STUDENT' and carrera=".$id);
            $this->view->setVar("student",$student);
        }
    }
    public function posAction(){
        $auth = $this->auth();
        $this->backView("Estudiantes","/dashboard/estudiantes");
        if($auth){
            $posgrados = CdBrand::find("status='ACTIVE'");
            $this->view->setVar("posgrados",$posgrados);
        }else{
            $this->response(array("code"=>404,"message"=>"You do not have permission"),404);
        }

    }
    public function blockstudentAction(){
        $request = $this->request;
        $auth = $this->auth();
        if($request->isAjax() && $request->isPost()&& $auth){
            $block = CdUser::findFirst($request->getPost("uid"));
            if($block){
                $client = CdClient::findFirst("uid=".$block->getUid());
            }
            $block->setStatuspagos($request->getPost("block"));
            if($block->update()){
                $this->response(array("message"=>"SUCCESS","braid"=>$client->getCarrera(),"code"=>200),200);
            }
            else{
                $this->response(array("message"=>"Error to block cd_user","code"=>404),200);
            }
        }
        else{
            $this->response(array("message"=>"You do not have permission","code"=>404),404);
        }
    }
    public function saveuserAction(){
        $request = $this->request;
        $auth = $this->auth();
        if($request->isAjax() && $request->isPost() && $auth ){
            $user = new CdUser();
            $find = $user;
            $usnm = str_replace(" ","-",$request->getPost("username"));
            $carrera = $request->getPost("carrera");
            $find->setName($request->getPost("name"))
                ->setLastName($request->getPost("last_name"))
                ->setSecondName($request->getPost("second_name"))
                ->setSex($request->getPost("sex"))
                ->setPhone($request->getPost("phone"))
                ->setUsername($usnm)
                ->setEmail($request->getPost("email"))
                ->setPhoto("no-image.jpg")
                ->setPassword($this->security->hash($request->getPost("password")))
                ->setRol($request->getPost('rol'))
                ->setStatus($request->getPost('status'))
                ->setDateCreation(date("Y-m-d H:i:s"));
            if($find->save()){
                $subServices = CdSubservices::find();
                $client = new CdClient();
                $client->setIdentifier("".$request->getPost("name")."".$request->getPost("last_name")."".date("Y")."".date("m")."".date("d")."".date("H")."".date("i")."".date("s"))
                        ->setName($request->getPost("name"))
                        ->setLastName($request->getPost("last_name"))
                        ->setSecondName($request->getPost("second_name"))
                        ->setSex($request->getPost("sex"))
                        ->setBirthdate(date("Y-m-d H:i:s"))
                        ->setEmail($request->getPost("email"))
                        ->setCarrera($carrera)
                        ->setTel($request->getPost("phone"))
                        ->setStatus("STUDENT")
                        ->setDateCreation(date("Y-m-d H:i:s"))
                        ->setUid($find->getUid())
                        ->setGeneration($request->getPost("generation"))
                        ->save();
                if(count($subServices)>=1){    
                    foreach($subServices as $values){    
                        $new = new CdUserSubservices();
                        $new->setSubsid($values->getSubsid())
                            ->setUid($find->getUid())
                            ->setPercentage(0)
                            ->setDateCreation(date("Y-m-d H:i:s"));
                        if(!$new->save())$this->response(array("code"=>404,"message"=>"Error to save cd_user_sub_services"),200);
                    }
                }
                $this->getUsersJson(2);
                $this->response(array("message"=>"SUCCESS","code"=>200),200);
            }else{
                /*foreach ($find->getMessages() as $message) {
                    $this->flash->error((string) $message);
                }*/
                $this->response(array("message"=>"Error to save cd_user","code"=>404),200);
            }
        }else{
            $this->response(array("message"=>"You do not have permission","code"=>404),404);
        }
    }
    public function updateuserAction(){
        $request = $this->request;
        $auth = $this->auth();
        if($request->isAjax() && $request->isPost() && $auth ){
            $usnm = str_replace(" ","-",$request->getPost("username"));
            $carrera = $request->getPost("posgrado");
            $clid = CdClient::findFirst("clid=".$request->getPost("clid"));
            $clid->setName($request->getPost("name2"))
                ->setLastName($request->getPost("last_name"))
                ->setSecondName($request->getPost("second_name"))
                ->setSex($request->getPost("sex"))
                ->setEmail($request->getPost("email2"))
                ->setCarrera($request->getPost("posgrado"))
                ->setStatus("STUDENT");
            $find = CdUser::findFirst($clid->getUid());
            /*if($request->getPost('status')=="ACTIVO"){
                $statususer="ACTIVE";
            }
            else{
                $statususer="INACTIVE";
            }*/
            $find->setName($request->getPost("name2"))
                ->setLastName($request->getPost("last_name"))
                ->setSecondName($request->getPost("second_name"))
                ->setSex($request->getPost("sex"))
                ->setPhone($request->getPost("phone"))
                ->setUsername($usnm)
                ->setEmail($request->getPost("email2"))
                ->setStatus($request->getPost('status'));
            if($find->update() and $clid->update()){
                $this->getUsersJson(2);
                $this->response(array("brid"=>$clid->getCarrera(),"message"=>"SUCCESS","code"=>200),200);
            }else{
                /*foreach ($find->getMessages() as $message) {
                    $this->flash->error((string) $message);
                }*/
                $this->response(array("message"=>"Error to save cd_user","code"=>404),200);
            }
        }else{
            $this->response(array("message"=>"You do not have permission","code"=>404),404);
        }
    }
    public function clasesanterioresAction(){
        $this->scripts();
    }
    public function chatAction(){

        $title = "CHAT";
        $url ="/";
        $description = "IDE Iberoamerica";
        $image = "dash/img/logo-100.png";
        $this->metaHome($title,$url,$description,$image);
        $this->backView("Aula virtual","/dashboard/student/clase");
    }
    public function sitiosAction(){
        $title = "SITIOS DE INTERES";
        $url ="/";
        $description = "IDE Iberoamerica";
        $image = "dash/img/logo-100.png";
        $this->metaHome($title,$url,$description,$image);
        $this->backView("Campus Virtual","/dashboard");
        $this->scripts();
    }
    public function calendarioAction(){
        $this->scripts();
        $auth = $this->auth();
        $this->view->setVar("stuid",$auth["uid"]);
        if($auth['rol']=="STUDENT"){
            $client = CdClient::findFirst("uid=".$auth["uid"]);
            $clase = CdClaseStudent::findFirst("status='ACTIVO' and clid=".$client->getClid());
            $clasenow = CdClase::findFirst($clase->getClasid());
            $this->view->setVar("clase",$clasenow);
        }
        elseif($auth['rol']=="TEACHER"){
            $clase = CdClase::find("status='ACTIVO' and uid=".$auth["uid"]);
            $this->view->setVar("clase",$clase[0]);
        }

        $this->backView("Aula virtual","/dashboard/student/clase");
        $this->view->setVar("rol",$auth["rol"]);
        $title = "CALENDARIO";
        $url ="/";
        $description = "IDE Iberoamerica";
        $image = "dash/img/logo-100.png";
        $this->metaHome($title,$url,$description,$image);
    }
    public function notasAction(){       
        $auth = $this->auth();
        $cliente = CdClient::findFirst("uid=".$auth['uid']);
        $this->scripts();
        /*$auth = $this->auth();
        if($auth['rol']=="STUDENT"){*/
        /*}
        elseif($auth['rol']=="TEACHER"){
            $this->scriptsmaestro();
            print_r("2");
        }*/
        $id = $this->dispatcher->getParam("id");
        $work = CdWorking::find("status='ACTIVO' and (type='NOTE' or type='VIDEO') and clasid='".$id."' order by date_creation asc");
        $this->view->setVar("work",$work);
        if($auth['rol']=="STUDENT"){
            $claseold = VClaseClient::find("status_clase_student='INACTIVO' and clid=".$cliente->getClid()." order by clasid desc");
            /*foreach ($claseold as $key => $value){
                $noteold = VWorkingUserClase::find("status_clase='INACTIVO' and (type='NOTE' or type='VIDEO') and clasid='".$value->getClasid()."' order by date_creation desc");
                $this->view->setVar("noteold".$key,$noteold);
            }*/
            $noteold = VWorkingUserClase::find("status_clase='INACTIVO' and (type='NOTE' or type='VIDEO') order by date_creation asc");
            $this->view->setVar("noteold",$noteold);
            $this->view->setVar("claseold",$claseold);
        }
        elseif($auth['rol']=="TEACHER"){
            $claseteacher = CdClase::findFirst("status='ACTIVO' and uid=".$auth['uid']);
            $catedras= CdConferencia::find("clasid=".$id);
            $this->view->setVar("catedras",$catedras);
        }
        $title = "Lecturas";
        $url ="/";
        $description = "IDE Iberoamerica";
        $image = "dash/img/logo-100.png";
        $this->metaHome($title,$url,$description,$image);
        $this->backView("Aula virtual","/dashboard/student/clase");
        $this->view->setVar("rol",$auth['rol']);
    }
    public function allstudentclassAction(){
        $this->scripts();        
        $auth = $this->auth();
        if($auth['rol']=="TEACHER"){
            //$this->dataTable();
            $teacher = CdUser::findFirst($auth["uid"]);
            $other = CdClase::find("status='ACTIVO' and uid=".$auth["uid"]);
            $array = array();
            foreach ($other as $key => $values) {
                $studentclass = VClaseClient::find("status_clase_student='ACTIVO' and clasid=".$values->getClasid());
                foreach ($studentclass as $key => $value) {
                    $array[] = array("clid"=>$value->getClid(),"name"=>$value->getName(),"last_name"=>$value->getLastName(),"second_name"=>$value->getSecondName(),"clasid"=>$value->getClasid(),"status_clase_student"=>$value->getStatusClaseStudent(),"calificacion"=>$value->getCalificacion(),"name_clase"=>$value->getNameClase());      
                }
            }
            //print_r($array);exit;
            //$studentclass = VClaseClient::find("status_clase_student='ACTIVO' and clasid=".$other->getClasid());
            $this->view->setVar("studentclass",$array);
        }
    }
    public function updatescorestudentAction(){
        $request = $this->request;
        $auth = $this->auth();
        if($request->isAjax() && $request->isPost() && $auth ){
            $student = CdClaseStudent::findFirst("clasid=".$request->getPost("clasid-update")." and clid=".$request->getPost("clid-update"));
            $student->setCalificacion($request->getPost("scorestudent"));
            if($student->update()){
                $notification = new CdNotification();
                $student = CdClient::findFirst($request->getPost("clid-update"));
                $notification->setNotification("Actualizaron tu calificación")
                        ->setStatus("ACTIVE")
                        ->setUid($student->getUid())
                        ->setStatusView("WAIT")
                        ->setDate(date("Y-m-d H:m:s"))
                        ->save();
                $this->response(array("message"=>"SUCCESS","clasid"=>$request->getPost("clasid-update"),"code"=>200),200);
            }
            else{
                $this->response(array("message"=>"Error to update score","code"=>404),200);
            }

        }
        else{
            $this->response(array("message"=>"You do not have permission","code"=>404),404);
        }

    }
    public function trabajosAction(){
        $this->scripts();
        $id = $this->dispatcher->getParam("id");        
        $auth = $this->auth();
        if($auth['rol']=="TEACHER"){
            $classTeacher = CdClase::find("uid=".$auth['uid']);
            if(count($classTeacher)>1){
                $id=0;
            }
            if($id==0){
                $clase = CdClase::find("status='ACTIVO' and uid=".$auth['uid']);
                $complemento =" ";
                foreach ($clase as $key => $value) {
                    if($key==0){
                        $complemento.="and (clasid=".$value->getClasid();
                    }
                    else{
                        $complemento.="or clasid=".$value->getClasid();
                    }
                }
                //$clase = CdClase::findFirst("uid=".$auth['uid']);
                $query = "status='ACTIVO' and status_user='ACTIVE' and status_clase='ACTIVO' and type='WORK' ";
                $query.=$complemento.")";
                //$work = VWorkingUserClase::find("status='ACTIVO' and status_user='ACTIVE' and status_clase='ACTIVO' and type='WORK' and clasid=".$id);
                $work = VWorkingUserClase::find($query);
            }
            else{
                $work = VWorkingUserClase::find("status='ACTIVO' and status_user='ACTIVE' and status_clase='ACTIVO' and type='WORK' and clasid=".$id);
                
            }
            if($work){
                $statuswork=1;
                $this->view->setVar("work",$work);
            }
            else $statuswork=0;
            $this->view->setVar("statuswork",$statuswork);
        }
        elseif($auth['rol']=="STUDENT"){
            $catedras = CdConferencia::find("clasid=".$id);
            $work = CdWorking::find("status='ACTIVO' and type='WORK' and uid='".$auth['uid']."' and clasid=".$id);
            if($work){
                $statuswork=1;
                $this->view->setVar("work",$work);
            }
            else $statuswork=0;
            $this->view->setVar("statuswork",$statuswork);
            $this->view->setVar("catedras",$catedras);
            $this->view->setVar("idclase",$id);
        }
        $this->view->setVar("auth",$auth);
        $title = "TRABAJOS";
        $url ="/";
        $description = "IDE Iberoamerica";
        $image = "dash/img/logo-100.png";
        $this->metaHome($title,$url,$description,$image);
        $this->backView("Aula virtual","/dashboard/student/clase");
    }
    public function asistenciasAction(){
        $auth = $this->auth();
        $id = $this->dispatcher->getParam("id");
        $title = "ASISTENCIAS";
        $url ="/";
        $description = "IDE Iberoamerica";
        $image = "dash/img/logo-100.png";
        $this->metaHome($title,$url,$description,$image);
        $this->backView("Aula virtual","/dashboard/student/clase");
        if($auth){
            $this->dataTable();
            $this->scripts();
            if($auth["rol"]=="STUDENT"){
                $student = CdClient::findFirst("email='".$auth["email"]."'");
                $clase = CdClase::findFirst("status='ACTIVO' and carrera=".$student->getCarrera());
                $this->view->setVar("clase",$clase);
                //$conferencias = VClientConferenciaAsistencia::find("status_conferencia='INACTIVO' and clasid=".$clase->getClasid()." and clid=".$student->getClid());
                $conferencias = VClientConferenciaAsistencia::find("clasid=".$clase->getClasid()." and clid=".$student->getClid());
                //$conferencias = CdConferencia::find("status='INACTIVO' and clasid=".$id);
            }
            elseif($auth["rol"]=="TEACHER"){
                $clase = CdClase::find("status='ACTIVO' and uid=".$auth["uid"]);
                $count = count($clase);
                $countfor = $count--;
                $text = "clasid=".$clase[0]->getClasid();
                for($i=1;$i< $countfor;$i++){ 
                    $text2=" or clasid=".$clase[$i]->getClasid();
                    $text.= $text2;
                    $text2="";
                }
                $confes = CdConferencia::find("status='INACTIVO' and ".$text);
                $conferencias = VClientConferenciaAsistencia::find("status_conferencia='INACTIVO' and ".$text);
                $this->view->setVar("clase",$clase);
                $this->view->setVar("confes",$confes);
                $this->view->setVar("conferencias",$conferencias);
            }
            if(empty($conferencias)){
                $statusconfe = 0;
            }
            else{
                $statusconfe = 1;
                $this->view->setVar("conferencias",$conferencias);
            } 
            //$find = VProductBrand::find("status='ACTIVE' and brid=".$student->getCarrera()." and stock_min=1");
            $this->view->setVar("statusconfe",$statusconfe);
            $this->view->setVar("auth",$auth);
            //print_r($conferencias->toArray());exit;
        }else{
            $this->response(array("code"=>404,"message"=>"You do not have permission"),404);
        }
    }
    public function materiasAction(){
        
        $auth = $this->auth();
        if($auth){
            $this->dataTable();
            $this->scripts();
            $student = CdClient::findFirst("email='".$auth["email"]."'");

            $find = VProductBrand::find("status='ACTIVE' and brid=".$student->getCarrera()." and stock_min=1");
            $find2 = VProductBrand::find("status='ACTIVE' and brid=".$student->getCarrera()." and stock_min=2");
            $find3 = VProductBrand::find("status='ACTIVE' and brid=".$student->getCarrera()." and stock_min=3");
            $find4 = VProductBrand::find("status='ACTIVE' and brid=".$student->getCarrera()." and stock_min=4");
            $find5 = VProductBrand::find("status='ACTIVE' and brid=".$student->getCarrera()." and stock_min=5");
            $this->view->setVar("products1",$find);
            $this->view->setVar("products2",$find2);
            $this->view->setVar("products3",$find3);
            $this->view->setVar("products4",$find4);
            $this->view->setVar("products5",$find5);
            $this->view->setVar("auth",$auth);
        }else{
            $this->response(array("code"=>404,"message"=>"You do not have permission"),404);
        }
    }
    public function claseAction(){
        $auth = $this->auth();
        $this->scripts();
        $student =  CdUser::findFirst($auth['uid']);
        $client = CdClient::findFirst("email='".$student->getEmail()."'");
        $id = $this->dispatcher->getParam("id");
        $title = "AULA VIRTUAL";
        $url ="/";
        $description = "IDE Iberoamerica";
        $image = "dash/img/logo-100.png";
        $this->metaHome($title,$url,$description,$image);
        $this->backView("Campus Virtual","/dashboard");
        $hoy = date("Y-m-d H:m:s");
        if($auth['rol']=="TEACHER"){
            $claseultima = CdClase::findFirst("status='ACTIVO' and uid=".$auth['uid']);
        }
        elseif($auth['rol']=="STUDENT"){
            $claseultima1 = CdClaseStudent::findFirst("status='ACTIVO' and clid=".$client->getClid());
            $claseultima = CdClase::findFirst($claseultima1->getClasid());
            //$claseultima = CdClase::findFirst("status='ACTIVO' and generation=".$client->getGeneration()." and carrera=".$client->getCarrera());
        }
        if($claseultima){
            $conferencia = CdConferencia::findFirst("status='ACTIVO' and clasid=".$claseultima->getClasid());
            $conferencianteriores = CdConferencia::find("status='INACTIVO' and clasid=".$claseultima->getClasid());
            $carrera = CdBrand::findFirst($claseultima->getCarrera());
            $this->view->setVar("claseultima",$claseultima);
            $this->view->setVar("conferencia",$conferencia);
            $this->view->setVar("carrera",$carrera);
        }


        /*$statusothersclase=0;
        if($auth['rol']=="TEACHER"){
            $count = count($claseultima);
            if($count>1){
                $statusothersclase=1;
                $clasetrabajos=0;
                $this->view->setVar("clasetrabajos",$clasetrabajos);
            }
        }
        $this->view->setVar("statusothersclase",$statusothersclase);*/


        if($conferencianteriores){
            $statusconfe = 1;
            $this->view->setVar("conferencianteriores",$conferencianteriores);
        }
        else $statusconfe = 0;
        $this->view->setVar("client",$client);
        $this->view->setVar("statusconfe",$statusconfe);
    }
    public function videoAction(){

        $title = "VIDEOS";
        $url ="/";
        $description = "IDE Iberoamerica";
        $image = "dash/img/logo-100.png";
        $this->metaHome($title,$url,$description,$image);
    }
    public function pagosAction(){
        $auth = $this->auth();
        $title = "PAGOS";
        $url ="/";
        $description = "IDE Iberoamerica";
        $image = "dash/img/logo-100.png";

        $statusrecibos="INACTIVO";
        $recibosall = VReciboUser::find("uid=".$auth["uid"]." order by date_creation desc");
        if($recibosall)$statusrecibos="ACTIVO";
        $this->view->setVar("recibosall",$recibosall);
        $this->view->setVar("statusrecibos",$statusrecibos);



        $this->metaHome($title,$url,$description,$image);
        $client = CdClient::findFirst("uid=".$auth['uid']);
        $pagosrealizados = CdPago::find("status='PAGADO' and clid=".$client->getClid());
        $pagospendiente = CdPago::findFirst("status='ACTIVO' and clid=".$client->getClid());
        if($auth['rol']=="ADMIN"){
            $student = CdClient::find("status='STUDENT'");
            $this->view->setVar("student",$student);
        }
        elseif($auth['rol']=="STUDENT"){
            $this->backView("Campus Virtual","/dashboard");
        }

        $this->view->setVar("emailuser",$auth['email']);
        $this->view->setVar("pagospendiente",$pagospendiente);
        $this->view->setVar("pagosrealizados",$pagosrealizados);
    }
    public function addpayAction(){
        $this->scripts();
        $auth = $this->auth();
        $request = new Request();
        $id = $this->dispatcher->getParam("id");
        $client = CdClient::findFirst($id);
        $this->backView("Pagos","/dashboard/controlescolar/pagos");
        $this->view->setVar("client",$client);

    }
    public function savepayAction(){
        $request = $this->request;
        $auth = $this->auth();
        if($request->isAjax() && $request->isPost() && $auth ){
            $id=$request->getPost("id");
            $relationship="NO";
            if($request->getPost("namepay")=="MENSUALIDAD"){
                $name="Mensualidad de ".$request->getPost("month");
            }
            elseif($request->getPost("namepay")=="OTRO"){
                $name="Pago de ".$request->getPost("month");
            }
            else{
                $name=$request->getPost("namepay");
            }
            if($request->getPost("status")=="PAGADO")$relationship="YES";
            $separador = explode("/", $request->getPost("vencimiento"));
            $date = date("".$separador[2]."-".$separador[1]."-".$separador[0]." 00:00:01");
            $user = new CdPago();
            $user->setName($name)
                ->setCantidad($request->getPost("cantidad"))
                ->setDateCreation(date("Y-m-d H:m:s"))
                ->setStatus($request->getPost("status"))
                ->setVencimiento($date)
                ->setType($request->getPost("namepay"))
                ->setRelationship($relationship)
                ->setView("NO")
                ->setClid($id);
            if(!$user->save())$this->response(array("code"=>404,"message"=>"Error to save cd_user_sub_services"),200);
            $this->response(array("message"=>"SUCCESS","id"=>$id,"code"=>200),200);
        }

    }
    public function viewpayAction(){
        $auth = $this->auth();
        $request = new Request();
        $statusrecibo="INACTIVO";
        $this->scripts();
        $this->dataTable();
        $id = $this->dispatcher->getParam("id");
        $pagos = CdPago::find("clid=".$id);
        $client = CdClient::findFirst($id);
        $recibosall=VReciboPago::find("uid=".$client->getUid()." and status='ACTIVO' order by vencimiento asc");
        if($recibosall){
            $statusrecibo="ACTIVO";
            $this->view->setVar("recibosall",$recibosall);
        }
        $this->backView("Pagos","/dashboard/controlescolar/pagos/".$client->getCarrera());
        $status = 0;
        if(isset($pagos)) {
            $status = 1;
        }
        $this->view->setVar("statusrecibo",$statusrecibo);
        $this->view->setVar("pagos",$pagos);
        $this->view->setVar("client",$client);
        $this->view->setVar("status",$status);
    }

    public function deletepayAction(){
        $request = $this->request;
        $auth = $this->auth();
        if($request->isAjax() && $request->isPost() && $auth ){
            $pagid=$request->getPost("pagid");
            $find = CdPago::findFirst($pagid);
            if(!$find->delete())$this->response(array("code"=>404,"message"=>"Error to delete cd_pago"),200);
            $this->response(array("message"=>"SUCCESS","pagid"=>$pagid,"code"=>200),200);

        }
    }
    public function serviciosAction(){
        $title = "SERVICIOS ESCOLARES";
        $url ="/";
        $description = "IDE Iberoamerica";
        $image = "dash/img/logo-100.png";
        $this->metaHome($title,$url,$description,$image);
    }
    public function videosAction(){
    }
    public function getcalendarAction(){
        $auth = $this->auth();
        $materia = CdClient::findFirst($auth['uid']);
        if($materia->getCarrera()){
            $clase = VProductBrandClase::findFirst("status_clase='ACTIVO' and brid=".$materia->getCarrera());
        }
        else{
            $clase = VProductBrandClase::findFirst("status_clase='ACTIVO'");
        }
        $datecaledar = CdDateCalendar::find("clasid=".$clase->getClasid());


        $user_events = [];
        foreach ($datecaledar as $key => $value) {
            $user_events[] = ['title' => $value->name,'allDay' => true,'start' => $value->datestart];
        }

        //print_r(json_encode($user_events));exit;
        print_r($user_events);exit;
        //echo $user_events;
        //return $user_events;
        //$this->response($user_events,200);
    }
    public function materiaAction(){
        $auth = $this->auth();
        $request = new Request();
        $id = $this->dispatcher->getParam("id","int");

        if($auth){
            $this->dataTable();
            $this->scripts();
            $find = CdProduct::findFirst("pid=".$id);
            $this->view->setVar("products",$find);
            $this->view->setVar("auth",$auth);
        }else{
            $this->response(array("code"=>404,"message"=>"You do not have permission"),404);
        }
    }
    public function updatediferidoAction(){
        $request = $this->request;
        $auth = $this->auth();
        if($auth){
            $client = CdClient::findFirst("uid=".$auth['uid']);
            $confe = CdConferencia::findFirst($request->getPost("confid"));
            $asistencia = CdAsistencia::findFirst("clid=".$client->getClid()." and confid=".$confe->getConfid());
            if(!$asistencia){
                $asistencia = new CdAsistencia();
                $asistencia->setClid($client->getClid())
                        ->setConfid($confe->getConfid())
                        ->setStatus("")
                        ->save();
            }
            //print_r($request->getPost("confid"));exit;
            if(!($asistencia->getStatus()=="VIVO" || $asistencia->getStatus()=="DIFERIDO")){
                $asistencia->setStatus("DIFERIDO");
                $mensaje = "Asistencia actualizado a DIFERIDO";
                if($asistencia->save()){
                    $this->response(array("mensaje"=>$mensaje,"url"=>$confe->getUrl(),"message"=>"SUCCESS","code"=>"200"),200);
                }
                else{
                    $this->response(array("message"=>"no guardo","code"=>"404"),200);
                }
            }
            else{
                $mensaje = "Ya tiene asistencia";
                $this->response(array("mensaje"=>$mensaje,"url"=>$confe->getUrl(),"message"=>"SUCCESS","code"=>"200"),200);
            }
        }
        else{
            $this->response(array("code"=>404,"message"=>"You do not have permission"),404);
        }
    }
    public function calificacionesAction(){
        $auth = $this->auth();
        $title = "PROGRESO ACADÉMICO";
        $url ="/";
        $description = "IDE Iberoamerica";
        $image = "dash/img/logo-100.png";
        $this->metaHome($title,$url,$description,$image);
        $this->backView("Campus Virtual","/dashboard");
        if($auth){
            $this->dataTable();
            $this->scripts();
            $student = CdClient::findFirst("email='".$auth["email"]."'");
            $quinto = 0;
            $score = VClaseStudentScore::find("clid=".$student->getClid()." and calificacion>=0");
            $this->view->setVar("score",$score);
            if($student->getCarrera()==17){ 
                    $modulo="Cuatrimestre";
            }
            elseif($student->getCarrera()==18){
                $modulo="Cuatrimestre";
            }
            elseif($student->getCarrera()==19){
                $modulo="Semestre";
                $quinto=1;
            }elseif($student->getCarrera()==20){
                $modulo="Semestre";
                $quinto=1;
            }
            $this->view->setVar("modulo",$modulo);
            $find = VProductBrand::find("status='ACTIVE' and brid=".$student->getCarrera()." and stock_min=1");
            $find2 = VProductBrand::find("status='ACTIVE' and brid=".$student->getCarrera()." and stock_min=2");
            $find3 = VProductBrand::find("status='ACTIVE' and brid=".$student->getCarrera()." and stock_min=3");
            $find4 = VProductBrand::find("status='ACTIVE' and brid=".$student->getCarrera()." and stock_min=4");
            $find5 = VProductBrand::find("status='ACTIVE' and brid=".$student->getCarrera()." and stock_min=5");
            $carreraestudiante = CdBrand::findFirst($student->getCarrera());
            $this->view->setVar("products1",$find);
            $this->view->setVar("products2",$find2);
            $this->view->setVar("products3",$find3);
            $this->view->setVar("products4",$find4);
            $this->view->setVar("products5",$find5);
            $this->view->setVar("auth",$auth);
            $this->view->setVar("student",$student);
            $this->view->setVar("carreraestudiante",$carreraestudiante->getName());
        }else{
            $this->response(array("code"=>404,"message"=>"You do not have permission"),404);
        }

    }
    public function hiddenpaystudentAction(){
        $auth = $this->auth();
        $request = $this->request;
        $statuspay = $request->getPost("status");
        $id = $request->getPost("id");
        if($auth){
            $pago = CdPago::findFirst($id);
            if($pago){
                $pago->setView($statuspay);
                if($pago->update()){
                    $this->response(array("message"=>"SUCCESS","code"=>"200"),200);
                }
                else{
                    $this->response(array("message"=>"no actualizo el estado","code"=>"404"),200);
                }   
            }
            else{
                $this->response(array("message"=>"no existe pago","code"=>"404"),200);
            }
        }
        else{
            $this->response(array("code"=>404,"message"=>"You do not have permission"),404);
        }
    }
    public function asistenciavivoAction(){
        $auth = $this->auth();
        $request = $this->request;
        $conferencia = $request->getPost("confid");
        if($auth){
            $client = CdClient::findFirst("uid=".$auth["uid"]);
            $statusasistencia = CdAsistencia::findFirst("confid=".$conferencia." and clid=".$client->getClid());
            if($statusasistencia){
                $statusasistencia->setStatus("VIVO");
                if($statusasistencia->save()){
                    $this->response(array("message"=>"SUCCESS","code"=>"200"),200);
                }
                else{
                    foreach ($statusasistencia->getMessages() as $message) {
                        $this->flash->error((string) $message);
                    }
                    $this->response(array("code"=>404,"message"=>$message),404);
                }

            }
            else{
                $asistenciavivio = new CdAsistencia();
                /*print_r($request->getPost("id"));*/
                $asistenciavivio->setStatus("VIVO")
                        ->setClid($client->getClid())
                        ->setConfid($request->getPost("confid"));
                if($asistenciavivio->save()){
                    $this->response(array("message"=>"SUCCESS","code"=>"200"),200);
                }
                else{
                    foreach ($asistenciavivio->getMessages() as $message) {
                        $this->flash->error((string) $message);
                    }
                    $this->response(array("code"=>404,"message"=>$message),404);
                }
            }
        }
        else{
            $this->response(array("code"=>404,"message"=>"You do not have permission"),404);
        }
    }
    public function aulaAction(){
        $auth = $this->auth();

        $title = "PLAN DE ESTUDIO";
        $url ="/";
        $description = "IDE Iberoamerica";
        $image = "dash/img/logo-100.png";
        $this->metaHome($title,$url,$description,$image);
        $this->backView("Campus Virtual","/dashboard");
        if($auth){
            $this->dataTable();
            $quinto = 0;
            $student = CdClient::findFirst("email='".$auth["email"]."'");

            if($student->getCarrera()==17){ 
                    $modulo="Cuatrimestre";
            }
            elseif($student->getCarrera()==18){
                $modulo="Cuatrimestre";
            }
            elseif($student->getCarrera()==19){
                $modulo="Semestre";
                $quinto=1;
            }elseif($student->getCarrera()==20){
                $modulo="Semestre";
                $quinto=1;
            }
            $this->view->setVar("modulo",$modulo);
            $find = VStockProduct::find("status_product='ACTIVE' and brid=".$student->getCarrera()." and stock_min=1");   
            $find2 = VStockProduct::find("status_product='ACTIVE' and brid=".$student->getCarrera()." and stock_min=2");  
            $find3 = VStockProduct::find("status_product='ACTIVE' and brid=".$student->getCarrera()." and stock_min=3"); 
            $find4 = VStockProduct::find("status_product='ACTIVE' and brid=".$student->getCarrera()." and stock_min=4");
            $find5 = VStockProduct::find("status_product='ACTIVE' and brid=".$student->getCarrera()." and stock_min=5");     
            //$find = VProductBrand::find("status='ACTIVE' and brid=".$student->getCarrera()." and stock_min=1");
            $this->view->setVar("products1",$find);
            $this->view->setVar("products2",$find2);
            $this->view->setVar("products3",$find3);
            $this->view->setVar("products4",$find4);
            $this->view->setVar("products5",$find5);
            $this->view->setVar("quinto",$quinto);
        }else{
            $this->response(array("message"=>"error"),404);
        }
    }
    public function uploadfileAction(){
        $request = $this->request;
        $auth = $this->auth();
        $client = CdClient::findFirst("uid=".$auth['uid']);
        $clase = CdClase::findFirst("status='ACTIVO' and carrera=".$client->getCarrera());
        $teacher = CdUser::findFirst($clase->getUid());
        $confid=$request->getPost("confid");
        $name="Trabajo de ".$auth['name'];
        if($request->isPost() && $request->isAjax() && $auth){
            if($request->hasFiles()==true){
                foreach($request->getUploadedFiles() as $file){
                    $type = explode("/",$file->getType());
                    $image_replace = $this->url_clean($file->getName());
                    $new_image = uniqid()."_".$image_replace;
                    if($file->moveTo(dirname(dirname(dirname(dirname(__DIR__))))."/public/documents/work/".$new_image))
                    {
                        $work = new CdWorking();
                        $work->setStatus("ACTIVO")
                            ->setName($name)
                            //->setName("trabajo")
                            ->setDateCreation(date("Y-m-d H:m:s"))
                            ->setUid($auth['uid'])
                            ->setClasid($clase->getClasid())
                            ->setFileTeacher($new_image)
                            ->setConfid($confid)
                            ->setType("WORK");
                        if($work->save()){
                            $this->sendEmail($teacher->getEmail());
                            $this->response(array("name"=>$new_image,"clase"=>$clase->getClasid(),"message"=>"SUCCESS","code"=>"200"),200);
                        }
                        else{
                            $this->response(array("name"=>$new_image,"message"=>"no guardo","code"=>"404"),200);
                        }
                    }
                    else{
                        $this->response(array("name"=>$new_image,"message"=>"no se pudo mover el archivo","code"=>"404"),200);
                    }
                }
            }
        }else{
            $this->response(array("name"=>$new_image,"message"=>"no se envío nada","code"=>"404"),200);
        }
    }


    private function sendEmail($values){
        $email = new \PHPMailer();
        $email->isSMTP();
        $email->Host = "smtp.gmail.com";
        $email->Port=587;
        $email->CharSet = 'UTF-8';
        $email->SMTPSecure="tls";
        $email->SMTPAuth =true;
        $email->Username = "enviodecorreoscdevelopers@gmail.com";
        $email->Password = "cdevelopers2018";
        $email->setFrom("contactanos@ideiberoamerica.com",'IDE Iberoamerica');
        $email->addReplyTo("contactanos@ideiberoamerica.com",'IDE Iberoamerica');
        $email->addAddress($values);
        $email->WordWrap =100;
        $email->isHTML(true);
        $email->Subject = "Trabajo cargado";

        $file = dirname(__DIR__)."/views/email/worksendstudent.html";
        $email->msgHTML(file_get_contents($file));
        $email->AltBody = "Instituto Iberoamericano de Derecho";
        
        if(!$email->send()) {
            echo 'Message could not be sent.';
            echo 'Mailer Error: ' . $email->ErrorInfo;
        } else {
            return true;
        }
    }
    private function urlclean($string) {
        $string = mb_strtolower(str_replace(' ', '-',str_replace('-','',$string)), 'UTF-8');
        $utf8 = array(
            '/[áàâãªä]/u'   =>   'a',
            '/[ÁÀÂÃÄ]/u'    =>   'A',
            '/[ÍÌÎÏ]/u'     =>   'I',
            '/[íìîï]/u'     =>   'i',
            '/[éèêë]/u'     =>   'e',
            '/[ÉÈÊË]/u'     =>   'E',
            '/[óòôõºö]/u'   =>   'o',
            '/[ÓÒÔÕÖ]/u'    =>   'O',
            '/[úùûü]/u'     =>   'u',
            '/[ÚÙÛÜ]/u'     =>   'U',
            '/ç/'           =>   'c',
            '/Ç/'           =>   'C',
            '/ñ/'           =>   'n',
            '/Ñ/'           =>   'N',
            '/–/'           =>   '-',
            '/:/'           =>   '-', // UTF-8 hyphen to "normal" hyphen
            '/!/'           =>   '', // UTF-8 hyphen to "normal" hyphen
            '/¡/'           =>   '', // UTF-8 hyphen to "normal" hyphen
            '/@/'           =>   '', // UTF-8 hyphen to "normal" hyphen
            '/,/'           =>   '', // UTF-8 hyphen to "normal" hyphen
            '/[’‘‹›‚¿?]/u'    => '', // Literally a single quote
            '/[“”«»„""]/u'    => '', // Double quote
            '/ /'           =>   '', // nonbreaking space (equiv. to 0x160)
        );
        $string = preg_replace('/[^A-Za-z0-9áéíóúÁÉÍÓÚñÑ\-!¡¿?@]/', '', $string); // Removes special chars.
        return preg_replace(array_keys($utf8),array_values($utf8),$string); // Removes special chars.
        //'/[^A-Za-z0-9áéíóúÁÉÍÓÚñÑ\-!¡¿?@]/', '',
    }

    /*private function scriptsmaestro(){
        $this->assets->collection('jsPlugins')
            ->setTargetPath("dash/js/general.student.min.js")
            ->setTargetUri("dash/js/general.student.min.js")
            ->addJs("dash/js/plugins/dropzone/dropzone.min.js")
            ->addJs("dash/js/plugins/bootstrapV/formValidation.min.js")
            ->addJs("dash/js/plugins/bootstrapV/bootstrapV.min.js")
            //->addJs("dash/js/sweetalert.min.js")
            ->addJs("dash/js/maestro/maestro.js")
            ->join(true)
            ->addFilter(new \Phalcon\Assets\Filters\Jsmin());
    }*/
    private function scripts(){
        $this->assets->collection('jsPlugins')
            ->setTargetPath("dash/js/general.student.min.js")
            ->setTargetUri("dash/js/general.student.min.js")
            ->addJs("dash/js/plugins/dropzone/dropzone.min.js")
            ->addJs("dash/js/plugins/bootstrapV/formValidation.min.js")
            ->addJs("dash/js/plugins/bootstrapV/bootstrapV.min.js")
            ->addJs("dash/js/plugins/BootstrapDP/bootstrap-datepicker.min.js")
            ->addJs("dash/js/plugins/BootstrapDP/bootstrap-datepicker.es.min.js")
            //->addJs("dash/js/sweetalert.min.js")
            ->addJs("dash/js/student/slick.js")
            ->addJs("dash/js/student/fullcalendar.js")
            ->addJs("dash/js/student/student.js")
            ->join(true)
            ->addFilter(new \Phalcon\Assets\Filters\Jsmin());
    }
}