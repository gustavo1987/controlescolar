<?php
namespace Modules\Dashboard\Controllers;

use Modules\Models\CdLogUserSubservices;
use Modules\Models\CdServices;
use Modules\Models\CdSubservices;
use Modules\Models\CdUser;
use Modules\Models\CdUserSubservices;

class ServicesController extends ControllerBase{
/* Actions Services */
    public function indexAction(){
        $auth = $this->auth();
        if($auth){
            $this->dataTable();
            $this->validationJs();
            $this->scripts();
            $services  = CdServices::find(array("order"=>"servid asc"));
            $this->view->setVar("services",$services);
        }else{
            $this->response(array("code"=>404,"message"=>"You do not have permission"),404);
        }
    }
    public function saveAction(){
        $request = $this->request;
        $auth = $this->auth();
        if($request->isPost() && $request->isAjax() && $auth ){
            $values = $request->getPost();
            if($values['servid']){
                $services = CdServices::findFirst($values['servid']);
                $services->setName($values['name'])
                    ->setPermalink($values['permalink'])
                    ->setDateCreation(date("Y-m-d H:i:s"))
                    ->setUid($auth['uid']);
                if(!$services->update())$this->response(array("code"=>404,"message"=>"Error to update cd_services"),200);
                $type = 2;
            }else{
                $services = new CdServices();
                $services->setName($values['name'])
                    ->setPermalink($values['permalink'])
                    ->setDateCreation(date("Y-m-d H:i:s"))
                    ->setUid($auth['uid']);
                if(!$services->save())$this->response(array("code"=>404,"message"=>"Error to save cd_services"),200);
                $type = 1;
            }
            $this->response(array("code"=>200,"message"=>"SUCCESS","type"=>$type,"result"=>array("id"=>$services->getServid(),"name"=>$services->getName(),"permalink"=>$services->getPermalink(),"url"=>"http://".$this->url->get("dashboard/services/sub-services/{$services->getServid()}"))),200);
        }else{
            $this->response(array("code"=>404,"message"=>"You do not have permission"),404);
        }
    }
    public function deleteAction(){
        $request = $this->request;
        $auth = $this->auth();
        if($request->isPost() && $request->isAjax() && $auth ){
            $id = $request->getPost("id");
            $service = CdServices::findFirst($id);
            try{
                if($service->delete())$this->response(array("message"=>"SUCCESS","code"=>"200"),200);
            }catch (\Exception $e){
                $this->response(array("message"=>"SUCCESS","code"=>"300"),200);
            }
        }else{
            $this->response(array("code"=>404,"message"=>"You do not have permission"),404);
        }
    }
    public function permalinkAction(){
        $request = $this->request;
        $auth = $this->auth();
        if($request->isPost() && $request->isAjax() && $auth ){
            $post = new CdServices();
            $name = $request->getPost("name");
            $new_url = $this->url_clean($name);
            $check_url = $post->find("permalink = '$new_url'");
            $count = 1;
            while(count($check_url)){
                $generate_url = $new_url."-".$count;
                $check_url = $post->find("permalink = '$generate_url'");
                if(count($check_url)==0){
                    $new_url = $generate_url;
                }
                $count++;
            }
            $this->response(array("message"=>"SUCCESS","permalink"=>$new_url,"code"=>"200","data"=>"url generated"),200);
        }else{
            $this->response(array("code"=>404,"message"=>"You do not have permission"),404);
        }
    }
/* End Actions Services */

/* Actions Sub-Services */
    public function subServicesAction(){
        $auth = $this->auth();
        $param = $this->dispatcher->getParam("id");
        if($auth && $param){
            $services = CdServices::findFirst($param);
            if(!$services)$this->response->redirect("dashboard/services");
            $this->validationJs();
            $this->scripts();
            $subServices  = CdSubServices::find("servid=$param order by subsid asc");
            $this->view->setVar("subServices",$subServices);
            $this->view->setVar("servid",$param);
        }else{
            $this->response(array("code"=>404,"message"=>"You do not have permission"),404);
        }
    }
    public function saveSubServiceAction(){
        $request = $this->request;
        $auth = $this->auth();
        if($request->isPost() && $request->isAjax() && $auth ){
            $values = $request->getPost();
            if($values['subsid']){
                $services = CdSubservices::findFirst($values['subsid']);
                $services->setName($values['nameSS'])
                    ->setPermalink($values['permalinkSS'])
                    ->setPrice($values['price'])
                    ->setDiscount($values['discount'])
                    ->setStatus($values['status']?$values['status']:"ACTIVE");
                if(!$services->save())$this->response(array("code"=>404,"message"=>"Error to save cd_sub_services"),200);
                $type = 2;
            }else{
                $services = new CdSubservices();
                $services->setName($values['nameSS'])
                    ->setPermalink($values['permalinkSS'])
                    ->setDateCreation(date("Y-m-d H:i:s"))
                    ->setPrice($values['price'])
                    ->setDiscount($values['discount'])
                    ->setServid($values['servidSS'])
                    ->setStatus("ACTIVE")
                    ->setUid($auth['uid']);
                if(!$services->save())$this->response(array("code"=>404,"message"=>"Error to save cd_sub_services"),200);
                $user = CdUser::find();
                foreach($user as $key =>$values){
                    $new = new CdUserSubservices();
                    $new->setSubsid($services->getSubsid())
                        ->setUid($values->getUid())
                        ->setPercentage(0)
                        ->setDateCreation(date("Y-m-d H:i:s"));
                    if(!$new->save())$this->response(array("code"=>404,"message"=>"Error to save cd_user_sub_services"),200);
                }
                $type = 1;
            }
            $this->response(array("code"=>200,"message"=>"SUCCESS","type"=>$type,"result"=>array("id"=>$services->getSubsid(),"name"=>$services->getName(),"permalink"=>$services->getPermalink(),"price"=>$services->getPrice(),"discount"=>$services->getDiscount(),"status"=>$services->getStatus())),200);
        }else{
            $this->response(array("code"=>404,"message"=>"You do not have permission"),404);
        }
    }
    public function deleteSubServiceAction(){
        $request = $this->request;
        $auth = $this->auth();
        if($request->isPost() && $request->isAjax() && $auth ){
            $id = $request->getPost("id");
            $subService= CdSubservices::findFirst($id);
            try{
                if($subService->delete())$this->response(array("message"=>"SUCCESS","code"=>"200"),200);
            }catch (\Exception $e){
                $this->response(array("message"=>"SUCCESS","code"=>"300"),200);
            }
        }else{
            $this->response(array("code"=>404,"message"=>"You do not have permission"),404);
        }
    }
    public function permalinkSubServiceAction(){
        $request = $this->request;
        $auth = $this->auth();
        if($request->isPost() && $request->isAjax() && $auth ){
            $post = new CdSubservices();
            $name = $request->getPost("name");
            $id = $request->getPost("id");
            $new_url = $this->url_clean($name);
            $check_url = $post->find("permalink = '$new_url' and servid=$id");
            $count = 1;
            while(count($check_url)){
                $generate_url = $new_url."-".$count;
                $check_url = $post->find("permalink = '$generate_url'");
                if(count($check_url)==0){
                    $new_url = $generate_url;
                }
                $count++;
            }
            $this->response(array("message"=>"SUCCESS","permalink"=>$new_url,"code"=>"200","data"=>"url generated"),200);
        }else{
            $this->response(array("code"=>404,"message"=>"You do not have permission"),404);
        }
    }
/* End Actions Sub-Services */

/* Other functions  */
    private function scripts(){
        $this->assets->collection('jsPlugins')
            ->setTargetPath("dash/js/general.services.min.js")
            ->setTargetUri("dash/js/general.services.min.js")
            ->addJs("dash/js/services/services.js")
            ->join(true)
            ->addFilter(new \Phalcon\Assets\Filters\Jsmin());
    }
}