<?php
namespace Modules\Dashboard\Controllers;

use Modules\Models\CdLogStock;
use Modules\Models\CdProduct;
use Modules\Models\CdStock;
use Modules\Models\VProductBrand;
use Phalcon\Http\Request;

class PulledApartController extends ControllerBase{
    public function indexAction(){
        $auth = $this->auth();
        if($auth){
            $this->dataTable();
            $this->scripts();
            $find = VProductBrand::find("status='ACTIVE' order by pid desc");
            $this->view->setVar("products",$find);
        }else{
            $this->response(array("code"=>404,"message"=>"You do not have permission"),404);
        }
    }
    public function newAction(){
        $auth = $this->auth();
        if($auth){
            $this->validationJs();
            $this->scripts();
            $this->view->setVar("brid",json_decode($this->getBrandsJson(1),true));
        }else{
            $this->response(array("code"=>404,"message"=>"You do not have permission"),404);
        }
    }
    public function saveAction(){
        $auth = $this->auth();
        $request = new Request();
        if($auth && $request->isPost() && $request->isAjax()){
            $values = $request->getPost();
            if($values['pid']){
                $find = CdProduct::findFirst($values['pid']);
                $find->setName($values['name'])
                    ->setPermalink($values['permalink'])
                    ->setPurchasePrice($values['purchase_price'])
                    ->setSalePrice($values['sale_price'])
                    ->setMeasure($values['measure'])
                    ->setQuantity($values['quantity'])
                    ->setPercentage($values['percentage'])
                    ->setDateCreation(date('Y-m-d H:i:s'))
                    ->setStatus($values['status'])
                    ->setBrid($values['brid'])
                    ->setTicket($values['ticket']);
                if($find->update())$this->response(array("code"=>"200","message"=>"SUCCESS"),200);
            }else{
                $new = new CdProduct();
                $new->setName($values['name'])
                    ->setPermalink($values['permalink'])
                    ->setPurchasePrice($values['purchase_price'])
                    ->setSalePrice($values['sale_price'])
                    ->setMeasure($values['measure'])
                    ->setQuantity($values['quantity'])
                    ->setPercentage($values['percentage'])
                    ->setDateCreation(date('Y-m-d H:i:s'))
                    ->setStatus('ACTIVE')
                    ->setBrid($values['brid'])
                    ->setTicket($values['ticket']);
                if($new->save()){
                    $new_stock = new CdStock();
                    $new_stock->setStock(0)->setPid($new->getPid())->setStockMax(0)->setStockMin(0)->setQuantity(0)->setDateCreation(date("Y-m-d H:i:s"))
                        ->save();
                    $this->response(array("code"=>"200","message"=>"SUCCESS"),200);
                }
            }
            $this->response(array("code"=>"404","message"=>"Error to save cd_products"),200);
        }else{
            $this->response(array("code"=>404,"message"=>"You do not have permission"),404);
        }
    }
    public function printAction(){
        $auth = $this->auth();
        if($auth){
            $this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_ACTION_VIEW);
            $param = $this->dispatcher->getParam("id")?$this->dispatcher->getParam("id"):0;
            //$general_data = VPulledApartGeneral::findFirst("paid=$param");
            $general_data = VShoppingGeneral::findFirst("salid=$param");
            if(!$general_data) return $this->response->redirect("dashboard");

            $this->assets->collection('cssPlugins')->setTargetPath("dash/css/print_bill.min.css")->setTargetUri("dash/css/print_bill.min.css")->addCss("dash/css/print_bill.css")->join(true)->addFilter(new \Phalcon\Assets\Filters\Cssmin());
            $st_shopping_print = $this->setStShoppingPrint($general_data->getSalid());
            $this->view->setVar("list",$st_shopping_print);
            $this->view->setVar("data",$general_data);
        }else{
            $this->response(array("code"=>404,"message"=>"You do not have permission"),404);
        }
    }
}