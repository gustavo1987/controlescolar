<?php
namespace Modules\Dashboard\Controllers;
use Modules\Models\CdNotification;
use Modules\Models\CdUser;
use Modules\Models\CdClase;
use Modules\Models\CdClient;
use Modules\Models\CdClaseStudent;

class NotificationController extends ControllerBase{
    public function indexAction(){
        $auth = $this->auth();
        if($auth){
            $statusnoti = "INACTIVO";
            $this->scripts();
            $notification = CdNotification::find();
            if ($notification) {

                $statusnoti = "ACTIVO";
                $this->view->setVar("notification",$notification);
                $this->view->setVar("statusnoti",$statusnoti);
            $this->backView("Campus Virtual","/dashboard");
            }
            else{
                $this->view->setVar("statusnoti",$statusnoti);
            }
        }else{
            $this->response(array("code"=>404,"message"=>"You do not have permission"),404);
        }
    }
    public function editAction(){
        $param = $this->dispatcher->getParam("id");
        $auth = $this->auth();
        if($auth){
            $this->scripts();
            $noti = CdNotification::findFirst($param);
            $this->view->setVar("noti",$noti);
            $user = CdUser::findFirst($noti->getUid());
            $this->view->setVar("user",$user);
            $this->backView("Notificaciones","/dashboard/notification/index");
        }
        else{
            $this->response(array("code"=>404,"message"=>"You do not have permission"),404);
        }
    }
    public function newAction(){
        $auth = $this->auth();
        if($auth){
            $this->scripts();
            $student = CdUser::find("status='ACTIVE' and rol='STUDENT'");
            $this->view->setVar("student",$student);
            $teacher = CdUser::find("status='ACTIVE' and rol='TEACHER'");
            $this->view->setVar("teacher",$teacher);
            $classActive = CdClase::find("status='ACTIVO'");
            $this->view->setVar("classActive",$classActive);
            $this->backView("Notificaciones","/dashboard/notification/index");
        }
        else{
            $this->response(array("code"=>404,"message"=>"You do not have permission"),404);
        }  
    }
    public function saveAction(){
        $request = $this->request;
        $auth = $this->auth();
        if($request->isPost() && $request->isAjax() && $auth ){
            $values = $request->getPost();
            $porciones = explode("/", $values['birth_date']);
            $datepublic = date_create($porciones[0]."-".$porciones[1]."-".$porciones[2]);
            if($values['typeNotifi']==1){
                $iduser = $values['users'];
            }
            elseif($values['typeNotifi']==2){
                $iduser = $values['teacher'];
            }

            if($values['notttid']==1){
                $find = CdNotification::findFirst($values['notid']);
                $find->setNotification($values['notification'])
                    ->setStatus($values['status'])
                    ->setDateCreation(date("Y-m-d H:i:s"))
                    ->setDate(date_format($datepublic, 'Y-m-d H:i:s'))
                    ->setStatusView("WAIT")
                    ->setUid($values['users']);
                if(!$find->update())$this->response(array("code"=>404,"message"=>"Error to update cd_services"),200);
                $type = 2;
            }else{
                if($values['typeNotifi']==3){
                    $classNotificate = CdClaseStudent::find("clasid=".$values['classselect']);
                    foreach ($classNotificate as $key => $value) {
                        $userclient = CdClient::findFirst($value->getClid());
                        $find = new CdNotification();
                        $find->setNotification($values['notification'])
                            ->setStatus($values['status'])
                            ->setDateCreation(date("Y-m-d H:i:s"))
                            ->setDate(date_format($datepublic, 'Y-m-d H:i:s'))
                            ->setStatusView("WAIT")
                            ->setUid($userclient->getUid());  
                       if(!$find->save()){
                            $this->response(array("code"=>404,"message"=>"Error to save cd_brand class"),200);
                       }
                    }
                    $type = 1;
                }
                else{
                    $find = new CdNotification();
                    $find->setNotification($values['notification'])
                        ->setStatus($values['status'])
                        ->setDateCreation(date("Y-m-d H:i:s"))
                        ->setDate(date_format($datepublic, 'Y-m-d H:i:s'))
                        ->setStatusView("WAIT")
                        ->setUid($iduser);
                        //->setUid($values['users']);
                    if(!$find->save())$this->response(array("code"=>404,"message"=>"Error to save cd_brand"),200);
                    $type = 1; 
                }
            }
            $this->response(array("code"=>200,"message"=>"SUCCESS","type"=>$type,"result"=>array("id"=>$find->getNotid())),200);
        }else{
            $this->response(array("code"=>404,"message"=>"You do not have permission"),404);
        }
    }
    public function deleteAction(){
        $request = $this->request;
        $auth = $this->auth();
        if($request->isPost() && $request->isAjax() && $auth ){
            $id = $request->getPost("id");
            $find = CdNotification::findFirst($id);
            try{
                if($find->delete()){
                    $this->response(array("message"=>"SUCCESS","code"=>"200"),200);
                }
            }catch (\Exception $e){
                $this->response(array("message"=>"SUCCESS","code"=>"300"),200);
            }
        }else{
            $this->response(array("code"=>404,"message"=>"You do not have permission"),404);
        }
    }
    private function scripts(){
        $this->assets->collection('jsPlugins')
            ->setTargetPath("dash/js/general.notification.min.js")
            ->setTargetUri("dash/js/general.notification.min.js")
            ->addJs("dash/js/plugins/BootstrapDP/bootstrap-datepicker.min.js")
            ->addJs("dash/js/plugins/BootstrapDP/bootstrap-datepicker.es.min.js")
            ->addJs("dash/js/notification/notification.js")
            ->join(true)
            ->addFilter(new \Phalcon\Assets\Filters\Jsmin());
    }
}