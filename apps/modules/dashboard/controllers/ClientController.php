<?php
namespace Modules\Dashboard\Controllers;

use Modules\Models\CdBilling;
use Modules\Models\CdClient;
use Modules\Models\CdMunicipios;
use Modules\Models\VClientDB;
use Modules\Models\CdUser;
use Modules\Models\CdBrand;
use Modules\Models\cms\CdPost;
use Modules\Models\cms\CdDocument;
use Modules\Models\VShoppingList;
use Modules\Models\cms\CdStudent;
use Modules\Models\cms\CdInscripciones;
use Modules\Models\cms\VStudentInscription;
use Modules\Models\CdSaleClient;
use \Modules\Models\VStudentClase;
use Phalcon\Http\Request;
require dirname(dirname(dirname(dirname(__FILE__))))."/library/PHPMailer/PHPMailerAutoload.php";

class ClientController extends ControllerBase{
    public function indexAction(){
        $auth = $this->auth();
        $this->backView("Campus Virtual","/dashboard");
        if($auth){
            $this->dataTable();
            $clients = null;
            if($auth['rol']!=="ADMIN")
                $clients = CdClient::find("status='ACTIVO' and uid={$auth['uid']}");
            else if($auth['rol']=="ADMIN"){
                $clients = CdClient::find("status='ACTIVO'");
            }
            $this->view->setVar("clients",$clients);
        }else{
            $this->response(array("message"=>"error"),404);
        }
    }
    public function aulaAction(){
        $salid = $this->dispatcher->getParam("salid","int");
        $subsalid = $this->dispatcher->getParam("subsalid","int");
        $student = VStudentClase::find("salid=".$salid);
        $this->view->setVar("student",$student);
        $this->view->setVar("salid",$salid);
        $this->view->setVar("subsalid",$subsalid);
    }
    public function studentAction(){
        $auth = $this->auth();
        $id = $this->dispatcher->getParam("id","int");
        $this->scripts();
        if($auth && $id){
            $student = VStudentInscription::findFirst("stuid=".$id);
            $this->validationJs();
            $this->scripts();
            $documents = CdDocument::find("stuid=".$id);
            $this->view->setVar("documents",$documents);
            $this->view->setVar("inscripciones",$student);
        }else{$this->response(array("message"=>"error"),404);}
    }
    public function generacionAction(){
        $salid = $this->dispatcher->getParam("salid","int");
        $subsalid = $this->dispatcher->getParam("subsalid","int");
        $student = VShoppingList::find("salid=".$salid." and subsalid=".$subsalid);
        $this->view->setVar("student",$student);
    }
    public function clasestudianteAction(){
        $clid = $this->dispatcher->getParam("clid","int");
        $salid = $this->dispatcher->getParam("salid","int");
        $subsalid = $this->dispatcher->getParam("subsalid","int");
        $estudiante = VStudentClase::findFirst("clid=".$clid." and salid=".$salid);
        $clase = VShoppingList::findFirst("salid=".$salid);
        $this->view->setVar("client",$estudiante);
        $this->view->setVar("clid",$clid);
        $this->view->setVar("salid",$salid);
        $this->view->setVar("clase",$clase);
        $this->view->setVar("subsalid",$subsalid);
    }
    public function workAction(){
        $student = CdStudent::find("status='ADMISION'");
        $this->view->setVar("student",$student);
    }
    public function admisionAction(){
        $student = CdStudent::find("status='ADMISION'");
        $students = VStudentInscription::find("status='ADMISION'");
        $this->view->setVar("student",$student);
        $this->view->setVar("students",$students);
    }
    public function admisioncreateAction(){
        $auth = $this->auth();
        $this->scripts();
        $this->backView("Admisión","/dashboard/client/all");
        if($auth){
            $postgrados = CdBrand::find("status='ACTIVE'");
            $this->view->setVar("postgrados",$postgrados);
        }
    }
    public function allAction(){
        $this->dataTable();
        $students = VStudentInscription::find("(sex='F' or sex='M') order by stuid desc");
        $this->view->setVar("students",$students);
        $this->backView("Campus Virtual","/dashboard");
    }
    public function reciboAction(){
        $rol = $auth["rol"];
        $title = "RECIBOS DE PAGO";
        $url ="/dashboard/client/recibo";
        $description = "IDE Iberoamerica";
        $image = "/dash/img/logo-100.png";
        $this->metaHome($title,$url,$description,$image);
        $student = CdStudent::find("status='RECIBO'");
        $students = VStudentInscription::find("status='RECIBO' or status='ADMITIDO'");
        $this->view->setVar("student",$student);
        $this->view->setVar("students",$students);
    }
    public function inscripcionAction(){
            $students = VStudentInscription::find("status='REGISTRO'");
            $this->view->setVar("student",$student);
            $this->view->setVar("students",$students);
    }
    public function viewoneAction(){
        $auth = $this->auth();
        $id = $this->dispatcher->getParam("id","int");
        $this->scripts();
        if($auth && $id){
            $student = CdStudent::findFirst("stuid=".$id);
            $this->validationJs();
            $this->scripts();
            $this->view->setVar("inscripciones",$student);
        }else{$this->response(array("message"=>"error"),404);}
    }
    public function editadmisionAction(){
        $auth = $this->auth();
        $id = $this->dispatcher->getParam("id","int");
        $this->backView("Admisión","/dashboard/client/all");
        $this->scripts();
        if($auth && $id){
            $student = VStudentInscription::findFirst("stuid=".$id);
            $this->validationJs();
            $this->scripts();
            $documents = CdDocument::find("stuid=".$id);
            $this->view->setVar("documents",$documents);
            $this->view->setVar("inscripciones",$student);
        }else{$this->response(array("message"=>"error"),404);}
    }
    public function editreciboAction(){
        $auth = $this->auth();
        $id = $this->dispatcher->getParam("id","int");
        $this->scripts();
        if($auth && $id){
            $student = VStudentInscription::findFirst("stuid=".$id);
            $this->validationJs();
            $this->scripts();
            $documents = CdDocument::find("stuid=".$id." and type='RECIBO' order by date_creation desc limit 1");
            $this->view->setVar("documents",$documents);
            $this->view->setVar("inscripciones",$student);
        }else{$this->response(array("message"=>"error"),404);}
    }
    public function convertirestudianteAction(){
        $request = new Request();
        $auth = $this->auth();
        $values = $request->getPost();
        if($request->isPost() && $request->isAjax() && $auth){
        $admision = new CdStudent();
        $admision->setName($values["name"])
                ->setLastName($values["last_name"])
                ->setSecondName($values["second_name"])
                ->setEmail($values["email"])
                ->setPhone($values["phone"])
                ->setSex($values["sex"])
                ->setCalle($values["street"])
                ->setNumExt($values["number_ext"])
                ->setNumInt($values["number_int"])
                ->setCp($values["cp"])
                ->setCiudad($values["ciudad"])
                ->setEstado($values["estado"])
                ->setPais($values["pais_nacimiento"])
                ->setDatecreation(date("Y-m-d H:m:s"))
                ->setActa($values["acta"])
                ->setUsername($values["name"])
                ->setPassword($this->security->hash("ideiberoamerica2019"))
                ->setStatus("REGISTRO COMPLETO")
                ->setPostgrado($values["posgrado"])
                ->setCareer($values["posgrado"]);
        $cat=0;
        if($values["posgrado"]==(38||39))$cat=4;
        else $cat=5;
        if($admision->save()){
            $inscripcion = new CdInscripciones();

            $arraydate = explode('/', $values["fecha_nacimiento"]);
            $datebeging = $arraydate[2]."-".$arraydate[1]."-".$arraydate[0]." 00:00:00";


            $arraydate = explode('/', $values["fecha_finalizacion"]);
            $datestudio = $arraydate[2]."-".$arraydate[1]."-".$arraydate[0];

            $inscripcion->setCatid($cat)
                ->setEstudio($values["posgrado"])
                ->setPeriodo($values["periodo"])
                ->setAcepto("SI")
                ->setStuid($admision->getStuid())
                ->setNacionalidad($values["nacionalidad"])
                ->setDocumentoAcreditacion($values["documento_acreditacion"])
                ->setNumeroDocumentoAcreditacion($values["numero_documento_acreditacion"])
                ->setFechaNacimiento($datebeging)
                ->setPaisNacimiento($values["pais_nacimiento"])
                ->setDomicilioNacimiento($values["domicilio_nacimiento"])
                ->setCpNacimiento($values["cp_nacimiento"])
                ->setCiudadNacimiento($values["ciudad_nacimeinto"])
                ->setEstadoNacimiento($values["estado_nacimiento"])
                ->setUniversidad($values["universidad"])
                ->setPaisEstudios($values["pais_estudios"])
                ->setNombreTitulo($values["nombre_titulo"])
                ->setTipoTitulo($values["tipo_titulo"])
                ->setEstadoEstudios($values["estado_estudios"])
                ->setAnioCursado($values["anio_cursado"])
                ->setIsOver("true")
                ->setFechaFinalizacion($datestudio)
                ->setEstatus("COMPLETO")
                ->setTelefonoResidencia($values["telefono_residencia"]);


            if($inscripcion->save()){
                $acta = new CdDocument();
                $acta->setName($values["acta"])
                    ->setType("ACTA")
                    ->setDateCreation(date("Y-m-d H:m:s"))
                    ->setStuid($admision->getStuid())
                    ->save();
                $titulo = new CdDocument();
                $titulo->setName($values["titulo"])
                    ->setType("TITULO")
                    ->setDateCreation(date("Y-m-d H:m:s"))
                    ->setStuid($admision->getStuid())
                    ->save();
                $cedula = new CdDocument();
                $cedula->setName($values["cedula"])
                    ->setType("CEDULA")
                    ->setDateCreation(date("Y-m-d H:m:s"))
                    ->setStuid($admision->getStuid())
                    ->save();
                $recibo = new CdDocument();
                $recibo->setName($values["recibo"])
                    ->setType("RECIBO")
                    ->setDateCreation(date("Y-m-d H:m:s"))
                    ->setStuid($admision->getStuid())
                    ->save();
                $photo = new CdDocument();
                $photo->setName($values["photo"])
                    ->setType("FOTOGRAFIA")
                    ->setDateCreation(date("Y-m-d H:m:s"))
                    ->setStuid($admision->getStuid())
                    ->save();
                $identificacion = new CdDocument();
                $identificacion->setName($values["identificacion"])
                    ->setType("IDENTIFICACION")
                    ->setDateCreation(date("Y-m-d H:m:s"))
                    ->setStuid($admision->getStuid())
                    ->save();
                $curriculum = new CdDocument();
                $curriculum->setName($values["curriculum"])
                    ->setType("CURRICULUM")
                    ->setDateCreation(date("Y-m-d H:m:s"))
                    ->setStuid($admision->getStuid())
                    ->save();
                $curp = new CdDocument();
                $curp->setName($values["curp"])
                    ->setType("CURP")
                    ->setDateCreation(date("Y-m-d H:m:s"))
                    ->setStuid($admision->getStuid())
                    ->save();
                $certificado = new CdDocument();
                $certificado->setName($values["certificado"])
                    ->setType("CERTIFICADO")
                    ->setDateCreation(date("Y-m-d H:m:s"))
                    ->setStuid($admision->getStuid())
                    ->save();
                $cedula = new CdDocument();
                $cedula->setName($values["cedula"])
                    ->setType("CEDULA")
                    ->setDateCreation(date("Y-m-d H:m:s"))
                    ->setStuid($admision->getStuid())
                    ->save();
                $this->response(array("code"=>200,"message"=>"ok"),200);
            }else{
                $this->response(array("code"=>300,"message"=>"Error, to save inscription"),200);
            }
        }
        else{
            $this->response(array("message"=>"Error to save student","code"=>"404"),404);
        }

        }
        else{
            $this->response(array("message"=>"Error try again","code"=>"404"),404);
        }
        
    }
    public function convertirestudianteXXXXXXAction(){
        $request = new Request();
        $auth = $this->auth();
        $values = $request->getPost();
        $clid = $values["clid"];
        if($request->isPost() && $request->isAjax() && $auth){
        $admision = new CdStudent();
        $admision->setName($values["name"])
                ->setLastName($values["last_name"])
                ->setSecondName($values["second_name"])
                ->setEmail($values["email"])
                ->setPhone($values["phone"])
                ->setSex($values["sex"])
                ->setCalle($values["calle"])
                ->setNumExt($values["num_ext"])
                ->setNumInt($values["num_int"])
                ->setCp($values["cp"])
                ->setCiudad($values["ciudad"])
                ->setEstado($values["estado"])
                ->setPais($values["pais"])
                ->setDatecreation($values["datecreation"])
                ->setComproba($values["comproba"])
                ->setActa($values["acta"])
                ->setUsername($values["username"])
                ->setPassword($values["password"])
                ->setStatus($values["status"])
                ->setPostgrado($values["posgrado"])
                ->setCareer($values["career"]);

        $cat=0;
        if($values["posgrado"]==(38||39))$cat=4;
        else $cat=5;
        if($admision->save()){
            $inscripcion = new CdInscripciones();
            $inscripcion->setCat($cat)
                ->setEstudio($values["posgrado"])
                ->setPeriodo($values["periodo"])
                ->setAcepto("SI")
                ->setStuid($admision->getStuid())
                ->setNacionalidad($values["nacionalidad"])
                ->setDocumentoAcreditacion($values["documento_acreditacion"])
                ->setNumeroDocumentoAcreditacion($values["numero_documento_acreditacion"])
                ->setFechaNacimiento($values["fecha_nacimiento"])
                ->setPaisNacimiento($values["pais_nacimiento"])
                ->setDomicilioNacimiento($values["domicilio_nacimiento"])
                ->setCpNacimiento($values["cp_nacimiento"])
                ->setCiudadNacimiento($values["ciudad_nacimiento"])
                ->setTelefonoResidencia($values["telefono_residencia"]);


            if($inscripcion->save()){
                $this->response(array("code"=>200,"message"=>"ok"),200);
            }else{
                $this->response(array("code"=>300,"message"=>"Error, to save inscription"),200);
            }
        }
        else{
            $this->response(array("message"=>"Error to save student","code"=>"404"),404);
        }

        }
        else{
            $this->response(array("message"=>"Error try again","code"=>"404"),404);
        }
        
    }

    public function addstudentAction(){
        $request = new Request();
        $auth = $this->auth();
        $values = $request->getPost();
        $clid = $values["clid"];
        if($request->isPost() && $request->isAjax() && $auth){
            
            $admision = CdStudent::findFirst($clid);
            $client = new CdClient();
            $userstudent = new CdUser();
            $client->setIdentifier("".$values['name'][0]."".$values['last_name'][0]."".date("Y")."".date("m")."".date("d")."".date("H")."".date("i")."".date("s"))
                ->setName($values['name'])
                ->setLastName($values['last_name'])
                ->setSecondName($values['second_name'])
                ->setSex($values['sex'])
                ->setBirthdate(date("Y-m-d H:i:s"))
                ->setEmail($values['email'])
                ->setTel($values['tel'])
                ->setStatus("STUDENT")
                ->setDateCreation(date("Y-m-d H:i:s"))
                ->setUid($auth['uid'])
                ->setCarrera($admision->getPostgrado());

            $contrasena = strtolower($request->getPost("last_name"))."".strtolower($request->getPost("second_name"));
            $result=str_replace(" ","-",$values['name']);
            $userstudent->setName($request->getPost("name"))
                ->setLastName($request->getPost("last_name"))
                ->setSecondName($request->getPost("second_name"))
                ->setSex($request->getPost("sex"))
                ->setPhone($request->getPost("tel"))
                ->setUsername($result)
                ->setEmail($request->getPost("email"))
                ->setPhoto("no-image.jpg")
                ->setPassword($this->security->hash($contrasena))
                ->setRol("STUDENT")
                ->setStatus("ACTIVE")
                ->setDateCreation(date("Y-m-d H:i:s"));

            $inscripcion = CdInscripciones::findFirst("stuid=".$clid);
            $inscripcion->setEstatus("FINALIZADO");

            if($client->save()&&$userstudent->save()&&$inscripcion->update()){
                $this->sendEmail($client->toArray());
                $admision->setStatus("FINALIZADO")
                        ->update();
                $this->response(array("code"=>200,"message"=>"ok"),200);
            }else{
                $this->response(array("code"=>300,"message"=>"Error, try again"),200);
            }
        }else{
            $this->response(array("message"=>"Error try again","code"=>"404"),404);
        }
        
    }

    private function sendEmail($values){
        $email = new \PHPMailer();
        $email->isSMTP();
        $email->Host = "smtp.gmail.com";
        $email->Port=587;
        $email->CharSet = 'UTF-8';
        $email->SMTPSecure="tls";
        $email->SMTPAuth =true;
        $email->Username = "enviodecorreoscdevelopers@gmail.com";
        $email->Password = "cdevelopers2018";
        $email->setFrom("contactanos@ideiberoamerica.com",'Admisión en Instituto Iberoamericano de Derecho Electoral');
        $email->addReplyTo("contactanos@ideiberoamerica.com",'Admisión en Instituto Iberoamericano de Derecho Electoral');
        $email->addAddress($values['email']);
        $email->WordWrap =100;
        $email->isHTML(true);
        $email->Subject = "Haz sido admitido, realiza tu pago, gracias";
        $emailaceptado = $values['email'];
        /*$html = file_get_contents($this->url->get("/dashboard/enviar-pago"));
        $email->msgHTML($html);*/
        $html = dirname(__DIR__)."/views/email/enviopago.html";
        $name = "gustavo";
        print_r($name);
        $html = file_get_contents($html);
        $html=str_replace("[_name_to_replace_]",ucfirst($name), $html);
        $html=str_replace("[_nameposgrado_to_replace_]",$values['carrera'], $html);
        $email->msgHTML($html);
        //$html = file_get_contents($this->url->get("/dashboard/enviar-pago?name=$name&email=$emailaceptado"));
        
        
        $email->AltBody = "Admisión en Instituto Iberoamericano de Derecho Electoral";
        
        if(!$email->send()) {
            echo 'Message could not be sent.';
            echo 'Mailer Error: ' . $email->ErrorInfo;
        } else {
            return true;
        }
    }
    public function emailaceptado22Action(){
        /*$email = $_GET['email'];
        $client = VStudentInscription::findFirst("email='".$email."'");
        $this->view->setVar("email",$email);
        $this->view->setVar("client",$client);*/
        
    }
    public function inactiveAction(){
        $auth = $this->auth();
        if($auth){
            $clients = CdClient::find("status='INACTIVE'");
            $this->view->setVar("clients",$clients);
        }else{
            $this->response(array("message"=>"error"),404);
        }
    }
    public function newAction(){
        $auth = $this->auth();
        if($auth){
            $this->validationJs();
            $this->scripts();
            $carreras = CdBrand::find("status='ACTIVE'");
            $this->view->setVar("carreras",$carreras);
            $this->view->setVar("estados",json_decode($this->getEstadosJson(),true));
        }else{$this->response(array("message"=>"error"),404);}
    }
    public function editAction(){
        $auth = $this->auth();
        $id = $this->dispatcher->getParam("id","int");
        if($auth && $id){
            $find = CdStudent::findFirst("clid=$id");
            if($find){
                $this->validationJs();
                $this->scripts();
                $this->view->setVar("client",$find);
                $this->view->setVar("estados",json_decode($this->getEstadosJson(),true));
                $this->view->setVar("municipios",json_decode($this->getMunicipiosJson(),true));
            }else{
                $this->response->redirect("dashboard/clients");
            }
        }else{$this->response(array("message"=>"error"),404);}
    }
    public function saveAction(){
        $request = new Request();
        $auth = $this->auth();
        if($request->isPost() && $request->isAjax() && $auth){
            $client = new CdClient();
            $values = $request->getPost();
            $iva = $values["iva"]=="mx"?"SI":"NO";
            $bird_date = $values['birth_date']?$this->getFormatDate($values['birth_date']):"";
            $client->setIdentifier(uniqid()."-".date("Y"))
                ->setName($values['name'])
                ->setLastName($values['last_name'])
                ->setSecondName($values['second_name'])
                ->setSex($values['sex'])
                ->setBirthdate($bird_date)
                ->setEmail($values['email'])
                ->setTel($values['tel'])
                ->setDateCreation(date("Y-m-d H:i:s"))
                ->setStatus($values["status"])
                ->setUid($auth['uid']);
            //Save to new Client
            if($client->save()){
                if($iva==="SI"){
                    $billing =  new CdBilling();
                    $billing->setRfc(strtoupper($values['rfc']))
                        ->setBussinessName($values['business_name'])
                        ->setStreet($values['street'])
                        ->setNumberExt($values['number_ext'])
                        ->setNumberInt($values['number_int'])
                        ->setColony($values['colony'])
                        ->setLocation($values['location'])
                        ->setPostalCode($values['postal_code'])
                        ->setEmail($values['email_fiscal'])
                        ->setIva($iva)
                        ->setMpid($values['mpid_fiscal'])
                        ->setClid($client->getClid())
                        ->setDateCreation(date("Y-m-d H:s:i"));
                    if(!$billing->save())$this->response(array("code"=>300,"message"=>"Error to save cd_billing, try again"),200);
                }
                /**$cd_direction = new CdDirection();
                $cd_direction->setStreet($values['street_dir'])
                    ->setColony($values['colony_dir'])
                    ->setCity($values['city_dir'])
                    ->setCp($values['cp'])
                    ->setMpid($values['mpid'])
                    ->setClid($client->getClid());
                if(!$cd_direction->save()) $this->response(array("code"=>300,"message"=>"Error to save cd_direction, try again"),200);*/

                $name = $client->getName()." ".$client->getLastName()." ".$client->getSecondName();
                $names = explode(" ",$name);
                $name = null;
                for($i=0;$i<count($names);$i++){
                    $arr1 = str_split($names[$i]);
                    $name = strtoupper($name.$arr1[0]);
                }
                $date = explode("-",date("d-m-Y", strtotime($client->getDateCreation())));
                $date = $date[0].$date[1].$date[2];
                $id = $client->getClid();
                $identifier = $name.$date.$id;
                //Update client with Identifier unique
                if($client->setIdentifier($identifier)->update()){
                    $this->response(array("code"=>200,"message"=>"ok"),200);
                };
            }else{
                /*foreach ($client->getMessages() as $message) {
                    $this->flash->error((string) $message);
                }*/
                $this->response(array("code"=>300,"message"=>"Error, try again"),200);
            }
        }else{
            $this->response(array("message"=>"Error try again","code"=>"404"),404);
        }
    }
    public function updateAction(){
        $request = new Request();
        $auth = $this->auth();
        if($request->isPost() && $request->isAjax() && $auth){
            $values = $request->getPost();
            $clid = $values["clid"];
            $client = CdClient::findFirst($clid);
            $iva = $values["iva"]=="mx"?"SI":"NO";
            $bird_date = $values['birth_date']?$this->getFormatDate($values['birth_date']):"";
            $client->setName($values['name'])
                ->setLastName($values['last_name'])
                ->setSecondName($values['second_name'])
                ->setSex($values['sex'])
                ->setBirthdate($bird_date)
                ->setEmail($values['email'])
                ->setTel($values['tel'])
                ->setInstitution($values['institution'])
                ->setDiscount($values['discount'])
                ->setStatus($values["status"])
                ->setUid($auth['uid']);
            //Save to new Client
            if($client->update()){
                $new_billing =  new CdBilling();
                $billing = $new_billing->findFirst($values['bllid']);
                if($billing){
                    $billing->setRfc(strtoupper($values['rfc']))
                        ->setBussinessName($values['business_name'])
                        ->setStreet($values['street'])
                        ->setNumberExt($values['number_ext'])
                        ->setNumberInt($values['number_int'])
                        ->setColony($values['colony'])
                        ->setLocation($values['location'])
                        ->setPostalCode($values['postal_code'])
                        ->setEmail($values['email_fiscal'])
                        ->setIva($iva)
                        ->setMpid($values['mpid_fiscal'])
                        ->setClid($client->getClid());
                    if(!$billing->update())$this->response(array("code"=>300,"message"=>"Error to save cd_update, try again"),200);
                }
                /*
                $cd_direction = CdDirection::findFirst($values['did']);
                $cd_direction->setStreet($values['street_dir'])
                    ->setColony($values['colony_dir'])
                    ->setCity($values['city_dir'])
                    ->setCp($values['cp'])
                    ->setMpid($values['mpid']);
                if(!$cd_direction->update()) $this->response(array("code"=>300,"message"=>"Error to save update_direction, try again"),200);
                */
                $this->response(array("code"=>200,"message"=>"ok"),200);
            }else{
                /*foreach ($client->getMessages() as $message) {
                    $this->flash->error((string) $message);
                }*/
                $this->response(array("code"=>300,"message"=>"Error, try again"),200);
            }
        }else{
            $this->response(array("message"=>"Error try again","code"=>"404"),404);
        }
    }
    public function validateEmailAction(){
        $request = $this->request;
        if($request->isPost() && $request->isAjax()){
            $email = $this->request->getPost("email");
            $validation = $this->request->getPost("validation");
            if($validation==$email)$this->response(array('valid' => true),200);
            $find  = CdClient::findFirst("email='$email'");
            if($find)$this->response(array('valid' => false),200);
            $this->response(array('valid' => true),200);
        }
        else{
            $this->response(array("message"=>"error try again","code"=>"404"),404);
        }
    }
    public function validateRfcAction(){
        $request = $this->request;
        if($request->isPost() && $request->isAjax()){
            $rfc = $this->request->getPost("rfc");
            $validation = $this->request->getPost("validation");
            if($validation==$rfc)$this->response(array('valid' => true),200);
            $find  = CdBilling::findFirst("rfc='$rfc'");
            if($find)$this->response(array('valid' => false),200);
            $this->response(array('valid' => true),200);
        }
        else{
            $this->response(array("message"=>"error try again","code"=>"404"),404);
        }
    }
    /* Control Client */
    public function controlAction(){
       $auth = $this->auth();
        $cid = $this->dispatcher->getParam("id","int");
        if($auth && $cid){
            $find  = Vclient::findFirst("clid=$cid");
            if($find){
                $scholarship = CdScholarship::findFirst("clid=$cid");
                $this->getScroll();
                $hourInterest = $this->getHourInterest();
                $this->view->setVar("client",$find);
                $this->view->setVar("birthdate",$this->getBirthDate($find->getBirthdate()));
                $this->view->setVar("hourInterest",$hourInterest[$find->getHourInterest()]);
                $this->view->setVar("scholarship",$scholarship);
                $this->view->setVar("followUp",CdFollowUp::find("clid=$cid order by date_creation desc"));
                $this->view->setVar("call",$this->getCall());
                $this->view->setVar("icon",$this->getIcon());
                $this->view->setVar("status",$this->getStatus());
                $this->view->setVar("dateSpanish",$this->dateSpanish());
            }else{
                $this->response->redirect("dashboard/clients");
            }
        }else{
            $this->response(array("message"=>"Error try again","code"=>"404"),404);
        }
    }
    public function saveControlAction(){
        $auth = $this->auth();
        $request = new Request();
        $redirect = 0;
        if($auth && $request->isAjax() && $request->isPost()){
            $values = $request->getPost();
            $follow_up = new CdFollowUp();
            $follow_up->setObservations($values["observations"])
                ->setCall($values['call'])
                ->setCallStatus($values['call_status'])
                ->setDateCreation(date("Y-m-d H:i:s"))
                ->setClid($values['clid'])
                ->setUid($auth['uid']);
            if($follow_up->save()){
                $this->changeStatusClient($values['call_status'],$values['cid']);
                $date = $this->getBirthDate(date("Y-m-d",strtotime($follow_up->getDateCreation())));
                $content = array(
                    "observations"=>$follow_up->getObservations(),
                    "call"=>$follow_up->getCall(),
                    "call_status"=>$follow_up->getCallStatus(),
                    "date"=>$date["day"]." ".$date['month']." ".$date['year'],
                    "hour"=> date("H:i",strtotime($follow_up->getDateCreation()))
                );
                if($values['call_status']=="INSCRITO" && $values["scholarship"]=="si"){
                    $this->scholarship($values,$follow_up->getClid());
                    $redirect=1;
                }
                $this->response(array("content"=>$content,"message"=>"data saved","code"=>200,"redirect"=>$redirect),200);
            }else{
                $this->response(array("message"=>"Error try again","code"=>"404"),200);
            }
        }else{
            $this->response(array("message"=>"Error try again","code"=>"404"),404);
        }
    }
    public function saveScholarshipControlAction(){
        $auth = $this->auth();
        $request = new Request();
        $values = $request->getPost();
        if($auth && $request->isAjax() && $request->isPost()){
            if($this->scholarship($values,$values["cid_client"])){
                $this->response(array("message"=>"data saved","code"=>200),200);
            }else{
                $this->response(array("message"=>"Error try again","code"=>"404"),200);
            }
        }else{
            $this->response(array("message"=>"Error try again","code"=>"404"),404);
        }
    }
    private function scholarship($values,$clid){
        $auth = $this->auth();
        $scholarship = CdScholarship::findFirst("clid=$clid");
        if($scholarship){
            $scholarship->setPercentage($values['percentage'])
                ->setDescription($values['observations-scho'])
                ->setUid($auth['uid'])
                ->setClid($clid);
            return $scholarship->update();
        }else{
            $scholarship = new CdScholarship();
            $scholarship->setPercentage($values['percentage'])
                ->setDescription($values['observations-scho'])
                ->setDateCreation(date("Y-m-d H:s:i"))
                ->setUid($auth['uid'])
                ->setClid($clid);
            return $scholarship->save();
        }
    }
    private function changeStatusClient($status,$cid){
        $client = CdClient::findFirst($cid);
        switch($status){
            case "SEGUIMIENTO" :$client->setStatus("SEGUIMIENTO"); $client->update();break;
            case "CITA":$client->setStatus("CITA"); $client->update();break;
            case "ATENDIDO":$client->setStatus("CITA"); $client->update();break;
            case "INSCRITO":
                $client->setStatus("INSCRITO"); $client->update();
                break;
            case "PERDIDO":$client->setStatus("PERDIDO"); $client->update();break;
            default : break;
        }
    }
    public function getMunicipiosAction(){
        $auth = $this->auth();
        $request = new Request();
        $estado = $request->getPost("estado");
        if($auth && $request->isPost() && $request->isAjax() && $estado){
            $municipios = CdMunicipios::find("eid=$estado");
            $content = array();
            foreach($municipios as $key =>$values){
                $content[] = array("value"=>$values->getNombre(),"id"=>$values->getMpid());
            }
            $this->response(array("message"=>"SUCCESS","result"=>$content,"code"=>200),200);
        }else{
            $this->response(array("message"=>"Unauthorized","code"=>404),404);
        }
    }
    private function getBirthDate($date){
        $newDate = explode("-",$date);
        $dateSpanish = $this->dateSpanish();
        return $newDate = array("year"=>$newDate["0"],"month"=>$dateSpanish[$newDate["1"]],"day"=>$newDate["2"]);
    }
    private function getIcon(){
        return array(
            "SEGUIMIENTO"=>"fa-user",
            "CITA"=>"fa-clock-o",
            "ATENDIDO"=>"fa-users",
            "PERDIDO"=>"fa-user-times",
            "INSCRITO"=>"fa-trophy"
        );
    }
    private function getCall(){
        return array(1=>"EFECTIVA",2=>"NO EFECTIVA",3=>"NO MARCO");
    }
    private function getStatus(){
        return array(
            "SEGUIMIENTO"=>"-warning",
            "CITA"=>"-primary",
            "ATENDIDO"=>"-info",
            "PERDIDO"=>"-danger",
            "INSCRITO"=>"-success",
            "NUEVO"=>"-default",
        );
    }
    private function getHourInterest(){
        return array(1=>"LUNES A VIERNES",2=>"SÁBADOS",3=>"DOMINGOS");
    }
    private function getScroll(){
        $this->assets->collection('jsScroll')
            ->setTargetPath("dash/js/plugins/scroll/umaee.scroll.min.js")
            ->setTargetUri("dash/js/plugins/scroll/umaee.scroll.min.js")
            ->addJs("dash/js/plugins/scroll/perfect-scrollbar.min.js")
            ->addJs("dash/js/plugins/scroll/umaee.scroll.js")->join(true)
            ->addFilter(new \Phalcon\Assets\Filters\Jsmin());;
    }

    /* Other functions  */
    private function scripts(){
        $this->assets->collection('jsPlugins')
            ->setTargetPath("dash/js/general.clients.min.js")
            ->setTargetUri("dash/js/general.clients.min.js")
            ->addJs("dash/js/plugins/select/select2.min.js")
            ->addJs("dash/js/plugins/select/es.js")
            ->addJs("dash/js/plugins/moment.min.js")
            ->addJs("dash/js/plugins/BootstrapDP/bootstrap-datepicker.min.js")
            ->addJs("dash/js/plugins/BootstrapDP/bootstrap-datepicker.es.min.js")
            ->addJs("dash/js/clients/clients.js")
            ->join(true)
            ->addFilter(new \Phalcon\Assets\Filters\Jsmin());
    }
}