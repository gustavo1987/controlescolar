<?php
namespace Modules\Dashboard\Controllers;

use Modules\Models\CdClient;
use Modules\Models\CdUser;
use Modules\Models\CdClase;
use Modules\Models\CdClaseStudent;
use Modules\Models\ChatRooms;
use Phalcon\Http\Request;

class ChatController extends ControllerBase{
    public function indexAction(){
        $auth = $this->auth();
        $id=$auth["uid"];

        $client = CdClient::findFirst("uid=".$id);
        $find = CdClaseStudent::findFirst("clid=".$client->getClid()." and status='ACTIVO'");
        $clase = CdClase::findFirst($find->getClasid());
        $name = $clase->getChat();
    	$this->scripts();
    	//session_start();
	    if ($name && $client->getClid()){
	    	$getRooms = ChatRooms::findFirst("name='".$name."'");
        	//print_r($getRooms->toArray());exit;
        	$file = $getRooms->getFile();

	        $this->view->setVar("file",$file);
	        $this->view->setVar("name",$name);
	        $this->view->setVar("clid",$client->getClid());
	    }    

    }
    public function processAction(){


		$function = htmlentities(strip_tags($_POST['function']), ENT_QUOTES);
		$file = htmlentities(strip_tags($_POST['file']), ENT_QUOTES);
		$log = array();
		switch ($function) {
			 case ('getState'):
		    	 if (file_exists($file)) {
		           $lines = file($file);
		    	 }
		         $log['state'] = count($lines);
		    	 break;	
			 case ('send'):
			     $nickname = htmlentities(strip_tags($_POST['nickname']), ENT_QUOTES);
			     $patterns = array("/:\)/", "/:D/", "/:p/", "/:P/", "/:\(/");
				 $replacements = array("<img src='smiles/smile.gif'/>", "<img src='smiles/bigsmile.png'/>", "<img src='smiles/tongue.png'/>", "<img src='smiles/tongue.png'/>", "<img src='smiles/sad.png'/>");
				 $reg_exUrl = "/(http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/";
				 $blankexp = "/^\n/";
				 $message = htmlentities(strip_tags($_POST['message']), ENT_QUOTES);
				 if (!preg_match($blankexp, $message)) {
					 if (preg_match($reg_exUrl, $message, $url)) {
		       			$message = preg_replace($reg_exUrl, '<a href="'.$url[0].'" target="_blank">'.$url[0].'</a>', $message);
					 } 
					 $message = preg_replace($patterns, $replacements, $message);
		             fwrite(fopen($file, 'a'), $nickname . "~\t~" . $message = str_replace("\n", " ", $message) . "\n");  
				 }
		    	 break;
		}
		echo json_encode($log);

    }
    public function updateAction(){
	    $state = htmlentities(strip_tags($_GET['state']), ENT_QUOTES);
	    $file = htmlentities(strip_tags($_GET['file']), ENT_QUOTES);
	    			
	    $finish = time() + 50;
	    $count = $this->getlines($this->getfile($file));
	    
	    while ($count <= $state) {
	    
	        $now = time();
	        usleep(10000);
	        
	        if ($now <= $finish) {
	            $count = $this->getlines($this->getfile($file));
	        } else {
	            break;	
	        }  
	         
	    }		 
	    
	    if ($state == $count) {
	    
	        $log['state'] = $state;
	        $log['t'] = "continue";
	        
	    } else {
	    
	        $text= array();
	        $log['state'] = $state + $this->getlines($this->getfile($file)) - $state;
	        
	        foreach ($this->getfile($file) as $line_num => $line) {
	            if ($line_num >= $state) {
	                $text[] =  $line = str_replace("\n", "", $line);
	            }
	    
	            $log['text'] = $text; 
	        }
	    }
	    
	    echo json_encode($log);	

    }
    public function userlistAction(){



    	//Start Array
		$data = array();
// Get data to work with
		$current = cleanInput($_GET['current']);
		$room = cleanInput($_GET['room']);
		$username = cleanInput($_GET['username']);
		$now = time();
// INSERT your data (if is not already there)
		$findUser = ChatUsersRooms::find("username='".$username."' and room='".$room."'");
       	//$findUser = "SELECT * FROM `chat_users_rooms` WHERE `username` = '$username' AND `room` ='$room' ";
		
		if(!hasData($findUser))
				{
					$insertUser = new ChatUsersRooms();
					$insertUser->setUsername($username)
							->setRoom($room)
							->setModTime($now)
							->save();
				}		
		 	//$findUser2 = "SELECT * FROM `chat_users` WHERE `username` = '$username'";
			$findUser2 = ChatUsers::find("username='".$username."'");
			if(!hasData($findUser2))
				{
					$insertUser2 = new ChatUsers();
					$insertUser2->setUsername($username)
							->setStatus(1)
							->setTimeMod($now)
							->save();
					//$insertUser2 = "INSERT INTO `chat_users` (`id` ,`username` , `status` ,`time_mod`)
					//VALUES (NULL , '$username', '1', '$now')";
					//mysql_query($insertUser2);
					$data['check'] = 'true';
				}			
		$finish = time() + 7;

		$getRoomUsers = ChatUsersRooms::find("room='".$room."'");
		//$getRoomUsers = mysql_query("SELECT * FROM `chat_users_rooms` WHERE `room` = '$room'");
		$check = mysql_num_rows($getRoomUsers);
        	
	    while(true)
		{
			usleep(10000);
			//mysql_query("UPDATE `chat_users` SET `time_mod` = '$now' WHERE `username` = '$username'");
			$chatusersupdate = ChatUsers("username='".$username."'");
			$chatusersupdate->setTimeMod($now)
					->update();
			$olduser = time() - 5;
			$eraseuser = time() - 30;


			$chatusersroomsdelete = ChatUsersRooms("mod_time<'".$olduser."'");
			$chatusersroomsdelete->delete();
			$usersdelete = ChatUsers("mod_time<'".$eraseuser."'");
			$usersdelete->delete();

			//mysql_query("DELETE FROM `chat_users_rooms` WHERE `mod_time` <  '$olduser'");
			//mysql_query("DELETE FROM `chat_users` WHERE `time_mod` <  '$eraseuser'");
			$check = count(ChatUsersRooms::find("room='".$room."'"));
			//$check = mysql_num_rows(mysql_query("SELECT * FROM `chat_users_rooms` WHERE `room` = '$room' "));
			$now = time();
			if($now <= $finish)
			{
				$chatusersupdate = ChatUsersRooms("username='".$username."' and room='".$room."' LIMIT 1");
				$chatusersupdate->setModTime($now)
					        ->update();
				//mysql_query("UPDATE `chat_users_rooms` SET `mod_time` = '$now' WHERE `username` = '$username' AND `room` ='$room'  LIMIT 1") ;
				if($check != $current){
				 break;
				}
			}
			else
			{
				 break;	
		    }
        }		 		
// Get People in chat
		if(mysql_num_rows($getRoomUsers) != $current)
		{
			$data['numOfUsers'] = mysql_num_rows($getRoomUsers);
			// Get the user list (Finally!!!)
			$data['userlist'] = array();
			while($user = mysql_fetch_array($getRoomUsers))
			{
				$data['userlist'][] = $user['username'];
			}
			$data['userlist'] = array_reverse($data['userlist']);
		}
		else
		{
			$data['numOfUsers'] = $current;	
			while($user = mysql_fetch_array($getRoomUsers))
			{
				$data['userlist'][] = $user['username'];
			}
			$data['userlist'] = array_reverse($data['userlist']);
		}
		echo json_encode($data);

    }
	public function checkVar($var)
	{
		$var = str_replace("\n", " ", $var);
		$var = str_replace(" ", "", $var);
		if(isset($var) && !empty($var) && $var != '')
		{
			return true;
		}
		else
		{
			return false;	
		}
	}
	private function getfile($f) {
    	if (file_exists($f)) {
            $lines = file($f);
        }	
        
        return $lines; 
	        
	}
	private function getlines($fl){
          return count($fl);	
    }
	private function hasData($query)
	{	$rows = mysql_query($query)or die("somthing is wrong");
		$results = mysql_num_rows($rows);
		if($results == 0)
		{
			return false;  
		}
		else
		{
			return true;  
		}
	}
	private function isAjax()
	{
		if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' )
		{
	     	return true; 
		}	
		else
		{
			return false;
		}
		
	}

	private function cleanInput($data)
	{
		// http://svn.bitflux.ch/repos/public/popoon/trunk/classes/externalinput.php
		// +----------------------------------------------------------------------+
		// | Copyright (c) 2001-2006 Bitflux GmbH                                 |
		// +----------------------------------------------------------------------+
		// | Licensed under the Apache License, Version 2.0 (the "License");      |
		// | you may not use this file except in compliance with the License.     |
		// | You may obtain a copy of the License at                              |
		// | http://www.apache.org/licenses/LICENSE-2.0                           |
		// | Unless required by applicable law or agreed to in writing, software  |
		// | distributed under the License is distributed on an "AS IS" BASIS,    |
		// | WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or      |
		// | implied. See the License for the specific language governing         |
		// | permissions and limitations under the License.                       |
		// +----------------------------------------------------------------------+
		// | Author: Christian Stocker <chregu@bitflux.ch>                        |
		// +----------------------------------------------------------------------+
		//
		// Kohana Modifications:
		// * Changed double quotes to single quotes, changed indenting and spacing
		// * Removed magic_quotes stuff
		// * Increased regex readability:
		//   * Used delimeters that aren't found in the pattern
		//   * Removed all unneeded escapes
		//   * Deleted U modifiers and swapped greediness where needed
		// * Increased regex speed:
		//   * Made capturing parentheses non-capturing where possible
		//   * Removed parentheses where possible
		//   * Split up alternation alternatives
		//   * Made some quantifiers possessive

		// Fix &entity\n;
		$data = str_replace(array('&amp;','&lt;','&gt;'), array('&amp;amp;','&amp;lt;','&amp;gt;'), $data);
		$data = preg_replace('/(&#*\w+)[\x00-\x20]+;/u', '$1;', $data);
		$data = preg_replace('/(&#x*[0-9A-F]+);*/iu', '$1;', $data);
		$data = html_entity_decode($data, ENT_COMPAT, 'UTF-8');

		// Remove any attribute starting with "on" or xmlns
		$data = preg_replace('#(<[^>]+?[\x00-\x20"\'])(?:on|xmlns)[^>]*+>#iu', '$1>', $data);

		// Remove javascript: and vbscript: protocols
		$data = preg_replace('#([a-z]*)[\x00-\x20]*=[\x00-\x20]*([`\'"]*)[\x00-\x20]*j[\x00-\x20]*a[\x00-\x20]*v[\x00-\x20]*a[\x00-\x20]*s[\x00-\x20]*c[\x00-\x20]*r[\x00-\x20]*i[\x00-\x20]*p[\x00-\x20]*t[\x00-\x20]*:#iu', '$1=$2nojavascript...', $data);
		$data = preg_replace('#([a-z]*)[\x00-\x20]*=([\'"]*)[\x00-\x20]*v[\x00-\x20]*b[\x00-\x20]*s[\x00-\x20]*c[\x00-\x20]*r[\x00-\x20]*i[\x00-\x20]*p[\x00-\x20]*t[\x00-\x20]*:#iu', '$1=$2novbscript...', $data);
		$data = preg_replace('#([a-z]*)[\x00-\x20]*=([\'"]*)[\x00-\x20]*-moz-binding[\x00-\x20]*:#u', '$1=$2nomozbinding...', $data);

		// Only works in IE: <span style="width: expression(alert('Ping!'));"></span>
		$data = preg_replace('#(<[^>]+?)style[\x00-\x20]*=[\x00-\x20]*[`\'"]*.*?expression[\x00-\x20]*\([^>]*+>#i', '$1>', $data);
		$data = preg_replace('#(<[^>]+?)style[\x00-\x20]*=[\x00-\x20]*[`\'"]*.*?behaviour[\x00-\x20]*\([^>]*+>#i', '$1>', $data);
		$data = preg_replace('#(<[^>]+?)style[\x00-\x20]*=[\x00-\x20]*[`\'"]*.*?s[\x00-\x20]*c[\x00-\x20]*r[\x00-\x20]*i[\x00-\x20]*p[\x00-\x20]*t[\x00-\x20]*:*[^>]*+>#iu', '$1>', $data);

		// Remove namespaced elements (we do not need them)
		$data = preg_replace('#</*\w+:\w[^>]*+>#i', '', $data);

		do
		{
			// Remove really unwanted tags
			$old_data = $data;
			$data = preg_replace('#</*(?:applet|b(?:ase|gsound|link)|embed|frame(?:set)?|i(?:frame|layer)|l(?:ayer|ink)|meta|object|s(?:cript|tyle)|title|xml)[^>]*+>#i', '', $data);
		}
		while ($old_data !== $data);

		return $data;
	}
    	 /* Other functions  */
    private function scripts(){
        $this->assets->collection('jsPlugins')
            ->setTargetPath("dash/js/general.chat.min.js")
            ->setTargetUri("dash/js/general.chat.min.js")
            ->addJs("dash/js/chat/chat.js")
            ->addJs("dash/js/chat/intermedio.js")
            ->addJs("dash/js/chat/settings.js")
            ->join(true)
            ->addFilter(new \Phalcon\Assets\Filters\Jsmin());

        $this->assets->collection('cssPlugins')
            ->setTargetPath("dash/css/general.chat.min.css")
            ->setTargetUri("dash/css/general.chat.min.css")
            ->addCss("dash/css/main.css")
            ->join(true)
            ->addFilter(new \Phalcon\Assets\Filters\Cssmin());
    }
}