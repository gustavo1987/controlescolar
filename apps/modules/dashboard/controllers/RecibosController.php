<?php
namespace Modules\Dashboard\Controllers;
use Modules\Models\CdRecibo;
use Modules\Models\CdClient;
use Modules\Models\CdPago;
use Modules\Models\CdGrado;
use Modules\Models\VReciboUser;
use Modules\Models\VReciboPago;

class RecibosController extends ControllerBase
{
    public function indexAction()
    {
        $auth = $this->auth();
        if($auth){
            $title = "PAGOS";
            $url ="/";
            $description = "IDE Iberoamerica";
            $image = "dash/img/logo-100.png";
            $this->metaHome($title,$url,$description,$image);
        	$this->scripts();
            $statuspago="INACTIVO";
        	if($auth["rol"]=="ADMIN"){
        		//$recibosall = VReciboUser::find(array("order"=>"date_creation desc"));
                $recibosall = VReciboPago::find("status='ACTIVO' order by vencimiento asc");
                $this->backView("Pagos","/dashboard/controlescolar/pagos");
                $this->view->setVar("recibosall",$recibosall);
        	}
        	elseif($auth["rol"]=="STUDENT"){
                $today = strtotime('now');
        		
                $client = CdClient::findFirst("uid=".$auth["uid"]);
                $inicio = CdGrado::findFirst("status='ACTIVO' and brid=".$client->getCarrera());
                if($client->getCarrera()<=18){
                    $mensualidad = CdPago::find("view='YES' and clid=".$client->getClid()." order by vencimiento asc limit 5");
                }
                else{
                    $mensualidad = CdPago::find("view='YES' and clid=".$client->getClid()." order by vencimiento asc limit 7");
                }
                /*print_r($mensualidad);exit;*/
                //$pagos = CdPago::find("clid=".$client->getClid()." order by vencimiento asc");
                $this->view->setVar("pagos",$mensualidad);
                $this->view->setVar("today",$today);
                if($mensualidad)$statuspago="ACTIVO";
                $this->view->setVar("statuspago",$statuspago);
                $this->backView("Pagos","/dashboard/student/pagos");
        	}
        	
        	$this->view->setVar("statusrecibos",$statusrecibos);
        }
        else{
        	$this->response(array("code"=>404,"message"=>"You do not have permission"),404);
        }
    }
    public function newAction(){

    }
    public function updateAction(){
        $request = $this->request;
        $auth = $this->auth();
        if($request->isPost() && $request->isAjax() && $auth ){
            $id = $request->getPost("id");
            $recibo = CdRecibo::findFirst($id);
            $recibo->setStatus("VALIDADO");
            if($recibo->update()){
                $pago = CdPago::findFirst($recibo->getPagid());
                $pago->setStatus("PAGADO");
                if($pago->update()){
                    $this->response(array("message"=>"SUCCESS","code"=>"200"),200);
                }
                else{
                    $this->response(array("code"=>404,"message"=>"Error to update pago to recibo validate"),200);
                }
            }
            else{
            	$this->response(array("code"=>404,"message"=>"Error to validate recibo"),200);
            }
        }
        else{
        	$this->response(array("code"=>404,"message"=>"You do not have permission"),404);
        }
    }
    private function scripts(){
        $this->assets->collection('jsPlugins')
            ->setTargetPath("dash/js/general.recibo.min.js")
            ->setTargetUri("dash/js/general.recibo.min.js")
            ->addJs("dash/js/plugins/BootstrapDP/bootstrap-datepicker.min.js")
            ->addJs("dash/js/plugins/BootstrapDP/bootstrap-datepicker.es.min.js")
            ->addJs("dash/js/recibo/recibo.js")
            ->join(true)
            ->addFilter(new \Phalcon\Assets\Filters\Jsmin());
    }
}