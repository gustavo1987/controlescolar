<?php
namespace Modules\Dashboard\Controllers;

use Modules\Models\CdClase;
use Modules\Models\CdClaseStudent;
use Modules\Models\CdBrand;
use Modules\Models\CdProduct;
use Modules\Models\CdConferencia;
use Modules\Models\CdUser;
use Modules\Models\VClaseClient;
use Modules\Models\VClaseUser;
use Phalcon\Http\Request;
require dirname(dirname(dirname(dirname(__FILE__))))."/library/PHPMailer/PHPMailerAutoload.php";

class ClasesController extends ControllerBase{
    public function indexAction(){
        $this->dataTable();
        $auth = $this->auth();
        $this->backView("Posgrados","/dashboard/clases/pos");
        if($auth){
            $param = $this->dispatcher->getParam("id");
            $clases = VClaseUser::find("carrera=".$param);
            $this->view->setVar("clases",$clases);
            $this->view->setVar("param",$param);
        }else{
            $this->response(array("code"=>404,"message"=>"You do not have permission"),404);
        }
   	}
    public function newAction(){
        $this->scripts();
        $brid = CdBrand::find();
        $this->view->setVar("brid",$brid);
        $this->backView("Posgrados","/dashboard/clases/pos");
    }

    public function modulonewAction(){
        $this->scripts();
        $auth = $this->auth();
        if($auth){
            $id = $this->dispatcher->getParam("id");
            $clases = CdProduct::find("status='ACTIVE' and brid=".$id);
            $teacher = CdUser::find("rol='TEACHER' and status='ACTIVE'");
            $this->view->setVar("clases",$clases);
            $this->view->setVar("id",$id);
            $this->view->setVar("teacher",$teacher);
        }else{
            $this->response(array("code"=>404,"message"=>"You do not have permission"),404);
        }
    }
    public function uploaddocumentcalendarioAction(){
        $request = new Request();
        if(!($request->isPost() and $request->isAjax()))$this->response(array("message"=>"error"),404);
        if($request->hasFiles()==true){
            foreach($request->getUploadedFiles() as $file){
                $type = explode("/",$file->getType());
                $image_replace = $this->url_clean($file->getName());
                $new_image = uniqid()."_".$image_replace;
                if($file->moveTo(dirname(dirname(dirname(dirname(__DIR__))))."/public/dash/img/calendarios/".$new_image)){
                    $this->response(array("message"=>"correcto","document"=>$new_image),200);
                }else{
                    $this->response(array("message"=>"error try again","code"=>"404"),200);
                }
            }
        }
        $this->response(array("message"=>"correcto"));
    }
    public function uploaddocumentplanAction(){
        $request = new Request();
        if(!($request->isPost() and $request->isAjax()))$this->response(array("message"=>"error"),404);
        if($request->hasFiles()==true){
            foreach($request->getUploadedFiles() as $file){
                $type = explode("/",$file->getType());
                $image_replace = $this->url_clean($file->getName());
                $new_image = uniqid()."_".$image_replace;
                if($file->moveTo(dirname(dirname(dirname(dirname(__DIR__))))."/public/documents/plan/".$new_image)){
                    $this->response(array("message"=>"correcto","document"=>$new_image),200);
                }else{
                    $this->response(array("message"=>"error try again","code"=>"404"),200);
                }
            }
        }
        $this->response(array("message"=>"correcto"));
    }
    public function savemoduloAction(){
        $request = new Request();
        $auth = $this->auth();
        $values = $request->getPost();
        if($request->isPost()&&$request->isAjax()&&$auth){
            $clase = new CdClase();
            $arraydate = explode('/', $values["birth_date"]);
            $datebeging = $arraydate[2]."-".$arraydate[1]."-".$arraydate[0]." 00:00:00";
            $nameclase = CdProduct::findFirst($values["brid"]);
            $clase->setUid($values["uid"])
                ->setPid($values["brid"])
                ->setName($nameclase->getName())
                ->setDateCreation($datebeging)
                ->setStatus($values["status"])
                ->setGeneration(1)
                ->setCarrera($values["carrera"])
                ->setFile($values["plan"])
                ->setChat($values["chat"])
                ->setCalendario($values["calendario"]);
            if($clase->save()){
                $this->response(array("message"=>"SUCCESS","code"=>200),200);
            }
            else{
                $this->response(array("message"=>"Error to save cd_class","code"=>404),200);
            }
        }else{
            $this->response(array("code"=>404,"message"=>"You do not have permission"),404);
        }
    }
    public function viewmoduleAction(){
        $auth = $this->auth();
        $this->scripts();
        $student =  CdUser::findFirst($auth['uid']);
        $client = CdClient::findFirst("email='".$student->getEmail()."'");
        $id = $this->dispatcher->getParam("id");
        $title = "AULA VIRTUAL";
        $url ="/";
        $description = "IDE Iberoamerica";
        $image = "dash/img/logo-100.png";
        $this->metaHome($title,$url,$description,$image);
        $this->backView("Campus Virtual","/dashboard");
        $hoy = date("Y-m-d H:m:s");
        if($auth['rol']=="TEACHER"){
            $claseultima = CdClase::findFirst("status='ACTIVO' and uid=".$auth['uid']);
        }
        elseif($auth['rol']=="STUDENT"){
            $claseultima1 = CdClaseStudent::findFirst("status='ACTIVO' and clid=".$client->getClid());
            $claseultima = CdClase::findFirst($claseultima1->getClasid());
        }
        if($claseultima){
            $conferencia = CdConferencia::findFirst("status='ACTIVO' and clasid=".$claseultima->getClasid());
            $conferencianteriores = CdConferencia::find("status='INACTIVO' and clasid=".$claseultima->getClasid());
            $carrera = CdBrand::findFirst($claseultima->getCarrera());
            $this->view->setVar("claseultima",$claseultima);
            $this->view->setVar("conferencia",$conferencia);
            $this->view->setVar("carrera",$carrera);
        }
        if($conferencianteriores){
            $statusconfe = 1;
            $this->view->setVar("conferencianteriores",$conferencianteriores);
        }
        else $statusconfe = 0;
        $this->view->setVar("client",$client);
        $this->view->setVar("statusconfe",$statusconfe);
    }
    public function editmoduloAction(){
    }
    public function deletemoduloAction(){
    }
    public function listAction(){
        $this->scripts();
        $this->dataTable();
        $auth = $this->auth();
        if($auth){
            $param = $this->dispatcher->getParam("id");
            $carrera = CdClase::findFirst($param);
            $lista = VClaseClient::find("clasid=".$param);
            $this->view->setVar("lista",$lista);
            $this->view->setVar("param",$param);
            $this->backView("Clases","/dashboard/clases/index/".$carrera->getCarrera());
        }else{
            $this->response(array("code"=>404,"message"=>"You do not have permission"),404);
        }
    }
    public function catedrasAction(){
        $this->scripts();
        $this->dataTable();
        $auth = $this->auth();
        if($auth){

            $param = $this->dispatcher->getParam("id");
            $clase = CdClase::findFirst($param);
            $catedras = CdConferencia::find("clasid=".$param);
            $this->view->setVar("catedras",$catedras);
            $this->view->setVar("clase",$clase);
            $this->backView("Clases","/dashboard/clases/index/".$clase->getCarrera());
        }else{
            $this->response(array("code"=>404,"message"=>"You do not have permission"),404);
        }
    }
    public function deletecatedraAction(){
        $request = $this->request;
        $auth = $this->auth();
        if($request->isAjax() && $request->isPost() && $auth ){
            $conferencia = CdConferencia::findFirst($request->getPost("id"));
            if($conferencia->delete()){
                $this->response(array("message"=>"SUCCESS","code"=>200),200);
            }
            else{
                $this->response(array("message"=>"Error to delete cd_conferencia","code"=>404),200);
            }
        }
        else{
            $this->response(array("message"=>"You do not have permission","code"=>404),404);
        }
    }

    public function newcatedraAction(){
        $auth = $this->auth();
        $this->scripts();
        if($auth ){
            $param = $this->dispatcher->getParam("id");
            $teacher = CdUser::find("rol='TEACHER' and status='ACTIVE'");
            $this->view->setVar("clasid",$param);
            $this->view->setVar("catedrid",$teacher);            
            $this->backView("Clases","/dashboard/clases/catedras/".$param);
        }
        else{
            $this->response(array("message"=>"You do not have permission","code"=>404),404);
        }

    }


    public function savecatedraAction(){
        $request = new Request();
        $auth = $this->auth();
        $values = $request->getPost();
        if($request->isPost()&&$request->isAjax()&&$auth){
            $conferencia = new CdConferencia();
            $arraydate = explode('/', $values["datebegin"]);
            $datebeging = $arraydate[2]."-".$arraydate[1]."-".$arraydate[0]." 00:00:00";
            $conferencia->setName($values["name"])
                ->setUrl($values["url"])
                ->setUrlteacher($values["urlteacher"])
                ->setDateBegin($datebeging)
                ->setStatus($values["status"])
                ->setClasid($values["clasid"])
                ->setUid($values["brid"]);
            if($conferencia->save()){
                $this->response(array("message"=>"SUCCESS","code"=>200),200);
            }
            else{
                $this->response(array("message"=>"Error to save cd_conferencia","code"=>404),200);
            }
        }else{
            $this->response(array("code"=>404,"message"=>"You do not have permission"),404);
        }
    }

    public function posAction(){
        $auth = $this->auth();
        $this->backView("Campus Virtual","/dashboard");
        if($auth){
            $posgrados = CdBrand::find("status='ACTIVE'");
            $this->view->setVar("posgrados",$posgrados);
        }else{
            $this->response(array("code"=>404,"message"=>"You do not have permission"),404);
        }

    }
    public function deleteuserclassAction(){
        $request = $this->request;
        $auth = $this->auth();
        if($request->isAjax() && $request->isPost() && $auth ){
            $userclass = CdClaseStudent::findFirst("clid=".$request->getPost("clid")." and clasid=".$request->getPost("clasid"));
            if($userclass->delete()){
                $this->response(array("message"=>"SUCCESS","code"=>200),200);
            }
            else{
                $this->response(array("message"=>"Error to delete cd_class_student","code"=>404),200);
            }
        }
        else{
            $this->response(array("message"=>"You do not have permission","code"=>404),404);
        }
    }
    public function editAction(){
        $this->scripts();
        $id = $this->dispatcher->getParam("id");
        $clase = CdClase::findFirst($id);
        $this->view->setVar("clase",$clase);
        $this->backView("Clase","/dashboard/clases/index/".$clase->getCarrera());
    }
    public function saveAction(){
    }
    public function planAction(){
        $rol = $auth["rol"];
        $title = "Programa de Estudios";
        $url ="/";
        $description = "Campus Virtual";
        $image = "dash/img/logo-100.png";
        $this->metaHome($title,$url,$description,$image);
        $this->backView("Aula virtual","/dashboard/student/clase");
        $id = $this->dispatcher->getParam("id");
        $clase = CdClase::findFirst($id);
        $this->view->setVar("file",$clase->getFile());
    }        
    public function updateAction(){
    }
    	 /* Other functions  */
    private function scripts(){
        $this->assets->collection('jsPlugins')
            ->setTargetPath("dash/js/general.clases.min.js")
            ->setTargetUri("dash/js/general.clases.min.js")
            ->addJs("dash/js/plugins/select/select2.min.js")
            ->addJs("dash/js/plugins/select/es.js")
            ->addJs("dash/js/plugins/moment.min.js")
            ->addJs("dash/js/plugins/BootstrapDP/bootstrap-datepicker.min.js")
            ->addJs("dash/js/plugins/BootstrapDP/bootstrap-datepicker.es.min.js")
            ->addJs("dash/js/clases/clases.js")
            ->join(true)
            ->addFilter(new \Phalcon\Assets\Filters\Jsmin());
    }
}