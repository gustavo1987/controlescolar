<?php
namespace Modules\Dashboard\Controllers;
use Modules\Models\CdLogUserSubservices;
use Modules\Models\CdSocialMedia;
use Modules\Models\CdSubservices;
use Modules\Models\CdUser;
use Modules\Models\CdBrand;
use Modules\Models\CdClient;
use Modules\Models\CdClase;
use Modules\Models\CdGrado;
use Modules\Models\CdCostos;
use Modules\Models\CdPago;
use Modules\Models\CdClaseStudent;
use Modules\Models\CdUserSubservices;
use Modules\Models\Vuserpost;
use Modules\Models\Vusers;
use Modules\Models\VUserSubservices;
use Phalcon\Http\Request;

include dirname(dirname(dirname(dirname(__FILE__))))."/library/wideimage/WideImage.php";
//require dirname(dirname(dirname(dirname(__FILE__))))."/library/PHPMailer/PHPMailerAutoload.php";

class UserController extends ControllerBase
{
    public function indexAction()
    {   $auth = $this->auth();
        if($auth){
            $uid = $auth["uid"];
            $users = CdUser::find("uid!=$uid and status='ACTIVE'");
            if(count($users)>=1) $this->assets->collection('jsMasonry')->addJs("dash/js/masonry.pkgd.min.js");
            $this->view->setVar("users",$users);
        }
    }
    public function inactiveAction()
    {   $auth = $this->auth();
        if($auth){
            $uid = $auth["uid"];
            $users = CdUser::find("uid!=$uid and status='INACTIVE'");
            if(count($users)>=1) $this->assets->collection('jsMasonry')->addJs("dash/js/masonry.pkgd.min.js");
            $this->view->setVar("users",$users);
        }
    }
    public function newUserAction(){
        $auth = $this->auth();
        if($auth){
            $postgrados = CdBrand::find("status='ACTIVE'");
            $this->validationJs();
            $this->scriptsUsers();
            $this->view->setVar("auth",$auth);
            $this->view->setVar("postgrados",$postgrados);
            $this->backView("Estudiantes","/dashboard/estudiantes");
        }else{
            $this->response(array("code"=>404,"message"=>"You do not have permission"),404);
        }
    }
    public function editAction(){
        $auth = $this->auth();
        if($auth){
            $uid = $this->dispatcher->getParam("id","int");
            $find = CdUser::findFirst($uid);
            if(!$find)return $this->response->redirect("dashboard/users");
            $this->validationJs();
            $this->scriptsUsers();
            $this->view->setVar("user",$find);
            $this->view->setVar("auth",$auth);
        }else{
            $this->response(array("code"=>404,"message"=>"You do not have permission"),404);
        }
    }
    public function profileAction(){
        $auth = $this->auth();
        $rol = $auth["rol"];
        if($auth){
            $this->validationJs();
            $this->scriptsUsers();
            $user = new CdUser();
            $find = $user->findFirst("uid={$auth['uid']}");
            $this->view->setVar("user",$find);
            $this->view->setVar("auth",$auth);
            $this->view->setVar("rol",$rol);
        }else{
            return $this->response->redirect();
        }
    }
    public function saveUserAction(){
        $request = $this->request;
        $auth = $this->auth();
        if($request->isAjax() && $request->isPost() && $auth ){
            $user = new CdUser();
            $find = $user;
            $usnm = str_replace(" ","-",$request->getPost("username"));
            $find->setName($request->getPost("name"))
                ->setLastName($request->getPost("last_name"))
                ->setSecondName($request->getPost("second_name"))
                ->setSex($request->getPost("sex"))
                ->setPhone($request->getPost("phone"))
                ->setUsername($usnm)
                ->setEmail($request->getPost("email"))
                ->setPhoto("no-image.jpg")
                ->setPassword($this->security->hash($request->getPost("password")))
                ->setRol($request->getPost('rol'))
                ->setStatus($request->getPost('status'))
                ->setDateCreation(date("Y-m-d H:i:s"));

            if($find->save()){
                  $client = new CdClient();
                  $identifi = $request->getPost("name")[0]."".$request->getPost("last_name")[0].date("Y").date("m").date("d").date("H").date("i").date("s");
                $client->setIdentifier($identifi)
                    ->setName($request->getPost("name"))
                    ->setLastName($request->getPost("last_name"))
                    ->setSecondName($request->getPost("second_name"))
                    ->setSex($request->getPost("sex"))
                    ->setEmail($request->getPost("email"))
                    ->setTel($request->getPost("phone"))
                    ->setStatus($request->getPost('rol'))
                    ->setDateCreation(date("Y-m-d H:i:s"))
                    ->setUid($find->getUid())
                    ->setCarrera($request->getPost('posgrado'))
                    ->setGeneration("1")
                    ->save();
                $clase = CdClase::findFirst("carrera=".$request->getPost('posgrado')." and status='ACTIVO'");

                if($clase){
                    $new = new CdClaseStudent();
                    $new->setClasid($clase->getClasid())
                        ->setClid($client->getClid())
                        ->setStatus("ACTIVO")
                        ->setBeginSesion(date("Y-m-d H:i:s"));
                    if(!$new->save())$this->response(array("code"=>404,"message"=>"Error to save cd_clase_student"),200);
                }else{
                    $this->response(array("message"=>"Error no class","code"=>404),200);
                }
                $this->generateGrado($find->getUid(),$request->getPost('posgrado'),1);
                $this->generatePay($request->getPost('posgrado'),$client->getClid());
                $this->getUsersJson(2);
                $this->response(array("message"=>"SUCCESS","code"=>200),200);
            }else{
                /*foreach ($find->getMessages() as $message) {
                    $this->flash->error((string) $message);
                }*/
                $this->response(array("message"=>"Error to save cd_user","code"=>404),200);
            }
        }else{
            $this->response(array("message"=>"You do not have permission","code"=>404),404);
        }
    }
    public function generateGrado($uid,$brid,$number){
        $grado = new CdGrado();
        $grado->setBrid($brid)
            ->setStatus("ACTIVO")
            ->setDate(date("Y-m-d H:i:s"))
            ->setUid($uid)
            ->setNumber($number);
        if(!$grado->save())$this->response(array("code"=>404,"message"=>"Error to save cd_grado"),200);
    }
    public function generatePay($brid,$clid){
        $costoReinscripcion = CdCostos::findFirst("name='REINSCRIPCION' and brid=".$brid);
        $costoMensualidad = CdCostos::findFirst("name='MENSUALIDAD' and brid=".$brid);  
        $pago = new CdPago();
        $pago1 = new CdPago();
        $pago2 = new CdPago();
        $pago3 = new CdPago();
        $pago4 = new CdPago();
        $today = date("j");
        /**** Si se inscribe antes del 20 se genera el pago el mismo mes ***/
        if($today<=20){
            $mes = date("m");
            if($mes==12){
                $mes=12;
                $mes1=1;
                $año=date("Y");
                $año1=$año+1;
            }
            else{
                $año=date("Y");
                $año1=$año;
                $mes1=$mes+1;
            }
            if($mes1==12){
                $mes2=1;
                $año2=$año1+1;
            }
            else{
                $año2=$año1;
                $mes2=$mes1+1;
            }
            if($mes2==12){
                $mes3=1;  
                $año3=$año2+1;
            } 
            else{
                $año3=$año2;
                $mes3=$mes2+1;
            }
            if($mes3==12){
                $mes4=1;  
                $año4=$año3+1;
            }
            else{
                $año4=$año3;
                $mes4=$mes3+1;
            }
            if($mes4==12){
                $mes5=1;  
                $año5=$año4+1;
            }
            else{
                $año5=$año4;
                $mes5=$mes4+1;
            }
            if($mes5==12){
                $mes6=1;  
                $año6=$año5+1;
            }
            else{
                $año6=$año5;
                $mes6=$mes5+1;
            }
            $vecimiento1="".$año."-".$mes."-05 00:00:00"; 
            $vecimiento2="".$año1."-".$mes1."-05 00:00:00"; 
            $vecimiento3="".$año2."-".$mes2."-05 00:00:00"; 
            $vecimiento4="".$año3."-".$mes3."-05 00:00:00"; 
            if($brid==(19||20)){ 
            $vecimiento5="".$año4."-".$mes4."-05 00:00:00";
            $vecimiento6="".$año5."-".$mes5."-05 00:00:00"; 
            }
        }
        /**** Si se inscribe despues del 20 se genera el pago es el siguiente mes ***/
        else{
            $mes = date("m");
            if($mes==12){
                $mes=12;
                $mes1=1;
                $año=date("Y");
                $año1=$año+1;
            }
            else{
                $año=date("Y");
                $año1=$año;
                $mes1=$mes+1;
            }
            if($mes1==12){
                $mes2=1;
                $año2=$año1+1;
            }
            else{
                $año2=$año1;
                $mes2=$mes1+1;
            }
            if($mes2==12){
                $mes3=1;  
                $año3=$año2+1;
            } 
            else{
                $año3=$año2;
                $mes3=$mes2+1;
            }
            if($mes3==12){
                $mes4=1;  
                $año4=$año3+1;
            }
            else{
                $año4=$año3;
                $mes4=$mes3+1;
            }
            if($mes4==12){
                $mes5=1;  
                $año5=$año4+1;
            }
            else{
                $año5=$año4;
                $mes5=$mes4+1;
            }
            if($mes5==12){
                $mes6=1;  
                $año6=$año5+1;
            }
            else{
                $año6=$año5;
                $mes6=$mes5+1;
            }
            $vecimiento1="".$año1."-".$mes1."-05 00:00:00"; 
            $vecimiento2="".$año2."-".$mes2."-05 00:00:00"; 
            $vecimiento3="".$año3."-".$mes3."-05 00:00:00"; 
            $vecimiento4="".$año4."-".$mes4."-05 00:00:00";  
            if($brid==(19||20)){
            $vecimiento5="".$año5."-".$mes5."-05 00:00:00";
            $vecimiento6="".$año6."-".$mes6."-05 00:00:00"; 
            }
        }
        $pago->setName("REINSCRIPCION")
            ->setCantidad($costoReinscripcion->getCount())
            ->setDateCreation(date("Y-m-d H:i:s"))
            ->setStatus("PAGADO")
            ->setClid($clid)
            ->setVencimiento(date($vecimiento1))
            ->setRelationship("NO")
            ->setType("REINSCRIPCION")
            ->setView("YES")
            ->save();
        $pago1->setName("MENSUALIDAD")
            ->setCantidad($costoMensualidad->getCount())
            ->setDateCreation(date("Y-m-d H:i:s"))
            ->setStatus("PAGADO")
            ->setClid($clid)
            ->setVencimiento(date($vecimiento1))
            ->setRelationship("NO")
            ->setType("MENSUALIDAD")
            ->setView("YES")
            ->save();
        $pago2->setName("MENSUALIDAD")
            ->setCantidad($costoMensualidad->getCount())
            ->setDateCreation(date("Y-m-d H:i:s"))
            ->setStatus("ADEUDA")
            ->setClid($clid)
            ->setVencimiento(date($vecimiento2))
            ->setRelationship("NO")
            ->setType("MENSUALIDAD")
            ->setView("YES")
            ->save();
        $pago3->setName("MENSUALIDAD")
            ->setCantidad($costoMensualidad->getCount())
            ->setDateCreation(date("Y-m-d H:i:s"))
            ->setStatus("ADEUDA")
            ->setClid($clid)
            ->setVencimiento(date($vecimiento3))
            ->setRelationship("NO")
            ->setType("MENSUALIDAD")
            ->setView("YES")
            ->save();
        $pago4->setName("MENSUALIDAD")
            ->setCantidad($costoMensualidad->getCount())
            ->setDateCreation(date("Y-m-d H:i:s"))
            ->setStatus("ADEUDA")
            ->setClid($clid)
            ->setVencimiento(date($vecimiento4))
            ->setRelationship("NO")
            ->setType("MENSUALIDAD")
            ->setView("YES")
            ->save();
        if($brid==(19||20)){
        $pago5 = new CdPago();
        $pago6 = new CdPago();

        $pago5->setName("MENSUALIDAD")
            ->setCantidad($costoMensualidad->getCount())
            ->setDateCreation(date("Y-m-d H:i:s"))
            ->setStatus("ADEUDA")
            ->setClid($clid)
            ->setVencimiento(date($vecimiento5))
            ->setRelationship("NO")
            ->setType("MENSUALIDAD")
            ->setView("YES")
            ->save();
        $pago6->setName("MENSUALIDAD")
            ->setCantidad($costoMensualidad->getCount())
            ->setDateCreation(date("Y-m-d H:i:s"))
            ->setStatus("ADEUDA")
            ->setClid($clid)
            ->setVencimiento(date($vecimiento6))
            ->setRelationship("NO")
            ->setType("MENSUALIDAD")
            ->setView("YES")
            ->save();
        }
    }
    public function updateUserAction(){
        $request = new Request();
        $auth = $this->auth();
        if($request->isAjax() && $request->isPost() && $auth ){
            $uid = $request->getPost("uid");
            $user = new CdUser();
            $find = $user->findFirst($uid);
            $usnm = str_replace(" ","-",$request->getPost("username"));
            $status = $request->getPost("status");
            $find->setName($request->getPost("name"))
                ->setLastName($request->getPost("last_name"))
                ->setSecondName($request->getPost("second_name"))
                ->setSex($request->getPost("sex"))
                ->setPhone($request->getPost("phone"))
                ->setUsername($usnm)
                ->setEmail($request->getPost("email"))
                ->setRol($request->getPost('rol')==null?$auth['rol']:$request->getPost('rol'))
                ->setStatus(empty($status)?'ACTIVE':$request->getPost("status"));
            if($find->update()){
                $this->getUsersJson(2);
                $this->response(array("message"=>"SUCCESS","code"=>200,"redirect"=>$request->getPost('redirect')),200);
            }else{
                
                 foreach ($find->getMessages() as $message) {
                    $this->flash->error((string) $message);
                }
                $this->response(array("message"=>"Error update cd_user","code"=>404),200);
            }
        }else{
            $this->response(array("message"=>"You do not have permission","code"=>404),404);
        }
    }
    public function percentageAction(){
        $auth = $this->auth();
        if($auth){
            $param = $this->dispatcher->getParam("id");
            $find_user = CdUser::findFirst($param);
            if($find_user){
                $this->scriptsUsers();
                $subServices = VUserSubservices::find("uid=$param and status_subservice!='CANCEL' order by name_service");
                $user = CdUser::findFirst($param);
                $this->view->setVar("user",$user);
                $this->view->setVar("sub_services",$subServices);
            }else{
                $this->response->redirect("dashboard/users");
            }
        }else{
            $this->response(array("message"=>"You do not have permission","code"=>404),404);
        }
    }
    public function savePercentageAction(){
        $auth = $this->auth();
        $request = new Request();
        if($auth && $request->getPost() && $request->isAjax()){
            $value = $request->getPost("value");
            $id = $request->getPost("id");
            $uid_update = $request->getPost("uid");
            $percentage = CdUserSubservices::findFirst($id);
            $percentage->setPercentage($value);
            if(!$percentage->update())$this->response(array("code"=>404,"message"=>"Error to update percentage"),200);
            $log_percentage = new CdLogUserSubservices();
            $log_percentage->setPercentage($value)->setUid($auth['uid'])->setUidUpdate($uid_update)->setUsid($percentage->getUsid())->setDateCreation(date("Y-m-d H:i:s"))->save();
            $this->response(array("code"=>200,"val"=>$percentage->getPercentage()),200);
        }else{
            $this->response(array("message"=>"You do not have permission","code"=>404),404);
        }
    }
    public function updatePasswordAction(){
        $request = $this->request;
        $auth = $this->auth();
        if($request->isAjax() && $request->isPost() && $auth ){
            $uid = $request->getPost("uid");
            $find = CdUser::findFirst($uid);
            $find->setPassword($this->security->hash($request->getPost("password")));
            if($find->update()){
                $this->response(array("message"=>"SUCCESS","code"=>200),200);
            }else{
                $this->response(array("message"=>"error update password","code"=>404),200);
            }
        }else{
            $this->response(array("message"=>"You do not have permission","code"=>404),404);
        }
    }
    public function updateUserImageAction(){
        $request = $this->request;
        $auth = $this->auth();
        if($request->isAjax() && $request->isPost() && $auth){
            $uid = $request->getPost("uid");
            $user = new CdUser();
            $find = $user->findFirst($uid);
            $image_actual = $find->getPhoto();
            if($image_actual==$request->getPost("photo")){
                $this->response(array("message"=>"warning","code"=>303),200);
            }else{
                $find->setPhoto($request->getPost("photo"));
                if($find->update()){
                    $_SESSION['auth']['photo'] = $request->getPost("photo");
                    $this->response(array("message"=>"SUCCESS","code"=>200),200);
                }else{
                    $this->response(array("message"=>"error","code"=>404),200);
                }
            }
        }else{
            return $this->response->redirect();
        }
    }
    public function uploadImageAction(){
        $request = $this->request;
        $auth = $this->auth();
        if($request->isPost() && $request->isAjax() && $auth){
            if($request->hasFiles()==true){
                foreach($request->getUploadedFiles() as $file){
                    $image_replace = $this->url_clean($file->getName());
                    $new_image = uniqid()."_".$image_replace;
                    if($file->moveTo(dirname(dirname(dirname(dirname(__DIR__))))."/public/dash/assets/images/users/".$new_image)){
                        $image_transform = \WideImage::load(dirname(dirname(dirname(dirname(__DIR__))))."/public/dash/assets/images/users/".$new_image);
                        $newImageThumbnail = $image_transform->resize(null,200);
                        $newImageThumbnail->saveToFile(dirname(dirname(dirname(dirname(__DIR__))))."/public/dash/assets/images/users/thumbnail/".$new_image);
                        $this->response(array("name"=>$new_image,"message"=>"SUCCESS","code"=>"200"),200);
                    }
                    else{
                        $this->response(array("name"=>$new_image,"message"=>"error try again","code"=>"404"),200);
                    }
                }
            }
        }else{
            exit();
        }
    }
    public function validateEmailAction(){
        $request = $this->request;
        if($request->isPost() && $request->isAjax()){
            $email = $this->request->getPost("email");
            $validation = $this->request->getPost("validation");
            if($email==$validation)$this->response(array('valid' => true),200);
            $user = new CdUser();
            $find  = $user->findFirst("email='$email'");
            if(!$find){
                $this->response(array('valid' => true),200);
            }
            else{
                $this->response(array('valid' => false),200);
            }
        }
    }
    public function validateUsernameAction(){
        $request = $this->request;
        if($request->isPost() && $request->isAjax()){
            $username = str_replace(" ","-",$request->getPost("username"));
            $validation = str_replace(" ","-",$request->getPost("validation"));
            if($username==$validation)$this->response(array('valid' => true),200);
            $user = new CdUser();
            $find  = $user->findFirst("username='$username'");
            if(!$find){
                $this->response(array('valid' => true),200);
            }
            else{
                $this->response(array('valid' => false),200);
            }
        }
    }
    public function _registerSession($user){
        $this->session->set("auth",array(
                "uid" => $user->getUid(),
                "username"=>$user->getUsername(),
                "rol"=>$user->getRol(),
                "name"=>$user->getName(),
                "user_photo"=>$user->getUserPhoto(),
                "register"=>$user->getRegister(),
                "email"=>$user->getEmail()
            )
        );
    }
    private function scriptsUsers(){
        $this->assets->collection('jsPlugins')
            ->setTargetPath("dash/js/general.users.min.js")
            ->setTargetUri("dash/js/general.users.min.js")
            ->addJs("dash/js/plugins/dropzone/dropzone.min.js")
            ->addJs("dash/js/users/users.js")
            ->join(true)
            ->addFilter(new \Phalcon\Assets\Filters\Jsmin());
    }

}
