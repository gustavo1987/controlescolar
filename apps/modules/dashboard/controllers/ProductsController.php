<?php
namespace Modules\Dashboard\Controllers;
require dirname(dirname(dirname(dirname(__FILE__))))."/library/Classes/PHPExcel.php";
use Modules\Models\CdLogStock;
use Modules\Models\CdProduct;
use Modules\Models\CdStock;
use Modules\Models\VProductBrand;
use Modules\Models\VStockProduct;
use Phalcon\Http\Request;

class ProductsController extends ControllerBase{
    public function indexAction(){
        $auth = $this->auth();
        if($auth){
            $this->dataTable();
            $this->scripts();
            $find = VProductBrand::find("status='ACTIVE' order by pid desc");
            $this->view->setVar("products",$find);
            $this->view->setVar("auth",$auth);
        }else{
            $this->response(array("code"=>404,"message"=>"You do not have permission"),404);
        }
    }
    public function printpAction(){
        $auth = $this->auth();
        $requets = new Request();
        if($auth){
            $status = $requets->get("status");
            $this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_ACTION_VIEW);
            $general_data = VStockProduct::find("status_product='$status'");
            $this->assets->collection('cssPlugins')->setTargetPath("dash/css/print_bill.min.css")->setTargetUri("dash/css/print_bill.min.css")->addCss("dash/css/print_bill.css")->join(true)->addFilter(new \Phalcon\Assets\Filters\Cssmin());
            $this->view->setVar("data",$general_data->toArray());
        }else{
            $this->response(array("code"=>404,"message"=>"You do not have permission"),404);
        }
    }
    public function newAction(){
        $auth = $this->auth();
        if($auth){
            $this->validationJs();
            $this->scripts();
            $this->view->setVar("brid",json_decode($this->getBrandsJson(1),true));
        }else{
            $this->response(array("code"=>404,"message"=>"You do not have permission"),404);
        }
    }
    public function editAction(){
        $auth = $this->auth();
        if($auth){
            $param = $this->dispatcher->getParam("id");
            $find = CdProduct::findFirst($param);
            if($find){
                $this->validationJs();
                $this->scripts();
                $this->view->setVar("product",$find);
                $this->view->setVar("brid",json_decode($this->getBrandsJson(1),true));
            }else{
                $this->response->redirect("dashboard/productos");
            }
        }else{
            $this->response(array("code"=>404,"message"=>"You do not have permission"),404);
        }
    }
    public function saveAction(){
        $auth = $this->auth();
        $request = new Request();
        if($auth && $request->isPost() && $request->isAjax()){
            $values = $request->getPost();
            /*if($values['pid']){
                $find = CdProduct::findFirst($values['pid']);
                $find->setName($values['name'])
                    ->setPermalink($values['permalink'])
                    ->setPurchasePrice($values['purchase_price'])
                    ->setSalePrice($values['sale_price'])
                    ->setMeasure("PZ")
                    ->setQuantity($values['quantity'])
                    ->setPercentage($values['percentage'])
                    ->setDateCreation(date('Y-m-d H:i:s'))
                    ->setStatus($values['status'])
                    ->setBrid($values['brid'])
                    ->setTicket($values['ticket'])
                    ->setDescription($values['description']);
                if($find->update())$this->response(array("code"=>"200","message"=>"SUCCESS"),200);
            }else{*/
                $new = new CdProduct();
                $new->setName($values['name'])
                    ->setPermalink($values['permalink'])
                    ->setPurchasePrice($values['purchase_price'])
                    ->setSalePrice($values['sale_price'])
                    ->setMeasure("PZ")
                    ->setQuantity($values['quantity'])
                    ->setPercentage($values['percentage'])
                    ->setDateCreation(date('Y-m-d H:i:s'))
                    ->setStatus($values['status'])
                    ->setBrid($values['brid'])
                    ->setTicket($values['ticket'])
                    ->setDescription($values['description']);
                if($new->save()){
                    $new_stock = new CdStock();
                    $new_stock->setStock("9999")->setPid($new->getPid())->setStockMax("9999")->setStockMin($values['stockmi'])->setQuantity(0)->setDateCreation(date("Y-m-d H:i:s"))
                        ->save();
                    $this->response(array("code"=>"200","message"=>"SUCCESS"),200);
                }
            /*}*/
            $this->response(array("code"=>"404","message"=>"Error to save cd_products"),200);
        }else{
            $this->response(array("code"=>404,"message"=>"You do not have permission"),404);
        }
    }
    public function inactiveAction(){
        $auth = $this->auth();
        if($auth){
            $this->dataTable();
            $this->scripts();
            $find = VProductBrand::find("status='INACTIVE' order by pid desc");
            $this->view->setVar("products",$find);
             $this->view->setVar("auth",$auth);
        }else{
            $this->response(array("code"=>404,"message"=>"You do not have permission"),404);
        }
    }
    public function deleteAction(){
        $auth = $this->auth();
        $request = new Request();
        if($auth && $request->isPost() && $request->isAjax()){
            $id = $request->getPost("id");
            $find = CdProduct::findFirst($id);
            if($find->getStatus()=='ACTIVE')$find->setStatus("INACTIVE");
            else $find->setStatus("ACTIVE");

            if($find->update())$this->response(array("code"=>"200","message"=>"SUCCESS"),200);
            $this->response(array("code"=>"404","message"=>"Error to update cd_products"),200);
        }else{
            $this->response(array("code"=>404,"message"=>"You do not have permission"),404);
        }
    }
    public function getAction(){
        $request = $this->request;
        $auth = $this->auth();
        if($request->isPost() && $request->isAjax() && $auth ){
            $id = $request->getPost("id");
            $find = VProductBrand::findFirst("pid=$id");
            if($find)$this->response(array("code"=>"200","message"=>"SUCCESS","result"=>$find->toArray()),200);
            $this->response(array("code"=>"404","message"=>"Error find v_product_brand"),200);
        }else{
            $this->response(array("code"=>404,"message"=>"You do not have permission"),404);
        }
    }
    public function stockAction(){
        $request = $this->request;
        $auth = $this->auth();
        if($request->isPost() && $request->isAjax() && $auth ){
            $values = $request->getPost();
            $find = CdStock::findFirst($values['id']);
            $stock = null;
            if($find){
                $newLogStock =  new CdLogStock();
                $find_product = VProductBrand::findFirst("pid={$find->getPid()}");
                switch($values['type']){
                    case "stock_max" : $find->setStockMax($values['value']);
                        break;
                    case "stock_min" : $find->setStockMin($values['value']);
                        break;
                    case "add" : $newLogStock->setAdd($values['value']);
                        break;
                    case "delete" :$newLogStock->setDelete($values['value']);
                        break;
                }
                if($find->update()){
                    switch($values['type']){
                        case "add":$this->setStockProcedure($values['pid'],$values['value'],"add");
                            break;
                        case "delete":$this->setStockProcedure($values['pid'],$values['value'],"delete");
                            break;
                    }
                    $find_vpb = VProductBrand::findFirst("pid={$find->getPid()}");
                    @$newLogStock->setStock($find->getStock())->setStockNew($find_vpb->getStock())->setQuantity($find_product->getQuantityProduct())->setQuantityNew($find_vpb->getQuantityProduct())->setStid($values['id'])->setDateCreation(date("Y-m-d H:i:s"))->save();
                    $this->response(array("code"=>200,"message"=>"SUCCESS","result"=>$find_vpb->toArray()),200);
                }
            }
            else{
                $this->response(array("code"=>404,"message"=>"Error find cd_stock"),200);
            }
            $this->response(array("code"=>200,"message"=>"SUCCESS","result"=>$values),200);
        }else{
            $this->response(array("code"=>404,"message"=>"You do not have permission"),404);
        }
    }
    public function permalinkAction(){
        $request = $this->request;
        $auth = $this->auth();
        if($request->isPost() && $request->isAjax() && $auth ){
            $post = new CdProduct();
            $name = $request->getPost("name");
            $new_url = $this->url_clean($name);
            $check_url = $post->find("permalink = '$new_url'");
            $count = 1;
            while(count($check_url)){
                $generate_url = $new_url."-".$count;
                $check_url = $post->find("permalink = '$generate_url'");
                if(count($check_url)==0){
                    $new_url = $generate_url;
                }
                $count++;
            }
            $this->response(array("message"=>"SUCCESS","permalink"=>$new_url,"code"=>"200","data"=>"url generated"),200);
        }else{
            $this->response(array("code"=>404,"message"=>"You do not have permission"),404);
        }
    }

/* Export */
    public function exportAction(){
        $auth = $this->auth();
        $status = $this->request->get("status");
        if($auth && $status){
            $objPHPExcel = new \PHPExcel();
            $objPHPExcel->getProperties()->setCreator($auth['name'])
                ->setLastModifiedBy($auth['email'])
                ->setTitle("Reporte de venta PONS")
                ->setSubject("Información de stock")
                ->setDescription("Información de stock")
                ->setKeywords("XLS Microsoft")
                ->setCategory("Boutique");
            $this->xlsGeneral($status,$objPHPExcel);
        }else{
            exit();
        }
    }
    private function xlsGeneral($type_status,$objectXls){
        $cont = 0;
        $objectXls->setActiveSheetIndex(0)
            ->setCellValue('A2', 'No.')
            ->setCellValue('B2', 'Ticket')
            ->setCellValue('C2', 'Nombre')
            ->setCellValue('D2', 'Categoria')
            ->setCellValue('E2', 'Precio')
            ->setCellValue('F2', 'Stock Minimo')
            ->setCellValue('G2', 'Stock Máximo')
            ->setCellValue('H2', 'Stock Actual');

        $v_product_brand = VProductBrand::find("status='$type_status'");
        if(count($v_product_brand)>=1){
            foreach($v_product_brand as $key => $value){
                $cont =$key+3;
                $objectXls->setActiveSheetIndex(0)
                    ->setCellValue("A".$cont, $value->getPid())
                    ->setCellValue("B".$cont, $value->getTicket())
                    ->setCellValue("C".$cont, $value->getName())
                    ->setCellValue("D".$cont, $value->getNameBrand())
                    ->setCellValue("E".$cont, $value->getSalePrice())
                    ->setCellValue("F".$cont, $value->getStockMin())
                    ->setCellValue("G".$cont, $value->getStockMax())
                    ->setCellValue("H".$cont, $value->getStock());
            }
        }else{
            $cont = 3;
            $objectXls->setActiveSheetIndex(0)
                ->setCellValue("A3".$cont, "Sin resultados")
                ->setCellValue("B3".$cont, "")
                ->setCellValue("C3".$cont, "")
                ->setCellValue("D3".$cont, "")
                ->setCellValue("E3".$cont, "")
                ->setCellValue("F3"."")
                ->setCellValue("G3"."")
                ->setCellValue("H3"."");
        }

        $this->autoWidth($objectXls,$cont);
        $objectXls->getActiveSheet()->setTitle("Pons Boutique");
        $objectXls->setActiveSheetIndex(0);
        $objWriter = \PHPExcel_IOFactory::createWriter($objectXls,'Excel2007');
        $xls = date('m-d-Y_H.i.s')."_$type_status.xlsx";
        $objWriter->save(dirname(dirname(dirname(dirname(__DIR__))))."/public/reports/".$xls);
        $this->download($xls);
    }
    private function autoWidth($objectXls,$cont){
        $this->style($objectXls,"A2:H2");
        $this->logo($objectXls,"A1:H1");
        $this->styleContent($objectXls,"A3:H$cont");

        $objectXls->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $objectXls->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $objectXls->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $objectXls->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
        $objectXls->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
        $objectXls->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
        $objectXls->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
        $objectXls->getActiveSheet()->getColumnDimension('h')->setAutoSize(true);

    }
/* End Export */

    private function scripts(){
        $this->assets->collection('jsPlugins')
            ->setTargetPath("dash/js/general.products.min.js")
            ->setTargetUri("dash/js/general.products.min.js")
            ->addJs("dash/js/products/products.js")
            ->join(true)
            ->addFilter(new \Phalcon\Assets\Filters\Jsmin());
    }
}