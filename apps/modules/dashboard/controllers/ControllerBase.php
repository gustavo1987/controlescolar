<?php
namespace Modules\Dashboard\Controllers;

use Modules\Models\CdBoxCut;
use Modules\Models\CdBrand;
use Modules\Models\CdCategory;
use Modules\Models\CdEstados;
use Modules\Models\CdClient;
use Modules\Models\CdClase;
use Modules\Models\CdClaseStudent;
use Modules\Models\CdMunicipios;
use Modules\Models\CdStock;
use Modules\Models\CdProduct;
use Modules\Models\CdGeneracion;
use Modules\Models\CdSubcategory;
use Modules\Models\CdUser;
use Modules\Models\Vphotonote;
use Modules\Models\Vsectionnotes;
use Modules\Models\VShoppingList;
use Phalcon\Mvc\Controller;

class ControllerBase extends Controller
{
    public function initialize(){    
        $assetsVersion = "-v1.1.3";
        $this->assets->collection('JsIndex')
            ->setTargetPath("dash/js/custom".$assetsVersion.".min.js")
            ->setTargetUri("dash/js/custom".$assetsVersion.".min.js")
            ->addJs("dash/js/plugins/jquery/jquery.min.js")
            ->addJs("dash/js/plugins/jquery/jquery-ui.min.js")
            ->addJs("dash/js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js")
            ->addJs("dash/js/plugins/bootstrap/bootstrap.min.js")
            ->addJs("dash/js/plugins/scrolltotop/scrolltopcontrol.js")
            ->addJs("dash/js/plugins/moment.min.js")
            ->addJs("dash/js/plugins/select/select2.min.js")
            ->addJs("dash/js/plugins/select/es.js")
            ->addJs("dash/js/ckeditor.js")
            ->addJs("dash/ckeditor/adapters/jquery.js")
            ->addJs("dash/js/plugins/bootstrapV/formValidation.min.js")
            ->addJs("dash/js/plugins/bootstrapV/bootstrapV.min.js")
            ->addJs("dash/js/plugins/dropzone/dropzone.min.js")
            ->addJs("dash/js/push1.min.js")
            ->addJs("dash/js/plugins.js")
            ->addJs("dash/js/actions.js")
            ->addJs("dash/js/slick.js")
            ->addJs("dash/js/2019.js")
            ->join(true)
            ->addFilter(new \Phalcon\Assets\Filters\Jsmin());

        $this->assets->collection('CssIndex')
            ->setTargetPath("dash/css/custom".$assetsVersion.".min.css")
            ->setTargetUri("dash/css/custom".$assetsVersion.".min.css")
            ->addCss("default/css/bootstrap.min.css")
            //->addCss("dash/css/fontawesome/font-awesome.min.css")
            // ->addCss("dash/css/fontawesome/fontawesome.min.css")
            ->addCss("dash/css/jquery/jquery-ui.min.css")
            ->addCss("dash/css/nvd3/nv.d3.css")
            ->addCss("dash/css/mcustomscrollbar/jquery.mCustomScrollbar.css")
            ->addCss("dash/css/rickshaw/rickshaw.css")
            ->addCss("dash/css/dropzone/dropzone.css")
            ->addCss("dash/css/animate/animate.min.css")
            ->addCss("dash/css/bootstrapV/formValidation.min.css")
            //->addCss("dash/css/sweetalert.css")
            ->addCss("dash/css/select/select2.min.css")
            ->addCss("dash/css/select/select2-bootstrap.css")
            ->addCss("dash/css/custom.css")
            ->addCss("dash/css/theme-serenity.css")
            ->addCss("dash/css/slick.css")
            ->addCss("dash/css/2019.css")
            ->join(true)
            ->addFilter(new \Phalcon\Assets\Filters\Cssmin());

        $this->view->setLayout("index");
        $this->_registerToken();
        //$this->statusBoxAction();
        $auth = $this->auth();
        $id = $auth["uid"];
        $clases = VShoppingList::find("uid_ss=".$id." and status='ACTIVO' and statussalid='PAY' and postgrado_bra=1");
        $clases2 = VShoppingList::find("uid_ss=".$id." and status='ACTIVO' and statussalid='PAY' and postgrado_bra=2");
        $carrerasMaestrias = CdBrand::find("status='ACTIVE' and postgrado=1");
        $carrerasDoctorados = CdBrand::find("status='ACTIVE' and postgrado=2");

        $MDCD = CdBrand::findFirst("name='Maestria en Derecho Constitucional y Derecho'");
        $MDE = CdBrand::findFirst("name='Maestría en Derecho Electoral'");
        $DDE = CdBrand::findFirst("name='Doctorado en Derecho Electoral'");
        $DDCD = CdBrand::findFirst("name='Doctorado en Derecho Constitucional y Derecho'");
        $statusMDCD = 0;
        $statusMDE = 0;
        $statusDDE = 0;
        $statusDDCD = 0;
        $statusMDCDmenu = 0;
        $statusMDEmenu = 0;
        $statusDDEmenu = 0;
        $statusDDCDmenu = 0;

        foreach ($clases as $value) {
            if($value->getUidAttend()==$id){
                if($MDCD){
                    if ($value->getBrid()==$MDCD->getBrid()) {
                        $statusMDCD++;
                    }
                }
                if($MDE){
                    if ($value->getBrid()==$MDE) {
                        $statusMDE++;
                    }
                }
            }
        }
        foreach ($clases2 as $value) {
            if($value->getUidAttend()==$id){
                if ($DDE) {
                    if ($value->getBrid()==$DDE->getBrid()) {
                        $statusDDE++;
                    }
                }
                if ($DDCD) {
                    if ($value->getBrid()==$DDCD->getBrid()) {
                        $statusDDCD++;
                    }
                } 
            }
        }
        if($MDCD){
            $carrerasMaestriasMDCD = VShoppingList::find("uid_ss=".$id." and status='ACTIVO' and statussalid='PAY' and postgrado_bra=1 and brid=".$MDCD->getBrid());
            $materiasMDCD = VShoppingList::find("status='ACTIVO' and statussalid='PAY' and postgrado_bra=1");
            $this->view->setVar("materiasMDCD",$materiasMDCD);
            $statusMDCDmenu = 1;
        }
        if($MDE){
            $carrerasMaestriasMDE = VShoppingList::find("uid_ss=".$id." and status='ACTIVO' and statussalid='PAY' and postgrado_bra=1 and brid=".$MDE->getBrid());
            $materiasMDE = VShoppingList::find("status='ACTIVO' and statussalid='PAY' and postgrado_bra=1");
            $this->view->setVar("materiasMDE",$materiasMDE);
            //print_r($materiasMDE->toArray());exit;
            $statusMDEmenu = 1;
        }
        if($DDE){
            $carrerasDoctoradosDDE = VShoppingList::find("uid_ss=".$id." and status='ACTIVO' and statussalid='PAY' and postgrado_bra=2 and brid=".$DDE->getBrid());
            $doctoradosDDE = VShoppingList::find("status='ACTIVO' and statussalid='PAY' and postgrado_bra=2");
            $this->view->setVar("doctoradosDDE",$doctoradosDDE);
            $statusDDEmenu = 1;
        }
        if($DDCD){
            $carrerasDoctoradosDDCD = VShoppingList::find("uid_ss=".$id." and status='ACTIVO' and statussalid='PAY' and postgrado_bra=2 and brid=".$DDCD->getBrid());
            $doctoradosDDCD = VShoppingList::find("status='ACTIVO' and statussalid='PAY' and postgrado_bra=2");
            $this->view->setVar("doctoradosDDCD",$doctoradosDDCD);
            $statusDDCDmenu = 1;
        }
        if($auth["rol"]=="TEACHER"){
            $this->view->setVar("MDCD",$MDCD);
            $this->view->setVar("MDE",$MDE);
            $this->view->setVar("DDE",$DDE);
            $this->view->setVar("DDCD",$DDCD);
            $this->view->setVar("statusMDCDmenu",$statusMDCDmenu);
            $this->view->setVar("statusMDEmenu",$statusMDEmenu);
            $this->view->setVar("statusDDEmenu",$statusDDEmenu);
            $this->view->setVar("statusDDCDmenu",$statusDDCDmenu);
            $this->view->setVar("statusMDCD",$statusMDCD);
            $this->view->setVar("statusMDE",$statusMDE);
            $this->view->setVar("statusDDE",$statusDDE);
            $this->view->setVar("statusDDCD",$statusDDCD);
            $this->view->setVar("clases",$clases);
            $this->view->setVar("carrerasMaestriasMDCD",$carrerasMaestriasMDCD);
            $this->view->setVar("carrerasMaestriasMDE",$carrerasMaestriasMDE);
            $this->view->setVar("carrerasDoctoradosDDE",$carrerasDoctoradosDDE);
            $this->view->setVar("carrerasDoctoradosDDCD",$carrerasDoctoradosDDCD);
            $this->view->setVar("clases2",$clases2);
            $clase = CdClase::find("uid=".$id);
            $titleCenter = "Instituto Iberoamericano de Derecho Electoral";
            //$carrera = CdBrand::find($clase->getCarrera());
            //print_r($clase->toArray());exit;
            $this->view->setVar("clase",$clase);
            $titleToBack = "Campus Virtual";
            //$this->view->setVar("carrera",$carrera);
        }
        if($auth["rol"]=="STUDENT"){
            $client = CdClient::findFirst("uid=".$id);
            $find = CdClaseStudent::findFirst("clid=".$client->getClid()." and status='ACTIVO'");
            $clase = CdClase::findFirst($find->getClasid());
            $this->view->setVar("clase",$clase);
            $student = CdClient::findFirst("email='".$auth["email"]."'");
            $this->view->setVar("estudiante",$student);
            $carrera = CdBrand::findFirst($student->getCarrera());
            $titleCenter = $carrera;
            $statusstudent = 1;
            $this->view->setVar("carrera",$carrera);
            $this->view->setVar("statusstudent",$statusstudent);
            $titleToBack = "Campus Virtual";
        }
        if($auth["rol"]=="ADMIN"){
            $carrerasMaestriasAllMDCD = VShoppingList::find("status='ACTIVO' and statussalid='PAY' and postgrado_bra=1 and brid=".$MDCD->getBrid());
            $carrerasMaestriasAllMDE = VShoppingList::find("status='ACTIVO' and statussalid='PAY' and postgrado_bra=1 and brid=".$MDE->getBrid());
            $carrerasDoctoradosAllDDE = VShoppingList::find("status='ACTIVO' and statussalid='PAY' and postgrado_bra=1 and brid=".$DDE->getBrid());
            $carrerasDoctoradosAllDDCD = VShoppingList::find("status='ACTIVO' and statussalid='PAY' and postgrado_bra=1 and brid=".$DDCD->getBrid());
            $this->view->setVar("carrerasMaestriasAllMDCD",$carrerasMaestriasAllMDCD);
            $this->view->setVar("carrerasMaestriasAllMDE",$carrerasMaestriasAllMDE);
            $this->view->setVar("carrerasDoctoradosAllDDE",$carrerasDoctoradosAllDDE);
            $this->view->setVar("carrerasDoctoradosAllDDCD",$carrerasDoctoradosAllDDCD);
            $generaciones = CdGeneracion::find("status='ACTIVO'");
            $this->view->setVar("generaciones",$generaciones);
            $titleCenter="Instituto Iberoamericano de Derecho Electoral";
            $titleToBack = "Campus Virtual";
        }



        $this->view->setVar("titleCenter",$titleCenter);
        $this->view->setVar("titleToBack",$titleToBack);
        $this->view->setVar("id",$id);
        $this->view->setVar("auth",$auth);
    }
    public function auth(){
        return $this->session->get("auth");
    }
    public function response($dataArray,$status)
    {
        $this->view->disable();
        if($status==200){
            $this->response->setStatusCode($status, "OK");
        }else{
            $this->response->setStatusCode($status, "ERROR");
        }
        $this->response->setJsonContent($dataArray);
        $this->response->send();
        exit();
    }
    protected function getEstadosJson(){
        $json_file = dirname(dirname(dirname(dirname(__DIR__))))."/public/api/json/estados.json";
        $get_file = file_get_contents($json_file);
        if(!file_exists($json_file) || empty($get_file)){
            $file = fopen($json_file,"wb");
            $find = CdEstados::find();
            $content = array();
            foreach($find as $values){
                $content[$values->getEid()] = $values->getNombre();
            }
            $json = json_encode($content);
            fwrite($file,$json);
            fclose($file);
            return $json;
        }else{
            return $get_file;
        }
    }
    protected function getMunicipiosJson(){
        $json_file = dirname(dirname(dirname(dirname(__DIR__))))."/public/api/json/municipios.json";
        $get_file = file_get_contents($json_file);
        if(!file_exists($json_file) || empty($get_file)){
            $file = fopen($json_file,"wb");
            $find = CdMunicipios::find();
            $content = array();
            foreach($find as $values){
                $content[$values->getMpid()] = array("eid"=>$values->getEid(),"nombre"=>$values->getNombre());
            }
            $json = json_encode($content);
            fwrite($file,$json);
            fclose($file);
            return $json;
        }else{
            return $get_file;
        }
    }
    protected function getBrandsJson($status){
        $json_file = dirname(dirname(dirname(dirname(__DIR__))))."/public/json/brands.json";
        $get_file = file_get_contents($json_file);
        if($status==1){
            if(!file_exists($json_file) || empty($get_file)){
                $file = fopen($json_file,"wb");
                $find = CdBrand::find("status='ACTIVE'");
                $content = array();
                foreach($find as $values){
                    $content[$values->getBrid()] = $values->getName();
                }
                $json = json_encode($content,true);
                fwrite($file,$json);
                fclose($file);
                return $json;
            }else{
                return $get_file;
            }
        }elseif($status==2){
            $file = fopen($json_file,"wb");
            $find = CdBrand::find("status='ACTIVE'");
            $content = array();
            foreach($find as $values){
                $content[$values->getBrid()] = $values->getName();
            }
            $json = json_encode($content,true);
            fwrite($file,$json);
            fclose($file);
            return true;
        }
    }
    protected function getUsersJson($status){
        $json_file = dirname(dirname(dirname(dirname(__DIR__))))."/public/json/users.json";
        $get_file = file_get_contents($json_file);
        if($status==1){
            if(!file_exists($json_file) || empty($get_file)){
                $file = fopen($json_file,"wb");
                $find = CdUser::find("status='ACTIVE'");
                $content = array();
                foreach($find as $values){
                    $content[$values->getUid()] = $values->getName()." ".$values->getLastName();
                }
                $json = json_encode($content);
                fwrite($file,$json);
                fclose($file);
                return $json;
            }else{
                return $get_file;
            }
        }elseif($status==2){
            $file = fopen($json_file,"wb");
            $find = CdUser::find("status='ACTIVE'");
            $content = array();
            foreach($find as $values){
                $content[$values->getUid()] = $values->getName()." ".$values->getLastName();
            }
            $json = json_encode($content);
            fwrite($file,$json);
            fclose($file);
            return true;
        }
    }
    protected function url_clean($string) {
        $string = mb_strtolower(str_replace("'",'',str_replace(' ', '-',str_replace('-','',$string))), 'UTF-8');
        $utf8 = array(
            '/[áàâãªä]/u'   =>   'a',
            '/[ÁÀÂÃÄ]/u'    =>   'A',
            '/[ÍÌÎÏ]/u'     =>   'I',
            '/[íìîï]/u'     =>   'i',
            '/[éèêë]/u'     =>   'e',
            '/[ÉÈÊË]/u'     =>   'E',
            '/[óòôõºö]/u'   =>   'o',
            '/[ÓÒÔÕÖ]/u'    =>   'O',
            '/[úùûü]/u'     =>   'u',
            '/[ÚÙÛÜ]/u'     =>   'U',
            '/ç/'           =>   'c',
            '/Ç/'           =>   'C',
            '/ñ/'           =>   'n',
            '/Ñ/'           =>   'N',
            '/–/'           =>   '-',
            '/:/'           =>   '-', // UTF-8 hyphen to "normal" hyphen
            '/!/'           =>   '',
            '/#/'           =>   '',// UTF-8 hyphen to "normal" hyphen
            '/¡/'           =>   '', // UTF-8 hyphen to "normal" hyphen
            '/@/'           =>   '', // UTF-8 hyphen to "normal" hyphen
            '/,/'           =>   '', // UTF-8 hyphen to "normal" hyphen
            '/[’‘‹›‚¿?]/u'    => '', // Literally a single quote
            '/[“”«»„""]/u'    => '', // Double quote
            '/ /'           =>   '', // nonbreaking space (equiv. to 0x160)
        );
        //$string = preg_replace('/[^A-Za-z0-9áéíóúÁÉÍÓÚñÑ\-!¡¿?@]/', '', $string); // Removes special chars.
        $string = preg_replace('/[^(\x20-\x7F)]*/', '', $string);
        return preg_replace(array_keys($utf8),array_values($utf8),$string); // Removes special chars.
        //'/[^A-Za-z0-9áéíóúÁÉÍÓÚñÑ\-!¡¿?@]/', '',
    }
    protected function dateSpanish(){
        return array(
            "01"=>"Enero",
            "02"=>"Febrero",
            "03"=>"Marzo",
            "04"=>"Abril",
            "05"=>"Mayo",
            "06"=>"Junio",
            "07"=>"Julio",
            "08"=>"Agosto",
            "09"=>"Septiembre",
            "10"=>"Octubre",
            "11"=>"Noviembre",
            "12"=>"Diciembre"
        );
    }
    protected function expressions(){
        return $utf8 = array(
            '/[áàâãªä]/u'   =>   'a',
            '/[ÁÀÂÃÄ]/u'    =>   'A',
            '/[ÍÌÎÏ]/u'     =>   'I',
            '/[íìîï]/u'     =>   'i',
            '/[éèêë]/u'     =>   'e',
            '/[ÉÈÊË]/u'     =>   'E',
            '/[óòôõºö]/u'   =>   'o',
            '/[ÓÒÔÕÖ]/u'    =>   'O',
            '/[úùûü]/u'     =>   'u',
            '/[ÚÙÛÜ]/u'     =>   'U',
            '/ç/'           =>   'c',
            '/Ç/'           =>   'C',
            '/ñ/'           =>   'n',
            '/Ñ/'           =>   'N',
            '/–/'           =>   '-',
            '/:/'           =>   '-', // UTF-8 hyphen to "normal" hyphen
            '/!/'           =>   '', // UTF-8 hyphen to "normal" hyphen
            '/¡/'           =>   '', // UTF-8 hyphen to "normal" hyphen
            '/@/'           =>   '', // UTF-8 hyphen to "normal" hyphen
            '/,/'           =>   '', // UTF-8 hyphen to "normal" hyphen
            '/[’‘‹›‚¿?]/u'    => '', // Literally a single quote
            '/[“”«»„""]/u'    => '', // Double quote
            '/ /'           =>   '', // nonbreaking space (equiv. to 0x160)
        );
    }
    protected function getFormatDate($date){
        $dateP = $date;
        $newDate = explode("/",$dateP);
        return $newDate = $newDate["2"]."-".$newDate["1"]."-".$newDate["0"];
    }
    public function setStockProcedure($pid,$val,$status){
        $cd_stock = new CdStock();
        $sql = "CALL `stock_procedure`($pid,$val,'$status');";
        return $cd_stock->getReadConnection()->query($sql)->fetchAll();
    }
    protected function setStShoppingPrint($salid){
        $print = new VShoppingList();
        $sql = "CALL `st_shopping_print`($salid);";
        return $print->getReadConnection()->query($sql)->fetchAll();
    }
    public function validationJs(){
        $this->assets->collection('jsValidation')
            ->setTargetPath("dash/js/validation.min.js")
            ->setTargetUri("dash/js/validation.min.js")
            ->addJs("dash/js/plugins/bootstrapV/formValidation.min.js")
            ->addJs("dash/js/plugins/bootstrapV/bootstrapV.min.js")
            ->addJs("dash/js/plugins/bootstrapV/es_ES.js")
            ->join(true)
            ->addFilter(new \Phalcon\Assets\Filters\Jsmin());
    }
    public function dataTable(){
        $this->assets->collection('jsDataTable')
            ->setTargetPath("dash/js/data-table.min.js")
            ->setTargetUri("dash/js/data-table.min.js")
            ->addJs("dash/js/plugins/datatables/jquery.dataTables.min.js")
            ->addJs("dash/js/plugins/datatables/functions_datatable.js")
            ->join(true)
            ->addFilter(new \Phalcon\Assets\Filters\Jsmin());
    }
    protected function getSession(){
        return $this->session->get("session");
    }
    private function generateToken(){
        return bin2hex(openssl_random_pseudo_bytes(32));
    }
    public function _registerToken(){
        $session = $this->getSession();
        if(empty($session)) {
            $token = $this->generateToken();
            $this->session->set("session",array(
                    "token" => $token,
                )
            );
            $auth = $this->auth();
            CdUser::findFirst($auth['uid'])->setKey($token)->update();
        }
    }
    
    public function metaHome($action,$canonical,$description,$image){
        $this->session->set("meta",
            array(
                "title"=>"$action",
                "url"=>$this->url->getBaseUri()."$canonical",
                "description"=>"$description",
                "image"=>$this->url->getBaseUri()."$image"
            )
        );
    }
    public function backView($title,$url){
        $this->session->set("back",
            array(
                "title"=>"$title",
                "url"=>"$url"
            )
        );
    }

/* Functions reports Excel*/
    protected function style($objectXls,$range){
        $objectXls->getActiveSheet()->getStyle($range)->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('f3d000');
        $style = array(
            'font' => array(
                'bold'  => true,
                'color' => array('rgb' => '000000'),
                'size'  => 16,
                'name'  => 'Calibri'
            ),
            'alignment' => array(
                'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER),
            'borders' => array(
                'allborders' => array(
                    'style' => \PHPExcel_Style_Border::BORDER_MEDIUM,
                    'color' => array('argb' => '1d4074'),

                ),
            ),
        );
        $objectXls->getActiveSheet()->getStyle($range)->applyFromArray($style);
        $objectXls->getActiveSheet()->getStyle($range)->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $objectXls->getActiveSheet()->getRowDimension('2')->setRowHeight(30);
    }
    protected function logo($objectXls,$range){
        $objectXls->getActiveSheet()->getStyle($range)->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('c7c7c7');
        $objectXls->getActiveSheet()->getRowDimension('1')->setRowHeight(130);
        $objectXls->getActiveSheet()->mergeCells($range);
        $sheet = $objectXls->getActiveSheet(); // first sheet
        $logo ="dash/img/BR900x170.png"; // Provide path to your logo file
        $objDrawing = new \PHPExcel_Worksheet_Drawing();
        $objDrawing->setName('Beauty Room');
        $objDrawing->setDescription('Logo Beauty Room');
        $objDrawing->setPath($logo);
        $objDrawing->setOffsetX(0);    // setOffsetX works properly
        $objDrawing->setOffsetY(0);  //setOffsetY has no effect
        $objDrawing->setCoordinates('A1');
        $objDrawing->setHeight(170); // logo height
        $objDrawing->setWorksheet($sheet);
    }
    protected function styleContent($objectXls,$range){
        $objectXls->getActiveSheet()->getStyle($range)->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('2A5681');
        $style = array(
            'font' => array(
                'bold'  => true,
                'color' => array('rgb' => 'FFFFFF'),
                'size'  => 14,
                'name'  => 'Calibri'
            ),
            'alignment' => array(
                'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER),
            'borders' => array(
                'outline' => array(
                    'style' => \PHPExcel_Style_Border::BORDER_MEDIUM,
                    'color' => array('argb' => '000000'),

                ),
            ),
        );
        $objectXls->getActiveSheet()->getStyle($range)->applyFromArray($style);
        $objectXls->getActiveSheet()->getStyle($range)->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $objectXls->getActiveSheet()->getRowDimension('2')->setRowHeight(30);
    }
    protected function download($element){
        $file_path = dirname(dirname(dirname(dirname(__DIR__))))."/public/reports/".$element;
        if (file_exists($file_path)){
            if (FALSE!== ($handler = fopen($file_path, 'r'))){
                header('Content-Description: File Transfer');
                header('Content-Type: application/octet-stream');
                header('Content-Disposition: attachment; filename='.basename($file_path));
                header('Expires: 0');
                header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
                header('Pragma: public');
                header ("Content-Length: ".filesize($file_path));
                readfile($file_path);
                exit;
            }
        }else{
            echo "no se econtro el xls";
            exit;
        }
    }
/* End Functions Reports Excel*/


    /*protected function getBox(){
        return $this->session->get("box");
    }
    private function statusBoxAction(){
        $date = date("Y-m-d");
        $cd_box = CdBoxCut::findFirst("date(date_creation)='$date' and status='CLOSE'");
        if($cd_box) {
            $this->session->set("box",array("status" => "CLOSE"));
        }else{
            $this->session->set("box",array("status" => "OPEN"));
        }
    }*/
}
