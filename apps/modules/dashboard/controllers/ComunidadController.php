<?php
namespace Modules\Dashboard\Controllers;

use Modules\Models\CdUser;
use Phalcon\Http\Request;

class ComunidadController extends ControllerBase{
    public function indexAction(){
        $auth = $this->auth();
        if($auth){
            $this->dataTable();
            $users = CdUser::find("rol='COORDINATOR' and status='ACTIVE'");
            $this->view->setVar("clients",$users);
        }else{
            $this->response(array("message"=>"error"),404);
        }
    }
}