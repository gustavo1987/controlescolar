<?php
namespace Modules\Dashboard\Controllers;
//require_once(dirname(dirname(dirname(dirname(__FILE__))))."/library/phpToPDF/phpToPDF.php");
require dirname(dirname(dirname(dirname(__FILE__))))."/library/PHPMailer/PHPMailerAutoload.php";
require dirname(dirname(dirname(dirname(__FILE__))))."/library/Classes/PHPExcel.php";
use Modules\Models\CdBoxCut;
use Modules\Models\CdClient;
use Modules\Models\CdCredit;
use Modules\Models\CdSale;
use Modules\Models\CdSubservicesSale;
use Modules\Models\CdUser;
use Modules\Models\CdGeneracion;
use Modules\Models\CdCupon;
use Modules\Models\CdSaleClient;
use Modules\Models\LogCdSale;
use Modules\Models\VSales;
use Modules\Models\VServicesSubServices;
use Modules\Models\VShoppingGeneral;
use Modules\Models\VShoppingList;
use Modules\Models\VStockProduct;
use Modules\Models\VUserSubservices;
use Modules\Models\VStudentClase;
use Modules\Models\VSaleCupon;
use Modules\Models\CdBrand;
use Phalcon\Forms\Element\Date;
use Phalcon\Http\Request;
use Phalcon\Http\Response;
use DateTime;

class SalesController extends ControllerBase{
    public function indexAction(){
        $auth = $this->auth();
        if(!$auth)$this->response(array("code"=>404,"message"=>"You do not have permission"),404);

        $this->dataTable();
        $this->scripts();
        $user = CdUser::find("status='ACTIVE'");
        $this->view->setVar("users",$user);

    }
    public function cancelAction(){
        $auth = $this->auth();
        $request = new Request();
        if($auth && $request->getPost() && $request->isAjax()){
            $salid = $request->getPost("salid");
            $pay = $request->getPost("pay");
            $cd_subservices_sale  = CdSubservicesSale::find("salid=$salid and type='VENTAS' and status='ACTIVO'");
            if(count($cd_subservices_sale)>=0){
                foreach($cd_subservices_sale as $values){
                    CdSubservicesSale::findFirst($values->getSubsalid())->setStatus("CANCELADO")->update();
                    $this->setStockProcedure($values->getPid(),$values->getQuantity(),"add");
                }
            }
            $cd_sales = CdSale::findFirst($salid);
            $cd_sales->setStatus("CANCEL")->setTotal($pay);
            if(!$cd_sales->update()) $this->response(array("code"=>300,"message"=>"Error try again"),200);
            $url = $this->url->get("dashboard/sales/completed/$salid");
            $this->response(array("code"=>200,"url"=>$url),200);

        }else{
            $this->response(array("code"=>404,"message"=>"You do not have permission"),404);
        }
    }
    public function numberAction(){
        $auth = $this->auth();
        if($auth){
            $param = $this->dispatcher->getParam("id")?$this->dispatcher->getParam("id"):0;
            $general_data = VShoppingGeneral::findFirst("salid=$param");
            if(!$general_data) return $this->response->redirect("dashboard");
            $this->scripts();
            $shopping_list = VShoppingList::find("salid=$param and status!='CANCELADO' order by subsalid asc");
            $status = 0;
            if($general_data->getStatus()=="PAY"){
                $status = 1;
                $this->assets->collection('cssPlugins')->setTargetPath("dash/css/print_bill.min.css")->setTargetUri("dash/css/print_bill.min.css")->addCss("dash/css/print_bill.css")->join(true)->addFilter(new \Phalcon\Assets\Filters\Cssmin());
                $st_shopping_print = $this->setStShoppingPrint($general_data->getSalid());
                $this->view->setVar("list",$st_shopping_print);
            }elseif($general_data->getStatus()=="PULLEDAPART"){
                $status = 2;
                $this->assets->collection('cssPlugins')->setTargetPath("dash/css/print_bill.min.css")->setTargetUri("dash/css/print_bill.min.css")->addCss("dash/css/print_bill.css")->join(true)->addFilter(new \Phalcon\Assets\Filters\Cssmin());
                $st_shopping_print = $this->setStShoppingPrint($general_data->getSalid());
                $this->view->setVar("list",$st_shopping_print);
            }else{
                $this->view->setVar("list",$shopping_list);
            }
            $generaciones = CdGeneracion::find("status='ACTIVO'");
            $this->view->setVar("generaciones",$generaciones);
            $this->view->setVar("data",$general_data);
            $this->view->setVar("status",$status);
        }else{
            $this->response(array("code"=>404,"message"=>"You do not have permission"),404);
        }
    }
    public function activeAction(){
        $auth = $this->auth();
        $request = new Request();
        if($auth){
            $uid = 1;
            $find_client = CdClient::findFirst($uid);
            if(!$find_client)$this->response(array("message"=>"User not found1","code"=>300),200);
            $cd_sale = new CdSale();
            $cd_sale->setUid($auth['uid'])
                ->setClid($find_client->getClid())
                ->setTotal(0)->setDiscount(0)->setReceived(0)
                ->setSendEmail(0)
                ->setEmail("empty")
                ->setDateCreation(date("Y-m-d H:i:s"))
                ->setStatus("CANCEL")
                ->setType("EFECTIVO")
                ->setGenid(1);
            if($cd_sale->save()){
                $cd_log_sale = new LogCdSale();
                @$cd_log_sale->setSalid($cd_sale->getSalid())->setUidadmin($auth['uid'])->setClid($find_client->getClid())->setStatus("CANCEL")->setDateCreation(date("Y-m-d H:s:i"))->save();
                $salid = $cd_sale->getSalid();
                $url = "dashboard/sales/number/$salid";
                return $this->response->redirect($url);
                //$this->response(array("url"=>$url),200);
            }else{
                foreach ($cd_sale->getMessages() as $message) {
                    $this->flash->error((string) $message);
                }
                $this->response(array("message"=>"Error try again, cd_sale not save","code"=>300),200);
            }
        }else{
            $this->response(array("code"=>404,"message"=>"You do not have permission"),404);
        }
    }
    public function payServicesAction(){
        $auth = $this->auth();
        $request = new Request();
        if(!($auth && $request->isPost() && $request->isAjax()))$this->response(array("code"=>404,"message"=>"You do not have permission"),404);
        $values = $request->getPost();
        /** Buscar si existe el cupón***/
        if($values['cupon']){
            $cupon = CdCupon::findFirst("cupid=".$values['cupon']);
            if($cupon){
                $cupon->setStatus("INACTIVO")
                    ->update();
            }
        }
        $cd_sale = CdSale::findFirst($values['salid']);
        if(!$cd_sale)$this->response(array("code"=>300),200);

        $descuento = isset($values['discount_ext']);

        if($descuento==true){
            $cd_sale->setTotal($values['total'])->setDiscount($values['discount_ext']);
        }
        else $cd_sale->setTotal($values['total']);

        if($values["type"]=="pay"){
            $cd_sale->setStatus("PAY")
                ->setReceived($values['received'])
                ->setType($values['typePay'])
                ->setGenid($values['genid']);
        }
        else {
            $received_apart = $values['received_apart'];

            $cd_sale->setStatus("PULLEDAPART")
                ->setReceived($received_apart)
                ->setType($values['typePay'])
                ->setName($values['name_client'])
                ->setPhone($values['phone_client'])
                ->setGenid($values['generacion']);
            $cd_credit = new CdCredit();
            $cd_credit->setStatus("ACTIVO")
                ->setDateCreation(date("Y-m-d H:i:s"))
                ->setCredit($received_apart)
                ->setSalid($cd_sale->getSalid());

            $cd_credit->save();
        }
        if(!$cd_sale->update())$this->response(array("code"=>300),200);

        $this->response(array("code"=>200),200);

    }

/*Abonar */
    public function payPulledApartAction(){
        $auth = $this->auth();
        $request = new Request();
        if($auth && $request->isPost() && $request->isAjax()) {
            $values = $request->getPost();

            $cd_sale = CdSale::findFirst($values['salid']);
            if(!$cd_sale)$this->response(array("code"=>300),200);

            $payed = $cd_sale->getReceived();
            $total = $cd_sale->getTotal();
            $pay = $values['pay_received_apart'];

            $new_payed = $payed+$pay;

            if($new_payed==$total){
                $cd_sale->setReceived($new_payed)
                    ->setStatus("PAY")->setPulletApart(1);
            }else{
                $cd_sale->setReceived($new_payed);
            }
            if($cd_sale->update()){
                $cd_credit = new CdCredit();
                $cd_credit->setSalid($values['salid'])
                    ->setCredit($values['pay_received_apart'])
                    ->setStatus("ACTIVE")
                    ->setDateCreation(date("Y-m-d H:s:i"));
                $cd_credit->save();
                $this->response(array("code"=>200,"content"=>$cd_sale->toArray()),200);
            }else{
                $this->response(array("code"=>300,"message"=>"No se ha podido guardar"),200);
            }
        }else{
            $this->response(array("code"=>404,"message"=>"You do not have permission"),404);
        }
    }
    public function pulledApartServicesAction(){
        //Valida si esta la sesión??
        $auth = $this->auth();
        //Crea una solicitud vacia ya que es por ajax
        $request = new Request();
        //Valida si esta bien la sesión y si la solicitud es por via ajax
        if($auth && $request->isPost() && $request->isAjax()) {
            /*En la variable $values obtiene los datos de la solicitud ajax*/
            $values = $request->getPost();
            /*Se busca la venta con el id salid obtenido de la solicitud*/
            $cd_sale = CdSale::findFirst($values['salid']);
            /*Si $cd_sale es diferente de vacio es correcto?????*/
            if(!$cd_sale)$this->response(array("code"=>300),200);
            /*se llena los datos obtenidos de $values a la variable de tipo CDSale y le llena sus campos*/
            $cd_sale->setTotal($values['total'])->setDiscount($values['discount_ext'])->setReceived($values['received'])->setStatus("PULLEDAPART");
            if(!$cd_sale->update())$this->response(array("code"=>300),200);

            $this->response(array("code"=>200),200);
        }
        //Si no coinciden la sesión y la solicitud ajax no entra
        else{
            $this->response(array("code"=>404,"message"=>"You do not have permission"),404);
        }
    }
    public function deleteabonoAction(){
        $auth = $this->auth();
        $request = new Request();
        if($auth && $request->isPost() && $request->isAjax()){
            $id = $request->getPost("id");
            $find = CdSale::findFirst($id);
            $find->setStatus("CANCEL");
            if($find->update())$this->response(array("code"=>"200","message"=>"SUCCESS"),200);
            $this->response(array("code"=>"404","message"=>"Error to delete apartado"),200);
        }else{
            $this->response(array("code"=>404,"message"=>"You do not have permission"),404);
        }
    }
    public function feedAction(){
        $auth = $this->auth();
        $request = new Request();
        if($auth && $request->isGet()){
            $values = $request->get();
            $word  = $values['q'];
            $content = array();
            switch($values['type']){
                case "service":
                    $find = VServicesSubServices::find("status='ACTIVE' and (name_services like '$word%' or name_sub_service like '$word%' ) order by name_services,name_sub_service");
                    if(count($find)>=0){
                        foreach($find as $values){
                            $content[$values->getSubsid()] = $values->getNameServices()." => ".$values->getNameSubService();
                        }
                    }
                    break;
                case "product":
                    $find = VStockProduct::find("status_brand='ACTIVE' and status_product='ACTIVE' and stock>0 and (name_brand like '$word%' or name_product like '$word%') order by name_brand,name_product");
                    if(count($find)>=0){
                        foreach($find as $values){
                            $content[$values->getPid()] = $values->getNameBrand()." => ".$values->getNameProduct();
                        }
                    }
                    break;
                case "client":
                    $find = CdClient::find("status='ACTIVO' order by name");
                    if(count($find)>=0){
                        foreach($find as $values){
                            $content[$values->getClid()] = $values->getName()." ".$values->getLastName();
                        }
                    }
                    break;
                case "product_sale":
                    $find = VStockProduct::find("status_brand='ACTIVE' and status_product='ACTIVE' and stock>0 and (ticket like '$word%' or ticket like '$word%') order by name_brand,name_product");
                    if(count($find)>=0){
                        foreach($find as $values){
                            $content[$values->getPid()] = $values->getTicket()." - ".$values->getNameBrand()." - ".$values->getNameProduct();
                        }
                    }
                    break;
                case "employed":
                    $find = CdUser::find("status='ACTIVE' order by name");
                    if(count($find)>=0){
                        foreach($find as $values){
                            $content[$values->getUid()] = $values->getName()." ".$values->getLastName();
                        }
                    }
                    break;
            }
            $this->response($content,200);
        }else{
            $this->response(array("code"=>404,"message"=>"You do not have permission"),404);
        }
    }
    
    public function feedSubServicesAction(){
        $auth = $this->auth();
        $request = new Request();
        if($auth && $request->isPost() && $request->isAjax()){
            $filter = null;
            $values = $request->getPost();
            $type_id = $values["type_id"];
            $salid = $values["salid"];
            $uidUser = $values["uidUser"];
            $find = array();
            $new = array();
            switch($values['type']){
                case "service":
                    $find = VServicesSubServices::findFirst("subsid=$type_id");
                    if(!$find)$this->response(array("code"=>404,"message"=>"Error find sub services"),200);

                    $percentage = 0;
                    $payout = $find->getPrice()-(($find->getPrice()*$find->getDiscount())/100);
                    $find_user = VUserSubservices::findFirst("uid=$uidUser and subsid={$find->getSubsid()}");
                    if($find_user)$percentage = $find_user->getPercentage();
                    $commission = $percentage==0?0:($find->getPrice()*$percentage)/100;

                    $new = new CdSubservicesSale();
                    $new->setSalid($salid)->setSubsid($find->getSubsid())->setQuantity(1)->setDateCreation(date("Y-m-d H:i:s"))->setPayout($payout)
                        ->setType("SERVICIO")->setStatus("ACTIVO")->setCommission($commission)->setUid($auth['uid'])->setPriceGeneral($find->getPrice());
                    if(!$new->save())$this->response(array("code"=>404,"message"=>"Error to save cd_sub_services_sales"),200);
                    break;
                case "product":
                    $find = VStockProduct::findFirst("pid=$type_id");
                    if(!$find)$this->response(array("code"=>404,"message"=>"Error find sub services"),200);

                    $new = new CdSubservicesSale();
                    $new->setSalid($salid)->setPid($find->getPid())->setQuantity(0)->setDateCreation(date("Y-m-d H:i:s"))->setPayout(0)
                        ->setType("CONSUMO")->setStatus("ACTIVO")->setUid($auth['uid'])->setCommission(0);
                    if(!$new->save())$this->response(array("code"=>404,"message"=>"Error to save cd_sub_services_sales"),200);
                    break;
                case "client":
                    $find = CdSale::findFirst($salid);
                    if(!$find)$this->response(array("code"=>404,"message"=>"Error find Sales services"),200);
                    $find->setClid($type_id);
                    $new = CdClient::findFirst($find->getClid());
                    if($find->update()){
                        $new_log = new LogCdSale();
                        $new_log->setSalid($find->getSalid())->setUidadmin($find->getUid())->setClid($find->getClid())->setStatus($find->getStatus())->setDateCreation(date("Y-m-d H:i:s"))->save();
                    }else $this->response(array("code"=>404,"message"=>"Error to update cd_sales"),200);
                    break;
                case "employed":
                    $find = CdSubservicesSale::find("salid=$salid");
                    if(!$find)$this->response(array("code"=>404,"message"=>"Error find Sales services"),200);
                    if(count($find)>=1){
                        foreach($find as $values){
                            $values->setUid($type_id);
                            if(!$values->update())$this->response(array("code"=>404,"message"=>"Error to update cd_sales"),200);
                        }
                    }
                    $filter = "employed";
                    $new = CdUser::findFirst($type_id);
                    break;
                case "product_sale":
                    $uidEmployed = $values["uidEmployed"];
                    $find = VStockProduct::findFirst("pid=$type_id");
                    if(!$find)$this->response(array("code"=>404,"message"=>"Error find sub services"),200);
                    $commission = $find->getPercentage()==0?0:($find->getSalePrice()*$find->getPercentage())/100;
                    $new = new CdSubservicesSale();
                    $new->setSalid($salid)->setPid($find->getPid())->setQuantity(1)->setDateCreation(date("Y-m-d H:i:s"))->setPayout($find->getSalePrice()-$commission)
                        ->setType("VENTAS")->setStatus("ACTIVO")->setCommission($commission)->setPriceGeneral($find->getSalePrice())->setDiscount(0);

                    if($uidEmployed>0)$new->setUid($uidEmployed);
                    else $new->setUid($auth['uid']);

                    if(!$new->save())$this->response(array("code"=>404,"message"=>"Error to save cd_sub_services_sales"),200);
                    ;
                    if(!$this->setStockProcedure($find->getPid(),1,"delete"))$this->response(array("code"=>404,"message"=>"Error update cd_stock"),200);
                    break;
            }
            $generacion = CdGeneracion::find("status='ACTIVO'");
            $this->response(array("result"=>array_merge($find->toArray(),$new->toArray()),"code"=>200,"type"=>$filter,"generacion"=>$generacion),200);
        }else{
            $this->response(array("code"=>404,"message"=>"You do not have permission"),404);
        }
    }
    public function updateElementsAction(){
        $auth = $this->auth();
        $request = new Request();
        if($auth && $request->isPost() && $request->isAjax()){
            $values = $request->getPost();
            $id = $values["id"];
            $val = $values["val"];
            switch($values['type']){
                case "service":
                    $cd_ss = CdSubservicesSale::findFirst($id);
                    if(!$cd_ss) $this->response(array("code"=>300),200);
                    $subsid = $cd_ss->getSubsid();
                    $uid = $cd_ss->getUid()?$cd_ss->getUid():$auth['uid'];
                    $percentage =0;
                    $find = VServicesSubServices::findFirst("subsid=$subsid");
                    if(!$find) $this->response(array("code"=>300),200);
                    $find_user = VUserSubservices::findFirst("uid=$uid and subsid=$subsid");
                    if($find_user)$percentage = $find_user->getPercentage();
                    $commission = $percentage==0?0:(($find->getPrice()*$percentage)/100)*$val;

                    $payout = $find->getPrice()-(($find->getDiscount()*$find->getPrice())/100);
                    $cd_ss->setCommission($commission)->setPayout($payout*$val)->setQuantity($val)->setPriceGeneral($val*$find->getPrice());
                    if(!$cd_ss->update())$this->response(array("code"=>300),200);
                    break;
                case "product":
                    $cd_ss = CdSubservicesSale::findFirst($id);
                    if(!$cd_ss) $this->response(array("code"=>300),200);
                    $cd_ss->setQuantity($val);
                    if(!$cd_ss->update())$this->response(array("code"=>300,"message"=>"Error to update cd_sub_services_sales"),200);
                    break;
                case "product_sale":
                    $cd_ss = CdSubservicesSale::findFirst($id);
                    if(!$cd_ss) $this->response(array("code"=>300),200);
                    $payout = $cd_ss->getPriceGeneral()-(($val*$cd_ss->getPriceGeneral())/100);
                    $cd_ss->setDiscount($val)->setPayout($payout);
                    if(!$cd_ss->update())$this->response(array("code"=>300,"message"=>"Error to update cd_sub_services_sales"),200);
                    break;
            }
            $this->response(array("code"=>200),200);
        }else{
            $this->response(array("code"=>404,"message"=>"You do not have permission"),404);
        }
    }
    public function changeStylistAction(){
        $auth = $this->auth();
        $request = new Request();
        if($auth && $request->isPost() && $request->isAjax()){
            $values = $request->getPost();
            $type_id = $values["type_id"];
            $user_services = $values["user_services"];
            $content = array("id"=>$type_id);
            $percentage =0;

            $cd_ss = CdSubservicesSale::findFirst($type_id);
            if(!$cd_ss)$this->response(array("code"=>300),200);

            $quantity = $cd_ss->getQuantity();
            $cd_ss->setUid($user_services);
            if($cd_ss->update()){
                $subsid = $cd_ss->getSubsid();
                $uid = $cd_ss->getUid();
                $find_user = "";
                switch($values['type_status']){
                    case "service":
                        $find_user = VUserSubservices::findFirst("uid=$uid and subsid=$subsid");
                        if($find_user)$percentage = $find_user->getPercentage();

                        $find = VServicesSubServices::findFirst("subsid=$subsid");
                        if(!$find) $this->response(array("code"=>300),200);

                        $commission = $percentage==0?0:(($find->getPrice()*$percentage)/100)*$quantity;
                        $cd_ss->setCommission($commission);
                        if(!$cd_ss->update())$this->response(array("code"=>300),200);
                        break;
                    case "product": $find_user = CdUser::findFirst($uid);
                        break;
                    case "product_sale": $find_user = CdUser::findFirst($uid); break;
                }
                $content["user"] = $find_user->getName()." ".$find_user->getLastName();
            }else{
                $this->response(array("code"=>300),200);
            }
            $this->response(array("result"=>$content,"code"=>200),200);
        }else{
            $this->response(array("code"=>404,"message"=>"You do not have permission"),404);
        }
    }
    public function getUsersAction(){
        $auth = $this->auth();
        $request = new Request();
        if($auth && $request->isPost() && $request->isAjax()){
            $users = json_decode($this->getUsersJson(1),true);
            $content = array();
            if(count($users)>=1){
                foreach($users as $key => $values){
                    $content[] = array("id"=>$key,"text"=>$values);
                }
            }
            $this->response(array("result"=>$content,"code"=>200),200);
        }else{
            $this->response(array("code"=>404,"message"=>"You do not have permission"),404);
        }
    }
    public function getClientsAction(){
        $auth = $this->auth();
        $request = new Request();
        if($auth && $request->isPost() && $request->isAjax()){
            $users = CdClient::find("status='ACTIVO' order by name='Venta' desc,name asc");
            $content = array();
            if(count($users)>=1){
                foreach($users as $values){
                    $content[] = array("id"=>$values->getClid(),"text"=>$values->getName()." ".$values->getLastName());
                }
            }
            $this->response(array("result"=>$content,"code"=>200),200);
        }else{
            $this->response(array("code"=>404,"message"=>"You do not have permission"),404);
        }
    }
    public function printAction(){
        $auth = $this->auth();
        if($auth){
            $this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_ACTION_VIEW);
            $param = $this->dispatcher->getParam("id")?$this->dispatcher->getParam("id"):0;
            $general_data = VShoppingGeneral::findFirst("salid=$param");
            if(!$general_data) return $this->response->redirect("dashboard");

            if($general_data->getStatus()=="PAY")$status=1;
            else if($general_data->getStatus()=="PULLEDAPART"){
                $status=2;
                $credit = CdCredit::find("salid={$general_data->getSalid()} order by crid desc limit 1");
                $this->view->setVar("credit",$credit->toArray());
            }

            $this->assets->collection('cssPlugins')->setTargetPath("dash/css/print_bill.min.css")->setTargetUri("dash/css/print_bill.min.css")->addCss("dash/css/print_bill.css")->join(true)->addFilter(new \Phalcon\Assets\Filters\Cssmin());
            $st_shopping_print = $this->setStShoppingPrint($general_data->getSalid());
            $this->view->setVar("list",$st_shopping_print);
            $this->view->setVar("data",$general_data);
            $this->view->setVar("status",$status);
        }else{
            $this->response(array("code"=>404,"message"=>"You do not have permission"),404);
        }
    }

/* open and close box */
    public function openAction(){
        $auth = $this->auth();
        $box = null;
        if($box['status']=='OPEN'){
            return $this->response->redirect("dashboard/sales/day?status=failed");
        }else{
            $cd_box = new CdBoxCut();
            $cd_box->setTotal(0)
                ->setUid($auth['uid'])
                ->setStatus("OPEN")
                ->setDateCreation(date("Y-m-d H:i:s"));
            if($cd_box->save()){
                return $this->response->redirect("dashboard/sales/day");
            }
        }
    }
/* End open and close box*/

/* report sales*/
    public function getSalesAction(){
        $auth = $this->auth();
        $request = new Request();
        if($auth && $request->getPost() && $request->isAjax()){
            $values = $request->getPost();
            
            $date_init = $this->getFormatDate($values['start_date']);
            $date_finish = $this->getFormatDate($values['cutoff_date']);

            $user = $values['user']=="general"?null:" and uid={$values['user']}";
            $start_date = $values['start_date']?" date(date_creation)>='$date_init'":null;
            $cutoff_date = $values['cutoff_date']?" date(date_creation)<='$date_finish'":null;
            $status = $values['status']=="AMBAS"?" (status='PAY' or status='CANCEL' or status='PULLEDAPART')":"status='{$values['status']}'";
            if($values['start_date']){
                $cutoff_date = $values['cutoff_date']?" and date(date_creation)<='$date_finish'":null;
                $status = $values['status']=="AMBAS"?" and (status='PAY' or status='CANCEL'  or status='PULLEDAPART')":" and status='{$values['status']}'";
            }
            if($values['cutoff_date']) $status = $values['status']=="AMBAS"?" and (status='PAY' or status='CANCEL' or status='PULLEDAPART')":" and status='{$values['status']}'";

            $query_credit = $start_date.$cutoff_date;
            $cd_credit = CdCredit::find("$query_credit group by salid");
            if(count($cd_credit)>0){
                $new_query = "";
                foreach ($cd_credit as $k => $vl){
                    if($k==0)$new_query = "salid={$vl->getSalid()}";
                    else $new_query = $new_query." or salid={$vl->getSalid()}";
                }
                $query = $start_date.$cutoff_date.$status.$user." or ($new_query) or pullet_apart=1 limit 500";
            }else{
                $query = $start_date.$cutoff_date.$status.$user." or pullet_apart=1 limit 500";
            }
            $cd_sale = VSales::find("$query");

            if(count($cd_sale)>=1){
                $content = array();
                $status =array("PAY"=>"Pagado","CANCEL"=>"Cancelada","PULLEDAPART"=>"Apartado");
                $total = 0;
                $t =0;
                foreach($cd_sale as $key => $values){
                    $v = 1;
                    $url_print = $this->url->get('dashboard/sales/print/'.$values->getSalid());
                    $url_view = $this->url->get('dashboard/sales/completed/'.$values->getSalid());
                    $url_list_print = $this->url->get('dashboard/sales/number/'.$values->getSalid());
                    if($values->getStatus()=="PAY"  and $values->getPulletApart()==0) {
                        $total += (double)$values->getTotal();
                        $t = $values->getTotal();
                    }
                    elseif($values->getStatus()=="PULLEDAPART" or $values->getPulletApart()==1){
                        $credit = CdCredit::find("$start_date $cutoff_date and salid={$values->getSalid()}");
                        if(count($credit)<=0)$v = 0;
                        else{
                            $tA = 0;
                            foreach($credit as $k => $vl){
                                $tA+=(double)$vl->getCredit();
                            }
                            $total+=(double)$tA;
                            $t = $tA;
                            $url_view = $this->url->get('dashboard/sales/completedapart/'.$values->getSalid());
                        }
                    }else if($values->getStatus()=="CANCEL"){
                        $t = $values->getTotal();
                    }
                    if($v==1){
                        $content[$key+1] = array(
                            "salid"=>$values->getSalid(),
                            "total"=>number_format($t,'2','.',','),
                            "status"=>$status[$values->getStatus()],
                            "discount"=>$values->getDiscount(),
                            "date"=>date("d-m-Y H:i",strtotime($values->getDateCreation())),
                            "user"=>$values->getName()." ".$values->getLastName(),
                            "url_print"=>$url_print,
                            "url_list_print"=>$url_list_print,
                            "url_view"=>$url_view
                        );
                    }
                    $t=0;
                }
                $breakdown = array("iva_general"=>0,"subtotal"=>0,"total"=>0);
                if($total>0){
                    $iva = 1.16;
                    $iva_general = $total - ($total/$iva);
                    $subtotal = ($total/$iva);
                    $breakdown = array("iva_general"=>number_format($iva_general,'2','.',','),"subtotal"=>number_format($subtotal,'2','.',','),"total"=>number_format($iva_general+$subtotal,'2','.',','));
                }
                $this->response(array("code"=>200,"message"=>"success","result"=>$content,"breakdown"=>$breakdown),200);
            }
            $this->response(array("code"=>300,"message"=>"O values"),200);
        }else{
            $this->response(array("code"=>404,"message"=>"You do not have permission"),404);
        }
    }
    public function completedAction(){
        $auth = $this->auth();
        if($auth){
            $param = $this->dispatcher->getParam("id")?$this->dispatcher->getParam("id"):0;
            $general_data = VShoppingGeneral::findFirst("salid=$param");
            if(!$general_data) return $this->response->redirect("dashboard");
            $this->dataTable();
            $this->scripts();
            $shopping_list = VShoppingList::find("salid=$param  order by subsalid asc");
            $this->view->setVar("list",$shopping_list);
            $this->view->setVar("data",$general_data);
            $this->view->setVar("auth",$this->auth());
        }else{
            $this->response(array("code"=>404,"message"=>"You do not have permission"),404);
        }
    }
    public function completedapartAction(){
        $auth = $this->auth();
        if($auth){
            $param = $this->dispatcher->getParam("id")?$this->dispatcher->getParam("id"):0;
            $general_data = VShoppingGeneral::findFirst("salid=$param");
            if(!$general_data) return $this->response->redirect("dashboard");
            $this->scripts();
            $shopping_list = $this->setStShoppingPrint($param);
            $this->view->setVar("list",$shopping_list);
            $this->view->setVar("data",$general_data);
        }else{
            $this->response(array("code"=>404,"message"=>"You do not have permission"),404);
        }
        $credito = CdCredit::find("salid=$param");
        $this->view->setVar("credit",$credito);
    }
    public function addstudentAction(){
         $auth = $this->auth();
         $clase = $this->dispatcher->getParam("id");
         $this->scripts();
        if($auth){
            $this->dataTable();
            $clients = null;
            if($auth['rol']!=="ADMIN")
                $clients = CdClient::find("status='ACTIVO' and uid={$auth['uid']}");
            else if($auth['rol']=="ADMIN"){
                $clients = CdClient::find("status='ACTIVO'");
            }
            $this->view->setVar("clients",$clients);
        }else{
            $this->response(array("message"=>"error"),404);
        }
        $this->view->setVar("clase",$clase);
    }
    public function listAction(){
         $auth = $this->auth();
         $clase = $this->dispatcher->getParam("id");
         $this->scripts();
        if($auth){
            $this->dataTable();
            if($auth['rol']!=="ADMIN"){
                $clients = VStudentClase::find("salid=".$clase." and statuscla='ACTIVO'");
            }
            else if($auth['rol']=="ADMIN"){
                $clients = VStudentClase::find("salid=".$clase);
            }
            $this->view->setVar("clients",$clients);
        }else{
            $this->response(array("message"=>"error"),404);
        }
        $this->view->setVar("clase",$clase);
    }
    public function salestudentclaseAction(){
         $auth = $this->auth();
         $carreras = $this->dispatcher->getParam("subsalid");
         $post = CdBrand::find();
         if($auth){
             
             
         }
        
    }
    public function saveaddstudentAction(){
        $request = new Request();
        $values = $request->getPost();
        $estudiante = CdClient::findFirst($values['id']);
        $salid = CdSale::findFirst($values['clase']);
       /* if($request->isAjax() && $estudiante && $salid){*/
        if($request->isAjax()){
            $estudianteclase = CdSaleClient::findFirst("clid=".$values['id']." and salid=".$values['clase']);
            
            if ($values['select']==1) {
                if($estudianteclase){
                    $estudianteclase->setStatus("ACTIVO")
                        ->setDateCreation(date("Y-m-d h:m:s"))
                        ->update();
                }
                else {
                    $estudianteclase=new CdSaleClient();
                    $estudianteclase->setSalid($values['clase'])
                            ->setClid($values['id'])
                            ->setStatus("ACTIVO")
                            ->setDateCreation(date("Y-m-d h:m:s"))
                            ->save();
                }
            }
            elseif($values['select']==0) {
                if($estudianteclase){
                    $estudianteclase->setStatus("INACTIVO")
                        ->setDateCreation(date("Y-m-d h:m:s"))
                        ->update();
                }
                else {
                    $estudianteclase=new CdSaleClient();
                    $estudianteclase->setSalid($values['clase'])
                            ->setClid($values['id'])
                            ->setStatus("INACTIVO")
                            ->setDateCreation(date("Y-m-d h:m:s"))
                            ->save();
                }
            }
            $this->response(array("code"=>200,"message"=>"ok"),200);
        }else{
            $this->response(array("message"=>"Error try again","code"=>"404"),404);
        }
    }
    public function clasesAction(){
        $auth = $this->auth();
        if($auth){
            $this->dataTable();
            $this->scripts();
            $date = date("Y-m-d");
            if($auth['rol']==("ADMIN"||"SUPERADMIN"))$cd_sale = VSales::find("status='PAY' and pullet_apart=0");
            else $cd_sale = VSales::find("uid={$auth['uid']}) and status='PAY' and pullet_apart=0");
            if(count($cd_sale)>=1){
                $content = array();
                $status =array("PAY"=>"Pagado","CANCEL"=>"Cancelada","PULLEDAPART"=>"Apartado");
                $total = 0;
                $t =0;
                foreach($cd_sale as $key => $values){
                    $v = 1;
                    $url_print = $this->url->get('dashboard/sales/print/'.$values->getSalid());
                    $url_view = $this->url->get('dashboard/sales/number/'.$values->getSalid());
                    
                    if($values->getStatus()=="PAY" and $values->getPulletApart()==0){
                        $total+=(double)$values->getTotal();
                        $t = $values->getTotal();

                    }
                    elseif($values->getStatus()=="PULLEDAPART" or $values->getPulletApart()==1){
                        $credit = CdCredit::find("date(date_creation)='$date' and salid={$values->getSalid()}");
                        if(count($credit)<=0)$v = 0;
                        else{
                            $tA = 0;
                            foreach($credit as $k => $vl){
                                $tA+=(double)$vl->getCredit();
                            }
                            $total+=(double)$tA;
                            $t = $tA;

                        }
                    }else if($values->getStatus()=="CANCEL"){
                        $t = $values->getTotal();
                    }
                    $materia = VShoppingList::findFirst("salid=".$values->getSalid());
                    if($v==1){
                        $content[$key+1] = array(
                            "salid"=>$values->getSalid(),
                            "total"=>number_format($t,'2','.',','),
                            "status"=>$status[$values->getStatus()],
                            "discount"=>$values->getDiscount()?$values->getDiscount():"0",
                            "date"=>date("d-m-Y H:i",strtotime($values->getDateCreation())),
                            "user"=>$values->getName()." ".$values->getLastName(),
                            "name"=>$materia->getNameProduct(),
                            "url_print"=>$url_print,
                            "url_view"=>$url_view
                        );
                    }
                    $t=0;
                }
                $breakdown = array("iva_general"=>0,"subtotal"=>0,"total"=>0);
                if($total>0){
                    $iva = 1.16;
                    $iva_general = $total - ($total/$iva);
                    $subtotal = ($total/$iva);
                    $breakdown = array("iva_general"=>number_format($iva_general,'2','.',','),"subtotal"=>number_format($subtotal,'2','.',','),"total"=>number_format($iva_general+$subtotal,'2','.',','));
                }
                $this->view->setVar("result",array_count_values());
                $this->view->setVar("result",$content);
                $this->view->setVar("breakdown",$breakdown);
            }
        }else{
            $this->response(array("code"=>404,"message"=>"You do not have permission"),404);
        }
    }
    public function dayAction(){
        $auth = $this->auth();
        if($auth){
            $this->dataTable();
            $this->scripts();
            //$date = date("Y-m-d");
            $date = date("Y-m-d");
           // if($auth['rol']=="ADMIN")$cd_sale = VSales::find("date(date_creation)='$date' or status='PULLEDAPART' or pullet_apart=1 limit 200");
            


            //if($auth['rol']==("ADMIN"||"SUPERADMIN"))$cd_sale = VSales::find(" (CAST(date_creation AS DATE)='".$date."') or (status='PULLEDAPART' or pullet_apart=1)");


            if($auth['rol']==("ADMIN"||"SUPERADMIN"))$cd_sale = VShoppingList::find("statussalid!='PULLEDAPART'");
            

            $this->view->setVar("result",$cd_sale);


            //else $cd_sale = VSales::find("(date(date_creation)='$date' and uid={$auth['uid']} ) or status='PULLEDAPART' or pullet_apart=1  limit 200");
            
            /*else $cd_sale = VSales::find(" (CAST(date_creation AS DATE)='".$date."' and uid={$auth['uid']}) or (status='PULLEDAPART' or pullet_apart=1)");
            if(count($cd_sale)>=1){
                $content = array();
                $status =array("PAY"=>"Pagado","CANCEL"=>"Cancelada","PULLEDAPART"=>"Apartado");
                $total = 0;
                $t =0;
                foreach($cd_sale as $key => $values){
                    $v = 1;

                    
                    $url_print = $this->url->get('dashboard/sales/print/'.$values->getSalid());
                    $url_view = $this->url->get('dashboard/sales/number/'.$values->getSalid());
                    
                    if($values->getStatus()=="PAY" and $values->getPulletApart()==0){
                        $total+=(double)$values->getTotal();
                        $t = $values->getTotal();

                    }
                    elseif($values->getStatus()=="PULLEDAPART" or $values->getPulletApart()==1){
                        
                        $credit = CdCredit::find("date(date_creation)='$date' and salid={$values->getSalid()}");
                        
                        if(count($credit)<=0)$v = 0;
                        else{
                            $tA = 0;
                            foreach($credit as $k => $vl){
                                $tA+=(double)$vl->getCredit();
                            }
                            $total+=(double)$tA;
                            $t = $tA;

                        }
                    }else if($values->getStatus()=="CANCEL"){
                        $t = $values->getTotal();
                    }
                    if($v==1){
                        $content[$key+1] = array(
                            "salid"=>$values->getSalid(),
                            "total"=>number_format($t,'2','.',','),
                            "status"=>$status[$values->getStatus()],
                            "discount"=>$values->getDiscount()?$values->getDiscount():"0",
                            "date"=>date("d-m-Y H:i",strtotime($values->getDateCreation())),
                            "user"=>$values->getName()." ".$values->getLastName(),
                            "url_print"=>$url_print,
                            "url_view"=>$url_view
                        );
                    }

                    $t=0;
                }
                $breakdown = array("iva_general"=>0,"subtotal"=>0,"total"=>0);
                if($total>0){
                    $iva = 1.16;
                    $iva_general = $total - ($total/$iva);
                    $subtotal = ($total/$iva);
                    $breakdown = array("iva_general"=>number_format($iva_general,'2','.',','),"subtotal"=>number_format($subtotal,'2','.',','),"total"=>number_format($iva_general+$subtotal,'2','.',','));
                }
                $this->view->setVar("result",array_count_values());
                $this->view->setVar("result",$content);
                $this->view->setVar("breakdown",$breakdown);
            }*/
        }else{
            $this->response(array("code"=>404,"message"=>"You do not have permission"),404);
        }
    }



    /******************************************/
    /**********  Cupones action ***************/
    /******************************************/
    public function cuponesAction(){
        $auth = $this->auth();
        if($auth){
            $this->dataTable();
            $this->scripts();
            $date = date("Y-m-d");
            $cd_sale = VSaleCupon::find();
                $this->view->setVar("result",$cd_sale);
            
        }else{
            $this->response(array("code"=>404,"message"=>"You do not have permission"),404);
        }
    }
    public function printcuponAction(){
        $this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_ACTION_VIEW);
        $param = $this->dispatcher->getParam("id")?$this->dispatcher->getParam("id"):0;
        $cupon = VSaleCupon::findFirst("cupid=".$param);
        $this->view->setVar("cupon",$cupon);
    }
    public function cuponviewAction(){
       /* $this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_ACTION_VIEW);*/
        $param = $this->dispatcher->getParam("id")?$this->dispatcher->getParam("id"):0;
        $cupon = VSaleCupon::findFirst("cupid=".$param);
        $this->view->setVar("cupon",$cupon);
    }
    public function cuponverificarAction(){
        $request = new Request();
        if($request->isPost() && $request->isAjax()){
            $values = $request->getPost();
            $id = $values["cupon"];
            $cupon = CdCupon::findFirst("cupid=".$id);
            /*print_r($cupon);*/
            if($cupon){
                $today = date("Y-m-d");
                $vence = date('Y-m-d', strtotime(str_replace('-','/', $cupon->getDateCreation())));
                
                $diff = abs(strtotime($today) - strtotime($vence));
                $years = floor($diff / (365*60*60*24)); 
                $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
                $days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));

                if($months==0&&$days<30&&$cupon->getStatus()=="ACTIVO"){
                    $costcupon = $cupon->getTotal();
                    $mensaje="Cupon valido";
                }
                else{
                    $costcupon = 0;
                    $mensaje="Cupón expirado";
                }                
                $this->response(array("result"=>"guardado","mensaje"=>$mensaje,"newmount"=>$costcupon,"code"=>200),200);
            }
            else $this->response(array("code"=>404,"message"=>"No existe este cupón"),200);
        }
        else{
             $this->response(array("code"=>404,"message"=>"Sin autorización y sin envio de datos"),200);
        }
    }
    /* Creación de cupon al dar click en el botón*/
    public function createcuponAction(){
        $auth = $this->auth();
        $request = new Request();
        if($auth && $request->isPost() && $request->isAjax()){
            $values = $request->getPost();
            $id = $values["id"];
            $apartado = CdSale::findFirst($id);
            $cd_subservices_sale  = CdSubservicesSale::find("salid=".$id." and type='VENTAS' and status='ACTIVO'");
            if(count($cd_subservices_sale)>=0){
                foreach($cd_subservices_sale as $values){
                    CdSubservicesSale::findFirst($values->getSubsalid())->setStatus("CANCELADO")->update();
                    $this->setStockProcedure($values->getPid(),$values->getQuantity(),"add");
                }
            }
            $cupon1 = new CdCupon();
            $cupon1->setSalid($id)
                ->setClid($apartado->getClid())
                ->setUid($apartado->getUid())
                ->setDateCreation(date("Y-m-d H:i:s"))
                ->setTotal($apartado->getReceived())
                ->setStatus("ACTIVO");
            $apartado->setStatus("CUPON");
                if($cupon1->save()&&$apartado->update()){
                    $this->response(array("result"=>"guardado","content"=>$cupon1->getCupid(),"code"=>200),200);
                }
                else{
                    $this->response(array("code"=>404,"message"=>"Error to save cd_cupon"),200);
                }
        }
        else{
             $this->response(array("code"=>404,"message"=>"Sin autorización y sin envio de datos"),200);
        }
    }
    /* End creación de cupon al dar click en el botón*/
    /*** Eliminar venta ***/
    public function deleteAction(){
        $auth = $this->auth();
        $request = new Request();
        if($auth && $request->isPost() && $request->isAjax()){
            $values = $request->getPost();
            $id = $values['id'];
            switch($values['type']){
                case "service":
                    $find_ss = CdSubservicesSale::findFirst($id);
                    if(!$find_ss)$this->response(array("message"=>"subService sale not found ","code"=>300),200);
                    $find_ss->setStatus("CANCELADO");
                    if(!$find_ss->update())$this->response(array("message"=>"cd_services_sale not update","code"=>300),200);
                    break;
                case "product":
                    $find_ss = CdSubservicesSale::findFirst($id);
                    if(!$find_ss)$this->response(array("message"=>"CdSubservicesSale not found","code"=>300),200);
                    $find_ss->setStatus("CANCELADO");
                    if(!$find_ss->update())$this->response(array("message"=>"cd_services_sale not update","code"=>300),200);
                    break;
                case "client":
                    break;
                case "product_sale":
                    $find_ss = CdSubservicesSale::findFirst($id);
                    if(!$find_ss)$this->response(array("message"=>"CdSubservicesSale not found","code"=>300),200);
                    $find_ss->setStatus("CANCELADO");

                    if($find_ss->update() && $this->setStockProcedure($find_ss->getPid(),1,"add"));
                    else $this->response(array("code"=>404,"message"=>"Error update cd_stock"),200);

                    break;
                default : $this->response(array("message"=>"type not found","code"=>300),200);
                    break;
            }
            $this->response(array("code"=>200),200);
        }else{
            $this->response(array("code"=>404,"message"=>"You do not have permission"),404);
        }
    }
    public function deletecuponAction(){
        $auth = $this->auth();
        $request = new Request();
        if($auth && $request->isPost() && $request->isAjax()){
            $id = $request->getPost("id");
            $find = CdCupon::findFirst($id);
            $find->setStatus("CANCEL");
            $sale = CdSale::findFirst($find->getSalid());
            $sale->setStatus("CANCEL");

            if($find->update()&&$sale->update())$this->response(array("code"=>"200","message"=>"SUCCESS"),200);
            $this->response(array("code"=>"404","message"=>"Error to delete cupon"),200);
        }else{
            $this->response(array("code"=>404,"message"=>"You do not have permission"),404);
        }
    }

    /***********************************************/
    /************ Fin de cupones action ************/
    /***********************************************/

    public function dayapartAction(){
        $auth = $this->auth();
        if($auth){
            $this->dataTable();
            $this->scripts();
            $date = date("Y-m-d");
            $cd_sale = VSales::find("status='PULLEDAPART'");
            if(count($cd_sale)>=1){
                $content = array();
                $status =array("PAY"=>"Pagado","CANCEL"=>"Cancelada");
                $total = 0;
                foreach($cd_sale as $key => $values){
                    $url_print = $this->url->get('dashboard/sales/print/'.$values->getSalid());
                    $url_view = $this->url->get('dashboard/sales/number/'.$values->getSalid());
                    if($values->getStatus()=="PAY")$total+=(double)$values->getTotal();
                    $content[$key+1] = array(
                        "salid"=>$values->getSalid(),
                        "total"=>number_format($values->getTotal(),'2','.',','),
                        "status"=>$status[$values->getStatus()],
                        "discount"=>$values->getDiscount()?$values->getDiscount():"0",
                        "date"=>date("d-m-Y H:i",strtotime($values->getDateCreation())),
                        "user"=>$values->getName()." ".$values->getLastName(),
                        "url_print"=>$url_print,
                        "url_view"=>$url_view,
                        "apart"=>$values->getReceived(),
                        "total_clean"=>$values->getTotal(),
                        "name_client"=>$values->getNameClient()
                    );
                }
                $breakdown = array("iva_general"=>0,"subtotal"=>0,"total"=>0);
                if($total>0){
                    $iva = 1.16;
                    $iva_general = $total - ($total/$iva);
                    $subtotal = ($total/$iva);
                    $breakdown = array("iva_general"=>number_format($iva_general,'2','.',','),"subtotal"=>number_format($subtotal,'2','.',','),"total"=>number_format($iva_general+$subtotal,'2','.',','));
                }
                $this->view->setVar("result",$content);
                $this->view->setVar("breakdown",$breakdown);
                $this->view->setVar("auth",$this->auth());
            }
        }else{
            $this->response(array("code"=>404,"message"=>"You do not have permission"),404);
        }
    }
    public function endapartAction(){
        $auth = $this->auth();
        if($auth){
            $this->dataTable();
            $this->scripts();
            if($auth['rol']=="ADMIN")$cd_sale = VSales::find("status='PULLEDAPART");
            else $cd_sale = VSales::find("status='PULLEDAPART' and uid={$auth['uid']}");
            if(count($cd_sale)>=1){
                $content = array();
                $status =array("PAY"=>"Pagado","CANCEL"=>"Cancelada");
                $total = 0;
                foreach($cd_sale as $key => $values){
                    $url_print = $this->url->get('dashboard/sales/print/'.$values->getSalid());
                    $url_view = $this->url->get('dashboard/sales/number/'.$values->getSalid());
                    if($values->getStatus()=="PAY")$total+=(double)$values->getTotal();
                    $content[$key+1] = array(
                        "salid"=>$values->getSalid(),
                        "total"=>number_format($values->getTotal(),'2','.',','),
                        "status"=>$status[$values->getStatus()],
                        "discount"=>$values->getDiscount()?$values->getDiscount():"0",
                        "date"=>date("d-m-Y H:i",strtotime($values->getDateCreation())),
                        "user"=>$values->getName()." ".$values->getLastName(),
                        "url_print"=>$url_print,
                        "url_view"=>$url_view,
                        "apart"=>$values->getReceived(),
                        "total_clean"=>$values->getTotal(),
                        "date_creation"=>$values->getDateCreation()
                    );
                }
                $breakdown = array("iva_general"=>0,"subtotal"=>0,"total"=>0);
                if($total>0){
                    $iva = 1.16;
                    $iva_general = $total - ($total/$iva);
                    $subtotal = ($total/$iva);
                    $breakdown = array("iva_general"=>number_format($iva_general,'2','.',','),"subtotal"=>number_format($subtotal,'2','.',','),"total"=>number_format($iva_general+$subtotal,'2','.',','));
                }
                $this->view->setVar("apartAll",$content);
                $this->view->setVar("breakdown",$breakdown);
            }
            /*$sale = VSales::find("status='PULLEDAPART'");
            $this->view->setVar("apartAll",$sale);*/

        }else{
            $this->response(array("code"=>404,"message"=>"You do not have permission"),404);
        }
    }
    public function exportGeneralAction(){
        $auth = $this->auth();
        $user = $this->request->get("user");
        $start_date = $this->request->get("start_date")?$this->getFormatDate($this->request->get("start_date")):null;
        $cutoff_date = $this->request->get("cutoff_date")?$this->getFormatDate($this->request->get("cutoff_date")):null;
        $status = $this->request->get("status");
        if($auth && $status){
            $objPHPExcel = new \PHPExcel();
            $objPHPExcel->getProperties()->setCreator($auth['name'])
                ->setLastModifiedBy($auth['email'])
                ->setTitle("Reporte de venta PONS")
                ->setSubject("Información de control de ventas")
                ->setDescription("Información de venta")
                ->setKeywords("XLS Microsoft")
                ->setCategory("Boutique");
            $this->xlsGeneral($user,$start_date,$cutoff_date,$status,$objPHPExcel);
        }else{
            exit();
        }
    }
    private function xlsGeneral($type_user,$date_init,$date_finish,$type_status,$objectXls){
        $cont = 0;
        $objectXls->setActiveSheetIndex(0)
            ->setCellValue('A2', 'No. Venta')
            ->setCellValue('B2', 'Cliente')
            ->setCellValue('C2', 'Total')
            ->setCellValue('D2', 'Tarjeta Regalo')
            ->setCellValue('E2', 'Vendedor')
            ->setCellValue('F2', 'Fecha')
            ->setCellValue('G2', 'Estatus')
            ->setCellValue('H2', '')
            ->setCellValue('I2', 'Subtotal')
            ->setCellValue('J2', 'IVA')
            ->setCellValue('K2', 'Total');

        $user = $type_user=="general"?null:" and uid={$type_user}";
        $start_date = $date_init?" date(date_creation)>='$date_init'":null;
        $cutoff_date = $date_finish?" date(date_creation)<='$date_finish'":null;
        $status = $type_status=="AMBAS"?" (status='PAY' or status='CANCEL' or status='PULLEDAPART')":"status='{$type_status}'";
        if($date_init){
            $cutoff_date = $date_finish?" and date(date_creation)<='$date_finish'":null;
            $status = $type_status=="AMBAS"?" and (status='PAY' or status='CANCEL'  or status='PULLEDAPART')":" and status='{$type_status}'";
        }
        if($date_finish) $status = $type_status=="AMBAS"?" and (status='PAY' or status='CANCEL' or status='PULLEDAPART')":" and status='{$type_status}'";

        $query_credit = $start_date.$cutoff_date;
        $cd_credit = CdCredit::find("$query_credit group by salid");
        if(count($cd_credit)>0){
            $new_query = "";
            foreach ($cd_credit as $k => $vl){
                if($k==0)$new_query = "salid={$vl->getSalid()}";
                else $new_query = $new_query." or salid={$vl->getSalid()}";
            }
            $query = $start_date.$cutoff_date.$status.$user." or ($new_query) or pullet_apart=1 limit 500";
        }else{
            $query = $start_date.$cutoff_date.$status.$user." or pullet_apart=1 limit 500";
        }
        $cd_sale = VSales::find("$query");
        if(count($cd_sale)>=1){
            $status =array("PAY"=>"Pagado","CANCEL"=>"Cancelada","PULLEDAPART"=>"Apartado");
            $total = 0;
            $t =0;
            foreach($cd_sale as $key => $value){
                $v=1;
                if($value->getStatus()=="PAY" and $value->getPulletApart()==0){
                    $total+=(double)$value->getTotal();
                    $t = $value->getTotal();
                }
                elseif($value->getStatus()=="PULLEDAPART" or $value->getPulletApart()==1){
                    $credit = CdCredit::find("$start_date $cutoff_date and salid={$value->getSalid()}");
                    if(count($credit)<=0)$v = 0;
                    else{
                        $tA = 0;
                        foreach($credit as $k => $vl){
                            $tA+=(double)$vl->getCredit();
                        }
                        $total+=(double)$tA;
                        $t = $tA;
                    }
                }
                if($v==1){
                    $cont =$key+3;
                    $objectXls->setActiveSheetIndex(0)
                        ->setCellValue("A".$cont, $value->getSalid())
                        ->setCellValue("B".$cont, $value->getNameClient()." ".$value->getLastNameClient())
                        ->setCellValue("C".$cont, "$".number_format($t,'2','.',','))
                        ->setCellValue("D".$cont, "$".number_format($value->getDiscount()?$value->getDiscount():"0",'2','.',','))
                        ->setCellValue("E".$cont, $value->getName()." ".$value->getLastName())
                        ->setCellValue("F".$cont, date("m-d-Y",strtotime($value->getDateCreation())))
                        ->setCellValue("G".$cont, $status[$value->getStatus()]);
                }
                $t=0;
            }
            if($total>0){
                $iva = 1.16;
                $iva_general = $total - ($total/$iva);
                $subtotal = ($total/$iva);
                $objectXls->setActiveSheetIndex(0)
                    ->setCellValue("I3", "$".number_format($subtotal,'2','.',','))
                    ->setCellValue("J3", "$".number_format($iva_general,'2','.',','))
                    ->setCellValue("K3", "$".number_format($iva_general+$subtotal,'2','.',','));
            }
        }else{
            $cont = 3;
            $objectXls->setActiveSheetIndex(0)
                ->setCellValue("A3".$cont, "Sin resultados")
                ->setCellValue("B3".$cont, "")
                ->setCellValue("C3".$cont, "")
                ->setCellValue("D3".$cont, "")
                ->setCellValue("E3".$cont, "")
                ->setCellValue("F3"."")
                ->setCellValue("G3"."")
                ->setCellValue("H3"."")
                ->setCellValue("I3"."")
                ->setCellValue("J3"."")
                ->setCellValue("K3"."");
        }

        $this->autoWidth($objectXls,$cont);
        $objectXls->getActiveSheet()->setTitle("Ventas Beauty Room");
        $objectXls->setActiveSheetIndex(0);
        $objWriter = \PHPExcel_IOFactory::createWriter($objectXls,'Excel2007');
        $xls = date('m-d-Y_H.i.s')."_$type_status.xlsx";
        $objWriter->save(dirname(dirname(dirname(dirname(__DIR__))))."/public/reports/".$xls);
        $this->download($xls);
    }
    private function autoWidth($objectXls,$cont){
        $this->style($objectXls,"A2:K2");
        $this->logo($objectXls,"A1:K1");
        $this->styleContent($objectXls,"A3:K$cont");

        $objectXls->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $objectXls->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $objectXls->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $objectXls->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
        $objectXls->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
        $objectXls->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
        $objectXls->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
        $objectXls->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
        $objectXls->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
        $objectXls->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);

    }
/* End report sales*/

/* create PDF and send Mail */
    private function createPDF($param){
        $session = $this->getSession();
        $auth = $this->auth();
        $url = $this->url->get("sales/print/$param");
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,"$url");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "key={$session['token']}&uid={$auth['uid']}");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_BINARYTRANSFER, true);
        $server_output = curl_exec ($ch);
        curl_close ($ch);
        $pdf_options = array(
            "source_type" => 'html',
            "source" =>$server_output,
            "action" => 'save',
            "save_directory" => 'sales/',
            "file_name" => "venta_$param.pdf");
        phptopdf($pdf_options);
    }
    private function sendMailClient($param,$mail,$values){
        @$this->createPDF($param);
        $email = new \PHPMailer();
        $email->isSMTP();
        $email->Host = "smtp.gmail.com";
        $email->Port=587;
        $email->SMTPSecure="tls";
        $email->SMTPAuth =true;
        $email->Username = "marketing.odontologia.integral@gmail.com";
        $email->Password = "PdRgLr20F#$";

        $email->setFrom("$mail","PONS");
        $email->addReplyTo("$mail","PONS");
        $email->addAddress("$mail");
        $email->addAttachment("sales/venta_$param.pdf");

        $email->WordWrap =100;
        $email->isHTML(true);

        $email->Subject = "Gracias por utilizar nuestros servicios";
        $html = file_get_contents($this->url->get("index/email"));
        $email->msgHTML($html);
        $email->AltBody = "PONS";

        if(!$email->send()) {
            echo 'Message could not be sent.';
            echo 'Mailer Error: ' . $email->ErrorInfo;
        } else {
            return true;
        }
        return true;
    }
/* create PDF and send Mail */

/* Other functions  */
    private function scripts(){
        $this->assets->collection('jsPlugins')
            ->setTargetPath("dash/js/general.sales.min.js")
            ->setTargetUri("dash/js/general.sales.min.js")
            ->addJs("dash/js/plugins/BootstrapDP/bootstrap-datepicker.min.js")
            ->addJs("dash/js/plugins/BootstrapDP/bootstrap-datepicker.es.min.js")
            ->addJs("dash/js/sales/sales.js")
            ->join(true)
            ->addFilter(new \Phalcon\Assets\Filters\Jsmin());

        $this->assets->collection('cssPlugins')
            ->setTargetPath("dash/css/general.sales.min.css")
            ->setTargetUri("dash/css/general.sales.min.css")
            ->addCss("dash/css/BDP/bootstrap-datepicker3.css")
            ->join(true)
            ->addFilter(new \Phalcon\Assets\Filters\Cssmin());
    }
}