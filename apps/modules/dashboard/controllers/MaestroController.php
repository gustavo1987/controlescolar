<?php
namespace Modules\Dashboard\Controllers;
require dirname(dirname(dirname(dirname(__FILE__))))."/library/PHPMailer/PHPMailerAutoload.php";

use Modules\Models\CdClient;
use Modules\Models\CdUser;
use Modules\Models\CdSubservicesSale;
use Modules\Models\CdSale;
use Modules\Models\CdClase;
use Modules\Models\CdSaleClient;
use Modules\Models\CdConferencia;
use Modules\Models\CdProduct;
use Modules\Models\VShoppingList;
use Modules\Models\cms\CdStudent;
use Modules\Models\VProductBrand;
use Modules\Models\VStudentClase;
use Modules\Models\CdWorking;
use Modules\Models\VWorkingUserClase;
use Phalcon\Http\Request;

class MaestroController extends ControllerBase{
    public function indexAction(){
        $auth = $this->auth();
        $this->backView("Campus Virtual","/dashboard");
        if($auth){
            $this->dataTable();
            $clients = null;
            if($auth['rol']!=="ADMIN")
                $teacher = CdUser::find("status='ACTIVE' and rol='TEACHER'");
                //$clients = CdClient::find("status='ACTIVO' and uid={$auth['uid']}");
            else if($auth['rol']=="ADMIN"){
                $teacher = CdUser::find("status='ACTIVE' and rol='TEACHER'");
            }

            $this->view->setVar("teacher",$teacher);
        }else{
            $this->response(array("message"=>"error"),404);
        }
    }
    public function viewclassAction(){
        $uid = $this->dispatcher->getParam("id","int");
        $this->backView("Catedraticos","/dashboard/maestro/index");
        $classall = CdClase::find("uid=".$uid);
        $this->view->setVar("classall",$classall);
    }
    public function savenoteAction(){
        $request = $this->request;
        $auth = $this->auth();
        $client = CdClient::findFirst("uid=".$auth['uid']);
        $clase = CdClase::findFirst("status='ACTIVO' and uid=".$auth['uid']);
        $teacher = CdUser::findFirst($clase->getUid());
        $clases = CdClase::find("status='ACTIVO' and uid=".$auth['uid']);
        if(count($clases)>1){
            $query="";
            foreach ($clases as $key => $value) {
                if($key+1==count($clases)){ 
                    $query=$query."carrera=".$value->getCarrera(); 
                }
                else{
                    $query=$query."carrera=".$value->getCarrera()." or ";
                }
            }
            $queryfull = $query." and generation=".$clase->getGeneration();
            //$studentsclase = CdClient::find("status='ACTIVO' and ".$queryfull);
            $studentsclase = CdClient::find("".$queryfull);
            /*print_r(count($studentsclase));  
            print_r("----------------".$queryfull);
            foreach ($studentsclase as $key => $value) {
                print_r($value->getName()."-");  
            }
            exit; */
        }
        else{
            $studentsclase = CdClient::find("carrera=".$clase->getCarrera()." and generation=".$clase->getGeneration());
        }
        $catedraworksave = CdConferencia::findFirst($request->getPost("confid"));
        foreach ($studentsclase as $key => $value) {
            $emails[]=$value->getEmail();
        }
        if($request->isPost() && $request->isAjax() && $auth){
            $work = new CdWorking();
            $fechaconferencia = $catedraworksave->getDateBegin();
            $dateSpanish = $this->dateSpanish();
            $mes = $dateSpanish[date('m',strtotime($fechaconferencia))];
            $workConfid = CdWorking::find("confid=".$request->getPost("confid"));
            $count = count($workConfid)+1;
            $dateday = date('d',strtotime($fechaconferencia));
            $namework = "Lectura ".$count." - Cátedra ".$dateday." de ".$mes;
            if($request->getPost("selecttype")=="liga"){
                $work->setStatus("ACTIVO")
                        ->setName($namework)
                        ->setConfid($request->getPost("confid"))
                        ->setDateCreation(date("Y-m-d H:m:s"))
                        ->setUid($auth['uid'])
                        ->setClasid($clase->getClasid())
                        ->setFileTeacher($request->getPost("video"))
                        ->setType("VIDEO");
                if($work->save()){
                    $this->sendEmailStudentWork(array_unique($emails));
                    $this->response(array("message"=>"SUCCESS","clase"=>$clase->getClasid(),"code"=>"200"),200);
                }
                else{
                    $messages = $work->getMessages();
                       foreach ($messages as $message) {
                           echo $message, "\n";
                       }
                    $this->response(array("message"=>"no guardo trabajo","code"=>"404"),200);
                }
            }
            else{
                $work->setStatus("ACTIVO")
                    ->setName($namework)
                    ->setConfid($request->getPost("confid"))
                    ->setDateCreation(date("Y-m-d H:m:s"))
                    ->setUid($auth['uid'])
                    ->setClasid($clase->getClasid())
                    ->setFileTeacher($request->getPost("image"))
                    ->setType("NOTE");
                if($work->save()){
                    $this->sendEmailStudentWork(array_unique($emails));
                    $this->response(array("message"=>"SUCCESS","clase"=>$clase->getClasid(),"code"=>"200"),200);
                }
                else{
                    $messages = $work->getMessages();
                       foreach ($messages as $message) {
                           echo $message, "\n";
                       }
                    $this->response(array("message"=>"no guardo archivo","code"=>"404"),200);
                }
            }
        }
        else{
            $this->response(array("message"=>"no tiene autorización","code"=>"404"),200);
        }
    }
    public function asistenciaAction(){
        $auth = $this->auth();
        if($auth){
            $title = "Lista de asistencias";
            $url ="/";
            $description = "IDE Iberoamerica";
            $image = "dash/img/logo-100.png";
            $this->metaHome($title,$url,$description,$image);
            $this->dataTable();
            $clase = CdClase::findFirst("status='ACTIVO' and uid={$auth['uid']}");
            $conferencias = CdConferencia::find("clasid=".$clase->getClasid());
            $users = CdUser::find("rol='COORDINATOR' and status='ACTIVE'");
            $student = CdClient::find("status='ACTIVO' and carrera=".$clase->getCarrera());
            $statusstudent=0;
            if($student)$statusstudent=1;
            $this->view->setVar("clients",$users);
            $this->view->setVar("student",$student);
            $this->view->setVar("conferencias",$conferencias);
            $this->view->setVar("statusstudent",$statusstudent);
        }else{
            $this->response(array("message"=>"error"),404);
        }
    }
    public function materiasAction(){
        
        $auth = $this->auth();
        if($auth){
            $this->dataTable();
            $this->scripts();
            $clases = VShoppingList::find("uid_ss=".$auth['uid']." and status='ACTIVO' and statussalid='PAY'");
            $this->view->setVar("clases",$clases);
            $this->view->setVar("auth",$auth);
        }else{
            $this->response(array("code"=>404,"message"=>"You do not have permission"),404);
        }
    }

    public function uploadfileAction(){
        $request = $this->request;
        $auth = $this->auth();
        if($request->isPost() && $request->isAjax() && $auth){
            if($request->hasFiles()==true){
                foreach($request->getUploadedFiles() as $file){
                    $type = explode("/",$file->getType());
                    $image_replace = $this->url_clean($file->getName());
                    $new_image = uniqid()."_".$image_replace;
                    if($file->moveTo(dirname(dirname(dirname(dirname(__DIR__))))."/public/documents/work/".$new_image))
                    {
                        $this->response(array("name"=>$new_image,"message"=>"SUCCESS","code"=>"200"),200);   
                    }
                    else{
                        $this->response(array("name"=>$new_image,"message"=>"no se pudo mover el archivo","code"=>"404"),200);
                    }
                }
            }
        }else{
            $this->response(array("name"=>$new_image,"message"=>"no se envío nada","code"=>"404"),200);
        }
    }

    public function uploadfilecalificacionAction(){
        $request = $this->request;
        $auth = $this->auth();
        $idid = $request->getPost("workid");
        $workin = CdWorking::findFirst("workid=".$idid);
        $users = CdUser::findFirst($workid->getUid());
        $client = CdClient::findFirst("uid=".$users->getUid());
        if($request->isPost() && $request->isAjax() && $auth){
            if($request->hasFiles()==true){
                foreach($request->getUploadedFiles() as $file){
                    $type = explode("/",$file->getType());
                    $image_replace = $this->url_clean($file->getName());
                    $new_image = uniqid()."_".$image_replace;
                    if($file->moveTo(dirname(dirname(dirname(dirname(__DIR__))))."/public/documents/work/".$new_image))
                    {
                        $workin->setFileStudent($new_image)
                            ->setCalificacion($request->getPost("calificacion"))
                            ->update();
                        $this->sendEmail($client->getEmail());
                        $this->response(array("name"=>$new_image,"message"=>"SUCCESS","code"=>"200"),200);
                        
                    }
                    else{
                        $this->response(array("name"=>$new_image,"message"=>"no se pudo mover el archivo","code"=>"404"),200);
                    }
                }
            }
        }else{
            $this->response(array("name"=>$new_image,"message"=>"no se envío nada","code"=>"404"),200);
        }
    }

    private function sendEmailStudentWork($emails){
        $email = new \PHPMailer();
        $email->isSMTP();
        $email->Host = "smtp.gmail.com";
        $email->Port=587;
        $email->CharSet = 'UTF-8';
        $email->SMTPSecure="tls";
        $email->SMTPAuth =true;
        $email->Username = "enviodecorreoscdevelopers@gmail.com";
        $email->Password = "cdevelopers2018";
        $email->setFrom("contactanos@ideiberoamerica.com",'IDEIberoamerica');
        $email->addReplyTo("contactanos@ideiberoamerica.com",'IDEIberoamerica');
        //$email->addAddress($values);
        $email->WordWrap =100;
        $email->isHTML(true);
        $email->Subject = "Nota academica cargada";

        $file = dirname(__DIR__)."/views/email/notaacademica.html";
        $email->msgHTML(file_get_contents($file));
        $email->AltBody = "Nota academica cargada IDEIBEROAMERICA";
        
        // if(!$email->send()) {
        //     echo 'Message could not be sent.';
        //     echo 'Mailer Error: ' . $email->ErrorInfo;
        // } else {
        //     return true;
        // }

        foreach($emails as $values){
            $email->addAddress("$values");
            $email->send();
            $email->ClearAddresses();
        }
        return true;
    }

    private function sendEmail($values){
        $email = new \PHPMailer();
        $email->isSMTP();
        $email->Host = "smtp.gmail.com";
        $email->Port=587;
        $email->CharSet = 'UTF-8';
        $email->SMTPSecure="tls";
        $email->SMTPAuth =true;
        $email->Username = "enviodecorreoscdevelopers@gmail.com";
        $email->Password = "cdevelopers2018";
        $email->setFrom("contactanos@ideiberoamerica.com",'IDE Iberoamerica');
        $email->addReplyTo("contactanos@ideiberoamerica.com",'IDE Iberoamerica');
        $email->addAddress($values);
        $email->WordWrap =100;
        $email->isHTML(true);
        $email->Subject = "Observaciones realizadas";
        $file = dirname(__DIR__)."/views/email/worksendteacher.html";
        $email->msgHTML(file_get_contents($file));
        $email->AltBody = "Instituto Iberoamericano de Derecho";
        if(!$email->send()) {
            echo 'Message could not be sent.';
            echo 'Mailer Error: ' . $email->ErrorInfo;
        } else {
            return true;
        }
    }
    public function aulaAction(){
        $auth = $this->auth();
        if($auth){
            $this->dataTable();
            $student = CdUser::findFirst("email='".$auth["email"]."' and rol='TEACHER'");
            $find = VShoppingList::find("uid_ss=".$student->getUid());
            $this->view->setVar("products1",$find);
        }else{
            $this->response(array("message"=>"error"),404);
        }
    }

    public function newworkAction(){

    }
    public function materiaAction(){
        
        $auth = $this->auth();

        $request = new Request();

            $id = $this->dispatcher->getParam("id","int");

        if($auth){
            $this->dataTable();
            $this->scripts();
            $find = CdProduct::findFirst("pid=".$id);
            $this->view->setVar("products",$find);
            $this->view->setVar("auth",$auth);
        }else{
            $this->response(array("code"=>404,"message"=>"You do not have permission"),404);
        }
    }
    public function calificacionesAction(){
        $auth = $this->auth();
        if($auth){
            $this->dataTable();
            $this->scripts();
            $student = CdClient::findFirst("email='".$auth["email"]."'");

            $find = VProductBrand::find("status='ACTIVE' and brid=".$student->getCarrera()." and stock_min=1");
            $find2 = VProductBrand::find("status='ACTIVE' and brid=".$student->getCarrera()." and stock_min=2");
            $find3 = VProductBrand::find("status='ACTIVE' and brid=".$student->getCarrera()." and stock_min=3");
            $find4 = VProductBrand::find("status='ACTIVE' and brid=".$student->getCarrera()." and stock_min=4");
            $find5 = VProductBrand::find("status='ACTIVE' and brid=".$student->getCarrera()." and stock_min=5");
            $this->view->setVar("products1",$find);
            $this->view->setVar("products2",$find2);
            $this->view->setVar("products3",$find3);
            $this->view->setVar("products4",$find4);
            $this->view->setVar("products5",$find5);
            $this->view->setVar("auth",$auth);
        }else{
            $this->response(array("code"=>404,"message"=>"You do not have permission"),404);
        }

    }

    public function claseAction(){
    }
    
    public function estudiantesAction(){
        $auth = $this->auth();
        $id = $this->dispatcher->getParam("id","int");
        if($auth){
            $this->dataTable();
            /*$clases = CdSubservicesSale::findFirst("subsalid=".$id." and status='ACTIVO'");
            
            $materia = CdProduct::findFirst($clases->getPid());*/
            /*$students = CdClient::find("clid>=23");*/
            //$students = CdSaleClient::find("salid=".$id." and status='ACTIVO'");
            $students = VStudentClase::find("salid=".$id);
            //$this->view->setVar("materia",$materia);
            $this->view->setVar("students",$students);
        }else{
            $this->response(array("message"=>"error"),404);
        }
    }
    /*public function aulaAction(){
        $auth = $this->auth();
        if($auth){
            $this->dataTable();
            $student = CdClient::findFirst("email='".$auth["email"]."'");

            $find = VProductBrand::find("status='ACTIVE' and brid=".$student->getCarrera()." and stock_min=1");
            $this->view->setVar("products1",$find);
        }else{
            $this->response(array("message"=>"error"),404);
        }
    }*/
    public function calificarAction(){
        $auth = $this->auth();
        $id = $this->dispatcher->getParam("id","int");
        if($auth){
            $this->dataTable();
            $students = CdClient::findFirst("clid=".$id);
            $this->view->setVar("students",$students);
        }else{
            $this->response(array("message"=>"error"),404);
        }
    }
    private function scripts(){
        $this->assets->collection('jsPlugins')
            ->setTargetPath("dash/js/general.maestro.min.js")
            ->setTargetUri("dash/js/general.maestro.min.js")
            ->addJs("dash/js/plugins/dropzone/dropzone.min.js")
            ->addJs("dash/js/plugins/bootstrapV/formValidation.min.js")
            ->addJs("dash/js/plugins/bootstrapV/bootstrapV.min.js")
            ->addJs("dash/js/student/slick.js")
            ->addJs("dash/js/student/fullcalendar.js")
            ->addJs("dash/js/maestro/maestro.js")
            ->join(true)
            ->addFilter(new \Phalcon\Assets\Filters\Jsmin());
    }
}