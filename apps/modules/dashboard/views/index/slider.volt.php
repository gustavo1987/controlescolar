<!-- START BREADCRUMB -->
<ul class="breadcrumb">
    <li><a href="<?php echo $this->url->get('dashboard'); ?>">Inicio</a></li>
    <li class="active">Slider</li>
</ul>
<!-- END BREADCRUMB -->
<!-- PAGE TITLE -->
<div class="page-title">
    <div class="col-sm-6 text-left">
        <h2><a href="<?php echo $this->url->get('dashboard'); ?>"><span class="fa fa-arrow-circle-o-left"></span></a> Menú principal</h2>
    </div>
    <div class="col-sm-6 text-right">
        <a href="<?php echo $this->url->get('dashboard/index/new'); ?>" class="btn btn-success btn-lg">Nuevo slider</a>
    </div>
</div>
<!-- END PAGE TITLE -->
<!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">
    <div class="row">
        <div class="col-md-12">
            <!-- START DEFAULT DATATABLE -->
            <div class="panel panel-success">
                <div class="panel-heading">
                    <h3 class="panel-title">Administración Slider</h3>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-hover table-actions generalDT" data-order="0" data-filter="asc">
                            <thead>
                            <tr>
                                <th>Id</th>
                                <th>Titulo</th>
                                <th>Fecha</th>
                                <th>Estatus</th>
                                <th>Opciones</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($sliders as $values) { ?>
                            <tr id="<?php echo $values->getSlid(); ?>">
                                <td><?php echo $values->getSlid(); ?></td>
                                <td><?php echo $values->getTitle(); ?></td>
                                <td>
                                    <?php echo $values->getDate(); ?>
                                </td>
                                <td><span class="btn <?=$values->getStatus()=='ACTIVE'?'btn-success':'btn-warning'?>"><?=$values->getStatus()=="ACTIVE"?"ACTIVO":"INACTIVO"?></span></td>
                                <td>
                                    <a href="<?php echo $this->url->get('dashboard/index/edit/'); ?><?php echo $values->getSlid(); ?>" class="btn btn-default btn-rounded btn-sm"><span class="fa fa-pencil"></span></a>
                                </td>
                            </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- END DEFAULT DATATABLE -->
        </div>
    </div>
</div>
<!-- PAGE CONTENT WRAPPER -->
