<div class="page-content-wrap" style="background-color: #25396880;">
    <div class=" container sinpadding">
        <div class="row">
            <?php if($rol=="ADMIN"):?>
             <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12 cuadro" style="background: rgba(85, 86, 97, 0.4);padding-top: 5%;">
                <div class="container">
                <div class="col-md-12 mt-3">
                    <div class="row">
                        <div class="col-md-3"> 
                            <ul>
                                <li class="student"><a href="/dashboard/client/all" style="text-decoration: none;"><i class="fas fa-tasks" aria-hidden="true"></i><p class="position1" style="color: white;">Proceso de admision</p></a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-6"> 
                            <ul>
                                <li class="student"><a href="/dashboard/estudiantes" style="text-decoration: none;"><i class="fas fa-id-card" aria-hidden="true"></i><p class="position1" style="color: white;">Estudiantes</p></a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-3"> 
                            <ul>
                                <li class="student"><a href="/dashboard/clases/pos" style="text-decoration: none;"><i class="fas fa-chalkboard-teacher" aria-hidden="true"></i><p class="position1" style="color: white;">Clases</p></a>

                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 mt-3" style="padding-top: 3.5%;">
                    <div class="row">
                        <div class="col-md-3 mt-5"> 
                            <ul>
                                <li class="student"><a href="/dashboard/comunidad-general" style="text-decoration: none;"><i class="fas fa-comments" aria-hidden="true"></i><p class="position1" style="color: white;">Comunidad</p></a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-6"> 
                            <ul>
                                <li class="lista1 student"><a href="/dashboard/sales/day" style="background: #483a69;text-decoration: none;" class="alista1 aula"><i class="fas fa-user-graduate ilista1" aria-hidden="true" style="padding-top: .2em;"></i><p class="position1" style="color: white;float: none;top:2.5em;">Aulas Virtuales</p></a>
                                </li>
                            </ul>
                        </div>
                        <!--div class="col-md-3 mt-5"> 
                            <ul>
                                <li class="student"><a href="/dashboard/student/calificaciones" style="text-decoration: none;"><i class="fas fa-file-signature" aria-hidden="true"></i><p class="position1" style="color: white;">Progreso Académico</p></a>
                                </li>
                            </ul>
                        </div-->

                        <div class="col-md-3 mt-5"> 
                            <ul>
                                <li class="student"><a href="/dashboard/maestro/index" style="text-decoration: none;"><i class="fas fa-file-signature" aria-hidden="true"></i><p class="position1" style="color: white;">Catedraticos</p></a>
                                </li>
                            </ul>x
                        </div>
                    </div>
                </div>
                <div class="col-md-12 mt-3" style="padding-top: 4%; margin-bottom: 3em;">
                    <div class="row">
                        <div class="col-md-3"> 
                            <ul>
                                <li class="student"><a href="/dashboard/controlescolar/pos" style="text-decoration: none;"><i class="fas fa-hand-holding-usd" aria-hidden="true"></i><p class="position1" style="color: white;">Pagos</p></a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-6"> 
                            <ul>
                                <li class="student"><a href="<?php echo $user->getStatuspagos()==0?'https://ideiberoamericasp.ebookcentral.proquest.com/libCentral':'';?>" target="_blank" style="text-decoration: none;"><i class="fas fa-book-reader" aria-hidden="true"></i><p class="position1" style="color: white;">Biblioteca</p></a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-3"> 
                            <ul>
                                <li class="student"><a href="/dashboard/servicios-escolares" style="text-decoration: none;"><i class="fas fa-id-card" aria-hidden="true"></i><p class="position1" style="color: white;">Servicios Escolares</p></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php elseif($rol=="STUDENT"):?>
            <!--p><?php echo $notification;?></p-->
        <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12 cuadro" style="background: rgba(85, 86, 97, 0.4);padding-top: 5%;">
            <div class="container">
            <div class="col-md-12 mt-3">
                <div class="row">
                    <div class="col-md-3"> 
                        <ul>
                            <li class="student"><a href="/dashboard/student/aula" style="text-decoration: none;"><i class="fas fa-tasks" aria-hidden="true"></i><p class="position1" style="color: white;">Plan de Estudio</p></a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-6"> 
                        <ul>
                            <li class="student"><a href="/dashboard/student/pagos" style="text-decoration: none;"><i class="fas fa-hand-holding-usd" aria-hidden="true"></i><p class="position1" style="color: white;">Pagos</p></a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-3"> 
                        <ul>
                            <li class="student"><a href="/dashboard/sitios-de-interes" style="text-decoration: none;"><i class="fas fa-link" aria-hidden="true"></i><p class="position1" style="color: white;">Sitios de Interes</p></a>
                                
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-12 mt-3" style="padding-top: 4%;">
                <div class="row">
                    <div class="col-md-3 mt-5"> 
                        <ul>
                            <li class="student"><a href="/dashboard/calendario" style="text-decoration: none;"><i class="fa fa-calendar" aria-hidden="true"></i><p class="position1" style="color: white;">Calendario</p></a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-6">
                        <ul>
                            <?php if($user->getStatuspagos()==0):?>
                            <li class="lista1 student"><a href="/dashboard/student/clase" style="background: #4d2168; text-decoration: none;" class="alista1 aula"><i class="fas fa-user-graduate ilista1" aria-hidden="true" style="padding-top: .2em;"></i><p class="position1" style="color: white;float: none;top:1.5em;">Aula Virtual</p></a>
                            </li>
                            <?php else:?>
                            <li class="lista1 student"><a style="background: #4d2168; text-decoration: none;" class="alista1 aula block-user"><i class="fas fa-user-graduate ilista1" aria-hidden="true" style="padding-top: .2em;"></i><p class="position1" style="color: white;float: none;top:1.5em;">Aula Virtual</p></a>
                            </li>
                            <?php endif;?>
                        </ul>
                    </div>
                    <div class="col-md-3 mt-5"> 
                        <ul>
                            <li class="student"><a href="/dashboard/student/calificaciones" style="text-decoration: none;"><i class="fas fa-file-signature" aria-hidden="true"></i><p class="position1" style="color: white;">Progreso Académico</p></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-12 mt-3" style="padding-top: 4%; margin-bottom: 3em;">
                <div class="row">
                    <div class="col-md-3"> 
                        <ul>
                            <?php if($user->getStatuspagos()==0):?>
                            <li class="student"><a href="https://ebookcentral.proquest.com/lib/ideiberoamericasp" target="_blank" style="text-decoration: none;"><i class="fas fa-book-reader" aria-hidden="true"></i><p class="position1" style="color: white;">Biblioteca Digital</p></a>
                            </li>
                            <?php else:?>
                            <li class="student"><a target="_blank" style="text-decoration: none;"><i class="fas fa-book-reader block-user" aria-hidden="true"></i><p class="position1" style="color: white;">Biblioteca Digital</p></a>
                            </li>
                            <?php endif;?>
                        </ul>
                    </div>
                    <div class="col-md-6"> 
                        <ul>
                            <li class="student"><a href="/dashboard/comunidad-general" style="text-decoration: none;"><i class="fas fa-comments" aria-hidden="true"></i><p class="position1" style="color: white;">Comunidad</p></a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-3"> 
                        <ul>
                            <?php if($user->getStatuspagos()==0):?>
                            <li class="student"><a target="_blank" href="<?= $this->url->get('https://hl322.hosteurope.es:2096/') ?>" style="text-decoration: none;"><i class="fas fa-envelope" aria-hidden="true"></i><p class="position1" style="color: white;">Correo Electronico</p></a>
                            </li>
                            <?php else:?>
                            <li class="student"><a target="_blank" style="text-decoration: none;"><i class="fas fa-envelope block-user" aria-hidden="true"></i><p class="position1" style="color: white;">Correo Electronico</p></a>
                            </li>
                            <?php endif;?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        </div>
        <?php elseif ($rol=="TEACHER"):?>
            <!--a id="loading_message"><p style="position: absolute;top: 25%;right: 5%; color: white; font-size: 30px;"><i class="fas fa-inbox"><?php echo " ".$notification." mensajes";?></i></p></a-->
        <div id="append_pasticipantes"></div>
        <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12 cuadro" style="background: rgba(85, 86, 97, 0.4);padding-top: 5%;">
            <div class="container">
                <div class="col-md-12 mt-3" style="padding-top: 4%;">
                    <div class="row">
                        <div class="col-md-6"> 
                            <ul>
                                <li class="student"><a href="/dashboard/calendario"><i class="fa fa-calendar" aria-hidden="true"></i><p class="position1" style="color: white;">Calendario</p></a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-6"> 
                            <ul>
                                <li class="student"><a href="/dashboard/comunidad-general"><i class="fas fa-comments" aria-hidden="true"></i><p class="position1" style="color: white;">Comunidad</p></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 mt-3" style="padding-top: 2%;">
                    <div class="row">
                        <div class="col-md-6 col-md-offset-3"> 
                            <ul>
                                <li class="lista1 student"><a href="/dashboard/student/clase" style="background: #483a69;text-decoration: none;" class="alista1 aula"><i class="fas fa-user-graduate ilista1" aria-hidden="true" style="padding-top: 55px;"></i><p class="position1" style="color: white;float: none;top:2.5em;">Aula Virtual</p></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 mt-3" style="padding-top: 2%;">
                    <div class="row">
                        <div class="col-md-6"> 
                            <ul>
                                <li class="student"><a href="https://ebookcentral.proquest.com/lib/ideiberoamericasp" target="_blank"><i class="fas fa-book-reader" aria-hidden="true"></i><p class="position1" style="color: white;">Biblioteca Digital</p></a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-6"> 
                            <ul>
                                <li class="student"><a target="_blank" href="<?= $this->url->get('https://hl322.hosteurope.es:2096/') ?>" style="text-decoration: none;"><i class="fas fa-envelope" aria-hidden="true"></i><p class="position1" style="color: white;">Correo Electronico</p></a>
                                </li>   
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php endif;?>
        <hr>
    </div>
</div>
<!--script id="hbs_participante" type="text/x-handlebars-template">
    <div class="alert alert-success">
        <strong>Mensaje nuevo! <?= $namenoti ?></strong>.
    </div>
</script-->