
<div class="page-content-wrap">
    <div class="container sinpadding">
    <div class="row">
        <div class="col-md-12 cuadro  fondo-gris-general">
            <h4 class="text-title">Colocar en Grado a Estudiante</h4>
            <div class="col-md-12 col-xs-12 panel-body form-group-separated">
                <form action="#" method="post" id="newGrado" name="newGrado" role="form" class="form-horizontal">


                    <div class="form-group">
                        <label class="col-md-2 col-xs-12 control-label c-white">Fecha de inicio<span>*</span></label>
                        <div class="col-md-8 col-xs-12">
                            <input type="text" class="form-control birth_date" name="birth_date" id="birth_date" placeholder="Fecha de inicio">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2 col-xs-12 control-label c-white">Posgrado <span>*</span></label>
                        <div class="col-md-8 col-sm-8 col-xs-12">
                            <input type="text" class="form-control" name="name" id="name" value="<?php ?>" placeholder="Nombres" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 col-xs-12 control-label c-white">Estatus <span>*</span></label>
                        <div class="col-md-8 col-sm-6 col-xs-12">
                            <select name="sex" id="sex" class="form-control select2">
                                <option value="">Selecciona el estatus del grado</option>
                                <option value="ACTIVO">ACTIVO</option>
                                <option value="INACTIVO">INACTIVO</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 col-xs-12 control-label c-white">Grado sugerido <span>*</span></label>
                        <div class="col-md-8 col-sm-8 col-xs-12">
                            <input type="text" class="form-control" name="name" id="name" value="<?php ?>" placeholder="Nombres" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-2">
                            <input type="submit" class="btn btn-success" value="Guardar"/>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    </div>
</div>

<!-- info -->
<div class="message-box message-box-info animated fadeIn" id="message-box-info">
    <div class="mb-container">
        <div class="mb-middle">
            <div class="mb-title"> Actualizando &nbsp; <i class="fa fa-circle-o-notch fa-spin style-fa"></i></div>
            <div class="mb-content">
                <p>Guardando y Actualizando su información, espere un momento por favor.</p>
            </div>
        </div>
    </div>
</div>
<!-- end info -->
<!-- success -->
<div class="message-box message-box-success animated fadeIn" id="message-box-success">
    <div class="mb-container">
        <div class="mb-middle">
            <div class="mb-title"><span class="fa fa-check"></span> Información actualizada</div>
            <div class="mb-content">
                <p>Sus cambios han sido guardados correctamente, actualizaremos el sitio para reflejar los cambios.</p>
            </div>
        </div>
    </div>
</div>
<!-- end success -->
<!-- warning -->
<div class="message-box message-box-warning animated fadeIn" id="message-box-warning">
    <div class="mb-container">
        <div class="mb-middle">
            <div class="mb-title"><span class="fa fa-warning"></span> Error</div>
            <div class="mb-content">
                <p>Ha ocurrido un error al guardar su información, inténtelo nuevamente o regrese mas tarde.</p>
            </div>
            <div class="mb-footer">
                <button class="btn btn-default btn-lg pull-right mb-control-close">Cerrar</button>
            </div>
        </div>
    </div>
</div>
<!-- end danger -->
<div class="message-box message-box-danger animated fadeIn" id="message-box-danger">
    <div class="mb-container">
        <div class="mb-middle">
            <div class="mb-title"><span class="fa fa-times"></span> Cuidado</div>
            <div class="mb-content">
                <p>Usted no puede actualizar su imagen.</p>
            </div>
            <div class="mb-footer">
                <button class="btn btn-default btn-lg pull-right mb-control-close">Cerrar</button>
            </div>
        </div>
    </div>
</div>
<div class="message-box animated fadeIn" data-sound="alert" id="other_products">
    <div class="mb-container">
        <div class="mb-middle">
            <div class="mb-title"><span class="fa fa-sign-out"></span> Alta <strong>Estudiante</strong></div>
            <div class="mb-content">
                <p>¿Le gustaría dar de alta otro estudiante?</p>
                <p>Pulse Sí para dar de alta un nuevo estudiante. Pulse No para ir a la lista de estudiantes.</p>
            </div>
            <div class="mb-footer">
                <div class="pull-right">
                    <button class="btn btn-success btn-lg mb-control-close">Si</button>
                    <a href="{{url('dashboard/clients')}}" class="btn btn-default btn-lg">No</a>
                </div>
            </div>
        </div>
    </div>
</div>