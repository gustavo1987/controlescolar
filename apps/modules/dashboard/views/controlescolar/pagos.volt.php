<div class="page-content-wrap">
    <div class=" container sinpadding">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12 fondo-gris-general">
            	<div class="panel panel-success">
		          <div class="panel-heading">
		            <h3 class="panel-title">Lista de alumnos</h3>
                        <a href="<?= $this->url->get('dashboard/recibos/index') ?>" class="btn btn-success btn-lg">Ver recibos</a>
                        <a href="<?= $this->url->get('dashboard/recibos/new') ?>" class="btn btn-success btn-lg">Crear nuevo recibo</a>
		          </div>
		          <div class="panel-body">
		            <div class="table-responsive">
		              <table id="tableProducts" class="table table-bordered table-striped table-actions generalDT" data-order="0" data-filter="desc">
		                <thead>
		                  <tr>
		                    <th>Postgrado</th>
		                    <th>Código</th>
		                    <th>Alumnos</th>
		                    <th>Opciones</th>
		                  </tr>
		                </thead>
		                <tbody>
		                  <?php foreach ($student as $key => $values):?>
		                  <tr class="<?= $values->getClid() ?>">
		                    <td style="line-height: 14px;padding: 5px 5px;"><?= $key + 1 ?></td>
		                    <td style="line-height: 14px;padding: 5px 5px;"><?= $values->getIdentifier() ?></td>
		                    <td style="line-height: 14px;padding: 5px 5px;" class="name"><?= $values->getName() ?> <?= $values->getLastName() ?> <?= $values->getSecondName() ?></td>
		                    <td style="line-height: 14px;padding: 5px 5px;">
		                      <a href="<?= $this->url->get('dashboard/student/viewpay/' . $values->getClid()) ?>"><span class="fa fa-eye fa-2x"></span></a>&nbsp;
		                    </td>
		                  </tr>
		                  <?php endforeach;?>		                </tbody>
		              </table>
		            </div>
		          </div>
		        </div>
        	</div>
    	</div>
	</div>
</div>