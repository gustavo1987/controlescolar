<div class="page-content-wrap" id="sales">
    <div class=" container sinpadding">
        <div class="row">
            <div class="col-md-12 fondo-gris-general">
                <div class="panel panel-success">
                    <div class="panel-heading">
                        <h3 class="panel-title">Clases</h3>
                        <a id="add-student" id-clasid="<?php echo $param;?>" class="btn btn-success btn-lg" style="margin-left: 75%;"><span class="fas fa-user-plus"></span> estudiante</a>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover table-actions generalDT" data-order="5" data-filter="desc">
                                <thead>
                                <tr>
                                    <th>Número&nbsp;Estudiante</th>
                                    <th>Nombre</th>
                                    <th>Apellido Paterno</th>
                                    <th>Apellido Materno</th>
                                    <th>Calificación</th>
                                    <th>Opción</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach($lista as $key => $values):?>
                                <tr data-id="<?php echo $values->getClid();?>" id="<?php echo  $values->getClid();?>" data-clasid="<?php echo $values->getClasid();?>">
                                    <td><?php echo $key+1;?></td>
                                    <td><?php echo $values->getName();?></td>
                                    <td><?php echo $values->getLastName();?></td>
                                    <td><?php echo $values->getSecondName();?></td>
                                    <td><?php echo $values->getCalificacion();?></td>
                                    <td>
                                    <a class="btn btn-success score" style="font-size: 16px;padding: 2px 2px;"><span class="fas fa-clipboard-list"></span></a>
                                    <a class="btn btn-danger delete-student-class" style="font-size: 16px;padding: 2px 2px;"><span class="fas fa-user-minus"></span></a>
                                    </td>
                                </tr>
                                <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="message-box message-box-info animated fadeIn" id="message-box-info">
    <div class="mb-container">
        <div class="mb-middle">
            <div class="mb-title"> Actualizando &nbsp; <i class="fa fa-circle-o-notch fa-spin style-fa"></i></div>
            <div class="mb-content">
                <p>Guardando y Actualizando su clase, espere un momento por favor.</p>
            </div>
        </div>
    </div>
</div>
<div class="message-box message-box-success animated fadeIn" id="message-box-success">
    <div class="mb-container">
        <div class="mb-middle">
            <div class="mb-title"><span class="fa fa-check"></span> Clase actualizada</div>
            <div class="mb-content">
                <p>Sus cambios han sido guardados correctamente, actualizaremos el sitio para reflejar los cambios.</p>
            </div>
        </div>
    </div>
</div>
<div class="message-box message-box-warning animated fadeIn" id="message-box-warning">
    <div class="mb-container">
        <div class="mb-middle">
            <div class="mb-title"><span class="fa fa-warning"></span> Error</div>
            <div class="mb-content">
                <p>Ha ocurrido un error al guardar su información, inténtelo nuevamente o regrese mas tarde.</p>
            </div>
            <div class="mb-footer">
                <button class="btn btn-default btn-lg pull-right mb-control-close">Cerrar</button>
            </div>
        </div>
    </div>
</div>
<div class="message-box message-box-danger animated fadeIn" id="message-box-danger">
    <div class="mb-container">
        <div class="mb-middle">
            <div class="mb-title"><span class="fa fa-times"></span> Cuidado</div>
            <div class="mb-content">
                <p>Usted no puede actualizar su imagen.</p>
            </div>
            <div class="mb-footer">
                <button class="btn btn-default btn-lg pull-right mb-control-close">Cerrar</button>
            </div>
        </div>
    </div>
</div>
<div class="message-box animated fadeIn" data-sound="alert" id="other_products">
    <div class="mb-container">
        <div class="mb-middle">
            <div class="mb-title"><span class="fa fa-sign-out"></span> Alta <strong>Cliente</strong></div>
            <div class="mb-content">
                <p>¿Le gustaría dar de alta otro cliente?</p>
                <p>Pulse Sí para dar de alta un nuevo cliente. Pulse No para ir a la lista de clientes.</p>
            </div>
            <div class="mb-footer">
                <div class="pull-right">
                    <button class="btn btn-success btn-lg mb-control-close">Si</button>
                    <a href="<?= $this->url->get('dashboard/clients') ?>" class="btn btn-default btn-lg">No</a>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="message-box animated fadeIn" data-sound="alert" id="updatescorestudent">
    <div class="mb-container">
        <div class="mb-middle">
            <div class="mb-title"><span class="fa fa-sign-out"></span> Calificación <strong>Estudiante</strong></div>
            <form action="#" method="post" id="updatescorestudentform" name="updatescorestudentform" role="form">
            <div class="mb-content col-md-12">
                <p>Coloca la calificación parcial</p>
                <input type="text" class="form-control" name="scorestudent" id="scorestudent" required placeholder="calificación">
                <input type="number" name="clid-update" class="form-control hidden" id="clid-student-update-score">
                <input type="number" name="clasid-update" class="form-control hidden" id="clasid-student-update-score">                
            </div>
            <div class="mb-footer">
                <div class="pull-right">
                    <button type="submit" class="btn btn-success btn-lg">Calificar</button>
                    <a href="/dashboard/clases/list/<?php echo $param;?>" class="btn btn-default btn-lg">No</a>
                </div>
            </div>
            </form>
        </div>
    </div>
</div>


<!--MODALS SUB-SERVICES -->
<div class="modal" id="modalAddStudent" tabindex="-1" role="dialog" aria-labelledby="defModalHead" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Cerrar</span></button>
                <h4 class="modal-title" id="defModalHeadSS">Agregar Estudiante</h4>
            </div>
            <form id="formSubServices" action="#" method="post" class="form-horizontal">
                <input type="hidden"  id="servidSS" name="servidSS">
                <div class="modal-body">
                    <h2>Agregar Estudiante</h2>
                    <div class="form-group">
                        <div class="col-md-6">
                            <input type="text" class="form-control" placeholder="Nombre" name="nameSS" id="nameSS" data-id="" required/>
                        </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Cerrar</button>
                    <input type="submit" class="btn btn-info btn-submit-sub-services" value="Agregar">
                </div>
            </form>
        </div>
    </div>
</div>
<!--END MODALS-->