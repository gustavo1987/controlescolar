<div class="page-content-wrap" style="background-color: #25396880;">
    <div class="row">
        <div class="col-md-12">
            <h4 class="text-title">Nuevo módulo</h4>
            <div class="col-md-12 col-xs-12 panel-body form-group-separated">
                <form action="#" method="post" id="modulonew" name="modulonew" role="form" class="form-horizontal">
                    <input type="text" class="hidden" name="carrera" id="carrera" value="<?php echo $id;?>">
                    <div class="form-group">
                        <label class="col-md-2 col-xs-12 control-label">Nombre <span>*</span></label>
                        <div class="col-md-8 col-sm-6 col-xs-12">
                            <select name="brid" id="brid" class="form-control" required>
                                <option value="">Seleccionar módulo</option>
                                <?php foreach($clases as $key=>$val):?>
                                    <option value="<?=$val->getPid();?>"><?=$val->getName();?></option>
                                <?php endforeach?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 col-xs-12 control-label">Maestro <span>*</span></label>
                        <div class="col-md-8 col-sm-6 col-xs-12">
                            <select name="uid" id="uid" class="form-control" required>
                                <option value="">Selecciona el maestro</option>
                                <?php foreach($teacher as $key=>$val):?>
                                    <option value="<?=$val->getUid();?>"><?=$val->getName()." ".$val->getLastName()." ".$val->getSecondName();?></option>
                                <?php endforeach?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 col-xs-12 control-label">Estatus<span>*</span></label>
                        <div class="col-md-8 col-sm-6 col-xs-12">
                            <select name="status" id="status" class="form-control" required>
                                <option value="ACTIVO">ACTIVO</option>
                                <option value="INACTIVO">INACTIVO</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 col-xs-12 control-label">Fecha inicio</label>
                        <div class="col-md-8 col-xs-12">
                            <input type="text" class="form-control birth_date" name="birth_date" id="birth_date" placeholder="Fecha nacimiento">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 col-xs-12 control-label">Plan de estudio</label>
                        <div class="col-md-8 col-sm-8 col-xs-12">
                            <div id="plan" class="dropzone dz-default dz-message documentload" data-id="plan" style="min-height:80px;">
                                <div class="dz-default dz-message col-md-12" style="top: 0% !important;">
                                    <h2 style="line-height: 0.8;cursor: pointer;padding-top: 4%;">
                                        <i class="fas fa-download col-md-4" style="font-size: 45px;cursor: pointer;"></i>
                                        <div class="col-md-8">
                                            <p class="col-md-12" style="font-size: 20px;margin:0;cursor: pointer;padding:0;">Arrastra un archivo</p>
                                            <i style="font-size: 14px;cursor: pointer;padding:0;" class="col-md-12">o haz click para seleccionar el plan de estudio</i>
                                            <input type="hidden" value="" name="plan" id="input-plan-receipt" class="col-md-12">
                                        </div>
                                    </h2>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 col-xs-12 control-label">Chat <span>*</span></label>
                        <div class="col-md-8 col-sm-6 col-xs-12">
                            <textarea class="form-control" name="chat"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 col-xs-12 control-label">Calendario <span>*</span></label>
                        <div class="col-md-8 col-sm-8 col-xs-12">
                            <div id="calendario" class="dropzone dz-default dz-message documentload" data-id="calendario" style="min-height:80px;">
                                <div class="dz-default dz-message col-md-12" style="top: 0% !important;">
                                    <h2 style="line-height: 0.8;cursor: pointer;padding-top: 4%;">
                                        <i class="fas fa-download col-md-4" style="font-size: 45px;cursor: pointer;"></i>
                                        <div class="col-md-8">
                                            <p class="col-md-12" style="font-size: 20px;margin:0;cursor: pointer;padding:0;">Arrastra un archivo</p>
                                            <i style="font-size: 14px;cursor: pointer;padding:0;" class="col-md-12">o haz click para seleccionar el calendario</i>
                                            <input type="hidden" value="" name="calendario" id="input-calendario-receipt" class="col-md-12">
                                        </div>
                                    </h2>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-2">
                            <input type="submit" class="btn btn-success" value="Guardar"/>
                        </div>
                    </div>
                    <div class="clearfiz"></div>
                </form>
            </div>
        </div>
    </div>
</div>