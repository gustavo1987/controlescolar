<div class="page-content-wrap" style="background-color: #25396880;">
    <div class="row">
        <div class="col-md-12">
            <h4 class="text-title">Nueva Clase</h4>
            <div class="col-md-12 col-xs-12 panel-body form-group-separated">
                <form action="#" method="post" id="catedranew" name="catedranew" role="form" class="form-horizontal">
                    <div class="form-group">
                        <label class="col-md-2 col-xs-12 control-label">Nombre <span>*</span></label>
                        <div class="col-md-8 col-xs-12">
                            <input type="text" class="form-control" name="name" id="name" required placeholder="Nombre" value="<?php echo $conferencia->getName();?>">
                            <input class="hidden" name="clasid" id="clasid" value="<?php echo $clasid;?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 col-xs-12 control-label">URL <span>*</span></label>
                        <div class="col-md-8 col-xs-12">
                            <textarea type="text" class="form-control" name="url" id="url" required placeholder="URL" value="<?php echo $conferencia->getUrl();?>"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 col-xs-12 control-label">URL teacher <span>*</span></label>
                        <div class="col-md-8 col-xs-12">
                            <textarea type="text" class="form-control" name="urlteacher" id="urlteacher" required placeholder="URL maestro" value="<?php echo $conferencia->getUrlteacher();?>"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 col-xs-12 control-label">Fecha de inicio</label>
                        <div class="col-md-8 col-sm-6 col-xs-12">
                            <input type="text" class="form-control datebegin" name="datebegin" id="datebegin" placeholder="Fecha nacimiento">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 col-xs-12 control-label">Estatus<span>*</span></label>
                        <div class="col-md-8 col-sm-6 col-xs-12">
                            <select name="status" id="status" class="form-control" required>
                                <option value="ACTIVO">ACTIVO</option>
                                <option value="INACTIVO">INACTIVO</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 col-xs-12 control-label">Catedratico <span>*</span></label>
                        <div class="col-md-8 col-sm-6 col-xs-12">
                            <select name="brid" id="brid" class="form-control" required>
                                <option value="">Seleccionar Catedratico</option>
                                <?php foreach($catedrid as $key=>$val):?>
                                    <option value="<?=$val->getUid();?>"><?=$val->getName()." ".$val->getLastName()." ".$val->getSecondName();?></option>
                                <?php endforeach?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-2">
                            <input type="submit" class="btn btn-success" value="Guardar"/>
                        </div>
                    </div>
                    <div class="clearfiz"></div>
                </form>
            </div>
        </div>
    </div>
</div>