
<div class="page-content-wrap">
    <div class=" container sinpadding">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12 fondo-gris-general">
              <?php if($conferencia):?>
              <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12 div-info-confe-next">
                  <p class="nombre-clase-vista"><?php echo $conferencia->getName();?></p>
                  <a class="btn" style="    padding: 10px 15px 4px 15px;" href="<?php echo $conferencia->getUrl();?>" target="_blank" data-id="<?php echo $conferencia->getConfid();?>" id="ingresar-asistencia"><i class="fab fa-youtube icono-clase-play"></i></a>
                  <p class="carrera-clase-titulo"><?php echo $claseultima->getName();?></p>
              </div>
              <?php else:?>
                <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12 div-info-confe-next">
                  <p class="nombre-clase-vista">"No hay clases por el momento"</p>
                  <p class="carrera-clase-titulo"></p>
              </div>
            <?php endif;?>
              <div class="col-md-12 col-xs-12 div-last-confe">
              <h2 class="title-last-confe">Cátedras Anteriores</h2>
              <table class="table table-striped custab">
                  <?php if($statusconfe==1):?>
                      <thead>
                          <tr>
                              <th class="text-center">Cátedra</th>
                              <th class="text-center">Fecha</th>
                              <th class="text-center">Transmisión</th>
                          </tr>
                      </thead>
                      <?php foreach ($conferencianteriores as $key => $confe):?>
                        <?php if($confe->getUrl()):?>
                          <tr>
                              <td class="text-center"><?php  echo $confe->getName();?></td>
                              <td class="text-center"><?php  echo $confe->getDateBegin();?></td>
                              <td class="text-center"><a class='btn btn-info btn-xs direfido' href="<?php echo $confe->getUrl();?>" target="_blank" data-id="<?php echo $confe->getConfid();?>"><span class="glyphicon glyphicon-facetime-video"></span></a></td>
                          </tr>
                        <?php endif;?>
                      <?php endforeach;?>
                  <?php else:?>
                      <h3>No existe clases anteriores</h3>
                  <?php endif;?>
              </table>
          </div>
          <br><br>
          <div class="row">
              <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12" style="margin-top: 4%;">
                  <?php if($claseultima):?>
                      <div class="col-lg-4 col-sm-4 col-md-4 col-xs-12 pull-left">
                          <div class="squareBanner banner-aqua banner-desktop" style="font-size: 14px;">
                              <p style="padding-top: 15%;"><a onclick="ga('send', 'event', 'BannersPrincipales', 'click', 'Defensoría Pública Electoral para Pueblos y Comunidades Indígenas ')" href="/dashboard/clases/plan/<?php echo $claseultima->getClasid();?>" target="blank" class="colorWhite textoAccesible" style="font-size: 20px; transition: all 0.2s linear 0s;color: white;">Programa de Estudios </a></p>
                          </div>
                      </div>
                      <?php else:?>
                      <?php endif;?>
                      <div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
                          <div class="squareBanner banner-aqua banner-desktop" style="font-size: 14px;">
                                <p style="padding-top: 15%;"><a onclick="ga('send', 'event', 'BannersPrincipales', 'click', 'Defensoría Pública Electoral para Pueblos y Comunidades Indígenas ')" href="/dashboard/estudiante/calendario" target="blank" class="colorWhite textoAccesible" style="font-size: 20px; transition: all 0.2s linear 0s;color: white;">Calendario </a></p>
                          </div>
                      </div>
                      <?php if($claseultima):?>
                      <div class="col-lg-4 col-sm-4 col-md-4 col-xs-12 pull-right">
                        <div class="squareBanner banner-aqua banner-desktop" style="font-size: 14px;">
                            <p style="padding-top: 15%;"><a onclick="ga('send', 'event', 'BannersPrincipales', 'click', 'Defensoría Pública Electoral para Pueblos y Comunidades Indígenas ')" href="/dashboard/student/asistencias/<?php echo $claseultima->getClasid();?>" target="blank" class="colorWhite textoAccesible" style="font-size: 20px; transition: all 0.2s linear 0s;color: white;">Control de Asistencias </a></p>
                        </div>
                      </div>
                      <?php else:?>

                      <div class="col-lg-4 col-sm-4 col-md-4 col-xs-12 pull-right">
                        <div class="squareBanner banner-aqua banner-desktop" style="font-size: 14px;">
                            <p style="padding-top: 15%;"><a onclick="ga('send', 'event', 'BannersPrincipales', 'click', 'Defensoría Pública Electoral para Pueblos y Comunidades Indígenas ')" href="/dashboard/student/asistencias" target="blank" class="colorWhite textoAccesible" style="font-size: 20px; transition: all 0.2s linear 0s;color: white;">Control de Asistencias </a></p>
                        </div>
                      </div>
                  <?php endif;?>
              </div>
              <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12" style="margin-top: 4%;">
                <?php if($claseultima):?>
                    <div class="col-lg-4 col-sm-4 col-md-4 col-xs-12 pull-left">
                      <div class="squareBanner banner-aqua banner-desktop" style="font-size: 14px;">
                          <p style="padding-top: 15%;"><a onclick="ga('send', 'event', 'BannersPrincipales', 'click', 'Defensoría Pública Electoral para Pueblos y Comunidades Indígenas ')" href="/dashboard/student/trabajos/<?php echo $claseultima->getClasid();?>" target="blank" class="colorWhite textoAccesible" style="font-size: 20px; transition: all 0.2s linear 0s;color: white;">Trabajos </a></p>
                      </div>
                    </div>
                <?php else:?>
                    <div class="col-lg-4 col-sm-4 col-md-4 col-xs-12 pull-left">
                      <div class="squareBanner banner-aqua banner-desktop" style="font-size: 14px;">
                          <p style="padding-top: 15%;"><a onclick="ga('send', 'event', 'BannersPrincipales', 'click', 'Defensoría Pública Electoral para Pueblos y Comunidades Indígenas ')" href="/dashboard/student/trabajos" target="blank" class="colorWhite textoAccesible" style="font-size: 20px; transition: all 0.2s linear 0s;color: white;">Trabajos </a></p>
                      </div>
                    </div>
                <?php endif;?>
                <div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
                  <div class="squareBanner banner-aqua banner-desktop" style="font-size: 14px;">
                      <p style="padding-top: 15%;"><a onclick="ga('send', 'event', 'BannersPrincipales', 'click', 'Defensoría Pública Electoral para Pueblos y Comunidades Indígenas ')" href="/dashboard/student/chat" target="blank" class="colorWhite textoAccesible" style="font-size: 20px; transition: all 0.2s linear 0s;color: white;">Chat </a></p>
                  </div>
                </div>
                <?php if($claseultima):?>
                    <div class="col-lg-4 col-sm-4 col-md-4 col-xs-12 pull-right">
                      <div class="squareBanner banner-aqua banner-desktop" style="font-size: 14px;">
                          <p style="padding-top: 15%;"><a onclick="ga('send', 'event', 'BannersPrincipales', 'click', 'Defensoría Pública Electoral para Pueblos y Comunidades Indígenas ')" href="/dashboard/student/notas/<?php echo $claseultima->getClasid();?>" target="blank" class="colorWhite textoAccesible" style="font-size: 20px; transition: all 0.2s linear 0s;color: white;">Lecturas </a></p>
                      </div>
                    </div>
                <?php else:?>
                    <div class="col-lg-4 col-sm-4 col-md-4 col-xs-12 pull-right">
                      <div class="squareBanner banner-aqua banner-desktop" style="font-size: 14px;">
                          <p style="padding-top: 15%;"><a onclick="ga('send', 'event', 'BannersPrincipales', 'click', 'Defensoría Pública Electoral para Pueblos y Comunidades Indígenas ')" href="/dashboard/student/notas" target="blank" class="colorWhite textoAccesible" style="font-size: 20px; transition: all 0.2s linear 0s;color: white;">Lecturas </a></p>
                      </div>
                    </div>
                <?php endif;?>
                  <?php if($auth["rol"]=="TEACHER"):?>
                    <div class="col-lg-4 col-lg-offset-4 col-sm-4 col-sm-offset-4 col-md-4 col-md-offset-4 col-xs-12 pull-right">
                      <div class="squareBanner banner-aqua banner-desktop" style="font-size: 14px;">
                          <p style="padding-top: 15%;"><a onclick="ga('send', 'event', 'BannersPrincipales', 'click', 'Defensoría Pública Electoral para Pueblos y Comunidades Indígenas ')" href="/dashboard/student/allstudentclass" target="blank" class="colorWhite textoAccesible" style="font-size: 20px; transition: all 0.2s linear 0s;color: white;">Estudiantes </a></p>
                      </div>
                    </div>
                  <?php endif;?>
              </div>
          </div>
        </div>
      </div>
    </div>
</div>