<div class="page-content-wrap" style="background-color: #25396880;">
    <div class="row">
        <div class="col-md-12">
            <h4 class="text-title">Nueva Clase</h4>
            <div class="col-md-12 col-xs-12 panel-body form-group-separated">
                <form action="#" method="post" id="newProduct" name="newProduct" role="form" class="form-horizontal">
                    <div class="form-group">
                        <label class="col-md-2 col-xs-12 control-label">Nombre <span>*</span></label>
                        <div class="col-md-5 col-xs-12">
                            <input type="text" class="form-control" name="name" id="name" required placeholder="Nombre">
                        </div>
                        <div class="col-md-5 col-xs-12">
                            <input type="text" class="form-control" name="permalink" id="permalink" required placeholder="Permalink" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 col-xs-12 control-label">Descripción <span>*</span></label>
                        <div class="col-md-8 col-xs-12">
                            <textarea type="text" class="form-control" name="description" id="description" required placeholder="Descripción"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 col-xs-12 control-label">Código</label>
                        <div class="col-md-8 col-sm-6 col-xs-12">
                            <input type="text" class="form-control" name="ticket" id="ticket" required placeholder="Código">
                        </div>
                    </div>
                    <div class="form-group hidden">
                        <label class="col-md-2 col-xs-12 control-label">Precio de compra</label>
                        <div class="col-md-8 col-sm-6 col-xs-12">
                            <input type="number" min="0" max="100000" class="form-control" name="purchase_price" id="purchase_price" required value="0" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 col-xs-12 control-label">Credito <span>*</span></label>
                        <div class="col-md-8 col-sm-6 col-xs-12">
                            <input type="number" min="0" max="100000" class="form-control" name="sale_price" id="sale_price" required placeholder="0" >
                        </div>
                    </div>
                    <div class="form-group hidden">
                        <label class="col-md-2 col-xs-12 control-label">Versión <span>*</span></label>
                        <div class="col-md-8 col-sm-6 col-xs-12">
                            <input type="text" name="measure" id="measure" class="form-control" required placeholder="PZ" value="PZ">
                        </div>
                    </div>
                    <div class="form-group hidden">
                        <label class="col-md-2 col-xs-12 control-label">Cantidad <span>*</span> <i class="fa fa-info-circle"  data-toggle="tooltip" data-placement="top" title="" data-original-title="Cantidad del producto para agregar al inventario."></i></label>
                        <div class="col-md-8 col-sm-6 col-xs-12">
                            <input type="number" min="0" max="1000000" class="form-control" name="quantity" id="quantity" placeholder="0" value="0" required>
                        </div>
                    </div>
                    <div class="form-group hidden">
                        <label class="col-md-2 col-xs-12 control-label">Porcentaje <span>*</span> <i class="fa fa-info"  data-toggle="tooltip" data-placement="top" title="" data-original-title="Porcentaje de ganancia que se otorgará al vender este producto."></i></label>
                        <div class="col-md-8 col-sm-6 col-xs-12">
                            <input type="number" min="0" max="10000" class="form-control" name="percentage" id="percentage" placeholder="0" value="0" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 col-xs-12 control-label">Postgrado <span>*</span></label>
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <select name="brid" id="brid" class="form-control" required>
                                <option value="">Seleccionar Postgrado</option>
                                <?php foreach($brid as $key=>$val):?>
                                    <option value="<?=$val->getBrid();?>"><?=$val->getName();?></option>
                                <?php endforeach?>
                            </select>
                        </div>
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <span class="btn btn-success btn-condensed" id="newElement"><i class="fa fa-plus"></i>&nbsp;Postgrado</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 col-xs-12 control-label">Cuatrimestre o Trimestre <span></span></label>
                        <div class="col-md-8 col-sm-6 col-xs-12">
                            <input type="number" min="0" max="100000" class="form-control" name="stockmi" id="stockmi" required value="0" placeholder="0" >
                        </div>
                    </div>
                    <!--div class="form-group">
                        <label class="col-md-2 col-xs-12 control-label">Alumnos Máximo <span></span></label>
                        <div class="col-md-8 col-sm-6 col-xs-12">
                            <input type="number" min="0" max="100000" class="form-control" name="stockma" id="stockma" required value="0" placeholder="0" >
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 col-xs-12 control-label">Stock<span></span></label>
                        <div class="col-md-8 col-sm-6 col-xs-12">
                            <input type="number" min="0" max="100000" class="form-control" name="stock" id="stock" required value="0" placeholder="0" >
                        </div>
                    </div-->
                    <div class="form-group">
                        <label class="col-md-2 col-xs-12 control-label">Estatus<span>*</span></label>
                        <div class="col-md-8 col-sm-6 col-xs-12">
                            <select name="status" id="status" class="form-control" required>
                                <option value="ACTIVE">ACTIVO</option>
                                <option value="INACTIVE">INACTIVO</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-2">
                            <input type="submit" class="btn btn-success" value="Guardar"/>
                        </div>
                    </div>
                    <div class="clearfiz"></div>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="message-box animated fadeIn" data-sound="alert" id="other_products">
    <div class="mb-container">
        <div class="mb-middle">
            <div class="mb-title"><span class="fa fa-sign-out"></span> Alta <strong>Materia</strong></div>
            <div class="mb-content">
                <p>¿Le gustaría dar de alta otra materia?</p>
                <p>Pulse Sí para dar de alta una nueva materia. Pulse No para ir al inventario.</p>
            </div>
            <div class="mb-footer">
                <div class="pull-right">
                    <a href="<?= $this->url->get('dashboard/products/new') ?>" class="btn btn-success btn-lg">Si</a>
                    <a href="<?= $this->url->get('dashboard/products') ?>" class="btn btn-default btn-lg">No</a>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal" id="modalElement" tabindex="-1" role="dialog" aria-labelledby="defModalHead" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Cerrar</span></button>
                <h4 class="modal-title" id="defModalHead"></h4>
            </div>
            <form id="formElement" action="#" method="post" class="form-horizontal">
                <input type="hidden"  id="brid" name="brid">
                <div class="modal-body">
                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-3">
                            <input type="text" class="form-control" placeholder="Nombre" name="nameM" id="nameM" required/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-3">
                            <input type="text" class="form-control" placeholder="Permalink" name="permalinkM" id="permalinkM" required readonly/>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Cerrar</button>
                    <input type="submit" class="btn btn-info btn-submit-element" value="Guardar">
                    <i id="session-loading" class="fa fa-spinner fa-spin fade hide" style="font-size: 22px;"></i>
                </div>
            </form>
        </div>
    </div>
</div>