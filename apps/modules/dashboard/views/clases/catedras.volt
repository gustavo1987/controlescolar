<div class="page-content-wrap">
    <div class=" container sinpadding">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12 fondo-gris-general">
                <div class="panel panel-success">
                    <div class="panel-heading text-right">
                        <h3 class="panel-title">Catedras</h3>
                    <?php if($clase->getStatus()=="ACTIVO"):?>
                        <a href="<?php echo '/dashboard/clases/newcatedra/'.$clase->getClasid();?>" class="btn btn-success btn-lg">Crear nueva catedra</a>
                    <?php endif;?>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table id="table-conferencias" class="table table-bordered table-hover table-actions generalDT" data-order="5" data-filter="desc">
                                <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Cátedra</th>
                                    <th>URL</th>
                                    <th>Fecha</th>
                                    <th>Estatus</th>
                                    <th>Opciones</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach($catedras as $key => $values):?>
                                <?php $dateC= $values->getDateBegin(); $newDate = date("d-m-Y", strtotime($dateC));?>
                                <tr class="<?php echo $values->getStatus()=="ACTIVO"?"tr-green":"tr-red";?>" id="<?=$values->getConfid();?>">
                                    <td class="<?php echo $values->getStatus()=="ACTIVO"?"tr-green":"tr-red";?>"><?=$key+1;?></td>
                                    <td class="<?php echo $values->getStatus()=="ACTIVO"?"tr-green":"tr-red";?>"><?=$values->getName();?></td>
                                    <td class="<?php echo $values->getStatus()=="ACTIVO"?"tr-green":"tr-red";?>"><?=$values->getUrl();?></td>
                                    <td class="<?php echo $values->getStatus()=="ACTIVO"?"tr-green":"tr-red";?>"><?= $newDate ?></td>
                                    <td class="<?php echo $values->getStatus()=="ACTIVO"?"tr-green":"tr-red";?>"><?php if($values->getStatus()=="INACTIVO"){echo "REALIZADA";}else{echo "POR LLEVAR";}?></td>
                                    <td class="<?php echo $values->getStatus()=="ACTIVO"?"tr-green":"tr-red";?>">
                                        <?php if($values->getStatus()=="ACTIVO"):?>
                                        <a title="Eliminar catedra" class="deletecatedra" data-id="<?=$values->getConfid();?>" data-clasid="<?php echo $clase->getClasid();?>"><span class="fa fa-times fa-2x"></span></a>&nbsp;
                                        <?php endif; ?>
                                    </td>
                                </tr>
                                <?php endforeach;?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>