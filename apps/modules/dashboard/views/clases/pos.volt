<div class="page-content-wrap">
   <div class=" container sinpadding">
      <div class="row">
         <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12 fondo-gris-general">
            <?php foreach ($posgrados as $key => $value):?>
            <div class="col-md-6" style="padding-bottom: 10%;padding-top: 5%;">
                <ul>
                    <li class="student"><a href="/dashboard/clases/index/<?php echo $value->getBrid();?>" style="text-decoration: none;"><i class="fas fa-chalkboard-teacher" aria-hidden="true"></i><p class="position1" style="color: white;"><?php if($value->getName()=="Maestria en Derecho Constitucional y Derecho"){echo "Maestría en Derecho Constitucional y Derechos Humanos";}elseif($value->getName()=="Doctorado en Derecho Constitucional y Derecho"){echo "Doctorado en Derecho Constitucional y Derechos Humanos";}else{ echo $value->getName();}?></p></a>
                    </li>
                </ul>
            </div>
            <?php endforeach;?>
         </div>
      </div>
   </div>
</div>