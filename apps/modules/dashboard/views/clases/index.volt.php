<div class="page-content-wrap">
    <div class=" container sinpadding">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12 fondo-gris-general">
                <div class="panel panel-success">
                    <div class="panel-heading text-right">
                        <h3 class="panel-title">Clases</h3>
                        <a href="<?= $this->url->get('dashboard/clases/anteriores') ?>" class="btn btn-danger btn-lg">Clases anteriores</a>
                        <a href="<?php echo '/dashboard/clases/agregar/'.$param;?>" class="btn btn-success btn-lg">Crear nueva clase</a>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover table-actions generalDT" data-order="5" data-filter="desc">
                                <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Cátedra</th>
                                    <th>Maestro</th>
                                    <th>Fecha Inicial</th>
                                    <th>Estatus</th>
                                    <th>Opciones</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach($clases as $key => $values):?>
                                <?php $dateC= $values->getDateCreation(); $newDate = date("d-m-Y", strtotime($dateC));?>
                                <tr class="<?php echo $values->getStatusClase()=="ACTIVO"?"tr-green":"tr-red";?>" id="<?= $values->getClasid() ?>">
                                    <td class="<?php echo $values->getStatusClase()=="ACTIVO"?"tr-green":"tr-red";?>"><?= $key + 1 ?></td>
                                    <td class="<?php echo $values->getStatusClase()=="ACTIVO"?"tr-green":"tr-red";?>"><?= $values->getNameClase() ?></td>
                                    <td class="<?php echo $values->getStatusClase()=="ACTIVO"?"tr-green":"tr-red";?>"><?= $values->getName() ?> <?= $values->getLastName() ?> <?= $values->getSecondName() ?></td>
                                    <td class="<?php echo $values->getStatusClase()=="ACTIVO"?"tr-green":"tr-red";?>"><?= $newDate ?></td>
                                    <td class="<?php echo $values->getStatusClase()=="ACTIVO"?"tr-green":"tr-red";?>"><?php if($values->getStatusClase()=="INACTIVO"){echo "Cursada";}else{echo "En curso";}?></td>
                                    <td class="<?php echo $values->getStatusClase()=="ACTIVO"?"tr-green":"tr-red";?>">
                                        <a title="Editar clase" href="<?= $this->url->get('dashboard/clases/edit/') ?><?= $values->getClasid() ?>" class=""><span class="fa fa-edit fa-2x"></span></a>&nbsp;
                                        <a title="Lista de alumnos" href="<?= $this->url->get('dashboard/clases/list/') ?><?= $values->getClasid() ?>" class=""><span class="fa fa-users fa-2x"></span></a>&nbsp;
                                        <a title="Lista de cátedras" href="<?= $this->url->get('dashboard/clases/catedras/') ?><?= $values->getClasid() ?>" class=""><span class="fa fa-eye fa-2x"></span></a>&nbsp;
                                        <a title="Estatus matería" class="class-off"><span class="fa fa-power-off fa-2x"></span></a>
                                    </td>
                                </tr>
                                <?php endforeach;?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="message-box animated fadeIn" data-sound="alert" id="mb-remove-row">
    <div class="mb-container">
        <div class="mb-middle">
            <div class="mb-title"><span class="fa fa-times"></span> Eliminar <strong>Datos</strong> ?</div>
            <div class="mb-content">
                <p>¿Estas seguro de eliminar esta fila?</p>
                <p>Presione "Si" si esta seguro.</p>
            </div>
            <div class="mb-footer">
                <div class="pull-right">
                    <button class="btn btn-success btn-lg mb-control-yes">Si</button>
                    <button class="btn btn-default btn-lg mb-control-close">No</button>
                </div>
            </div>
        </div>
    </div>
</div>