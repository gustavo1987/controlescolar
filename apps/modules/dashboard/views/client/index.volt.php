<div class="page-title">
    <div class="col-sm-6 text-left">
        <h2><a href="<?= $this->url->get('dashboard') ?>"><span class="fa fa-arrow-circle-o-left"></span></a> Menú principal</h2>
    </div>
    <div class="col-sm-6 text-right">
        <a href="<?= $this->url->get('dashboard/client/new') ?>" class="btn btn-success btn-lg">Registrar nuevo estudiante</a>
    </div>
</div>
<div class="page-content-wrap">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-success">
                <div class="panel-heading">
                    <h3 class="panel-title">Registrados</h3>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover table-actions generalDT" data-order="5" data-filter="desc">
                            <thead>
                            <tr>
                                <th>Número&nbsp;Cliente</th>
                                <th>Nombre</th>
                                <th>Apellido Paterno</th>
                                <th>Apellido Materno</th>
                                <th>Factura</th>
                                <th>Fecha registro</th>
                                <th>Opciones</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($clients as $values) { ?>
                            <?php $dateC= $values->getDateCreation(); $newDate = date("d-m-Y", strtotime($dateC));?>
                            <tr id="<?= $values->getClid() ?>">
                                <td><?= $values->getIdentifier() ?></td>
                                <td><?= $values->getName() ?></td>
                                <td><?= $values->getLastName() ?></td>
                                <td><?= $values->getSecondName() ?></td>
                                <td>No</td>
                                <!--td><= $values->getJurisdiction()=="NATIONAL"?"Si":"No";?></td-->
                                <td><?= $newDate ?></td>
                                <td>
                                    <a title="Editar cliente" href="<?= $this->url->get('dashboard/client/edit/') ?><?= $values->getClid() ?>" class=""><span class="fa fa-edit fa-2x"></span></a>&nbsp;
                                </td>
                            </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- END DEFAULT DATATABLE -->
        </div>
    </div>
</div>
<!-- PAGE CONTENT WRAPPER -->
<!-- MESSAGE BOX-->
<div class="message-box animated fadeIn" data-sound="alert" id="mb-remove-row">
    <div class="mb-container">
        <div class="mb-middle">
            <div class="mb-title"><span class="fa fa-times"></span> Eliminar <strong>Datos</strong> ?</div>
            <div class="mb-content">
                <p>¿Estas seguro de eliminar esta fila?</p>
                <p>Presione "Si" si esta seguro.</p>
            </div>
            <div class="mb-footer">
                <div class="pull-right">
                    <button class="btn btn-success btn-lg mb-control-yes">Si</button>
                    <button class="btn btn-default btn-lg mb-control-close">No</button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END MESSAGE BOX-->