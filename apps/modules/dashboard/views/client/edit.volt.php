<style type="text/css">.page-container .page-content{position: absolute !important;}</style>
<div class="page-title">
    <h2><a href="<?= $this->url->get('dashboard/clients') ?>"><span class="fa fa-arrow-circle-o-left"></span></a> Regresar</h2>
</div>
<div class="page-content-wrap">
    <div class="row">
        <div class="col-md-12">
            <h4 class="text-title">Editar cliente</h4>
            <div class="col-md-12 col-xs-12 panel-body form-group-separated">
                <form action="#" method="post" id="editClient" name="editClient" role="form" class="form-horizontal">
                    <div class="form-group">
                        <label class="col-md-2 col-xs-12 control-label">Nombre <span>*</span></label>
                        <div class="col-md-3 col-xs-12">
                            <input type="text" class="form-control" name="name" id="name" value="<?= $client->getName() ?>" required placeholder="Nombres">
                        </div>
                        <div class="col-md-3 col-xs-12">
                            <input type="text" class="form-control" name="last_name" id="last_name" value="<?= $client->getLastName() ?>" required placeholder="Apellido Paterno">
                        </div>
                        <div class="col-md-3 col-xs-12">
                            <input type="text" class="form-control" name="second_name" id="second_name" value="<?= $client->getSecondName() ?>" placeholder="Apellido Materno">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 col-xs-12 control-label">Sexo <span>*</span></label>
                        <div class="col-md-8 col-sm-6 col-xs-12">
                            <select name="sex" id="sex" class="form-control select2">
                                <option value="">Selecciona el tipo de sexo</option>
                                <option <?=$client->getSex()=="F"?"selected":""?> value="F">Femenino</option>
                                <option <?=$client->getSex()=="M"?"selected":""?> value="M">Masculino</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 col-xs-12 control-label">Fecha Nacimiento<span>*</span></label>
                        <div class="col-md-3 col-xs-12">
                            <?php $birthDate = explode("-",$client->getBirthDate());?>
                            <input type="text" class="form-control birth_date" name="birth_date" id="birth_date" placeholder="Fecha nacimiento" value="<?= $client->getBirthDate()?"$birthDate[2]/$birthDate[1]/$birthDate[0]":""?>">
                        </div>
                        <div class="col-md-3 col-xs-12">
                            <input type="text" class="form-control" name="age" id="age" value="<?=date("Y")-$birthDate[0]?>" placeholder="Edad" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 col-xs-12 control-label">Email <span>*</span></label>
                        <div class="col-md-8 col-sm-6 col-xs-12">
                            <input type="email" class="form-control" name="email" data-value="<?= $client->getEmail() ?>" id="email" value="<?= $client->getEmail() ?>" required placeholder="Cuenta de correo">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 col-xs-12 control-label">Teléfono <span>*</span></label>
                        <div class="col-md-8 col-sm-6 col-xs-12">
                            <input type="text" class="form-control" minlength="10" maxlength="15" name="tel" id="tel" value="<?= $client->getTel() ?>" required placeholder="Numero telefónico" >
                        </div>
                    </div>
                    <!--<div class="form-group">
                        <label class="col-md-2 col-xs-12 control-label">Institución</label>
                        <div class="col-md-8 col-sm-6 col-xs-12">
                            <input type="text" class="form-control" name="institution" id="institution" value="client.getInstitution()}}" placeholder="Institución Educativa o de Trabajo">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 col-xs-12 control-label">Descuento <span>*</span></label>
                        <div class="col-md-8 col-sm-6 col-xs-12">
                            <input required type="number" min="0" max="100" class="form-control" name="discount" value="client.getDiscount()}}" id="discount" placeholder="Descuento para su servicios">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 col-xs-12 control-label">Calle <span>*</span></label>
                        <div class="col-md-8 col-sm-6 col-xs-12">
                            <input type="text" class="form-control" name="street_dir" id="street_dir" value="client.getStreet()}}" placeholder="Calle o Avenida">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 col-xs-12 control-label">Colonia </label>
                        <div class="col-md-8 col-sm-6 col-xs-12">
                            <input type="text" class="form-control" name="colony_dir" id="colony_dir" value="client.getColony()}}" placeholder="Colonia">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 col-xs-12 control-label">Ciudad <span>*</span></label>
                        <div class="col-md-8 col-sm-6 col-xs-12">
                            <input type="text"  class="form-control" name="city_dir" id="city_dir" value="client.getCity()}}" placeholder="Ciudad">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 col-xs-12 control-label">Código Postal <span>*</span></label>
                        <div class="col-md-8 col-sm-6 col-xs-12">
                            <input type="text" class="form-control" name="cp" id="cp" value="client.getCp()}}" placeholder="Código Postal" >
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 col-xs-12 control-label">Estado y Municipio <span>*</span></label>
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <select name="state_dir" id="state_dir" class="form-control select2" data-placeholder="Selecciona el estado de procedencia." multiple>
                                <php foreach($estados as $key=>$val):?>
                                    <option <=$client->getEid()==$key?"selected":""?> value="<=$key;?>"><=$val;?></option>
                                <php endforeach?>
                            </select>
                        </div>
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <select name="mpid" id="mpid" class="form-control select2 selectU" data-placeholder="Selecciona el municipio de procedencia." multiple>
                                <php foreach($municipios as $key => $val):?>
                                    <php if($client->getEid()==$val["eid"]):?>
                                        <option <=$client->getMpid()==$key?"selected":"";?> value="<=$key;?>" class="options"><=$val['nombre'];?></option>
                                    endif; ?>
                                endforeach?>
                            </select>
                        </div>
                    </div>-->
                    <div class="form-group">
                        <input type="hidden" value="<?=$client->getBllid()?>" name="bllid">
                        <input type="hidden" value="<?=$client->getDid()?>" name="did">
                        <input type="hidden" value="<?=$client->getClid()?>" name="clid">
                        <label class="col-md-2 col-xs-12 control-label">Facturación</label>
                        <div class="col-md-2 col-sm-6 col-xs-12">
                            <input type="radio" name="iva" value="mx" <?=$client->getIva()?"checked":"";?> >&nbsp;Si
                        </div>
                        <div class="col-md-2 col-sm-6 col-xs-12">
                            <input type="radio"  name="iva"  value="et" <?=$client->getIva()!=="SI"?"checked":"";?>>&nbsp;No
                        </div>
                    </div>
                    <div class="form-group billing hide">
                        <label class="col-md-2 col-xs-12 control-label">RFC</label>
                        <div class="col-md-8 col-sm-6 col-xs-12">
                            <input type="text" class="form-control" name="rfc" data-value="<?= $client->getRfc() ?>" id="rfc" value="<?= $client->getRfc() ?>" maxlength="15" minlength="10" placeholder="RFC">
                        </div>
                    </div>
                    <div class="form-group billing hide">
                        <label class="col-md-2 col-xs-12 control-label">Razón Social</label>
                        <div class="col-md-8 col-sm-6 col-xs-12">
                            <input type="text" placeholder="Razón social" class="form-control" name="business_name" id="business_name" value="<?= $client->getBussinessName() ?>">
                        </div>
                    </div>
                    <div class="form-group billing hide">
                        <label class="col-md-2 col-xs-12 control-label">Calle </label>
                        <div class="col-md-8 col-sm-6 col-xs-12">
                            <input type="text" class="form-control" name="street" id="street" placeholder="Calle" value="<?= $client->getStreetFiscal() ?>">
                        </div>
                    </div>
                    <div class="form-group billing hide">
                        <label class="col-md-2 col-sm-2 col-xs-12 control-label">Número </label>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <input type="text" class="form-control" name="number_int" id="number_int" placeholder="Número Interior" value="<?= $client->getNumberInt() ?>">
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <input type="text" class="form-control" name="number_ext" id="number_ext" placeholder="Número Exterior" value="<?= $client->getNumberExt() ?>">
                        </div>
                    </div>
                    <div class="form-group billing hide">
                        <label class="col-md-2 col-xs-12 control-label">Colonia </label>
                        <div class="col-md-8 col-sm-6 col-xs-12">
                            <input type="text"  class="form-control" name="colony" id="colony" placeholder="Colonia" value="<?= $client->getColonyFiscal() ?>">
                        </div>
                    </div>
                    <div class="form-group billing hide">
                        <label class="col-md-2 col-xs-12 control-label">Localidad </label>
                        <div class="col-md-8 col-sm-6 col-xs-12">
                            <input type="text" class="form-control" name="location" id="location" placeholder="Localidad" value="<?= $client->getLocation() ?>">
                        </div>
                    </div>
                    <div class="form-group billing hide">
                        <label class="col-md-2 col-xs-12 control-label">Estado y Municipio <span>*</span></label>
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <select name="state" id="state" class="form-control select2" data-placeholder="Selecciona el estado de procedencia." multiple>
                                <?php foreach($estados as $key=>$val):?>
                                    <option <?=$client->getEidFiscal()==$key?"selected":""?> value="<?=$key;?>"><?=$val;?></option>
                                <?php endforeach?>
                            </select>
                        </div>
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <select name="mpid_fiscal" id="mpid_fiscal" class="form-control select2 selectU" data-placeholder="Selecciona el municipio de procedencia." multiple>
                                <?php foreach($municipios as $key => $val):?>
                                    <?php if($client->getEidFiscal()==$val["eid"]):?>
                                        <option <?=$client->getMpidFiscal()==$key?"selected":"";?> value="<?=$key;?>" class="options"><?=$val['nombre'];?></option>
                                    <?php endif; ?>
                                <?php endforeach?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group billing hide">
                        <label class="col-md-2 col-xs-12 control-label">Código Postal </label>
                        <div class="col-md-8 col-sm-6 col-xs-12">
                            <input type="text" class="form-control" name="postal_code" id="postal_code" placeholder="Código Postal"  value="<?= $client->getPostalCode() ?>">
                        </div>
                    </div>
                    <div class="form-group billing hide">
                        <label class="col-md-2 col-xs-12 control-label">Email Facturación </label>
                        <div class="col-md-8 col-sm-6 col-xs-12">
                            <input type="email" placeholder="Email Facturación" class="form-control" name="email_fiscal" id="email_fiscal" value="<?= $client->getEmailFiscal() ?>">
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="form-group">
                        <label class="col-md-2 col-xs-12 control-label">Estatus</label>
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <select id="status" name="status" class="form-control selectU" required multiple>
                                <option <?=$client->getStatus()=="ACTIVO"?"selected":""?> value="ACTIVO">ACTIVO</option>
                                <option <?=$client->getStatus()=="INACTIVO"?"selected":""?> value="INACTIVO">INACTIVO</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-2">
                            <input type="submit" class="btn btn-success" value="Guardar"/>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- info -->
<div class="message-box message-box-info animated fadeIn" id="message-box-info">
    <div class="mb-container">
        <div class="mb-middle">
            <div class="mb-title"> Actualizando &nbsp; <i class="fa fa-circle-o-notch fa-spin style-fa"></i></div>
            <div class="mb-content">
                <p>Guardando y Actualizando su información, espere un momento por favor.</p>
            </div>
        </div>
    </div>
</div>
<!-- end info -->
<!-- success -->
<div class="message-box message-box-success animated fadeIn" id="message-box-success">
    <div class="mb-container">
        <div class="mb-middle">
            <div class="mb-title"><span class="fa fa-check"></span> Información actualizada</div>
            <div class="mb-content">
                <p>Sus cambios han sido guardados correctamente, actualizaremos el sitio para reflejar los cambios.</p>
            </div>
        </div>
    </div>
</div>
<!-- end success -->
<!-- warning -->
<div class="message-box message-box-warning animated fadeIn" id="message-box-warning">
    <div class="mb-container">
        <div class="mb-middle">
            <div class="mb-title"><span class="fa fa-warning"></span> Error</div>
            <div class="mb-content">
                <p>Ha ocurrido un error al guardar su información, inténtelo nuevamente o regrese mas tarde.</p>
            </div>
            <div class="mb-footer">
                <button class="btn btn-default btn-lg pull-right mb-control-close">Cerrar</button>
            </div>
        </div>
    </div>
</div>
<!-- end danger -->
<div class="message-box message-box-danger animated fadeIn" id="message-box-danger">
    <div class="mb-container">
        <div class="mb-middle">
            <div class="mb-title"><span class="fa fa-times"></span> Cuidado</div>
            <div class="mb-content">
                <p>Usted no puede actualizar su imagen.</p>
            </div>
            <div class="mb-footer">
                <button class="btn btn-default btn-lg pull-right mb-control-close">Cerrar</button>
            </div>
        </div>
    </div>
</div>
<div class="message-box animated fadeIn" data-sound="alert" id="other_products">
    <div class="mb-container">
        <div class="mb-middle">
            <div class="mb-title"><span class="fa fa-sign-out"></span> Alta <strong>Cliente</strong></div>
            <div class="mb-content">
                <p>¿Le gustaría dar de alta otro cliente?</p>
                <p>Pulse Sí para dar de alta un nuevo cliente. Pulse No para ir a la lista de clientes.</p>
            </div>
            <div class="mb-footer">
                <div class="pull-right">
                    <button class="btn btn-success btn-lg mb-control-close">Si</button>
                    <a href="<?= $this->url->get('dashboard/clients') ?>" class="btn btn-default btn-lg">No</a>
                </div>
            </div>
        </div>
    </div>
</div>