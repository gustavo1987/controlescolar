<style type="text/css">
    .page-container .page-content{
        position: absolute !important;
    }
    label.control-label, input.form-control{
        color: black;
        font-size: 15px;
    }
</style>
<div class="page-content-wrap">
    <div class="container sinpadding">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12 fondo-gris-general">
                <div class="col-md-12 col-xs-12 panel-body form-group-separated" style="background-color: white;">
                    <form action="#" method="post" id="editClientadmision" name="editClientadmision" role="form" class="form-horizontal">
                        <div class="form-group">
                            <input type="hidden" value="<?=$inscripciones->getStuid()?>" name="clid">
                            <label class="col-md-2 col-sm-2 col-lg-2  col-xs-12 control-label">Nombre <span>*</span></label>
                            <div class="col-md-3 col-xs-12">
                                <input type="text" class="form-control" name="name" id="name" value="<?= $inscripciones->getName() ?>" required placeholder="Nombres">
                            </div>
                            <div class="col-md-3 col-xs-12">
                                <input type="text" class="form-control" name="last_name" id="last_name" value="<?= $inscripciones->getLastName() ?>" required placeholder="Apellido Paterno">
                            </div>
                            <div class="col-md-3 col-xs-12">
                                <input type="text" class="form-control" name="second_name" id="second_name" value="<?= $inscripciones->getSecondName() ?>" placeholder="Apellido Materno">
                            </div>
                        </div>
                        <div class="col-md-12 col-xs-12 col-sm-12 col-lg-12">
                            <div class="col-md-6 col-xs-12 col-sm-6 col-lg-6 form-group">
                                <label class="col-md-4 col-sm-4 col-lg-4 col-xs-12 control-label">Posgrado <span>*</span></label>
                                <div class="col-md-8 col-sm-8  col-lg-8 col-xs-12">
                                    <input type="text" class="form-control" name="carrera" data-value="<?= $inscripciones->getTitle() ?>" id="email" value="<?= $inscripciones->getTitle() ?>" required placeholder="Carrera">
                                </div>
                            </div>
                            <div class="col-md-6 col-xs-12 col-sm-6 col-lg-6 form-group">
                                <label class="col-md-4 col-sm-4 col-lg-4 col-xs-12 control-label">Sexo <span>*</span></label>
                                <div class="col-md-8 col-sm-8  col-lg-8 col-xs-12">
                                    <select name="sex" id="sex" class="form-control select2">
                                        <option value="">Selecciona el tipo de sexo</option>
                                        <option <?=$inscripciones->getSex()=="F"?"selected":""?> value="F">Femenino</option>
                                        <option <?=$inscripciones->getSex()=="M"?"selected":""?> value="M">Masculino</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 col-xs-12 col-sm-12 col-lg-12">
                            <div class="col-md-6 col-xs-12 col-sm-6 col-lg-6 form-group">
                                <label class="col-md-4 col-sm-4 col-lg-4 col-xs-12 control-label">Email <span>*</span></label>
                                <div class="col-md-8 col-sm-8  col-lg-8 col-xs-12">
                                    <input type="email" class="form-control" name="email" data-value="<?= $inscripciones->getEmail() ?>" id="email" value="<?= $inscripciones->getEmail() ?>" required placeholder="Cuenta de correo">
                                </div>
                            </div>
                            <div class="col-md-6 col-xs-12 col-sm-6 col-lg-6 form-group">
                                <label class="col-md-4 col-sm-4 col-lg-4 col-xs-12 control-label">Teléfono <span>*</span></label>
                                <div class="col-md-8 col-sm-8  col-lg-8 col-xs-12">
                                    <input type="text" class="form-control" minlength="10" maxlength="15" name="tel" id="tel" value="<?= $inscripciones->getPhone() ?>" required placeholder="Numero telefónico" >
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12 col-xs-12 col-sm-12 col-lg-12">
                            <div class="col-md-6 col-xs-12 col-sm-6 col-lg-6 form-group billing">
                                <label class="col-md-4 col-sm-4 col-lg-4 col-xs-12 control-label">Calle </label>
                                <div class="col-md-8 col-sm-8  col-lg-8 col-xs-12">
                                    <input type="text" class="form-control" name="street" id="street" placeholder="Calle" value="<?= $inscripciones->getCalle() ?>">
                                </div>
                            </div>
                            <div class="col-md-6 col-xs-12 col-sm-6 col-lg-6 form-group billing">
                                <label class="col-md-2 col-sm-2 col-xs-12 control-label">Número </label>
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <input type="text" class="form-control" name="number_int" id="number_int" placeholder="Número Interior" value="<?= $inscripciones->getNumExt() ?>">
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <input type="text" class="form-control" name="number_ext" id="number_ext" placeholder="Número Exterior" value="<?= $inscripciones->getNumInt() ?>">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 col-xs-12 col-sm-12 col-lg-12">
                            <div class="col-md-6 col-xs-12 col-sm-6 col-lg-6 form-group billing">
                                <label class="col-md-4 col-sm-4 col-lg-4  col-xs-12 control-label">Ciudad </label>
                                <div class="col-md-8 col-sm-8  col-lg-8 col-xs-12">
                                    <input type="text" class="form-control" name="location" id="location" placeholder="Localidad" value="<?= $inscripciones->getCiudad() ?>">
                                </div>
                            </div>
                            <div class="col-md-6 col-xs-12 col-sm-6 col-lg-6 form-group billing">
                                <label class="col-md-4 col-sm-4 col-lg-4  col-xs-12 control-label">Estado<span>*</span></label>
                                <div class="col-md-8 col-sm-8  col-lg-8 col-xs-12">
                                    <input type="text" class="form-control" name="estado" id="estado" placeholder="Estado" value="<?= $inscripciones->getEstado() ?>">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 col-xs-12 col-sm-12 col-lg-12">
                            <div class="col-md-6 col-xs-12 col-sm-6 col-lg-6 form-group billing">
                                <label class="col-md-4 col-sm-4 col-lg-4  col-xs-12 control-label">Código Postal </label>
                                <div class="col-md-8 col-sm-8  col-lg-8 col-xs-12">
                                    <input type="text" class="form-control" value="<?= $inscripciones->getCp() ?>">
                                </div>
                            </div>
                            <div class="col-md-6 col-xs-12 col-sm-6 col-lg-6 form-group billing">
                                <label class="col-md-4 col-sm-4 col-lg-4  col-xs-12 control-label">Periodo </label>
                                <div class="col-md-8 col-sm-8  col-lg-8 col-xs-12">
                                    <input type="text" class="form-control" name="postal_code" id="postal_code" placeholder="Código Postal"  value="<?php echo $inscripciones->getPeriodo();?>">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 col-xs-12 col-sm-12 col-lg-12">
                            <div class="col-md-6 col-xs-12 col-sm-6 col-lg-6 form-group billing">
                                <label class="col-md-4 col-sm-4 col-lg-4  col-xs-12 control-label">Nacionalidad </label>
                                <div class="col-md-8 col-sm-8  col-lg-8 col-xs-12">
                                    <input type="text" class="form-control" value="<?php echo $inscripciones->getNacionalidad();?>">
                                </div>
                            </div>
                            <div class="col-md-6 col-xs-12 col-sm-6 col-lg-6 form-group billing">
                                <label class="col-md-4 col-sm-4 col-lg-4  col-xs-12 control-label">Documento Acreditación </label>
                                <div class="col-md-8 col-sm-8  col-lg-8 col-xs-12">
                                    <input type="text" class="form-control" value="<?php echo $inscripciones->getDocumentoAcreditacion();?>">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 col-xs-12 col-sm-12 col-lg-12">
                            <div class="col-md-6 col-xs-12 col-sm-6 col-lg-6 form-group billing">
                                <label class="col-md-4 col-sm-4 col-lg-4  col-xs-12 control-label">Número de documento </label>
                                <div class="col-md-8 col-sm-8  col-lg-8 col-xs-12">
                                    <input type="text" class="form-control" value="<?php echo $inscripciones->getNumeroDocumentoAcreditacion();?>">
                                </div>
                            </div>
                            <div class="col-md-6 col-xs-12 col-sm-6 col-lg-6 form-group billing">
                                <label class="col-md-4 col-sm-4 col-lg-4  col-xs-12 control-label">Fecha de Nacimiento </label>
                                <div class="col-md-8 col-sm-8  col-lg-8 col-xs-12">
                                    <input type="text" class="form-control" value="<?php echo $inscripciones->getFechaNacimiento();?>">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 col-xs-12 col-sm-12 col-lg-12">
                            <div class="col-md-6 col-xs-12 col-sm-6 col-lg-6 form-group billing">
                                <label class="col-md-4 col-sm-4 col-lg-4  col-xs-12 control-label">País de  Nacimiento </label>
                                <div class="col-md-8 col-sm-8  col-lg-8 col-xs-12">
                                    <input type="text" class="form-control" value="<?php echo $inscripciones->getPaisNacimiento();?>">
                                </div>
                            </div>
                            <div class="col-md-6 col-xs-12 col-sm-6 col-lg-6 form-group billing">
                                <label class="col-md-4 col-sm-4 col-lg-4  col-xs-12 control-label">Domicilio de Nacimiento </label>
                                <div class="col-md-8 col-sm-8  col-lg-8 col-xs-12">
                                    <input type="text" class="form-control" value="<?php echo $inscripciones->getDomicilioNacimiento();?>">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 col-xs-12 col-sm-12 col-lg-12">
                            <div class="col-md-6 col-xs-12 col-sm-6 col-lg-6 form-group billing">
                                <label class="col-md-4 col-sm-4 col-lg-4  col-xs-12 control-label">C.P. de Nacimiento </label>
                                <div class="col-md-8 col-sm-8  col-lg-8 col-xs-12">
                                    <input type="text" class="form-control" value="<?php echo $inscripciones->getCpNacimiento();?>">
                                </div>
                            </div>
                            <div class="col-md-6 col-xs-12 col-sm-6 col-lg-6 form-group billing">
                                <label class="col-md-4 col-sm-4 col-lg-4  col-xs-12 control-label">Ciudad de Nacimiento </label>
                                <div class="col-md-8 col-sm-8  col-lg-8 col-xs-12">
                                    <input type="text" class="form-control" value="<?php echo $inscripciones->getCiudadNacimiento();?>">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 col-xs-12 col-sm-12 col-lg-12">
                            <div class="col-md-6 col-xs-12 col-sm-6 col-lg-6 form-group billing">
                                <label class="col-md-4 col-sm-4 col-lg-4  col-xs-12 control-label">Estado de Nacimiento </label>
                                <div class="col-md-8 col-sm-8  col-lg-8 col-xs-12">
                                    <input type="text" class="form-control" value="<?php echo $inscripciones->getEstadoNacimiento();?>">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12 col-xs-12 col-sm-12 col-lg-12 form-group billing">
                            <h2 style="text-align: center;">Domicilio de Residencia</h2>
                        </div>

                        <div class="col-md-12 col-xs-12 col-sm-12 col-lg-12">
                            <div class="col-md-6 col-xs-12 col-sm-6 col-lg-6 form-group billing">
                                <label class="col-md-4 col-sm-4 col-lg-4  col-xs-12 control-label">Teléfono Residencia </label>
                                <div class="col-md-8 col-sm-8  col-lg-8 col-xs-12">
                                    <input type="text" class="form-control" value="<?php echo $inscripciones->getTelefonoResidencia();?> <?php echo $inscripciones->getFijoResidencia();?>">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12 col-xs-12 col-sm-12 col-lg-12 form-group billing">
                            <h2 style="text-align: center;">Estudios previos</h2>
                        </div>
                        <div class="col-md-12 col-xs-12 col-sm-12 col-lg-12">
                            <div class="col-md-6 col-xs-12 col-sm-6 col-lg-6 col-md-6 col-xs-12 col-sm-6 col-lg-6 form-group billing">
                                <label class="col-md-4 col-sm-4 col-lg-4 col-xs-12 control-label"> Universidad </label>
                                <div class="col-md-8 col-sm-8 col-lg-8 col-xs-12">
                                    <input type="text" class="form-control" value="<?php echo $inscripciones->getUniversidad();?>">
                                </div>
                            </div>
                            <div class="col-md-6 col-xs-12 col-sm-6 col-lg-6 col-md-6 col-xs-12 col-sm-6 col-lg-6 form-group billing">
                                <label class="col-md-4 col-sm-4 col-lg-4 col-xs-12 control-label"> País de estudios </label>
                                <div class="col-md-8 col-sm-8  col-lg-8 col-xs-12">
                                    <input type="text" class="form-control" value="<?php echo $inscripciones->getPaisEstudios();?>">
                                </div>
                            </div>
                        </div>


                        <div class="col-md-12 col-xs-12 col-sm-12 col-lg-12">
                            <div class="col-md-6 col-xs-12 col-sm-6 col-lg-6 col-md-6 col-xs-12 col-sm-6 col-lg-6 form-group billing">
                                <label class="col-md-4 col-sm-4 col-lg-4 col-xs-12 control-label">Nombre del titulo </label>
                                <div class="col-md-8 col-sm-8  col-lg-8 col-xs-12">
                                    <input type="text" class="form-control" value="<?php echo $inscripciones->getNombreTitulo();?>">
                                </div>
                            </div>
                            <div class="col-md-6 col-xs-12 col-sm-6 col-lg-6 col-md-6 col-xs-12 col-sm-6 col-lg-6 form-group billing">
                                <label class="col-md-4 col-sm-4 col-lg-4 col-xs-12 control-label">Tipo del titulo </label>
                                <div class="col-md-8 col-sm-8  col-lg-8 col-xs-12">
                                    <input type="text" class="form-control" value="<?php echo $inscripciones->getTipoTitulo();?>">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12 col-xs-12 col-sm-12 col-lg-12">

                            <div class="col-md-6 col-xs-12 col-sm-6 col-lg-6 col-md-6 col-xs-12 col-sm-6 col-lg-6 form-group billing">
                                <label class="col-md-4 col-sm-4 col-lg-4 col-xs-12 control-label">Estado del titulo </label>
                                <div class="col-md-8 col-sm-8  col-lg-8 col-xs-12">
                                    <input type="text" class="form-control" value="<?php echo $inscripciones->getEstadoEstudios();?>">
                                </div>
                            </div>
                            <?php if($inscripciones->getEstadoEstudios()=="finalizado"):?>
                            <div class="col-md-6 col-xs-12 col-sm-6 col-lg-6 col-md-6 col-xs-12 col-sm-6 col-lg-6 form-group billing">
                                <label class="col-md-4 col-sm-4 col-lg-4 col-xs-12 control-label">Fecha de finalización </label>
                                <div class="col-md-8 col-sm-8  col-lg-8 col-xs-12">
                                    <input type="text" class="form-control" value="<?php echo $inscripciones->getFechaFinalizacion();?>">
                                </div>
                            </div>
                            <?php else:?>
                            <div class="col-md-6 col-xs-12 col-sm-6 col-lg-6 col-md-6 col-xs-12 col-sm-6 col-lg-6 form-group billing">
                                <label class="col-md-4 col-sm-4 col-lg-4 col-xs-12 control-label">Año cursado </label>
                                <div class="col-md-8 col-sm-8  col-lg-8 col-xs-12">
                                    <input type="text" class="form-control" value="<?php echo $inscripciones->getAnioCursado();?>">
                                </div>
                            </div>
                            <?php endif;?>
                        </div>
                        <div class="col-md-12 col-xs-12 col-sm-12 col-lg-12 form-group billing">
                            <h2 style="text-align: center;">Documentación</h2>
                        </div>
                        <?php foreach ($documents as $key => $value):?>
                        <div class="col-md-6 col-xs-12 col-sm-6 col-lg-6 form-group billing">
                                <label class="col-md-3 col-sm-3 col-lg-3 col-xs-12 control-label"><?php echo $value->getType(); ?></label>
                                <div class="col-md-5 col-sm-5 col-xs-12">
                                    <?php if($value->getType()=="RECIBO"):?>
                                        <label><a class="" href="http://ideiberoamerica.com/front/api/documentacion/recibos/<?php echo $value->getName();?>" target="_blank"><?php echo $value->getType(); ?></a></label>
                                    <?php else:?>
                                        <label><a class="" href="http://ideiberoamerica.com/front/api/documentacion/<?php echo $value->getName();?>" target="_blank"><?php echo $value->getType(); ?></a></label>
                                    <?php endif; ?>
                                </div>
                                <div class="col-md-4 col-sm-4 col-lg-4 col-xs-12">
                                    <label class="control-label"><?php echo $value->getDateCreation(); ?></label>
                                </div>
                        </div>
                        <?php endforeach; ?>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-2">
                                <input type="submit" class="btn btn-success" value="Mandar pago"/>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- info -->
<div class="message-box message-box-info animated fadeIn" id="message-box-info">
    <div class="mb-container">
        <div class="mb-middle">
            <div class="mb-title"> Actualizando &nbsp; <i class="fa fa-circle-o-notch fa-spin style-fa"></i></div>
            <div class="mb-content">
                <p>Guardando y Actualizando su información, espere un momento por favor.</p>
            </div>
        </div>
    </div>
</div>
<!-- end info -->
<!-- success -->
<div class="message-box message-box-success animated fadeIn" id="message-box-success">
    <div class="mb-container">
        <div class="mb-middle">
            <div class="mb-title"><span class="fa fa-check"></span> Información actualizada</div>
            <div class="mb-content">
                <p>Sus cambios han sido guardados correctamente, actualizaremos el sitio para reflejar los cambios.</p>
            </div>
        </div>
    </div>
</div>
<!-- end success -->
<!-- warning -->
<div class="message-box message-box-warning animated fadeIn" id="message-box-warning">
    <div class="mb-container">
        <div class="mb-middle">
            <div class="mb-title"><span class="fa fa-warning"></span> Error</div>
            <div class="mb-content">
                <p>Ha ocurrido un error al guardar su información, inténtelo nuevamente o regrese mas tarde.</p>
            </div>
            <div class="mb-footer">
                <button class="btn btn-default btn-lg pull-right mb-control-close">Cerrar</button>
            </div>
        </div>
    </div>
</div>
<!-- end danger -->
<div class="message-box message-box-danger animated fadeIn" id="message-box-danger">
    <div class="mb-container">
        <div class="mb-middle">
            <div class="mb-title"><span class="fa fa-times"></span> Cuidado</div>
            <div class="mb-content">
                <p>Usted no puede actualizar su imagen.</p>
            </div>
            <div class="mb-footer">
                <button class="btn btn-default btn-lg pull-right mb-control-close">Cerrar</button>
            </div>
        </div>
    </div>
</div>
<div class="message-box animated fadeIn" data-sound="alert" id="other_products">
    <div class="mb-container">
        <div class="mb-middle">
            <div class="mb-title"><span class="fa fa-sign-out"></span> Alta <strong>Cliente</strong></div>
            <div class="mb-content">
                <p>¿Le gustaría dar de alta otro estudiante?</p>
                <p>Pulse Sí para dar de alta un nuevo estudiante. Pulse No para ir a la lista de estudiantes.</p>
            </div>
            <div class="mb-footer">
                <div class="pull-right">
                    <button class="btn btn-success btn-lg mb-control-close">Si</button>
                    <a href="<?= $this->url->get('dashboard/clients') ?>" class="btn btn-default btn-lg">No</a>
                </div>
            </div>
        </div>
    </div>
</div>