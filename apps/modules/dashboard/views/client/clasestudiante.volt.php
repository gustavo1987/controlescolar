<style type="text/css">.page-container .page-content{position: absolute !important;}</style>
<ul class="breadcrumb">
    <li><a href="<?= $this->url->get('dashboard') ?>">Inicio</a></li>
    <li class="active"><a href="#">Editar información de clase del estudiante</a></li>
</ul>
<div class="page-title">
    <h2><a href="<?= $this->url->get('/dashboard/estudiantes/'.$salid."/".$subsalid) ?>"><span class="fa fa-arrow-circle-o-left"></span></a> Regresar</h2>
</div>
<div class="page-content-wrap">
    <div class="row">
        <div class="col-md-12">
            <h4 class="text-title">Editar estudiante</h4>
            <div class="col-md-12 col-xs-12 panel-body form-group-separated">
                <form action="#" method="post" id="editClientadmision" name="editClientadmision" role="form" class="form-horizontal">
                    <div class="form-group">
                        <input type="hidden" value="<?=$client->getClid()?>" name="clid">
                        <label class="col-md-2 col-xs-12 control-label">Nombre <span>*</span></label>
                        <div class="col-md-3 col-xs-12">
                            <input type="text" class="form-control" name="name" id="name" value="<?= $client->getName() ?>" required placeholder="Nombres">
                        </div>
                        <div class="col-md-3 col-xs-12">
                            <input type="text" class="form-control" name="last_name" id="last_name" value="<?= $client->getLastName() ?>" required placeholder="Apellido Paterno">
                        </div>
                        <div class="col-md-3 col-xs-12">
                            <input type="text" class="form-control" name="second_name" id="second_name" value="<?= $client->getSecondName() ?>" placeholder="Apellido Materno">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 col-xs-12 control-label">Matricula <span>*</span></label>
                        <div class="col-md-8 col-sm-6 col-xs-12">
                            <input type="text" class="form-control" name="carrea" value="<?= $client->getIdentifier() ?>" required placeholder="Matricula" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 col-xs-12 control-label">Email <span>*</span></label>
                        <div class="col-md-8 col-sm-6 col-xs-12">
                            <input type="email" class="form-control" name="email" data-value="<?= $client->getEmail() ?>" id="email" value="<?= $client->getEmail() ?>" required placeholder="Cuenta de correo">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 col-xs-12 control-label">Calificación <span>*</span></label>
                        <div class="col-md-8 col-sm-6 col-xs-12">
                            <input type="text" class="form-control" name="calificacion" id="calificacion" value="<?= $client->getCalificacion() ?>" required placeholder="Calificación" >
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 col-xs-12 control-label">Asistencia </label>
                        <div class="col-md-8 col-sm-6 col-xs-12">
                            <input type="text" class="form-control" name="asistencia" id="asistencia" placeholder="asistencia" value="<?= $client->getAsistencia() ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 col-xs-12 control-label">Observación </label>
                        <div class="col-md-8 col-sm-6 col-xs-12">
                            <input type="text" class="form-control" name="observacion" id="observacion" placeholder="observacion" value="<?= $client->getObservacion() ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-2">
                            <input type="submit" class="btn btn-success" value="Guardar"/>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- info -->
<div class="message-box message-box-info animated fadeIn" id="message-box-info">
    <div class="mb-container">
        <div class="mb-middle">
            <div class="mb-title"> Actualizando &nbsp; <i class="fa fa-circle-o-notch fa-spin style-fa"></i></div>
            <div class="mb-content">
                <p>Guardando y Actualizando su información, espere un momento por favor.</p>
            </div>
        </div>
    </div>
</div>
<!-- end info -->
<!-- success -->
<div class="message-box message-box-success animated fadeIn" id="message-box-success">
    <div class="mb-container">
        <div class="mb-middle">
            <div class="mb-title"><span class="fa fa-check"></span> Información actualizada</div>
            <div class="mb-content">
                <p>Sus cambios han sido guardados correctamente, actualizaremos el sitio para reflejar los cambios.</p>
            </div>
        </div>
    </div>
</div>
<!-- end success -->
<!-- warning -->
<div class="message-box message-box-warning animated fadeIn" id="message-box-warning">
    <div class="mb-container">
        <div class="mb-middle">
            <div class="mb-title"><span class="fa fa-warning"></span> Error</div>
            <div class="mb-content">
                <p>Ha ocurrido un error al guardar su información, inténtelo nuevamente o regrese mas tarde.</p>
            </div>
            <div class="mb-footer">
                <button class="btn btn-default btn-lg pull-right mb-control-close">Cerrar</button>
            </div>
        </div>
    </div>
</div>
<!-- end danger -->
<div class="message-box message-box-danger animated fadeIn" id="message-box-danger">
    <div class="mb-container">
        <div class="mb-middle">
            <div class="mb-title"><span class="fa fa-times"></span> Cuidado</div>
            <div class="mb-content">
                <p>Usted no puede actualizar su imagen.</p>
            </div>
            <div class="mb-footer">
                <button class="btn btn-default btn-lg pull-right mb-control-close">Cerrar</button>
            </div>
        </div>
    </div>
</div>
<div class="message-box animated fadeIn" data-sound="alert" id="other_products">
    <div class="mb-container">
        <div class="mb-middle">
            <div class="mb-title"><span class="fa fa-sign-out"></span> Alta <strong>Cliente</strong></div>
            <div class="mb-content">
                <p>¿Le gustaría dar de alta otro estudiante?</p>
                <p>Pulse Sí para dar de alta un nuevo estudiante. Pulse No para ir a la lista de estudiantes.</p>
            </div>
            <div class="mb-footer">
                <div class="pull-right">
                    <button class="btn btn-success btn-lg mb-control-close">Si</button>
                    <a href="<?= $this->url->get('dashboard/clients') ?>" class="btn btn-default btn-lg">No</a>
                </div>
            </div>
        </div>
    </div>
</div>