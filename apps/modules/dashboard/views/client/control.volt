<style>.select2-container {  width: 100% !important;  }</style>
<div class="page-title">
    <div class="col-sm-6 col-xs-12">
        <h2><span class="fa fa-cogs"></span> Adminstrar contacto</h2>
    </div>
    <div class="col-sm-6 col-xs-12 text-right">
        <h2 style="float: right"> Estatus : <button class="btn <?php echo "btn".$status[$client->getStatus()]?>"><?=$client->getStatus();?></button></h2>
    </div>
</div>
<div class="page-content-wrap">
    <div class="row">
        <div class="col-md-3 col-sm-4 col-xs-12">
            <form action="#" class="form-horizontal">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h3><span class="fa fa-user"></span> <?= $client->getName()?></h3>
                    </div>
                    <div class="panel-body form-group-separated">

                        <div class="form-group">
                            <label class="col-md-4 col-xs-5 control-label">#ID</label>
                            <div class="col-md-8 col-xs-7 line-height-30">
                                <?=$client->getClid()?>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 col-xs-5 control-label">Nombre</label>
                            <div class="col-md-8 col-xs-7 line-height-30">
                                <?=$client->getName()?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 col-xs-5 control-label">Apellidos</label>
                            <div class="col-md-8 col-xs-7 line-height-30">
                                <?=$client->getLastName()." ".$client->getSecondName()?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 col-xs-5 control-label">Sexo</label>
                            <div class="col-md-8 col-xs-7 line-height-30">
                                <?=$client->getSex()=="F"?"FEMENINO":"MASCULINO"?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 col-xs-5 control-label">Edad</label>
                            <div class="col-md-8 col-xs-7 line-height-30"><?=date('Y')-$birthdate["day"]?> años</div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 col-xs-5 control-label">Cumpleaños</label>
                            <div class="col-md-8 col-xs-7 line-height-30"><?= $birthdate["day"]." de ".$birthdate["month"]?></div>
                        </div>
                        <?php if($client->getTel()):?>
                            <div class="form-group">
                                <label class="col-md-4 col-xs-5 control-label">Celular</label>
                                <div class="col-md-8 col-xs-7 line-height-30">
                                    <?=$client->getTel()?>
                                </div>
                            </div>
                        <?php endif;?>
                        <?php if($client->getTelHouse()):?>
                            <div class="form-group">
                                <label class="col-md-4 col-xs-5 control-label">Tel.&nbsp;Casa</label>
                                <div class="col-md-8 col-xs-7 line-height-30">
                                    <?=$client->getTelHouse()?>
                                </div>
                            </div>
                        <?php endif;?>
                        <?php if($client->getTelWork()):?>
                            <div class="form-group">
                                <label class="col-md-4 col-xs-5 control-label">Tel.&nbsp;Trabajo</label>
                                <div class="col-md-8 col-xs-7 line-height-30">
                                    <?=$client->getTelWork()?>
                                </div>
                            </div>
                        <?php endif;?>
                    </div>
                </div>
            </form>
            <div class="visible-sm visible-xs panel panel-default form-horizontal">
                <div class="panel-body">
                    <h3><span class="fa fa-info-circle"></span> Información rápida</h3>
                    <p>Información rápida de este usuario</p>
                </div>
                <div class="panel-body form-group-separated">
                    <div class="form-group">
                        <label class="col-md-4 col-xs-5 control-label">Registro</label>
                        <div class="col-md-8 col-xs-7 line-height-30"><?= date("d-m-Y H:i:s",strtotime($client->getDateInscription()))?></div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 col-xs-5 control-label">Horario I.</label>
                        <div class="col-md-8 col-xs-7 line-height-30"><?= $hourInterest?></div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 col-xs-5 control-label">Turno A.</label>
                        <div class="col-md-8 col-xs-7 line-height-30"><?=$client->getTurn()?></div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 col-xs-5 control-label">Semestre</label>
                        <div class="col-md-8 col-xs-7 line-height-30"><?=$client->getSemester()?></div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 col-xs-5 control-label">Trabaja</label>
                        <div class="col-md-8 col-xs-7 line-height-30"><?=$client->getWorking()==1?"SI":"NO"?></div>
                    </div>
                </div>

            </div>
        </div>
        <div class="col-md-6 col-sm-8 col-xs-12">
            <form action="#" class="form-horizontal" id="controlClient" name="controlClient">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h3><span class="fa fa-pencil"></span> Control</h3>
                        <p>Control de usuario, mediante citas, llamadas etc</p>
                    </div>
                    <div class="panel-body form-group-separated">
                        <div class="form-group">
                            <label class="col-md-3 col-xs-5 control-label">Mensaje</label>
                            <div class="col-md-9 col-xs-7">
                                <textarea name="observations" id="observations" required class="form-control" rows="5" placeholder="Descripción del seguimiento"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 col-xs-5 control-label">Estatus actual</label>
                            <div class="col-md-5 col-xs-7">
                                <select required multiple name="call_status" id="call_status" class="selectU form-control" data-placeholder="Estatus de la llamada">
                                    <option <?= $client->getStatus()=="SEGUIMIENTO"?"selected":"";?> value="SEGUIMIENTO">SEGUIMIENTO</option>
                                    <option <?= $client->getStatus()=="CITA"?"selected":"";?> value="CITA">CITA</option>
                                    <option <?= $client->getStatus()=="ATENDIDO"?"selected":"";?> value="ATENDIDO">ATENDIDO</option>
                                    <option <?= $client->getStatus()=="INSCRITO"?"selected":"";?> value="INSCRITO">INSCRITO</option>
                                    <option <?= $client->getStatus()=="PERDIDO"?"selected":"";?> value="PERDIDO">PERDIDO</option>
                                </select>
                            </div>
                        </div>
                        <?php if($client->getStatus()!=="INSCRITO"):?>
                            <div class="form-group hidden color-scholarship" id="scholarshipDiv">
                                <label class="col-md-3 col-xs-3 control-label">¿Tiene beca?</label>
                                <div class="col-md-3 col-xs-3">
                                    <label class="switch">
                                        <input id="noScholarship" type="radio" required name="scholarship" checked value="no" data-value="no"/>
                                        <span></span>
                                    </label>
                                    <br>
                                    <label>No</label>
                                </div>
                                <div class="col-md-3 col-xs-3">
                                    <label class="switch">
                                        <input type="radio" required name="scholarship" value="si" data-value="si"/>
                                        <span></span>
                                    </label>
                                    <br>
                                    <label for="">Si</label>
                                </div>
                                <div class="col-md-3 col-xs-3">
                                    <div class="hidden">
                                        <label class="switch">
                                            <input type="radio" required name="scholarship" value="0" data-value="0"/>
                                            <span></span>
                                        </label>
                                        <br>
                                        <label for="">No</label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group color-scholarship hidden scholarship-true">
                                <label class="col-md-3 col-xs-5 control-label">Porcentaje</label>
                                <div class="col-md-5 col-xs-12">
                                    <select multiple name="percentage" id="percentage" class="selectU form-control" data-placeholder="Porcentaje de beca">
                                        <option value="5">5%</option>
                                        <option value="10">10%</option>
                                        <option value="15">15%</option>
                                        <option value="20">20%</option>
                                        <option value="25">25%</option>
                                        <option value="30">30%</option>
                                        <option value="35">35%</option>
                                        <option value="40">40%</option>
                                        <option value="45">45%</option>
                                        <option value="50">50%</option>
                                        <option value="100">100%</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group color-scholarship hidden scholarship-true">
                                <label class="col-md-3 col-xs-5 control-label">Motivo Beca</label>
                                <div class="col-md-5 col-xs-7">
                                    <textarea name="observations-scho" id="observations-scho" class="form-control" rows="5" placeholder="Motivo de la beca."></textarea>
                                </div>
                            </div>
                        <?php endif;?>
                        <div class="form-group">
                            <label class="col-md-3 col-xs-3 control-label">Llamada</label>
                            <div class="col-md-3 col-xs-3">
                                <label class="switch">
                                    <input type="radio" required name="call" checked value="1" data-value="1"/>
                                    <span></span>
                                </label>
                                <br>
                                <label>Efectiva</label>
                            </div>
                            <div class="col-md-3 col-xs-3">
                                <label class="switch">
                                    <input type="radio" required name="call" value="2" data-value="2"/>
                                    <span></span>
                                </label>
                                <br>
                                <label for="">No efectiva</label>
                            </div>
                            <div class="col-md-3 col-xs-3">
                                <label class="switch">
                                    <input id="noMarco" type="radio" name="call" value="3" data-value="3"/>
                                    <span></span>
                                </label>
                                <br>
                                <label for="">No marco</label>
                            </div>
                            <div class="col-md-3 col-xs-3 hidden">
                                <label class="switch">
                                    <input  type="radio" name="call" value="4" data-value="4"/>
                                    <span></span>
                                </label>
                                <br>
                                <label for="">No marco</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-xs-6">
                                <input type="hidden" name="clid" id="cid" value="<?=$client->getClid()?>">
                                <button class="btn btn-primary btn-rounded pull-right" id="submitNote">Guardar nota</button>
                            </div>
                            <div class="col-md-6 col-xs-6">
                                <a href="https://www.google.com/calendar/b/1/render" target="_blank" class="btn btn-info btn-rounded pull-left">Google Calendar</a>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="alert alert-info fade hidden" id="infoA" role="alert">
                                <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                                <strong><span style="font-size: 18px;" class="fa fa-spinner fa-pulse"></span>&nbsp;Guardando información!</strong> espere un momento por favor...
                            </div>
                            <div class="alert alert-success fade hidden" id="successA" role="alert">
                                <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                                <strong>Bien hecho!</strong> Descripción guardada correctamente
                            </div>
                            <div class="alert alert-warning fade hidden" id="warningA" role="alert">
                                <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                                <strong>Disculpe las molestias!</strong> Ha ocurrido un error, intente nuevamente.
                            </div>
                        </div>
                    </div>
                </div>
            </form>

            <div class="panel panel-default tabs">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab1" data-toggle="tab">Contacto con el cliente</a></li>
                    <?php if($client->getEmail()):?>
                        <li><a href="#tab2" data-toggle="tab">Mensaje</a></li>
                    <?php endif;?>
                    <li><a href="#direction" data-toggle="tab">Dirección</a></li>
                    <?php if($client->getStatus()=="INSCRITO"):?>
                        <li><a href="#beca" data-toggle="tab">Beca</a></li>
                    <?php endif;?>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane panel-body active" id="tab1" style="height: 600px">
                        <div id="timeLine" class="timeline timeline-right">

                            <div class="timeline-item timeline-main">
                                <div class="timeline-date">Hoy</div>
                            </div>
                            <div id="today"></div>
                            <?php $date = date("d-m-Y"); $dateFUV = null;?>
                            <?php foreach($followUp as $key=>$values):?>
                                <?php $dateFU = date("d-m-Y",strtotime($values->getDateCreation()));
                                if($date==$dateFU):?>
                                    <div class="timeline-item timeline-item-right">
                                        <div class="timeline-item-info"><?=date("H:i",strtotime($values->getDateCreation()))?></div>
                                        <div class="timeline-item-icon"><span class="fa <?=$icon[$values->getCallStatus()]?>"></span></div>
                                        <div class="tasks ui-sortable">
                                            <div class="task-item <?php echo "task".$status[$values->getCallStatus()]?>">
                                                <div class="task-text ui-sortable-handle"><?=$values->getObservations();?></div>
                                                <div class="task-footer">
                                                    <div class="pull-left"><span class="fa <?=$icon[$values->getCallStatus()]?>"></span>&nbsp;<?=$values->getCallStatus();?></div>
                                                    <div class="pull-right"><span class="fa fa-phone"></span>&nbsp;<?= $call[$values->getCall()]?></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php else:?>
                                    <?php if($dateFU!==$dateFUV):?>
                                        <div class="timeline-item timeline-main">
                                            <div class="timeline-date"><?php
                                                $newDate = explode("-",date("d-m-Y",strtotime($values->getDateCreation())));
                                                echo $newDate[0]."-".$dateSpanish[$newDate[1]]."-".$newDate[2];
                                                ?>
                                            </div>
                                        </div>
                                        <?php $dateFUV = date("d-m-Y",strtotime($values->getDateCreation()));?>
                                        <div class="timeline-item timeline-item-right">
                                            <div class="timeline-item-info"><?=date("H:i",strtotime($values->getDateCreation()))?></div>
                                            <div class="timeline-item-icon"><span class="fa <?=$icon[$values->getCallStatus()]?>"></span></div>
                                            <div class="tasks ui-sortable">
                                                <div class="task-item <?php echo "task".$status[$values->getCallStatus()]?>">
                                                    <div class="task-text ui-sortable-handle"><?=$values->getObservations();?></div>
                                                    <div class="task-footer">
                                                        <div class="pull-left"><span class="fa <?=$icon[$values->getCallStatus()]?>"></span>&nbsp;<?=$values->getCallStatus();?></div>
                                                        <div class="pull-right"><span class="fa fa-phone"></span>&nbsp;<?= $call[$values->getCall()]?></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php else:?>
                                        <div class="timeline-item timeline-item-right">
                                            <div class="timeline-item-info"><?=date("H:i",strtotime($values->getDateCreation()))?></div>
                                            <div class="timeline-item-icon"><span class="fa <?=$icon[$values->getCallStatus()]?>"></span></div>
                                            <div class="tasks ui-sortable">
                                                <div class="task-item <?php echo "task".$status[$values->getCallStatus()]?>">
                                                    <div class="task-text ui-sortable-handle"><?=$values->getObservations();?></div>
                                                    <div class="task-footer">
                                                        <div class="pull-left"><span class="fa <?=$icon[$values->getCallStatus()]?>"></span>&nbsp;<?=$values->getCallStatus();?></div>
                                                        <div class="pull-right"><span class="fa fa-phone"></span>&nbsp;<?= $call[$values->getCall()]?></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endif?>
                                <?php endif;?>
                            <?php endforeach?>
                            <div class="timeline-item timeline-main">
                                <div class="timeline-date"><a href="#"><span class="fa fa-ellipsis-h"></span></a></div>
                            </div>
                        </div>
                    </div>
                    <?php if($client->getEmail()):?>
                        <div class="tab-pane panel-body" id="tab2">
                            <p>Enviar mensaje a usuario</p>

                            <form action="#" method="post" id="formMessage" name="formMessage">
                                <div class="form-group">
                                    <label>E-mail</label>
                                    <input type="email" name="email" id="emailMessage" class="form-control text-lowercase color-black" value="<?=$client->getEmail()?>" readonly>
                                </div>
                                <div class="form-group">
                                    <label>Asunto</label>
                                    <input type="text" name="subject" class="form-control" placeholder="Asunto">
                                </div>
                                <div class="form-group">
                                    <label>Mensaje</label>
                                    <textarea class="form-control ckeditor" name="message" id="messageCK" placeholder="Escribir mensaje" rows="3"></textarea>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Adjuntar Archivos o Imágenes</label>
                                    <!--dropzone-->
                                    <div id="sendMailPersonal" class="dropzone-personalized dz-default dz-message">
                                        <div class="dz-default dz-message">
                                            <div class="alert alert-info" style="cursor: pointer">
                                                <i class="fa fa-files-o" style="font-size: 18px"></i>&nbsp;Clik para subir archivos
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                    <span>Máximo 4 tipo de archivos y no mayores a 5 MB</span>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-12 col-xs-5">
                                        <button type="submit" id="submitMessage" class="btn-big btn btn-primary btn-rounded pull-right">Enviar</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    <?php endif;?>
                    <div class="tab-pane panel-body" id="direction">
                        <div class="panel panel-default form-horizontal">
                            <div class="panel-body">
                                <h3><span class="fa fa-info-circle"></span> Direccion del alumno</h3>
                            </div>
                            <div class="panel-body form-group-separated">
                                <div class="form-group">
                                    <label class="col-md-4 col-xs-5 control-label">Calle</label>
                                    <div class="col-md-8 col-xs-7"><?=$client->getStreetPersonal();?></div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 col-xs-5 control-label">Colonia</label>
                                    <div class="col-md-8 col-xs-7"><?=$client->getColonyPersonal();?></div>
                                </div>
                                <?php if($client->getCpPersonal()):?>
                                    <div class="form-group">
                                        <label class="col-md-4 col-xs-5 control-label">Código Postal</label>
                                        <div class="col-md-8 col-xs-7 line-height-30"><?=$client->getCpPersonal();?></div>
                                    </div>
                                <?php endif;?>
                                <div class="form-group">
                                    <label class="col-md-4 col-xs-5 control-label">Municipio y Estado</label>
                                </div>
                                <?php if($client->getReferencesPersonal()):?>
                                    <div class="form-group">
                                        <label class="col-md-4 col-xs-5 control-label">Referencias</label>
                                        <div class="col-md-8 col-xs-7"><?=$client->getReferencesPersonal()?></div>
                                    </div>
                                <?php endif;?>
                            </div>

                        </div>
                    </div>
                    <?php if($client->getStatus()=="INSCRITO"):?>
                        <div class="tab-pane panel-body" id="beca">
                            <p>Beca</p>
                            <?php if($scholarship):?>
                                <form action="#" method="post" id="formScholarship" name="formScholarship">
                                    <input type="hidden" value="<?=$client->getClid()?>" name="cid_client">
                                    <div class="form-group">
                                        <label>Porcentaje</label>
                                        <select multiple name="percentage" class="selectU form-control" data-placeholder="Porcentaje de beca">
                                            <option <?=$scholarship->getPercentage()=="5"?"selected":"";?> value="5">5%</option>
                                            <option <?=$scholarship->getPercentage()=="10"?"selected":"";?> value="10">10%</option>
                                            <option <?=$scholarship->getPercentage()=="15"?"selected":"";?> value="15">15%</option>
                                            <option <?=$scholarship->getPercentage()=="20"?"selected":"";?> value="20">20%</option>
                                            <option <?=$scholarship->getPercentage()=="25"?"selected":"";?> value="25">25%</option>
                                            <option <?=$scholarship->getPercentage()=="30"?"selected":"";?> value="30">30%</option>
                                            <option <?=$scholarship->getPercentage()=="35"?"selected":"";?> value="35">35%</option>
                                            <option <?=$scholarship->getPercentage()=="40"?"selected":"";?> value="40">40%</option>
                                            <option <?=$scholarship->getPercentage()=="45"?"selected":"";?> value="45">45%</option>
                                            <option <?=$scholarship->getPercentage()=="50"?"selected":"";?> value="50">50%</option>
                                            <option <?=$scholarship->getPercentage()=="100"?"selected":"";?> value="100">100%</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label class="">Motivo Beca</label>
                                        <textarea name="observations-scho" id="observations-scho" class="form-control" rows="5" placeholder="Motivo de la beca."><?=$scholarship->getDescription();?></textarea>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-12 col-xs-5">
                                            <button type="submit" id="submitMessage" class="btn-big btn btn-primary btn-rounded pull-right">Enviar</button>
                                        </div>
                                    </div>
                                </form>
                            <?php else:?>
                                <form action="#" method="post" id="formScholarship" name="formScholarship">
                                    <input type="hidden" value="<?=$client->getClid()?>" name="cid_client">
                                    <div class="form-group">
                                        <label>Porcentaje</label>
                                        <select multiple name="percentage" class="selectU form-control" data-placeholder="Porcentaje de beca">
                                            <option value="5">5%</option>
                                            <option value="10">10%</option>
                                            <option value="15">15%</option>
                                            <option value="20">20%</option>
                                            <option value="25">25%</option>
                                            <option value="30">30%</option>
                                            <option value="35">35%</option>
                                            <option value="40">40%</option>
                                            <option value="45">45%</option>
                                            <option value="50">50%</option>
                                            <option value="100">100%</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label class="">Motivo Beca</label>
                                        <textarea name="observations-scho" id="observations-scho" class="form-control" rows="5" placeholder="Motivo de la beca."></textarea>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-12 col-xs-5">
                                            <button type="submit" id="submitMessage" class="btn-big btn btn-primary btn-rounded pull-right">Enviar</button>
                                        </div>
                                    </div>
                                </form>
                            <?php endif;?>
                        </div>
                    <?php endif;?>
                </div>

            </div>
        </div>
        <div class="col-md-3 hidden-xs hidden-sm">
            <div class="panel panel-default form-horizontal">
                <div class="panel-body">
                    <h3><span class="fa fa-info-circle"></span> Información rápida</h3>
                    <p>Información rápida de este usuario</p>
                </div>
                <div class="panel-body form-group-separated">
                    <div class="form-group">
                        <label class="col-md-4 col-xs-5 control-label">Registro</label>
                        <div class="col-md-8 col-xs-7 line-height-30"><?= date("d-m-Y H:i:s",strtotime($client->getDateInscription()))?></div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 col-xs-5 control-label">Horario I.</label>
                        <div class="col-md-8 col-xs-7 line-height-30"><?= $hourInterest?></div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 col-xs-5 control-label">Turno A.</label>
                        <div class="col-md-8 col-xs-7 line-height-30"><?=$client->getTurn()?></div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 col-xs-5 control-label">Semestre</label>
                        <div class="col-md-8 col-xs-7 line-height-30"><?=$client->getSemester()?></div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 col-xs-5 control-label">Trabaja</label>
                        <div class="col-md-8 col-xs-7 line-height-30"><?=$client->getWorking()==1?"SI":"NO"?></div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<!-- info -->
<div class="message-box message-box-info animated fadeIn" id="message-box-info">
    <div class="mb-container">
        <div class="mb-middle">
            <div class="mb-title"> Enviando información &nbsp; <i class="fa fa-circle-o-notch fa-spin style-fa"></i></div>
            <div class="mb-content">
                <p>Estamos enviando su información, espere un momento por favor.</p>
            </div>
        </div>
    </div>
</div>

<!-- success -->
<div class="message-box message-box-success animated fadeIn" id="message-box-success">
    <div class="mb-container">
        <div class="mb-middle">
            <div class="mb-title"><span class="fa fa-check"></span> Felicidades !!.</div>
            <div class="mb-content">
                <p>Todo ha salido bien.</p>
            </div>
        </div>
    </div>
</div>
<!-- end success -->
<!-- warning -->
<div class="message-box message-box-warning animated fadeIn" id="message-box-warning">
    <div class="mb-container">
        <div class="mb-middle">
            <div class="mb-title"><span class="fa fa-warning"></span> Error</div>
            <div class="mb-content">
                <p>Ha ocurrido un error, inténtelo nuevamente o regrese mas tarde.</p>
            </div>
            <div class="mb-footer">
                <button class="btn btn-default btn-lg pull-right mb-control-close">Cerrar</button>
            </div>
        </div>
    </div>
</div>