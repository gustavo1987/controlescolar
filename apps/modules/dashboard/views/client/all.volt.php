
<div class="page-content-wrap">
    <div class=" container sinpadding">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12 fondo-gris-general">
            <div class="panel panel-success">
                <div class="panel-heading">
                    <h3 class="panel-title">Todos</h3>
                    <div class="col-sm-6 col-xs-12 text-right">
                        <a href="/dashboard/client/admisioncreate"><button class="btn btn-default"><span class="fa fa-plus fa-2x"></span> Crear</button></a>
                    </div>
                </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover table-actions generalDT" data-order="5" data-filter="desc">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Fecha inicial</th>
                                    <th>Nombre</th>
                                    <th>A. Paterno</th>
                                    <th>A. Materno</th>
                                    <th>Teléfono</th>
                                    <th>Email</th>
                                    <th>Interes</th>
                                    <th>Status</th>
                                    <th>Acción</th>
                                </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($students as $key => $values):?>
                                <tr id="<?= $values->getStuid() ?>">
                                    <td style="color:black;font-size: 15px;"><?php echo $key+1;?></td>
                                    <td style="color:black;font-size: 15px;"><?= $values->getDatecreation() ?></td>
                                    <td style="color:black;font-size: 15px;"><?= $values->getName() ?></td>
                                    <td style="color:black;font-size: 15px;"><?= $values->getLastname() ?></td>
                                    <td style="color:black;font-size: 15px;"><?= $values->getSecondname() ?></td>
                                    <td style="color:black;font-size: 15px;"><?= $values->getPhone() ?></td>
                                    <td style="color:black;font-size: 15px;"><?= $values->getEmail() ?></td>
                                    <td style="color:black;font-size: 15px;"><?php if($values->getTitle()=="Maestría en Derecho Electoral"){echo "MDE";}
                                        elseif($values->getTitle()=="Maestría en Derecho Constitucional y Derechos Humanos"){echo "MDCDH";}
                                        elseif($values->getTitle()=="Doctorado en Derecho Electoral"){echo "DDE";}
                                        elseif($values->getTitle()=="Doctorado en Derecho Constitucional y Derechos Humanos"){echo "DDCDH";}?></td>
                                    <td style="color:black;font-size: 15px;">
                                        <?php if($values->getStatus()=="REGISTRO COMPLETO"){
                                            echo "INSCRIPCIÓN";

                                        }
                                        elseif($values->getStatus()=="REGISTRO INCOMPLETO"){
                                            echo "INSCRIPCIÓN INCOMPLETA";
                                        }
                                        else{
                                            echo $values->getStatus();

                                        }?></td>
                                    <td style="color:black;font-size: 15px;">
                                        <!--a title="Editar estudiante" href="<?= $this->url->get('dashboard/client/viewone/') ?><?= $values->getStuid() ?>" class=""><span class="fa fa-edit fa-2x"></span></a-->
                                        <?php if($values->getStatus()=="ACCESO"):?>
                                        <?php else:?>
                                            <a title="Editar estudiante" href="<?= $this->url->get('dashboard/client/editadmision/') ?><?= $values->getStuid() ?>" class=""><span class="fa fa-edit fa-2x"></span></a>&nbsp;
                                        <?php endif;?>
                                    </td>
                                </tr>
                                <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="message-box animated fadeIn" data-sound="alert" id="mb-remove-row">
    <div class="mb-container">
        <div class="mb-middle">
            <div class="mb-title"><span class="fa fa-times"></span> Eliminar <strong>Datos</strong> ?</div>
            <div class="mb-content">
                <p>¿Estas seguro de eliminar esta fila?</p>
                <p>Presione "Si" si esta seguro.</p>
            </div>
            <div class="mb-footer">
                <div class="pull-right">
                    <button class="btn btn-success btn-lg mb-control-yes">Si</button>
                    <button class="btn btn-default btn-lg mb-control-close">No</button>
                </div>
            </div>
        </div>
    </div>
</div>
