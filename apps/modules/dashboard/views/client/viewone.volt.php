<style type="text/css">.page-container .page-content{position: absolute !important;}</style>
<ul class="breadcrumb">
    <li><a href="<?= $this->url->get('dashboard') ?>">Inicio</a></li>
    <li><a href="<?= $this->url->get('dashboard/client/all') ?>">Solicitante</a></li>
    <li class="active"><a href="#">Información</a></li>
</ul>
<div class="page-title">
    <h2><a href="<?= $this->url->get('dashboard/client/all') ?>"><span class="fa fa-arrow-circle-o-left"></span></a> Regresar</h2>
</div>
<div class="page-content-wrap">
    <div class="row">
        <div class="col-md-12">
            <h4 class="text-title">Información</h4>
            <div class="col-md-12 col-xs-12 panel-body form-group-separated">
                    <div class="form-group">
                        <input type="hidden" value="<?=$inscripciones->getStuid()?>" name="clid">
                        <label class="col-md-2 col-sm-2 col-lg-2  col-xs-12 control-label">Nombre <span>*</span></label>
                        <div class="col-md-3 col-xs-12">
                            <input type="text" class="form-control" name="name" id="name" value="<?= $inscripciones->getName() ?>" required placeholder="Nombres">
                        </div>
                        <div class="col-md-3 col-xs-12">
                            <input type="text" class="form-control" name="last_name" id="last_name" value="<?= $inscripciones->getLastName() ?>" required placeholder="Apellido Paterno">
                        </div>
                        <div class="col-md-3 col-xs-12">
                            <input type="text" class="form-control" name="second_name" id="second_name" value="<?= $inscripciones->getSecondName() ?>" placeholder="Apellido Materno">
                        </div>
                    </div>
                    <div class="col-md-12 col-xs-12 col-sm-12 col-lg-12">
                        <div class="col-md-6 col-xs-12 col-sm-6 col-lg-6 form-group">
                            <label class="col-md-4 col-sm-4 col-lg-4 col-xs-12 control-label">Sexo <span>*</span></label>
                            <div class="col-md-8 col-sm-8  col-lg-8 col-xs-12">
                                <select name="sex" id="sex" class="form-control select2">
                                    <option value="">Selecciona el tipo de sexo</option>
                                    <option <?=$inscripciones->getSex()=="F"?"selected":""?> value="F">Femenino</option>
                                    <option <?=$inscripciones->getSex()=="M"?"selected":""?> value="M">Masculino</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 col-xs-12 col-sm-12 col-lg-12">
                        <div class="col-md-6 col-xs-12 col-sm-6 col-lg-6 form-group">
                            <label class="col-md-4 col-sm-4 col-lg-4 col-xs-12 control-label">Email <span>*</span></label>
                            <div class="col-md-8 col-sm-8  col-lg-8 col-xs-12">
                                <input type="email" class="form-control" name="email" data-value="<?= $inscripciones->getEmail() ?>" id="email" value="<?= $inscripciones->getEmail() ?>" required placeholder="Cuenta de correo">
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12 col-sm-6 col-lg-6 form-group">
                            <label class="col-md-4 col-sm-4 col-lg-4 col-xs-12 control-label">Teléfono <span>*</span></label>
                            <div class="col-md-8 col-sm-8  col-lg-8 col-xs-12">
                                <input type="text" class="form-control" minlength="10" maxlength="15" name="tel" id="tel" value="<?= $inscripciones->getPhone() ?>" required placeholder="Numero telefónico" >
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 col-xs-12 col-sm-12 col-lg-12">
                        <div class="col-md-6 col-xs-12 col-sm-6 col-lg-6 form-group billing">
                            <label class="col-md-4 col-sm-4 col-lg-4 col-xs-12 control-label">Calle </label>
                            <div class="col-md-8 col-sm-8  col-lg-8 col-xs-12">
                                <input type="text" class="form-control" name="street" id="street" placeholder="Calle" value="<?= $inscripciones->getCalle() ?>">
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12 col-sm-6 col-lg-6 form-group billing">
                            <label class="col-md-2 col-sm-2 col-xs-12 control-label">Número </label>
                            <div class="col-md-4 col-sm-4 col-xs-12">
                                <input type="text" class="form-control" name="number_int" id="number_int" placeholder="Número Interior" value="<?= $inscripciones->getNumExt() ?>">
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-12">
                                <input type="text" class="form-control" name="number_ext" id="number_ext" placeholder="Número Exterior" value="<?= $inscripciones->getNumInt() ?>">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 col-xs-12 col-sm-12 col-lg-12">
                        <div class="col-md-6 col-xs-12 col-sm-6 col-lg-6 form-group billing">
                            <label class="col-md-4 col-sm-4 col-lg-4  col-xs-12 control-label">Ciudad </label>
                            <div class="col-md-8 col-sm-8  col-lg-8 col-xs-12">
                                <input type="text" class="form-control" name="location" id="location" placeholder="Localidad" value="<?= $inscripciones->getCiudad() ?>">
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12 col-sm-6 col-lg-6 form-group billing">
                            <label class="col-md-4 col-sm-4 col-lg-4  col-xs-12 control-label">Estado<span>*</span></label>
                            <div class="col-md-8 col-sm-8  col-lg-8 col-xs-12">
                                <input type="text" class="form-control" name="estado" id="estado" placeholder="Estado" value="<?= $inscripciones->getEstado() ?>">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 col-xs-12 col-sm-12 col-lg-12">
                        <div class="col-md-6 col-xs-12 col-sm-6 col-lg-6 form-group billing">
                            <label class="col-md-4 col-sm-4 col-lg-4  col-xs-12 control-label">Código Postal </label>
                            <div class="col-md-8 col-sm-8  col-lg-8 col-xs-12">
                                <input type="text" class="form-control" value="<?= $inscripciones->getCp() ?>">
                            </div>
                        </div>
                    </div>
            </div>
        </div>
    </div>
</div>
<div class="message-box message-box-info animated fadeIn" id="message-box-info">
    <div class="mb-container">
        <div class="mb-middle">
            <div class="mb-title"> Actualizando &nbsp; <i class="fa fa-circle-o-notch fa-spin style-fa"></i></div>
            <div class="mb-content">
                <p>Guardando y Actualizando su información, espere un momento por favor.</p>
            </div>
        </div>
    </div>
</div>
<div class="message-box message-box-success animated fadeIn" id="message-box-success">
    <div class="mb-container">
        <div class="mb-middle">
            <div class="mb-title"><span class="fa fa-check"></span> Información actualizada</div>
            <div class="mb-content">
                <p>Sus cambios han sido guardados correctamente, actualizaremos el sitio para reflejar los cambios.</p>
            </div>
        </div>
    </div>
</div>
<div class="message-box message-box-warning animated fadeIn" id="message-box-warning">
    <div class="mb-container">
        <div class="mb-middle">
            <div class="mb-title"><span class="fa fa-warning"></span> Error</div>
            <div class="mb-content">
                <p>Ha ocurrido un error al guardar su información, inténtelo nuevamente o regrese mas tarde.</p>
            </div>
            <div class="mb-footer">
                <button class="btn btn-default btn-lg pull-right mb-control-close">Cerrar</button>
            </div>
        </div>
    </div>
</div>
<div class="message-box message-box-danger animated fadeIn" id="message-box-danger">
    <div class="mb-container">
        <div class="mb-middle">
            <div class="mb-title"><span class="fa fa-times"></span> Cuidado</div>
            <div class="mb-content">
                <p>Usted no puede actualizar su imagen.</p>
            </div>
            <div class="mb-footer">
                <button class="btn btn-default btn-lg pull-right mb-control-close">Cerrar</button>
            </div>
        </div>
    </div>
</div>
<div class="message-box animated fadeIn" data-sound="alert" id="other_products">
    <div class="mb-container">
        <div class="mb-middle">
            <div class="mb-title"><span class="fa fa-sign-out"></span> Alta <strong>Cliente</strong></div>
            <div class="mb-content">
                <p>¿Le gustaría dar de alta otro estudiante?</p>
                <p>Pulse Sí para dar de alta un nuevo estudiante. Pulse No para ir a la lista de estudiantes.</p>
            </div>
            <div class="mb-footer">
                <div class="pull-right">
                    <button class="btn btn-success btn-lg mb-control-close">Si</button>
                    <a href="<?= $this->url->get('dashboard/clients') ?>" class="btn btn-default btn-lg">No</a>
                </div>
            </div>
        </div>
    </div>
</div>