<style type="text/css">
    .page-container .page-content{
        position: absolute !important;
    }
    label.control-label, input.form-control{
        color: black;
        font-size: 15px;
    }
</style>
<div class="page-content-wrap">
    <div class="container sinpadding">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12 fondo-gris-general">
                <div class="col-md-12 col-xs-12 panel-body form-group-separated" style="background-color: white;">
                    <form action="#" method="post" id="editClientadmision" name="editClientadmision" role="form" class="form-horizontal">
                        <div class="form-group">
                            <label class="col-md-2 col-sm-2 col-lg-2  col-xs-12 control-label">Nombre <span>*</span></label>
                            <div class="col-md-3 col-xs-12">
                                <input type="text" class="form-control" name="name" id="name" value="" required placeholder="Nombres">
                            </div>
                            <div class="col-md-3 col-xs-12">
                                <input type="text" class="form-control" name="last_name" id="last_name" value="" required placeholder="Apellido Paterno">
                            </div>
                            <div class="col-md-3 col-xs-12">
                                <input type="text" class="form-control" name="second_name" id="second_name" value="" placeholder="Apellido Materno">
                            </div>
                        </div>
                        <div class="col-md-12 col-xs-12 col-sm-12 col-lg-12">
                            <div class="col-md-6 col-xs-12 col-sm-6 col-lg-6 form-group">
                                <label class="col-md-4 col-sm-4 col-lg-4 col-xs-12 control-label">Posgrado <span>*</span></label>
                                <div class="col-md-8 col-sm-8  col-lg-8 col-xs-12">
                                    <select name="posgrado" id="posgrado" class="form-control select2">
                                        <option value="">Selecciona el posgrado</option>
                                        <option value="38">MDE</option>
                                        <option value="39">MDCDH</option>
                                        <option value="42">DDE</option>
                                        <option value="43">DDCDH</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6 col-xs-12 col-sm-6 col-lg-6 form-group">
                                <label class="col-md-4 col-sm-4 col-lg-4 col-xs-12 control-label">Sexo <span>*</span></label>
                                <div class="col-md-8 col-sm-8  col-lg-8 col-xs-12">
                                    <select name="sex" id="sex" class="form-control select2">
                                        <option value="">Selecciona el tipo de sexo</option>
                                        <option value="F">Femenino</option>
                                        <option value="M">Masculino</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 col-xs-12 col-sm-12 col-lg-12">
                            <div class="col-md-6 col-xs-12 col-sm-6 col-lg-6 form-group">
                                <label class="col-md-4 col-sm-4 col-lg-4 col-xs-12 control-label">Email <span>*</span></label>
                                <div class="col-md-8 col-sm-8  col-lg-8 col-xs-12">
                                    <input type="email" class="form-control" name="email" data-value="" id="email" value="" required placeholder="Cuenta de correo">
                                </div>
                            </div>
                            <div class="col-md-6 col-xs-12 col-sm-6 col-lg-6 form-group">
                                <label class="col-md-4 col-sm-4 col-lg-4 col-xs-12 control-label">Teléfono <span>*</span></label>
                                <div class="col-md-8 col-sm-8  col-lg-8 col-xs-12">
                                    <input type="text" class="form-control" minlength="10" maxlength="15" name="phone" id="phone" value="" required placeholder="Numero telefónico" >
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12 col-xs-12 col-sm-12 col-lg-12">
                            <div class="col-md-6 col-xs-12 col-sm-6 col-lg-6 form-group billing">
                                <label class="col-md-4 col-sm-4 col-lg-4 col-xs-12 control-label">Calle </label>
                                <div class="col-md-8 col-sm-8  col-lg-8 col-xs-12">
                                    <input type="text" class="form-control" name="street" id="street" placeholder="Calle" value="">
                                </div>
                            </div>
                            <div class="col-md-6 col-xs-12 col-sm-6 col-lg-6 form-group billing">
                                <label class="col-md-2 col-sm-2 col-xs-12 control-label">Número </label>
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <input type="text" class="form-control" name="number_int" id="number_int" placeholder="Número Interior" value="">
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <input type="text" class="form-control" name="number_ext" id="number_ext" placeholder="Número Exterior" value="">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 col-xs-12 col-sm-12 col-lg-12">
                            <div class="col-md-6 col-xs-12 col-sm-6 col-lg-6 form-group billing">
                                <label class="col-md-4 col-sm-4 col-lg-4  col-xs-12 control-label">Ciudad </label>
                                <div class="col-md-8 col-sm-8  col-lg-8 col-xs-12">
                                    <input type="text" class="form-control" name="ciudad" id="ciudad" placeholder="Ciudad" value="">
                                </div>
                            </div>
                            <div class="col-md-6 col-xs-12 col-sm-6 col-lg-6 form-group billing">
                                <label class="col-md-4 col-sm-4 col-lg-4  col-xs-12 control-label">Estado<span>*</span></label>
                                <div class="col-md-8 col-sm-8  col-lg-8 col-xs-12">
                                    <input type="text" class="form-control" name="estado" id="estado" placeholder="Estado" value="">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 col-xs-12 col-sm-12 col-lg-12">
                            <div class="col-md-6 col-xs-12 col-sm-6 col-lg-6 form-group billing">
                                <label class="col-md-4 col-sm-4 col-lg-4  col-xs-12 control-label">Código Postal </label>
                                <div class="col-md-8 col-sm-8  col-lg-8 col-xs-12">
                                    <input type="text" name="cp" class="form-control" value="">
                                </div>
                            </div>
                            <div class="col-md-6 col-xs-12 col-sm-6 col-lg-6 form-group billing">
                                <label class="col-md-4 col-sm-4 col-lg-4  col-xs-12 control-label">Periodo </label>
                                <div class="col-md-8 col-sm-8  col-lg-8 col-xs-12">
                                    <select name="periodo" id="periodo" class="form-control select2">
                                        <option value="">Selecciona el periodo</option>
                                        <option value="2019-2020">2019-2020</option>
                                        <option value="2020-2021">2020-2021</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 col-xs-12 col-sm-12 col-lg-12">
                            <div class="col-md-6 col-xs-12 col-sm-6 col-lg-6 form-group billing">
                                <label class="col-md-4 col-sm-4 col-lg-4  col-xs-12 control-label">Nacionalidad </label>
                                <div class="col-md-8 col-sm-8  col-lg-8 col-xs-12">
                                    <input type="text" name="nacionalidad" class="form-control" value="">
                                </div>
                            </div>
                            <div class="col-md-6 col-xs-12 col-sm-6 col-lg-6 form-group billing">
                                <label class="col-md-4 col-sm-4 col-lg-4  col-xs-12 control-label">Documento Acreditación </label>
                                <div class="col-md-8 col-sm-8  col-lg-8 col-xs-12">
                                    <input type="text" name="documento_acreditacion" class="form-control" value="">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 col-xs-12 col-sm-12 col-lg-12">
                            <div class="col-md-6 col-xs-12 col-sm-6 col-lg-6 form-group billing">
                                <label class="col-md-4 col-sm-4 col-lg-4  col-xs-12 control-label">Número de documento </label>
                                <div class="col-md-8 col-sm-8  col-lg-8 col-xs-12">
                                    <input type="text" name="numero_documento_acreditacion" class="form-control" value="">
                                </div>
                            </div>
                            <div class="col-md-6 col-xs-12 col-sm-6 col-lg-6 form-group billing">
                                <label class="col-md-4 col-sm-4 col-lg-4  col-xs-12 control-label">Fecha de Nacimiento </label>
                                <div class="col-md-8 col-sm-8  col-lg-8 col-xs-12">
                                    <input type="text" name="fecha_nacimiento" class="birth_date form-control" value="">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 col-xs-12 col-sm-12 col-lg-12">
                            <div class="col-md-6 col-xs-12 col-sm-6 col-lg-6 form-group billing">
                                <label class="col-md-4 col-sm-4 col-lg-4  col-xs-12 control-label">País de  Nacimiento </label>
                                <div class="col-md-8 col-sm-8  col-lg-8 col-xs-12">
                                    <input type="text" name="pais_nacimiento" class="form-control" value="">
                                </div>
                            </div>
                            <div class="col-md-6 col-xs-12 col-sm-6 col-lg-6 form-group billing">
                                <label class="col-md-4 col-sm-4 col-lg-4  col-xs-12 control-label">Domicilio de Nacimiento </label>
                                <div class="col-md-8 col-sm-8  col-lg-8 col-xs-12">
                                    <input type="text" name="domicilio_nacimiento" class="form-control" value="">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 col-xs-12 col-sm-12 col-lg-12">
                            <div class="col-md-6 col-xs-12 col-sm-6 col-lg-6 form-group billing">
                                <label class="col-md-4 col-sm-4 col-lg-4  col-xs-12 control-label">C.P. de Nacimiento </label>
                                <div class="col-md-8 col-sm-8  col-lg-8 col-xs-12">
                                    <input type="text" name="cp_nacimiento" class="form-control" value="">
                                </div>
                            </div>
                            <div class="col-md-6 col-xs-12 col-sm-6 col-lg-6 form-group billing">
                                <label class="col-md-4 col-sm-4 col-lg-4  col-xs-12 control-label">Ciudad de Nacimiento </label>
                                <div class="col-md-8 col-sm-8  col-lg-8 col-xs-12">
                                    <input type="text" name="ciudad_nacimiento" class="form-control" value="">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 col-xs-12 col-sm-12 col-lg-12">
                            <div class="col-md-6 col-xs-12 col-sm-6 col-lg-6 form-group billing">
                                <label class="col-md-4 col-sm-4 col-lg-4  col-xs-12 control-label">Estado de Nacimiento </label>
                                <div class="col-md-8 col-sm-8  col-lg-8 col-xs-12">
                                    <input type="text" name="estado_nacimiento" class="form-control" value="">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12 col-xs-12 col-sm-12 col-lg-12 form-group billing">
                            <h2 style="text-align: center;">Domicilio de Residencia</h2>
                        </div>

                        <div class="col-md-12 col-xs-12 col-sm-12 col-lg-12">
                            <div class="col-md-6 col-xs-12 col-sm-6 col-lg-6 form-group billing">
                                <label class="col-md-4 col-sm-4 col-lg-4  col-xs-12 control-label">Teléfono Residencia </label>
                                <div class="col-md-8 col-sm-8  col-lg-8 col-xs-12">
                                    <input type="text" name="telefono_residencia" class="form-control" value="">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12 col-xs-12 col-sm-12 col-lg-12 form-group billing">
                            <h2 style="text-align: center;">Estudios previos</h2>
                        </div>
                        <div class="col-md-12 col-xs-12 col-sm-12 col-lg-12">
                            <div class="col-md-6 col-xs-12 col-sm-6 col-lg-6 col-md-6 col-xs-12 col-sm-6 col-lg-6 form-group billing">
                                <label class="col-md-4 col-sm-4 col-lg-4 col-xs-12 control-label"> Universidad </label>
                                <div class="col-md-8 col-sm-8 col-lg-8 col-xs-12">
                                    <input type="text" name="universidad" class="form-control" value="">
                                </div>
                            </div>
                            <div class="col-md-6 col-xs-12 col-sm-6 col-lg-6 col-md-6 col-xs-12 col-sm-6 col-lg-6 form-group billing">
                                <label class="col-md-4 col-sm-4 col-lg-4 col-xs-12 control-label"> País de estudios </label>
                                <div class="col-md-8 col-sm-8  col-lg-8 col-xs-12">
                                    <input type="text" name="pais_estudios" class="form-control" value="">
                                </div>
                            </div>
                        </div>


                        <div class="col-md-12 col-xs-12 col-sm-12 col-lg-12">
                            <div class="col-md-6 col-xs-12 col-sm-6 col-lg-6 col-md-6 col-xs-12 col-sm-6 col-lg-6 form-group billing">
                                <label class="col-md-4 col-sm-4 col-lg-4 col-xs-12 control-label">Nombre del titulo </label>
                                <div class="col-md-8 col-sm-8  col-lg-8 col-xs-12">
                                    <input type="text" name="nombre_titulo" class="form-control" value="">
                                </div>
                            </div>
                            <div class="col-md-6 col-xs-12 col-sm-6 col-lg-6 col-md-6 col-xs-12 col-sm-6 col-lg-6 form-group billing">
                                <label class="col-md-4 col-sm-4 col-lg-4 col-xs-12 control-label">Tipo del titulo </label>
                                <div class="col-md-8 col-sm-8  col-lg-8 col-xs-12">
                                    <input type="text" name="tipo_titulo" class="form-control" value="">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12 col-xs-12 col-sm-12 col-lg-12">

                            <div class="col-md-6 col-xs-12 col-sm-6 col-lg-6 col-md-6 col-xs-12 col-sm-6 col-lg-6 form-group billing">
                                <label class="col-md-4 col-sm-4 col-lg-4 col-xs-12 control-label">Estado del titulo </label>
                                <div class="col-md-8 col-sm-8  col-lg-8 col-xs-12">
                                    <input type="text" name="estado_estudios" class="form-control" value="">
                                </div>
                            </div>
                            <div class="col-md-6 col-xs-12 col-sm-6 col-lg-6 col-md-6 col-xs-12 col-sm-6 col-lg-6 form-group billing">
                                <label class="col-md-4 col-sm-4 col-lg-4 col-xs-12 control-label">Fecha de finalización </label>
                                <div class="col-md-8 col-sm-8  col-lg-8 col-xs-12">
                                    <input type="text" name="fecha_finalizacion" class="date-end-students form-control" value="">
                                </div>
                            </div>
                            <div class="col-md-6 col-xs-12 col-sm-6 col-lg-6 col-md-6 col-xs-12 col-sm-6 col-lg-6 form-group billing">
                                <label class="col-md-4 col-sm-4 col-lg-4 col-xs-12 control-label">Año cursado </label>
                                <div class="col-md-8 col-sm-8  col-lg-8 col-xs-12">
                                    <input type="text" name="anio_cursado" class="form-control" value="">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 col-xs-12 col-sm-12 col-lg-12 form-group billing">
                            <h2 style="text-align: center;">Documentación</h2>
                        </div>
                        <div class="col-md-6 col-xs-12 col-sm-6 col-lg-6 form-group billing">
                            <label class="col-md-3 col-sm-3 col-lg-3 col-xs-12 control-label">Recibo</label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                            <div id="recibo" class="dropzone dz-default dz-message documentload" data-id="recibos" style="min-height:80px;">
                              <div class="dz-default dz-message col-md-12" style="top: 0% !important;">
                                <h2 style="line-height: 0.8;cursor: pointer;padding-top: 4%;">
                                  <i class="fas fa-download col-md-4" style="font-size: 45px;cursor: pointer;"></i>
                                  <div class="col-md-8">
                                    <p class="col-md-12" style="font-size: 20px;margin:0;cursor: pointer;padding:0;">Arrastra un archivo</p>
                                    <i style="font-size: 14px;cursor: pointer;padding:0;" class="col-md-12">o haz click para seleccionar manualmente</i>
                                    <input type="hidden" value="" name="recibo" id="input-recibo-receipt" class="col-md-12">
                                  </div>
                                </h2>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-6 col-xs-12 col-sm-6 col-lg-6 form-group billing">
                            <label class="col-md-3 col-sm-3 col-lg-3 col-xs-12 control-label">Fotografía</label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                            <div id="photo" class="dropzone dz-default dz-message documentload" data-id="photo" style="min-height:80px;">
                              <div class="dz-default dz-message col-md-12" style="top: 0% !important;">
                                <h2 style="line-height: 0.8;cursor: pointer;padding-top: 4%;">
                                  <i class="fas fa-download col-md-4" style="font-size: 45px;cursor: pointer;"></i>
                                  <div class="col-md-8">
                                    <p class="col-md-12" style="font-size: 20px;margin:0;cursor: pointer;padding:0;">Arrastra un archivo</p>
                                    <i style="font-size: 14px;cursor: pointer;padding:0;" class="col-md-12">o haz click para seleccionar manualmente</i>
                                    <input type="hidden" value="" name="photo" id="input-photo-receipt" class="col-md-12">
                                  </div>
                                </h2>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-6 col-xs-12 col-sm-6 col-lg-6 form-group billing">
                            <label class="col-md-3 col-sm-3 col-lg-3 col-xs-12 control-label">Acta</label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                            <div id="acta" class="dropzone dz-default dz-message documentload" data-id="acta" style="min-height:80px;">
                              <div class="dz-default dz-message col-md-12" style="top: 0% !important;">
                                <h2 style="line-height: 0.8;cursor: pointer;padding-top: 4%;">
                                  <i class="fas fa-download col-md-4" style="font-size: 45px;cursor: pointer;"></i>
                                  <div class="col-md-8">
                                    <p class="col-md-12" style="font-size: 20px;margin:0;cursor: pointer;padding:0;">Arrastra un archivo</p>
                                    <i style="font-size: 14px;cursor: pointer;padding:0;" class="col-md-12">o haz click para seleccionar manualmente</i>
                                    <input type="hidden" value="" name="acta" id="input-acta-receipt" class="col-md-12">
                                  </div>
                                </h2>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-6 col-xs-12 col-sm-6 col-lg-6 form-group billing">
                            <label class="col-md-3 col-sm-3 col-lg-3 col-xs-12 control-label">Titulo</label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                            <div id="titulo" class="dropzone dz-default dz-message documentload" data-id="titulo" style="min-height:80px;">
                              <div class="dz-default dz-message col-md-12" style="top: 0% !important;">
                                <h2 style="line-height: 0.8;cursor: pointer;padding-top: 4%;">
                                  <i class="fas fa-download col-md-4" style="font-size: 45px;cursor: pointer;"></i>
                                  <div class="col-md-8">
                                    <p class="col-md-12" style="font-size: 20px;margin:0;cursor: pointer;padding:0;">Arrastra un archivo</p>
                                    <i style="font-size: 14px;cursor: pointer;padding:0;" class="col-md-12">o haz click para seleccionar manualmente</i>
                                    <input type="hidden" value="" name="titulo" id="input-titulo-receipt" class="col-md-12">
                                  </div>
                                </h2>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-6 col-xs-12 col-sm-6 col-lg-6 form-group billing">
                            <label class="col-md-3 col-sm-3 col-lg-3 col-xs-12 control-label">Identificación</label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                            <div id="identificacion" class="dropzone dz-default dz-message documentload" data-id="identificacion" style="min-height:80px;">
                              <div class="dz-default dz-message col-md-12" style="top: 0% !important;">
                                <h2 style="line-height: 0.8;cursor: pointer;padding-top: 4%;">
                                  <i class="fas fa-download col-md-4" style="font-size: 45px;cursor: pointer;"></i>
                                  <div class="col-md-8">
                                    <p class="col-md-12" style="font-size: 20px;margin:0;cursor: pointer;padding:0;">Arrastra un archivo</p>
                                    <i style="font-size: 14px;cursor: pointer;padding:0;" class="col-md-12">o haz click para seleccionar manualmente</i>
                                    <input type="hidden" value="" name="identificacion" id="input-identificacion-receipt">
                                  </div>
                                </h2>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-6 col-xs-12 col-sm-6 col-lg-6 form-group billing">
                            <label class="col-md-3 col-sm-3 col-lg-3 col-xs-12 control-label">curriculum</label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                            <div id="curriculum" class="dropzone dz-default dz-message documentload" data-id="curriculum" style="min-height:80px;">
                              <div class="dz-default dz-message col-md-12" style="top: 0% !important;">
                                <h2 style="line-height: 0.8;cursor: pointer;padding-top: 4%;">
                                  <i class="fas fa-download col-md-4" style="font-size: 45px;cursor: pointer;"></i>
                                  <div class="col-md-8">
                                    <p class="col-md-12" style="font-size: 20px;margin:0;cursor: pointer;padding:0;">Arrastra un archivo</p>
                                    <i style="font-size: 14px;cursor: pointer;padding:0;" class="col-md-12">o haz click para seleccionar manualmente</i>
                                    <input type="hidden" value="" name="curriculum" id="input-curriculum-receipt">
                                  </div>
                                </h2>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-6 col-xs-12 col-sm-6 col-lg-6 form-group billing">
                           <label class="col-md-3 col-sm-3 col-lg-3 col-xs-12 control-label">CURP</label>
                           <div class="col-md-9 col-sm-9 col-xs-12">
                              <div id="curp" class="dropzone dz-default dz-message documentload" data-id="curp" style="min-height:80px;">
                                 <div class="dz-default dz-message col-md-12" style="top: 0% !important;">
                                    <h2 style="line-height: 0.8;cursor: pointer;padding-top: 4%;">
                                       <i class="fas fa-download col-md-4" style="font-size: 45px;cursor: pointer;"></i>
                                       <div class="col-md-8">
                                          <p class="col-md-12" style="font-size: 20px;margin:0;cursor: pointer;padding:0;">Arrastra un archivo</p>
                                          <i style="font-size: 14px;cursor: pointer;padding:0;" class="col-md-12">o haz click para seleccionar manualmente</i>
                                          <input type="hidden" value="" name="curp" id="input-curp-receipt">
                                       </div>
                                    </h2>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="col-md-6 col-xs-12 col-sm-6 col-lg-6 form-group billing">
                          <label class="col-md-3 col-sm-3 col-lg-3 col-xs-12 control-label">Certificado</label>
                          <div class="col-md-9 col-sm-9 col-xs-12">
                            <div id="certificado" class="dropzone dz-default dz-message documentload" data-id="certificado" style="min-height:80px;">
                              <div class="dz-default dz-message col-md-12" style="top: 0% !important;">
                                <h2 style="line-height: 0.8;cursor: pointer;padding-top: 4%;">
                                  <i class="fas fa-download col-md-4" style="font-size: 45px;cursor: pointer;"></i>
                                  <div class="col-md-8">
                                    <p class="col-md-12" style="font-size: 20px;margin:0;cursor: pointer;padding:0;">Arrastra un archivo</p>
                                    <i style="font-size: 14px;cursor: pointer;padding:0;" class="col-md-12">o haz click para seleccionar manualmente</i>
                                    <input type="hidden" value="" name="certificado" id="input-certificado-receipt" class="col-md-12">
                                  </div>
                                </h2>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-6 col-xs-12 col-sm-6 col-lg-6 form-group billing">
                          <label class="col-md-3 col-sm-3 col-lg-3 col-xs-12 control-label">Cédula</label>
                          <div class="col-md-9 col-sm-9 col-xs-12">
                            <div id="cedula" class="dropzone dz-default dz-message documentload" data-id="cedula" style="min-height:80px;">
                              <div class="dz-default dz-message col-md-12" style="top: 0% !important;">
                                <h2 style="line-height: 0.8;cursor: pointer;">
                                  <i class="fas fa-download col-md-4" style="font-size: 45px;cursor: pointer;"></i>
                                  <div class="col-md-8" style="padding:0;">
                                    <p class="col-md-12" style="font-size: 20px;margin:0;cursor: pointer;padding:0;">Arrastra un archivo</p>
                                    <i style="font-size: 14px;cursor: pointer;padding:0;" class="col-md-12">o haz click para seleccionar manualmente</i>
                                    <input type="hidden" value="" name="cedula" id="input-cedula-receipt" class="col-md-12">
                                  </div>
                                </h2>
                              </div>
                            </div>
                          </div>
                        </div-->
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-2">
                                <input type="submit" class="btn btn-success" value="Mandar pago"/>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="message-box message-box-info animated fadeIn" id="message-box-info">
    <div class="mb-container">
        <div class="mb-middle">
            <div class="mb-title"> Actualizando &nbsp; <i class="fa fa-circle-o-notch fa-spin style-fa"></i></div>
            <div class="mb-content">
                <p>Guardando y Actualizando su información, espere un momento por favor.</p>
            </div>
        </div>
    </div>
</div>
<!-- end info -->
<!-- success -->
<div class="message-box message-box-success animated fadeIn" id="message-box-success">
    <div class="mb-container">
        <div class="mb-middle">
            <div class="mb-title"><span class="fa fa-check"></span> Información actualizada</div>
            <div class="mb-content">
                <p>Sus cambios han sido guardados correctamente, actualizaremos el sitio para reflejar los cambios.</p>
            </div>
        </div>
    </div>
</div>
<!-- end success -->
<!-- warning -->
<div class="message-box message-box-warning animated fadeIn" id="message-box-warning">
    <div class="mb-container">
        <div class="mb-middle">
            <div class="mb-title"><span class="fa fa-warning"></span> Error</div>
            <div class="mb-content">
                <p>Ha ocurrido un error al guardar su información, inténtelo nuevamente o regrese mas tarde.</p>
            </div>
            <div class="mb-footer">
                <button class="btn btn-default btn-lg pull-right mb-control-close">Cerrar</button>
            </div>
        </div>
    </div>
</div>
<!-- end danger -->
<div class="message-box message-box-danger animated fadeIn" id="message-box-danger">
    <div class="mb-container">
        <div class="mb-middle">
            <div class="mb-title"><span class="fa fa-times"></span> Cuidado</div>
            <div class="mb-content">
                <p>Usted no puede actualizar su imagen.</p>
            </div>
            <div class="mb-footer">
                <button class="btn btn-default btn-lg pull-right mb-control-close">Cerrar</button>
            </div>
        </div>
    </div>
</div>
<div class="message-box animated fadeIn" data-sound="alert" id="other_products">
    <div class="mb-container">
        <div class="mb-middle">
            <div class="mb-title"><span class="fa fa-sign-out"></span> Alta <strong>Cliente</strong></div>
            <div class="mb-content">
                <p>¿Le gustaría dar de alta otro estudiante?</p>
                <p>Pulse Sí para dar de alta un nuevo estudiante. Pulse No para ir a la lista de estudiantes.</p>
            </div>
            <div class="mb-footer">
                <div class="pull-right">
                    <button class="btn btn-success btn-lg mb-control-close">Si</button>
                    <a href="{{url('dashboard/clients')}}" class="btn btn-default btn-lg">No</a>
                </div>
            </div>
        </div>
    </div>
</div>