<div class="page-title">
    <div class="col-sm-6 text-left">
        <h2><a href="<?= $this->url->get('dashboard') ?>"><span class="fa fa-arrow-circle-o-left"></span></a> Menú principal</h2>
    </div>
</div>
<div class="page-content-wrap">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-success">
                <div class="panel-heading">
                    <h3 class="panel-title">Estatus de recibo de pago</h3>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover table-actions generalDT" data-order="5" data-filter="desc">
                            <thead>
                            <tr>
                                <th>Número&nbsp;Cliente</th>
                                <th>Nombre</th>
                                <th>Apellido Paterno</th>
                                <th>Apellido Materno</th>
                                <th>Estado</th>
                                <th>Posgrado</th>
                                <th>Opciones</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($students as $values) { ?>
                            <tr id="<?= $values->getStuid() ?>">
                                <td><?= $values->getStuid() ?></td>
                                <td><?= $values->getName() ?></td>
                                <td><?= $values->getLastname() ?></td>
                                <td><?= $values->getSecondname() ?></td>
                                <td><?php echo $values->getStatus()=="ADMITIDO"?"Sin Recibo":"Recibo Cargado";?></td>
                                <td><?= $values->getTitle() ?></td>
                                <td>
                                    <?php if($values->getStatus()=="ADMITIDO"):?>
                                    <?php else:?>
                                    <a title="Editar estudiante" href="<?= $this->url->get('dashboard/client/editrecibo/') ?><?= $values->getStuid() ?>" class=""><span class="fa fa-edit fa-2x"></span></a>&nbsp;
                                    <?php endif;?>
                                </td>
                            </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="message-box animated fadeIn" data-sound="alert" id="mb-remove-row">
    <div class="mb-container">
        <div class="mb-middle">
            <div class="mb-title"><span class="fa fa-times"></span> Eliminar <strong>Datos</strong> ?</div>
            <div class="mb-content">
                <p>¿Estas seguro de eliminar esta fila?</p>
                <p>Presione "Si" si esta seguro.</p>
            </div>
            <div class="mb-footer">
                <div class="pull-right">
                    <button class="btn btn-success btn-lg mb-control-yes">Si</button>
                    <button class="btn btn-default btn-lg mb-control-close">No</button>
                </div>
            </div>
        </div>
    </div>
</div>