
<div class="page-content-wrap">
    <div class="row">
        <div class="col-md-12 cuadro">
            <h4 class="text-title">Alta estudiante</h4>
            <div class="col-md-12 col-xs-12 panel-body form-group-separated">
                <form action="#" method="post" id="newClient" name="newClient" role="form" class="form-horizontal">
                    <div class="form-group">
                        <label class="col-md-2 col-xs-12 control-label">Nombre <span>*</span></label>
                        <div class="col-md-3 col-xs-12">
                            <input type="text" class="form-control" name="name" id="name" required placeholder="Nombres">
                        </div>
                        <div class="col-md-3 col-xs-12">
                            <input type="text" class="form-control" name="last_name" id="last_name" required placeholder="Apellido Paterno">
                        </div>
                        <div class="col-md-3 col-xs-12">
                            <input type="text" class="form-control" name="second_name" id="second_name" placeholder="Apellido Materno">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 col-xs-12 control-label">Sexo <span>*</span></label>
                        <div class="col-md-8 col-sm-6 col-xs-12">
                            <select name="sex" id="sex" class="form-control select2">
                                <option value="">Selecciona el tipo de sexo</option>
                                <option value="F">Femenino</option>
                                <option value="M">Masculino</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 col-xs-12 control-label">Fecha Nacimiento<span>*</span></label>
                        <div class="col-md-3 col-xs-12">
                            <input type="text" class="form-control birth_date" name="birth_date" id="birth_date" placeholder="Fecha nacimiento">
                        </div>
                        <div class="col-md-3 col-xs-12">
                            <input type="text" class="form-control" name="age" id="age" placeholder="Edad" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 col-xs-12 control-label">Email <span>*</span></label>
                        <div class="col-md-8 col-sm-6 col-xs-12">
                            <input type="email" class="form-control" name="email" id="email" required ng-model="registerData.email" placeholder="Cuenta de correo">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 col-xs-12 control-label">Teléfono <span>*</span></label>
                        <div class="col-md-8 col-sm-6 col-xs-12">
                            <input type="text" class="form-control" minlength="10" maxlength="15" name="tel" id="tel" required placeholder="Numero telefónico" >
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 col-xs-12 control-label">Facturación</label>
                        <div class="col-md-2 col-sm-6 col-xs-12">
                            <input type="radio" name="iva" value="mx">&nbsp;Si
                        </div>
                        <div class="col-md-2 col-sm-6 col-xs-12">
                            <input type="radio"  name="iva" checked value="et">&nbsp;No
                        </div>
                    </div>
                    <div class="form-group billing hide">
                        <label class="col-md-2 col-xs-12 control-label">RFC</label>
                        <div class="col-md-8 col-sm-6 col-xs-12">
                            <input type="text" class="form-control" name="rfc" id="rfc" ng-maxlength="15" ng-minlength="10" placeholder="RFC">
                        </div>
                    </div>
                    <div class="form-group billing hide">
                        <label class="col-md-2 col-xs-12 control-label">Razón Social*</label>
                        <div class="col-md-8 col-sm-6 col-xs-12">
                            <input type="text" placeholder="Razón social" class="form-control" name="business_name" id="business_name" >
                        </div>
                    </div>
                    <div class="form-group billing hide">
                        <label class="col-md-2 col-xs-12 control-label">Calle*</label>
                        <div class="col-md-8 col-sm-6 col-xs-12">
                            <input type="text" class="form-control" name="street" id="street" placeholder="Calle">
                        </div>
                    </div>
                    <div class="form-group billing hide">
                        <label class="col-md-2 col-sm-2 col-xs-12 control-label">Número </label>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <input type="text" class="form-control" name="number_int" id="number_int" placeholder="Número Interior">
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <input type="text" class="form-control" name="number_ext" id="number_ext" placeholder="Número Exterior">
                        </div>
                    </div>
                    <div class="form-group billing hide">
                        <label class="col-md-2 col-xs-12 control-label">Colonia </label>
                        <div class="col-md-8 col-sm-6 col-xs-12">
                            <input type="text"  class="form-control" name="colony" id="colony" placeholder="Colonia">
                        </div>
                    </div>
                    <div class="form-group billing hide">
                        <label class="col-md-2 col-xs-12 control-label">Localidad </label>
                        <div class="col-md-8 col-sm-6 col-xs-12">
                            <input type="text" class="form-control" name="location" id="location" placeholder="Localidad">
                        </div>
                    </div>
                    <div class="form-group billing hide">
                        <label class="col-md-2 col-xs-12 control-label">Estado y Municipio <span>*</span></label>
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <select name="state" id="state" class="form-control select2" data-placeholder="Selecciona el estado de procedencia." multiple>
                                <?php foreach($estados as $key=>$val):?>
                                    <option value="<?=$key;?>"><?=$val;?></option>
                                <?php endforeach?>
                            </select>
                        </div>
                        <div class="col-md-4 col-sm-6 col-xs-12 hidden">
                            <select name="mpid_fiscal" id="mpid_fiscal" class="form-control select2" data-placeholder="Selecciona el municipio de procedencia." multiple>
                            </select>
                        </div>
                    </div>
                    <div class="form-group billing hide">
                        <label class="col-md-2 col-xs-12 control-label">Código Postal </label>
                        <div class="col-md-8 col-sm-6 col-xs-12">
                            <input type="text" class="form-control" name="postal_code" id="postal_code" placeholder="Código Postal">
                        </div>
                    </div>
                    <div class="form-group billing hide">
                        <label class="col-md-2 col-xs-12 control-label">Email Facturación </label>
                        <div class="col-md-8 col-sm-6 col-xs-12">
                            <input type="email" placeholder="Email Facturación" class="form-control" name="email_fiscal" id="email_fiscal" >
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="form-group">
                        <label class="col-md-2 col-xs-12 control-label">Postgrado <span>*</span></label>
                        <div class="col-md-8 col-sm-6 col-xs-12">
                            <select name="sex" id="sex" class="form-control select2">
                                <option value="">Selecciona el postgrado</option>
                                <?php foreach ($carreras as $key => $value):?>
                                <option value="<?=$value->getBrid();?>"><?=$value->getName();?></option>
                                <?php endforeach;?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 col-xs-12 control-label">Estatus</label>
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <select id="status" name="status" class="form-control selectU" required multiple>
                                <option selected value="ACTIVO">ACTIVO</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-2">
                            <input type="submit" class="btn btn-success" value="Guardar"/>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- info -->
<div class="message-box message-box-info animated fadeIn" id="message-box-info">
    <div class="mb-container">
        <div class="mb-middle">
            <div class="mb-title"> Actualizando &nbsp; <i class="fa fa-circle-o-notch fa-spin style-fa"></i></div>
            <div class="mb-content">
                <p>Guardando y Actualizando su información, espere un momento por favor.</p>
            </div>
        </div>
    </div>
</div>
<!-- end info -->
<!-- success -->
<div class="message-box message-box-success animated fadeIn" id="message-box-success">
    <div class="mb-container">
        <div class="mb-middle">
            <div class="mb-title"><span class="fa fa-check"></span> Información actualizada</div>
            <div class="mb-content">
                <p>Sus cambios han sido guardados correctamente, actualizaremos el sitio para reflejar los cambios.</p>
            </div>
        </div>
    </div>
</div>
<!-- end success -->
<!-- warning -->
<div class="message-box message-box-warning animated fadeIn" id="message-box-warning">
    <div class="mb-container">
        <div class="mb-middle">
            <div class="mb-title"><span class="fa fa-warning"></span> Error</div>
            <div class="mb-content">
                <p>Ha ocurrido un error al guardar su información, inténtelo nuevamente o regrese mas tarde.</p>
            </div>
            <div class="mb-footer">
                <button class="btn btn-default btn-lg pull-right mb-control-close">Cerrar</button>
            </div>
        </div>
    </div>
</div>
<!-- end danger -->
<div class="message-box message-box-danger animated fadeIn" id="message-box-danger">
    <div class="mb-container">
        <div class="mb-middle">
            <div class="mb-title"><span class="fa fa-times"></span> Cuidado</div>
            <div class="mb-content">
                <p>Usted no puede actualizar su imagen.</p>
            </div>
            <div class="mb-footer">
                <button class="btn btn-default btn-lg pull-right mb-control-close">Cerrar</button>
            </div>
        </div>
    </div>
</div>
<div class="message-box animated fadeIn" data-sound="alert" id="other_products">
    <div class="mb-container">
        <div class="mb-middle">
            <div class="mb-title"><span class="fa fa-sign-out"></span> Alta <strong>Estudiante</strong></div>
            <div class="mb-content">
                <p>¿Le gustaría dar de alta otro estudiante?</p>
                <p>Pulse Sí para dar de alta un nuevo estudiante. Pulse No para ir a la lista de estudiantes.</p>
            </div>
            <div class="mb-footer">
                <div class="pull-right">
                    <button class="btn btn-success btn-lg mb-control-close">Si</button>
                    <a href="{{url('dashboard/clients')}}" class="btn btn-default btn-lg">No</a>
                </div>
            </div>
        </div>
    </div>
</div>