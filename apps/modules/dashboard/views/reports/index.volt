<ul class="breadcrumb">
    <li><a href="/dashboard">Inicio</a></li>
    <li><a href="#">Clases</a></li>
</ul>
<div class="page-title">
    <div class="col-sm-12 text-left">
        <h2>Historial de clases por: <strong>usuario, fechas, estatus</strong>.</h2>
    </div>
</div>
<div class="page-content-wrap" id="sales">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-success">
                <div class="panel-heading">
                    <div class="col-sm-3"><h3 class="panel-title">Estatus de clases:</h3></div>
                    <div class="col-sm-2"><span class="fa fa-cube fa-2x text-warning"></span>&nbsp;Apartados</div>
                    <div class="col-sm-2"><span class="fa fa-cube fa-2x text-info"></span>&nbsp;Ventas</div>
                    <div class="col-sm-2"><span class="fa fa-cube fa-2x text-danger"></span>&nbsp;Cancelados</div>
                </div>
                <div class="panel-body">
                    <div class="col-sm-12">
                        <form action="#" class="form-horizontal" id="formReports">
                            <div class="col-sm-3 padding-0">
                                <div class="form-group">
                                    <label class="col-xs-12 control-label text-left">Maestro</label>
                                    <div class="col-xs-12">
                                        <select name="user" id="user" class="form-control">
                                            <option selected value="general">General</option>
                                            <?php foreach($users as $values):?>
                                                <option value="<?=$values->getUid()?>"><?=$values->getName()." ".$values->getLastName()?></option>
                                            <?php endforeach?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3 padding-0">
                                <div class="form-group">
                                    <label class="col-xs-12 control-label text-left">Fecha inicio</label>
                                    <div class="col-xs-12">
                                        <input type="text" name="start_date" value="<?=date('d/m/Y')?>" id="start_date" class="form-control getDatepicker" placeholder="D/M/A">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3 padding-0">
                                <div class="form-group">
                                    <label class="col-xs-12 control-label text-left">Fecha corte</label>
                                    <div class="col-xs-12">
                                        <input type="text" name="cutoff_date" placeholder="D/M/A" value="" id="cutoff_date" class="form-control getDatepicker" data-container="body" data-placement="top" data-content="La fecha de corte no puede ser menor a la fecha de inicio.">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3 padding-0">
                                <div class="form-group">
                                    <label class="col-xs-12 control-label text-left">Estatus</label>
                                    <div class="col-xs-12">
                                        <select name="status" id="status" class="form-control">
                                            <option value="TODAS" selected>Todas</option>
                                            <!--option value="SERVICIO">Servicios</option-->
                                            <!--option value="APARTADO">Apartados</option-->
                                            <option value="CANCELADO">Cancelados</option>
                                            <option value="ACTIVO">Realizadas</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3 padding-0">
                                <div class="form-group">
                                    <div class="col-sm-6 col-xs-12">
                                        <br>
                                        <input type="submit" value="Filtrar" class="btn btn-success" id="submitSales" data-container="body" data-placement="top" data-content="No se han encontrado resultados.">
                                    </div>
                                    <div class="col-sm-6 col-xs-12">
                                        <br>
                                        <a class="btn btn-info" id="exportReports">Exportar</a>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="clearfix"></div>
                    <br>

                    <div class="clearfix"></div>
                    <br>
                    <div class="table-responsive">
                        <table id="tableReports" class="table table-bordered table-hover table-actions dataTable" data-order="0" data-filter="desc">
                            <thead>
                            <tr>
                                <!--th>Tipo</th-->
                                <th>Fecha&nbsp;Venta</th>
                                <th>Atendio</th>
                                <th>Nombre</th>
                                <th>Precio</th>
                                <th>Cant.</th>
                                <th>Desct.</th>
                                <th>Total</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                        </div>
                        <!--div class="col-md-6">
                            <h4>Monto total</h4>
                            <table class="table table-striped">
                                <tbody><tr>
                                    <td width="200"><strong>Subtotal:</strong></td><td class="text-right" id="subtotal"></td>
                                </tr>
                                <tr id="iva" data-value="16">
                                    <td><strong>IVA 16%:</strong></td><td class="text-right" id="iva_general"></td>
                                </tr>
                                <tr class="total">
                                    <td>Total de las ventas</td><td class="text-right" id="amount_to_pay" data-value=""></td>
                                </tr>
                                </tbody>
                            </table>
                        </div-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
