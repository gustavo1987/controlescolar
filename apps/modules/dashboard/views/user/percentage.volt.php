<!-- START BREADCRUMB -->
<ul class="breadcrumb">
    <li><a href="<?php echo $this->url->get('dashboard'); ?>">Inicio</a></li>
    <li><a href="<?php echo $this->url->get('dashboard/users'); ?>">Usuarios</a></li>
    <li class="active">Porcentajes</li>
</ul>
<!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">
    <div class="row">
        <div class="col-md-12">
            <div class="page-content-wrap">
                <div class="row">
                    <div class="col-md-12">
                        <h4 class="text-title">Porcentaje Usuario <strong><?=$user->getName()." ".$user->getLastName()?></strong></h4>
                        <div class="col-md-12 col-xs-12 panel-body form-group-separated">
                            <form action="#" method="post" id="percentage" role="form" class="form-horizontal">
                                <span class="hide" id="uid" data-uid="<?=$user->getUid()?>"></span>
                                <?php foreach($sub_services as $values):?>
                                    <div class="form-group">
                                        <span class="col-md-2"><?=$values->getNameservice()?>:</span>
                                        <label class="col-md-2 col-xs-12 control-label text-left"><?=$values->getNameSubservice()?></label>
                                        <div class="col-md-6 col-xs-12">
                                            <input readonly type="number" max="100" min="0" class="form-control percentage" data-value="<?=$values->getPercentage()?>" data-usid="<?=$values->getUsid()?>" placeholder="Porcentaje" value="<?=$values->getPercentage()?>"/>
                                        </div>
                                    </div>
                                <?php endforeach;?>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- PAGE CONTENT WRAPPER -->

