<style>
.banner-desktop {
    margin: 0px 5.5% 20px 2.5% !important;
    width: 80% !important;
}
.banner-aqua {
    background-image: url(/dash/images/ide.png);
    background-size: cover;
    -moz-background-size: cover;
    -webkit-background-size: cover;
    -o-background-size: cover;
}
.banner-aqua{
    background-color: grey;
}
.squareBanner {
    border-radius: 4px;
    float: left;
    padding: 40px 15px 40px 15px;
    text-align: center;
    margin: 0px 1.5% 20px 1.5%;
    background-color: #000;
    color: white;
    height: 185px !important;
}
    .mt-5 {
        margin-top: 5rem !important;
    }
    /*Ibedel*/
    .lista1{
        text-align: center;
        position: relative;
    }
    ul li.student .alista1{
        width: 350px;
        height: 250px;
    }
    ul li.student .alista1::after{
        content: "";
        position: absolute;
        top: 250px;
        left: -11px;
        height: 20px;
        width: 100%;
        background: #83a8ec;
        transition: .5s;
        transform: rotate(0deg) skewX(-45deg);
    }
    ul li.student .alista1.aula::after{
            background: #737175;
    }
    ul li.student a.aula:before{
            background: #807c84 !important;
    }


    .ilista1{
        font-size: 150px !important;
    }
    .position1{
        position: relative;
        display: block;
        font-size: 1.5em;
        font-weight: bold;
        margin-top: .5em;
    }
    /*Ibedel*/
    body{
        margin: 0;
        padding: 0;
        background-color: #ccc;
    }
    /*Se cambió*/
    ul{
        position: relative;
        margin: 0;
        padding: 0;
        display: flex;
        justify-content: center;
    }
    /*Se cambió*/
    ul li{
        list-style: none;
        margin: 0 40px;
    }
    ul li.student .fa, ul li.student .fas{
        font-size: 80px;
        display: block;
        color: #fff;
        line-height: 80px;
        transition: .5s;
        padding-top: 20px;
    }
    ul li.student a{
        position: relative;
        display: block;
        width: 240px;
        height: 150px;
        background-color: #fff;
        text-align: center;
        /*transform: perspective(100px) rotate(0deg) skew(25deg) translate(0,0);*/
        transform: perspective(100px) rotate(0deg) skew(0deg) translate(0,0);
        transition: .5s;
        box-shadow: -20px 20px 10px rgb(0, 0, 0, 0.5);
    }
    ul li.student a::before{
        content: "";
        position: absolute;
        top: 10px;
        left: -20px;
        height: 100%;
        width: 20px;
        background: #b1b1b1;
        transition: .5s;
        transform: rotate(0deg) skewY(-45deg);
    }
    ul li.student a::after{
        content: "";
        position: absolute;
        top: 150px;
        left: -11px;
        height: 20px;
        width: 100%;
        background: #b1b1b1;
        transition: .5s;
        transform: rotate(0deg) skewX(-45deg);
    }
    ul li.student a:hover{
        /*transform: perspective(1000px) rotate(0deg) skew(25deg) translate(20px, -20px);*/
        transform: perspective(1000px) rotate(0deg) skew(0deg) translate(20px, -20px);
        box-shadow: -50px 43px 36px rgba(105, 49, 105, 0.5);
    }
    ul li.lista1.student a:hover{
        /*transform: perspective(1000px) rotate(0deg) skew(25deg) translate(20px, -20px);*/
        transform: perspective(1000px) rotate(0deg) skew(0deg) translate(20px, -20px);
        box-shadow: -50px 43px 36px rgba(101, 98, 101, 0.5);
    }


    ul li.student:hover .fa{
        color: #fff;
    }
    ul li.student a{
        background: #727175;
    }
    ul li.student a:before{
        background: #805ea5;
    }
    ul li.student a:after{
        background: #4d326a;
    }
    ul li.student:hover:nth-child(1) a{
        background: #737175;
    }

    ul li.student:hover:nth-child(1) a:before{
        background: #7967a4;
    }

    

    

    ul li.student:hover:nth-child(1) a.alista1.aula:after{
        background: #737175;
    }
    ul li.student:hover:nth-child(1) a:after{
        background: #483a69;
    }
    ul li.student:hover:nth-child(2) a{
        background: #55acee;
    }
    ul li.student:hover:nth-child(2) a:before{
        background: #4184b7;
    }
    ul li.student:hover:nth-child(2) a:after{
        background: #4d9fde;
    }
    ul li.student:hover:nth-child(3) a{
        background: #dd4b39;
    }

    ul li.student:hover:nth-child(3) a:before{
        background: #c13929;
    }
    ul li.student:hover:nth-child(3) a:after{
        background: #e83322;
    }

    ul li.student:hover:nth-child(4) a{
        background: #0077B5;
    }

    ul li.student:hover:nth-child(4) a:before{
        background: #036aa0;
    }
    ul li.student:hover:nth-child(4) a:after{
        background: #0d82bf;
    }

    ul li.student:hover:nth-child(5) a{
        background: linear-gradient(#400080, transparent), linear-gradient(200deg, #d047d1, #ff0000, #ffff00);
    }

    ul li.student:hover:nth-child(5) a:before{
        background: linear-gradient(#400080, transparent), linear-gradient(200deg, #d047d1, #ff0000, #ffff00);
    }
    ul li.student:hover:nth-child(5) a:after{
        background: linear-gradient(#400080, transparent), linear-gradient(200deg, #d047d1, #ff0000, #ffff00);
    }


    ul li.student a.red{
        background: #f1808d;
    }
    ul li.student a.red:before{
        background: #f3597a;
    }
    ul li.student a.red:after{
        background: #fd6c9f;
    }
    ul li.student:hover a.red{
        background: #ec3c50;
    }
    ul li.student:hover a.red:before{
        background: #881f36;
    }
    ul li.student:hover a.red:after{
        background: #bb1d54;
    }
    li.profile-tab-title, .correo-institucional{
        background-color: #483a69b3;
        margin: 0 2px;
        border: 3px #352f44 solid;
    }
</style>


<span class="hide" id="uid" data-uid="<?= $user->getUid() ?>"></span>
<!-- <div class="page-title">
    <div class="col-sm-6 text-left">
        <h2><a href="<?= $this->url->get('dashboard') ?>"><span class="fa fa-arrow-circle-o-left"></span> Menú principal</a></h2>
    </div>
</div> -->

<div class="page-content-wrap">
        <div class="row">
          <div class="col-md-2 col-sm-2 col-lg-2 col-xs-12">


            <div class="panel panel-default">
                <div class="panel-body profile" style="background: url('<?= $this->url->get('dash/assets/images/gallery/music-4.jpg') ?>') center center no-repeat;">
                    <div class="profile-image">
                        <img src="/dash/assets/images/users/thumbnail/<?= $user->getPhoto() ?>" alt="Nadia Ali"/>
                    </div>
                    <div class="profile-data">
                        <div class="profile-data-name" style="color: #000;"><?= $user->getUsername() ?></div>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <a href="#" class="list-group-item text-center"><?= $user->getName() . ' ' . $user->getLastName() . ' ' . $user->getSecondName() ?></a>
                        </div>
                        <div class="col-md-12">
                            <a href="#" data-toggle="modal" data-target="#modal_basic" class="list-group-item"><span class="fa fa-ellipsis-h"></span> Cambiar contraseña</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel panel-default tabs">
                <ul class="nav nav-tabs" role="tablist">
                    <li class="profile-tab-title active"><a href="#tab-second" role="tab" data-toggle="tab" aria-expanded="false">Datos personales</a></li>
                    <li class="profile-tab-title"><a href="#tab-third" role="tab" data-toggle="tab" aria-expanded="false">Imagen de perfil</a></li>
                    <li class="correo-institucional"><a target="_blank" href="<?= $this->url->get('https://hl322.hosteurope.es:2096/') ?>" style="background-color: #00ffff00;">Correo</a></li>
                </ul>
                <div class="panel-body tab-content">
                    <div class="tab-pane active" id="tab-second">
                        <h4 class="text-title">Actualizar datos personales</h4>
                        <div class="col-md-12 col-xs-12 panel-body form-group-separated">
                            <form action="#" method="post" id="updateProfile" role="form" class="form-horizontal">
                                <div class="form-group">
                                    <label class="col-md-4 col-xs-12 control-label tab-profile">Nombre</label>
                                    <div class="col-md-8 col-xs-12 sin-margen-up-down">
                                        <input type="text" class="form-control" placeholder="Nombre" name="name" value="<?= $user->getName() ?>" required/>
                                        <input class="hidden" name="email" value="<?= $user->getEmail() ?>"/>
                                        <input class="hidden" name="username" value="<?= $user->getUsername() ?>"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 col-xs-12 control-label tab-profile">Apellido paterno</label>
                                    <div class="col-md-8 col-xs-12 sin-margen-up-down">
                                        <input type="text" class="form-control" placeholder="Apellido Paterno" name="last_name" value="<?= $user->getLastName() ?>" required/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 col-xs-12 control-label tab-profile">Apellido Materno</label>
                                    <div class="col-md-8 col-xs-12 sin-margen-up-down">
                                        <input type="text" class="form-control" placeholder="Apellido Materno" name="second_name" value="<?= $user->getSecondName() ?>"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 col-xs-12 control-label tab-profile">Sexo</label>
                                    <div class="col-md-8 col-xs-12 sin-margen-up-down">
                                        <select id="sex" name="sex" class="form-control" required>
                                            <option <?php echo $user->getSex()=="M"?"selected":""; ?> value="M">MASCULINO</option>
                                            <option <?php echo $user->getSex()=="F"?"selected":""; ?> value="F">FEMENINO</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 col-xs-12 control-label tab-profile">Teléfono</label>
                                    <div class="col-md-8 col-xs-12 sin-margen-up-down">
                                        <input type="phone" pattern="[789][0-9]{9}" class="form-control" placeholder="Teléfonono" name="phone" value="<?= $user->getPhone() ?>" required/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-8 col-md-offset-2">
                                        <input type="submit" class="btn btn-success" value="Actualizar"/>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab-third">
                        <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <h3><span class="fa fa-download"></span> Imagen perfil</h3>
                                    <p>Actualizar imagen de perfil</p>
                                    <form action="#" class="dropzone dropzone-mini" id="dropzone-user-edit" name="dropzone-user-edit" rol="form">
                                        <input type="hidden" value="<?= $user->getPhoto() ?>" id="user-image" name="photo"/>
                                    </form>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <button class="btn btn-success" id="save-image">Actualizar</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>



    <div class="col-md-8 col-sm-8 col-lg-8 fondo-gris-general">
        <div class="page-content-wrap" style="background-color: #25396880;">
    <div class=" container sinpadding">
        <div class="row">
            <?php if($rol=="ADMIN"):?>
             <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12 cuadro" style="background: rgba(85, 86, 97, 0.4);padding-top: 5%;">
                <div class="container">
                <div class="col-md-12 mt-3">
                    <div class="row">
                        <div class="col-md-3"> 
                            <ul>
                                <li class="student"><a href="/dashboard/client/all" style="text-decoration: none;"><i class="fas fa-tasks" aria-hidden="true"></i><p class="position1" style="color: white;">Proceso de admision</p></a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-6"> 
                            <ul>
                                <li class="student"><a href="/dashboard/estudiantes" style="text-decoration: none;"><i class="fas fa-id-card" aria-hidden="true"></i><p class="position1" style="color: white;">Estudiantes</p></a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-3"> 
                            <ul>
                                <li class="student"><a href="/dashboard/clases/pos" style="text-decoration: none;"><i class="fas fa-chalkboard-teacher" aria-hidden="true"></i><p class="position1" style="color: white;">Clases</p></a>

                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 mt-3" style="padding-top: 3.5%;">
                    <div class="row">
                        <div class="col-md-3 mt-5"> 
                            <ul>
                                <li class="student"><a href="/dashboard/comunidad-general" style="text-decoration: none;"><i class="fas fa-comments" aria-hidden="true"></i><p class="position1" style="color: white;">Comunidad</p></a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-6"> 
                            <ul>
                                <li class="lista1 student"><a href="/dashboard/sales/day" style="background: #483a69;text-decoration: none;" class="alista1 aula"><i class="fas fa-user-graduate ilista1" aria-hidden="true" style="padding-top: .2em;"></i><p class="position1" style="color: white;float: none;top:2.5em;">Aulas Virtuales</p></a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-3 mt-5"> 
                            <ul>
                                <li class="student"><a href="/dashboard/student/calificaciones" style="text-decoration: none;"><i class="fas fa-file-signature" aria-hidden="true"></i><p class="position1" style="color: white;">Progreso Académico</p></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 mt-3" style="padding-top: 4%; margin-bottom: 3em;">
                    <div class="row">
                        <div class="col-md-3"> 
                            <ul>
                                <li class="student"><a href="/dashboard/controlescolar/pos" style="text-decoration: none;"><i class="fas fa-hand-holding-usd" aria-hidden="true"></i><p class="position1" style="color: white;">Pagos</p></a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-6"> 
                            <ul>
                                <li class="student"><a href="<?php echo $user->getStatuspagos()==0?'https://ideiberoamericasp.ebookcentral.proquest.com/libCentral':'';?>" target="_blank" style="text-decoration: none;"><i class="fas fa-book-reader" aria-hidden="true"></i><p class="position1" style="color: white;">Biblioteca</p></a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-3"> 
                            <ul>
                                <li class="student"><a href="/dashboard/servicios-escolares" style="text-decoration: none;"><i class="fas fa-id-card" aria-hidden="true"></i><p class="position1" style="color: white;">Servicios Escolares</p></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php elseif($rol=="STUDENT"):?>
            <!--p><?php echo $notification;?></p-->
        <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12 cuadro" style="background: rgba(85, 86, 97, 0.4);padding-top: 5%;">
            <div class="container">
            <div class="col-md-12 mt-3">
                <div class="row">
                    <div class="col-md-3"> 
                        <ul>
                            <li class="student"><a href="/dashboard/student/aula" style="text-decoration: none;"><i class="fas fa-tasks" aria-hidden="true"></i><p class="position1" style="color: white;">Plan de Estudio</p></a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-6"> 
                        <ul>
                            <li class="student"><a href="/dashboard/student/pagos" style="text-decoration: none;"><i class="fas fa-hand-holding-usd" aria-hidden="true"></i><p class="position1" style="color: white;">Pagos</p></a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-3"> 
                        <ul>
                            <li class="student"><a href="/dashboard/sitios-de-interes" style="text-decoration: none;"><i class="fas fa-link" aria-hidden="true"></i><p class="position1" style="color: white;">Sitios de Interes</p></a>
                                
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-12 mt-3" style="padding-top: 4%;">
                <div class="row">
                    <div class="col-md-3 mt-5"> 
                        <ul>
                            <li class="student"><a href="/dashboard/calendario" style="text-decoration: none;"><i class="fa fa-calendar" aria-hidden="true"></i><p class="position1" style="color: white;">Calendario</p></a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-6">
                        <ul>
                            <?php if($user->getStatuspagos()==0):?>
                            <li class="lista1 student"><a href="/dashboard/student/clase" style="background: #4d2168; text-decoration: none;" class="alista1 aula"><i class="fas fa-user-graduate ilista1" aria-hidden="true" style="padding-top: .2em;"></i><p class="position1" style="color: white;float: none;top:1.5em;">Aula Virtual</p></a>
                            </li>
                            <?php else:?>
                            <li class="lista1 student"><a style="background: #4d2168; text-decoration: none;" class="alista1 aula block-user"><i class="fas fa-user-graduate ilista1" aria-hidden="true" style="padding-top: .2em;"></i><p class="position1" style="color: white;float: none;top:1.5em;">Aula Virtual</p></a>
                            </li>
                            <?php endif;?>
                        </ul>
                    </div>
                    <div class="col-md-3 mt-5"> 
                        <ul>
                            <li class="student"><a href="/dashboard/student/calificaciones" style="text-decoration: none;"><i class="fas fa-file-signature" aria-hidden="true"></i><p class="position1" style="color: white;">Progreso Académico</p></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-12 mt-3" style="padding-top: 4%; margin-bottom: 3em;">
                <div class="row">
                    <div class="col-md-3"> 
                        <ul>
                            <?php if($user->getStatuspagos()==0):?>
                            <li class="student"><a href="https://ebookcentral.proquest.com/lib/ideiberoamericasp" target="_blank" style="text-decoration: none;"><i class="fas fa-book-reader" aria-hidden="true"></i><p class="position1" style="color: white;">Biblioteca Digital</p></a>
                            </li>
                            <?php else:?>
                            <li class="student"><a target="_blank" style="text-decoration: none;"><i class="fas fa-book-reader block-user" aria-hidden="true"></i><p class="position1" style="color: white;">Biblioteca Digital</p></a>
                            </li>
                            <?php endif;?>
                        </ul>
                    </div>
                    <div class="col-md-6"> 
                        <ul>
                            <li class="student"><a href="/dashboard/comunidad-general" style="text-decoration: none;"><i class="fas fa-comments" aria-hidden="true"></i><p class="position1" style="color: white;">Comunidad</p></a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-3"> 
                        <ul>
                            <?php if($user->getStatuspagos()==0):?>
                            <li class="student"><a target="_blank" href="<?= $this->url->get('https://hl322.hosteurope.es:2096/') ?>" style="text-decoration: none;"><i class="fas fa-envelope" aria-hidden="true"></i><p class="position1" style="color: white;">Correo Electronico</p></a>
                            </li>
                            <?php else:?>
                            <li class="student"><a target="_blank" style="text-decoration: none;"><i class="fas fa-envelope block-user" aria-hidden="true"></i><p class="position1" style="color: white;">Correo Electronico</p></a>
                            </li>
                            <?php endif;?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        </div>
        <?php elseif ($rol=="TEACHER"):?>
            <!--a id="loading_message"><p style="position: absolute;top: 25%;right: 5%; color: white; font-size: 30px;"><i class="fas fa-inbox"><?php echo " ".$notification." mensajes";?></i></p></a-->
        <div id="append_pasticipantes"></div>
        <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12 cuadro" style="background: rgba(85, 86, 97, 0.4);padding-top: 5%;">
            <div class="container">
                <div class="col-md-12 mt-3" style="padding-top: 4%;">
                    <div class="row">
                        <div class="col-md-6"> 
                            <ul>
                                <li class="student"><a href="/dashboard/calendario"><i class="fa fa-calendar" aria-hidden="true"></i><p class="position1" style="color: white;">Calendario</p></a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-6"> 
                            <ul>
                                <li class="student"><a href="/dashboard/comunidad-general"><i class="fas fa-comments" aria-hidden="true"></i><p class="position1" style="color: white;">Comunidad</p></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 mt-3" style="padding-top: 2%;">
                    <div class="row">
                        <div class="col-md-6 col-md-offset-3"> 
                            <ul>
                                <li class="lista1 student"><a href="/dashboard/student/clase" style="background: #483a69;text-decoration: none;" class="alista1 aula"><i class="fas fa-user-graduate ilista1" aria-hidden="true" style="padding-top: 55px;"></i><p class="position1" style="color: white;float: none;top:2.5em;">Aula Virtual</p></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 mt-3" style="padding-top: 2%;">
                    <div class="row">
                        <div class="col-md-6"> 
                            <ul>
                                <li class="student"><a href="https://ebookcentral.proquest.com/lib/ideiberoamericasp" target="_blank"><i class="fas fa-book-reader" aria-hidden="true"></i><p class="position1" style="color: white;">Biblioteca Digital</p></a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-6"> 
                            <ul>
                                <li class="student"><a target="_blank" href="<?= $this->url->get('https://hl322.hosteurope.es:2096/') ?>" style="text-decoration: none;"><i class="fas fa-envelope" aria-hidden="true"></i><p class="position1" style="color: white;">Correo Electronico</p></a>
                                </li>   
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php endif;?>
        <hr>
    </div>
</div>
    </div>
    </div>
</div>
<div class="modal" id="modal_basic" tabindex="-1" role="dialog" aria-labelledby="defModalHead" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Cerrar</span></button>
                <h4 class="modal-title" id="defModalHead">Cambiar contraseña</h4>
            </div>
            <form id="updatePassword" action="#" method="post" rol="form" class="form-horizontal">
                <div class="modal-body">
                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-3">
                            <div class="input-group">
                                <span class="input-group-addon"><span class="fa fa-unlock-alt"></span></span>
                                <input type="password" class="form-control" placeholder="Nueva contraseña" name="password" required/>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-3">
                            <div class="input-group">
                                <span class="input-group-addon"><span class="fa fa-unlock-alt"></span></span>
                                <input type="password" class="form-control" placeholder="Repetir contraseña" name="confirmPassword" required/>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">

                    </div>
                    <div class="form-group fade hide" id="container-messages">
                        <div class="alert alert-warning fade hide" role="alert" id="email-incorrect">
                            <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Cerrar</span></button>
                            <strong>Email incorrecto.</strong>
                        </div>
                        <div class="alert alert-warning fade hide" role="alert" id="password-incorrect">
                            <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                            <strong>Contraseña incorrecta.</strong>
                        </div>
                        <div class="alert alert-warning fade hide" role="alert" id="all-incorrect">
                            <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                            <strong>Email o contraseña incorrecto</strong>
                        </div>
                        <!--End Mesages-->
                    </div>
                </div>
                <div class="modal-footer">
                        <button type="button" class="btn btn-warning" data-dismiss="modal">Cerrar</button>
                        <button class="btn btn-info">Cambiar</button>
                    <!--Messages-->
                    <i id="session-loading" class="fa fa-spinner fa-spin fade hide" style="font-size: 22px;"></i>
                </div>
            </form>
        </div>
    </div>
</div>
<!--END MODALS-->

<!-- info -->
<div class="message-box message-box-info animated fadeIn" id="message-box-info">
    <div class="mb-container">
        <div class="mb-middle">
            <div class="mb-title"> Actualizando &nbsp; <i class="fa fa-circle-o-notch fa-spin style-fa"></i></div>
            <div class="mb-content">
                <p>Guardando y Actualizando su información, espere un momento por favor.</p>
            </div>
        </div>
    </div>
</div>
<!-- end info -->
<!-- success -->
<div class="message-box message-box-success animated fadeIn" id="message-box-success">
    <div class="mb-container">
        <div class="mb-middle">
            <div class="mb-title"><span class="fa fa-check"></span> Información actualizada</div>
            <div class="mb-content">
                <p>Sus cambios han sido guardados correctamente, actualizaremos el sitio para reflejar los cambios.</p>
            </div>
        </div>
    </div>
</div>
<!-- end success -->
<!-- warning -->
<div class="message-box message-box-warning animated fadeIn" id="message-box-warning">
    <div class="mb-container">
        <div class="mb-middle">
            <div class="mb-title"><span class="fa fa-warning"></span> Error</div>
            <div class="mb-content">
                <p>Ha ocurrido un error al guardar su información, inténtelo nuevamente o regrese mas tarde.</p>
            </div>
            <div class="mb-footer">
                <button class="btn btn-default btn-lg pull-right mb-control-close">Cerrar</button>
            </div>
        </div>
    </div>
</div>
<!-- end danger -->
<div class="message-box message-box-danger animated fadeIn" id="message-box-danger">
    <div class="mb-container">
        <div class="mb-middle">
            <div class="mb-title"><span class="fa fa-times"></span> Cuidado</div>
            <div class="mb-content">
                <p>Usted no puede actualizar su imagen.</p>
            </div>
            <div class="mb-footer">
                <button class="btn btn-default btn-lg pull-right mb-control-close">Cerrar</button>
            </div>
        </div>
    </div>
</div>