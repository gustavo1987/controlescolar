<?php if(count($users)==0):?>
    <div class="page-content-wrap">
        <div class="row">
            <div class="col-md-6">
                <h2 class="text-title">Actualmente no hay ningun usuario activo</h2>
            </div>
            <div class="col-md-6">
                <h4 class="text-title text-right"><a class="btn btn-success" href="<?= $this->url->get('dashboard/user/new-user') ?>">Nuevo usuario</a></h4>
            </div>
        </div>
    </div>
<?php else :?>
    <div class="page-content-wrap">

        <div class="row">
            <div class="col-md-6">
                <h4 class="text-title">Usuarios Activos</h4>
            </div>
            <div class="col-md-6">
                <h4 class="text-right"><a class="btn btn-success" href="<?= $this->url->get('dashboard/user/new-user') ?>">Nuevo usuario</a></h4>
            </div>
        </div>

        <div class="row" id="manzory">
            <?php foreach ($users as $user) { ?>
            <div class="col-md-3">
                <div class="panel panel-default">
                    <div class="panel-body profile" style="background: url('<?= $this->url->get('dash/assets/images/gallery/music-4.jpg') ?>') center center no-repeat;">
                        <div class="profile-image">
                            <img src="/dash/assets/images/users/thumbnail/<?= $user->getPhoto() ?>" alt="Imagen Perfil"/>
                        </div>
                        <div class="profile-data">
                            <div class="profile-data-name" style="color: #000;"><?= $user->getUsername() ?></div>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                                <a href="#" class="list-group-item text-center"><?= $user->getName() . ' ' . $user->getLastName() . ' ' . $user->getSecondName() ?></a>
                            </div>
                        </div>
                    </div>
                    <div class="panel-body list-group border-bottom">
                        <a href="#" class="list-group-item active"><span class="fa fa-bar-chart-o"></span> Actividad</a>
                        <a href="#" class="list-group-item"><span class="fa fa-percent"></span> Porcentaje de comisión
                        <a href="#" class="list-group-item"><span class="fa fa-users"></span> Estatus
                            <?php switch($user->getStatus()){
                                case "ACTIVE": echo '<span class="badge badge-info">ACTIVO</span>';break;
                                case "INACTIVE": echo '<span class="badge badge-warning">INACTIVO</span>';break;
                                case "LOCKED": echo '<span class="badge badge-danger">BLOQUEADO</span>';break;
                            }?>
                        </a>
                        <a href="<?= $this->url->get('dashboard/user/edit/' . $user->getUid()) ?>" class="list-group-item"><span class="fa fa-cog"></span> Administración editar</a>
                    </div>
                </div>
            </div>
            <?php } ?>
        </div>

    </div>
<?php endif;?>