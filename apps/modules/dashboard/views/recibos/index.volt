<div class="page-content-wrap">
    <div class="container sinpadding">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12 fondo-gris-general">
                
                <?php if($auth["rol"]=="STUDENT"):?>
                <div class="panel panel-success" style="background-color: #ffffff54;">
                    <div class="panel-heading text-right">
                        <h3 class="panel-title">Pagos</h3>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover table-actions generalDT" data-order="5" data-filter="desc">
                                <thead>
                                <tr>
                                    <?php foreach($pagos as $key => $values):?>
                                    <?php $vence=date("d-m-Y", strtotime($values->getVencimiento()))?>
                                    <th><?php echo $values->getName()?><br><?php if($values->getName()=="MENSUALIDAD"){echo date("M", strtotime($values->getVencimiento()));}?><br><?php echo "Vence:".$vence;?></th>
                                    <?php endforeach;?>
                                </tr>
                                </thead>
                                <tbody>
                                <?php if($statuspago=="ACTIVO"):?>

                                    <tr>

                                    <?php foreach($pagos as $key => $values):?>
                                    <?php $dateC= $values->getDateCreation(); $newDate = date("d-m-Y", strtotime($dateC));?>
                                        <td><?php if($values->getStatus()=="PAGADO"){
                                            echo '<i class="fas fa-check-circle" style="color:green;"></i>';
                                        }
                                        elseif($values->getStatus()=="ADEUDA"){
                                            if($today>strtotime($values->getVencimiento())){
                                                echo '<i class="fas fa-times-circle" style="color:red;"></i>';
                                            }
                                            else{
                                                echo '<i class="far fa-clock" style="color:yellow;"></i>';
                                            }
                                        }?></td>

                                    <?php endforeach; ?>
                                    </tr>
                                <?php else:?>
                                    <h4>Sin Pagos.</h4>
                                <?php endif;?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <?php endif;?>
            </div>
        </div>
    </div>
</div>