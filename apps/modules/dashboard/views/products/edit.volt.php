<ul class="breadcrumb">
    <li><a href="<?= $this->url->get('dashboard') ?>">Inicio</a></li>
    <?php $estatus= $product->getStatus();
            if($estatus=="ACTIVE"):?>
        <li><a href="<?= $this->url->get('dashboard/products') ?>">Materias</a></li>
    <?php else :?>
        <li><a href="<?= $this->url->get('dashboard/products/inactive') ?>">Materias inactivas</a></li>
    <?php endif;?>
    <li class="active"><a href="#">Editar Materia</a></li>
</ul>
<div class="page-title">
</div>
<div class="page-content-wrap">
    <div class="row">
        <div class="col-md-12">
            <h4 class="text-title">Editar Materia</h4>
            <div class="col-md-12 col-xs-12 panel-body form-group-separated">
                <form action="#" method="post" id="editProduct" name="editProduct" role="form" class="form-horizontal">
                    <input type="hidden" name="pid" id="pid" value="<?= $product->getPid() ?>">
                    <div class="form-group">
                        <label class="col-md-2 col-xs-12 control-label">Nombre <span>*</span></label>
                        <div class="col-md-4 col-xs-12">
                            <input type="text" class="form-control" name="name" id="name" value="<?= $product->getName() ?>" required placeholder="Nombre" >
                        </div>
                        <div class="col-md-4 col-xs-12">
                            <input type="text" class="form-control" name="permalink" id="permalink" value="<?= $product->getPermalink() ?>" required placeholder="Permalink" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 col-xs-12 control-label">Descripción <span>*</span></label>
                        <div class="col-md-8 col-xs-12">
                            <textarea type="text" class="form-control" name="description" id="description" required placeholder="Descripción"><?= $product->getDescription() ?></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 col-xs-12 control-label">Código</label>
                        <div class="col-md-8 col-sm-6 col-xs-12">
                            <input type="text" class="form-control" name="ticket" id="ticket" value="<?= $product->getTicket() ?>" required placeholder="Código">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 col-xs-12 control-label">Credito <span>*</span></label>
                        <div class="col-md-8 col-sm-6 col-xs-12">
                            <input type="number" min="0" max="100000" class="form-control" name="sale_price" id="sale_price" value="<?= $product->getSalePrice() ?>" required placeholder="0" >
                        </div>
                    </div>
                    <div class="form-group hidden">
                        <label class="col-md-2 col-xs-12 control-label">Versión <span>*</span></label>
                        <div class="col-md-8 col-sm-6 col-xs-12">
                            <select name="measure" id="measure" class="form-control" required>
                                <option value="">Elige la versión del producto</option>
                                <option <?=$product->getMeasure()=="mL"?"selected":""?> value="mL">Mililitros</option>
                                <option <?=$product->getMeasure()=="L"?"selected":""?> value="L">Litro</option>
                                <option <?=$product->getMeasure()=="PZ"?"selected":""?> value="PZ">Pieza</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group hidden">
                        <label class="col-md-2 col-xs-12 control-label">Cantidad <span>*</span> <i class="fa fa-info-circle"  data-toggle="tooltip" data-placement="top" title="" data-original-title="Cantidad del producto expresada en miliLitros ejemplo 1 Litro = 1000 mililitros."></i></label>
                        <div class="col-md-8 col-sm-6 col-xs-12">
                            <input type="number" min="0" max="1000000" value="0" class="form-control" name="quantity" id="quantity" value="<?= $product->getQuantity() ?>" placeholder="0" required>
                        </div>
                    </div>
                    <div class="form-group hidden">
                        <label class="col-md-2 col-xs-12 control-label">Porcentaje <span>*</span> <i class="fa fa-info"  data-toggle="tooltip" data-placement="top" title="" data-original-title="Porcentaje de ganancia que se otorgará al vender este producto."></i></label>
                        <div class="col-md-8 col-sm-6 col-xs-12">
                            <input type="number" min="0" max="10000" class="form-control" name="percentage" id="percentage" value="0" placeholder="0" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 col-xs-12 control-label">Grado académico <span>*</span></label>
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <select name="brid" id="brid" class="form-control">
                                <option value="">Seleccionar grado académico</option>
                                <?php foreach($brid as $key=>$val):?>
                                    <option <?=$product->getBrid()==$key?"selected":""?> value="<?=$key;?>"><?=$val;?></option>
                                <?php endforeach?>
                            </select>
                        </div>
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <span class="btn btn-success btn-condensed" id="newElement"><i class="fa fa-plus"></i>&nbsp;Grado Académico</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 col-xs-12 control-label">Estatus<span>*</span></label>
                        <div class="col-md-8 col-sm-6 col-xs-12">
                            <select name="status" id="status" class="form-control">
                                <option <?=$product->getStatus()=='ACTIVE'?"selected":""?> value="ACTIVE">ACTIVO</option>
                                <option <?=$product->getStatus()=='INACTIVE'?"selected":""?> value="INACTIVE">INACTIVO</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-2">
                            <input type="submit" class="btn btn-success" value="Actualizar"/>
                        </div>
                    </div>
                    <div class="clearfiz"></div>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="modal" id="modalElement" tabindex="-1" role="dialog" aria-labelledby="defModalHead" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Cerrar</span></button>
                <h4 class="modal-title" id="defModalHead"></h4>
            </div>
            <form id="formElement" action="#" method="post" class="form-horizontal">
                <input type="hidden"  id="brid" name="brid">
                <div class="modal-body">
                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-3">
                            <input type="text" class="form-control" placeholder="Nombre" name="nameM" id="nameM" required/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-3">
                            <input type="text" class="form-control" placeholder="Permalink" name="permalinkM" id="permalinkM" required readonly/>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Cerrar</button>
                    <input type="submit" class="btn btn-info btn-submit-element" value="Guardar">
                    <i id="session-loading" class="fa fa-spinner fa-spin fade hide" style="font-size: 22px;"></i>
                </div>
            </form>
        </div>
    </div>
</div>