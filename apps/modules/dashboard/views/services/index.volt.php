<!-- START BREADCRUMB -->
<ul class="breadcrumb">
    <li><a href="<?php echo $this->url->get('dashboard'); ?>">Inicio</a></li>
    <li class="active">Servicios</li>
</ul>
<!-- END BREADCRUMB -->
<!-- PAGE TITLE -->
<div class="page-title text-right">
    <span class="btn btn-success btn-condensed" id="newService"><i class="fa fa-plus"></i>&nbsp;Servicio</span>
</div>
<!-- END PAGE TITLE -->
<!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">
    <div class="row">
        <div class="col-md-12">
            <!-- START DEFAULT DATATABLE -->
            <div class="panel panel-success">
                <div class="panel-heading">
                    <h3 class="panel-title">Servicios</h3>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Id</th>
                                <th>Nombre</th>
                                <th>Opciones</th>
                            </tr>
                            </thead>
                            <tbody id="tableTbody">
                            <?php foreach($services as $values):?>
                                <?php $servid = $values->getServid()?>
                                <tr class="<?=$servid?>">
                                    <td><?=$servid?></td>
                                    <td class="name"><?=$values->getName()?></td>
                                    <td data-id="<?=$servid?>">
                                        <span data-name="<?=$values->getName()?>" data-permalink="<?=$values->getPermalink()?>" class="editService" data-toggle="tooltip" data-placement="top" title="" data-original-title="Editar servicio"><i class="fa fa-pencil-square-o fa-2x"></i></span>&nbsp;
                                        <span class="deleteService" data-toggle="tooltip" data-placement="top" title="" data-original-title="Eliminar servicio"><i class="fa fa-remove fa-2x"></i></span>&nbsp;
                                        <span class="newSubService" data-toggle="tooltip" data-placement="top" title="" data-original-title="Agregar sub-servicio"><i class="fa fa-plus-square fa-2x"></i></span>&nbsp;
                                        <a href="<?=$this->url->get('dashboard/services/sub-services/'.$servid)?>" data-toggle="tooltip" data-placement="top" title="" data-original-title="Lista de sub-servicios"><i class="fa fa-list-ol fa-2x"></i></a>&nbsp;
                                    </td>
                                </tr>
                            <?php endforeach;?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- END DEFAULT DATATABLE -->
        </div>
    </div>
</div>
<!-- PAGE CONTENT WRAPPER -->

<!-- MESSAGE BOX-->
<div class="message-box animated fadeIn" data-sound="alert" id="mb-remove-row">
    <div class="mb-container">
        <div class="mb-middle">
            <div class="mb-title"><span class="fa fa-times"></span> Eliminar <strong>Datos</strong> ?</div>
            <div class="mb-content">
                <p>¿Estas seguro de eliminar esta fila?</p>
                <p>Presione "Si" si esta seguro.</p>
            </div>
            <div class="mb-footer">
                <div class="pull-right">
                    <button class="btn btn-success btn-lg mb-control-yes">Si</button>
                    <button class="btn btn-default btn-lg mb-control-close">No</button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END MESSAGE BOX-->
<!--MODALS SERVICES-->
<div class="modal" id="modalService" tabindex="-1" role="dialog" aria-labelledby="defModalHead" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Cerrar</span></button>
                <h4 class="modal-title" id="defModalHead"></h4>
            </div>
            <form id="formServices" action="#" method="post" class="form-horizontal">
                <input type="hidden"  id="servid" name="servid">
                <div class="modal-body">
                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-3">
                            <input type="text" class="form-control" placeholder="Nombre" name="name" id="name" required/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-3">
                            <input type="text" class="form-control" placeholder="Permalink" name="permalink" id="permalink" required readonly/>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Cerrar</button>
                    <input type="submit" class="btn btn-info btn-submit-services" value="Guardar">
                    <!--Messages-->
                    <i id="session-loading" class="fa fa-spinner fa-spin fade hide" style="font-size: 22px;"></i>
                </div>
            </form>
        </div>
    </div>
</div>
<!--END MODALS-->

<!--MODALS SUB-SERVICES -->
<div class="modal" id="modalSubService" tabindex="-1" role="dialog" aria-labelledby="defModalHead" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Cerrar</span></button>
                <h4 class="modal-title" id="defModalHeadSS"></h4>
            </div>
            <form id="formSubServices" action="#" method="post" class="form-horizontal">
                <input type="hidden"  id="servidSS" name="servidSS">
                <div class="modal-body">
                    <div class="form-group">
                        <div class="col-md-6">
                            <input type="text" class="form-control" placeholder="Nombre" name="nameSS" id="nameSS" data-id="" required/>
                        </div>
                        <div class="col-md-6">
                            <input type="text" class="form-control" placeholder="Permalink" name="permalinkSS" id="permalinkSS" required readonly/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6">
                            <input min="0" max="10000" type="number" class="form-control" placeholder="Precio" name="price" id="price" required/>
                        </div>
                        <div class="col-md-6">
                            <input type="number" min="0" max="100" value="0" class="form-control" placeholder="Descuento" name="discount" id="discount" required/>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Cerrar</button>
                    <input type="submit" class="btn btn-info btn-submit-sub-services" value="Guardar">
                </div>
            </form>
        </div>
    </div>
</div>
<!--END MODALS-->
<div class="message-box message-box-danger animated fadeIn" id="message-box-danger-deleted">
    <div class="mb-container">
        <div class="mb-middle">
            <div class="mb-title"><span class="fa fa-times"></span> Cuidado</div>
            <div class="mb-content">
                <p>Usted no puede eliminar este registro ya que esta ligado a otra información.</p>
                <p>Por favor vaya nuevamente al registro de click en editar y elija el estatus de inactivo, de esta forma ya no podrá ser visible en sus operaciones.</p>
                <p><strong>Gracias</strong></p>
            </div>
            <div class="mb-footer">
                <button class="btn btn-default btn-lg pull-right mb-control-close">Cerrar</button>
            </div>
        </div>
    </div>
</div>