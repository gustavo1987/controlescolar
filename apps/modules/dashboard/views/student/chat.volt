<style type="text/css">
.chat
{
    list-style: none;
    margin: 0;
    padding: 0;
}
.chat li
{
    margin-bottom: 10px;
    padding-bottom: 5px;
    border-bottom: 1px dotted #B3A9A9;
}
.chat li.left .chat-body
{
    margin-left: 60px;
}
.chat li.right .chat-body
{
    margin-right: 60px;
}
.chat li .chat-body p
{
    margin: 0;
    color: #777777;
}
.panel .slidedown .glyphicon, .chat .glyphicon
{
    margin-right: 5px;
}
.panel-body
{
    overflow-y: scroll;
    height: 250px;
}
::-webkit-scrollbar-track
{
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
    background-color: #F5F5F5;
}
::-webkit-scrollbar
{
    width: 12px;
    background-color: #F5F5F5;
}
::-webkit-scrollbar-thumb
{
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,.3);
    background-color: #555;
}

</style>
<div class="page-content-wrap">
    <div class=" container sinpadding">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12 fondo-gris-general cuadro">
                <?php if($auth["rol"]=="STUDENT"):?>
                <?php echo $clase->getChat();?>
                <?php elseif($auth["rol"]=="TEACHER"):?>
                    <?php if(count($clase)>1):?>
                        <?php echo $clase[0]->getChat();?>
                    <?php else:?>
                        <?php echo $clase[0]->getChat();?>
                    <?php endif;?>
                <?php else:?>
                    <?php echo "No existe chat por el momento, consultar con la administración." ?>
                <?php endif;?>
            </div>
        </div>
    </div>
</div>