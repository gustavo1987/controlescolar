<div class="page-content-wrap">
    <div class=" container sinpadding">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12 fondo-gris-general cuadro">
                <div class="container">
                    <div class="row">
                        <!--div class="col-lg-8 col-xs-12 col-lg-offset-2 text-center">  
                            <p class="title-site-carrousel"><span class="ion-minus"></span>Organismos Internacionales y Nacionales<span class="ion-minus"></span></p>
                        </div-->
                        <div class="col-md-10 col-md-offset-1 col-xs-12 content-sponsors" style="display: -webkit-inline-box;text-align: center;background-color: white;padding: 13px;">
                            <section class="center slider">
                                  <div>
                                    <a href="http://www.oas.org/es/cidh/" target="_black"><img src="/dash/img/portafolio/portafolio1.jpg"></a>
                                  </div>
                                  <div>
                                    <a href="http://www.cndh.org.mx/" target="_blank"><img src="/dash/img/portafolio/portafolio2.jpg"></a>
                                  </div>
                                  <div>
                                    <a href="https://portal.te.gob.mx/" target="_blank"><img src="/dash/img/portafolio/portafolio3.jpg"></a>
                                  </div>
                                  <div>
                                    <a href="https://www.scjn.gob.mx/" target="_blank"><img src="/dash/img/portafolio/portafolio4.jpg"></a>
                                  </div>
                                  <div>
                                    <a href="http://www.corteidh.or.cr/" target="_blank"><img src="/dash/img/portafolio/portafolio5.jpg"></a>
                                  </div>
                            </section>
                        </div>
                        <div class="col-lg-8 col-xs-12 col-lg-offset-2 text-center">  
                            <p class="title-site-carrousel"><span class="ion-minus"></span>Tribunales Electorales Locales de México<span class="ion-minus"></span></p>
                        </div> 
                        <div class="col-md-10 col-md-offset-1 col-xs-12 content-sponsors" style="display: -webkit-inline-box;text-align: center;background-color: white;padding: 13px;">
                          <section class="center slider">
                                  <div>
                                    <a href="http://teeags.mx/" target="_black"><img src="https://www.te.gob.mx/media/images/58e70a06a44fd79.jpg"></a>
                                  </div>
                                  <div>
                                    <a href="http://tje-bc.gob.mx/index.php" target="_blank"><img src="https://www.te.gob.mx/media/images/ab5601b246591fd.jpg"></a>
                                  </div>
                                  <div>
                                    <a href="http://teebcs.org/" target="_blank"><img src="https://www.te.gob.mx/media/images/b87942c30df9a82.jpg"></a>
                                  </div>
                                  <div>
                                    <a href="http://teecampeche.jimdo.com/" target="_blank"><img src="https://www.te.gob.mx/media/images/644a85f6fa38647.jpg"></a>
                                  </div>
                                  <div>
                                    <a href="http://www.tribunalelectoralchiapas.gob.mx/" target="_blank"><img src="https://www.te.gob.mx/media/images/a81e367198d76a4.jpg"></a>
                                  </div>
                                  <div>
                                    <a href="http://techihuahua.org.mx/" target="_blank"><img src="https://www.te.gob.mx/media/images/b78a047e1adf03d.jpg"></a>
                                  </div>
                                  <div>
                                    <a href="https://www.tedf.org.mx/" target="_blank"><img src="https://www.te.gob.mx/media/images/a81e367198d76a4.jpg"></a>
                                  </div>
                                  <div>
                                    <a href="http://www.pjec.gob.mx/tribunal-electoral.html" target="_blank"><img src="https://www.te.gob.mx/media/images/57990da4f38ed5d.jpg"></a>
                                  </div>
                                  <div>
                                    <a href="http://www.tee.org.mx/" target="_blank"><img src="https://www.te.gob.mx/media/images/7eac56af9cd6613.jpg"></a>
                                  </div>
                                  <div>
                                    <a href="http://www.tedgo.gob.mx/" target="_blank"><img src="https://www.te.gob.mx/media/images/d1b731cdf8fdb54.jpg"></a>
                                  </div>
                                  <div>
                                    <a href="http://www.teemmx.org.mx/" target="_blank"><img src="https://www.te.gob.mx/media/images/3239b598a908000.jpg"></a>
                                  </div>
                                  <div>
                                    <a href="http://www.teegto.org.mx/" target="_blank"><img src="https://www.te.gob.mx/media/images/c0c9481fb6f4c30.jpg"></a>
                                  </div>
                                  <div>
                                    <a href="http://www.teegro.gob.mx/" target="_blank"><img src="https://www.te.gob.mx/media/images/400f4a69503e1d1.jpg"></a>
                                  </div>
                                  <div>
                                    <a href="http://www.trielectoralhidalgo.org.mx/" target="_blank"><img src="https://www.te.gob.mx/media/images/9b34d7237b6ffeb.jpg"></a>
                                  </div>
                                  <div>
                                    <a href="http://www.triejal.gob.mx/" target="_blank"><img src="https://www.te.gob.mx/media/images/09ceaba067652e6.jpg"></a>
                                  </div>
                                  <div>
                                    <a href="http://www.teemich.org.mx/" target="_blank"><img src="https://www.te.gob.mx/media/images/b142ee391be300d.jpg"></a>
                                  </div>
                                  <div>
                                    <a href="http://www.teem.gob.mx/" target="_blank"><img src="https://www.te.gob.mx/media/images/98394d137ab80c1.jpg"></a>
                                  </div>
                                  <div>
                                    <a href="http://www.tee-nl.org.mx/" target="_blank"><img src="https://www.te.gob.mx/media/images/4fc0400547c2504.jpg"></a>
                                  </div>
                                  <div>
                                    <a href="http://teoax.org/" target="_blank"><img src="https://www.te.gob.mx/media/images/e762e44096f83b7.jpg"></a>
                                  </div>
                                  <div>
                                    <a href="http://www.teep.org.mx/" target="_blank"><img src="https://www.te.gob.mx/media/images/faa96b162a42c81.jpg"></a>
                                  </div>
                                  <div>
                                    <a href="http://www.teeq.gob.mx/" target="_blank"><img src="https://www.te.gob.mx/media/images/5626929e7c4b469.jpg"></a>
                                  </div>
                                  <div>
                                    <a href="http://www.teqroo.com.mx/sitio2007/teqroo/" target="_blank"><img src="https://www.te.gob.mx/media/images/4e9fb9419f5c9cc.jpg"></a>
                                  </div>
                                  <div>
                                    <a href="http://teeslp.gob.mx/"><img src="https://www.te.gob.mx/media/images/a83a046e60f4863.jpg"></a>
                                  </div>
                                  <div>
                                    <a href="http://www.teesin.org.mx/" target="_blank"><img src="https://www.te.gob.mx/media/images/49b013a67a25b93.jpg"></a>
                                  </div>
                                  <div>
                                    <a href="http://www.teesonora.org.mx/" target="_blank"><img src="https://www.te.gob.mx/media/images/b7b2ca34b4be317.jpg"></a>
                                  </div>
                                  <div>
                                    <a href="http://www.tet.gob.mx/index-web.php" target="_blank"><img src="https://www.te.gob.mx/media/images/a9a3c0bff18dc9a.jpg"></a>
                                  </div>
                                  <div>
                                    <a href="http://www.trieltam.org.mx/" target="_blank"><img src="https://www.te.gob.mx/media/images/d7ac1f7535b1cd2.jpg"></a>
                                  </div>
                                  <div>
                                    <a href="http://tetlax.org.mx/respaldo/" target="_blank"><img src="https://www.te.gob.mx/media/images/4e0f192422e9c2d.jpg"></a>
                                  </div>
                                  <div>
                                    <a href="http://www.teever.gob.mx/" target="_blank"><img src="https://www.te.gob.mx/media/images/aeb3b2f2f20e5d4.jpg"></a>
                                  </div>
                                  <div>
                                    <a href="http://www.teey.org.mx/" target="_blank"><img src="https://www.te.gob.mx/media/images/9efdd676f77152a.jpg"></a>
                                  </div>
                                  <div>
                                    <a href="http://www.tjez.gob.mx/" target="_blank"><img src="https://www.te.gob.mx/media/images/a5509171c6a0f83.jpg"></a>
                                  </div>
                             </section>
                        </div>
                        <div class="col-lg-8 col-xs-12 col-lg-offset-2 text-center">  
                            <p class="title-site-carrousel"><span class="ion-minus"></span>Organismos Públicos Electorales Locales de México<span class="ion-minus"></span></p>
                        </div> 



                        <div class="col-md-10 col-md-offset-1 col-xs-12 content-sponsors" style="display: -webkit-inline-box;text-align: center;background-color: white;padding: 13px;">
                            <section class="center slider">
                                    <div>
                                      <a href="http://www.ieeags.org.mx/" target="_black"><img src="http://www.ieeags.org.mx/1_imagenes/logo.png"></a>
                                    </div>
                                    <div>
                                      <a href="http://www.ieebc.mx/" target="_blank"><img src="http://www.ieebc.mx/images/logo.png"></a>
                                    </div>
                                    <div>
                                      <a href="https://www.ieebcs.org.mx/" target="_blank"><img src="https://www.ieebcs.org.mx/img/logo-name.png"></a>
                                    </div>
                                    <div>
                                      <a href="http://www.ieec.org.mx/" target="_blank"><img src="http://www.ieec.org.mx/images/elements/sup/logonuevo7.png"></a>
                                    </div>
                                    <div>
                                      <a href="https://www.iepc-chiapas.org.mx/" target="_blank"><img src="https://www.iepc-chiapas.org.mx/images/logook.png"></a>
                                    </div>
                                    <div>
                                      <a href="http://www.ieechihuahua.org.mx/" target="_blank"><img src="http://www.ieechihuahua.org.mx/public/img/logo.png"></a>
                                    </div>
                                    <div>
                                      <a href="http://www.iec.org.mx/v1/index.php" target="_blank"><img src="http://www.iec.org.mx/v1/images/banners/logomini2.png"></a>
                                    </div>
                                    <div>
                                      <a href="https://ieecolima.org.mx/" target="_blank"><img src="https://ieecolima.org.mx/img/core-img/IEE.png"></a>
                                    </div>
                                    <div>
                                      <a href="http://www.iedf.org.mx/index.php?cadena=" target="_blank"><img src="https://www.ine.mx/wp-content/uploads/2017/03/Logo_IECDMX-160X90.jpg"></a>
                                    </div>
                                    <div>
                                      <a href="https://www.iepcdurango.mx/x/instituto-electoral-de-durango"><img src="https://www.ine.mx/wp-content/uploads/2017/03/durango.jpg"></a>
                                    </div>
                                    <div>
                                      <a href="https://ieeg.mx/" target="_blank"><img src="https://www.ine.mx/wp-content/uploads/2017/03/guanajuato.jpg"></a>
                                    </div>
                                    <div>
                                      <a href="http://www.iepcgro.mx/" target="_blank"><img src="https://www.ine.mx/wp-content/uploads/2017/03/guerrero.jpg"></a>
                                    </div>
                                    <div>
                                      <a href="http://www.ieehidalgo.org.mx/" target="_blank"><img src="https://www.ine.mx/wp-content/uploads/2017/03/hidalgo.jpg"></a>
                                    </div>
                                    <div>
                                      <a href="http://www.iepcjalisco.org.mx/" target="_blank"><img src="https://www.ine.mx/wp-content/uploads/2017/03/jalisco.jpg"></a>
                                    </div>
                                    <div>
                                      <a href="http://www.ieem.org.mx/" target="_blank"><img src="https://www.ine.mx/wp-content/uploads/2017/03/mexico.jpg"></a>
                                    </div>
                                    <div>
                                      <a href="http://www.iem.org.mx/index.php"><img src="https://www.ine.mx/wp-content/uploads/2017/03/michoacan.jpg"></a>
                                    </div>
                                    <div>
                                      <a href="http://impepac.mx/" target="_blank"><img src="https://www.ine.mx/wp-content/uploads/2017/03/morelos.jpg"></a>
                                    </div>
                                    <div>
                                      <a href="http://www.ieenayarit.org/" target="_blank"><img src="https://www.ine.mx/wp-content/uploads/2017/03/nayarit.jpg"></a>
                                    </div>
                                    <div>
                                      <a href="http://www.cee-nl.org.mx/" target="_blank"><img src="https://www.ine.mx/wp-content/uploads/2017/03/nuevo-leon.jpg"></a>
                                    </div>
                                    <div>
                                      <a href="http://ieepco.org.mx/" target="_blank"><img src="https://www.ine.mx/wp-content/uploads/2017/03/oaxaca.jpg"></a>
                                    </div>
                                    <div>
                                      <a href="https://www.ieepuebla.org.mx/"><img src="https://www.ine.mx/wp-content/uploads/2017/03/puebla.jpg"></a>
                                    </div>
                                    <div>
                                      <a href="http://ieeq.mx/" target="_blank"><img src="https://www.ine.mx/wp-content/uploads/2017/03/queretaro.jpg"></a>
                                    </div>
                                    <div>
                                      <a href="http://www.ieqroo.org.mx/" target="_blank"><img src="https://www.ine.mx/wp-content/uploads/2017/03/quintana-roo.jpg"></a>
                                    </div>
                                    <div>
                                      <a href="http://www.ceepacslp.org.mx/ceepac/" target="_blank"><img src="https://www.ine.mx/wp-content/uploads/2017/03/san-luis.jpg"></a>
                                    </div>
                                    <div>
                                      <a href="https://www.ieesinaloa.mx/" target="_blank"><img src="https://www.ine.mx/wp-content/uploads/2017/03/sinaloa.jpg"></a>
                                    </div>
                              </section>
                        </div>
                    </div>  
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://code.jquery.com/jquery-2.2.0.min.js" type="text/javascript"></script>
<script src="./slick/slick.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript">
    $(document).on('ready', function() {
      $(".center").slick({
        infinite: true,
        centerMode: true,
        slidesToShow: 4,
        slidesToScroll: 1,
        arrows:false,
        autoplay: true,
        autoplaySpeed: 2000,
      });

    });
</script>