<style>
.banner-desktop {
    margin: 0px 5.5% 20px 2.5% !important;
    width: 80% !important;
}
.banner-aqua {
    background-image: url(/dash/images/ide.png);
    background-size: cover;
    -moz-background-size: cover;
    -webkit-background-size: cover;
    -o-background-size: cover;
}
.banner-aqua{
    background-color: grey;
}
.squareBanner {
    border-radius: 4px;
    float: left;
    padding: 40px 15px 40px 15px;
    text-align: center;
    margin: 0px 1.5% 20px 1.5%;
    background-color: #000;
    color: white;
    height: 185px !important;
}
.mt-5 {
    margin-top: 5rem !important;
}
/*Ibedel*/
.lista1{
    text-align: center;
    position: relative;
}
ul li.student .alista1{
    width: 350px;
    height: 250px;
}
ul li.student .alista1::after{
    content: "";
    position: absolute;
    top: 250px;
    left: -11px;
    height: 20px;
    width: 100%;
    background: #83a8ec;
    transition: .5s;
    transform: rotate(0deg) skewX(-45deg);
}
ul li.student .alista1.aula::after{
        background: #737175;
}
ul li.student a.aula:before{
        background: #807c84 !important;
}


.ilista1{
    font-size: 150px !important;
    margin-top: 10% !important;
}
.position1{
    position: relative;
    margin-top: 1em;
    font-size: 1.5em;
    font-weight: bold;
    top: 3%;
    float: left;
}
/*Ibedel*/
body{
    margin: 0;
    padding: 0;
    background-color: #ccc;
}
/*Se cambió*/
ul{
    position: relative;
    margin: 0;
    padding: 0;
    display: flex;
    justify-content: center;
}
/*Se cambió*/
ul li{
    list-style: none;
    margin: 0 40px;
}
ul li.student .fa, ul li.student .fas{
    font-size: 100px;
    color: #fff;
    line-height: 80px;
    transition: .5s;
    padding-top: 20px;
}
ul li.student a{
    position: relative;
    display: block;
    width: 250px;
    height: 150px;
    background-color: #fff;
    text-align: center;
    /*transform: perspective(100px) rotate(0deg) skew(25deg) translate(0,0);*/
    transform: perspective(100px) rotate(0deg) skew(0deg) translate(0,0);
    transition: .5s;
    box-shadow: -20px 20px 10px rgb(0, 0, 0, 0.5);
}
ul li.student a::before{
    content: "";
    position: absolute;
    top: 10px;
    left: -20px;
    height: 100%;
    width: 20px;
    background: #b1b1b1;
    transition: .5s;
    transform: rotate(0deg) skewY(-45deg);
}
ul li.student a::after{
    content: "";
    position: absolute;
    top: 150px;
    left: -11px;
    height: 20px;
    width: 100%;
    background: #b1b1b1;
    transition: .5s;
    transform: rotate(0deg) skewX(-45deg);
}
ul li.student a:hover{
    /*transform: perspective(1000px) rotate(0deg) skew(25deg) translate(20px, -20px);*/
    transform: perspective(1000px) rotate(0deg) skew(0deg) translate(20px, -20px);
    box-shadow: -50px 43px 36px rgba(105, 49, 105, 0.5);
}
ul li.lista1.student a:hover{
    /*transform: perspective(1000px) rotate(0deg) skew(25deg) translate(20px, -20px);*/
    transform: perspective(1000px) rotate(0deg) skew(0deg) translate(20px, -20px);
    box-shadow: -50px 43px 36px rgba(101, 98, 101, 0.5);
}


ul li.student:hover .fa{
    color: #fff;
}
ul li.student a{
    background: #727175;
}
ul li.student a:before{
    background: #805ea5;
}
ul li.student a:after{
    background: #4d326a;
}
ul li.student:hover:nth-child(1) a{
    background: #737175;
}

ul li.student:hover:nth-child(1) a:before{
    background: #7967a4;
}



ul li.student:hover:nth-child(1) a.alista1.aula:after{
    background: #737175;
}
ul li.student:hover:nth-child(1) a:after{
    background: #483a69;
}
ul li.student:hover:nth-child(2) a{
    background: #55acee;
}
ul li.student:hover:nth-child(2) a:before{
    background: #4184b7;
}
ul li.student:hover:nth-child(2) a:after{
    background: #4d9fde;
}
ul li.student:hover:nth-child(3) a{
    background: #dd4b39;
}

ul li.student:hover:nth-child(3) a:before{
    background: #c13929;
}
ul li.student:hover:nth-child(3) a:after{
    background: #e83322;
}

ul li.student:hover:nth-child(4) a{
    background: #0077B5;
}

ul li.student:hover:nth-child(4) a:before{
    background: #036aa0;
}
ul li.student:hover:nth-child(4) a:after{
    background: #0d82bf;
}

ul li.student:hover:nth-child(5) a{
    background: linear-gradient(#400080, transparent), linear-gradient(200deg, #d047d1, #ff0000, #ffff00);
}

ul li.student:hover:nth-child(5) a:before{
    background: linear-gradient(#400080, transparent), linear-gradient(200deg, #d047d1, #ff0000, #ffff00);
}
ul li.student:hover:nth-child(5) a:after{
    background: linear-gradient(#400080, transparent), linear-gradient(200deg, #d047d1, #ff0000, #ffff00);
}
ul li.student a.red{
    background: #f1808d;
}
ul li.student a.red:before{
    background: #f3597a;
}
ul li.student a.red:after{
    background: #fd6c9f;
}
ul li.student:hover a.red{
    background: #ec3c50;
}
ul li.student:hover a.red:before{
    background: #881f36;
}
ul li.student:hover a.red:after{
    background: #bb1d54;
}
</style>
<div class="page-content-wrap" style="background-color: #25396880;">
    <div class=" container sinpadding">
    <div class="row">
             <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12 cuadro" style="background: rgba(85, 86, 97, 0.4);padding-top: 5%;">
                <div class="container">
                <div class="col-md-12 mt-3">
                    <div class="row">
                        <div class="col-md-3"> 
                            <ul>
                                <li class="student"><a href="/dashboard/user/new-user"><i class="fas fa-user-plus" aria-hidden="true"></i><p class="position1" style="color: white;">Nuevo</p></a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-6"> 
                        </div>
                        <div class="col-md-3"> 
                            <ul>
                                <li class="student"><a><i class="fas fa-user-graduate" aria-hidden="true"></i><p class="position1" style="color: white;">Egresados</p></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 mt-3" style="padding-top: 4%;">
                    <div class="row">
                        <div class="col-md-3 mt-5"> 
                        </div>
                        <div class="col-md-6"> 
                            <ul>
                                <li class="lista1 student"><a href="/dashboard/student/pos" style="background: #483a69;" class="alista1 aula"><i class="fas fa-users ilista1" aria-hidden="true" style="padding-top: 40px;"></i><p class="position1" style="color: white;float: none;top: 25%;">Estudiantes</p></a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-3 mt-5">
                        </div>
                    </div>
                </div>
            </div>
            </div>
        <hr>
    </div>
    </div>
</div>