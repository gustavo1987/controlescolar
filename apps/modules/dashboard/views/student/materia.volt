<style type="text/css">
    .card-header{
        background-color: #11b7e8;
        padding: 8px;
    }
    .collapse.in {
        display: block;
        border: rgba(0,0,0,0.2) 1px solid;
        background-color: rgba(0,0,0,0.2);
        color: brown;
    }
    h5.mb-0.white-text.text-uppercase.font-thin{
        color: white;
        font-size: 19px;
        font-weight: 800;

    }
    .accordion.accordion-2 p {
        font-size: 1rem; 
    }
    .accordion.accordion-2 .card {
        border: 0;
        background-color: transparent; 
    }
    .accordion.accordion-2 .card .card-header,
    .accordion.accordion-2 .card .card-body {
        border: 0;
        border-radius: 3px; 
    }
</style>
<div class="page-content-wrap">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-success">
                <div class="panel-heading">
                    <h3 class="panel-title">Materia: <?= $products->getName(); ?></h3>
                </div>


<div>

  <!-- Nav tabs -->
  <ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Datos</a></li>
    <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Descripción</a></li>
    <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Plan de Estudio</a></li>
    <li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">Biblio</a></li>
  </ul>

  <!-- Tab panes -->
  <div class="tab-content">
    <div role="tabpanel" class="tab-pane active" id="home">
        <h2>Datos de <?=$products->getName(); ?></h2>
        <form>
          <div class="form-group">
            <label for="exampleInputEmail1">Nombre</label>
            <input type="text" class="form-control" value="<?=$products->getName(); ?>" placeholder="Nombre de Materia" readonly>
          </div>
          <div class="form-group">
            <label for="exampleInputPassword1">Creditos</label>
            <input type="text" class="form-control" value="<?=$products->getSalePrice(); ?>" placeholder="Creditos de Materia" readonly>
          </div>
          <div class="form-group">
            <label for="exampleInputPassword1">Clave</label>
            <input type="text" class="form-control" value="<?=$products->getTicket(); ?>" placeholder="Creditos de Materia" readonly>
          </div>
        </form>
    </div>
    <div role="tabpanel" class="tab-pane" id="profile">
        <h2>Descripción de <?=$products->getName(); ?></h2>
        
        <p><?=$products->getDescription(); ?></p>
    </div>
    <div role="tabpanel" class="tab-pane" id="messages">
            <h2>Plan de estudio de <?=$products->getName(); ?></h2>


    </div>
    <div role="tabpanel" class="tab-pane" id="settings">
        <h2>Bibliografia de <?=$products->getName(); ?></h2>
    </div>
  </div>

</div>


            </div>
        </div>
    </div>
</div>