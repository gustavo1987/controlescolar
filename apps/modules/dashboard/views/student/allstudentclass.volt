<div class="page-content-wrap" id="sales">
    <div class=" container sinpadding">
        <div class="row">
            <div class="col-md-12 fondo-gris-general">
                <div class="panel panel-success">
                    <div class="panel-heading">
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                           <!--form action="#" method="post" id="addStudent" name="addStudent" role="form"-->
                                <table class="table table-bordered table-hover table-actions generalDT" data-order="5" data-filter="desc">
                                    <thead>
                                    <tr>
                                        <th>Número&nbsp;Estudiante</th>
                                        <th>Nombre</th>
                                        <th>Apellido Paterno</th>
                                        <th>Apellido Materno</th>
                                        <th>Calificación</th>
                                        <th>Opción</th>
                                        <!-- <th>Opciones</th> -->
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {% for values in studentclass %}
                                    <tr data-id="{{values['clid']}}" id="{{ values['clid']}}" data-clasid="{{values['clasid']}}">
                                        <td>{{values['clid']}}</td>
                                        <td>{{values['name']}}</td>
                                        <td>{{values['last_name']}}</td>
                                        <td>{{values['second_name']}}</td>
                                        <td>{{values['calificacion']}}</td>
                                        <td>
                                        <a class="btn btn-success score">Calificar</a>
                                        </td>
                                    </tr>
                                    {% endfor %}
                                    </tbody>
                                </table>
                           <!--/form-->
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="message-box message-box-info animated fadeIn" id="message-box-info">
    <div class="mb-container">
        <div class="mb-middle">
            <div class="mb-title"> Actualizando &nbsp; <i class="fa fa-circle-o-notch fa-spin style-fa"></i></div>
            <div class="mb-content">
                <p>Guardando y Actualizando su clase, espere un momento por favor.</p>
            </div>
        </div>
    </div>
</div>
<div class="message-box message-box-success animated fadeIn" id="message-box-success">
    <div class="mb-container">
        <div class="mb-middle">
            <div class="mb-title"><span class="fa fa-check"></span> Clase actualizada</div>
            <div class="mb-content">
                <p>Sus cambios han sido guardados correctamente, actualizaremos el sitio para reflejar los cambios.</p>
            </div>
        </div>
    </div>
</div>
<div class="message-box message-box-warning animated fadeIn" id="message-box-warning">
    <div class="mb-container">
        <div class="mb-middle">
            <div class="mb-title"><span class="fa fa-warning"></span> Error</div>
            <div class="mb-content">
                <p>Ha ocurrido un error al guardar su información, inténtelo nuevamente o regrese mas tarde.</p>
            </div>
            <div class="mb-footer">
                <button class="btn btn-default btn-lg pull-right mb-control-close">Cerrar</button>
            </div>
        </div>
    </div>
</div>
<div class="message-box message-box-danger animated fadeIn" id="message-box-danger">
    <div class="mb-container">
        <div class="mb-middle">
            <div class="mb-title"><span class="fa fa-times"></span> Cuidado</div>
            <div class="mb-content">
                <p>Usted no puede actualizar su imagen.</p>
            </div>
            <div class="mb-footer">
                <button class="btn btn-default btn-lg pull-right mb-control-close">Cerrar</button>
            </div>
        </div>
    </div>
</div>
<div class="message-box animated fadeIn" data-sound="alert" id="other_products">
    <div class="mb-container">
        <div class="mb-middle">
            <div class="mb-title"><span class="fa fa-sign-out"></span> Alta <strong>Cliente</strong></div>
            <div class="mb-content">
                <p>¿Le gustaría dar de alta otro cliente?</p>
                <p>Pulse Sí para dar de alta un nuevo cliente. Pulse No para ir a la lista de clientes.</p>
            </div>
            <div class="mb-footer">
                <div class="pull-right">
                    <button class="btn btn-success btn-lg mb-control-close">Si</button>
                    <a href="{{url('dashboard/clients')}}" class="btn btn-default btn-lg">No</a>
                </div>
            </div>
        </div>
    </div>
</div>




<div class="message-box animated fadeIn" data-sound="alert" id="updatescorestudent">
    <div class="mb-container">
        <div class="mb-middle">
            <div class="mb-title"><span class="fa fa-sign-out"></span> Calicifación <strong>Estudiante</strong></div>
            <form action="#" method="post" id="updatescorestudentform" name="updatescorestudentform" role="form">
            <div class="mb-content col-md-12">
                <p>Coloca la calificación parcial</p>
                <input type="number" class="form-control" name="scorestudent" id="scorestudent" required placeholder="calificación">
                <input type="number" name="clid-update" class="form-control hidden" id="clid-student-update-score">
                <input type="number" name="clasid-update" class="form-control hidden" id="clasid-student-update-score">                
            </div>
            <div class="mb-footer">
                <div class="pull-right">
                    <button type="submit" class="btn btn-success btn-lg">Calificar</button>
                    <a href="{{url('dashboard/student/allstudentclass')}}" class="btn btn-default btn-lg">No</a>
                </div>
            </div>
            </form>
        </div>
    </div>
</div>