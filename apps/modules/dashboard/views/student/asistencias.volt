<style type="text/css">
    .toggle-header{
        padding:10px 0;
        margin:10px 0;
        background-color:black;
        color:white;
    }
    .txt-green{
        color:green;
    }
    .txt-red{
        color:red;
    }
    .cuadro-ide {
        border: #4d326a solid 2px;
        border-radius: 11px;
        padding: 5px 20px;
        text-align: center;
        font-size: 20px;
    }
    .title-class {
        padding: 2% 0;
        text-align: center;
        color: #483a69;
    }
</style>
<div class="page-content-wrap">
    <div class=" container sinpadding">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12 fondo-gris-general cuadro">
                <?php if($auth['rol']=="STUDENT"):?>
                <!--p class="cuadro-ide">Para acreditar la materia se debe cumplir con el 80% de la asistencia.<br>La asistencia puede ser en vivo o en diferido</p-->
                <!--h3 class="title-class"><?php echo $clase->getName();?></h3-->
                <div class="row">
                    <?php if($statusconfe==1):?>
                    <div class="panel panel-default clearfix">
                        <div class="panel-heading visible-xs">
                            <h2 class="panel-title">Nomenclatura</h2>
                            <br>
                            <br>
                            <p class="small">
                                V = Asistencia Virtual en Vivo,  D = Asistencia Virtual Diferida, I = Inasistenica                            
                            </p>
                        </div>
                        <div class="col-xs-12 toggle-header">
                            <div class="col-xs-6">
                                <button type="button" class="btn btn-primary btn-sm hidden-xs" data-toggle="collapse" data-target="#feature-1" style="margin: 0 50%;">
                                    Cátedras
                                </button>
                                <button type="button" class="btn btn-primary btn-xs visible-xs" data-toggle="collapse" data-target="#feature-1">
                                    Cátedras
                                </button>
                            </div>
                            <div class="col-xs-2 text-center">
                                <span class="hidden-xs" style="font-size: 16px;">Asistencia Virtual en Vivo</span>
                                <span class="visible-xs">V</span>
                            </div>
                            <div class="col-xs-2 text-center">
                                 <span class="hidden-xs" style="font-size: 16px;">Asistencia Virtual Diferida</span>
                                <span class="visible-xs">D</span>
                            </div>
                            <div class="col-xs-2 text-center">
                                 <span class="hidden-xs" style="font-size: 16px;">Inasistencia</span>
                                <span class="visible-xs">I</span>
                            </div>
                        </div>
                        <div id="feature-1" class="collapse in">
                            <?php foreach ($conferencias as $key => $value):?>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-xs-6">
                                        <?php echo $value->getNameConferencia();?>                                            
                                    </div>
                                    <div class="col-xs-2 text-center">
                                        <?php if($value->getStatusAsistencia()=="VIVO"):?>
                                        <i class="glyphicon glyphicon-ok txt-green"></i>
                                        <?php else:?>
                                        <?php endif;?>
                                    </div>
                                    <div class="col-xs-2 text-center">
                                        <?php if($value->getStatusAsistencia()=="DIFERIDO"):?>
                                        <i class="glyphicon glyphicon-ok txt-green"></i>
                                        <?php else:?>
                                        <?php endif;?>
                                    </div>
                                    <div class="col-xs-2 text-center">
                                        <?php if($value->getStatusAsistencia()==null):?>
                                        <i class="glyphicon glyphicon-remove txt-red"></i>
                                        <?php else:?>
                                        <?php endif;?>
                                    </div>
                                </div>
                            </div>  
                            <?php endforeach; ?>
                        </div>
                    </div>
                    <?php else:?>
                        <p style="text-align: center;font-weight: bold;font-size: 20px;">No hay clases por el momento</p>
                    <?php endif;?>
                </div>
                <?php elseif ($auth['rol']=="TEACHER"):?>
                <h3 class="title-class"><?php echo $clase[0]->getName();?></h3>
                <div class="row">
                    <?php if($statusconfe==1):?>
                        <?php foreach($confes as $key => $cla):?>
                        <div class="panel panel-default clearfix">
                            <div class="panel-heading visible-xs">
                                <h2 class="panel-title">Nomenclatura</h2>
                                <br>
                                <br>
                                <p class="small">
                                    V = Asistencia Virtual en Vivo,  D = Asistencia Virtual Diferida, I = Inasistenica                            
                                </p>
                            </div>
                            <div class="col-xs-12 toggle-header">
                                <div class="col-xs-6">
                                    <button type="button" class="btn btn-primary btn-sm hidden-xs" data-toggle="collapse" data-target="#feature-<?php echo $key;?>">
                                        <i class="glyphicon glyphicon-resize-vertical"></i><?php echo $cla->getName();?>
                                    </button>
                                    <button type="button" class="btn btn-primary btn-xs visible-xs" data-toggle="collapse" data-target="#feature-<?php echo $key;?>">
                                        <i class="glyphicon glyphicon-resize-vertical"></i><?php echo $cla->getName();?>
                                    </button>
                                </div>
                                <div class="col-xs-2 text-center">
                                    <span class="hidden-xs">Asistencia Virtual en Vivo</span>
                                    <span class="visible-xs">V</span>
                                </div>
                                <div class="col-xs-2 text-center">
                                     <span class="hidden-xs">Asistencia Virtual Diferida</span>
                                    <span class="visible-xs">D</span>
                                </div>
                                <div class="col-xs-2 text-center">
                                     <span class="hidden-xs">Inasistencia</span>
                                    <span class="visible-xs">I</span>
                                </div>
                            </div>
                            <div id="feature-<?php echo $key;?>" class="collapse <?php if($key==0){echo in;}?>">
                                <?php foreach ($conferencias as $key => $value):?>
                                    <?php if($cla->getConfid()==$value->getConfid()):?>
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-xs-6 col-sm-6 col-lg-6 col-md-6">
                                                <?php echo $value->getName()." ".$value->getLastName()." ".$value->getSecondName();?>                                            
                                            </div>
                                            <div class="col-xs-2 col-sm-2 col-lg-2 col-md-2 text-center">
                                                <?php if($value->getStatusAsistencia()=="VIVO"):?>
                                                <i class="glyphicon glyphicon-ok txt-green"></i>
                                                <?php else:?>
                                                <i class="glyphicon glyphicon-remove txt-red"></i>
                                                <?php endif;?>
                                            </div>
                                            <div class="col-xs-2 col-sm-2 col-lg-2 col-md-2 text-center">
                                                <?php if($value->getStatusAsistencia()=="DIFERIDO"):?>
                                                <i class="glyphicon glyphicon-ok txt-green"></i>
                                                <?php else:?>
                                                <i class="glyphicon glyphicon-remove txt-red"></i>
                                                <?php endif;?>
                                            </div>
                                            <div class="col-xs-2 col-sm-2 col-lg-2 col-md-2 text-center">
                                                <?php if($value->getStatusAsistencia()==null):?>
                                                <i class="glyphicon glyphicon-ok txt-green"></i>
                                                <?php else:?>
                                                <i class="glyphicon glyphicon-remove txt-red"></i>
                                                <?php endif;?>
                                            </div>
                                        </div>
                                    </div>  
                                    <?php endif;?>
                                <?php endforeach; ?>
                            </div>
                        </div>
                        <?php endforeach;?>
                    <?php else:?>
                        <p style="text-align: center;font-weight: bold;font-size: 20px;">No hay clases por el momento</p>
                    <?php endif;?>
                </div>
                <?php endif;?>
            </div>
        </div>
    </div>
</div>