  <style>
    body {
    }
    .user_name{
      font-size:14px;
      font-weight: bold;
    }
    .comments-list .media{
      border-bottom: 1px dotted #ccc;
    }
    .well{
      background-color: #FFF!important;
    }
    .box
    {
      position: relative;
      width: 100%;
      height: 110px;
      background: #4c0c69;
      box-sizing: border-box;
      box-shadow: 0 10px 20px rgba(0,0,0,.5);
      border:2px solid rgba(0,0,0,.5);
    }
    .box .content{
      position: absolute;
      top: 15px;
      left: 15px;
      right: 15px;
      bottom: 15px;
      border: 2px solid #ffeb3b;
      box-shadow: 0 5px 10px rgba(0,0,0,.5);
      text-align: center;
    }
    .box .content h1{
      color: #fff;
      font-size: 30px;
      margin: 0 0 10px;
      padding: 0;
    }
    .box .content p{
      color: #fff;
    }
    .box svg,
    .box svg rect{
      position: absolute;
      top: 0;
      left: 0;
      width: 100%;
      height: 100%;
      fill: transparent;
    }
    .box svg rect{
      stroke: #0093ff;
      stroke-width:4px;
      stroke-dasharray: 400;
      animation: animate 3s linear infinite;
    }
    .box2{
      background-color: #FFF;
      border: 1px solid #e3e3e3;
      height: 200px;
      padding: 2em;
    }

    @keyframes animate
    {
      0%{
        stroke-dashoffset:800;
      }
      100%{
        stroke-dashoffset:0;
      }

    }
    input[type=checkbox] {
      -ms-transform: scale(2);
      -moz-transform: scale(2);
      -webkit-transform: scale(2);
      -o-transform: scale(1);
      padding: 32px;
    }
  </style>
  <?php if($auth['rol']=="STUDENT"):?>
    <div class="page-content-wrap">
      <div class=" container sinpadding">
        <div class="row">
          <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12 fondo-gris-general cuadro">
            <div class="container">
              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding: 20px 0;">
                <div class="box">
                  <svg><rect></rect></svg>
                  <div class="content">
                    <p style="font-size: 18px;padding: 2%;">NO OLVIDES PAGAR LOS PRIMEROS 5 DÍAS DE CADA MES. EVITA RECARGOS.</p>
                  </div>
                </div>
              </div>
              <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12">
                <!--div class="col-md-12 col-sm-12 col-lg-12 col-xs-12" data-example-id="list-group-badges">
                  <ul class="list-group">
                    <a><li class="list-group-item" style="width: 50%;">
                      <span class="badge">14</span>
                      Pagos
                    </li></a>
                    <a><li class="list-group-item" style="width: 50%;">
                      <span class="badge">14</span>
                      Recibos
                    </li></a>
                  </ul>
                </div-->
                <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12">
                  <p class="parrafo-general pagos-view" style="text-align: end;font-weight: bold;">CONTORL DE PAGOS. Consúltalo <a href="/dashboard/recibos/index" style="color: purple;">aquí</a></p>
                </div>
              </div>
              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding-bottom: 5%;">
                <p class="parrafo-general pagos-view">Los pagos pueden ser realizados mediante pago en línea, transferencia electrónica o depósito bancario para que elijas la modalidad que más te convenga</p>
                <br>
                <ol>
                  <li class="parrafo-general pagos-view" style="font-weight: 800;">PAGO EN LÍNEA - VÍA PAYPAL</li>
                  <!--p class="parrafo-general pagos-view" style="margin-bottom:0%;">Si deseas realizar tu pago en línea, elige el concepto que se deseas pagar -puedes elegir ambos-: </p-->
                  <p class="parrafo-general pagos-view" style="margin-bottom:0%;">Si deseas realizar tu pago en línea, debe contemplar el pago de comisión que genera PAYPAL, el cual es del 5% del monto a pagar + $4.00, ejemplo $1,000.00 + $50.00 + $4.00, total a pagar $1,054.00.</p>
                  <br>
                  <!--form action="#" id="pago-paypal" novalidate="novalidate" class="fv-form fv-form-bootstrap"-->
                    <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12" >
                        <div class="form-check form-check-inline col-md-3 col-sm-3 col-lg-3 col-xs-3" style="text-align: left;">
                        <label class="form-check-label parrafo-general  pagos-view" for="inlineCheckbox1" style="font-size: 22px;font-weight: 400;padding-left: 22%;">Pagar:</label>
                        </div>
                        <!--div class="form-check form-check-inline col-md-3 col-sm-3 col-lg-3 col-xs-3" style="padding-right: 0;padding-left: 0; ">
                          <input class="form-check-input inscripcion-paypal" type="checkbox" name="cobrar" id="inscripcion-paypal" value="inscripcion" data-fv-field="cobrar">
                          <label class="form-check-label" for="inscripcion" style="font-size: 11px">Inscripción</label>
                        </div-->
                        <!--div class="form-check form-check-inline col-md-3 col-sm-3 col-lg-3 col-xs-3" style="padding-right: 0;padding-left: 0; ">
                          <input class="form-check-input inscripcion-paypal" type="checkbox" name="cobrar" id="inscripcion-paypal" value="reinscripcion" data-fv-field="cobrar">
                          <label class="form-check-label parrafo-general pagos-view" for="reinscripcion" style="font-size: 22px;font-weight: 400;padding-left: 12px;">Reinscripción</label>
                        </div>
                        <div class="form-check form-check-inline col-md-3 col-sm-3 col-lg-3 col-xs-3" style="font-size: 22px;font-weight: 400;">
                          <input class="form-check-input inscripcion-paypal" type="checkbox" name="cobrar" id="inscripcion-paypal" value="mensualidad" data-fv-field="cobrar"--><!--i class="form-control-feedback fv-icon-no-label glyphicon glyphicon-ok" data-fv-icon-for="cobrar" style="display: inline;"></i-->
                          <!--label class="form-check-label parrafo-general pagos-view" for="mensualidad" style="font-size: 22px;font-weight: 400;padding-left: 12px;">Mensualidad</label-->
                          <!--small class="help-block" data-fv-validator="notEmpty" data-fv-for="cobrar" data-fv-result="NOT_VALIDATED" style="display: none;">Es necesario elegir que pagar</small-->
                          <!--input class="hidden" type="email" name="emailpago" value="<?php echo $emailuser;?>" data-fv-field="emailpago" style="border: 1px solid #000;width: 100%;margin-bottom: .5em; font-size: 0.5em;">
                        </div-->
                        <div class="form-check form-check-inline col-md-2 offset-md-5  col-sm-2 offset-sm-5  col-lg-2 offset-lg-5 col-xs-12" style="padding-right: 0;padding-left: 0;">
                          <a id="paypal-notification"><button class="btn-pay-student-profile" style="font-size: 15px;">REALIZAR PAGO</button></a>
                        </div> 
                    </div>
                      <br><br><br>
                      <li class="parrafo-general pagos-view" style="font-weight: 800;">DEPÓSITO BANCARIO</li>
                      <p class="parrafo-general pagos-view">Institución Bancaria: Bancomer<br>
                        Nombre: Instituto Iberoamericano de Derecho Electoral<br>
                      Número de Cuenta:0112312530</p>
                      <br>
                      <li class="parrafo-general pagos-view" style="font-weight: 800;">TRANSFERENCIA ELECTRÓNICA</li>
                      <p class="parrafo-general pagos-view">Clabe Interbancaria 012180001123125300</p>
                    </ol>
                    <br>
                    <p class="parrafo-general pagos-view">Efectuado el pago vía depósito bancario o transferencia electrónica no olvides enviarnos tu comprobante de pago.</p>
                    <br>

                    <h2 style="text-align: center;color: black;font-weight: bold;">Recibo de pago</h2>
                    <p class="parrafo-general pagos-view">Para poder subir tu recibo, primero seleccione el concepto de tu recibo y despues carque el recibo y en automatico se guardara su recibo.</p>

                    <!--p  class="parrafo-general" style="padding-left: 0; padding-right: 0;">Si realizaste el pago vía transferencia electrónica o depósito bancario, ingresa tu comprobante de pago.</p-->

                    <div class="col-md-4 col-sm-4 col-lg-4 col-xs-12" style="text-align: left;margin-top: 24px;">
                      <label class="parrafo-general pagos-view" style="font-weight: 500;">Ingresa tu recibo de pago:</label>
                    </div>
                    <div class="col-md-4 col-sm-4 col-lg-4 col-xs-12" style="text-align: center;">
                      <label class="form-check-label" style="font-size: 15px;color: black;font-weight: inherit;">Concepto</label>
                      <select id="concepto" class="form-control">
                        <option value="#">Selecciona un concepto</option>
                        <option value="MENSUALIDAD">Mensualidad</option>
                        <option value="INSCRIPCION">Inscripción</option>
                        <option value="AMBOS">Ambos</option>
                        <option value="OTROS">Otros</option>
                      </select>
                    </div>
                    <div class="col-md-4 col-sm-4 col-lg-4 col-lg-4 col-xs-12 lock-div-file" id="file-recibo">
                      <!--input type="email" name="emailrecibo" id="emailrecibo"  style="border: 1px solid #000;width: 100%;font-size: 1.2em;padding: 0 10px;height: 38px;margin-left: 30%;color: black; text-align: center;"-->
                        <div id="recibo" class="dropzone dz-default dz-message" style="min-height:80px;">
                          <div class="dz-default dz-message col-md-12" style="top: 0% !important;">
                              <h2 style="line-height: 0.8;cursor: pointer;padding-top: 4%;">
                                <i class="fas fa-download col-md-4" style="font-size: 45px;cursor: pointer;"></i>
                                <div class="col-md-8">
                                  <p class="col-md-12" style="font-size: 20px;margin:0;cursor: pointer;">Arrastra un archivo</p>
                                  <i style="font-size: 14px;cursor: pointer;" class="col-md-12">o haz click para seleccionar manualmente</i>
                                  <input type="hidden" value="" name="document" id="input-image-receipt" class="col-md-12">
                                </div>
                              </h2>
                          </div>
                        </div>
                        <input class="hidden" type="number" id="uid" name="uid" value="<?php echo $auth["uid"];?>">                    
                    </div>
                    <div class="col-md-12 col-sm-12 col-lg-12 col-lg-12 col-xs-12" style="margin-top: 5%;">
                    <div class="panel panel-success" style="background-color: #ffffff54;">
                        <div class="panel-heading text-right table-recibo-student-all">
                            <h3 class="panel-title" style="color: white;">Recibos</h3>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover table-actions generalDT" data-order="5" data-filter="desc">
                                    <thead>
                                    <tr>
                                        <th class="table-recibo-student">Concepto</th>
                                        <th class="table-recibo-student">Estatus</th>
                                        <th class="table-recibo-student">Fecha de carga</th>
                                        <th class="table-recibo-student">Recibo</th>
                                        <?php if($auth["rol"]=="ADMIN"):?>
                                        <th class="table-recibo-student">Pago</th>
                                        <th class="table-recibo-student">Cantidad</th>
                                        <th class="table-recibo-student">Alumno</th>
                                        <th class="table-recibo-student">Opciones</th>
                                        <?php endif;?>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php if($statusrecibos=="ACTIVO"):?>
                                        <?php foreach($recibosall as $key => $values):?>
                                        <?php $dateC= $values->getDateCreation(); $newDate = date("d-m-Y", strtotime($dateC));?>

                                        <tr id="<?= $values->getRecid() ?>">
                                            <td class="table-recibo-student-registros"><?= $values->getName() ?></td>
                                            <td class="table-recibo-student-registros"><?php if($values->getStatus()=="ACTIVO"){ echo "EN VALIDACIÓN";}else{ echo "VALIDADO";}?></td>
                                            <td class="table-recibo-student-registros"><?= $newDate ?></td>
                                            <td class="table-recibo-student-registros"><a target="_blank" href="/documents/recibos/<?= $values->getDocument() ?>"><span class="fa fa-eye fa-2x"  style="color:white;"></span></a></td>
                                            <?php if($auth["rol"]=="ADMIN"):?>
                                            <td class="table-recibo-student-registros"><?= $values->getNamepago() ?></td>
                                            <td class="table-recibo-student-registros"><?= $values->getCantidad() ?></td>
                                            <td class="table-recibo-student-registros"><?php echo "".$values->getNameuser()." ".$values->getLastName()." ".$values->getSecondName();?></td>
                                            <td class="table-recibo-student-registros">
                                                <a title="Validar Recibo" data-recid="<?= $values->getRecid() ?>" class="validate-recibo"><span class="fa fa-edit fa-2x" style="color:white;"></span></a>
                                            </td>
                                            <?php endif;?>
                                        </tr>
                                        <?php endforeach;?>
                                    <?php else:?>
                                        <h4>Sin recibos.</h4>
                                    <?php endif;?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    </div>
                    <!--div class="col-md-2 col-sm-2 col-lg-2 col-xs-12">
                      <button id="mostrarrecibo" class="btn-pay-recibo" style="width: 100%;">SUBIR RECIBO</button-->
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
  <?php else:?>
  <?php endif;?>