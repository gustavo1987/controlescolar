<div class="page-content-wrap">
    <div class="container sinpadding">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12 fondo-gris-general">
                <h4 class="text-title" style="color: white;text-align: center;">Nuevo pago para <?php echo $client->getName()." ".$client->getLastName()." ".$client->getSecondName();?></h4>
                <div class="col-md-12 col-xs-12 panel-body form-group-separated">
                    <form action="#" method="post" id="newPay" role="form" class="form-horizontal">
                        <span class="hide" id="key-security" data-key="<?php echo $this->security->getToken(); ?>"></span>
                        <span class="hide" id="value-security" data-value="<?php echo $this->security->getTokenKey(); ?>"></span>
                        <div class="form-group">
                            <label class="col-md-3 col-xs-12 control-label" style="color: white;">Descripción del pago</label>
                            <div class="col-md-6 col-xs-12">
                                <input class="hidden" type="" name="id" value="<?php echo $client->getClid();?>">
                                <!--input type="text" class="form-control" placeholder="Nombre" name="namepay" required/-->
                                <select id="namepay" name="namepay" class="form-control" required>
                                    <option value="">Selecciona el pago</option>
                                    <option  value="MENSUALIDAD">MENSUALIDAD</option>
                                    <option  value="INSCRIPCION">INSCRIPCIÓN</option>
                                    <option  value="REINSCRIPCION">REINSCRIPCIÓN</option>
                                    <option  value="OTRO">OTRO</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group hidden div-month">
                            <label class="col-md-3 col-xs-12 control-label" style="color: white;">Mes</label>
                            <div class="col-md-6 col-xs-12">
                                <select id="month" name="month" class="form-control" required>
                                    <option value="">Selecciona el mes</option>
                                    <option  value="Enero">Enero</option>
                                    <option  value="Febrero">Febrero</option>
                                    <option  value="Marzo">Marzo</option>
                                    <option  value="Abril">Abril</option>
                                    <option  value="Mayo">Mayo</option>
                                    <option  value="Junio">Junio</option>
                                    <option  value="Julio">Julio</option>
                                    <option  value="Agosto">Agosto</option>
                                    <option  value="Septiembre">Septiembre</option>
                                    <option  value="Octubre">Octubre</option>
                                    <option  value="Noviembre">Noviembre</option>
                                    <option  value="Diciembre">Diciembre</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group hidden div-others">
                            <label class="col-md-3 col-xs-12 control-label" style="color: white;">Otro</label>
                            <div class="col-md-6 col-xs-12">
                                <select id="others" name="others" class="form-control" required>
                                    <option value="">Selecciona el concepto</option>
                                    <option  value="Certificado">Certificado</option>
                                    <option  value="Titulo">Titulo</option>
                                    <option  value="Cedula">Cedula</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 col-xs-12 control-label" style="color: white;">Costo</label>
                            <div class="col-md-6 col-xs-12">
                                <input type="text" class="form-control" placeholder="Costo" name="cantidad" required/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 col-xs-12 control-label" style="color: white;">Estatus</label>
                            <div class="col-md-6 col-xs-12">
                                <select id="status" name="status" class="form-control" required>
                                    <option value="">Selecciona un estatus</option>
                                    <option  value="PAGADO">PAGADO</option>
                                    <option  value="ADEUDA">ADEUDA</option>
                                    <option  value="ABONO">ABONO</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label" style="color: white;">Fecha de vencimiento </label>
                                <div class="col-md-6 col-xs-12">
                                    <input type="text" class="form-control vencimiento" name="vencimiento" id="vencimiento" placeholder="Fecha nacimiento">
                                </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-2">
                                <input type="submit" class="btn btn-success" value="Guardar"/>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="message-box message-box-info animated fadeIn" id="message-box-info">
    <div class="mb-container">
        <div class="mb-middle">
            <div class="mb-title"> Actualizando &nbsp; <i class="fa fa-circle-o-notch fa-spin style-fa"></i></div>
            <div class="mb-content">
                <p>Guardando y Actualizando su información, espere un momento por favor.</p>
            </div>
        </div>
    </div>
</div>
<div class="message-box message-box-success animated fadeIn" id="message-box-success">
    <div class="mb-container">
        <div class="mb-middle">
            <div class="mb-title"><span class="fa fa-check"></span> Información actualizada</div>
            <div class="mb-content">
                <p>Sus cambios han sido guardados correctamente, actualizaremos el sitio para reflejar los cambios.</p>
            </div>
        </div>
    </div>
</div>
<div class="message-box message-box-warning animated fadeIn" id="message-box-warning">
    <div class="mb-container">
        <div class="mb-middle">
            <div class="mb-title"><span class="fa fa-warning"></span> Error</div>
            <div class="mb-content">
                <p>Ha ocurrido un error al guardar su información, inténtelo nuevamente o regrese mas tarde.</p>
            </div>
            <div class="mb-footer">
                <button class="btn btn-default btn-lg pull-right mb-control-close">Cerrar</button>
            </div>
        </div>
    </div>
</div>
<div class="message-box message-box-danger animated fadeIn" id="message-box-danger">
    <div class="mb-container">
        <div class="mb-middle">
            <div class="mb-title"><span class="fa fa-times"></span> Cuidado</div>
            <div class="mb-content">
                <p>Usted no puede actualizar su imagen.</p>
            </div>
            <div class="mb-footer">
                <button class="btn btn-default btn-lg pull-right mb-control-close">Cerrar</button>
            </div>
        </div>
    </div>
</div>