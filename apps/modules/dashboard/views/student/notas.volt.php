<style type="text/css">
    .toggle-header{
        padding:10px 0;
        margin:10px 0;
        background-color:black;
        color:white;
    }
    .txt-green{
        color:green;
    }
    .txt-red{
        color:red;
    }
    .cuadro-ide {
        border: #4d326a solid 2px;
        border-radius: 11px;
        padding: 5px 20px;
        text-align: center;
        font-size: 20px;
    }
    .title-class {
        padding: 2% 0;
        text-align: center;
        color: #483a69;
    }
</style>
<div class="page-content-wrap">
    <div class=" container sinpadding">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12 fondo-gris-general cuadro">
                <div class="row">
                    <div class="panel panel-default clearfix">
                        <?php if($auth['rol']=="TEACHER"):?>
                            <a class="btn btn-success" data-toggle="modal" data-target="#exampleModal">Agregar Nota</a>
                        <?php endif;?>
                        <div class="col-xs-12 toggle-header">
                            <div class="col-xs-3">
                                <span class="hidden-xs" style="font-size: 19px;">Número </span>
                                <span class="visible-xs" style="font-size: 19px;">#</span>
                            </div>
                            <div class="col-xs-6 text-center">
                                <span class="hidden-xs" style="font-size: 19px;">Lectura o Trabajo </span>
                                <span class="visible-xs" style="font-size: 19px;">Lectura o Trabajo</span>
                            </div>
                            <div class="col-xs-3 text-center">
                                 <span class="hidden-xs" style="font-size: 19px;">Archivo</span>
                                <span class="visible-xs" style="font-size: 19px;">Archivo</span>
                            </div>
                        </div>
                        <div id="feature-1" class="collapse in">
                            <?php foreach ($work as $key => $value):?>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-xs-3" style="font-size: 19px;">
                                        <?php echo $key+1;?>
                                    </div>
                                    <div class="col-xs-6 text-center" style="font-size: 19px;">
                                        <?php echo $value->getName();?>
                                    </div>
                                    <div class="col-xs-3 text-center">
                                        <?php if($value->getFileteacher()):?>
                                            <?php if($value->getType()=="VIDEO"):?>
                                                <a href="<?php echo $value->getFileteacher();?>" target="_black"><i class="fab fa-youtube" style="font-size: 30px;"></i></a>
                                            <?php else:?>
                                                <a href="/documents/work/<?php echo $value->getFileteacher();?>" target="_black"><i class="fas fa-file-pdf" style="font-size: 30px;"></i></a>
                                            <?php endif;?>
                                        <?php else:?>
                                            <h3>SIN ARCHIVO</h3>
                                        <?php endif;?>
                                    </div>
                                </div>
                            </div>
                            <?php endforeach;?>
                        </div>
                    </div>
                    <?php if($claseold):?>
                        <h3>Trabajos Anteriores</h3>
                        <?php foreach ($claseold as $key => $value):?>
                        <div class="panel panel-default clearfix">
                            <h4 class="text-center" style="padding-top: 12px;font-weight: bold;"><?php echo $value->getNameClase();?></h4>
                            <div class="col-xs-12 toggle-header">
                                <div class="col-xs-3">
                                    <span class="hidden-xs" style="font-size: 19px;">Número </span>
                                    <span class="visible-xs" style="font-size: 19px;">#</span>
                                </div>
                                <div class="col-xs-6 text-center">
                                    <span class="hidden-xs" style="font-size: 19px;">Lectura o Trabajo </span>
                                    <span class="visible-xs" style="font-size: 19px;">Lectura o Trabajo</span>
                                </div>
                                <div class="col-xs-3 text-center">
                                     <span class="hidden-xs" style="font-size: 19px;">Archivo</span>
                                    <span class="visible-xs" style="font-size: 19px;">Archivo</span>
                                </div>
                            </div>
                            <div id="feature-1" class="collapse in">
                                <?php foreach ($noteold as $key => $value2):?>
                                <?php if($value2->getClasid()==$value->getClasid()):?>
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-xs-3" style="font-size: 19px;">
                                            <?php echo $key+1;?>
                                        </div>
                                        <div class="col-xs-6 text-center" style="font-size: 19px;">
                                            <?php echo $value2->getName();?>
                                        </div>
                                        <div class="col-xs-3 text-center">
                                            <?php if($value2->getFileteacher()):?>
                                                <?php if($value2->getType()=="VIDEO"):?>
                                                    <a href="<?php echo $value2->getFileteacher();?>" target="_black"><i class="fab fa-youtube" style="font-size: 30px;"></i></a>
                                                <?php else:?>
                                                    <a href="/documents/work/<?php echo $value2->getFileteacher();?>" target="_black"><i class="fas fa-file-pdf" style="font-size: 30px;"></i></a>
                                                <?php endif;?>
                                            <?php else:?>
                                                <h3>SIN ARCHIVO</h3>
                                            <?php endif;?>
                                        </div>
                                    </div>
                                </div>
                                <?php endif;?>
                                <?php endforeach;?>
                            </div>
                        </div>
                        <?php endforeach;?>
                    <?php endif;?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <form action="#" id="save-note-teacher" method="post" name="save-note-teacher" role="form">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Agregar nota</h5>
      </div>
      <div class="modal-body">
         <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12" style="margin-bottom: 5%;margin-top: 5%;">
            <div class="form-group">
                <div class="controls">
                    <div class="col-md-4 col-sm-4 col-xs-4 col-lg-4">
                        <label>Lectura o Trabajo</label>
                    </div>
                    <div class="col-md-8 col-sm-8 col-xs-8 col-lg-8">
                        <input type="text" name="namework" value="" class="form-control hidden" id="namework">
                        <select name="confid" id="confid" class="form-control select2">
                            <option value="">Selecciona la catedra</option>
                            <?php foreach($catedras as $key => $value):?>
                            <option value="<?= $value->getConfid();?>"><?=str_replace('PRÓXIMA ','',$value->getName());?></option>
                        <?php endforeach;?>
                        </select>
                    </div>
                </div>
            </div>
        </div>
         <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12" style="margin-bottom: 5%;margin-top: 5%;">
            <div class="form-group">
                <div class="controls">
                    <div class="col-md-4 col-sm-4 col-xs-4 col-lg-4">
                        <label>Tipo de nota</label>
                    </div>
                    <div class="col-md-8 col-sm-8 col-xs-8 col-lg-8">
                        <select class="form-control" id="select-type" name="selecttype">
                            <option value="">Selecciona el tipo de nota</option>
                            <option value="liga">Video</option>
                            <option value="document">Documento</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
         <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12 hidden" id="input-hidden-liga" style="margin-bottom: 5%;margin-top: 5%;">
            <div class="form-group">
                <div class="controls">
                    <div class="col-md-4 col-sm-4 col-xs-4 col-lg-4">
                        <label>Liga</label>
                    </div>
                    <div class="col-md-8 col-sm-8 col-xs-8 col-lg-8">
                        <input type="text" name="video" value="" class="form-control" id="liga">
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12 hidden" id="document-hidden-modal" style="margin-bottom: 5%;">
            <div class="form-group">
                <div class="controls">
                    <div class="col-md-4 col-sm-4 col-xs-4 col-lg-4">
                        <label>Archivo</label>
                    </div>
                    <div class="col-md-8 col-sm-8 col-xs-8 col-lg-8">
                        <div id="note-teacher" class="dropzone dz-default btn btn-primary" style="min-height: 40px;margin-left: 30%;">
                            <p class="text-center">Seleccionar archivo
                                <input type="hidden" name="image" id="input-image-principal"  value="">
                            </p>
                        </div>
                        <label class="form-label text-center" for="permalink">* El tamaño del archivo debe ser no mayor a 25mb:</label>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button type="submit" class="btn btn-success" >Guardar nota</button>
      </div>
    </form>
    </div>
  </div>
</div>