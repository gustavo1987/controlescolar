<div class="page-content-wrap">
    <div class=" container sinpadding">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12 fondo-gris-general">
                <h4 class="text-title c-white text-center">Nuevo Usuario</h4>
                <div class="col-md-12 col-xs-12 panel-body form-group-separated">
                    <form action="#" method="post" id="editUser" name="editUser" role="form" class="form-horizontal">
                        <span class="hide" id="key-security" data-key="<?php echo $this->security->getToken(); ?>"></span>
                        <span class="hide" id="value-security" data-value="<?php echo $this->security->getTokenKey(); ?>"></span>
                        <input type="text" class="hidden" name="clid" value="<?php echo $user->getClid();?>">
                        <div class="form-group">
                            <label class="col-md-2 col-xs-12 control-label c-white">Nombre</label>
                            <div class="col-md-4 col-xs-12">
                                <input type="text" class="form-control" placeholder="Nombre" name="name2" value="<?php echo $user->getName();?>" required >
                            </div>
                            <label class="col-md-2 col-xs-12 control-label c-white">Apellido paterno</label>
                            <div class="col-md-4 col-xs-12">
                                <input type="text" class="form-control" placeholder="Apellido Paterno" name="last_name" value="<?php echo $user->getLastName();?>" required/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 col-xs-12 control-label c-white">Apellido Materno</label>
                            <div class="col-md-4 col-xs-12">
                                <input type="text" class="form-control" placeholder="Apellido Materno" value="<?php echo $user->getSecondName();?>" name="second_name"/>
                            </div>
                            <label class="col-md-2 col-xs-12 control-label c-white">Sexo</label>
                            <div class="col-md-4 col-xs-12">
                                <select id="sex" name="sex" class="form-control" required>
                                    <option value="">Selecciona un sexo</option>
                                    <option <?php echo $user->getSex()=='M'?'selected':'';?>  value="M">Masculino</option>
                                    <option  <?php echo $user->getSex()=='F'?'selected':'';?> value="F">Femenino</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 col-xs-12 control-label c-white">Posgrado</label>
                            <div class="col-md-4 col-xs-12">
                                <select id="posgrado" name="posgrado" class="form-control" required>
                                    <option value="">Selecciona un postgrado</option>
                                    <?php foreach ($postgrados as $key => $value):?>
                                        <option <?php echo $user->getCarrera()==$value->getBrid()?'selected':'';?> value="<?php echo $value->getBrid();?>"><?php echo $value->getName();?></option>
                                    <?php endforeach;?>
                                </select>
                            </div>
                            <label class="col-md-2 col-xs-12 control-label c-white">Teléfono</label>
                            <div class="col-md-4 col-xs-12">
                                <input type="phone" pattern="[0-9]{10}" class="form-control" placeholder="Teléfonono" value="<?php echo $user->getPhone();?>" name="phone"  required/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 col-xs-12 control-label c-white">Username</label>
                            <div class="col-md-4 col-xs-12">
                                <input type="text" class="form-control" placeholder="Nombre de usuario" name="username" value="<?php echo $user->getUsername();?>" required/>
                            </div>
                            <label class="col-md-2 col-xs-12 control-label c-white">Email</label>
                            <div class="col-md-4 col-xs-12">
                                <input type="email" class="form-control" placeholder="Correo Eelectrónico" name="email2" value="<?php echo $user->getEmail();?>" required/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 col-xs-12 control-label c-white">Status</label>
                            <div class="col-md-4 col-xs-12">
                                <select id="sex" name="status" class="form-control" required>
                                    <option  selected value="ACTIVE">ACTIVO</option>
                                    <option  value="INACTIVE">INACTIVO</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-2">
                                <input type="submit" class="btn btn-success" value="Editar"/>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- info -->
<div class="message-box message-box-info animated fadeIn" id="message-box-info">
    <div class="mb-container">
        <div class="mb-middle">
            <div class="mb-title"> Actualizando &nbsp; <i class="fa fa-circle-o-notch fa-spin style-fa"></i></div>
            <div class="mb-content">
                <p>Guardando y Actualizando su información, espere un momento por favor.</p>
            </div>
        </div>
    </div>
</div>
<!-- end info -->
<!-- success -->
<div class="message-box message-box-success animated fadeIn" id="message-box-success">
    <div class="mb-container">
        <div class="mb-middle">
            <div class="mb-title"><span class="fa fa-check"></span> Información actualizada</div>
            <div class="mb-content">
                <p>Sus cambios han sido guardados correctamente, actualizaremos el sitio para reflejar los cambios.</p>
            </div>
        </div>
    </div>
</div>
<!-- end success -->
<!-- warning -->
<div class="message-box message-box-warning animated fadeIn" id="message-box-warning">
    <div class="mb-container">
        <div class="mb-middle">
            <div class="mb-title"><span class="fa fa-warning"></span> Error</div>
            <div class="mb-content">
                <p>Ha ocurrido un error al guardar su información, inténtelo nuevamente o regrese mas tarde.</p>
            </div>
            <div class="mb-footer">
                <button class="btn btn-default btn-lg pull-right mb-control-close">Cerrar</button>
            </div>
        </div>
    </div>
</div>
<!-- end danger -->
<div class="message-box message-box-danger animated fadeIn" id="message-box-danger">
    <div class="mb-container">
        <div class="mb-middle">
            <div class="mb-title"><span class="fa fa-times"></span> Cuidado</div>
            <div class="mb-content">
                <p>Usted no puede actualizar su imagen.</p>
            </div>
            <div class="mb-footer">
                <button class="btn btn-default btn-lg pull-right mb-control-close">Cerrar</button>
            </div>
        </div>
    </div>
</div>