<div class="page-content-wrap" id="sales" style="background-color: #25396880;">
    <div class=" container sinpadding">
    <div class="row">
        <div class="col-md-12 cuadro"  style="background: rgba(85, 86, 97, 0.4);padding-top: 5%;">
            <div class="panel panel-success">
                <div class="panel-heading">
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                       <form action="#" method="post" id="addStudent" name="addStudent" role="form">
                            <input class="hidden" value="<?=$clase?>" name="salid">
                            <table class="table table-bordered table-hover table-actions generalDT" data-order="5" data-filter="desc">
                                <thead>
                                <tr>
                                    <th>Número</th>
                                    <th>Nombre Estudiante</th>
                                    <th>Email</th>
                                    <th>Bloqueado</th>
                                    <th>Fecha registro</th>
                                    <!-- <th>Opciones</th> -->
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($student as $key => $values):?>
                                <tr id="<?= $values->getUid() ?>">
                                    <td class="student-center"><?= $key + 1 ?></td>
                                    <td class="student-center"><?= $values->getName() ?> <?= $values->getLastName() ?> <?= $values->getSecondName() ?></td>
                                    <td class="student-center"><?= $values->getEmail() ?></td>
                                    <td class="student-center"><?php echo $values->getStatuspagos()==0?"No":"SI";?></td>
                                    <td class="student-center">
                                    <a href="/dashboard/student/edit/<?= $values->getClid() ?>" style="padding: 4px 4px;" target="_black" class="btn btn-success">Editar</a>&nbsp;<?php echo $values->getStatuspagos()==0?'<a style="padding: 4px 4px;" data-id="'.$values->getUid().'" data-status="1" class="btn btn-danger block-student">Bloquear</a>':'<a data-id="'.$values->getUid().'" data-status="0" style="padding: 4px 4px;" class="btn btn-danger block-student">Desbloquear</a>'?>
                                    </td>
                                </tr>
                                <?php endforeach;?>
                                </tbody>
                            </table>
                       </form>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
</div>
<div class="message-box message-box-info animated fadeIn" id="message-box-info">
    <div class="mb-container">
        <div class="mb-middle">
            <div class="mb-title"> Actualizando &nbsp; <i class="fa fa-circle-o-notch fa-spin style-fa"></i></div>
            <div class="mb-content">
                <p>Guardando y Actualizando su clase, espere un momento por favor.</p>
            </div>
        </div>
    </div>
</div>
<div class="message-box message-box-success animated fadeIn" id="message-box-success">
    <div class="mb-container">
        <div class="mb-middle">
            <div class="mb-title"><span class="fa fa-check"></span> Clase actualizada</div>
            <div class="mb-content">
                <p>Sus cambios han sido guardados correctamente, actualizaremos el sitio para reflejar los cambios.</p>
            </div>
        </div>
    </div>
</div>
<div class="message-box message-box-warning animated fadeIn" id="message-box-warning">
    <div class="mb-container">
        <div class="mb-middle">
            <div class="mb-title"><span class="fa fa-warning"></span> Error</div>
            <div class="mb-content">
                <p>Ha ocurrido un error al guardar su información, inténtelo nuevamente o regrese mas tarde.</p>
            </div>
            <div class="mb-footer">
                <button class="btn btn-default btn-lg pull-right mb-control-close">Cerrar</button>
            </div>
        </div>
    </div>
</div>
<div class="message-box message-box-danger animated fadeIn" id="message-box-danger">
    <div class="mb-container">
        <div class="mb-middle">
            <div class="mb-title"><span class="fa fa-times"></span> Cuidado</div>
            <div class="mb-content">
                <p>Usted no puede actualizar su imagen.</p>
            </div>
            <div class="mb-footer">
                <button class="btn btn-default btn-lg pull-right mb-control-close">Cerrar</button>
            </div>
        </div>
    </div>
</div>
<div class="message-box animated fadeIn" data-sound="alert" id="other_products">
    <div class="mb-container">
        <div class="mb-middle">
            <div class="mb-title"><span class="fa fa-sign-out"></span> Alta <strong>Cliente</strong></div>
            <div class="mb-content">
                <p>¿Le gustaría dar de alta otro cliente?</p>
                <p>Pulse Sí para dar de alta un nuevo cliente. Pulse No para ir a la lista de clientes.</p>
            </div>
            <div class="mb-footer">
                <div class="pull-right">
                    <button class="btn btn-success btn-lg mb-control-close">Si</button>
                    <a href="<?= $this->url->get('dashboard/clients') ?>" class="btn btn-default btn-lg">No</a>
                </div>
            </div>
        </div>
    </div>
</div>