<style type="text/css">
    .card-header{
        background-color: #11b7e8;
        padding: 8px;
    }
    .collapse.in {
        display: block;
        border: rgba(0,0,0,0.2) 1px solid;
        background-color: rgba(0,0,0,0.2);
        color: brown;
    }
    h5.mb-0.white-text.text-uppercase.font-thin{
        color: white;
        font-size: 19px;
        font-weight: 800;

    }
    .accordion.accordion-2 p {
        font-size: 1rem; 
    }
    .accordion.accordion-2 .card {
        border: 0;
        background-color: transparent; 
    }
    .accordion.accordion-2 .card .card-header,
    .accordion.accordion-2 .card .card-body {
        border: 0;
        border-radius: 3px; 
    }
</style>
<ul class="breadcrumb">
    <li><a href="<?= $this->url->get('dashboard') ?>">Inicio</a></li>
    <li class="active">Materias</li>
</ul>
<div class="page-title text-right">
    
</div>
<div class="page-content-wrap">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-success">
                <div class="panel-heading">
                    <h3 class="panel-title">Lista de materias</h3>
                </div>
                <br><br>
                <br><br>
                <div class="row d-flex justify-content-center">
                    <div class="col-md-10 col-xl-6 py-5">
                        <div class="accordion accordion-2" id="accordionEx7" role="tablist" aria-multiselectable="true">
                            
                            <div class="card">
                                <div class="card-header rgba-stylish-strong z-depth-1 mb-1" role="tab" id="heading1">
                                    <a data-toggle="collapse" data-parent="#accordionEx7" href="#collapse1" aria-expanded="true" aria-controls="collapse1">
                                        <h5 class="mb-0 white-text text-uppercase font-thin">
                                            Primer Cuatrimestre <i class="fa fa-angle-down rotate-icon"></i>
                                        </h5>
                                    </a>
                                </div>
                                <div id="collapse1" class="collapse in" role="tabpanel" aria-labelledby="heading1" data-parent="#accordionEx7">
                                    <div class="card-body mb-1 rgba-grey-light white-text">
                                        <ul class="list-group">
                                            <?php foreach ($products1 as $key => $value):?>
                                            <li class="list-group-item"><a href="/dashboard/student/materia/<?=$value->getPid()?>"><?=$value->getName()?> <span></span></a></li>
                                            <?php endforeach; ?>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                            <div class="card">
                                <div class="card-header rgba-stylish-strong z-depth-1 mb-1" role="tab" id="heading2">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx7" href="#collapse2" aria-expanded="false" aria-controls="collapse2">
                                        <h5 class="mb-0 white-text text-uppercase font-thin">
                                            Segundo Cuatrimestre <i class="fa fa-angle-down rotate-icon"></i>
                                        </h5>
                                    </a>
                                </div>
                                <div id="collapse2" class="collapse" role="tabpanel" aria-labelledby="heading2" data-parent="#accordionEx7">
                                    <div class="card-body mb-1 rgba-grey-light white-text">
                                        <ul class="list-group">
                                          <?php foreach ($products2 as $key => $value):?>
                                            <li class="list-group-item"><a href="/dashboard/student/materia/<?=$value->getPid()?>"><?=$value->getName()?></a></li>
                                            <?php endforeach; ?>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header rgba-stylish-strong z-depth-1 mb-1" role="tab" id="heading3">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx7" href="#collapse3" aria-expanded="false" aria-controls="collapse3">
                                        <h5 class="mb-0 white-text text-uppercase font-thin">
                                            Tercer Cuatrimestre <i class="fa fa-angle-down rotate-icon"></i>
                                        </h5>
                                    </a>
                                </div>                                    
                                <div id="collapse3" class="collapse" role="tabpanel" aria-labelledby="heading3" data-parent="#accordionEx7">
                                    <div class="card-body mb-1 rgba-grey-light white-text">
                                        <ul class="list-group">
                                          <?php foreach ($products3 as $key => $value):?>
                                            <li class="list-group-item"><a href="/dashboard/student/materia/<?=$value->getPid()?>"><?=$value->getName()?></a></li>
                                            <?php endforeach; ?>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header rgba-stylish-strong z-depth-1 mb-1" role="tab" id="heading4">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx7" href="#collapse4" aria-expanded="false" aria-controls="collapse4">
                                        <h5 class="mb-0 white-text text-uppercase font-thin">
                                            Cuarto Cuatrimestre <i class="fa fa-angle-down rotate-icon"></i>
                                        </h5>
                                    </a>
                                </div>                                    
                                <div id="collapse4" class="collapse" role="tabpanel" aria-labelledby="heading3" data-parent="#accordionEx7">
                                    <div class="card-body mb-1 rgba-grey-light white-text">
                                        <ul class="list-group">
                                          <?php foreach ($products4 as $key => $value):?>
                                            <li class="list-group-item"><a href="/dashboard/student/materia/<?=$value->getPid()?>"><?=$value->getName()?></a></li>
                                            <?php endforeach; ?>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header rgba-stylish-strong z-depth-1 mb-1" role="tab" id="heading5">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx7" href="#collapse5" aria-expanded="false" aria-controls="collapse5">
                                        <h5 class="mb-0 white-text text-uppercase font-thin">
                                            Quinto Cuatrimestre <i class="fa fa-angle-down rotate-icon"></i>
                                        </h5>
                                    </a>
                                </div>                                    
                                <div id="collapse5" class="collapse" role="tabpanel" aria-labelledby="heading3" data-parent="#accordionEx7">
                                    <div class="card-body mb-1 rgba-grey-light white-text">
                                        <ul class="list-group">
                                          <?php foreach ($products5 as $key => $value):?>
                                            <li class="list-group-item"><a href="/dashboard/student/materia/<?=$value->getPid()?>"><?=$value->getName()?></a></li>
                                            <?php endforeach; ?>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>