<style type="text/css">
    .toggle-header{
        padding:10px 0;
        margin:10px 0;
        background-color:#462e67;
        color:white;
    }
    .txt-green{
        color:green;
    }
    .txt-red{
        color:red;
    }
</style>
<div class="page-content-wrap">
    <div class=" container sinpadding">
        <div class="row">
             <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12 fondo-gris-general">

        <div class="container" id="certificado">
                <!--img src="/dash/assets/images/news-image/construccion.png" style="width: 67vw;"-->
                <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12">
                    <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12  text-center">
                        <img src="/dash/img/logo50.png">
                    </div>
                    <!--div class="col-md-9 col-sm-9 col-lg-9 col-xs-9" >
                        <h2 class="text-center">INSTITUTO IBEROAMERICANO DE DERECHO ELECTORAL</h2>
                    </div-->
                </div>
                <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12" style="padding-top: 7%;">
                    <div class="col-md-3 col-sm-3 col-lg-3 col-xs-3">
                      <img class="img-profile-score" src="/dash/assets/images/users/thumbnail/<?= $auth['photo'] ?>" style="border-radius: 50%;width: 75%;" />
                    </div>
                    <div class="col-md-9 col-sm-9 col-lg-9 col-xs-9" >
                        <p class="text-justify class-text-certificate">El Instituto Iberoamericano De Derecho Electoral hace constar que el alumno:</p>
                        <h3 class="text-center name-student-certificate"><?php echo strtoupper($student->getName()." ".$student->getLastName()." ".$student->getSecondName());?></h3>
                        <p class="text-justify class-text-certificate" style="padding-bottom: 5%;">Ha cursado los módulos siguientes: <!--span class="carrera-progreso"><?php if($carreraestudiante=="Maestria en Derecho Constitucional y Derecho"){ echo "Maestría en Derecho Constitucional y Derecho Humano";}
                            else{echo $carreraestudiante;}?></span--></p>
                    </div>
                </div>
                <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12" style="min-height: 40vh;">
                    <div class="col-md-3 col-sm-3 col-lg-3 col-xs-3  text-center">
                        
                    </div>
                    <div class="col-md-9 col-sm-9 col-lg-9 col-xs-9" >
                        <table class="table-progress" style="width:100%;">
                            <tr>
                                <th class="table-progress class-text-certificate">Asignatura</th>
                                <th class="table-progress class-text-certificate">Calificación</th> 
                            </tr>
                        <?php foreach ($score as $key => $value):?>
                          <tr>
                            <td class="table-progress class-text-certificate"><div class="margin-div-score"><?=$value->getName();?></div></td>
                            <td class="table-progress text-center class-text-certificate"><div class="margin-div-score"><?=$value->getCalificacion();?></div></td> 
                          </tr>
                        <?php endforeach;?>
                        </table>
                    </div>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>