<style type="text/css">
    .toggle-header{
        padding:10px 0;
        margin:10px 0;
        background-color:black;
        color:white;
    }
    .txt-green{
        color:green;
    }
    .txt-red{
        color:red;
    }
    .cuadro-ide {
        border: #4d326a solid 2px;
        border-radius: 11px;
        padding: 5px 20px;
        text-align: center;
        font-size: 20px;
    }
    .title-class {
        padding: 2% 0;
        text-align: center;
        color: #483a69;
    }
</style>
<div class="page-content-wrap">
    <?php if($auth['rol']=='TEACHER'):?>
        <a type="button" id="video" data-backdrop="static" data-keyboard="false" class="btn upload-file" data-toggle="modal" data-target="#helpworkload"  data-video="https://www.youtube.com/embed/EeL_aDsKr3g" target="_black"><img class="help-campus-gif" src="/dash/img/IconoIIDE.gif"></a>
    <?php endif;?>
    <div class=" container sinpadding">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12 fondo-gris-general cuadro">
                <?php if($auth['rol']=="STUDENT"):?>
                <div class="row">
                    <div class="panel panel-default clearfix">
                        <div class="panel-heading visible-xs">
                            <h2 class="panel-title">Nomenclatura</h2>
                            <br>
                            <br>
                            <p class="small">
                                V = Asistencia Virtual en Vivo,  D = Asistencia Virtual Diferida, I = Inasistenica                            
                            </p>
                        </div>
                        <div class="col-xs-12 toggle-header">
                            <!--div class="col-xs-1">
                                <span class="hidden-xs">Número </span>
                                <span class="visible-xs">#</span>
                            </div-->
                            <div class="col-xs-4 text-center">
                                <span class="hidden-xs">Nombre trabajo </span>
                                <span class="visible-xs">Trabajo</span>
                            </div>
                            <div class="col-xs-2 text-center">
                                 <span class="hidden-xs">Fecha de cargado</span>
                                <span class="visible-xs">F. Cargado</span>
                            </div>
                            <div class="col-xs-2">
                                <span class="hidden-xs">Archivo del Alumno </span>
                                <span class="visible-xs">Archivo</span>
                            </div>
                            <div class="col-xs-2 text-center">
                                <span class="hidden-xs">Archivo con observaciones </span>
                                <span class="visible-xs">Observación</span>
                            </div>
                            <div class="col-xs-1 text-center">
                                 <span class="hidden-xs">Cal. trabajo</span>
                                <span class="visible-xs">Calificación</span>
                            </div>
                        </div>
                        <div id="feature-1" class="collapse in">
                            <?php if($statuswork==1):?>
                            <?php foreach ($work as $key => $value):?>
                            <div class="panel-body">
                                <div class="row">
                                    <!--div class="col-xs-1">
                                        <?php echo $key+1;?>                                           
                                    </div-->
                                    <div class="col-xs-4 text-center">
                                        <?php echo $value->getName();?>
                                    </div>
                                    <div class="col-xs-2 text-center">
                                        <?php echo $value->getDateCreation();?>
                                    </div>
                                    <div class="col-xs-2 text-center">
                                        <a href="/documents/work/<?php echo $value->getFileteacher();?>" target="_black"><i class="fas fa-cloud-upload-alt txt-green">Ver</i></a>
                                    </div>
                                    <div class="col-xs-2 text-center">
                                        <?php if($value->getFilestudent()):?>
                                            <a href="/documents/work/<?php echo $value->getFilestudent();?>" target="_black"><i class="fas fa-cloud-upload-alt txt-green">descargar</i></a>
                                        <?php else:?>
                                            <p>Sin observar</p>
                                        <?php endif;?>
                                    </div>
                                    <div class="col-xs-1">
                                        -                                      
                                    </div>
                                </div>
                            </div>  
                        <?php endforeach;?>
                        <div class="clearfix"></div>
                            <br>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="col-md-6 col-sm-6 col-xs-12" style="">
                                    <div class="form-group">
                                        <div class="controls">
                                            <div class="col-md-4 col-sm-4 col-xs-4 col-lg-4">
                                                <label style="font-weight: 400;font-size: 20px;">Nombre del trabajo</label>
                                            </div>
                                            <div class="col-md-8 col-sm-8 col-xs-8 col-lg-8">
                                                <input type="text" name="namework" class="form-control hidden" id="namework">
                                                <input type="text" name="idclase" class="form-control hidden" id="idclase" value="<?php echo $idclase;?>">
                                                <select name="confid" id="confid" class="form-control select2">
                                                    <option value="">Selecciona la catedra</option>
                                                    <?php foreach($catedras as $key => $value):?>
                                                    <option value="<?= $value->getConfid();?>"><?=str_replace('PRÓXIMA ','',$value->getName());?></option>
                                                    <?php endforeach;?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-3 col-xs-12">
                                    <div class="form-group">
                                        <div class="controls">
                                            <div id="image-principal-dz" class="dropzone dz-default btn btn-primary" style="min-height: 10px;margin-left: 5%;line-height: 2px;          border: 1px solid black;background: #53266a;color: white;width: 100%;font-size: .8em;font-weight: bold;height: 38px;font-size: 17px;">
                                                <p class="text-center"  style="padding:10px 0 5px !important">SUBIR ARCHIVO
                                                    <input type="hidden" name="image" id="input-image-principal">
                                                </p>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-3 col-xs-12" style="margin-bottom: 5%;">
                                    <div class="form-group">
                                        <label class="form-label text-center" for="permalink">* El Archivo de preferencia .docs para realizar observaciones, tamaño no mayor a 25mb.</label>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </div>

                            <?php else:?>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <p>No hay trabajos</p>
                                    </div>
                                </div>
                            </div>
                            <?php endif;?> 
                        </div>
                    </div>    
                </div>
                <?php else:?>
                    <div class="row">
                        <div class="panel panel-default clearfix">
                            <div class="panel-heading visible-xs">
                                <h2 class="panel-title">Nomenclatura</h2>
                                <br>
                                <br>
                                <p class="small">
                                    V = Asistencia Virtual en Vivo,  D = Asistencia Virtual Diferida, I = Inasistenica                            
                                </p>
                            </div>
                            <div class="col-xs-12 toggle-header">
                                <!--div class="col-xs-1">
                                    <span class="hidden-xs">Número </span>
                                    <span class="visible-xs">#</span>
                                </div-->

                                <div class="col-xs-2 text-center">
                                    <span class="hidden-xs">Usuario </span>
                                    <span class="visible-xs">User</span>
                                </div>
                                <div class="col-xs-2 text-center">
                                    <span class="hidden-xs">Nombre trabajo </span>
                                    <span class="visible-xs">Trabajo</span>
                                </div>
                                <div class="col-xs-2 text-center">
                                     <span class="hidden-xs">Fecha de cargado</span>
                                    <span class="visible-xs">F. Cargado</span>
                                </div>
                                <div class="col-xs-2">
                                    <span class="hidden-xs">Archivo del Alumno </span>
                                    <span class="visible-xs">Archivo</span>
                                </div>
                                <div class="col-xs-1 text-center">
                                    <span class="hidden-xs">Archivo con observaciones </span>
                                    <span class="visible-xs">Observación</span>
                                </div>
                                <div class="col-xs-1 text-center">
                                     <span class="hidden-xs">Cal. trabajo</span>
                                    <span class="visible-xs">Calificación</span>
                                </div>
                                <div class="col-xs-1 text-center">
                                     <span class="hidden-xs">Calificar</span>
                                    <span class="visible-xs">Calificar</span>
                                </div>
                            </div>
                            <div id="feature-1" class="collapse in">
                                <?php if($statuswork==1):?>
                                <?php foreach ($work as $key => $value):?>
                                <div class="panel-body">
                                    <div class="row">
                                        <!--div class="col-xs-1">
                                            <?php echo $key+1;?>                                     
                                        </div-->
                                        <div class="col-xs-2 text-center">
                                            <?php echo $value->getNameUser()." ".$value->getLastName()." ".$value->getSecondName();?>
                                        </div>
                                        <div class="col-xs-2 text-center">
                                            <?php echo $value->getName();?>
                                        </div>
                                        <div class="col-xs-2 text-center">
                                            <?php echo $value->getDateCreation();?>
                                        </div>
                                        <div class="col-xs-2 text-center">
                                            <a href="/documents/work/<?php echo $value->getFileteacher();?>" target="_black"><i class="fas fa-cloud-upload-alt txt-green">Ver</i></a>
                                        </div>
                                        <div class="col-xs-1 text-center">
                                            <?php if($value->getFilestudent()):?>
                                                <a href="/documents/work/<?php echo $value->getFilestudent();?>" target="_black"><i class="fas fa-cloud-upload-alt txt-green">descargar</i></a>
                                            <?php else:?>
                                                <p>Sin observar</p>
                                            <?php endif;?>
                                        </div>
                                        <div class="col-xs-1">

                                            <?php if($value->getCalificacion()):?>
                                            <?php echo $value->getCalificacion();?>
                                            <?php else:?>
                                                - 
                                            <?php endif;?>
                                                                                 
                                        </div>

                                        <div class="col-xs-1 text-center">
                                            <?php if($value->getFilestudent()&&$value->getCalificacion()):?>
                                                <a  type="button" class="btn btn-primary upload-file" data-id="<?php echo $value->getWorkid();?>" data-toggle="modal" data-target="#exampleModal" target="_black" style="padding-right: 8px;"><i class="fas fa-edit">Observar</i></a>
                                            <?php else:?>
                                                <a  type="button" class="btn btn-primary upload-file" data-id="<?php echo $value->getWorkid();?>" data-toggle="modal" data-target="#exampleModal" target="_black" style="padding-right: 8px;"><i class="fas fa-edit">Actualizar</i></a>
                                            <?php endif;?>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach;?>
                                <?php else:?>
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <p>No hay trabajos</p>
                                        </div>
                                    </div>
                                </div>
                                <?php endif;?> 
                            </div>
                        </div>    
                    </div>
                <?php endif;?>

                <!--div class="accordion panel panel-default clearfix" id="accordionExample">
                  <div class="card">
                    <div class="card-header toggle-header" id="headingOne">
                      <h2 class="mb-0">
                        <button class="btn btn-link c-white" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                          Collapsible Group Item #1
                        </button>
                      </h2>
                    </div>

                    <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                      <div class="card-body">
                        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                      </div>
                    </div>
                  </div>
                </div-->

            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Cargar observaciones</h5>
      </div>
      <div class="modal-body">
        <div class="col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 5%;">
            <div class="form-group">
                <div class="controls">
                    <div class="col-md-4 col-sm-4 col-xs-4 col-lg-4">
                        <label>Calificación</label>
                    </div>
                    <div class="col-md-8 col-sm-8 col-xs-8 col-lg-8">
                        <input type="text" name="calificacion" value="" class="form-control" id="calificacion">
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-md-offset-4 col-sm-offset-4 col-sm-4 col-xs-12" style="margin-bottom: 5%;">
            <div class="form-group">
                <div class="controls">
                    <div id="image-principal-dz-teacher" class="dropzone dz-default btn btn-primary" style="min-height: 40px;">
                        <p class="text-center">Cargar Archivo
                            <input type="hidden" name="imageteacher" data-id="" id="input-image-principal-teacher"  value="">
                        </p>
                    </div>
                </div>
                <label class="form-label" for="permalink">*Archivo no mayor a 25mb, ingresa calificación primero:</label>
                <div class="clearfix"></div>
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="helpworkload" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document" style="width: 89%;">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel" style="font-size: 20px; color: #4c1368;text-align: center;">Tutorial</h5>
      </div>
      <div class="modal-body">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <iframe width="100%" height="443" src="" frameborder="0" allowfullscreen></iframe>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
