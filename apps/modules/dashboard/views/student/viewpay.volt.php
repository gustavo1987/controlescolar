<div class="page-content-wrap">
    <div class="container sinpadding">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12 fondo-gris-general">
                <div class="panel panel-success">
                    <div class="panel-heading">
                        <div class="col-md-9 col-sm-9 col-lg-9">
                            <h3 class="panel-title">Lista de pagos de <?php echo $client->getName()." ".$client->getLastname()." ".$client->getSecondname();?></h3>
                            <br>
                            <br>
                            <p class="panel-title">Fecha de inicio: <?php echo $client->getDateCreation();?></p>
                        </div>
                        <div class="col-md-3 col-sm-3 col-lg-3">
                            <a href="<?= $this->url->get('dashboard/student/addpay/' . $client->getClid()) ?>" class="btn btn-success btn-lg">Crear nuevo pago</a>
                        </div>  
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table id="tableProducts" class="table table-bordered table-striped table-actions generalDT" data-order="0" data-filter="desc">
                                <thead>
                                <tr>
                                    <th>Número</th>
                                    <th>Pago</th>
                                    <th>Cantidad</th>
                                    <th>Fecha</th>
                                    <th>Status</th>
                                    <th>Opciones</th>
                                </tr>
                                </thead>
                                <tbody>
                            	<?php if($status==1):?>
                                <?php foreach ($pagos as $key => $value):?>
                                <tr class="<?= $value->getPagid() ?>" data-id="<?= $value->getPagid() ?>" data-clid="<?=$client->getClid();?>">
                                    <td><?php echo $key+1; ?></td>
                                    <td><?= $value->getName() ?></td>
                                    <td><?= $value->getCantidad() ?></td>
                                    <td><?= $value->getVencimiento() ?></td>
                                    <td><?= $value->getStatus() ?></td>
                                    <td>
                                        <a class="deletepay"><span class="fas fa-trash-alt fa-2x"></span></a>
                                        <?php if($value->getView()=="YES"):?>
                                        <a class="viewpay" data-status="NO" data-id="<?= $value->getPagid() ?>" data-clid="<?=$client->getClid();?>"><span class="fas fa-eye-slash fa-2x"></span></a>
                                        <?php else:?>
                                        <a class="viewpay" data-status="YES" data-id="<?= $value->getPagid() ?>" data-clid="<?=$client->getClid();?>"><span class="fas fa-eye fa-2x"></span></a>
                                        <?php endif;?>
                                    </td>
                                </tr>
                                <?php endforeach;?>
                                <?php elseif($status==0):?>
                                	<td>No hay pagos</td>
                            	<?php endif;?>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <?php if($statusrecibo=="ACTIVO"):?>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table id="tableProducts" class="table table-bordered table-striped table-actions generalDT" data-order="0" data-filter="desc">
                                <thead>
                                <tr>
                                    <th>Número</th>
                                    <th>Pago</th>
                                    <th>Cantidad</th>
                                    <th>Fecha</th>
                                    <th>Status</th>
                                    <th>Recibo</th>
                                    <th>Opciones</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php if($status==1):?>
                                <?php foreach ($recibosall as $key => $value):?>
                                <tr class="<?= $value->getPagid() ?>" data-id="<?= $value->getPagid() ?>" data-clid="<?=$client->getClid();?>">
                                    <td><?php echo $key+1; ?></td>
                                    <td><?= $value->getName() ?></td>
                                    <td><?= $value->getCantidad() ?></td>
                                    <td><?= $value->getVencimiento() ?></td>
                                    <td><?= $value->getStatus() ?></td>
                                    <td><a target="_blank" href="/documents/recibos/<?= $value->getDocument() ?>"><span class="fa fa-eye fa-2x"></span>Recibo</a></td>
                                    <td>
                                        <a class="deletepay"><span class="fas fa-trash-alt fa-2x"></span>
                                        Eliminar</a>
                                    </td>
                                </tr>
                                <?php endforeach;?>
                                <?php elseif($status==0):?>
                                    <td>No hay pagos</td>
                                <?php endif;?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <?php endif;?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="message-box animated fadeIn" data-sound="alert" id="mb-remove-row">
    <div class="mb-container">
        <div class="mb-middle">
            <div class="mb-title"><span class="fa fa-times"></span> Eliminar <strong>Datos</strong> ?</div>
            <div class="mb-content">
                <p>¿Estas seguro de eliminar esta fila?</p>
                <p>Presione "Si" si esta seguro.</p>
            </div>
            <div class="mb-footer">
                <div class="pull-right">
                    <button class="btn btn-success btn-lg mb-control-yes">Si</button>
                    <button class="btn btn-default btn-lg mb-control-close">No</button>
                </div>
            </div>
        </div>
    </div>
</div>