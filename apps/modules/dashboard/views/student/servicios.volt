

<div class="page-content-wrap">
    <div class=" container sinpadding">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12 fondo-gris-general cuadro">



        <div class="panel panel-default panel-table">
          <div class="panel-heading">
            <div class="row">
              <div class="col col-xs-6">
                <h3 class="panel-title">Tramites</h3>
              </div>
              <!--div class="col col-xs-6 text-right">
                <button type="button" class="btn btn-sm btn-primary btn-create">Create New</button>
              </div-->
            </div>
          </div>
          <div class="panel-body">
            <table class="table table-striped table-bordered table-list">
              <thead>
                <tr>
                    <th class="hidden-xs">ID</th>
                    <th>Tramite</th>
                    <th><em class="fa fa-cog"></em></th>
                </tr> 
              </thead>
              <tbody>
                      <tr>
                        <td class="hidden-xs">1</td>
                        <td>Título</td>
                        <td align="center">
                          <a class="btn btn-danger"><em class="fas fa-user-check"></em></a>
                        </td>
                      </tr>
                      <tr>
                        <td class="hidden-xs">2</td>
                        <td>Cedula</td>
                        <td align="center">
                          <a class="btn btn-danger"><em class="fas fa-user-check"></em></a>
                        </td>
                      </tr>
                      <tr>
                        <td class="hidden-xs">3</td>
                        <td>Certificado de Estudios</td>
                        <td align="center">
                          <a class="btn btn-danger"><em class="fas fa-user-check"></em></a>
                        </td>
                      </tr>
                    </tbody>
            </table>
          </div>
          <div class="panel-footer">
          </div>
        </div>
      </div>
    </div>
  </div>
</div>