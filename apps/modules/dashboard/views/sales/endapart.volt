<div class="page-content-wrap" id="sales">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-success">
                <!--div class="panel-heading">
                    <div class="col-sm-3"><h3 class="panel-title">Estatus de apartado:</h3></div>
                    <div class="col-sm-3"><span class="fa fa-cube fa-2x text-success"></span>&nbsp;Pagado</div>
                    <div class="col-sm-3"><span class="fa fa-cube fa-2x text-warning"></span>&nbsp;En proceso</div>
                </div-->
                <div class="panel-body">
                    <div class="table-responsive">
                        <table id="tableSales" class="table table-bordered table-hover table-actions dataTable" data-order="0" data-filter="desc">
                            <thead>
                            <tr>
                                <th>#Apartado</th>
                                <th>Total</th>
                                <th>Abonado</th>
                                <th>Restante</th>
                                <th>Vendedor</th>
                                <th>Fecha</th>
                                <th>Opciones</th>
                            </tr>
                            </thead>
                            <tbody>



                            <?php foreach($apartAll as $values):?>
                                <?php
                                    $status_total = $values['total']<=0?"info":"warning";
                                    $apart = $values['apart'];
                                    $total = $values['total_clean'];

                                    $today = date("Y-m-d");
                                    $apart = $values['date_creation'];
                                    $apart2 = new \DateTime($apart);
                                    $apart3 =$apart2->format('Y-m-d');
                                    $datetime1 = date_create($today);
                                    $datetime2 = date_create($apart3);
                                    $interval2 = date_diff($datetime1, $datetime2);
                                    $result = $interval2->format('%R%a');
                                    /*print($result);*/
                                    $mes = -30;
                                    if ($result<=$mes){
                                        /*$status = "vencido";
                                        print($status);*/


                                ?>
                                <tr class='<?=$values["status"]=="Pagado"?"success":$status_total?>'>
                                    <td><?=$values['salid']?></td>
                                    <td class='text-right'>$<?=$values['total'];?></td>
                                    <td class='text-right'>$<?=number_format($apart,"2",".",",")?></td>
                                    <td>$<?=number_format($total-$apart,"2",".",",")?></td>
                                    <td><?=$values['user']?></td>
                                    <td><?=$values['date']?></td>
                                    <td>
                                        <?php if($values["status"]=="Pagado"):?>
                                            <a onclick="printPage('<?=$values['url_print']?>')" data-toggle='tooltip' data-placement='top' title='Imprimir comprobante de venta'><span class='fa fa-print fa-2x'></span></a>&nbsp;<a target='_blank' href="<?=$values['url_view']?>" data-toggle='tooltip' data-placement='top' title='Ver comprobante de venta'><span class='fa fa-eye fa-2x'></span></a>&nbsp;<a target="_blank" href="<?=$this->url->get("dashboard/sales/completed/{$values['salid']}")?>" data-toggle="tooltip" data-placement="top" title="" data-original-title="Información general de venta"><span class="fa fa-folder-open fa-2x"></span></a>
                                        <?php elseif($values['total']<=0):?>
                                            <a target="_blank" href="<?=$this->url->get("dashboard/sales/number/{$values['salid']}")?>" data-toggle="tooltip" data-placement="top" title="" data-original-title="Editar venta"><span class="fa fa-edit fa-2x"></span></a>
                                        <?php else:?>
                                            <a target="_blank" href="<?=$this->url->get("dashboard/sales/completedapart/{$values['salid']}")?>" data-toggle="tooltip" data-placement="top" title="" data-original-title="Información general de venta"><span class="fa fa-folder-open fa-2x"></span></a>&nbsp;<!--a target="_blank" href="<?=$this->url->get("dashboard/sales/number/{$values['salid']}")?>" data-toggle="tooltip" data-placement="top" title="" data-original-title="Editar venta"><span class="fa fa-edit fa-2x"></span></a-->
                                        <?php endif;?>
                                    </td>
                                </tr>
                                <?php } ?>
                            <?php endforeach;?>
                            </tbody>
                        </table>
                    </div>
                    <!--div class="row">
                        <div class="col-md-6">
                        </div>
                        <div class="col-md-6">
                            <h4>Monto total del día</h4>

                            <table class="table table-striped">
                                <tbody><tr>
                                    <td width="200"><strong>Subtotal:</strong></td><td class="text-right" id="subtotal">$ <?=$breakdown['subtotal']?></td>
                                </tr>
                                <tr id="iva" data-value="16">
                                    <td><strong>IVA 16%:</strong></td><td class="text-right" id="iva_general">$ <?=$breakdown['iva_general']?></td>
                                </tr>
                                <tr class="total">
                                    <td>Total de las ventas</td><td class="text-right" id="amount_to_pay" data-value=""><?=$breakdown['total']?></td>
                                </tr>
                                </tbody></table>
                        </div>
                    </div-->
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function closePrint () {
        document.body.removeChild(this.__container__);
    }

    function setPrint () {
        this.contentWindow.__container__ = this;
        this.contentWindow.onbeforeunload = closePrint;
        this.contentWindow.onafterprint = closePrint;
        this.contentWindow.focus(); // Required for IE
        this.contentWindow.print();
    }

    function printPage (sURL) {
        var oHiddFrame = document.createElement("iframe");
        oHiddFrame.onload = setPrint;
        oHiddFrame.style.visibility = "hidden";
        oHiddFrame.style.position = "fixed";
        oHiddFrame.style.right = "0";
        oHiddFrame.style.bottom = "0";
        oHiddFrame.src = sURL;
        document.body.appendChild(oHiddFrame);
    }
</script>