<?php $url = $this->url->get('dashboard/sales/print/'.$data->getSalid())?>
<?php if($status==0):?>
    <div class="page-content-wrap" id="sales">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <span class="hidden" id="uid" data-value="{{data.getUid()}}"></span>
                    <span class="hidden" id="salid" data-value="{{data.getSalid()}}"></span>
                    <span class="hidden" id="clid" data-value="{{data.getClid()}}"></span>
                    <span class="hidden" id="email" data-value="{{data.getEmail()}}"></span>
                    <div class="panel-body">
                        <h2>Estatus  <strong>de Clase</strong></h2>
                        <div class="invoice">
                            <div class="row">
                                <div class="col-md-6">
                                    <label for="">Materia a elegir</label>
                                    <select class="form-control select_product_sale" data-placeholder="Buscar Materia">
                                    </select>
                                </div>
                                <div class="col-md-6">
                                    <label for="">Maestro</label>
                                    <select name="employed" id="employed"  class="form-control select_employed" data-placeholder="Buscar Maestro">
                                    </select>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <hr>
                            <?php if($data->getEmailClient()=="ventas@beautyroom.com"):?>
                            <h4 class="type_sales" style="font-weight: lighter"><strong class="name_user_active">Clase</strong></h4>
                            <?php else:?>
                            <h4 class="type_sales" style="font-weight: lighter">Bienvenido nuevamente: <strong class="name_user_active"><?=$data->getNameClient()." ".$data->getLastNameClient()." ".$data->getSecondNameClient()?></strong></h4>
                            <?php endif;?>
                            <div class="">
                                <table class="table table-bordered" id="tableSales">
                                    <thead>
                                    <tr>
                                        <th class="success">&nbsp;</th>
                                        <th class="success">Descripción</th>
                                        <th class="success text-center">Credito</th>
                                        <th class="success text-center">Generación</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php if(count($list)>=1):?>
                                    <?php $calculate = 0; foreach($list as $val): $subsalid = $val->getSubsalid()?>
                                    <?php if($val->getType()=="VENTAS"):?>
                                    <tr data-type="product_sale" data-value="<?=$subsalid;?>" class="<?=$subsalid;?> info">
                                        <td class="text-center">
                                            <span class="deleteElement cursor_pointer" data-toggle="tooltip" data-placement="top" title="" data-original-title="Eliminar clase"><i class="fa fa-remove fa-2x"></i></span>
                                        </td>
                                        <td class="description">
                                            <p><?=$val->getTicket()?></p>
                                            <strong class="name_service"><?=$val->getNameBrand()?></strong>
                                            <p class="name_sub_service"><?=$val->getNameProduct()?>
                                                
                                            </p>
                                        </td>
                                        <td class="text-center price_service" data-value="<?=$val->getSalePrice()?>"><?=$val->getSalePrice()?></td>
                                        <?php $discountt = $val->getPercentage()?>

                                        <td class="text-center discount_service">
                                                <select id="generacion" name="generacion" class="form-control discountt">

                                                    <option  <?=$val->getPercentage()==0?"selected":"";?> value="0">Seleccionar Generación</option>
                                                    <?php foreach($generaciones as $value):?>
                                                        <option name="generacion" value="<?= $value->getGenid();?>"><?= $value->getName();?></option>
                                                    <?php endforeach;?>
                                                    
                                                </select>
                                            <?php $discountt = 1?>
                                        </td>
                                        <?php
                                                    $total = 99999;
                                                    $calculate = $calculate+$total;
                                                    ?>
                                        <td class="text-center total_service hidden" data-discount="<?=$discountt?>" data-value="9999999">$<?=$total?></td>
                                    </tr>
                                    <?php endif;?>
                                    <?php endforeach?>
                                    <?php endif;?>
                                    </tbody>
                                </table>
                            </div>
                            <div class="row hidden">
                                <div class="col-md-6">
                                </div>
                                <div class="col-md-6">
                                    <h4>Monto a pagar</h4>
                                    <?php
                                        $total = round($calculate,2);
                                        $iva =  round(($total/1.16)*.16,2);
                                        $subtotal = round($total - $iva,2);
                                    ?>
                                    <table class="table table-striped">
                                        <tbody><tr>
                                            <td width="200"><strong>Subtotal:</strong></td><td class="text-right" id="subtotal"><?= $subtotal?></td>
                                        </tr>
                                        <tr id="iva" data-value="16"><td><strong>IVA 16%:</strong></td><td class="text-right iva_general">$<?=number_format($iva, "2", ".", "") ?></td>
                                        
                                        <tr class="total">
                                            <?php $pay = number_format($total,"2",".","")?>
                                            <td>Total a pagar:</td><td class="text-right" id="amount_to_pay" data-value="<?=$pay?>">$<?=$pay?></td>
                                        </tr>
                                        </tbody></table>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="pull-right push-down-20">
                                        <button class="btn btn-danger" id="cancel_checkout"><span class="fa fa-remove"></span> Cancelar clase</button>
                                        <button class="btn btn-success" id="checkout"><span class="fa fa-credit-card"></span> Asignar clase</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class="message-box animated fadeIn" data-sound="alert" id="mb-remove-row">
        <div class="mb-container">
            <div class="mb-middle">
                <div class="mb-title"><span class="fa fa-times"></span> Eliminar <strong>Datos</strong> ?</div>
                <div class="mb-content">
                    <p>¿Estas seguro de eliminar esta fila?</p>
                    <p>Presione "Si" si esta seguro.</p>
                </div>
                <div class="mb-footer">
                    <div class="pull-left fade in" id="status_delete">
                        <button class="btn btn-success btn-lg mb-control-yes">Si</button>
                        <button class="btn btn-default btn-lg mb-control-close">No</button>
                    </div>
                    <div class="pull-right fade hidden" id="message_delete">
                        <div class="mb-title"><span class="fa fa-spinner fa-pulse fa-3x fa-fwe"></span> Eliminando</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal  bs-example-modal-lg" id="modalStylist" tabindex="-1" role="dialog" aria-labelledby="defModalHead" aria-hidden="true" style="display: none;" aria-labelledby="myLargeModalLabel">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Cerrar</span></button>
                    <h4 class="modal-title" id="defModalHead">Agregar Vendedora</h4>
                    <form id="formServices" action="#" method="post" class="form-horizontal">
                        <div class="modal-body">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <input type="hidden" name="type_id" id="type_id" value="">
                                    <input type="hidden" name="type_status" id="type_status" value="">
                                    <select name="user_services" id="user_services" class="form-control" required multiple data-placeholder="Seleccionar el Vendedora">
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
                            <input type="submit" class="btn btn-info btn-submit-sub-services" value="Guardar">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="modal  bs-example-modal-lg" id="modalPulledApart" tabindex="-1" role="dialog" aria-labelledby="defModalHead" aria-hidden="true" style="display: none;" aria-labelledby="myLargeModalLabel">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Cerrar</span></button>
                    <h4 class="modal-title">Realizar apartado</h4>
                </div>
                <div class="modal-body" id="modal_body">
                    <form action="#" method="post" id="pulled_apart_form">
                        <div class="row">
                            <div class="col-sm-offset-2 col-sm-4"><h3>Total a pagar</h3></div>
                            <div class="col-sm-offset-2 col-sm-4"><h3><span id="debt_apart" data-pay="0">$0</span>&nbsp;<span id="discount_debt_apart" data-discount=""></span></h3></div>
                            <input class="hidden" type="" value="1" id="generacion-seleccionada" name="generacion">
                        </div>
                        <div class="row">
                            <br>
                            <div class="col-sm-offset-2 col-sm-4"><h3>Efectivo Recibido($)</h3></div>
                            <div class="col-sm-offset-2 col-sm-4"><input type="number" min="0" step="0.01" value="" id="received_apart" name="received_apart" placeholder="$" class="form-control"></div>
                        </div>
                        <div class="row">
                            <br>
                            <div class="col-sm-offset-2 col-sm-4"><h3>Restante a pagar($)</h3></div>
                            <div class="col-sm-offset-2 col-sm-4"><h3 class="btn btn-info btn-lg" id="vuelto_cliente_apart">$0</h3></div>
                        </div>
                        <div class="row">
                            <br>
                            <div class="col-sm-offset-8 col-sm-4 text-right">
                                <input type="radio" name="typePay" id="typePay" value="EFECTIVO"><strong>&nbsp; Efectivo</strong>
                                <input type="radio" name="typePay" id="typePay" value="CREDITO"><strong>&nbsp; Credito</strong>
                                <input type="radio" name="typePay" id="typePay" value="AMBOS"><strong>&nbsp; Ambos</strong>
                            </div>
                        </div>
                        <div class="row">
                            <br>
                            <div class="col-sm-offset-2 col-sm-4"><h3>Nombre Cliente</h3></div>
                            <div class="col-sm-offset-2 col-sm-4"><input type="text" id="name_client" name="name_client" placeholder="Nombre del Cliente" class="form-control"></div>
                        </div>
                        <div class="row">
                            <br>
                            <div class="col-sm-offset-2 col-sm-4"><h3>Número de Teléfono</h3></div>
                            <div class="col-sm-offset-2 col-sm-4"><input type="text" id="phone_client" name="phone_client" placeholder="Número de Teléfono" class="form-control"></div>
                        </div>
                        <div class="row">
                            <br>
                            <div class="col-sm-offset-6 col-sm-6 text-right">
                                <button type="submit" class="btn btn-success btn-md disabled" id="buttonPay_apart" disabled><span class="fa fa-dollar"></span>&nbsp;Apartar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="modal  bs-example-modal-lg" id="modalPay" tabindex="-1" role="dialog" aria-labelledby="defModalHead" aria-hidden="true" style="display: none;" aria-labelledby="myLargeModalLabel">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Cerrar</span></button>
                    <h4 class="modal-title" id="defModalHead">Confirmar creación de clase</h4>
                </div>
                <div class="modal-body" id="modal_body">
                    <form action="#" method="post" id="pay_form_action">
                        <div class="row">
                            <div class="col-sm-offset-2 col-sm-4"><h3>Creditos</h3></div>
                            <div class="col-sm-offset-2 col-sm-4"><h3><span id="debt" data-pay="0">0</span>&nbsp;<span id="discount_debt" data-discount=""></span></h3></div>
                        </div>
                        <div class="row hidden">
                            <br>
                            <div class="col-sm-offset-2 col-sm-4"><h3>Vale de Descuento($)</h3></div>
                            <div class="col-sm-offset-2 col-sm-4"><input type="number" min="0" value="" id="discount_ext" name="discount_ext" data-id="" placeholder="$" class="form-control" readonly></div>
                        </div>
                        <div class="row hidden">
                            <br>
                            <div class="col-sm-offset-2 col-sm-4"><h3>Número de Cupon(#)</h3></div>
                            <div class="col-sm-offset-2 col-sm-4"><input type="number" min="0" value="" id="number_cupon" name="number_cupon" placeholder="#" class="form-control"></div>
                        </div>
                        <div class="row hidden">
                            <br>
                            <div class="col-sm-offset-2 col-sm-4"><h3>Efectivo Recibido($)</h3></div>
                            <div class="col-sm-offset-2 col-sm-4"><input type="number" min="0" step="0.01" value="100" id="received" name="received" placeholder="$" class="form-control"></div>
                        </div>
                        <div class="row hidden">
                            <br>
                            <div class="col-sm-offset-2 col-sm-4"><h3>Vuelto del cliente($)</h3></div>
                            <div class="col-sm-offset-2 col-sm-4"><h3 class="btn btn-info btn-lg" id="vuelto_cliente">$0</h3></div>
                        </div>
                        <div class="row hidden">
                            <br>
                            <div class="col-sm-offset-8 col-sm-4 text-right">
                                <input type="text" name="typePay" id="typePay" value="EFECTIVO"><strong>&nbsp; Efectivo</strong>
                            </div>
                        </div>
                        <div class="row">
                            <br>
                            <div class="col-lg-6 col-md-6 col-sm-6 text-right">
                                <h3>¿Esta seguro de guardar la clase?</h3>
                            </div>
                        </div>
                        <div class="row">
                            <br>
                            <div class="col-sm-offset-6 col-sm-6 text-right">
                                <button type="submit" class="btn btn-success btn-md" id="buttonPay">&nbsp;Si</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="message-box message-box-info animated fadeIn" id="message-box-process">
        <div class="mb-container">
            <div class="mb-middle">
                <div class="mb-title"> Procesando el pago &nbsp; <i class="fa fa-circle-o-notch fa-spin style-fa"></i></div>
                <div class="mb-content">
                    <p>Estamos realizando el pago, espere un momento por favor.</p>
                </div>
            </div>
        </div>
    </div>
    <div class="message-box message-box-info animated fadeIn" id="message-box-process-pulled-apart">
        <div class="mb-container">
            <div class="mb-middle">
                <div class="mb-title"> Procesando el apartado &nbsp; <i class="fa fa-circle-o-notch fa-spin style-fa"></i></div>
                <div class="mb-content">
                    <p>Estamos realizando el apartado, espere un momento por favor.</p>
                </div>
            </div>
        </div>
    </div>
    <div class="message-box message-box-success animated fadeIn" id="message-box-pay">
        <div class="mb-container">
            <div class="mb-middle">
                <div class="mb-title"><span class="fa fa-check"></span> Pago realizado correctamente</div>
                <div class="mb-content">
                    <p>Estamos generando su ticket de venta, gracias.</p>
                </div>
            </div>
        </div>
    </div>
    <div class="message-box message-box-success animated fadeIn" id="message-box-pulledApart">
        <div class="mb-container">
            <div class="mb-middle">
                <div class="mb-title"><span class="fa fa-check"></span> Apartado realizado correctamente</div>
                <div class="mb-content">
                    <p>Estamos generando su ticket de apartado, gracias.</p>
                </div>
            </div>
        </div>
    </div>
    <div class="message-box message-box-danger animated fadeIn" data-sound="alert" id="message-box-cancel">
        <div class="mb-container">
            <div class="mb-middle">
                <div class="mb-title"><span class="fa fa-remove"></span> Cancelar esta <strong>venta</strong> ?</div>
                <div class="mb-content">
                    <p>¿Estas seguro de cancelar esta venta?</p>
                    <p>Pulse <strong>"No"</strong> para continuar vendiendo. Pulse <strong>"Sí"</strong> para cancelarla.</p>
                </div>
                <div class="mb-footer">
                    <div class="pull-left  fade in" id="buttons_cancel">
                        <a href="#" id="cancel_sale" class="btn btn-success btn-lg">Si</a>
                        <button class="btn btn-default btn-lg mb-control-close">No</button>
                    </div>
                    <div class="pull-right fade hidden" id="message_cancel">
                        <div class="mb-title"><span class="fa fa-spinner fa-pulse fa-3x fa-fwe"></span> Cancelando</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php elseif($status==2):?>
<div class="page-content-wrap">
<button class="btn btn-success" onclick="printPage('<?=$url?>')">Imprimir</button>
    <div class="content" id="content">
        <div style="margin: 0 auto; padding: 20px; width: 900px; font-weight: normal;">
            <div style="width: 900px; height: 400px;">
                <div style="width: 900px; height: 400px; float: left;">
                    <center><div style="font:bold 150px &#39;Arial&#39;;">PONS</div><br>
                        <div style="font:bold 75px &#39;Arial&#39;;">COLLECTION</div>
                        <br>
                        <div style="font:bold 50px &#39;Arial&#39;;">RFC MAPJ850930CU5</div><br><br>
                        <div style="text-align:left; font-size:40px; height: 400px;"> Av. Ramón Mendoza 102 Int D6 Col. José<br> María Pino Suárez<br> CP 86029
                        </div>
                    </center>
                    <div>
                    </div>
                </div>
                <div style="width: 900px; float: left; height: 70px; margin-top:130px;">
                    <table cellpadding="3" cellspacing="0" style="font-family: arial; font-size: 30px;text-align:left;width : 100%; font-weight:bold;">
                        <tbody><tr>
                            <td>OR No. :</td>
                            <td><?=$data->getSalid()?></td>
                        </tr>
                        <tr>
                            <td>Fecha :</td>
                            <?php $fecha2 = $data->getDateCreationSale();
                            $vencimiento2 = strtotime('+1 seconds', strtotime($fecha2));
                            $vencimiento2 = date('d-m-Y h:m', $vencimiento2);
                            ?>
                            <td><?= $vencimiento2; ?></td>
                        </tr>
                        <?php if($status==2):?>
                            <tr>
                                <td>Nombre:</td>
                                <td><?=$data->getName()?></td>
                            </tr>
                            <tr>
                                <td>Teléfono: </td>
                                <td><?=$data->getPhone()?></td>
                            </tr>
                            <?php $fecha = $data->getDateCreationSale();
                            $vencimiento = strtotime('+1 month', strtotime($fecha));
                            $vencimiento = date('d-m-Y h:m', $vencimiento);
                            ?>
                            <tr>
                                <td>Vence:</td>
                                <td><?=$vencimiento?></td>
                            </tr>
                            <br>
                        <?php endif;?>
                        </tbody></table>
                </div>
                <div class="clearfix"></div>
            </div>
            <div style="width: 900px;; margin-top:300px;">
                <table border="0" cellpadding="4" cellspacing="0" style="font-family: arial; font-size: 40px;	text-align:left;" width="860">
                    <thead style="text-align:center;">
                    <tr>
                        <th height="45" colspan="2"> Articulo </th>
                        <th width="152"> Cant. </th>
                        <th width="209"> Precio </th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $total = (int) 0; $discount = (int) 0;?>
                    <?php foreach($list as $key => $val):?>
                    <?php $total = $total + $val['total_payout'];?>
                    <?php if($val['type']=="VENTAS"):?>
                    <tr>
                        <td height="45" colspan="2"><strong><?=$val['name_brand'];?> &nbsp; <?=$val['name_product'];?> </strong></td>
                        <td width="152"><?=$val['quantity_services']?></td>
                        <td width="209">$<?=number_format($val['total_payout'],"2",".",",")?></td>
                        <?php $discount = $discount + $val['percentage_product']; ?>
                    </tr>
                    <?php endif?>
                    <?php endforeach;?>
                    </tbody>
                </table>
                <br><br><br><br>
                <table style="margin-left:450px;">
                    <?php
                        $iva =  round(($data->getTotal()/1.16)*.16,2);
                        $subtotal = round($data->getTotal() - $iva,2);
                    ?>
                    <tbody>
                    <tr style="height:40px;">
                        <td colspan="5" style=" text-align:right;"><strong style="font-size: 30px;">Subtotal: &nbsp;</strong></td>
                        <td> <strong style="font-size: 30px;">$<?=number_format($subtotal,"2",".",",")?></strong></td>
                    </tr>
                    <tr style="height:40px;">
                        <td colspan="5" style=" text-align:right;"><strong style="font-size: 30px;">IVA: &nbsp;</strong></td>
                        <td><strong style="font-size: 30px;">$<?=number_format($iva,"2",".",",")?></strong></td>
                    </tr>
                    <?php if($status==1):?>
                        <tr>
                            <td colspan="5" style=" text-align:right;"><strong style="font-size: 50px;">Total:&nbsp;</strong></td>
                            <td colspan="2"><strong style="font-size: 50px;">$<?=number_format($total,"2",".",",")?></strong></td>
                        </tr>
                        <?php $nDiscount =0; if($data->getDiscount()>0):?>
                            <tr>
                                <td colspan="5" style=" text-align:right;"><strong style="font:bold 50px 'Arial';">Tarjeta R:&nbsp;</strong></td>
                                <td colspan="2"><strong style="font:bold 50px 'Arial';">$<?=number_format($data->getDiscount(),"2",".",",")?></strong></td>
                            </tr>
                            <?php $nDiscount = $data->getDiscount();?>
                        <?php endif;?>
                        <tr style="height:54px;">
                            <td colspan="5" style=" text-align:right;"><strong style="font-size: 50px; color: #222222;">
                                    <font style="font-size:50px;">
                                        Cambio:&nbsp;
                                    </font></strong></td>
                            <td colspan="2"><strong style="font-size: 50px; color: #222222;">
                                    $<?=number_format(($data->getReceived()+$nDiscount-$total)<0?0:($data->getReceived()+$nDiscount-$total),"2",".",",")?>					</strong></td>
                        </tr>
                    <?php elseif($status==2):?>
                        <tr>
                            <td colspan="5" style=" text-align:right;"><strong style="font-size: 50px;">Total:&nbsp;</strong></td>
                            <td colspan="2"><strong style="font-size: 50px;">$<?=number_format($data->getTotal(),"2",".",",")?></strong></td>
                        </tr>
                        <tr style="height:54px;">
                            <td colspan="5" style=" text-align:right;"><strong style="font-size: 50px; color: #222222;">Pagos:&nbsp;</strong></td>
                            <td colspan="2"><strong style="font-size: 50px; color: #222222;">
                                    $<?=number_format($data->getReceived(),"2",".",",")?>					</strong></td>
                        </tr>
                        <tr style="height:54px;">
                            <td colspan="5" style=" text-align:right;"><strong style="font-size: 50px; color: #222222;">
                                    <font style="font-size:50px;">
                                        Deuda:&nbsp;
                                    </font></strong></td>
                            <td colspan="2"><strong style="font-size: 50px; color: #222222;">
                                    $<?=number_format($data->getTotal()-$data->getReceived(),"2",".",",")?>					</strong></td>
                        </tr>
                    <?php endif;?>
                    </tbody>
                </table>
                <table with="400" align="left">
                    <tbody><tr>
                        <td><h1 style="font-size:40px;">Vigencia de apartado 30 días, no hay cambios ni devolución.</h1></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function closePrint () {
        document.body.removeChild(this.__container__);
    }
    function setPrint () {
        this.contentWindow.__container__ = this;
        this.contentWindow.onbeforeunload = closePrint;
        this.contentWindow.onafterprint = closePrint;
        this.contentWindow.focus(); // Required for IE
        this.contentWindow.print();
    }
    function printPage (sURL) {
        var oHiddFrame = document.createElement("iframe");
        oHiddFrame.onload = setPrint;
        oHiddFrame.style.visibility = "hidden";
        oHiddFrame.style.position = "fixed";
        oHiddFrame.style.right = "0";
        oHiddFrame.style.bottom = "0";
        oHiddFrame.src = sURL;
        document.body.appendChild(oHiddFrame);
    }
    printPage("<?=$url?>");
</script>
<?php else:?>
<div class="page-content-wrap">
<button class="btn btn-success" onclick="printPage('<?=$url?>')">Imprimir</button>
    <div class="content" id="content">
        <div style="margin: 0 auto; padding: 20px; width: 900px; font-weight: normal;">
            <div style="width: 900px; height: 400px;">
                <div style="width: 900px; height: 400px; float: left;">
                    <center><div style="font:bold 150px &#39;Arial&#39;;">IDEI</div><br>
                        <div style="font:bold 75px &#39;Arial&#39;;">Iberoamerica</div>
                        <br>
                        <div style="font:bold 50px &#39;Arial&#39;;">RFC IDEI101018</div><br><br>
                        <div style="text-align:left; font-size:40px; height: 400px;"> Distrito Federal<br> México<br> CP 86029
                        </div>
                    </center>
                    <div>
                    </div>
                </div>
                <div style="width: 300px; float: left; height: 70px; margin-top:150px;">
                    <table cellpadding="3" cellspacing="0" style="font-family: arial; font-size: 30px;text-align:left;width : 100%; font-weight:bold;">
                        <tbody>
                            <tr>
                                <td>OR No. :</td>
                                <td><?=$data->getSalid()?></td>
                            </tr>
                            <tr>
                                <td>Fecha :</td>
                                <?php $fecha3 = $data->getDateCreationSale();
                                $vencimiento3 = strtotime('+1 seconds', strtotime($fecha3));
                                $vencimiento3 = date('d-m-Y h:m', $vencimiento3);
                                ?>
                                <td><?= $vencimiento3 ?></td>
                            </tr>
                            <?php if($status==2):?>
                                <tr>
                                    <td>Nombre :</td>
                                    <td><?=$data->getName()?></td>
                                </tr>
                                <tr>
                                    <td>Teléfono :</td>
                                    <td><?=$data->getPhone()?></td>
                                </tr>
                                <?php $fecha = $data->getDateCreationSale();
                                $vencimiento = strtotime('+1 month', strtotime($fecha));
                                $vencimiento = date('d-m-Y h:m:s', $vencimiento);
                                ?>
                                <tr>
                                    <td>Vence:</td>
                                    <td><?=$vencimiento?></td>
                                </tr>
                                <br>
                            <?php endif;?>
                        </tbody>
                    </table>
                </div>
                <div class="clearfix"></div>
            </div>
            <div style="width: 900px;; margin-top:300px;">
                <table border="0" cellpadding="4" cellspacing="0" style="font-family: arial; font-size: 40px;	text-align:left;" width="860">
                    <thead style="text-align:center;">
                    <tr>
                        <th height="45" colspan="2"> Materia </th>
                        <th width="209"> Credito </th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $total = (int) 0; $discount = (int) 0;?>
                    <?php foreach($list as $key => $val):?>
                    <?php $total = $total + $val['total_payout'];?>
                    <?php if($val['type']=="VENTAS"):?>
                    <tr>
                        <td height="45" colspan="2"><strong><?=$val['name_brand'];?> &nbsp; <?=$val['name_product'];?> </strong></td>
                        <td width="209"><?=$val['total_payout']?></td>
                        <?php $discount = $discount + $val['percentage_product']; ?>
                    </tr>
                    <?php endif?>
                    <?php endforeach;?>
                    </tbody>
                </table>
                <br><br><br><br>
                <table with="400" align="left">
                    <tbody><tr>
                        <td><h1 style="font-size:40px;">Tiene una semana antes de inicio de clase para editar la clase despues de este tiempo se bloquea el acceso a editar la clase</h1></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function closePrint () {
        document.body.removeChild(this.__container__);
    }

    function setPrint () {
        this.contentWindow.__container__ = this;
        this.contentWindow.onbeforeunload = closePrint;
        this.contentWindow.onafterprint = closePrint;
        this.contentWindow.focus(); // Required for IE
        this.contentWindow.print();
    }

    function printPage (sURL) {
        var oHiddFrame = document.createElement("iframe");
        oHiddFrame.onload = setPrint;
        oHiddFrame.style.visibility = "hidden";
        oHiddFrame.style.position = "fixed";
        oHiddFrame.style.right = "0";
        oHiddFrame.style.bottom = "0";
        oHiddFrame.src = sURL;
        document.body.appendChild(oHiddFrame);
    }
    printPage("<?=$url?>");
</script>
<?php endif;?>