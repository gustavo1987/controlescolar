<ul class="breadcrumb">
    <li><a href="/dashboard">Inicio</a></li>
    <li><a href="#">Apartado</a></li>
</ul>
<!-- PAGE TITLE -->
<div class="page-title">
    <div class="col-sm-12 text-left">
        <h2>Estado de <strong>Apartados de Productos</strong></h2>
    </div>
</div>
<?php $url = $this->url->get('dashboard/sales/print/' . $data->getSalid()) ?>
<div class="page-content-wrap" id="sales">
    <div class="row">
        <div class="col-md-8">
            <div class="panel panel-success">
                <!--div class="panel-heading">
                    <div class="col-sm-3"><h3 class="panel-title">Estatus de :</h3></div>
                   <div class="col-sm-2"><span class="fa fa-cube fa-2x text-info"></span>&nbsp;Ventas</div>
                    <div class="col-sm-2"><span class="fa fa-cube fa-2x text-warning"></span>&nbsp;Consumo</div>
                    <div class="col-sm-2"><span class="fa fa-cube fa-2x text-danger"></span>&nbsp;Cancelados</div>
                </div-->
                <span class="hidden" id="uid" data-value="<?= $data->getUid() ?>"></span>
                <span class="hidden" id="salid" data-value="<?= $data->getSalid() ?>"></span>
                <span class="hidden" id="clid" data-value="<?= $data->getClid() ?>"></span>
                <span class="hidden" id="email" data-value="<?= $data->getEmail() ?>"></span>

                <div class="panel-body">

                    <!-- INVOICE -->
                    <div class="invoice">
                        <div class="">
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th class="success">Producto</th>
                                    <th class="success text-center">Precio</th>
                                    <th class="success text-center">Cantidad</th>
                                    <th class="success text-center">descuento</th>
                                    <th class="success text-center">Total</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                    $calculate = 0;
                                    foreach ($list as $val): $subsalid = $val['sub_salid'] ?>
                                        <tr data-type="product_sale" data-value="<?= $subsalid; ?>" class="<?= $subsalid; ?> <?= $val['status'] == 'CANCELADO' ? 'danger' : 'info' ?>">
                                            <td class="description">
                                                <strong class="name_service"><?= $val['name_brand'] ?></strong>
                                                <p class="name_sub_service"><?= $val['name_product'] ?></p>
                                                <strong class="color-success"><?=$val['discount']>0?$val['discount']."% de descuento":""?></strong>
                                            </td>
                                            <td class="text-center price_service" data-value="<?= $val->sale_price ?>">$<?= $val['sale_price'] . ".00" ?></td>
                                            <td class="text-center count_subtotal">
                                                <input readonly type="number" min="1" max="100" value="<?= $val['quantity_services']  ?>" class="form-control comission" size="color:#000!important;">
                                            </td>
                                            <?php $subtotal = $val['quantity_services'] * $val['sale_price'] ?>
                                            <td class="text-center subtotal_service hide" data-value="<?= $subtotal ?>">
                                                <p>$<?= $subtotal ?>.00</p>
                                            </td>
                                            <?php $discount = $val->percentage ?>
                                            <td class="text-center discount_service">
                                                <?php if ($val['percentage_product']): ?>
                                                    <strong>-$<?= $val['percentage_product'] ?></strong>
                                                <?php endif; ?>
                                            </td>
                                            <?php
                                                    $total = number_format($val['total_payout'], "2", ".", '');

                                                    $calculate = $calculate + $total;
                                            ?>
                                            <td class="text-center total_service" data-discount="<?= $discount ?>" data-value="<?= $total ?>">$<?= $total ?></td>
                                        </tr>
                                <?php endforeach ?>
                                </tbody>
                            </table>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                            </div>
                            <div class="col-md-6">
                                <h4>Monto de apartado</h4>
                                 <?php
                                    $total = $calculate;
                                    $iva =  round(($total/1.16)*.16,2);
                                    $subtotal = round($total - $iva,2);
                                 ?>
                                <table class="table table-striped">
                                    <tbody>
                                    <tr>
                                        <td width="200"><strong>Subtotal:</strong></td>
                                        <td class="text-right" id="subtotal">
                                            $<?= number_format($subtotal, "2", ".", "") ?></td>
                                    </tr>
                                    <tr id="iva" data-value="16"><td><strong>IVA 16%:</strong></td><td class="text-right iva_general">$<?=number_format($iva, "2", ".", "") ?></td>
                                    <tr id="received"><td><strong>Abonos:</strong></td><td class="text-right iva_general">$<?=number_format($data->getReceived(), "2", ".", "") ?></td>
                                    </tr>
                                    <!--tr>
                                        <td><strong>Discount (2%):</strong></td><td class="text-right">$50.66</td>
                                    </tr-->
                                    <tr class="total">
                                        <?php $pay = number_format($total-$data->getReceived(), "2", ".", "") ?>
                                        <td>Total a pagar:</td>
                                        <td class="text-right" id="amount_to_pay" data-value="<?= $pay ?>">
                                            $<?= $pay ?></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <?php if($data->getStatus()=="PULLEDAPART"):?>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="pull-right push-down-20">
                                        <button class="btn btn-warning abonarPulledApart" ><span class="fa fa-calendar-check-o"></span> Abonar
                                        </button>
                                        <!--button disabled class="btn btn-success disabled" id="checkout"><span class="fa fa-credit-card"></span> Realizar pago</button-->
                                    </div>
                                </div>
                            </div>
                       <?php endif;?>
                    </div>
                    <!-- END INVOICE -->

                </div>
            </div>

            <!-- process pulled apart -->
            <div class="message-box message-box-info animated fadeIn" id="message-box-info">
                <div class="mb-container">
                    <div class="mb-middle">
                        <div class="mb-title"> Procesando el abono del apartado &nbsp; <i class="fa fa-circle-o-notch fa-spin style-fa"></i></div>
                        <div class="mb-content">
                            <p>Estamos realizando el abono del apartado, espere un momento por favor.</p>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end process pulled apart -->

        </div>
        <div class="col-md-4">
            <div class="panel panel-success">
                <div class="panel-body">
                    <h3>
                        Realizar:
                    </h3>
                    <?php if($data->getStatus()=="PULLEDAPART"):?>
                        <?php $url = $this->url->get("dashboard/sales/print/{$data->getSalid()}")?>
                        <button class="btn btn-warning abonarPulledApart"><span class="fa fa-calendar-check-o"></span> Abonar
                        </button>
                        <a onclick="printPage('<?=$url?>')" class="btn btn-success"><span class="fa fa-print"></span> Imprimir
                        </a>
                    <?php endif;?>
                </div>
            </div>


            <div class="panel panel-success">
                <div class="panel-body">
                    <h3>
                        Abonos Realizados:
                    </h3>
                    <div class="invoice">
                        <div class="">
                            <table class="table table-bordered" id="tableSales">
                                <thead>
                                <tr>
                                    <th class="success">Abono</th>
                                    <th class="success text-center">Fecha</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                foreach ($credit as $val): ?>
                                    <tr data-type="product_sale" data-value="" class="">
                                        <td class="description">
                                            <strong class="name_service">$<?= $val->getCredit(); ?> </strong>
                                        </td>
                                        <td class="text-center price_service" data-value=""><?= $val->getDateCreation();?></td>
                                    </tr>
                                <?php endforeach ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--MODALS Apartado -->
<div class="modal  bs-example-modal-lg" id="modalAbonarPulledApart" tabindex="-1" role="dialog" aria-labelledby="defModalHead"
     aria-hidden="true" style="display: none;" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span
                        class="sr-only">Cerrar</span></button>
                <h4 class="modal-title">Abonar al apartado</h4>
            </div>
            <div class="modal-body" id="modal_body">
                <form action="#" method="post" id="pulled_apart_abono_form">
                    <div class="row">
                        <div class="col-sm-offset-2 col-sm-4"><h3>Total deuda</h3></div>
                        <div class="col-sm-offset-2 col-sm-4"><h3><span id="debt_apart" data-pay="0">$0</span>&nbsp;<span id="discount_debt_apart" data-discount=""></span></h3></div>
                    </div>
                    <div class="row">
                        <br>

                        <div class="col-sm-offset-2 col-sm-4"><h3>Efectivo Recibido($)</h3></div>
                        <div class="col-sm-offset-2 col-sm-4"><input step="any" type="number" min="0" value="" id="pay_received_apart"
                                                                     name="pay_received_apart" placeholder="$"
                                                                     class="form-control"></div>
                    </div>
                    <div class="row">
                        <br>

                        <div class="col-sm-offset-2 col-sm-4"><h3>Restante a pagar($)</h3></div>
                        <div class="col-sm-offset-2 col-sm-4"><h3 class="btn btn-info btn-lg" id="vuelto_cliente_apart_abono">
                                $0</h3></div>
                    </div>
                    <div class="row">
                        <br>

                        <div class="col-sm-offset-6 col-sm-6 text-right">
                            <button type="submit" class="btn btn-success btn-md disabled" id="buttonPay_apart_abono" disabled>
                                <span class="fa fa-dollar"></span>&nbsp;Apartar
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!--END MODALS-->
<script type="text/javascript">
    function closePrint () {
        document.body.removeChild(this.__container__);
    }

    function setPrint () {
        this.contentWindow.__container__ = this;
        this.contentWindow.onbeforeunload = closePrint;
        this.contentWindow.onafterprint = closePrint;
        this.contentWindow.focus(); // Required for IE
        this.contentWindow.print();
    }

    function printPage (sURL) {
        var oHiddFrame = document.createElement("iframe");
        oHiddFrame.onload = setPrint;
        oHiddFrame.style.visibility = "hidden";
        oHiddFrame.style.position = "fixed";
        oHiddFrame.style.right = "0";
        oHiddFrame.style.bottom = "0";
        oHiddFrame.src = sURL;
        document.body.appendChild(oHiddFrame);
    }
</script>