<ul class="breadcrumb">
    <li><a href="/dashboard">Inicio</a></li>
    <li><a href="#">Clases</a></li>
</ul>
<div class="page-title">
    <div class="col-sm-12 text-left">
        <h2>Clases</h2>
    </div>
</div>
<div class="page-content-wrap" id="sales">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-success">
                <div class="panel-heading">
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                       <form action="#" method="post" id="addStudent" name="addStudent" role="form">
                            <input class="hidden" value="<?=$clase?>" name="salid">
                            <table class="table table-bordered table-hover table-actions generalDT" data-order="5" data-filter="desc">
                                <thead>
                                <tr>
                                    <th>Número&nbsp;Cliente</th>
                                    <th>Nombre</th>
                                    <th>Apellido Paterno</th>
                                    <th>Apellido Materno</th>
                                    <th>Factura</th>
                                    <th>Fecha registro</th>
                                    <th>Opciones</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($clients as $values) { ?>
                                <?php $dateC= $values->getDateCreation(); $newDate = date("d-m-Y", strtotime($dateC));?>
                                <tr id="<?= $values->getClid() ?>">
                                    <td><?= $values->getIdentifier() ?></td>
                                    <td><?= $values->getName() ?></td>
                                    <td><?= $values->getLastName() ?></td>
                                    <td><?= $values->getSecondName() ?></td>
                                    <td>No</td>
                                    <!--td><= $values->getJurisdiction()=="NATIONAL"?"Si":"No";?></td-->
                                    <td><?= $newDate ?></td>
                                    <td>
                                        <input class="add-student" data-clid="<?= $values->getClid();?>" data-salid="<?=$clase;?>" type="radio" name="add-student<?= $values->getClid();?>" value="1"> Agregar<br>
                                        <input class="add-student" data-clid="<?= $values->getClid();?>" data-salid="<?=$clase;?>" type="radio" name="add-student<?= $values->getClid();?>" value="0"> Quitar<br>
                                    </td>
                                </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                            <div class="form-group">
                                <div class="col-md-offset-10 col-md-2">
                                    <input type="submit" class="btn btn-success" value="Guardar"/>
                                </div>
                            </div>
                       </form>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="message-box message-box-info animated fadeIn" id="message-box-info">
    <div class="mb-container">
        <div class="mb-middle">
            <div class="mb-title"> Actualizando &nbsp; <i class="fa fa-circle-o-notch fa-spin style-fa"></i></div>
            <div class="mb-content">
                <p>Guardando y Actualizando su clase, espere un momento por favor.</p>
            </div>
        </div>
    </div>
</div>
<div class="message-box message-box-success animated fadeIn" id="message-box-success">
    <div class="mb-container">
        <div class="mb-middle">
            <div class="mb-title"><span class="fa fa-check"></span> Clase actualizada</div>
            <div class="mb-content">
                <p>Sus cambios han sido guardados correctamente, actualizaremos el sitio para reflejar los cambios.</p>
            </div>
        </div>
    </div>
</div>
<div class="message-box message-box-warning animated fadeIn" id="message-box-warning">
    <div class="mb-container">
        <div class="mb-middle">
            <div class="mb-title"><span class="fa fa-warning"></span> Error</div>
            <div class="mb-content">
                <p>Ha ocurrido un error al guardar su información, inténtelo nuevamente o regrese mas tarde.</p>
            </div>
            <div class="mb-footer">
                <button class="btn btn-default btn-lg pull-right mb-control-close">Cerrar</button>
            </div>
        </div>
    </div>
</div>
<div class="message-box message-box-danger animated fadeIn" id="message-box-danger">
    <div class="mb-container">
        <div class="mb-middle">
            <div class="mb-title"><span class="fa fa-times"></span> Cuidado</div>
            <div class="mb-content">
                <p>Usted no puede actualizar su imagen.</p>
            </div>
            <div class="mb-footer">
                <button class="btn btn-default btn-lg pull-right mb-control-close">Cerrar</button>
            </div>
        </div>
    </div>
</div>
<div class="message-box animated fadeIn" data-sound="alert" id="other_products">
    <div class="mb-container">
        <div class="mb-middle">
            <div class="mb-title"><span class="fa fa-sign-out"></span> Alta <strong>Cliente</strong></div>
            <div class="mb-content">
                <p>¿Le gustaría dar de alta otro cliente?</p>
                <p>Pulse Sí para dar de alta un nuevo cliente. Pulse No para ir a la lista de clientes.</p>
            </div>
            <div class="mb-footer">
                <div class="pull-right">
                    <button class="btn btn-success btn-lg mb-control-close">Si</button>
                    <a href="<?= $this->url->get('dashboard/clients') ?>" class="btn btn-default btn-lg">No</a>
                </div>
            </div>
        </div>
    </div>
</div>