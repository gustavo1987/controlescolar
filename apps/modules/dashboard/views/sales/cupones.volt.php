<ul class="breadcrumb">
    <li><a href="/dashboard">Inicio</a></li>
    <li><a href="#">Ventas</a></li>
</ul>
<div class="page-title">
    <div class="col-sm-12 text-left">
        <h2>Apartados del día.</h2>
    </div>
</div>
<div class="page-content-wrap" id="sales">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-success">
                <div class="panel-body">
                    <div class="table-responsive">
                        <table id="tableCupon" class="table table-bordered table-hover table-actions dataTable generalDT" data-order="0" data-filter="desc">
                            <thead>
                            <tr>
                                <th>#Cupon</th>
                                <th>Cliente</th>
                                <th>Total</th>
                                <th>Estatus</th>
                                <th>Fecha</th>
                                <th>Imprimir</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach($result as $values):?>
                            <tr class="<?=$values->getCupid();?>" id="<?=$values->getCupid();?>">
                                <td><?=$values->getCupid();?></td>
                                <td><?=$values->getName();?></td>
                                <td class='text-right'>$<?=$values->getTotal();?></td>
                                <td class='text-right'><?=$values->getStatus();?></td>
                                <td><?=date('d-m-Y', strtotime(str_replace('-','/', $values->getDateCreation())));?></td>
                                <?php $url = $this->url->get("dashboard/sales/printcupon/".$values->getCupid())?>
                                <td>
                                    <a onclick="printPage('<?=$url?>')" class="btn btn-success"><span class="fa fa-print"></span> Imprimir</a>
                                    <span class="deleteElementcupon cursor_pointer" data-toggle="tooltip" data-id="<?=$values->getCupid();?>" data-placement="top" title="" data-original-title="Eliminar Cupón">
                                        <i class="fa fa-remove fa-2x"></i>
                                    </span>
                                </td>
                            </tr>
                            <?php endforeach;?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="message-box animated fadeIn" data-sound="alert" id="mb-remove-row">
    <div class="mb-container">
        <div class="mb-middle">
            <div class="mb-title"><span class="fa fa-times"></span> Eliminar <strong>Datos</strong> ?</div>
            <div class="mb-content">
                <p>¿Estas seguro de eliminar esta fila?</p>
                <p>Presione "Si" si esta seguro.</p>
            </div>
            <div class="mb-footer">
                <div class="pull-right">
                    <button class="btn btn-success btn-lg mb-control-yes">Si</button>
                    <button class="btn btn-default btn-lg mb-control-close">No</button>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function closePrint () {
        document.body.removeChild(this.__container__);
    }
    function setPrint () {
        this.contentWindow.__container__ = this;
        this.contentWindow.onbeforeunload = closePrint;
        this.contentWindow.onafterprint = closePrint;
        this.contentWindow.focus(); // Required for IE
        this.contentWindow.print();
    }
    function printPage (sURL) {
        var oHiddFrame = document.createElement("iframe");
        oHiddFrame.onload = setPrint;
        oHiddFrame.style.visibility = "hidden";
        oHiddFrame.style.position = "fixed";
        oHiddFrame.style.right = "0";
        oHiddFrame.style.bottom = "0";
        oHiddFrame.src = sURL;
        document.body.appendChild(oHiddFrame);
    }
</script>