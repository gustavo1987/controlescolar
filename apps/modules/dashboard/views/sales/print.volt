<!DOCTYPE html>
<html>
<head>
    <title></title>
</head>
<body style="width: 300px; font-size: 13px; font-family: arial;">
<div class="page-content-wrap">
    <div class="content" id="content">
        <div>
            <div style="width: 330px;">
                <div>
                    <center><div style="text-align: center;font: bold 95px 'Arial';">PONS</div><br>
                        <div style="font:bold 20px 'Arial';">COLLECTION <br> BOUTIQUE</div>
                        <!--div style="font:bold 15px ';Arial';"></div><br><br--> <br><br>
                        <div style="text-align:left; font:bold 15px 'Arial';">Plaza las Américas Av. Ramón Mendoza 102 Int D6 Col. José María Pino Suárez, Villahermosa Tabasco CP 86029 <br>RFC MAPJ850930CU5  <?php if($status==1):?><br><br><?php endif;?>
                        </div>
                    </center>
                </div>
                <div style="width: 330px;">
                    <table cellpadding="3" cellspacing="0" style="font:bold 15px 'Arial';">
                        <tbody>
                        <tr>
                            <td>Ticket:</td>
                            <td><?=$data->getSalid()?></td>
                        </tr>
                        <?php if($status==1):?>
                            <tr>
                                <td>Fecha :</td>
                                <td><?=date("d-m-Y h:m",strtotime($data->getDateCreationSale()))?></td>
                            </tr>
                        <?php elseif($status==2):?>
                            <tr>
                                <td>Fecha :</td>
                                <td><?=date("d-m-Y h:m",strtotime($credit[0]["date_creation"]))?></td>
                            </tr>
                        <?php endif;?>

                        <tr>
                            <td>Asesor:</td>
                            <td><?=$data->getNameAttend()." ".$data->getLastNameAttend()?></td>
                        </tr>
                        <?php if($status==2):?>
                            <tr>
                                <td>Nombre:</td>
                                <td><?=$data->getName()?></td>
                            </tr>
                            <tr>
                                <td>Teléfono:</td>
                                <td><?=$data->getPhone()?></td>
                            </tr>
                            <?php $fecha = $data->getDateCreationSale();
                                    $vencimiento = strtotime('+1 month', strtotime($fecha));
                                    $vencimiento = date('d-m-Y h:m', $vencimiento);
                            ?>
                            <tr>
                                <td>Vence:</td>
                                <td><?=$vencimiento?></td>
                            </tr>
                            <br><br>
                        <?php endif;?>
                        </tbody>
                    </table>
                </div>
                <div class="clearfix"></div>
            </div>
            <br><br>
            <div style="width: 330px;">
                <table border="0" cellpadding="4" cellspacing="0" style="width: 330px;font:bold 15px 'Arial';   text-align:left;">
                    <thead style="text-align:center;">
                    <tr>
                        <th width="100" colspan="2"> Articulo </th>
                        <th width="30"> Cant. </th>
                        <th width="50"> Subt. </th>
                        <th width="50"> Desc. </th>
                        <th width="90"> Total </th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $total = (int) 0; $discount = (int) 0;?>
                    <?php foreach($list as $key => $val):?>
                        <?php $total = $total + $val['total_payout'];?>
                        <?php if($val['type']=="VENTAS"):?>
                        <tr>
                            <?php $discount = $val['percentage_product'];?>
                            <td width="100" colspan="2"><strong><?=$val['name_brand'];?> &nbsp; <?=$val['name_product'];?> </strong></td>
                            <td width="90"><?=$val['quantity_services']?></td>
                            <td width="100">$<?=number_format($val['price_general'],"2",".",",")?></td>
                            <td width="100">$<?=number_format($discount,"2",".",",")?></td>
                            <td width="100">$<?=number_format($val['total_payout'],"2",".",",")?></td>

                        </tr>
                        <?php endif?>
                    <?php endforeach;?>
                    </tbody>
                </table>
                <br><br>
                <table style="margin-left:190px;">
                    <?php
                        $total = $total;
                        $iva =  round(($total/1.16)*.16,2);
                        $subtotal = round($total - $iva,2);
                    ?>
                    <tbody>
                    <tr>
                        <td colspan="5" style=" text-align:right;"><strong style="font:bold 15px 'Arial';">Subt.: &nbsp;</strong></td>
                        <td style="display: block;"> <strong style="font:bold 15px 'Arial';">$<?=number_format($subtotal,"2",".",",")?></strong></td>
                    </tr>
                    <tr>
                        <td colspan="5" style=" text-align:right;"><strong style="font:bold 15px 'Arial';">IVA: &nbsp;</strong></td>
                        <td><strong style="font-size: 15px;">$<?=number_format($iva,"2",".",",")?></strong></td>
                    </tr>
                    <tr>
                        <td colspan="5" style=" text-align:right;"><strong style="font:bold 15px 'Arial';">Total:&nbsp;</strong></td>
                        <td colspan="2"><strong style="font:bold 15px 'Arial';">$<?=number_format($total,"2",".",",")?></strong></td>
                    </tr>
                    <?php $nDiscount =0; if($data->getDiscount()>0):?>
                        <tr>
                            <td colspan="5" style=" text-align:right;"><strong style="font:bold 15px 'Arial';">Tarjeta R:&nbsp;</strong></td>
                            <td colspan="2"><strong style="font:bold 15px 'Arial';">$<?=number_format($data->getDiscount(),"2",".",",")?></strong></td>
                        </tr>
                        <?php $nDiscount = $data->getDiscount();?>
                    <?php endif;?>
                    <?php if($status==2):?>
                        <tr>
                            <td colspan="5" style=" text-align:right;"><strong style="font:bold 15px 'Arial'; color: #222222;">
                                    <font style="font:bold 15px 'Arial';">
                                        Abonó:&nbsp;
                                    </font></strong></td>
                            <td colspan="2"><strong style="font:bold 15px 'Arial'; color: #222222;">
                                    $<?=number_format($credit[0]["credit"],"2",".",",")?>                    </strong></td>
                        </tr>
                        <tr>
                            <td colspan="5" style=" text-align:right;"><strong style="font:bold 15px 'Arial';; color: #222222;">Total&nbsp;Pagado:&nbsp;</strong></td>
                            <td colspan="2"><strong style="font:bold 15px 'Arial'; color: #222222;">
                                    $<?=number_format($data->getReceived(),"2",".",",")?>                   </strong></td>
                        </tr>
                    <?php endif;?>
                    <?php if($status==1):?>
                        <tr>
                            <td colspan="5" style=" text-align:right;"><strong style="font:bold 15px 'Arial';; color: #222222;">Efectivo:&nbsp;</strong></td>
                            <td colspan="2"><strong style="font:bold 15px 'Arial'; color: #222222;">
                                    $<?=number_format($data->getReceived(),"2",".",",")?>                   </strong></td>
                        </tr>
                        <tr>
                            <td colspan="5" style=" text-align:right;"><strong style="font:bold 15px 'Arial'; color: #222222;">
                                    <font style="font:bold 15px 'Arial';">
                                        Cambio:&nbsp;
                                    </font></strong></td>
                            <td colspan="2"><strong style="font:bold 15px 'Arial'; color: #222222;">
                                    $<?=number_format(($data->getReceived()+$nDiscount-$total)<0?0:($data->getReceived()+$nDiscount-$total),"2",".",",")?>                    </strong></td>
                        </tr>
                    <?php elseif($status==2):?>
                        <tr>
                            <td colspan="5" style=" text-align:right;"><strong style="font:bold 15px 'Arial'; color: #222222;">
                                    <font style="font:bold 15px 'Arial';">
                                        Deuda:&nbsp;
                                    </font></strong></td>
                            <td colspan="2"><strong style="font:bold 15px 'Arial'; color: #222222;">
                                    $<?=number_format($total-$data->getReceived(),"2",".",",")?>                    </strong></td>
                        </tr>
                    <?php endif;?>
                    </tbody>
                </table>
                <br><br>
                <table with="400" align="left">

                    <tbody><tr>
                        <?php if($status==1):?>
                        <td><h1 style="font-size:15px;">Cambio de prenda únicamente por defecto de fábrica, con un plazo de 8 días presentando el ticket de compra y la prenda con etiqueta, se aplican restricciones.</h1></td>
                        <?php elseif($status==2):?>
                            <td><h1 style="font-size:15px;">Vigencia de apartado 30 días, En apartado no aplica cambio de modelo</h1></td>
                        <?php endif;?>
                    </tr>
                    </tbody></table>
            </div>
            <br><br><br>
            <div style="width: 330px; height: 300px;">
                <div>
                    <center>
                        <div style="text-align:left; font:bold 15px 'Arial';">Siguenos en nuestras redes sociales:
                            <br><br> Facebook e Instagram como:<br>
                            <span style="text-decoration: underline;">PonsCollection</span>
                            <br> WhatsApp : 9934321895
                        </div>
                        <br><br><br>
                        <div style="text-align:center; font:bold 15px 'Arial';text-transform: uppercase;">
                            Gracias por hacer tu compra en <span style="text-align: center;font: bold 20px 'Arial';">PONS COLLECTION</span>
                            ¡Tu aliado para deslumbrar!
                        </div>
                    </center>
                </div>
            </div>
        </div>
    </div>
</div>  
</body>
</html>

