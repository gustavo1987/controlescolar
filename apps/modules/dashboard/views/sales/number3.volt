<ul class="breadcrumb">
    <li><a href="/dashboard">Inicio</a></li>

    <li><a href="#">Venta</a></li>
</ul>
<?php $url = $this->url->get('dashboard/sales/print/'.$data->getSalid())?>
<?php if($status==0):?>
<div class="page-content-wrap" id="sales">
    <div class="row">
        <div class="col-md-12">

            <div class="panel panel-default">
                <span class="hidden" id="uid" data-value="{{data.getUid()}}"></span>
                <span class="hidden" id="salid" data-value="{{data.getSalid()}}"></span>
                <span class="hidden" id="clid" data-value="{{data.getClid()}}"></span>
                <span class="hidden" id="email" data-value="{{data.getEmail()}}"></span>
                <div class="panel-body">
                    <h2>Estatus  <strong>Venta de Productos</strong></h2>
                    <!-- INVOICE -->
                    <div class="invoice">
                        <div class="row">
                            <!--div class="col-md-3">
                                <label for="">Servicios</label>
                                <select class="form-control select_service" data-placeholder="Buscar servicios">
                                </select>
                            </div-->
                            <!--div class="col-md-3">
                                <label for="">Producto de uso</label>
                                <select class="form-control select_product" data-placeholder="Buscar producto">
                                </select>
                            </div-->
                            <div class="col-md-6">
                                <label for="">Producto de venta</label>
                                <select class="form-control select_product_sale" data-placeholder="Buscar producto">
                                </select>
                            </div>
                            <div class="col-md-6">
                                <label for="">Cliente</label>
                                <select name="user" id="user"  class="form-control select_client" data-placeholder="Buscar cliente">
                                </select>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <hr>
                        <?php if($data->getEmailClient()=="ventas@beautyroom.com"):?>
                            <h4 class="type_sales" style="font-weight: lighter"><strong class="name_user_active">Venta General</strong></h4>
                        <?php else:?>
                            <h4 class="type_sales" style="font-weight: lighter">Bienvenido nuevamente: <strong class="name_user_active"><?=$data->getNameClient()." ".$data->getLastNameClient()." ".$data->getSecondNameClient()?></strong></h4>
                        <?php endif;?>
                        <div class="">
                            <table class="table table-bordered" id="tableSales">
                                <thead>
                                    <tr>
                                        <th class="success">&nbsp;</th>
                                        <th class="success">Descripción</th>
                                        <th class="success">Usuario</th>
                                        <th class="success text-center">Precio</th>
                                        <th class="success text-center">Cantidad</th>
                                        <th class="success text-center">Subtotal</th>
                                        <th class="success text-center">Descuento</th>
                                        <th class="success text-center">Total</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php if(count($list)>=1):?>
                                    <?php $calculate = 0; foreach($list as $val): $subsalid = $val->getSubsalid()?>
                                        <?php if($val->getType()=="SERVICIO"):?>
                                            <tr data-type="service" data-value="<?=$subsalid;?>" class="<?=$subsalid;?>">
                                                <td class="text-center">
                                                    <span class="deleteElement cursor_pointer" data-toggle="tooltip" data-placement="top" title="" data-original-title="Eliminar servicio"><i class="fa fa-remove fa-2x"></i></span>
                                                </td>
                                                <td class="description">
                                                    <strong class="name_service"><?=$val->getNameService()?></strong>
                                                    <p class="name_sub_service"><?=$val->getNameSubservice()?>
                                                        <strong class="color-success"><?=$val->getDiscount()?$val->getDiscount()."% de descuento":""?></strong>
                                                    </p>
                                                </td>
                                                <td class="add_stylist">
                                                    <button class="btn btn-success"><?=$val->getUidAttend()?$val->getNameAttend()." ".$val->getLastNameAttend():"Asignar Vendedora" ?></button>
                                                </td>
                                                <td class="text-center price_service" data-value="<?=$val->getPrice()?>">$<?=$val->getPrice().".00"?></td>
                                                <td class="text-center count_subtotal">
                                                    <input type="number" min="1" max="100" value="<?=$val->getQuantitySs()?>" class="form-control comission">
                                                </td>
                                                <?php $subtotal = $val->getQuantitySs()*$val->getPrice()?>
                                                <td class="text-center subtotal_service" data-value="<?=$subtotal?>">
                                                    <p>$<?=$subtotal?>.00</p>
                                                </td>
                                                <?php $discount = 0?>
                                                <td class="text-center discount_service">
                                                    <?php if($val->getDiscount()):?>
                                                        <?php $discount = number_format(($subtotal*$val->getDiscount())/100,2,".","")?>
                                                        <strong>-$<?=$discount?></strong>
                                                    <?php endif;?>
                                                </td>
                                                <?php
                                                    $total = number_format($subtotal-$discount,"2",".",'');
                                                    $calculate = $calculate+$total;
                                                ?>
                                                <td class="text-center total_service" data-discount="<?= number_format(($val->getPrice()*$val->getDiscount())/100,2,".","")?>" data-value="<?=$total?>">$<?=$total?></td>
                                            </tr>
                                        <?php elseif($val->getType()=="VENTAS"):?>
                                            <tr data-type="product_sale" data-value="<?=$subsalid;?>" class="<?=$subsalid;?> info">
                                                <td class="text-center">
                                                    <span class="deleteElement cursor_pointer" data-toggle="tooltip" data-placement="top" title="" data-original-title="Eliminar venta de producto"><i class="fa fa-remove fa-2x"></i></span>
                                                </td>
                                                <td class="description">
                                                    <strong class="name_service"><?=$val->getNameBrand()?> <span class="fa fa-info consumer-product" data-toggle="tooltip" data-placement="top" data-original-title="Venta de producto."></span></strong>
                                                    <p class="name_sub_service"><?=$val->getNameProduct()?>
                                                        <strong class="color-success"><?=$val->getPercentage()?$val->getPercentage()."% de descuento":""?></strong>
                                                    </p>
                                                </td>
                                                <td class="add_stylist">
                                                    <button class="btn btn-success"><?=$val->getUidAttend()?$val->getNameAttend()." ".$val->getLastNameAttend():"Asignar Vendedora" ?></button>
                                                </td>
                                                <td class="text-center price_service" data-value="<?=$val->getSalePrice()?>">$<?=$val->getSalePrice().".00"?></td>
                                                <td class="text-center count_subtotal">
                                                    <input type="number" min="1" max="100" value="<?=$val->getQuantitySs()?>" class="form-control comission" readonly>
                                                </td>
                                                <?php $subtotal = $val->getQuantitySs()*$val->getSalePrice()?>
                                                <td class="text-center subtotal_service" data-value="<?=$subtotal?>">
                                                    <p>$<?=$subtotal?>.00</p>
                                                </td>
                                                <?php $discount = 0?>
                                                <td class="text-center discount_service">
                                                    <?php if($val->getPercentage()):?>
                                                        <?php $discount = number_format(($subtotal*$val->getPercentage())/100,2,".","")?>
                                                        <strong>-$<?=$discount?></strong>
                                                    <?php endif;?>
                                                </td>
                                                <?php
                                                $total = number_format($subtotal-$discount,"2",".",'');
                                                $calculate = $calculate+$total;
                                                ?>
                                                <td class="text-center total_service" data-discount="<?=$discount?>" data-value="<?=$total?>">$<?=$total?></td>
                                            </tr>
                                        <?php elseif($val->getType()=="CONSUMO"):?>
                                            <tr data-type="product" data-value="<?=$subsalid;?>" class="<?=$subsalid;?> warning">
                                                <td class="text-center">
                                                    <span class="deleteElement cursor_pointer" data-toggle="tooltip" data-placement="top" title="" data-original-title="Eliminar venta de producto"><i class="fa fa-remove fa-2x"></i></span>
                                                </td>
                                                <td class="description">
                                                    <strong class="name_service"><?=$val->getNameBrand()?> <span class="fa fa-info consumer-product" data-toggle="tooltip" data-placement="top" data-original-title="Venta de producto."></span></strong>
                                                    <p class="name_sub_service"><?=$val->getNameProduct()?></p>
                                                </td>
                                                <td class="add_stylist">
                                                    <button class="btn btn-success"><?=$val->getUidAttend()?$val->getNameAttend()." ".$val->getLastNameAttend():"Asignar Vendedora" ?></button>
                                                </td>
                                                <td class="text-center price_service" data-value="0">$0</td>
                                                <td class="text-center count_subtotal">
                                                    <input type="number" min="1" max="10000" value="<?=$val->getQuantitySs()?>" class="form-control comission" readonly>
                                                </td>
                                                <td class="text-center subtotal_service" data-value="0">
                                                    <p>$0</p>
                                                </td>
                                                <td class="text-center discount_service"><strong>$0</strong></td>
                                                <td class="text-center total_service" data-discount="0" data-value="0">$0</td>
                                            </tr>
                                        <?php endif;?>
                                    <?php endforeach?>
                                <?php endif;?>
                                </tbody>
                            </table>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                            </div>
                            <div class="col-md-6">
                                <h4>Monto a pagar</h4>

                                <table class="table table-striped">
                                    <tbody><tr>
                                        <td width="200"><strong>Subtotal:</strong></td><td class="text-right" id="subtotal">$<?=number_format($calculate,"2",".","")?></td>
                                    </tr>
                                    <tr id="iva" data-value="16">
                                        <?php $iva = number_format(($calculate*16)/100,"2",".","")?>
                                        <td><strong>IVA 16%:</strong></td><td class="text-right iva_general">$<?=$iva?></td>
                                    </tr>
                                    <!--tr>
                                        <td><strong>Discount (2%):</strong></td><td class="text-right">$50.66</td>
                                    </tr-->
                                    <tr class="total">
                                        <?php $pay = number_format($calculate+$iva,"2",".","")?>
                                        <td>Total a pagar:</td><td class="text-right" id="amount_to_pay" data-value="<?=$pay?>">$<?=$pay?></td>
                                    </tr>
                                    </tbody></table>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="pull-right push-down-20">
                                    <button class="btn btn-danger" id="cancel_checkout"><span class="fa fa-remove"></span> Cancelar venta</button>
                                    <button class="btn btn-success" id="checkout"><span class="fa fa-credit-card"></span> Realizar pago</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END INVOICE -->

                </div>
            </div>

        </div>
    </div>
</div>
<!-- MESSAGE BOX-->
<div class="message-box animated fadeIn" data-sound="alert" id="mb-remove-row">
    <div class="mb-container">
        <div class="mb-middle">
            <div class="mb-title"><span class="fa fa-times"></span> Eliminar <strong>Datos</strong> ?</div>
            <div class="mb-content">
                <p>¿Estas seguro de eliminar esta fila?</p>
                <p>Presione "Si" si esta seguro.</p>
            </div>
            <div class="mb-footer">
                <div class="pull-left fade in" id="status_delete">
                    <button class="btn btn-success btn-lg mb-control-yes">Si</button>
                    <button class="btn btn-default btn-lg mb-control-close">No</button>
                </div>
                <div class="pull-right fade hidden" id="message_delete">
                    <div class="mb-title"><span class="fa fa-spinner fa-pulse fa-3x fa-fwe"></span> Eliminando</div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END MESSAGE BOX-->

<!-- Pay -->
<!--MODALS SUB-SERVICES -->
<div class="modal  bs-example-modal-lg" id="modalStylist" tabindex="-1" role="dialog" aria-labelledby="defModalHead" aria-hidden="true" style="display: none;" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Cerrar</span></button>
                <h4 class="modal-title" id="defModalHead">Agregar Vendedora</h4>

                <form id="formServices" action="#" method="post" class="form-horizontal">
                    <div class="modal-body">
                        <div class="form-group">
                            <div class="col-md-12">
                                <input type="hidden" name="type_id" id="type_id" value="">
                                <input type="hidden" name="type_status" id="type_status" value="">
                                <select name="user_services" id="user_services" class="form-control" required multiple data-placeholder="Seleccionar el Vendedora">
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
                        <input type="submit" class="btn btn-info btn-submit-sub-services" value="Guardar">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!--END MODALS-->
<!--MODALS SUB-SERVICES -->
<div class="modal  bs-example-modal-lg" id="modalPay" tabindex="-1" role="dialog" aria-labelledby="defModalHead" aria-hidden="true" style="display: none;" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Cerrar</span></button>
                <h4 class="modal-title" id="defModalHead">Realizar pago</h4>
            </div>
            <div class="modal-body" id="modal_body">
                <form action="#" method="post" id="pay_form_action">
                    <div class="row">
                        <div class="col-sm-offset-2 col-sm-4"><h3>Total a pagar</h3></div>
                        <div class="col-sm-offset-2 col-sm-4"><h3><span id="debt" data-pay="0">$0</span>&nbsp;<span id="discount_debt" data-discount=""></span></h3></div>
                    </div>
                    <div class="row">
                        <br>
                        <div class="col-sm-offset-2 col-sm-4"><h3>Descuento extra($)</h3></div>
                        <div class="col-sm-offset-2 col-sm-4"><input type="number" min="0" value="" id="discount_ext" name="discount_ext" placeholder="$" class="form-control" readonly></div>
                    </div>
                    <div class="row">
                        <br>
                        <div class="col-sm-offset-2 col-sm-4"><h3>Efectivo Recibido($)</h3></div>
                        <div class="col-sm-offset-2 col-sm-4"><input type="number" min="0" value="" id="received" name="received" placeholder="$" class="form-control"></div>
                    </div>
                    <div class="row">
                        <br>
                        <div class="col-sm-offset-2 col-sm-4"><h3>Vuelto del cliente($)</h3></div>
                        <div class="col-sm-offset-2 col-sm-4"><h3 class="btn btn-info btn-lg" id="vuelto_cliente">$0</h3></div>
                    </div>
                    <div class="row">
                        <br>
                        <div class="col-sm-offset-8 col-sm-4 text-right">
                            <input type="checkbox" name="mail" id="mail_ticket"><strong>&nbsp; Enviar ticket por email</strong>
                            <br><br>
                            <input type="email" name="send_email" id="send_email" value="" class="form-control fade hidden">
                        </div>
                    </div>
                    <div class="row">
                        <br>
                        <div class="col-sm-offset-6 col-sm-6 text-right">
                            <button type="submit" class="btn btn-success btn-md disabled" id="buttonPay" disabled><span class="fa fa-dollar"></span>&nbsp;Cobrar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!--END MODALS-->
<!-- process pay -->
<div class="message-box message-box-info animated fadeIn" id="message-box-process">
    <div class="mb-container">
        <div class="mb-middle">
            <div class="mb-title"> Procesando el pago &nbsp; <i class="fa fa-circle-o-notch fa-spin style-fa"></i></div>
            <div class="mb-content">
                <p>Estamos realizando el pago, espere un momento por favor.</p>
            </div>
        </div>
    </div>
</div>
<!-- end process pay -->
<!-- pay -->
<div class="message-box message-box-success animated fadeIn" id="message-box-pay">
    <div class="mb-container">
        <div class="mb-middle">
            <div class="mb-title"><span class="fa fa-check"></span> Pago realizado correctamente</div>
            <div class="mb-content">
                <p>Estamos generando su ticket de venta, gracias.</p>
            </div>
        </div>
    </div>
</div>
<!-- end pay -->

<!-- Cancel -->
    <div class="message-box message-box-danger animated fadeIn" data-sound="alert" id="message-box-cancel">
        <div class="mb-container">
            <div class="mb-middle">
                <div class="mb-title"><span class="fa fa-remove"></span> Cancelar esta <strong>venta</strong> ?</div>
                <div class="mb-content">
                    <p>¿Estas seguro de cancelar esta venta?</p>
                    <p>Pulse <strong>"No"</strong> para continuar vendiendo. Pulse <strong>"Sí"</strong> para cancelarla.</p>
                </div>
                <div class="mb-footer">
                    <div class="pull-left  fade in" id="buttons_cancel">
                        <a href="#" id="cancel_sale" class="btn btn-success btn-lg">Si</a>
                        <button class="btn btn-default btn-lg mb-control-close">No</button>
                    </div>
                    <div class="pull-right fade hidden" id="message_cancel">
                        <div class="mb-title"><span class="fa fa-spinner fa-pulse fa-3x fa-fwe"></span> Cancelando</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<!-- end Cancel -->
<?php else:?>
<div class="page-content-wrap">
    <div class="row">
        <div class="col-sm-10">
            <header class="clearfix">
                <figure>
                    <img class="logo" src="data:image/svg+xml;charset=utf-8;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+Cjxzdmcgd2lkdGg9IjQxcHgiIGhlaWdodD0iNDFweCIgdmlld0JveD0iMCAwIDQxIDQxIiB2ZXJzaW9uPSIxLjEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHhtbG5zOnNrZXRjaD0iaHR0cDovL3d3dy5ib2hlbWlhbmNvZGluZy5jb20vc2tldGNoL25zIj4KICAgIDwhLS0gR2VuZXJhdG9yOiBTa2V0Y2ggMy40LjEgKDE1NjgxKSAtIGh0dHA6Ly93d3cuYm9oZW1pYW5jb2RpbmcuY29tL3NrZXRjaCAtLT4KICAgIDx0aXRsZT5MT0dPPC90aXRsZT4KICAgIDxkZXNjPkNyZWF0ZWQgd2l0aCBTa2V0Y2guPC9kZXNjPgogICAgPGRlZnM+PC9kZWZzPgogICAgPGcgaWQ9IlBhZ2UtMSIgc3Ryb2tlPSJub25lIiBzdHJva2Utd2lkdGg9IjEiIGZpbGw9Im5vbmUiIGZpbGwtcnVsZT0iZXZlbm9kZCIgc2tldGNoOnR5cGU9Ik1TUGFnZSI+CiAgICAgICAgPGcgaWQ9IklOVk9JQ0UtMiIgc2tldGNoOnR5cGU9Ik1TQXJ0Ym9hcmRHcm91cCIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoLTMwLjAwMDAwMCwgLTMwLjAwMDAwMCkiIGZpbGw9IiMyQThFQUMiPgogICAgICAgICAgICA8ZyBpZD0iWkFHTEFWTEpFIiBza2V0Y2g6dHlwZT0iTVNMYXllckdyb3VwIiB0cmFuc2Zvcm09InRyYW5zbGF0ZSgzMC4wMDAwMDAsIDE1LjAwMDAwMCkiPgogICAgICAgICAgICAgICAgPGcgaWQ9IkxPR08iIHRyYW5zZm9ybT0idHJhbnNsYXRlKDAuMDAwMDAwLCAxNS4wMDAwMDApIiBza2V0Y2g6dHlwZT0iTVNTaGFwZUdyb3VwIj4KICAgICAgICAgICAgICAgICAgICA8cGF0aCBkPSJNMzkuOTI0NjM2MywxOC40NDg2MjEgTDMzLjc3MDczNTgsMTEuODQyMjkyMyBMMzMuNzcwNzM1OCw0LjIxMDUyNjgxIEMzMy43NzA3MzU4LDIuODMwOTIyMzYgMzIuNzI5MzQxMSwxLjcxMjU0NDE0IDMxLjQ0MTczNzIsMS43MTI1NDQxNCBDMzAuMTU3NDExOSwxLjcxMjU0NDE0IDI5LjExNjAxNzMsMi44MzA5MjIzNiAyOS4xMTYwMTczLDQuMjEwNTI2ODEgTDI5LjExNjAxNzMsNi44NDUxMTcwNCBMMjQuNTMzNzM3NCwxLjkyNjAzNDcxIEMyMi4yNjgwNTg1LC0wLjUwNDQxNDA5NCAxOC4zMjkwMTcxLC0wLjUwMDEyNDQ4NCAxNi4wNjg4NzEsMS45MzAzMjQzMiBMMC42ODExNDgzMjksMTguNDQ4NjIxIEMtMC4yMjY5NDY5ODQsMTkuNDI1NjYyMSAtMC4yMjY5NDY5ODQsMjEuMDA2NzY4MiAwLjY4MTE0ODMyOSwyMS45ODIwNDk0IEMxLjU5MDE2NTc3LDIyLjk1OTA5MDUgMy4wNjU3ODIyMywyMi45NTkwOTA1IDMuOTczODc3NTUsMjEuOTgyMDQ5NCBMMTkuMzU5OTYwOSw1LjQ2Mzc1Mjc1IEMxOS44NjE0OTg0LDQuOTI4NDMxNDcgMjAuNzQ0Nzk4Niw0LjkyODQzMTQ3IDIxLjI0MzQ2NzIsNS40NjIxMDI5IEwzNi42MzE5MDcxLDIxLjk4MjA0OTQgQzM3LjA4ODU2NzUsMjIuNDcwNTE1IDM3LjY4MzM0MjgsMjIuNzEzNzAyOSAzOC4yNzgxMTgsMjIuNzEzNzAyOSBDMzguODc0MDIwNCwyMi43MTM3MDI5IDM5LjQ3MDAyNTIsMjIuNDcwNTE1IDM5LjkyNTA0NjIsMjEuOTgyMDQ5NCBDNDAuODMzNTUxMywyMS4wMDY3NjgyIDQwLjgzMzU1MTMsMTkuNDI1NjYyMSAzOS45MjQ2MzYzLDE4LjQ0ODYyMSBMMzkuOTI0NjM2MywxOC40NDg2MjEgWiIgaWQ9IkZpbGwtMSI+PC9wYXRoPgogICAgICAgICAgICAgICAgICAgIDxwYXRoIGQ9Ik0yMS4xMTEzOTc0LDEwLjIwNTg2MTIgQzIwLjY2NDM2ODIsOS43MjYzMDQ4MiAxOS45NDA2OTkzLDkuNzI2MzA0ODIgMTkuNDk0ODk5NiwxMC4yMDU4NjEyIEw1Ljk1OTg0Mjk2LDI0LjczMTM1OTIgQzUuNzQ2MTEzMiwyNC45NjAzNTg0IDUuNjI1MjExNDIsMjUuMjczNjA5OSA1LjYyNTIxMTQyLDI1LjYwMDA2MDIgTDUuNjI1MjExNDIsMzYuMTk0ODQ2IEM1LjYyNTIxMTQyLDM4LjY4MDcyOTcgNy41MDI3NzUwNyw0MC42OTYxODYzIDkuODE4NDUzOTgsNDAuNjk2MTg2MyBMMTYuNTE5NDg2Myw0MC42OTYxODYzIEwxNi41MTk0ODYzLDI5LjU1NTQxMDIgTDI0LjA4NTA2ODgsMjkuNTU1NDEwMiBMMjQuMDg1MDY4OCw0MC42OTYxODYzIEwzMC43ODY2MTM1LDQwLjY5NjE4NjMgQzMzLjEwMjI5MjQsNDAuNjk2MTg2MyAzNC45Nzk3NTM2LDM4LjY4MDcyOTcgMzQuOTc5NzUzNiwzNi4xOTQ4NDYgTDM0Ljk3OTc1MzYsMjUuNjAwMDYwMiBDMzQuOTc5NzUzNiwyNS4yNzM2MDk5IDM0Ljg1OTY3MTUsMjQuOTYwMzU4NCAzNC42NDUyMjQ1LDI0LjczMTM1OTIgTDIxLjExMTM5NzQsMTAuMjA1ODYxMiBaIiBpZD0iRmlsbC0zIj48L3BhdGg+CiAgICAgICAgICAgICAgICA8L2c+CiAgICAgICAgICAgIDwvZz4KICAgICAgICA8L2c+CiAgICA8L2c+Cjwvc3ZnPg==" alt="">
                </figure>
                <div class="company-info">
                    <h2 class="title">Boutique "XXXX"</h2>
                    <span>Plaza Las Americas Local ##</span>
                    <span class="line"></span>
                    <a class="phone" href="tel:3167987">Citas al 312-3456</a>
                    <span class="line"></span>
                    <a class="email" href="mailto:company@example.com">recepcion@boutique.com</a>
                </div>
            </header>
        </div>
        <div class="col-sm-2">
            <button class="btn btn-success" onclick="printPage('<?=$url?>')">Imprimir</button>
        </div>
    </div>
    <section class="row">
        <div class="col-sm-12">
            <div class="details clearfix">
                <div class="client left">
                    <p>Comprobante de compra:</p>
                    <?php if($data->getEmailClient()=="ventas@beautyroom.com"):?>
                        <p class="name">Venta General</p>
                    <?php else:?>
                        <p class="name"><?=$data->getNameClient()." ".$data->getLastNameClient()." ".$data->getSecondNameClient()?></p>
                        <!--p>
                            796 Silver Harbour,<br>
                            TX 79273, US
                            data-toggle='tooltip' data-placement='top' title='Imprimir recibo de compra'
                        </p-->
                        <a href="mailto:<?=$data->getEmailClient()?>"><?=$data->getEmailClient()?></a>
                    <?php endif;?>
                </div>
                <div class="data right">
                    <div class="title">Comprobante #<?=$data->getSalid()?></div>
                    <?php
                        $date_creation = date("d/m/Y",strtotime($data->getDateCreationSale()));
                    ?>
                    <div class="date">
                        Fecha de compra: <?=$date_creation?><br>
                    </div>
                </div>
            </div>
            <div class="table-wrapper">
                <table>
                    <tbody class="head">
                    <tr>
                        <th class="no"></th>
                        <th class="desc"><div>Descripción</div></th>
                        <th class="qty"><div>Cantidad</div></th>
                        <th class="unit"><div>Precio</div></th>
                        <th class="qty"><div>Descuento</div></th>
                        <th class="total"><div>Total</div></th>
                    </tr>
                    </tbody>
                    <tbody class="body">
                    <?php $total = (int) 0; $discount = (int) 0;?>
                    <?php foreach($list as $key => $val):?>
                    <?php $total = $total + $val['total_payout'];?>
                    <?php if($val['type']=="SERVICIO"):?>
                        <tr>
                            <td class="no"><?=$key+1?></td>
                            <td class="desc"><strong><?=$val['name_service'];?> &nbsp; <?=$val['name_subservice'];?> </strong></td>
                            <td class="qty"><?=$val['quantity_services']?></td>
                            <td class="unit">$<?=number_format($val['price'],"2",".",",")?></td>
                            <td class="qty">-$<?=number_format($val['discount_services'],"2",".",",")?></td>
                            <td class="total">$<?=number_format($val['total_payout'],"2",".",",")?></td>
                            <?php $discount = $discount + $val['discount_services']; ?>
                        </tr>
                    <?php elseif($val['type']=="VENTAS"):?>
                        <tr>
                            <td class="no"><?=$key+1?></td>
                            <td class="desc"><strong><?=$val['name_brand'];?> &nbsp; <?=$val['name_product'];?> </strong></td>
                            <td class="qty"><?=$val['quantity_services']?></td>
                            <td class="unit">$<?=number_format($val['sale_price'],"2",".",",")?></td>
                            <td class="qty">-$<?=number_format($val['percentage_product'],"2",".",",")?></td>
                            <td class="total">$<?=number_format($val['total_payout'],"2",".",",")?></td>
                            <?php $discount = $discount + $val['percentage_product']; ?>
                        </tr>
                    <?php endif?>
                    <?php endforeach;?>
                    </tbody>
                </table>
            </div>
            <div class="no-break">
                <table class="grand-total">
                    <tbody>
                    <tr>
                        <td class="no"></td>
                        <td class="desc"></td>
                        <td class="qty"></td>
                        <td class="unit">SUBTOTAL:</td>
                        <td class="total">$<?=number_format($total,"2",".",",")?></td>
                    </tr>
                    <tr>
                        <td class="no"></td>
                        <td class="desc"></td>
                        <td class="qty"></td>
                        <td class="unit">IVA 16%:</td>
                        <?php
                            $iva = ($total * 16)/100;
                        ?>
                        <td class="total">$<?=number_format($iva,"2",".",",")?></td>
                    </tr>
                    <tr>
                        <td class="grand-total" colspan="5"><div><span>TOTAL:</span>$<?=number_format($total+$iva,"2",".",",")?></div></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </section>

    <footer>
        <div class="row">
            <div class="col-sm-12">
                <div class="thanks">Muchas Gracias! Vuelva pronto</div>
                <div class="notice">
                    <div>INFORMACIÓN ADICIONAL:</div>
                    <?php if($discount>0):?>
                        <div>Usted ha tenido un descuento de $<?=number_format($discount,"2",".",",")?> esperemos vuelva pronto.</div>
                    <?php endif?>

                </div>
                <div class="end">Este comprobante de pago no es ni será una factura electrónica.</div>
            </div>
        </div>
    </footer>
</div>
    <script type="text/javascript">
        function closePrint () {
            document.body.removeChild(this.__container__);
        }

        function setPrint () {
            this.contentWindow.__container__ = this;
            this.contentWindow.onbeforeunload = closePrint;
            this.contentWindow.onafterprint = closePrint;
            this.contentWindow.focus(); // Required for IE
            this.contentWindow.print();
        }

        function printPage (sURL) {
            var oHiddFrame = document.createElement("iframe");
            oHiddFrame.onload = setPrint;
            oHiddFrame.style.visibility = "hidden";
            oHiddFrame.style.position = "fixed";
            oHiddFrame.style.right = "0";
            oHiddFrame.style.bottom = "0";
            oHiddFrame.src = sURL;
            document.body.appendChild(oHiddFrame);
        }
        printPage("<?=$url?>");
    </script>
<?php endif;?>