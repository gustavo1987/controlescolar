<ul class="breadcrumb">
    <li><a href="/dashboard">Inicio</a></li>
    <li><a href="#">Clases</a></li>
</ul>
<!-- PAGE TITLE -->
<div class="page-title">
    <div class="col-sm-12 text-left">
        <h2>Clases</h2>
    </div>
</div>
<!-- END PAGE TITLE -->
<div class="page-content-wrap" id="sales">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-success">
                <div class="panel-heading">
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table id="tableSales" class="table table-bordered table-hover table-actions dataTable" data-order="0" data-filter="desc">
                            <thead>
                            <tr>
                                <th>#clase</th>
                                <th>Credito</th>
                                <th>Materia</th>
                                <th>Fecha</th>
                                <th>Generación</th>
                                <th>Opciones</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach($result as $values):?>
                                <?php $status_total = $values['total']<=0?"info":"warning";?>
                                <?php $status_message= $values['total']<=0?"En proceso":$values['status'];?>
                                <tr class='<?=$values["status"]=="Pagado"?"success":$status_total?>'>
                                    <td><?=$values['salid']?></td>
                                    <td class='text-right'><?=$values['total']?></td>
                                    <td><?=$values['name']?></td>
                                    <td><?=$values['date']?></td>
                                    <td><?=$status_message?></td>
                                    <td>
                                        <?php if($values["status"]=="Pagado"):?>
                                            <a target="_blank" href="<?=$this->url->get("dashboard/sales/addstudent/{$values['salid']}")?>" data-toggle="tooltip" data-placement="top" title="" data-original-title="Agregar alumnos a clase"><span class="fa fa-user-plus fa-2x"></span></a>

                                            <a target="_blank" href="<?=$this->url->get("dashboard/sales/list/{$values['salid']}")?>" data-toggle="tooltip" data-placement="top" title="" data-original-title="Información general de clase"><span class="fa fa-eye fa-2x"></span></a>


                                            
                                        <?php elseif($values["status"]=="Apartado"):?>
                                            <a target="_blank" href="<?=$this->url->get("dashboard/sales/addstudent/{$values['salid']}")?>" data-toggle="tooltip" data-placement="top" title="" data-original-title="Agregar alumnos a clase"><span class="fa fa-user-plus fa-2x"></span></a>

                                            <a target="_blank" href="<?=$this->url->get("dashboard/sales/list/{$values['salid']}")?>" data-toggle="tooltip" data-placement="top" title="" data-original-title="Información general de clase"><span class="fa fa-eye fa-2x"></span></a>
                                        <?php elseif($values['total']<=0):?>
                                            <a target="_blank" href="<?=$this->url->get("dashboard/sales/addstudent/{$values['salid']}")?>" data-toggle="tooltip" data-placement="top" title="" data-original-title="Editar clase"><span class="fa fa-edit fa-2x"></span></a>
                                        <?php else:?>
                                            <a target="_blank" href="<?=$this->url->get("dashboard/sales/addstudent/{$values['salid']}")?>" data-toggle="tooltip" data-placement="top" title="" data-original-title="Agregar alumnos a clase"><span class="fa fa-user-plus fa-2x"></span></a>

                                            <a target="_blank" href="<?=$this->url->get("dashboard/sales/list/{$values['salid']}")?>" data-toggle="tooltip" data-placement="top" title="" data-original-title="Información general de clase"><span class="fa fa-eye fa-2x"></span></a>
                                        <?php endif;?>
                                    </td>
                                </tr>
                            <?php endforeach;?>
                            </tbody>
                        </table>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                        </div>
                        <!--div class="col-md-6">
                            <h4>Monto total del día</h4>

                            <table class="table table-striped">
                                <tbody><tr>
                                    <td width="200"><strong>Subtotal:</strong></td><td class="text-right" id="subtotal">$<?=$breakdown['subtotal']?></td>
                                </tr>
                                <tr id="iva" data-value="16">
                                    <td><strong>IVA 16%:</strong></td><td class="text-right" id="iva_general">$<?=$breakdown['iva_general']?></td>
                                </tr>
                                <tr class="total">
                                    <td>Total de las clases</td><td class="text-right" id="amount_to_pay" data-value="">$<?=$breakdown['total']?></td>
                                </tr>
                                </tbody></table>
                        </div-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function closePrint () {
        document.body.removeChild(this.__container__);
    }

    function setPrint () {
        this.contentWindow.__container__ = this;
        this.contentWindow.onbeforeunload = closePrint;
        this.contentWindow.onafterprint = closePrint;
        this.contentWindow.focus(); // Required for IE
        this.contentWindow.print();
    }

    function printPage (sURL) {
        var oHiddFrame = document.createElement("iframe");
        oHiddFrame.onload = setPrint;
        oHiddFrame.style.visibility = "hidden";
        oHiddFrame.style.position = "fixed";
        oHiddFrame.style.right = "0";
        oHiddFrame.style.bottom = "0";
        oHiddFrame.src = sURL;
        document.body.appendChild(oHiddFrame);
    }
</script>