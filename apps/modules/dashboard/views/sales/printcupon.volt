<!DOCTYPE html>
<html>
<head>
    <title></title>
</head>
<body style="width: 300px; font-size: 13px; font-family: arial;">
<div class="page-content-wrap">
    <div class="content" id="content">
        <div>
            <div style="width: 330px;">
                <div>
                    <center><div style="text-align: center;font: bold 95px 'Arial';">PONS</div><br>
                        <div style="font:bold 20px 'Arial';">COLLECTION <br> BOUTIQUE</div>
                        <!--div style="font:bold 15px ';Arial';"></div><br><br--> <br><br>
                        <div style="text-align:left; font:bold 15px 'Arial';">Plaza las Américas Av. Ramón Mendoza 102 Int D6 Col. José María Pino Suárez, Villahermosa Tabasco CP 86029 <br>RFC MAPJ850930CU5  
                        </div>
                    </center>
                </div>
                <div style="width: 330px;">
                    <div style="font:bold 20px 'Arial';">CUPÓN  DE<br> DESCUENTO</div>
                    <table cellpadding="3" cellspacing="0" style="font:bold 15px 'Arial';">
                        <tbody>
                        <tr>
                            <td>No Cupón:</td>
                            <td><?=$cupon->getCupid()?></td>
                        </tr>
                        <tr>
                            <td>Fecha :</td>
                            <td><?=date("d-m-Y h:m",strtotime($cupon->getDateCreation()))?></td>
                        </tr>
                        <tr>
                            <td>Vence :</td>
                            <td><?php $vence = strtotime($cupon->getDateCreation()); echo $vence2=date("d-m-Y h:m", strtotime("+1 month",$vence));?></td>
                        </tr>
                        

                        <tr>
                            <td>Beneficiario:</td>
                            <td><?=$cupon->getName()?></td>
                        </tr>

                        <tr style="font:bold 20px 'Arial';">
                            <td style="width: 100%;text-align: center;">Valido por:</td>
                        </tr>
                        <tr style="font:bold 20px 'Arial';">
                            <td style="width: 100%;text-align: center;">$<?=number_format($cupon->getTotal(),"2",".",",")?></td>
                        </tr>
                        
                        </tbody>
                    </table>
                </div>
                <div class="clearfix"></div>
            </div>
            <br><br>
            <div style="width: 330px;">
                <br><br>
                <table with="400" align="left">
                    <tbody><tr>
                        <td><h1 style="font-size:15px;">Cupones solo validos en compras en efectivo, vencimiento de cupón es de un mes.</h1></td>
                        
                    </tr>
                    </tbody></table>
            </div>
            <br><br><br>
            <div style="width: 330px; height: 300px;">
                <div>
                    <center>
                        <div style="text-align:left; font:bold 15px 'Arial';">Siguenos en nuestras redes sociales:
                            <br><br> Facebook e Instagram como:<br>
                            <span style="text-decoration: underline;">PonsCollection</span>
                            <br> WhatsApp : 9934321895
                        </div>
                        <br><br><br>
                        <div style="text-align:center; font:bold 15px 'Arial';text-transform: uppercase;">
                            Gracias por hacer tu compra en <span style="text-align: center;font: bold 20px 'Arial';">PONS COLLECTION</span>
                            ¡Tu aliado para deslumbrar!
                        </div>
                    </center>
                </div>
            </div>
        </div>
    </div>
</div>  
</body>
</html>

