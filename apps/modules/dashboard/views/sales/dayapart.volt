<div class="page-content-wrap" id="sales">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-success">
                <!--div class="panel-heading">
                    <div class="col-sm-3"><h3 class="panel-title">Estatus de apartado:</h3></div>
                    <div class="col-sm-3"><span class="fa fa-cube fa-2x text-success"></span>&nbsp;Pagado</div>
                    <div class="col-sm-3"><span class="fa fa-cube fa-2x text-warning"></span>&nbsp;En proceso</div>
                </div-->
                <div class="panel-body">
                    <div class="table-responsive">
                        <table id="tableSalesApart" class="table table-bordered table-hover table-actions dataTable generalDT" data-order="0" data-filter="desc">
                            <thead>
                            <tr>
                                <th>#Apartado</th>
                                <th>Cliente</th>
                                <th>Total</th>
                                <th>Abonado</th>
                                <th>Restante</th>
                                <th>Vendedor</th>
                                <th>Fecha</th>
                                <th>Opciones</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach($result as $values):?>
                            <?php
                                    $status_total = $values['total']<=0?"info":"warning";
                                    $apart = $values['apart'];
                                    $total = $values['total_clean'];
                            ?>
                            <tr class='<?=$values['salid']?> <?=$values["status"]=="Pagado"?"success":$status_total?>' id="<?=$values['salid']?>">
                                <td><?=$values['salid']?></td>
                                <td><?=$values['name_client'];?></td>
                                <td class='text-right'>$<?=$values['total'];?></td>
                                <td class='text-right'>$<?=number_format($apart,"2",".",",")?></td>
                                <td>$<?=number_format($total-$apart,"2",".",",")?></td>
                                <td><?=$values['user']?></td>
                                <td><?=$values['date']?></td>
                                <td data-id="<?=$values['salid']?>">
                                    <?php if($values["status"]=="Pagado"):?>
                                    <a onclick="printPage('<?=$values['url_print']?>')" data-toggle='tooltip' data-placement='top' title='Imprimir comprobante de venta'><span class='fa fa-print fa-2x'></span></a>&nbsp;<a target='_blank' href="<?=$values['url_view']?>" data-toggle='tooltip' data-placement='top' title='Ver comprobante de venta'><span class='fa fa-eye fa-2x'></span></a>&nbsp;<a target="_blank" href="<?=$this->url->get("dashboard/sales/completed/{$values['salid']}")?>" data-toggle="tooltip" data-placement="top" title="" data-original-title="Información general de venta"><span class="fa fa-folder-open fa-2x"></span></a>
                                    <?php elseif($values['total']<=0):?>
                                    <a target="_blank" href="<?=$this->url->get("dashboard/sales/number/{$values['salid']}")?>" data-toggle="tooltip" data-placement="top" title="" data-original-title="Editar venta"><span class="fa fa-edit fa-2x"></span></a>
                                    <?php else:?>
                                    <a target="_blank" href="<?=$this->url->get("dashboard/sales/completedapart/{$values['salid']}")?>" data-toggle="tooltip" data-placement="top" title="" data-original-title="Información general de venta"><span class="fa fa-folder-open fa-2x"></span></a>&nbsp;
                                    <?php endif;?>
                                    <?php if($auth["rol"]=="SUPERADMIN"):?>

                                    <span class="deleteElementapart cursor_pointer" data-toggle="tooltip" data-id="<?=$values['salid']?>" data-placement="top" title="" data-original-title="Eliminar Apartado"><i class="fa fa-remove fa-2x"></i></span>
                                  
                                    <a target="_blank" class="createcupon" id="createcupon" data-id="<?=$values['salid']?>" data-toggle="tooltip" data-placement="top" title="" data-original-title="Realizar cupón"><span class="fa fa-edit fa-2x"></span></a>
                                    <?php endif;?>
                                </td>
                            </tr>
                            <?php endforeach;?>
                            </tbody>
                        </table>
                    </div>
                    <!--div class="row">
                        <div class="col-md-6">
                        </div>
                        <div class="col-md-6">
                            <h4>Monto total del día</h4>

                            <table class="table table-striped">
                                <tbody><tr>
                                    <td width="200"><strong>Subtotal:</strong></td><td class="text-right" id="subtotal">$ <?=$breakdown['subtotal']?></td>
                                </tr>
                                <tr id="iva" data-value="16">
                                    <td><strong>IVA 16%:</strong></td><td class="text-right" id="iva_general">$ <?=$breakdown['iva_general']?></td>
                                </tr>
                                <tr class="total">
                                    <td>Total de las ventas</td><td class="text-right" id="amount_to_pay" data-value=""><?=$breakdown['total']?></td>
                                </tr>
                                </tbody></table>
                        </div>
                    </div-->
                </div>
            </div>
        </div>
    </div>
</div>

<div class="message-box animated fadeIn" data-sound="alert" id="mb-create-cupon-row">
    <div class="mb-container">
        <div class="mb-middle">
            <div class="mb-title"><span class="fa fa-times"></span> Crear <strong>Cupón</strong> ?</div>
            <div class="mb-content">
                <p>¿Estas seguro de hacer cupón este apartado?</p>
                <p>Presione "Si" si esta seguro.</p>
            </div>
            <div class="mb-footer">
                <div class="pull-right">
                    <button class="btn btn-success btn-lg mb-control-yes">Si</button>
                    <button class="btn btn-default btn-lg mb-control-close">No</button>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="message-box animated fadeIn" data-sound="alert" id="mb-remove-row">
    <div class="mb-container">
        <div class="mb-middle">
            <div class="mb-title"><span class="fa fa-times"></span> Eliminar <strong>Datos</strong> ?</div>
            <div class="mb-content">
                <p>¿Estas seguro de eliminar esta fila?</p>
                <p>Presione "Si" si esta seguro.</p>
            </div>
            <div class="mb-footer">
                <div class="pull-right">
                    <button class="btn btn-success btn-lg mb-control-yes" data-id="">Si</button>
                    <button class="btn btn-default btn-lg mb-control-close">No</button>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function closePrint () {
        document.body.removeChild(this.__container__);
    }

    function setPrint () {
        this.contentWindow.__container__ = this;
        this.contentWindow.onbeforeunload = closePrint;
        this.contentWindow.onafterprint = closePrint;
        this.contentWindow.focus(); // Required for IE
        this.contentWindow.print();
    }

    function printPage (sURL) {
        var oHiddFrame = document.createElement("iframe");
        oHiddFrame.onload = setPrint;
        oHiddFrame.style.visibility = "hidden";
        oHiddFrame.style.position = "fixed";
        oHiddFrame.style.right = "0";
        oHiddFrame.style.bottom = "0";
        oHiddFrame.src = sURL;
        document.body.appendChild(oHiddFrame);
    }
</script>