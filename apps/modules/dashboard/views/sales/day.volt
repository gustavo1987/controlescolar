<div class="page-content-wrap" id="sales">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-success">
                <div class="panel-heading">
                    <div class="col-sm-3"><h3 class="panel-title">Estatus de clases:</h3></div>
                    <div class="col-sm-3"><span class="fa fa-cube fa-2x text-success"></span>&nbsp;Realizadas</div>
                    <div class="col-sm-3"><span class="fa fa-cube fa-2x text-warning"></span>&nbsp;Terminada</div>
                    <div class="col-sm-3"><span class="fa fa-cube fa-2x text-info"></span>&nbsp;En proceso</div>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table id="tableSales" class="table table-bordered table-hover table-actions dataTable" data-order="0" data-filter="desc">
                            <thead>
                            <tr>
                                <th>#clase</th>
                                <th>Materia</th>
                                <th>Carrera</th>
                                <th>Estatus</th>
                                <th>Opciones</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach($result as $values):?>
                                <tr class='<?=$values->getStatus()=="Pagado"?"success":$status_total?>'>
                                    <td><?=$values->getSalid();?></td>
                                    <td><?=$values->getNameProduct();?></td>
                                    <td><?=$values->getNameBrand();?></td>
                                    <td><?=$values->getStatus();?></td>

                                    <td>
                                        
                                            <a target="_blank" href="<?=$this->url->get("dashboard/sales/completed/{$values->getSalid()}")?>" data-toggle="tooltip" data-placement="top" title="" data-original-title="Información general de clase"><span class="fa fa-folder-open fa-2x"></span></a>
                                    </td>
                                </tr>
                            <?php endforeach;?>
                            </tbody>
                        </table>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                        </div>
                        <!--div class="col-md-6">
                            <h4>Monto total del día</h4>

                            <table class="table table-striped">
                                <tbody><tr>
                                    <td width="200"><strong>Subtotal:</strong></td><td class="text-right" id="subtotal">$<?=$breakdown['subtotal']?></td>
                                </tr>
                                <tr id="iva" data-value="16">
                                    <td><strong>IVA 16%:</strong></td><td class="text-right" id="iva_general">$<?=$breakdown['iva_general']?></td>
                                </tr>
                                <tr class="total">
                                    <td>Total de las clases</td><td class="text-right" id="amount_to_pay" data-value="">$<?=$breakdown['total']?></td>
                                </tr>
                                </tbody></table>
                        </div-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function closePrint () {
        document.body.removeChild(this.__container__);
    }

    function setPrint () {
        this.contentWindow.__container__ = this;
        this.contentWindow.onbeforeunload = closePrint;
        this.contentWindow.onafterprint = closePrint;
        this.contentWindow.focus(); // Required for IE
        this.contentWindow.print();
    }

    function printPage (sURL) {
        var oHiddFrame = document.createElement("iframe");
        oHiddFrame.onload = setPrint;
        oHiddFrame.style.visibility = "hidden";
        oHiddFrame.style.position = "fixed";
        oHiddFrame.style.right = "0";
        oHiddFrame.style.bottom = "0";
        oHiddFrame.src = sURL;
        document.body.appendChild(oHiddFrame);
    }
</script>