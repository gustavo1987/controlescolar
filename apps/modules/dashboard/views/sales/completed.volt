<?php $url = $this->url->get('dashboard/sales/print/'.$data->getSalid())?>
    <div class="page-content-wrap" id="sales">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-success">
                    <div class="panel-heading">
                    </div>
                    <span class="hidden" id="uid" data-value="{{data.getUid()}}"></span>
                    <span class="hidden" id="salid" data-value="{{data.getSalid()}}"></span>
                    <span class="hidden" id="clid" data-value="{{data.getClid()}}"></span>
                    <span class="hidden" id="email" data-value="{{data.getEmail()}}"></span>
                    <div class="panel-body">

                        <!-- INVOICE -->
                        <div class="invoice">
                            <?php if($data->getEmailClient()=="ventas@beautyroom.com"):?>
                                <h4 class="type_sales" style="font-weight: lighter"><strong class="name_user_active">Control Escolar</strong></h4>
                            <?php else:?>
                                <h4 class="type_sales" style="font-weight: lighter">Control Escolar</h4>
                            <?php endif;?>
                            <div class="">
                                <table class="table table-bordered dataTable" id="tableSales">
                                    <thead>
                                    <tr>
                                        <th class="success">&nbsp;</th>
                                        <th class="success">Materia</th>
                                        <th class="success">Maestro</th>
                                        <th class="success text-center">Credito</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php if(count($list)>=1):?>
                                        <?php $calculate = 0; foreach($list as $val): $subsalid = $val->getSubsalid()?>
                                            <?php if($val->getType()=="SERVICIO"):?>
                                                <tr data-type="service" data-value="<?=$subsalid;?>" class="<?=$subsalid;?> <?=$val->getStatus()=='CANCELADO'?'danger':null?>" >
                                                    <td class="text-center">
                                                        <span class="deleteElement cursor_pointer" data-toggle="tooltip" data-placement="top" title="" data-original-title="Eliminar clase"><i class="fa fa-remove fa-2x"></i></span>
                                                    </td>
                                                    <td class="description">
                                                        <strong class="name_service"><?=$val->getNameService()?></strong>
                                                        <p class="name_sub_service"><?=$val->getNameSubservice()?>
                                                        </p>
                                                    </td>
                                                    <td class="add_stylist">
                                                        <button class="btn btn-success"><?=$val->getUidAttend()?$val->getNameAttend()." ".$val->getLastNameAttend():"Asignar Vendedora" ?></button>
                                                    </td>
                                                    <td class="text-center price_service" data-value="<?=$val->getPrice()?>">$<?=$val->getPrice().".00"?></td>
                                                    <td class="text-center count_subtotal">
                                                        <input readonly type="number" min="1" max="100" value="<?=$val->getQuantitySs()?>" class="form-control comission">
                                                    </td>
                                                    <?php $subtotal = $val->getQuantitySs()*$val->getPrice()?>
                                                    <td class="text-center subtotal_service" data-value="<?=$subtotal?>">
                                                        <p>$<?=$subtotal?>.00</p>
                                                    </td>
                                                    <?php $discount = 0?>
                                                    <td class="text-center discount_service">
                                                        <?php if($val->getDiscount()):?>
                                                            <?php $discount = number_format(($subtotal*$val->getDiscount())/100,2,".","")?>
                                                            <strong>-$<?=$discount?></strong>
                                                        <?php endif;?>
                                                    </td>
                                                    <?php
                                                    $total = number_format($subtotal-$discount,"2",".",'');
                                                    if($val->getStatus()=='CANCELADO'){
                                                        $total = 0;
                                                    }
                                                    $calculate = $calculate+$total;
                                                    ?>
                                                    <td class="text-center total_service" data-discount="<?= number_format(($val->getPrice()*$val->getDiscount())/100,2,".","")?>" data-value="<?=$total?>"><?=$total?></td>
                                                </tr>
                                            <?php elseif($val->getType()=="VENTAS"):?>
                                                <tr data-type="product_sale" data-value="<?=$subsalid;?>" class="<?=$subsalid;?> <?=$val->getStatus()=='CANCELADO'?'danger':'info'?>">
                                                    <td class="text-center">
                                                        <span class="deleteElement cursor_pointer" data-toggle="tooltip" data-placement="top" title="" data-original-title="Eliminar Clase"><i class="fa fa-remove fa-2x"></i></span>
                                                    </td>
                                                    <td class="description">
                                                        <strong class="name_service"><?=$val->getNameBrand()?> <span class="fa fa-info consumer-product" data-toggle="tooltip" data-placement="top" data-original-title="Clase."></span></strong>
                                                        <p class="name_sub_service"><?=$val->getNameProduct()?>
                                                            
                                                        </p>
                                                    </td>
                                                    <td class="add_stylist">
                                                        <button class="btn btn-success"><?=$val->getUidAttend()?$val->getNameAttend()." ".$val->getLastNameAttend():"Asignar Maestro" ?></button>
                                                    </td>
                                                    <td class="text-center price_service" data-value="<?=$val->getSalePrice()?>"><?=$val->getSalePrice()?></td>
                                                    
                                                </tr>
                                            <?php elseif($val->getType()=="CONSUMO"):?>
                                                <tr data-type="product" data-value="<?=$subsalid;?>" class="<?=$subsalid;?> <?=$val->getStatus()=='CANCELADO'?'danger':'warning'?>">
                                                    <td class="text-center">
                                                        <span class="deleteElement cursor_pointer" data-toggle="tooltip" data-placement="top" title="" data-original-title="Eliminar clase"><i class="fa fa-remove fa-2x"></i></span>
                                                    </td>
                                                    <td class="description">
                                                        <strong class="name_service"><?=$val->getNameBrand()?> <span class="fa fa-info consumer-product" data-toggle="tooltip" data-placement="top" data-original-title="Clase"></span></strong>
                                                        <p class="name_sub_service"><?=$val->getNameProduct()?></p>
                                                    </td>
                                                    <td class="add_stylist">
                                                        <button class="btn btn-success"><?=$val->getUidAttend()?$val->getNameAttend()." ".$val->getLastNameAttend():"Asignar Maestro" ?></button>
                                                    </td>
                                                    <td class="text-center price_service" data-value="0">$0</td>
                                                    <td class="text-center count_subtotal">
                                                        <input readonly type="number" min="1" max="10000" value="<?=$val->getQuantitySs()?>" class="form-control comission">
                                                    </td>
                                                    <td class="text-center subtotal_service" data-value="0">
                                                        <p>0</p>
                                                    </td>
                                                    <td class="text-center discount_service"><strong>$0</strong></td>
                                                    <td class="text-center total_service" data-discount="0" data-value="0">0</td>
                                                </tr>
                                            <?php endif;?>
                                        <?php endforeach?>
                                    <?php endif;?>
                                    </tbody>
                                </table>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                </div>
                            </div>
                            <?php if($auth["rol"]=="SUPERADMIN" and $data->getStatus()=="PAY"):?>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="pull-right push-down-20">
                                            <button class="btn btn-danger" id="cancel_checkout"><span class="fa fa-remove"></span> Cancelar clase</button>
                                        </div>
                                    </div>
                                </div>
                            <?php endif;?>
                        </div>
                        <!-- END INVOICE -->

                    </div>
                </div>

            </div>
        </div>
    </div>


<!-- Cancel -->
<div class="message-box message-box-danger animated fadeIn" data-sound="alert" id="message-box-cancel">
    <div class="mb-container">
        <div class="mb-middle">
            <div class="mb-title"><span class="fa fa-remove"></span> Cancelar esta <strong>venta</strong> ?</div>
            <div class="mb-content">
                <p>¿Estas seguro de cancelar esta venta?</p>
                <p>Pulse <strong>"No"</strong> para continuar vendiendo. Pulse <strong>"Sí"</strong> para cancelarla.</p>
            </div>
            <div class="mb-footer">
                <div class="pull-left  fade in" id="buttons_cancel">
                    <a href="#" id="cancel_sale" class="btn btn-success btn-lg">Si</a>
                    <button class="btn btn-default btn-lg mb-control-close">No</button>
                </div>
                <div class="pull-right fade hidden" id="message_cancel">
                    <div class="mb-title"><span class="fa fa-spinner fa-pulse fa-3x fa-fwe"></span> Cancelando</div>
                </div>
            </div>
        </div>
    </div>
</div>