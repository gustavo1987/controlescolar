<!DOCTYPE html>
<html lang="en">
<head>
    <title>Campus Virtual</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="shortcut icon" href="/dash/img/logo-100.png">
    <link href="https://fonts.googleapis.com/css?family=Nunito+Sans:400,600,700" rel="stylesheet">
    {% block head %}
        {{ assets.outputCss('functions') }}
    {% endblock %}
    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    {{ content() }}
    {% block head %}
    {{ assets.outputJs('JsIndexLogin') }}
    {% endblock %}
</body>
</html>