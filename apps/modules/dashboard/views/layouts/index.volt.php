<!DOCTYPE html>
<html lang="es-MX">
<head>
    <?php $auth = $this->session->get("auth"); /*$status_box = $this->session->get("box");*/?>
    <?php $meta= $this->session->get("meta");?>

    <?php $back= $this->session->get("back");?>
    <title>Control escolar ideiberoamerica</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="shortcut icon" href="/dash/img/logo-100.png">
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <meta name="robots" content="nofollow">
    <meta name="googlebot" content="nofollow">
    <meta name="google" content="notranslate" />
    <meta name="author" content="C-Developers" />
    <meta name="copyright" content="2018 c-develpers.com Todos los derechos reservados." />
    <meta name="application-name" content="Plataforma educativa de Ideiberoamerica México" />
    <link rel="author" href="https://c-developers.com/"/>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    
        <?= $this->assets->outputCss('CssIndex') ?>
    
    <?php $cssPlugins = $this->assets->collection('cssPlugins'); ?>
    <?php if (!empty($cssPlugins)) { ?>
        <?php echo $this->assets->outputCss('cssPlugins'); ?>
    <?php } ?>
    <?php if ($this->router->getControllerName()=='client' && ($this->router->getActionName()=='control')) { ?>
    <link rel="stylesheet" type="text/css" href="/dash/css/scroll/perfect-scrollbar.min.css" />
    <?php } ?>
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
    <!--link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,300,500,600,700&subset=latin,latin-ext"-->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/sweetalert2@7.29.2/dist/sweetalert2.min.css">
    <script src="//cdnjs.cloudflare.com/ajax/libs/handlebars.js/1.3.0/handlebars.js"></script>
    <!-- Cache Control  -->
    <meta http-equiv="cache-control" content="max-age=0" />
    <meta http-equiv="cache-control" content="no-cache" />
    <meta http-equiv="expires" content="0" />
    <meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
    <meta http-equiv="pragma" content="no-cache" />
</head>
<body>
    <div class="page-container">
        <!--div class="page-sidebar mCustomScrollbar _mCS_1 mCS-autoHide page-sidebar-fixed scroll d-none" style="display: none">
            <ul class="x-navigation x-navigation-custom">
                <li class="xn-profile">
                    <a href="<?= $this->url->get('dashboard/user/profile') ?>" class="profile-mini">
                        <img src="/dash/assets/images/users/thumbnail/<?= $auth['photo'] ?>" alt="Gustavo"/>
                    </a>
                    <div class="profile" style="display: none">
                        <div class="profile-image">
                            <img src="/dash/assets/images/users/thumbnail/<?= $auth['photo'] ?>" alt="Gustavo"/>
                        </div>
                        <div class="profile-data">
                            <div class="profile-data-name"><?= $auth['username'] ?></div>
                        </div>
                        <div class="profile-controls">
                            <a href="<?= $this->url->get('dashboard/user/profile') ?>" class="profile-control-left" title="Editar perfil"><span class="fa fa-info"></span></a>
                            <a href="<?= $this->url->get('dashboard') ?>" class="profile-control-right" title="Mensajes de post"><span class="fa fa-envelope"></span></a>
                        </div>
                    </div>
                </li>
                <div style="<?php echo $auth['rol']=="STUDENT"?"display: none;":"";  ?>">
                <li class="xn-title">Navegación</li>
                <?php if ($auth['rol'] == 'SUPERADMIN' || $auth['rol'] == 'ADMIN') { ?>
                <li class="<?php echo $this->router->getControllerName()=='index'?'active':''?>">
                    <a href="<?= $this->url->get('dashboard') ?>"><span class="fa fa-desktop"></span> <span class="xn-text">Menú principal</span></a>
                </li>
                <li class="xn-openable <?php echo $this->router->getControllerName()=='client' &&  $this->router->getActionName()=='admision'?'active':''?>">
                    <a href="#"><span class="fa fa-cart-plus"></span> <span class="xn-text">Matricula</span></a>
                    <ul>
                        <li class="<?=$this->router->getControllerName()=='client' && $this->router->getActionName()=='inscripcion'?'active':''?>">
                            <a href="<?= $this->url->get('dashboard/client/all') ?>"><span class="fa fa-th"></span> Todos</a>
                        </li>
                        <li class="<?=$this->router->getControllerName()=='client' && $this->router->getActionName()=='inscripcion'?'active':''?>">
                            <a href="<?= $this->url->get('dashboard/client/inscripcion') ?>"><span class="fa fa-th"></span> Inscripciones Iniciadas</a>
                        </li>
                        <li class="<?=$this->router->getControllerName()=='client' && $this->router->getActionName()=='admision'?'active':''?>">
                            <a href="<?= $this->url->get('dashboard/client/admision') ?>"><span class="fa fa-th"></span> Inscritos</a>
                        </li>
                        <li class="<?=$this->router->getControllerName()=='client' && $this->router->getActionName()=='recibo'?'active':''?>">
                            <a href="<?= $this->url->get('dashboard/client/recibo') ?>"><span class="fa fa-th"></span> Pagos</a>
                        </li>
                    </ul>
                </li>
                <?php } ?>
                <?php if ($auth['rol'] == 'SUPERADMIN' || $auth['rol'] == 'ADMIN') { ?>
                <li class="xn-openable <?= ($this->router->getControllerName()=='sales'||$this->router->getControllerName()=='maestro'||($this->router->getControllerName()=='products' && $this->router->getActionName()=='new')||($this->router->getControllerName()=='brands' && ($this->router->getActionName()=='index'||$this->router->getActionName()=='postgrado')))?'active':''?>">
                    <a href="#"><span class="fa fa-cart-plus"></span> <span class="xn-text">Programas de Estudio</span></a>
                    <ul>
                        <li class="<?=$this->router->getControllerName()=='products' && $this->router->getActionName()=='new'?'active':''?>"><a href="<?=$this->url->get('dashboard/products/new')?>" class=""><span class="fa fa-laptop"></span>Agregar Matería</a></li>
                        <li class="<?=$this->router->getControllerName()=='sales' && $this->router->getActionName()=='number'?'active':''?>"><a href="<?=$this->url->get('dashboard/sales/active')?>" class=""><span class="fa fa-laptop"></span>Agregar Grupo</a></li>
                        <li class="<?=$this->router->getControllerName()=='brands' && ($this->router->getActionName()=='index'||$this->router->getActionName()=='postgrado')?'active':''?>"><a href="<?=$this->url->get('dashboard/brands/index/1')?>" class=""><span class="fa fa-laptop"></span>Maestrias</a></li>
                        <li class="<?=$this->router->getControllerName()=='brands' &&( $this->router->getActionName()=='index'||$this->router->getActionName()=='postgrado')?'active':''?>"><a href="<?=$this->url->get('dashboard/brands/index/2')?>" class=""><span class="fa fa-laptop"></span>Doctorados</a></li>
                        <li class="<?=$this->router->getControllerName()=='brands' &&( $this->router->getActionName()=='grupocuatrimestre'||$this->router->getActionName()=='grupocarrera'||$this->router->getActionName()=='grupo')?'active':''?>"><a href="<?=$this->url->get('dashboard/brands/index/2')?>" class=""><span class="fa fa-laptop"></span>Grupos Activos</a></li>
                    </ul>
                </li>
                <li class="<?php echo $this->router->getControllerName()=='client' &&  $this->router->getActionName()=='admision'?'active':''?>">
                    <a href="<?= $this->url->get('dashboard/client/admision') ?>"><span class="fa fa-th"></span> Servicios Escolares</a>
                </li>
                <?php } ?>
                <?php if ($auth['rol'] == 'STUDENT') { ?>
                        <li class="<?php echo $this->router->getControllerName()=='user' && $this->router->getActionName()=='profile'?'active':''?>"><a href="<?= $this->url->get('dashboard/user/profile') ?>"><span class="fa fa-user"></span>Mi perfil</a></li>
                        <?php if($estudiante->getStatus()=='PAGAR'):?>
                        <li><a href="https://www.sandbox.paypal.com/cgi-bin/webscr'.?business=edgar.alexander.sm-facilitator%40gmail.com&item_name=Publicacion&amount=3000&no_note=1&cmd=_xclick&lc=MX&currency_code=MXN&bn=PP-BuyNowBF%3Abtn_buynow_LG.gif%3ANonHostedGuest&item_number=250&payer_email=gustavo.alberto%40c-developers.com&charset=utf-8&custom=250&stid=146&cuotid=3&name_cuota=ESTUDIANTE&costo_cuota=3000&costo_inscription=1000&pay=PAYPAL&return=http%3A%2F%2Flocal.controlescolar.com%2Fpaypal%2Fpagadocuota%3Fentrada%3D300%26id%3D250&cancel_return=http%3A%2F%2Flocal.amgp.com%2Fpaypal%2Fcanceladocuota%3Fid%3D250&notify_url=http%3A%2F%2Flocal.controlescolar.com%2Fpaypal"><span class="fa fa-money"></span> Pagar</a></li>
                        <?php else: ?>
                        <li class="<?php echo $this->router->getControllerName()=='student' && $this->router->getActionName()=='aula'?'active':''?>"><a href="<?= $this->url->get('dashboard/student/aula') ?>"><span class="fa fa-building"></span>Mi Aula Virtual</a></li>
                        <li class="<?php echo $this->router->getControllerName()=='student' && $this->router->getActionName()=='calificaciones'?'active':''?>"><a href="<?= $this->url->get('dashboard/student/calificaciones') ?>"><span class="fa fa-book"></span> Mi progreso académico</a></li>
                        <li class="<?php echo $this->router->getControllerName()=='student' && $this->router->getActionName()=='calificaciones'?'active':''?>"><a href="<?= $this->url->get('dashboard/student/pagos') ?>"><span class="fa fa-book"></span> Mis Pagos</a></li>
                        <?php endif;?>
                <?php } ?>
                <?php if ($auth['rol'] == 'TEACHER') { ?>
                        <li class="<?php echo $this->router->getControllerName()=='user' && $this->router->getActionName()=='profile'?'active':''?>"><a href="<?= $this->url->get('dashboard/user/profile') ?>"><span class="fa fa-user"></span> Mi Perfil</a></li>
                        <li class="xn-openable <?= ($this->router->getControllerName()=='sales'||$this->router->getControllerName()=='maestro')?'active':''?>">
                            <a href="#"><span class="fa fa-cart-plus"></span> <span class="xn-text">Programas de Estudio</span></a>
                            <ul>
                               <li class="<?=$this->router->getControllerName()=='sales' && $this->router->getActionName()=='number'?'active':''?>"><a href="<?=$this->url->get('dashboard/sales/active')?>" class=""><span class="fa fa-laptop"></span>Maestrias</a></li>
                                <li class="<?=$this->router->getControllerName()=='sales' && $this->router->getActionName()=='number'?'active':''?>"><a href="<?=$this->url->get('dashboard/sales/active')?>" class=""><span class="fa fa-laptop"></span>Doctorados</a></li>
                            </ul>
                        </li>
                        <li class="<?php echo $this->router->getControllerName()=='maestro' && $this->router->getActionName()=='materias'?'active':''?>"><a target="_blank" href="/dashboard/maestro/aula"><span class="fa fa-building"></span>Mi Aula Virtual</a></li>
                <?php } ?>
                <?php if ($auth['rol'] == 'SUPERADMIN') { ?>
                <li class="<?php echo $this->router->getControllerName()=='finanzas' && $this->router->getActionName()=='materias'?'active':''?>"><a target="_blank" href="https://aula.ideiberoamerica.com/"><span class="fa fa-building"></span>Finanzas</a></li>
                <li class="xn-openable <?= $this->router->getControllerName()=='user' || $this->router->getControllerName()=='reports' ?'active':''?>">
                    <a href="#"><span class="fa fa-cogs"></span> <span class="xn-text">Configuración General</span></a>
                    <ul>
                        <li class="xn-openable <?php echo $this->router->getControllerName()=='user'?'active':''?>">
                            <a href="#"><span class="fa fa-users"></span> <span class="xn-text">Usuarios</span></a>
                            <ul>
                                <li class="<?php echo $this->router->getControllerName()=='user' && $this->router->getActionName()=='newuser'?'active':''?>">
                                    <a href="<?= $this->url->get('dashboard/user/new-user') ?>"><span class="fa fa-user-plus"></span> Nuevo usuario</a>
                                </li>
                                <li class="<?php echo ($this->router->getActionName()=='index' || $this->router->getActionName()=='percentage') && $this->router->getControllerName()=='user'?'active':''?>">
                                    <a href="<?= $this->url->get('dashboard/users') ?>"><span class="fa fa-user"></span> Activos</a>
                                </li>
                                <li class="<?php echo $this->router->getActionName()=='inactive' && $this->router->getControllerName()=='user'?'active':''?>">
                                    <a href="<?= $this->url->get('dashboard/user/inactive') ?>"><span class="fa fa-user-secret"></span> Inactivos</a>
                                </li>
                            </ul>
                        </li>
                        <li class="xn-openable <?= $this->router->getControllerName()=='reports'?'active':''?>">
                            <a href="#"><span class="fa fa-file"></span> <span class="xn-text">Reportes</span></a>
                            <ul>
                                <li class="<?=$this->router->getControllerName()=='reports' && $this->router->getActionName()=='index'?'active':''?>"><a href="<?=$this->url->get('dashboard/reports')?>"><span class="fa fa-file-excel-o"></span>Historial de clases</a></li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <?php } ?>
                </div>
            </ul>
        </div-->
        <div class="page-content" style="margin-left: 0px;">
            <div class="col-md-12 sinpadding content-topmenu">
                    <div class="col-md-2 col-xs-3 sinpadding">
                        <div class="profile">
                            <div class="profile-image">
                                <a href="<?= $this->url->get('dashboard/user/profile') ?>" class="profile-control-left" title="Editar perfil">
                                    <img src="/dash/assets/images/users/thumbnail/<?= $auth['photo'] ?>" alt="Gustavo"/>
                                </a>
                            </div>
                            <div class="profile-data">
                                <div class="profile-data-name"  id="name-user-layout" style="text-transform: capitalize;"><?= $auth['username'] ?></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8 col-xs-7 sinpadding text-center">
                        <a href="<?= $this->url->get('dashboard') ?>" ><img class="logo-main-iide" src="/dash/img/logo50.png"></a>
                        <h3 class="text-center title-head-iide"><?= $meta['title']?></h3>
                    </div>
                    <div class="col-md-2 col-xs-2 text-center sinpadding">
                        <a href="#" class="mb-control" data-box="#mb-signout" style="height: 100%;"><span class="fa fa-power-off close-head-iide"></span></a>
                    </div>
            </div>
            <div class="breadcrumb">
                <div class="col-md-2 col-sm-2 col-lg-2 col-xs-4">
                    <?php if($back['title']!=""):?>
                        <?php if(($this->router->getActionName()=='index'&&$this->router->getControllerName()=='index')):?>
                        <p class="back-title-bar"><a href="<?php echo $back['url']?>"><span class="fas fa-home"><?php echo $back['title']?></a></p>
                        <?php else:?>
                        <p class="back-title-bar"><a href="<?php echo $back['url']?>"><span class="fas fa-arrow-left"></span> &nbsp;&nbsp;<?php echo $back['title']?></a></p>
                        <?php endif;?>
                    <?php endif;?>
                   <!-- <?php if(($this->router->getActionName()=='plan'&&$this->router->getControllerName()=='clases')||($this->router->getControllerName()=='student'&&($this->router->getActionName()=='calendario'||$this->router->getActionName()=='allstudentclass'||$this->router->getActionName()=='calificaciones'||$this->router->getActionName()=='asistencias'||$this->router->getActionName()=='trabajos'||$this->router->getActionName()=='chat'||$this->router->getActionName()=='notas'))):?>
                    <p class="back-title-bar"><a href="<?= $this->url->get('dashboard/student/clase') ?>"><span class="fas fa-arrow-left"></span> &nbsp;&nbsp;Aula virtual</a></p>
                    <?php elseif($this->router->getActionName()=='editadmision'&&$this->router->getControllerName()=='client'):?>
                    <p class="back-title-bar"><a href="<?= $this->url->get('dashboard/client/all') ?>"><span class="fas fa-arrow-left"></span> &nbsp;&nbsp;Proceso de admisión</a></p>

                    <?php elseif($this->router->getActionName()=='index'&&$this->router->getControllerName()=='index'):?>
                    <p class="back-title-bar"></p>
                    <?php elseif($this->router->getActionName()=='list'&&$this->router->getControllerName()=='clases'):?>
                    <p class="back-title-bar"><a href="<?= $this->url->get('dashboard/clases') ?>"><span class="fas fa-arrow-left"></span> &nbsp;&nbsp;Clases</a></p>
                    <?php elseif($this->router->getActionName()=='index'&&$this->router->getControllerName()=='clases'):?>
                    <p class="back-title-bar"><a href="<?= $this->url->get('dashboard/clases') ?>"><span class="fas fa-arrow-left"></span> &nbsp;&nbsp;Regresar</a></p>
                    <?php else:?>
                    <p class="back-title-bar"><a href="<?= $this->url->get('dashboard') ?>"><span class="fas fa-arrow-left"></span> &nbsp;&nbsp;Campus virtual</a></p>
                    <?php endif;?>-->
                </div>
                <div class="col-md-8 col-sm-8 col-lg-8 col-xs-8">
                    <?php if($auth["rol"]=="STUDENT"):?>
                        <li class="active title-header"><?php if($this->router->getControllerName()=='index'||$this->router->getControllerName()=='recibos'||($this->router->getControllerName()=='user'&&$this->router->getActionName()=='profile')||($this->router->getControllerName()=='student'&&($this->router->getActionName()=='aula'||$this->router->getActionName()=='pagos'||$this->router->getActionName()=='sitios'||$this->router->getActionName()=='calificaciones'||$this->router->getActionName()=='servicios'))){ 
                        if($carrera->getName()=="Maestria en Derecho Constitucional y Derecho"){
                            echo "Maestría en Derecho Constitucional y Derechos Humanos";
                        }
                        elseif($carrera->getName()=="Doctorado en Derecho Constitucional y Derecho"){
                            echo "Doctorado en Derecho Constitucional y Derechos Humanos";
                        }
                        else{
                            echo $carrera->getName();
                        }
                    }else{ echo $clase->getName();}?></li>
                    <?php else:?>
                    <li class="active title-header"><?php if($this->router->getControllerName()=='index'||$this->router->getControllerName()=='recibos'||($this->router->getControllerName()=='student'&&($this->router->getActionName()=='aula'||$this->router->getActionName()=='pagos'||$this->router->getActionName()=='sitios'||$this->router->getActionName()=='calificaciones'||$this->router->getActionName()=='servicios'||$this->router->getActionName()=='asistencias'))){ 
                        echo "Instituto Iberoamericano de Derecho Electoral";
                    }else{ echo "Instituto Iberoamericano de Derecho Electoral";}?></li>
                    <?php endif;?>
                </div>
                <div class="col-md-2 col-sm-2 col-lg-2">
                </div>
            </div>
            <?= $this->getContent() ?>
        </div>
    </div>
    <div class="message-box animated fadeIn" data-sound="alert" id="mb-signout">
        <div class="mb-container">
            <div class="mb-middle">
                <div class="mb-title"><span class="fa fa-sign-out"></span> Cerrar <strong>Sesión</strong> ?</div>
                <div class="mb-content">
                    <p>¿Estas seguro de cerrar esta sesión?</p>
                    <p>Pulse No si desea continuar con el trabajo. Pulse Sí para cerrar la sesión del usuario actual.</p>
                </div>
                <div class="mb-footer">
                    <div class="pull-right">
                        <a href="<?= $this->url->get('logout') ?>" class="btn btn-success btn-lg">Si</a>
                        <button class="btn btn-default btn-lg mb-control-close">No</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
<div class="message-box message-box-info animated fadeIn" id="message-box-info">
    <div class="mb-container">
        <div class="mb-middle">
            <div class="mb-title"> Actualizando &nbsp; <i class="fa fa-circle-o-notch fa-spin style-fa"></i></div>
            <div class="mb-content">
                <p>Guardando y Actualizando su información, espere un momento por favor.</p>
            </div>
        </div>
    </div>
</div>
<div class="message-box message-box-success animated fadeIn" id="message-box-success">
    <div class="mb-container">
        <div class="mb-middle">
            <div class="mb-title"><span class="fa fa-check"></span> Información actualizada</div>
            <div class="mb-content">
                <p>Sus cambios han sido guardados correctamente, actualizaremos el sitio para reflejar los cambios.</p>
            </div>
        </div>
    </div>
</div>
<div class="message-box message-box-warning animated fadeIn" id="message-box-warning">
    <div class="mb-container">
        <div class="mb-middle">
            <div class="mb-title"><span class="fa fa-warning"></span> Error</div>
            <div class="mb-content">
                <p>Ha ocurrido un error al guardar su información, inténtelo nuevamente o regrese mas tarde.</p>
            </div>
            <div class="mb-footer">
                <button class="btn btn-default btn-lg pull-right mb-control-close">Cerrar</button>
            </div>
        </div>
    </div>
</div>
<div class="message-box message-box-danger animated fadeIn" id="message-box-danger">
    <div class="mb-container">
        <div class="mb-middle">
            <div class="mb-title"><span class="fa fa-times"></span> Cuidado</div>
            <div class="mb-content">
                <p>Usted no puede actualizar su imagen.</p>
            </div>
            <div class="mb-footer">
                <button class="btn btn-default btn-lg pull-right mb-control-close">Cerrar</button>
            </div>
        </div>
    </div>
</div>
    
        <?= $this->assets->outputJs('JsIndex') ?>
    
    <?php $jsValidation = $this->assets->collection('jsValidation'); ?>
    <?php if (!empty($jsValidation)) { ?>
        <?php echo $this->assets->outputJs('jsValidation'); ?>
    <?php } ?>
    <?php $dataTable = $this->assets->collection('jsDataTable');
        if (!empty($dataTable)) {echo $this->assets->outputJs('jsDataTable');}
    ?>
    <?php $plugins = $this->assets->collection('jsPlugins');
        if (!empty($plugins)) {echo $this->assets->outputJs('jsPlugins');}
    ?>
    <script type="text/javascript" src='https://cdn.jsdelivr.net/npm/sweetalert2@7.29.2/dist/sweetalert2.all.min.js'></script>
    <?php $jsPhotoNotes = $this->assets->collection('jsPhotoNotes'); ?>
    <?php if (!empty($jsPhotoNotes)) { ?>
        <?php echo $this->assets->outputJs('jsPhotoNotes'); ?>
    <?php } ?>
    <?php $jsMasonry = $this->assets->collection('jsMasonry'); ?>
    <?php if (!empty($jsMasonry)) { ?>
        <?php echo $this->assets->outputJs('jsMasonry'); ?>
    <?php } ?>
    <?php $jsGoogle = $this->assets->collection('jsGoogle'); ?>
    <?php if (!empty($jsGoogle) && $this->router->getControllerName()=='process' && ($this->router->getActionName()=='activecourse' || $this->router->getActionName()=='edit')) { ?>
        <script type="text/javascript" src='maps.google.com/maps/api/js?sensor=false&libraries=places'></script>
        <?php echo $this->assets->outputJs('jsGoogle'); ?>
    <?php } ?>
    <?php $jsMorris = $this->assets->collection('jsMorris'); ?>
    <?php if (!empty($jsMorris) && $this->router->getControllerName()=='process' && ($this->router->getActionName()=='statistics')) { ?>
        <?php echo $this->assets->outputJs('jsMorris'); ?>
    <?php } ?>
<?php $jsScroll = $this->assets->collection('jsScroll'); ?>
<?php if (!empty($jsScroll) && $this->router->getControllerName()=='client' && ($this->router->getActionName()=='control')) { ?>
    <?php echo $this->assets->outputJs('jsScroll'); ?>
<?php } ?>
    <script>
    function openCity(evt, cityName) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
            document.getElementById(cityName).style.display = "block";
            evt.currentTarget.className += " active";
        }
    // Get the element with id="defaultOpen" and click on it
    /*document.getElementById("defaultOpen").click();*/
    </script>
    <!--Start of Tawk.to Script-->
    <script type="text/javascript">
        var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
        (function(){
            var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
            s1.async=true;
            s1.src='https://embed.tawk.to/5bc45bc308387933e5bb563a/default';
            s1.charset='UTF-8';      
            s1.setAttribute('crossorigin','*');
            s0.parentNode.insertBefore(s1,s0);
        })();
    </script>
    <!--End of Tawk.to Script-->
</body>
</html>
