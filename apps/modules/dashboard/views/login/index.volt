<style>
    input:not([type]), input[type=text], input[type=password], input[type=email], input[type=url], input[type=time], input[type=date], input[type=datetime], input[type=datetime-local], input[type=tel], input[type=number], input[type=search], textarea.materialize-textarea{
        border: none;
        border-bottom: 1px solid #9e9e9e;
        border-radius: 0;
        outline: none;
        height: 3rem;
        width: 100%;
        font-size: 1.5rem;
        margin: 0 0 7px 0;
        padding: 0;
        box-shadow: none;
        transition: all 0.3s;
        background-color: transparent;
    }
    input:-webkit-autofill{
        background-color: rgba(72, 58, 105, 0.4) !important;
    }
    .has-feedback .form-control{
        padding-right: 0!important;
    }
    .input-group{
        display: block!important;
    }


#mostrar_contrasena{
    position: relative;
    left: 5%;
    opacity: 0;
    z-index: 999999;
    width: 19px;
    height: 19px;
}
</style>
<span class="hide" id="key-security" data-key="<?=$key?>"></span>
<span class="hide" id="value-security" data-value="<?php echo $this->security->getTokenKey(); ?>"></span>
<div class="login-container login-v2 blog-login">
            
    <form id="session" action="/" class="form-horizontal" method="post">
        <img class="img-logo" src="/dash/img/LogoEnBlanco.png" alt="" />
        <div class="login-box blog-login-in">
            <div class="login-logo"></div>
            <div class="login-body">
                <div class="login-title text-center"><strong>Iniciar Sesión</strong> <br>Ingresar a su cuenta</div>
                <div class="form-group">
                    <div class="col-md-12" style="margin-top: 2em">
                        <div class="input-group">
                            <div>
                                <span class="fa fa-user"></span>
                            </div>
                            <input type="email" class="form-control" placeholder="E-mail" name="email" required/>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-12">
                        <div class="input-group">
                            <div>
                                <span class="fa fa-lock"></span>
                            </div>
                            <input type="password" id="contrasena" class="form-control" placeholder="Contraseña" name="password" required/>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="form-row">
                        <div class="col-md-12">
                            <div class="input-group">
                                <div class="form-check">
                                    <input type="checkbox" id="mostrar_contrasena" class="form-check-input"/>
                                    <label for="remember-me">Ver contraseña</label>
                                  </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-12" style="margin-top: 2em">
                        <button class="btn btn-primary btn-lg btn-block">Iniciar Sesión</button>
                    </div>
                    <i id="session-loading" class="fa fa-spinner fa-spin fade hide" style="font-size: 22px;"></i>
                </div>
                <div class="form-group fade hide" id="container-messages">
                    <div class="alert alert-warning fade hide" role="alert" id="email-incorrect">
                        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Cerrar</span></button>
                        <strong>Email incorrecto.</strong>
                    </div>
                    <div class="alert alert-warning fade hide" role="alert" id="password-incorrect">
                        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                        <strong>Contraseña incorrecta.</strong>
                    </div>
                    <div class="alert alert-warning fade hide" role="alert" id="all-incorrect">
                        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                        <strong>Email o contraseña incorrecto</strong>
                    </div>
                </div>
            
            </div>
        </form>
    </div>

</div>
<div class="modal fade" id="background" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="content-fa">
            <h5 id="new_account" class="account">Creando su cuenta, por favor espere....</h5>
            <h5 id="success_account" class="hide account">Se ha registrado correctamente,se le redireccionara a su ue.</h5>
            <h5 id="fail_account" class="hide account">Ha ocurrio un error, intente nuevamente o mas tarde.</h5>
            <h1><i class="fa fa-circle-o-notch fa-spin style-fa"></i></h1>
        </div>
    </div>
</div>











<!-- <span class="hide" id="key-security" data-key="<?=$key?>"></span>
<span class="hide" id="value-security" data-value="<?php echo $this->security->getTokenKey(); ?>"></span>

<div class="blog-login">
    <div class="blog-login-in">
        <form id="session" action="/" class="form-horizontal" method="post">
            <img src="images/logo.png" alt="" />
            <div class="row">
                <div class="input-field col s12">
                    <input id="first_name1" type="text" class="validate">
                    <label for="first_name1">User Name</label>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s12">
                    <input id="last_name" type="password" class="validate">
                    <label for="last_name">Password</label>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s12">
                    <a class="waves-effect waves-light btn-large btn-log-in" href="index.html">Login</a>
                </div>
            </div>
            <a href="forgot.html" class="for-pass">Forgot Password?</a>
        </form>
    </div>
</div>

<div class="modal fade" id="background" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="content-fa">
            <h5 id="new_account" class="account">Creando su cuenta, por favor espere....</h5>
            <h5 id="success_account" class="hide account">Se ha registrado correctamente,se le redireccionara a su ue.</h5>
            <h5 id="fail_account" class="hide account">Ha ocurrio un error, intente nuevamente o mas tarde.</h5>
            <h1><i class="fa fa-circle-o-notch fa-spin style-fa"></i></h1>
        </div>
    </div>
</div> -->