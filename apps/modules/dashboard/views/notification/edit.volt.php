<div class="page-content-wrap" style="background-color: #25396880;">
    <div class="container sinpadding">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12 cuadro" style="background: rgba(85, 86, 97, 0.4);padding-top: 5%;">
                <h4 class="text-title text-center c-white">Notificación para: <?php echo $user->getName()." ".$user->getLastName()." ".$user->getSecondName();?></h4>
                <div class="col-md-12 col-xs-12 panel-body form-group-separated">
                    <form action="#" method="post" id="newNotification" name="newNotification" role="form" class="form-horizontal">
                        
                        <div class="form-group">
                            <label class="col-md-2 col-xs-12 control-label c-white">Notificación <span>*</span></label>
                            <div class="col-md-8 col-xs-12">
                                <textarea type="text" class="form-control" name="notification" id="notification" required placeholder="Notificación"><?php echo $noti->getNotification();?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 col-xs-12 control-label c-white">Estatus<span>*</span></label>
                            <div class="col-md-8 col-sm-6 col-xs-12">
                                <select name="status" id="status" class="form-control" required>
                                    <option <?php echo $noti->getNotification()=='ACTIVE'?'selected':'';?> value="ACTIVE">ACTIVO</option>
                                    <option <?php echo $noti->getNotification()=='INACTIVE'?'selected':'';?> value="INACTIVE">INACTIVO</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 col-xs-12 control-label c-white">Programar notificación</label>
                            <div class="col-md-3 col-xs-12">
                                <input type="text" class="form-control birth_date" name="birth_date" id="birth_date" value="<?php echo $noti->getDate();?>">
                            </div>
                        </div>
                         
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-2">
                                <input type="submit" class="btn btn-success" value="Guardar"/>
                            </div>
                        </div>
                        <div class="clearfiz"></div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="message-box animated fadeIn" data-sound="alert" id="other_products">
    <div class="mb-container">
        <div class="mb-middle">
            <div class="mb-title"><span class="fa fa-sign-out"></span> Alta <strong>Materia</strong></div>
            <div class="mb-content">
                <p>¿Le gustaría dar de alta otra materia?</p>
                <p>Pulse Sí para dar de alta una nueva materia. Pulse No para ir al inventario.</p>
            </div>
            <div class="mb-footer">
                <div class="pull-right">
                    <a href="<?= $this->url->get('dashboard/products/new') ?>" class="btn btn-success btn-lg">Si</a>
                    <a href="<?= $this->url->get('dashboard/products') ?>" class="btn btn-default btn-lg">No</a>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal" id="modalElement" tabindex="-1" role="dialog" aria-labelledby="defModalHead" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Cerrar</span></button>
                <h4 class="modal-title" id="defModalHead"></h4>
            </div>
            <form id="formElement" action="#" method="post" class="form-horizontal">
                <input type="hidden"  id="brid" name="brid">
                <div class="modal-body">
                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-3">
                            <input type="text" class="form-control" placeholder="Nombre" name="nameM" id="nameM" required/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-3">
                            <input type="text" class="form-control" placeholder="Permalink" name="permalinkM" id="permalinkM" required readonly/>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Cerrar</button>
                    <input type="submit" class="btn btn-info btn-submit-element" value="Guardar">
                    <i id="session-loading" class="fa fa-spinner fa-spin fade hide" style="font-size: 22px;"></i>
                </div>
            </form>
        </div>
    </div>
</div>