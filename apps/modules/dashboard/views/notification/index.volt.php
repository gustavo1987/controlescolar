<div class="page-content-wrap">
    <div class=" container sinpadding">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12 fondo-gris-general">
                <div class="panel panel-success">
                    <div class="panel-heading text-right">
                        <h3 class="panel-title">Notificaciones</h3>
                        <a href="<?= $this->url->get('dashboard/notification/new') ?>" class="btn btn-success btn-lg">Crear nueva notificación</a>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover table-actions generalDT" data-order="5" data-filter="desc">
                                <thead>
                                <tr>
                                    <!--th>Número&nbsp;Clase</th-->
                                    <th>Notificación</th>
                                    <th>Estatus</th>
                                    <th>Fecha de Creación</th>
                                    <th>Fecha Publicación</th>
                                    <th>Estatus de visualización</th>
                                    <th>Opciones</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php if($statusnoti=="ACTIVO"):?>
                                    <?php foreach ($notification as $values) { ?>
                                    <?php $dateC= $values->getDateCreation(); $newDate = date("d-m-Y", strtotime($dateC));?>
                                    <?php $dateC2= $values->getDate(); $newDate2 = date("d-m-Y", strtotime($dateC2));?>
                                    <tr id="<?= $values->getNotid() ?>">
                                        <td><?= $values->getNotification() ?></td>
                                        <td><?= $values->getStatus() ?></td>
                                        <td><?= $newDate ?></td>
                                        <td><?= $newDate2 ?></td>
                                        <td><?= $values->getStatusView() ?></td>
                                        <td>
                                            <a title="Editar Notificación" href="<?= $this->url->get('dashboard/notification/edit/') ?><?= $values->getNotid() ?>" class=""><span class="fa fa-edit fa-2x"></span></a>&nbsp;<a title="Eliminar Notificación" data-notid="<?= $values->getNotid() ?>" class="delete-notification"><span class="fas fa-comment-slash fa-2x" style="color: red;"></span></a>
                                        </td>
                                    </tr>
                                    <?php } ?>
                                <?php else:?>
                                    <h4>Se el primero en notificar algo.</h4>
                                <?php endif;?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="message-box animated fadeIn" data-sound="alert" id="mb-remove-row">
    <div class="mb-container">
        <div class="mb-middle">
            <div class="mb-title"><span class="fa fa-times"></span> Eliminar <strong>Datos</strong> ?</div>
            <div class="mb-content">
                <p>¿Estas seguro de eliminar esta fila?</p>
                <p>Presione "Si" si esta seguro.</p>
            </div>
            <div class="mb-footer">
                <div class="pull-right">
                    <button class="btn btn-success btn-lg mb-control-yes">Si</button>
                    <button class="btn btn-default btn-lg mb-control-close">No</button>
                </div>
            </div>
        </div>
    </div>
</div>