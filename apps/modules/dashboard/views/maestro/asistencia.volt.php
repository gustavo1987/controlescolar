<style type="text/css">
    .toggle-header{
        padding:10px 0;
        margin:10px 0;
        background-color:black;
        color:white;
    }
    .txt-green{
        color:green;
    }
    .txt-red{
        color:red;
    }
    .cuadro-ide {
        border: #4d326a solid 2px;
        border-radius: 11px;
        padding: 5px 20px;
        text-align: center;
        font-size: 20px;
    }
    .title-class {
        padding: 2% 0;
        text-align: center;
        color: #483a69;
    }
</style>
<div class="page-title">
    <div class="col-sm-6 text-left">
        <h2><a href="/dashboard/student/clase"><span class="fas fa-arrow-circle-left"></span>Aula Virtual</a> </h2>
    </div>
</div>
<div class="page-content-wrap">
    <div class="container">
        <div class="row">
            <div class="panel panel-default clearfix">
                <div class="panel-heading visible-xs">
                    <h2 class="panel-title">Nomenclatura</h2>
                    <br>
                    <br>
                    <p class="small">
                        V = Asistencia Virtual en Vivo,  D = Asistencia Virtual Diferida, I = Inasistenica                            
                    </p>
                </div>
                <div class="col-xs-12 toggle-header">
                    <div class="col-xs-4 text-center">
                        <span class="hidden-xs">Alumno </span>
                        <span class="visible-xs">Alumno </span>
                    </div>
                    <?php foreach ($conferencias as $key => $value):?>
                        <div class="col-xs-2 text-center">
                             <span class="hidden-xs"><?php $value->getName();?></span>
                            <span class="visible-xs"><?php $value->getName();?></span>
                        </div>
                    <?php endforeach;?>
                    <!--div class="col-xs-2">
                        <span class="hidden-xs">Archivo del Alumno </span>
                        <span class="visible-xs">Archivo</span>
                    </div>
                    <div class="col-xs-2 text-center">
                        <span class="hidden-xs">Archivo con observaciones </span>
                        <span class="visible-xs">Observación</span>
                    </div>
                    <div class="col-xs-1 text-center">
                         <span class="hidden-xs">Cal. trabajo</span>
                        <span class="visible-xs">Calificación</span>
                    </div-->
                </div>
                <div id="feature-1" class="collapse in">
                    <?php if($statusstudent==1):?>
                    <?php foreach ($student as $key => $value):?>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-xs-4 text-center">
                                Trabajo 1
                            </div>
                            <div class="col-xs-2 text-center">
                                21/11/2018
                            </div>
                            <div class="col-xs-2 text-center">
                                <a href="/" target="_black"><i class="fas fa-cloud-upload-alt txt-green">Cargar</i></a>
                            </div>
                            <div class="col-xs-2 text-center">
                                <a href="/" target="_black"><i class="fas fa-cloud-upload-alt txt-green">Cargar</i></a>
                            </div>
                            <div class="col-xs-1">
                                -                                      
                            </div>
                        </div>
                    </div>  
                <?php endforeach;?>
                    <?php else:?>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-xs-12">
                                <p>No hay trabajos</p>
                            </div>
                        </div>
                    </div>
                    <?php endif;?>
                    <!--div class="panel-body">
                        <div class="row">
                            <div class="col-xs-1">
                                2                                            
                            </div>
                            <div class="col-xs-4 text-center">
                                Trabajo 2
                            </div>
                            <div class="col-xs-2 text-center">
                                28/11/2018
                            </div>
                            <div class="col-xs-2 text-center">
                                <a href="/" target="_black"><i class="fas fa-file-pdf txt-green">Nombre</i></a>
                            </div>
                            <div class="col-xs-2 text-center">
                                <a href="/" target="_black"><i class="fas fa-file-pdf txt-green">Nombre</i></a>
                            </div>
                            <div class="col-xs-1">
                                10                                       
                            </div>
                        </div>
                    </div-->  
                </div>
            </div>
            
        </div>
    </div>
</div>