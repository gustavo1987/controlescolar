<!--ul class="breadcrumb">
    <li><a href="{{url('dashboard')}}">Inicio</a></li>
    <li class="active">Estudiantes</li>
</ul-->
<?php $url = $this->url->get('dashboard/products/printp?status=ACTIVE')?>
<div class="page-title text-right">
    <button class="btn btn-primary" onclick="printPage('<?=$url?>')"><i class="fa fa-print"></i>Imprimir</button>
    <a class="btn btn-info" href="<?=$this->url->get('dashboard/products/export?status=ACTIVE');?>"><i class="fa fa-file-excel-o"></i>Exportar</a>
</div>
<div class="page-content-wrap">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-success">
                <div class="panel-heading">
                    <h3 class="panel-title">Lista de alumnos de </h3>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table id="tableProducts" class="table table-bordered table-striped table-actions generalDT" data-order="0" data-filter="desc">
                            <thead>
                            <tr>
                                <th>Matricula</th>
                                <th>Nombre</th>
                                <th>Apellidos</th>
                                <th>Calificación</th>
                                <th>Opciones</th>
                            </tr>
                            </thead>
                            <tbody>
                            {% for values in students %}
                            <tr class="{{values.getClid()}}">
                                <td>{{values.getIdentifier()}}</td>
                                <td>{{values.getName()}}</td>
                                <td class="name"></td>
                                <td>8</td>
                                <td class="options" data-id="{{values.getClid()}}">
                                    <a href="{{url('dashboard/maestro/calificar/'~values.getClid())}}" class="editElement" data-toggle="tooltip" data-placement="top" title="" data-original-title="Agregar Calificación"><i class="fa fa-pencil-square-o fa-2x"></i></a>&nbsp;
                                </td>
                            </tr>
                            {% endfor %}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="message-box animated fadeIn" data-sound="alert" id="mb-remove-row">
    <div class="mb-container">
        <div class="mb-middle">
            <div class="mb-title"><span class="fa fa-times"></span> Eliminar <strong>Datos</strong> ?</div>
            <div class="mb-content">
                <p>¿Estas seguro de eliminar esta fila?</p>
                <p>Presione "Si" si esta seguro.</p>
            </div>
            <div class="mb-footer">
                <div class="pull-right">
                    <button class="btn btn-success btn-lg mb-control-yes">Si</button>
                    <button class="btn btn-default btn-lg mb-control-close">No</button>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal  bs-example-modal-lg" id="modalInventory" tabindex="-1" role="dialog" aria-labelledby="defModalHead" aria-hidden="true" style="display: none;" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Cerrar</span></button>
                <h4 class="modal-title" id="defModalHead"></h4>
            </div>
            <div class="modal-body">
                <div class="col-md-12 col-xs-12 panel-body form-group-separated">
                    <table class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Stock Máximo</th>
                            <th>Stock Mínimo</th>
                            <th>Stock</th>
                            <th>Añaidr</th>
                            <th>Restar</th>
                        </tr>
                        </thead>
                        <tbody>
                            <tr id="trInventory" data-id="" data-pid="">
                                <td><input type="number" min="2" max="1000" name="stock_max" id="stock_max" readonly class="form-control"></td>
                                <td><input type="number" min="1" max="999" name="stock_min" id="stock_min" readonly class="form-control"></td>
                                <td><input type="text" name="stock" id="stock" readonly class="form-control"></td>
                                <td><input type="number" name="add" id="add" readonly class="form-control" value="0"></td>
                                <td><input type="number" name="delete" id="delete" readonly class="form-control" value="0"></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function closePrint () {
        document.body.removeChild(this.__container__);
    }
    function setPrint () {
        this.contentWindow.__container__ = this;
        this.contentWindow.onbeforeunload = closePrint;
        this.contentWindow.onafterprint = closePrint;
        this.contentWindow.focus(); // Required for IE
        this.contentWindow.print();
    }
    function printPage (sURL) {
        var oHiddFrame = document.createElement("iframe");
        oHiddFrame.onload = setPrint;
        oHiddFrame.style.visibility = "hidden";
        oHiddFrame.style.position = "fixed";
        oHiddFrame.style.right = "0";
        oHiddFrame.style.bottom = "0";
        oHiddFrame.src = sURL;
        document.body.appendChild(oHiddFrame);
    }
</script>