<div class="page-content-wrap" id="sales" style="background-color: #25396880;">
    <div class=" container sinpadding">
        <div class="row">
            <div class="col-md-12 cuadro"  style="background: rgba(85, 86, 97, 0.4);padding-top: 5%;">
                <div class="panel panel-success">
                    <div class="panel-heading">
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                           <form action="#" method="post" id="addStudent" name="addStudent" role="form">
                                <input class="hidden" value="<?=$clase?>" name="salid">
                                <table class="table table-bordered table-hover table-actions generalDT" data-order="5" data-filter="desc">
                                    <thead>
                                    <tr>
                                        <th>Número</th>
                                        <th>Clase</th>
                                        <th>Posgrado</th>
                                        <th>Fecha</th>
                                        <th>Estatus</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($classall as $key => $values):?>
                                    <tr id="{{values.getUid()}}">
                                        <td class="student-center">{{key+1}}</td>
                                        <td class="student-center">{{values.getName()}}</td>
                                        <td class="student-center">{{values.getCarrera()}}</td>
                                        <td class="student-center">{{values.getDateCreation()}}</td>
                                        <td class="student-center">{{values.getStatus()}}</td>
                                    </tr>
                                    <?php endforeach;?>
                                    </tbody>
                                </table>
                           </form>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>