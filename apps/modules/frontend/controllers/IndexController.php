<?php
namespace Modules\Frontend\Controllers;
use Phalcon\Http\Request;

class IndexController extends ControllerBase
{
    public function indexAction(){
        $this->response->redirect("login");
        $title = "CAMPUS VIRTUAL";
        $url ="";
        $description = "Instituto Iberoamericano del Derecho Electoral.";
        $this->metaHome($title,$url,$description);
    }
    public function contactAction(){
        $title = "CONTÁCTANOS CAMPUS VIRTUAL";
        $url ="contactanos";
        $description = "Instituto Iberoamericano del Derecho Electoral.";
        $this->metaHome($title,$url,$description);
    }
    public function contactCompanyAction(){
        $title = "REGISTRO IN COMPANY CAMPUS VIRTUAL";
        $url ="register-in-company";
        $description = "Instituto Iberoamericano del Derecho Electoral.";
        $this->metaHome($title,$url,$description);
    }
    public function courseAction(){
    }
    public function emailAction(){
        $this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_ACTION_VIEW);
    }
}