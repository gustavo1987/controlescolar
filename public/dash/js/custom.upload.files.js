$(document).ready(function(){
    if($("#image-principal-dz-2").length>=1){
        Dropzone.autoDiscover = false;
        var minImageWidth2 =105, minImageHeight2 = 105;
        var myDropzone = new Dropzone('#image-principal-dz-2', {
            url: "/dashboard/course/uploadimage",
            uploadMultiple : false,
            paramName: "file", // The name that will be used to transfer the file
            maxFilesize: 5, // MB
            maxFiles: 1 ,
            parallelUploads : 1,
            addRemoveLinks : true,
            dictResponseError: "No se puede subir esta imagen!",
            autoProcessQueue: true,
            thumbnailWidth: 138,
            thumbnailHeight: 120,
            acceptedFiles:"image/*",
            previewTemplate: '<div class="dz-preview dz-file-preview"><div class="dz-details"><div class="dz-filename"><span data-dz-name style:"font-weight: bold;"></span></div><div class="dz-size">File size: <span data-dz-size></span></div><div class="dz-thumbnail-wrapper"><div class="dz-thumbnail"><img data-dz-thumbnail><span class="dz-nopreview">No preview</span><div class="dz-success-mark"><i class="fa fa-check-circle-o"></i></div><div class="dz-error-mark"><i class="fa fa-times-circle-o"></i></div><div class="dz-error-message"><span data-dz-errormessage></span></div></div></div></div><div class="progress progress-striped active"><div class="progress-bar progress-bar-success" data-dz-uploadprogress></div></div></div>',
            init: function() {
                this.on("success", function(file, responseText) {
                    file.previewTemplate.setAttribute('id',responseText[0].id);
                });
                this.on("thumbnail", function(file) {
                    if (file.width < minImageWidth2 || file.height < minImageHeight2 && file.width > minImageWidth2 || file.height > minImageHeight2) {
                        file.rejectDimensions()
                    }
                    else {
                        file.acceptDimensions();
                    }
                });
            },
            accept: function(file, done) {
                file.acceptDimensions = done;
                file.rejectDimensions = function() { done("La imagen no coincide con las dimensiones"); };
            },
            resize: function(file) {
                var info = { srcX: 0, srcY: 0, srcWidth: file.width, srcHeight: file.height },
                    srcRatio = file.width / file.height;
                if (file.height > this.options.thumbnailHeight || file.width > this.options.thumbnailWidth) {
                    info.trgHeight = this.options.thumbnailHeight;
                    info.trgWidth = info.trgHeight * srcRatio;
                    if (info.trgWidth > this.options.thumbnailWidth) {
                        info.trgWidth = this.options.thumbnailWidth;
                        info.trgHeight = info.trgWidth / srcRatio;
                    }
                } else {
                    info.trgHeight = file.height;
                    info.trgWidth = file.width;
                }
                return info;
            },
            sending : function(file, xhr, formData){
                formData.append("image-post",2);
                formData.append("name-image",$("#img-principal").val());
            },
            success: function(file, response){
                var name_image = jQuery.parseJSON(response);
                $(".dz-preview").addClass("dz-success");
                $("div.progress").remove();
                $("#image-2").val(name_image.name);
                $('#newCourse').formValidation('revalidateField', 'image-2');
            },
            removedfile: function(file) {
                $(file.previewElement).remove();
                $("#image-2").val('');
                $('#newCourse').formValidation('revalidateField', 'image-2');
            }
        });
    }
    if($("#image-principal-edit-2").length>=1){
        Dropzone.autoDiscover = false;
        var minImageWidth2 =105, minImageHeight2 = 105;
        var myDropzone = new Dropzone('#image-principal-edit-2', {
            url: "/dashboard/course/uploadimage",
            paramName: "file", // The name that will be used to transfer the file
            maxFilesize: 5, // MB
            maxFiles: 2,
            addRemoveLinks : true,
            dictResponseError: "No se puede subir esta imagen!",
            autoProcessQueue: true,
            thumbnailWidth: 138,
            thumbnailHeight: 120,
            acceptedFiles:"image/*",
            previewTemplate: '<div class="dz-preview dz-file-preview"><div class="dz-details"><div class="dz-filename"><span data-dz-name style:"font-weight: bold;"></span></div><div class="dz-size">File size: <span data-dz-size></span></div><div class="dz-thumbnail-wrapper"><div class="dz-thumbnail"><img data-dz-thumbnail><span class="dz-nopreview">No preview</span><div class="dz-success-mark"><i class="fa fa-check-circle-o"></i></div><div class="dz-error-mark"><i class="fa fa-times-circle-o"></i></div><div class="dz-error-message"><span data-dz-errormessage></span></div></div></div></div><div class="progress progress-striped active"><div class="progress-bar progress-bar-success" data-dz-uploadprogress></div></div></div>',
            init: function() {
                this.on("success", function(file, responseText) {
                    file.previewTemplate.setAttribute('id',responseText[0].id);
                });
                this.on("thumbnail", function(file) {
                    if (file.width < minImageWidth2 || file.height < minImageHeight2 && file.width > minImageWidth2 || file.height > minImageHeight2) {
                        file.rejectDimensions()
                    }
                    else {
                        if(typeof file !== 'undefined' && file.acceptDimensions===undefined){
                        }else{
                            file.acceptDimensions();
                        }
                    }
                });
            },
            accept: function(file, done) {
                file.acceptDimensions = done;
                file.rejectDimensions = function() { done("La imagen no coincide con las dimensiones"); };
            },
            resize: function(file) {
                var info = { srcX: 0, srcY: 0, srcWidth: file.width, srcHeight: file.height },
                    srcRatio = file.width / file.height;
                if (file.height > this.options.thumbnailHeight || file.width > this.options.thumbnailWidth) {
                    info.trgHeight = this.options.thumbnailHeight;
                    info.trgWidth = info.trgHeight * srcRatio;
                    if (info.trgWidth > this.options.thumbnailWidth) {
                        info.trgWidth = this.options.thumbnailWidth;
                        info.trgHeight = info.trgWidth / srcRatio;
                    }
                } else {
                    info.trgHeight = file.height;
                    info.trgWidth = file.width;
                }
                return info;
            },
            sending : function(file, xhr, formData){
                formData.append("image-post",2);
                formData.append("name-image",$("#img-principal").val());
            },
            success: function(file, response){
                var name_image = jQuery.parseJSON(response);
                $(".dz-preview").addClass("dz-success");
                $("div.progress").remove();
                $("#image-2").val(name_image.name);
                $('#editCourse').formValidation('revalidateField', 'image-2');
            },
            removedfile: function(file) {
                $(file.previewElement).remove();
                $("#image-2").val('');
                $('#editCourse').formValidation('revalidateField', 'image-2');
            }
        });
        var mockFile = { name: "Click en 'Remover archivo'", size: 12345};
        myDropzone.emit("addedfile", mockFile);
        $(".progress.progress-striped.active").addClass("hide");
        var img_principal = $("#img-principal").val();
        var image_load="";
        if(img_principal==""){
            image_load = "/dash/assets/images/users/thumbnail/no-image.jpg";
        }else{
            image_load = "/front/src/images/courses/"+img_principal;
        }
        myDropzone.emit("thumbnail", mockFile,image_load);
        var existingFileCount = 1; // The number of files already uploaded
        myDropzone.options.maxFiles = myDropzone.options.maxFiles - existingFileCount;
    }

    /* Instructor */
    if($("#imgInstructor").length>=1){
        Dropzone.autoDiscover = false;
        var minImageWidth =300, minImageHeight = 300;
        var myDropzone = new Dropzone('#imgInstructor', {
            url: "/dashboard/instructor/uploadfile",
            uploadMultiple : false,
            paramName: "file", // The name that will be used to transfer the file
            maxFilesize: 5, // MB
            maxFiles: 1 ,
            parallelUploads : 1,
            addRemoveLinks : true,
            dictResponseError: "No se puede subir esta imagen!",
            autoProcessQueue: true,
            thumbnailWidth: 138,
            thumbnailHeight: 120,
            acceptedFiles:"image/*",
            previewTemplate: '<div class="dz-preview dz-file-preview"><div class="dz-details"><div class="dz-filename"><span data-dz-name style:"font-weight: bold;"></span></div><div class="dz-size">File size: <span data-dz-size></span></div><div class="dz-thumbnail-wrapper"><div class="dz-thumbnail"><img data-dz-thumbnail><span class="dz-nopreview">No preview</span><div class="dz-success-mark"><i class="fa fa-check-circle-o"></i></div><div class="dz-error-mark"><i class="fa fa-times-circle-o"></i></div><div class="dz-error-message"><span data-dz-errormessage></span></div></div></div></div><div class="progress progress-striped active"><div class="progress-bar progress-bar-success" data-dz-uploadprogress></div></div></div>',
            init: function() {
                this.on("success", function(file, responseText) {
                    file.previewTemplate.setAttribute('id',responseText[0].id);
                });
                this.on("thumbnail", function(file) {
                    if (file.width < minImageWidth || file.height < minImageHeight && file.width > minImageWidth || file.height > minImageHeight) {
                        file.rejectDimensions()
                    }
                    else {
                        file.acceptDimensions();
                    }
                });
            },
            accept: function(file, done) {
                file.acceptDimensions = done;
                file.rejectDimensions = function() { done("La imagen no coincide con las dimensiones"); };
            },
            resize: function(file) {
                var info = { srcX: 0, srcY: 0, srcWidth: file.width, srcHeight: file.height },
                    srcRatio = file.width / file.height;
                if (file.height > this.options.thumbnailHeight || file.width > this.options.thumbnailWidth) {
                    info.trgHeight = this.options.thumbnailHeight;
                    info.trgWidth = info.trgHeight * srcRatio;
                    if (info.trgWidth > this.options.thumbnailWidth) {
                        info.trgWidth = this.options.thumbnailWidth;
                        info.trgHeight = info.trgWidth / srcRatio;
                    }
                } else {
                    info.trgHeight = file.height;
                    info.trgWidth = file.width;
                }
                return info;
            },
            sending : function(file, xhr, formData){
                formData.append("file-post",1);
                formData.append("name-image",$("#name-image").val());
            },
            success: function(file, response){
                var name_image = jQuery.parseJSON(response);
                $(".dz-preview").addClass("dz-success");
                $("div.progress").remove();
                $("#imageI").val(name_image.name);
                $('#newInstructor').formValidation('revalidateField', 'imageI');
            },
            removedfile: function(file) {
                $(file.previewElement).remove();
                $("#imageI").val('');
                $('#newInstructor').formValidation('revalidateField', 'imageI');
            }
        });
    }
    if($("#pdfInstructor").length>=1){
        Dropzone.autoDiscover = false;
        var myDropzone = new Dropzone('#pdfInstructor', {
            url: "/dashboard/instructor/uploadfile",
            uploadMultiple : false,
            paramName: "file", // The name that will be used to transfer the file
            //maxFilesize: 5, // MB
            maxFiles: 1 ,
            parallelUploads : 1,
            addRemoveLinks : true,
            dictResponseError: "No se puede subir esta imagen!",
            autoProcessQueue: true,
            thumbnailWidth: 138,
            thumbnailHeight: 120,
            acceptedFiles:".pdf",
            previewTemplate: '<div class="dz-preview dz-file-preview"><div class="dz-details"><div class="dz-filename"><span data-dz-name style:"font-weight: bold;"></span></div><div class="dz-size">File size: <span data-dz-size></span></div><div class="dz-thumbnail-wrapper"><div class="dz-thumbnail"><img data-dz-thumbnail><span class="dz-nopreview">No preview</span><div class="dz-success-mark"><i class="fa fa-check-circle-o"></i></div><div class="dz-error-mark"><i class="fa fa-times-circle-o"></i></div><div class="dz-error-message"><span data-dz-errormessage></span></div></div></div></div><div class="progress progress-striped active"><div class="progress-bar progress-bar-success" data-dz-uploadprogress></div></div></div>',
            resize: function(file) {
                var info = { srcX: 0, srcY: 0, srcWidth: file.width, srcHeight: file.height },
                    srcRatio = file.width / file.height;
                if (file.height > this.options.thumbnailHeight || file.width > this.options.thumbnailWidth) {
                    info.trgHeight = this.options.thumbnailHeight;
                    info.trgWidth = info.trgHeight * srcRatio;
                    if (info.trgWidth > this.options.thumbnailWidth) {
                        info.trgWidth = this.options.thumbnailWidth;
                        info.trgHeight = info.trgWidth / srcRatio;
                    }
                } else {
                    info.trgHeight = file.height;
                    info.trgWidth = file.width;
                }
                return info;
            },
            sending : function(file, xhr, formData){
                formData.append("file-post",2);
                formData.append("curriculum",$("#name-curriculum").val());
            },
            success: function(file, response){
                var name_image = jQuery.parseJSON(response);
                $(".dz-preview").addClass("dz-success");
                $("div.progress").remove();
                $("#pdfI").val(name_image.name);
                $('#newInstructor').formValidation('revalidateField', 'pdfI');
            },
            removedfile: function(file) {
                $(file.previewElement).remove();
                $("#pdfI").val('');
                $('#newInstructor').formValidation('revalidateField', 'pdfI');
            }
        });
    }
    if($("#imgInstructor-edit").length>=1){
        Dropzone.autoDiscover = false;
        var minImageWidth =300, minImageHeight = 300;
        var myDropzone = new Dropzone('#imgInstructor-edit', {
            url: "/dashboard/instructor/uploadfile",
            uploadMultiple : false,
            paramName: "file", // The name that will be used to transfer the file
            maxFilesize: 5, // MB
            maxFiles: 2 ,
            parallelUploads : 1,
            addRemoveLinks : true,
            dictResponseError: "No se puede subir esta imagen!",
            autoProcessQueue: true,
            thumbnailWidth: 138,
            thumbnailHeight: 120,
            acceptedFiles:"image/*",
            previewTemplate: '<div class="dz-preview dz-file-preview"><div class="dz-details"><div class="dz-filename"><span data-dz-name style:"font-weight: bold;"></span></div><div class="dz-size">File size: <span data-dz-size></span></div><div class="dz-thumbnail-wrapper"><div class="dz-thumbnail"><img data-dz-thumbnail><span class="dz-nopreview">No preview</span><div class="dz-success-mark"><i class="fa fa-check-circle-o"></i></div><div class="dz-error-mark"><i class="fa fa-times-circle-o"></i></div><div class="dz-error-message"><span data-dz-errormessage></span></div></div></div></div><div class="progress progress-striped active"><div class="progress-bar progress-bar-success" data-dz-uploadprogress></div></div></div>',
            init: function() {
                this.on("success", function(file, responseText) {
                    file.previewTemplate.setAttribute('id',responseText[0].id);
                });
                this.on("thumbnail", function(file) {
                    if (file.width < minImageWidth || file.height < minImageHeight && file.width > minImageWidth || file.height > minImageHeight) {
                        file.rejectDimensions()
                    }
                    else {
                        if(typeof file !== 'undefined' && file.acceptDimensions===undefined){
                        }else{
                            file.acceptDimensions();
                        }
                    }
                });
            },
            accept: function(file, done) {
                file.acceptDimensions = done;
                file.rejectDimensions = function() { done("La imagen no coincide con las dimensiones"); };
            },
            resize: function(file) {
                var info = { srcX: 0, srcY: 0, srcWidth: file.width, srcHeight: file.height },
                    srcRatio = file.width / file.height;
                if (file.height > this.options.thumbnailHeight || file.width > this.options.thumbnailWidth) {
                    info.trgHeight = this.options.thumbnailHeight;
                    info.trgWidth = info.trgHeight * srcRatio;
                    if (info.trgWidth > this.options.thumbnailWidth) {
                        info.trgWidth = this.options.thumbnailWidth;
                        info.trgHeight = info.trgWidth / srcRatio;
                    }
                } else {
                    info.trgHeight = file.height;
                    info.trgWidth = file.width;
                }
                return info;
            },
            sending : function(file, xhr, formData){
                formData.append("file-post",1);
                formData.append("name-image",$("#name-image").val());
            },
            success: function(file, response){
                var name_image = jQuery.parseJSON(response);
                $(".dz-preview").addClass("dz-success");
                $("div.progress").remove();
                $("#imageI").val(name_image.name);
                $('#editInstructor').formValidation('revalidateField', 'imageI');
            },
            removedfile: function(file) {
                $(file.previewElement).remove();
                $("#imageI").val('');
                $('#editInstructor').formValidation('revalidateField', 'imageI');
            }
        });
        var mockFile = { name: "Click en 'Remover archivo'", size: 12345};
        myDropzone.emit("addedfile", mockFile);
        $(".progress.progress-striped.active").addClass("hide");
        var img_principal = $("#imageI").val();
        var image_load="";
        if(img_principal==""){
            image_load = "/dash/assets/images/users/thumbnail/no-image.jpg";
        }else{
            image_load = "/dash/img/instructor/img/"+img_principal;
        }
        myDropzone.emit("thumbnail", mockFile,image_load);
        var existingFileCount = 1; // The number of files already uploaded
        myDropzone.options.maxFiles = myDropzone.options.maxFiles - existingFileCount;
    }
    if($("#pdfInstructor-edit").length>=1){
        Dropzone.autoDiscover = false;
        var myDropzone = new Dropzone('#pdfInstructor-edit', {
            url: "/dashboard/instructor/uploadfile",
            uploadMultiple : false,
            paramName: "file", // The name that will be used to transfer the file
            //maxFilesize: 5, // MB
            maxFiles: 2 ,
            parallelUploads : 1,
            addRemoveLinks : true,
            dictResponseError: "No se puede subir este archivo!",
            autoProcessQueue: true,
            thumbnailWidth: 138,
            thumbnailHeight: 120,
            acceptedFiles:".pdf",
            previewTemplate: '<div class="dz-preview dz-file-preview"><div class="dz-details"><div class="dz-filename"><span data-dz-name style:"font-weight: bold;"></span></div><div class="dz-size">File size: <span data-dz-size></span></div><div class="dz-thumbnail-wrapper"><div class="dz-thumbnail"><img data-dz-thumbnail><span class="dz-nopreview">No preview</span><div class="dz-success-mark"><i class="fa fa-check-circle-o"></i></div><div class="dz-error-mark"><i class="fa fa-times-circle-o"></i></div><div class="dz-error-message"><span data-dz-errormessage></span></div></div></div></div><div class="progress progress-striped active"><div class="progress-bar progress-bar-success" data-dz-uploadprogress></div></div></div>',
            resize: function(file) {
                var info = { srcX: 0, srcY: 0, srcWidth: file.width, srcHeight: file.height },
                    srcRatio = file.width / file.height;
                if (file.height > this.options.thumbnailHeight || file.width > this.options.thumbnailWidth) {
                    info.trgHeight = this.options.thumbnailHeight;
                    info.trgWidth = info.trgHeight * srcRatio;
                    if (info.trgWidth > this.options.thumbnailWidth) {
                        info.trgWidth = this.options.thumbnailWidth;
                        info.trgHeight = info.trgWidth / srcRatio;
                    }
                } else {
                    info.trgHeight = file.height;
                    info.trgWidth = file.width;
                }
                return info;
            },
            sending : function(file, xhr, formData){
                formData.append("file-post",2);
                formData.append("curriculum",$("#name-curriculum").val());
            },
            success: function(file, response){
                var name_image = jQuery.parseJSON(response);
                $(".dz-preview").addClass("dz-success");
                $("div.progress").remove();
                $("#pdfI").val(name_image.name);
                $('#editInstructor').formValidation('revalidateField', 'pdfI');
            },
            removedfile: function(file) {
                $(file.previewElement).remove();
                $("#pdfI").val('');
                $('#editInstructor').formValidation('revalidateField', 'pdfI');
            }
        });
        var mockFile = { name: "Click en 'Remover archivo'", size: 256};
        myDropzone.emit("addedfile", mockFile);
        $(".progress.progress-striped.active").addClass("hide");
        var img_principal = $("#pdfI").val();
        var image_load="";
        if(img_principal==""){
            image_load = "/dash/assets/images/users/thumbnail/no-image.jpg";
        }else{
            image_load = "/dash/img/instructor/curriculum/PDF.png";
        }
        myDropzone.emit("thumbnail", mockFile,image_load);
        var existingFileCount = 1; // The number of files already uploaded
        myDropzone.options.maxFiles = myDropzone.options.maxFiles - existingFileCount;
    }

    /* Alliances */
    if($("#imgAlliance").length>=1){
        Dropzone.autoDiscover = false;
        var minImageWidth =76, minImageHeight = 76;
        var myDropzone = new Dropzone('#imgAlliance', {
            url: "/dashboard/alliance/uploadfile",
            uploadMultiple : false,
            paramName: "file", // The name that will be used to transfer the file
            maxFilesize: 5, // MB
            maxFiles: 1 ,
            parallelUploads : 1,
            addRemoveLinks : true,
            dictResponseError: "No se puede subir esta imagen!",
            autoProcessQueue: true,
            thumbnailWidth: 138,
            thumbnailHeight: 120,
            acceptedFiles:"image/*",
            previewTemplate: '<div class="dz-preview dz-file-preview"><div class="dz-details"><div class="dz-filename"><span data-dz-name style:"font-weight: bold;"></span></div><div class="dz-size">File size: <span data-dz-size></span></div><div class="dz-thumbnail-wrapper"><div class="dz-thumbnail"><img data-dz-thumbnail><span class="dz-nopreview">No preview</span><div class="dz-success-mark"><i class="fa fa-check-circle-o"></i></div><div class="dz-error-mark"><i class="fa fa-times-circle-o"></i></div><div class="dz-error-message"><span data-dz-errormessage></span></div></div></div></div><div class="progress progress-striped active"><div class="progress-bar progress-bar-success" data-dz-uploadprogress></div></div></div>',
            init: function() {
                this.on("success", function(file, responseText) {
                    file.previewTemplate.setAttribute('id',responseText[0].id);
                });
                this.on("thumbnail", function(file) {
                    if (file.width < minImageWidth || file.height < minImageHeight && file.width > minImageWidth || file.height > minImageHeight) {
                        file.rejectDimensions()
                    }
                    else {
                        file.acceptDimensions();
                    }
                });
            },
            accept: function(file, done) {
                file.acceptDimensions = done;
                file.rejectDimensions = function() { done("La imagen no coincide con las dimensiones"); };
            },
            resize: function(file) {
                var info = { srcX: 0, srcY: 0, srcWidth: file.width, srcHeight: file.height },
                    srcRatio = file.width / file.height;
                if (file.height > this.options.thumbnailHeight || file.width > this.options.thumbnailWidth) {
                    info.trgHeight = this.options.thumbnailHeight;
                    info.trgWidth = info.trgHeight * srcRatio;
                    if (info.trgWidth > this.options.thumbnailWidth) {
                        info.trgWidth = this.options.thumbnailWidth;
                        info.trgHeight = info.trgWidth / srcRatio;
                    }
                } else {
                    info.trgHeight = file.height;
                    info.trgWidth = file.width;
                }
                return info;
            },
            sending : function(file, xhr, formData){
                formData.append("file-post",1);
                formData.append("name-image",$("#name-image").val());
            },
            success: function(file, response){
                var name_image = jQuery.parseJSON(response);
                $(".dz-preview").addClass("dz-success");
                $("div.progress").remove();
                $("#imageI").val(name_image.name);
                $('#newAlliance').formValidation('revalidateField', 'imageI');
            },
            removedfile: function(file) {
                $(file.previewElement).remove();
                $("#imageI").val('');
                $('#imgAlliance').formValidation('revalidateField', 'imageI');
            }
        });
    }
    if($("#pdfAlliance").length>=1){
        Dropzone.autoDiscover = false;
        var myDropzone = new Dropzone('#pdfAlliance', {
            url: "/dashboard/alliance/uploadfile",
            uploadMultiple : false,
            paramName: "file", // The name that will be used to transfer the file
            //maxFilesize: 5, // MB
            maxFiles: 1 ,
            parallelUploads : 1,
            addRemoveLinks : true,
            dictResponseError: "No se puede subir esta imagen!",
            autoProcessQueue: true,
            thumbnailWidth: 138,
            thumbnailHeight: 120,
            acceptedFiles:".pdf",
            previewTemplate: '<div class="dz-preview dz-file-preview"><div class="dz-details"><div class="dz-filename"><span data-dz-name style:"font-weight: bold;"></span></div><div class="dz-size">File size: <span data-dz-size></span></div><div class="dz-thumbnail-wrapper"><div class="dz-thumbnail"><img data-dz-thumbnail><span class="dz-nopreview">No preview</span><div class="dz-success-mark"><i class="fa fa-check-circle-o"></i></div><div class="dz-error-mark"><i class="fa fa-times-circle-o"></i></div><div class="dz-error-message"><span data-dz-errormessage></span></div></div></div></div><div class="progress progress-striped active"><div class="progress-bar progress-bar-success" data-dz-uploadprogress></div></div></div>',
            resize: function(file) {
                var info = { srcX: 0, srcY: 0, srcWidth: file.width, srcHeight: file.height },
                    srcRatio = file.width / file.height;
                if (file.height > this.options.thumbnailHeight || file.width > this.options.thumbnailWidth) {
                    info.trgHeight = this.options.thumbnailHeight;
                    info.trgWidth = info.trgHeight * srcRatio;
                    if (info.trgWidth > this.options.thumbnailWidth) {
                        info.trgWidth = this.options.thumbnailWidth;
                        info.trgHeight = info.trgWidth / srcRatio;
                    }
                } else {
                    info.trgHeight = file.height;
                    info.trgWidth = file.width;
                }
                return info;
            },
            sending : function(file, xhr, formData){
                formData.append("file-post",2);
                formData.append("agreement",$("#name-agreement").val());
            },
            success: function(file, response){
                var name_image = jQuery.parseJSON(response);
                $(".dz-preview").addClass("dz-success");
                $("div.progress").remove();
                $("#pdfI").val(name_image.name);
                $('#newAlliance').formValidation('revalidateField', 'pdfI');
            },
            removedfile: function(file) {
                $(file.previewElement).remove();
                $("#pdfI").val('');
                $('#newAlliance').formValidation('revalidateField', 'pdfI');
            }
        });
    }
    if($("#imgAlliance-edit").length>=1){
        Dropzone.autoDiscover = false;
        var minImageWidth =76, minImageHeight = 76;
        var myDropzone = new Dropzone('#imgAlliance-edit', {
            url: "/dashboard/alliance/uploadfile",
            uploadMultiple : false,
            paramName: "file", // The name that will be used to transfer the file
            maxFilesize: 5, // MB
            maxFiles: 2 ,
            parallelUploads : 1,
            addRemoveLinks : true,
            dictResponseError: "No se puede subir esta imagen!",
            autoProcessQueue: true,
            thumbnailWidth: 138,
            thumbnailHeight: 120,
            acceptedFiles:"image/*",
            previewTemplate: '<div class="dz-preview dz-file-preview"><div class="dz-details"><div class="dz-filename"><span data-dz-name style:"font-weight: bold;"></span></div><div class="dz-size">File size: <span data-dz-size></span></div><div class="dz-thumbnail-wrapper"><div class="dz-thumbnail"><img data-dz-thumbnail><span class="dz-nopreview">No preview</span><div class="dz-success-mark"><i class="fa fa-check-circle-o"></i></div><div class="dz-error-mark"><i class="fa fa-times-circle-o"></i></div><div class="dz-error-message"><span data-dz-errormessage></span></div></div></div></div><div class="progress progress-striped active"><div class="progress-bar progress-bar-success" data-dz-uploadprogress></div></div></div>',
            init: function() {
                this.on("success", function(file, responseText) {
                    file.previewTemplate.setAttribute('id',responseText[0].id);
                });
                this.on("thumbnail", function(file) {
                    if (file.width < minImageWidth || file.height < minImageHeight && file.width > minImageWidth || file.height > minImageHeight) {
                        file.rejectDimensions()
                    }
                    else {
                        if(typeof file !== 'undefined' && file.acceptDimensions===undefined){
                        }else{
                            file.acceptDimensions();
                        }
                    }
                });
            },
            accept: function(file, done) {
                file.acceptDimensions = done;
                file.rejectDimensions = function() { done("La imagen no coincide con las dimensiones"); };
            },
            resize: function(file) {
                var info = { srcX: 0, srcY: 0, srcWidth: file.width, srcHeight: file.height },
                    srcRatio = file.width / file.height;
                if (file.height > this.options.thumbnailHeight || file.width > this.options.thumbnailWidth) {
                    info.trgHeight = this.options.thumbnailHeight;
                    info.trgWidth = info.trgHeight * srcRatio;
                    if (info.trgWidth > this.options.thumbnailWidth) {
                        info.trgWidth = this.options.thumbnailWidth;
                        info.trgHeight = info.trgWidth / srcRatio;
                    }
                } else {
                    info.trgHeight = file.height;
                    info.trgWidth = file.width;
                }
                return info;
            },
            sending : function(file, xhr, formData){
                formData.append("file-post",1);
                formData.append("name-image",$("#name-image").val());
            },
            success: function(file, response){
                var name_image = jQuery.parseJSON(response);
                $(".dz-preview").addClass("dz-success");
                $("div.progress").remove();
                $("#imageI").val(name_image.name);
                $('#imgAllianceEdit').formValidation('revalidateField', 'imageI');
            },
            removedfile: function(file) {
                $(file.previewElement).remove();
                $("#imageI").val('');
                $('#imgAllianceEdit').formValidation('revalidateField', 'imageI');
            }
        });
        var mockFile = { name: "Click en 'Remover archivo'", size: 12345};
        myDropzone.emit("addedfile", mockFile);
        $(".progress.progress-striped.active").addClass("hide");
        var img_principal = $("#imageI").val();
        var image_load="";
        if(img_principal==""){
            image_load = "/dash/assets/images/users/thumbnail/no-image.jpg";
        }else{
            image_load = "/front/src/images/about/alliance/"+img_principal;
        }
        myDropzone.emit("thumbnail", mockFile,image_load);
        var existingFileCount = 1; // The number of files already uploaded
        myDropzone.options.maxFiles = myDropzone.options.maxFiles - existingFileCount;
    }
    if($("#pdfAlliance-edit").length>=1){
        Dropzone.autoDiscover = false;
        var myDropzone = new Dropzone('#pdfAlliance-edit', {
            url: "/dashboard/alliance/uploadfile",
            uploadMultiple : false,
            paramName: "file", // The name that will be used to transfer the file
            maxFilesize: 100, // MB
            maxFiles: 2 ,
            parallelUploads : 1,
            addRemoveLinks : true,
            dictResponseError: "No se puede subir este archivo!",
            autoProcessQueue: true,
            thumbnailWidth: 138,
            thumbnailHeight: 120,
            acceptedFiles:".pdf",
            previewTemplate: '<div class="dz-preview dz-file-preview"><div class="dz-details"><div class="dz-filename"><span data-dz-name style:"font-weight: bold;"></span></div><div class="dz-size">File size: <span data-dz-size></span></div><div class="dz-thumbnail-wrapper"><div class="dz-thumbnail"><img data-dz-thumbnail><span class="dz-nopreview">No preview</span><div class="dz-success-mark"><i class="fa fa-check-circle-o"></i></div><div class="dz-error-mark"><i class="fa fa-times-circle-o"></i></div><div class="dz-error-message"><span data-dz-errormessage></span></div></div></div></div><div class="progress progress-striped active"><div class="progress-bar progress-bar-success" data-dz-uploadprogress></div></div></div>',
            resize: function(file) {
                var info = { srcX: 0, srcY: 0, srcWidth: file.width, srcHeight: file.height },
                    srcRatio = file.width / file.height;
                if (file.height > this.options.thumbnailHeight || file.width > this.options.thumbnailWidth) {
                    info.trgHeight = this.options.thumbnailHeight;
                    info.trgWidth = info.trgHeight * srcRatio;
                    if (info.trgWidth > this.options.thumbnailWidth) {
                        info.trgWidth = this.options.thumbnailWidth;
                        info.trgHeight = info.trgWidth / srcRatio;
                    }
                } else {
                    info.trgHeight = file.height;
                    info.trgWidth = file.width;
                }
                return info;
            },
            sending : function(file, xhr, formData){
                formData.append("file-post",2);
                formData.append("agreement",$("#name-agreement").val());
            },
            success: function(file, response){
                var name_image = jQuery.parseJSON(response);
                $(".dz-preview").addClass("dz-success");
                $("div.progress").remove();
                $("#pdfI").val(name_image.name);
                $('#newAllianceEdit').formValidation('revalidateField', 'pdfI');
            },
            removedfile: function(file) {
                $(file.previewElement).remove();
                $("#pdfI").val('');
                $('#newAllianceEdit').formValidation('revalidateField', 'pdfI');
            }
        });
        var mockFile = { name: "Click en 'Remover archivo'", size: 11256};
        myDropzone.emit("addedfile", mockFile);
        $(".progress.progress-striped.active").addClass("hide");
        var img_principal = $("#pdfI").val();
        var image_load="";
        if(img_principal==""){
            image_load = "/dash/assets/images/users/thumbnail/no-image.jpg";
        }else{
            image_load = "/dash/img/instructor/curriculum/PDF.png";
        }
        myDropzone.emit("thumbnail", mockFile,image_load);
        var existingFileCount = 1; // The number of files already uploaded
        myDropzone.options.maxFiles = myDropzone.options.maxFiles - existingFileCount;
    }

    /* Slider */
    if($("#imgSlider").length>=1){
        Dropzone.autoDiscover = false;
        var minImageWidth =1350, minImageHeight = 520;
        var myDropzone = new Dropzone('#imgSlider', {
            url: "/dashboard/index/uploadfile",
            uploadMultiple : false,
            paramName: "file", // The name that will be used to transfer the file
            maxFilesize: 5, // MB
            maxFiles: 1 ,
            parallelUploads : 1,
            addRemoveLinks : true,
            dictResponseError: "No se puede subir esta imagen!",
            autoProcessQueue: true,
            thumbnailWidth: 138,
            thumbnailHeight: 120,
            acceptedFiles:"image/*",
            previewTemplate: '<div class="dz-preview dz-file-preview"><div class="dz-details"><div class="dz-filename"><span data-dz-name style:"font-weight: bold;"></span></div><div class="dz-size">File size: <span data-dz-size></span></div><div class="dz-thumbnail-wrapper"><div class="dz-thumbnail"><img data-dz-thumbnail><span class="dz-nopreview">No preview</span><div class="dz-success-mark"><i class="fa fa-check-circle-o"></i></div><div class="dz-error-mark"><i class="fa fa-times-circle-o"></i></div><div class="dz-error-message"><span data-dz-errormessage></span></div></div></div></div><div class="progress progress-striped active"><div class="progress-bar progress-bar-success" data-dz-uploadprogress></div></div></div>',
            init: function() {
                this.on("success", function(file, responseText) {
                    file.previewTemplate.setAttribute('id',responseText[0].id);
                });
                this.on("thumbnail", function(file) {
                    if (file.width < minImageWidth || file.height < minImageHeight && file.width > minImageWidth || file.height > minImageHeight) {
                        file.rejectDimensions()
                    }
                    else {
                        file.acceptDimensions();
                    }
                });
            },
            accept: function(file, done) {
                file.acceptDimensions = done;
                file.rejectDimensions = function() { done("La imagen no coincide con las dimensiones"); };
            },
            resize: function(file) {
                var info = { srcX: 0, srcY: 0, srcWidth: file.width, srcHeight: file.height },
                    srcRatio = file.width / file.height;
                if (file.height > this.options.thumbnailHeight || file.width > this.options.thumbnailWidth) {
                    info.trgHeight = this.options.thumbnailHeight;
                    info.trgWidth = info.trgHeight * srcRatio;
                    if (info.trgWidth > this.options.thumbnailWidth) {
                        info.trgWidth = this.options.thumbnailWidth;
                        info.trgHeight = info.trgWidth / srcRatio;
                    }
                } else {
                    info.trgHeight = file.height;
                    info.trgWidth = file.width;
                }
                return info;
            },
            sending : function(file, xhr, formData){
                formData.append("name-image",$("#name-image").val());
            },
            success: function(file, response){
                var name_image = jQuery.parseJSON(response);
                $(".dz-preview").addClass("dz-success");
                $("div.progress").remove();
                $("#imageI").val(name_image.name);
                $('#newSlider').formValidation('revalidateField', 'imageI');
            },
            removedfile: function(file) {
                $(file.previewElement).remove();
                $("#imageI").val('');
                $('#newSlider').formValidation('revalidateField', 'imageI');
            }
        });
    }
    if($("#imgSlider-edit").length>=1){
        Dropzone.autoDiscover = false;
        var minImageWidth =1350, minImageHeight = 520;
        var myDropzone = new Dropzone('#imgSlider-edit', {
            url: "/dashboard/index/uploadfile",
            uploadMultiple : false,
            paramName: "file", // The name that will be used to transfer the file
            maxFilesize: 5, // MB
            maxFiles: 2 ,
            parallelUploads : 1,
            addRemoveLinks : true,
            dictResponseError: "No se puede subir esta imagen!",
            autoProcessQueue: true,
            thumbnailWidth: 138,
            thumbnailHeight: 120,
            acceptedFiles:"image/*",
            previewTemplate: '<div class="dz-preview dz-file-preview"><div class="dz-details"><div class="dz-filename"><span data-dz-name style:"font-weight: bold;"></span></div><div class="dz-size">File size: <span data-dz-size></span></div><div class="dz-thumbnail-wrapper"><div class="dz-thumbnail"><img data-dz-thumbnail><span class="dz-nopreview">No preview</span><div class="dz-success-mark"><i class="fa fa-check-circle-o"></i></div><div class="dz-error-mark"><i class="fa fa-times-circle-o"></i></div><div class="dz-error-message"><span data-dz-errormessage></span></div></div></div></div><div class="progress progress-striped active"><div class="progress-bar progress-bar-success" data-dz-uploadprogress></div></div></div>',
            init: function() {
                this.on("success", function(file, responseText) {
                    file.previewTemplate.setAttribute('id',responseText[0].id);
                });
                this.on("thumbnail", function(file) {
                    if (file.width < minImageWidth || file.height < minImageHeight && file.width > minImageWidth || file.height > minImageHeight) {
                        file.rejectDimensions()
                    }
                    else {
                        if(typeof file !== 'undefined' && file.acceptDimensions===undefined){
                        }else{
                            file.acceptDimensions();
                        }
                    }
                });
            },
            accept: function(file, done) {
                file.acceptDimensions = done;
                file.rejectDimensions = function() { done("La imagen no coincide con las dimensiones"); };
            },
            resize: function(file) {
                var info = { srcX: 0, srcY: 0, srcWidth: file.width, srcHeight: file.height },
                    srcRatio = file.width / file.height;
                if (file.height > this.options.thumbnailHeight || file.width > this.options.thumbnailWidth) {
                    info.trgHeight = this.options.thumbnailHeight;
                    info.trgWidth = info.trgHeight * srcRatio;
                    if (info.trgWidth > this.options.thumbnailWidth) {
                        info.trgWidth = this.options.thumbnailWidth;
                        info.trgHeight = info.trgWidth / srcRatio;
                    }
                } else {
                    info.trgHeight = file.height;
                    info.trgWidth = file.width;
                }
                return info;
            },
            sending : function(file, xhr, formData){
                formData.append("name-image",$("#name-image").val());
            },
            success: function(file, response){
                var name_image = jQuery.parseJSON(response);
                $(".dz-preview").addClass("dz-success");
                $("div.progress").remove();
                $("#imageI").val(name_image.name);
                $('#editSlider').formValidation('revalidateField', 'imageI');
            },
            removedfile: function(file) {
                $(file.previewElement).remove();
                $("#imageI").val('');
                $('#editSlider').formValidation('revalidateField', 'imageI');
            }
        });
        var mockFile = { name: "Click en 'Remover archivo'", size: 12345};
        myDropzone.emit("addedfile", mockFile);
        $(".progress.progress-striped.active").addClass("hide");
        var img_principal = $("#imageI").val();
        var image_load="";
        if(img_principal==""){
            image_load = "/dash/assets/images/users/thumbnail/no-image.jpg";
        }else{
            image_load = "/front/src/images/slider/"+img_principal;
        }
        myDropzone.emit("thumbnail", mockFile,image_load);
        var existingFileCount = 1; // The number of files already uploaded
        myDropzone.options.maxFiles = myDropzone.options.maxFiles - existingFileCount;
    }

    /* Finished Course*/
    if($("#image-principal-Fcourse").length>=1){
        Dropzone.autoDiscover = false;
        var minImageWidth2 =370, minImageHeight2 = 277;
        var myDropzone = new Dropzone('#image-principal-Fcourse', {
            url: "/dashboard/finished/uploadfile",
            uploadMultiple : false,
            paramName: "file", // The name that will be used to transfer the file
            maxFilesize: 5, // MB
            maxFiles: 1 ,
            parallelUploads : 1,
            addRemoveLinks : true,
            dictResponseError: "No se puede subir esta imagen!",
            autoProcessQueue: true,
            thumbnailWidth: 138,
            thumbnailHeight: 120,
            acceptedFiles:"image/*",
            previewTemplate: '<div class="dz-preview dz-file-preview"><div class="dz-details"><div class="dz-filename"><span data-dz-name style:"font-weight: bold;"></span></div><div class="dz-size">File size: <span data-dz-size></span></div><div class="dz-thumbnail-wrapper"><div class="dz-thumbnail"><img data-dz-thumbnail><span class="dz-nopreview">No preview</span><div class="dz-success-mark"><i class="fa fa-check-circle-o"></i></div><div class="dz-error-mark"><i class="fa fa-times-circle-o"></i></div><div class="dz-error-message"><span data-dz-errormessage></span></div></div></div></div><div class="progress progress-striped active"><div class="progress-bar progress-bar-success" data-dz-uploadprogress></div></div></div>',
            init: function() {
                this.on("success", function(file, responseText) {
                    file.previewTemplate.setAttribute('id',responseText[0].id);
                });
                this.on("thumbnail", function(file) {
                    if (file.width < minImageWidth2 || file.height < minImageHeight2 && file.width > minImageWidth2 || file.height > minImageHeight2) {
                        file.rejectDimensions()
                    }
                    else {
                        file.acceptDimensions();
                    }
                });
            },
            accept: function(file, done) {
                file.acceptDimensions = done;
                file.rejectDimensions = function() { done("La imagen no coincide con las dimensiones"); };
            },
            resize: function(file) {
                var info = { srcX: 0, srcY: 0, srcWidth: file.width, srcHeight: file.height },
                    srcRatio = file.width / file.height;
                if (file.height > this.options.thumbnailHeight || file.width > this.options.thumbnailWidth) {
                    info.trgHeight = this.options.thumbnailHeight;
                    info.trgWidth = info.trgHeight * srcRatio;
                    if (info.trgWidth > this.options.thumbnailWidth) {
                        info.trgWidth = this.options.thumbnailWidth;
                        info.trgHeight = info.trgWidth / srcRatio;
                    }
                } else {
                    info.trgHeight = file.height;
                    info.trgWidth = file.width;
                }
                return info;
            },
            sending : function(file, xhr, formData){
                formData.append("file-post",1);
                formData.append("name-image",$("#name-image").val());
            },
            success: function(file, response){
                var name_image = jQuery.parseJSON(response);
                $(".dz-preview").addClass("dz-success");
                $("div.progress").remove();
                $("#image-1").val(name_image.name);
                $('#newFinishedCourse').formValidation('revalidateField', 'image-1');
                file.previewElement.id = name_image.name;
            },
            removedfile: function(file) {
                $(file.previewElement).remove();
                $("#image-1").val('');
                $('#newFinishedCourse').formValidation('revalidateField', 'image-1');
            }
        });
    }
    if($("#gallery-Fcourse").length>=1){
        var order=1;
        Dropzone.autoDiscover = false;
        var minImageWidth =776, minImageHeight = 582;
        var myDropzone = new Dropzone('#gallery-Fcourse', {
            url: "/dashboard/finished/uploadfile",
            uploadMultiple : false,
            paramName: "file", // The name that will be used to transfer the file
            maxFilesize: 5, // MB
            maxFiles: 100 ,
            addRemoveLinks : true,
            dictResponseError: "No se puede subir esta imagen!",
            autoProcessQueue: true,
            thumbnailWidth: 138,
            thumbnailHeight: 120,
            acceptedFiles:"image/*",
            previewTemplate: '<div class="dz-preview dz-file-preview"><div class="dz-details"><div class="dz-filename"><span data-dz-name style:"font-weight: bold;"></span></div><div class="dz-size">File size: <span data-dz-size></span></div><div class="dz-thumbnail-wrapper"><div class="dz-thumbnail"><img data-dz-thumbnail><span class="dz-nopreview">No preview</span><div class="dz-success-mark"><i class="fa fa-check-circle-o"></i></div><div class="dz-error-mark"><i class="fa fa-times-circle-o"></i></div><div class="dz-error-message"><span data-dz-errormessage></span></div></div></div></div><div class="progress progress-striped active"><div class="progress-bar progress-bar-success" data-dz-uploadprogress></div></div></div>',
            init: function() {
                this.on("success", function(file, responseText) {
                    file.previewTemplate.setAttribute('id',responseText[0].id);
                });
                this.on("thumbnail", function(file) {
                    if (file.width < minImageWidth || file.height < minImageHeight && file.width > minImageWidth || file.height > minImageHeight) {
                        file.rejectDimensions()
                    }
                    else {
                        file.acceptDimensions();
                    }
                });
            },
            accept: function(file, done) {
                file.acceptDimensions = done;
                file.rejectDimensions = function() { done("La imagen no coincide con las dimensiones"); };
            },
            resize: function(file) {
                var info = { srcX: 0, srcY: 0, srcWidth: file.width, srcHeight: file.height },
                    srcRatio = file.width / file.height;
                if (file.height > this.options.thumbnailHeight || file.width > this.options.thumbnailWidth) {
                    info.trgHeight = this.options.thumbnailHeight;
                    info.trgWidth = info.trgHeight * srcRatio;
                    if (info.trgWidth > this.options.thumbnailWidth) {
                        info.trgWidth = this.options.thumbnailWidth;
                        info.trgHeight = info.trgWidth / srcRatio;
                    }
                } else {
                    info.trgHeight = file.height;
                    info.trgWidth = file.width;
                }
                return info;
            },
            sending : function(file, xhr, formData){
                formData.append("file-post",2);
            },
            success: function(file, response){
                var name_image = jQuery.parseJSON(response);
                $(".dz-preview").addClass("dz-success");
                $("div.progress").remove();
                file.previewElement.accessKey =name_image.idImage;
                if(order<=1){
                    order++;
                    $(".galleryI").val(name_image.idImage).addClass(name_image.idImage);
                }else{
                    $("#upload-gallery").append("<input name='galleryI[]' value='"+name_image.idImage+"' class='"+name_image.idImage+"'>");
                    order++;
                }
            },
            removedfile: function(file) {
                image = file.previewElement.accessKey;
                $.ajax({
                    url: "/dashboard/finished/deleteimage",
                    type: "post",
                    data: { "image":image},
                    dataType:"json",
                    success : function(response){
                        if(response.message=="SUCCESS" && response.code==200){
                            $(file.previewElement).remove();
                            $("."+image).remove();
                        }else{
                            $(file.previewElement).remove();
                        }
                    },
                    error : function(){
                        alert("Ha ocurrido un error");
                    }
                });
            }
        });
    }
    if($("#image-principal-Fcourse-edit").length>=1){
        Dropzone.autoDiscover = false;
        var minImageWidth =370, minImageHeight = 277;
        var myDropzone = new Dropzone('#image-principal-Fcourse-edit', {
            url: "/dashboard/finished/uploadfile",
            uploadMultiple : false,
            paramName: "file", // The name that will be used to transfer the file
            maxFilesize: 5, // MB
            maxFiles: 2 ,
            parallelUploads : 1,
            addRemoveLinks : true,
            dictResponseError: "No se puede subir esta imagen!",
            autoProcessQueue: true,
            thumbnailWidth: 138,
            thumbnailHeight: 120,
            acceptedFiles:"image/*",
            previewTemplate: '<div class="dz-preview dz-file-preview"><div class="dz-details"><div class="dz-filename"><span data-dz-name style:"font-weight: bold;"></span></div><div class="dz-size">File size: <span data-dz-size></span></div><div class="dz-thumbnail-wrapper"><div class="dz-thumbnail"><img data-dz-thumbnail><span class="dz-nopreview">No preview</span><div class="dz-success-mark"><i class="fa fa-check-circle-o"></i></div><div class="dz-error-mark"><i class="fa fa-times-circle-o"></i></div><div class="dz-error-message"><span data-dz-errormessage></span></div></div></div></div><div class="progress progress-striped active"><div class="progress-bar progress-bar-success" data-dz-uploadprogress></div></div></div>',
            init: function() {
                this.on("success", function(file, responseText) {
                    file.previewTemplate.setAttribute('id',responseText[0].id);
                });
                this.on("thumbnail", function(file) {
                    if (file.width < minImageWidth || file.height < minImageHeight && file.width > minImageWidth || file.height > minImageHeight) {
                        file.rejectDimensions()
                    }
                    else {
                        if(typeof file !== 'undefined' && file.acceptDimensions===undefined){
                        }else{
                            file.acceptDimensions();
                        }
                    }
                });
            },
            accept: function(file, done) {
                file.acceptDimensions = done;
                file.rejectDimensions = function() { done("La imagen no coincide con las dimensiones"); };
            },
            resize: function(file) {
                var info = { srcX: 0, srcY: 0, srcWidth: file.width, srcHeight: file.height },
                    srcRatio = file.width / file.height;
                if (file.height > this.options.thumbnailHeight || file.width > this.options.thumbnailWidth) {
                    info.trgHeight = this.options.thumbnailHeight;
                    info.trgWidth = info.trgHeight * srcRatio;
                    if (info.trgWidth > this.options.thumbnailWidth) {
                        info.trgWidth = this.options.thumbnailWidth;
                        info.trgHeight = info.trgWidth / srcRatio;
                    }
                } else {
                    info.trgHeight = file.height;
                    info.trgWidth = file.width;
                }
                return info;
            },
            sending : function(file, xhr, formData){
                formData.append("file-post",1);
                formData.append("name-image",$("#name-image").val());
            },
            success: function(file, response){
                var name_image = jQuery.parseJSON(response);
                $(".dz-preview").addClass("dz-success");
                $("div.progress").remove();
                $("#image-1").val(name_image.name);
                $('#editFinishedCourse').formValidation('revalidateField', 'image-1');
                file.previewElement.id = name_image.name;
            },
            removedfile: function(file) {
                $(file.previewElement).remove();
                $("#image-1").val('');
                $('#editFinishedCourse').formValidation('revalidateField', 'image-1');
            }
        });
        var mockFile = { name: "Click en 'Remover archivo'", size: 12345};
        myDropzone.emit("addedfile", mockFile);
        $(".progress.progress-striped.active").addClass("hide");
        var img_principal = $("#image-1").val();
        var image_load="";
        if(img_principal==""){
            image_load = "/dash/assets/images/users/thumbnail/no-image.jpg";
        }else{
            image_load = "/front/src/images/blog/"+img_principal;
        }
        myDropzone.emit("thumbnail", mockFile,image_load);
        var existingFileCount = 1; // The number of files already uploaded
        myDropzone.options.maxFiles = myDropzone.options.maxFiles - existingFileCount;
    }
    if($("#gallery-Fcourse-edit").length>=1){
        Dropzone.autoDiscover = false;
        var minImageWidth2 =776, minImageHeight2 = 582;
        var myDropzone2 = new Dropzone('#gallery-Fcourse-edit', {
            url: "/dashboard/finished/uploadfile",
            uploadMultiple : false,
            paramName: "file", // The name that will be used to transfer the file
            maxFilesize: 5, // MB
            maxFiles: 1000 ,
            parallelUploads : 1,
            addRemoveLinks : true,
            dictResponseError: "No se puede subir esta imagen!",
            autoProcessQueue: true,
            thumbnailWidth: 138,
            thumbnailHeight: 120,
            acceptedFiles:"image/*",
            previewTemplate: '<div class="dz-preview dz-file-preview"><div class="dz-details"><div class="dz-filename"><span data-dz-name style:"font-weight: bold;"></span></div><div class="dz-size">File size: <span data-dz-size></span></div><div class="dz-thumbnail-wrapper"><div class="dz-thumbnail"><img data-dz-thumbnail><span class="dz-nopreview">No preview</span><div class="dz-success-mark"><i class="fa fa-check-circle-o"></i></div><div class="dz-error-mark"><i class="fa fa-times-circle-o"></i></div><div class="dz-error-message"><span data-dz-errormessage></span></div></div></div></div><div class="progress progress-striped active"><div class="progress-bar progress-bar-success" data-dz-uploadprogress></div></div></div>',
            init: function() {
                this.on("success", function(file, responseText) {
                    file.previewTemplate.setAttribute('id',responseText[0].id);
                });
                this.on("thumbnail", function(file) {
                    if (file.width < minImageWidth2 || file.height < minImageHeight2 && file.width > minImageWidth2 || file.height > minImageHeight2) {
                        file.rejectDimensions()
                    }
                    else {
                        if(typeof file !== 'undefined' && file.acceptDimensions===undefined){
                        }else{
                            file.acceptDimensions();
                        }
                    }
                });
            },
            accept: function(file, done) {
                file.acceptDimensions = done;
                file.rejectDimensions = function() { done("La imagen no coincide con las dimensiones"); };
            },
            resize: function(file) {
                var info = { srcX: 0, srcY: 0, srcWidth: file.width, srcHeight: file.height },
                    srcRatio = file.width / file.height;
                if (file.height > this.options.thumbnailHeight || file.width > this.options.thumbnailWidth) {
                    info.trgHeight = this.options.thumbnailHeight;
                    info.trgWidth = info.trgHeight * srcRatio;
                    if (info.trgWidth > this.options.thumbnailWidth) {
                        info.trgWidth = this.options.thumbnailWidth;
                        info.trgHeight = info.trgWidth / srcRatio;
                    }
                } else {
                    info.trgHeight = file.height;
                    info.trgWidth = file.width;
                }
                return info;
            },
            sending : function(file, xhr, formData){
                formData.append("file-post",2);
                formData.append("fid",$("#fid").val());
            },
            success: function(file, response){
                var name_image = jQuery.parseJSON(response);
                $(".dz-preview").addClass("dz-success");
                $("div.progress").remove();
                file.previewElement.accessKey =name_image.idImage;
                $("#upload-gallery").append("<input name='galleryI[]' value='"+name_image.idImage+"' class='"+name_image.idImage+"'>");

            },
            removedfile: function(file) {
                image = file.previewElement.accessKey;
                $.ajax({
                    url: "/dashboard/finished/deleteimage",
                    type: "post",
                    data: { "image":image},
                    dataType:"json",
                    success : function(response){
                        if(response.message=="SUCCESS" && response.code==200){
                            $(file.previewElement).remove();
                            $("."+image).remove();
                        }else{
                            $(file.previewElement).remove();
                        }
                    },
                    error : function(){
                        alert("Ha ocurrido un error");
                    }
                });
            }
        });
            var images = $('input[name="galleryI[]"]');
            images.each(function() {
                var value = $(this).val(); // get values
                var id = $(this).attr('id'); // split value

                var mockFile = { name: "Click para remover la imagen", size: 12345};
                myDropzone2.emit("addedfile",mockFile);
                $(".progress.progress-striped.active").addClass("hide");
                myDropzone2.element.lastChild.accessKey=id;

                image_load = "/front/src/images/blog/gallery/normal/"+value;
                myDropzone2.emit("thumbnail", mockFile,image_load);
                var existingFileCount = 1; // The number of files already uploaded
                myDropzone2.options.maxFiles = myDropzone2.options.maxFiles - existingFileCount;
         });
    }
})