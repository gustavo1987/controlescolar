var morrisCharts = function() {
    Morris.Donut({
        element: 'morris-donut-example',
        data: [
            {label: "Total de usuarios", value: $("#users").data('users')},
            {label: "Usuarios con pagos", value: $("#payed").data('payed')},
            {label: "Usuarios sin pagos", value: $("#notPayed").data('not')}
        ],
        colors: ['#95B75D', '#3FBAE4', '#FEA223']
    });
    Morris.Donut({
        element: 'morris-money',
        data: [
            {label: "Ganacia estimada", value: $("#earnings").data('earnings')},
            {label: "Total de pagos", value: $("#money").data('money')},
            {label: "Faltante total", value: $("#notMoney").data('not')}
        ],
        colors: ['#95B75D', '#3FBAE4', '#FEA223'],
        formatter : function (y, data) { return '$' + y }
    });
    Morris.Donut({
        element: 'morris-visits',
        data: [
            {label: "Numero de visitas", value: $("#visits").data('visits')}
        ],
        colors: ['#95B75D']
    });
    if($("#morris-poll").length>=1){
        Morris.Donut({
            element: 'morris-poll',
            data: [
                {label: "Total enviadas", value: $("#send").data('send')},
                {label: "Total contestadas", value: $("#answered").data('answered')},
                {label: "Total faltantes", value: $("#unanswered").data('unanswered')}
            ],
            colors: ['#95B75D', '#3FBAE4', '#FEA223']
        });
    }
}();