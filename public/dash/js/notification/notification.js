$(document).ready(function () {
	$(".typeNotifi").on("change",function(){
		if($(".typeNotifi:checked").val()==1){
			$("#student-notification").removeClass("hidden");
			$("#teacher-notification").addClass("hidden");
			$("#class-notification").addClass("hidden");
		}
		else if($(".typeNotifi:checked").val()==2){
			$("#teacher-notification").removeClass("hidden");
			$("#student-notification").addClass("hidden");
			$("#class-notification").addClass("hidden");
		}
		else if($(".typeNotifi:checked").val()==3){
			$("#class-notification").removeClass("hidden");
			$("#teacher-notification").addClass("hidden");
			$("#student-notification").addClass("hidden");
		}
	});


    $(document.body).on("click", ".delete-notification", function(){
        id_noti = $(this).attr("data-notid");
        Swal.fire('Eliminando Notificación, con id:'+id_noti);
        $.ajax({
            type : "POST",
            url : "/dashboard/notification/delete",
            data :{id:id_noti},
            dataType : "json",
            success : function(response){
                if(response.code==200){
                    //messages(1);
                    Swal.fire({
                      position: 'top-end',
                      type: 'success',
                      title: 'Notificación Eliminada',
                      showConfirmButton: false,
                      timer: 1500
                    })
                    setTimeout(function(){
                        window.location.href = "/dashboard/notification/index";
                    },1500)
                }else{
                    swal ( "Oops" ,  "Hubo algún error de guardado!" ,  "error" );
                }
            },
            error : function(){
                swal ( "Oops" ,  "No pudo entrar al servidor!" ,  "error" );
            }
        });
    });


	




	var newNotification= $("#newNotification");
    var count_fiscal_state = 1;
    if(newNotification.length>=1){
        /*getSpanishD();*/
        $('.birth_date').datepicker({
            format: 'dd/mm/yyyy',
            language : "es",
            autoclose:true
        });
        newNotification.formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            locale: 'es_ES',
            excluded: ':disabled',
            fields: {
                notification: {
                    validators: {notEmpty: {}}
                },
                status: {
                    validators: {notEmpty: {}}
                },
                users: {
                    validators: {notEmpty: {}}
                },
                birth_date: {
                	validators: {notEmpty: {}}
                }
            }
        }).on('success.form.fv', function(e) {
            e.preventDefault();
            $("#message-box-info").toggleClass("open");
            $.ajax({
                type : "POST",
                url : "/dashboard/notification/save",
                data :$(this).serialize(),
                dataType : "json",
                success : function(response){
                    if(response.code==200){
                        messages(1);
                        setTimeout(function(){
                            window.location.href = "/dashboard/notification/index";
                        },1500)

                    }else{
                        swal ( "Oops" ,  "Hubo algún error de guardado!" ,  "error" );
                    }
                },
                error : function(){
                    swal ( "Oops" ,  "No pudo entrar al servidor!" ,  "error" );
                }
            });
        });
    }
})

/* End Clients */
function messages($type){
    if($type==1){
        setTimeout(function(){
            $("#message-box-info").removeClass("open");
            $("#message-box-success").toggleClass("open");
        },1500);
        setTimeout(function(){
            $("#message-box-success").removeClass("open");
        },2000);
    }
    else if($type==2){
        setTimeout(function(){
            $("#message-box-info").removeClass("open");
            $("#message-box-warning").toggleClass("open");
        },1500);
        setTimeout(function(){
            $("#message-box-warning").removeClass("open");
        },3000);
    }else if($type==3){
        setTimeout(function(){
            $("#message-box-info").removeClass("open");
            $("#message-box-warning").toggleClass("open");
        },1500);
        setTimeout(function(){
            $("#message-box-warning").removeClass("open");
        },3000);
    }
}