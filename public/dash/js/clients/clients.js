/* Clients */
$(document).ready(function(){


/*********  crear admisión  ***********/
     var newClient = $("#newadmisioncreate");
    var count_fiscal_state = 1;
    if(newClient.length>=1){
        getSpanishD();
        $('.birth_date').datepicker({
            format: 'dd/mm/yyyy',
            language : "es",
            autoclose:true
        }).on('changeDate', function(e) {
            var date = $(".birth_date").val();
            var year = date.split("/");
            var d = new Date();
            var age = d.getFullYear()-year[2];
            $("#age").val(age);
        }).on('clearDate', function(e) {
            $("#age").val("");
        });
        newClient.formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            locale: 'es_ES',
            excluded: ':disabled',
            fields: {
                name: {
                    validators: {notEmpty: {}}
                },
                last_name: {
                    validators: {notEmpty: {}}
                },
                sex: {
                    validators: {notEmpty: {}}
                },
                email: {
                    validators: {
                        validators: {notEmpty: {}},
                        emailAddress: {
                            message: 'Este campo no contiene una dirección de correo electrónico válida.'
                        },
                        remote: {
                            message: 'Esta cuenta de correo electrónico ya existe, pruebe con otra',
                            url: '/dashboard/client/validate-email',
                            data: {
                                type: 'email'
                            },
                            type: 'POST'
                        }
                    }
                },
                tel: {
                    validators: {notEmpty: {}}
                },
                iva: {
                    validators: {
                        notEmpty: {
                            message: 'El iva es un campo necesario y no puede estar vacío.'
                        }
                    }
                },
                rfc: {
                    validators: {
                        callback: {
                            message: 'El RFC es un campo necesario.',
                            callback: function(value, validator, $field) {
                                var channel = newClient.find('[name="iva"]:checked').val();
                                return (channel !== 'mx') ? true : (value !== '');
                            }
                        },
                        remote: {
                            message: 'Este RFC ya existe, pruebe con otro',
                            url: '/dashboard/client/validate-rfc',
                            data: {
                                type: 'rfc'
                            },
                            type: 'POST'
                        }
                    }
                },
                business_name: {
                    validators: {
                        callback: {
                            message: 'La Razón Social es un campo necesario.',
                            callback: function(value, validator, $field) {
                                var channel = newClient.find('[name="iva"]:checked').val();
                                return (channel !== 'mx') ? true : (value !== '');
                            }
                        }
                    }
                },
                street: {
                    validators: {
                        callback: {
                            message: 'La Dirección Fiscal es un campo necesario.',
                            callback: function(value, validator, $field) {
                                var channel = newClient.find('[name="iva"]:checked').val();
                                return (channel !== 'mx') ? true : (value !== '');
                            }
                        }
                    }
                },
                mpid_fiscal: {
                    validators: {
                        callback: {
                            message: 'El municipio es requerido',
                            callback: function(value, validator, $field) {
                                var channel = newClient.find('[name="iva"]:checked').val();
                                return (channel !== 'mx') ? true : (value !== '');
                            }
                        }
                    }
                },
                postal_code: {
                    validators: {
                        callback: {
                            message: 'El Código Postal es un campo necesario.',
                            callback: function(value, validator, $field) {
                                var channel = newClient.find('[name="iva"]:checked').val();
                                return (channel !== 'mx') ? true : (value !== '');
                            }
                        }
                    }
                },
                email_fiscal: {
                    validators: {
                        callback: {
                            message: 'El campo Email Facturación es un campo necesario.',
                            callback: function(value, validator, $field) {
                                var channel = newClient.find('[name="iva"]:checked').val();
                                return (channel !== 'mx') ? true : (value !== '');
                            }
                        }
                    }
                }
            }
        }).on('change', '[name="iva"]', function(e) {
            newClient.formValidation('revalidateField', 'rfc');
            newClient.formValidation('revalidateField', 'business_name');
            newClient.formValidation('revalidateField', 'street');
            newClient.formValidation('revalidateField', 'mpid_fiscal');
            newClient.formValidation('revalidateField', 'postal_code');
            newClient.formValidation('revalidateField', 'email_fiscal');
            if(count_fiscal_state==1){getMunicipios($("#state"),"mpid_fiscal",newClient);count_fiscal_state++}
        }).on('success.field.fv', function(e, data) {
            if (data.field === 'rfc' || data.field === 'business_name' || data.field === 'street' || data.field === 'mpid_fiscal' || data.field === 'postal_code' || data.field === 'email_fiscal') {
                var channel = newClient.find('[name="iva"]:checked').val();
                // User choose given channel
                if (channel !== 'mx') {
                    // Remove the success class from the container
                    data.element.closest('.form-group').removeClass('has-success');
                    // Hide the tick icon
                    data.element.data('fv.icon').hide();
                }
            }
        }).on('success.form.fv', function(e) {
            e.preventDefault();
            $("#message-box-info").toggleClass("open");
            $.ajax({
                type : "POST",
                url : "/dashboard/client/save",
                data :$(this).serialize(),
                dataType : "json",
                success : function(response){
                    if(response.code==200){
                        messages(1);
                        $(".billing").addClass("hide");
                        var channel = $('input[type="radio"][name="iva"]:checked').val();
                        if (channel=='mx'){$("#state").select2("val", "");$("#mpid_fiscal").select2("val", "");}

                        resetForm(newClient);
                        newClient.formValidation('resetForm', true);
                        setTimeout(function(){
                            $("#other_products").toggleClass("open");
                        },1000);
                    }else{
                        messages(2)
                    }
                },
                error : function(){
                    messages(3);
                }
            });
        });
        $("form#newadmisioncreate input[name='iva']").click(function(){
            var radio = $(this).val();
            if(radio=="mx"){
                $(".billing").removeClass("hide");
            }else{
                $(".billing").addClass("hide");
            }
        });
        getMunicipios($("#state_dir"),"mpid",newClient);
    }

    /********* Crear estudiante ***********/
    var newClient = $("#newClient");
    var count_fiscal_state = 1;
    if(newClient.length>=1){
        getSpanishD();
        $('.birth_date').datepicker({
            format: 'dd/mm/yyyy',
            language : "es",
            autoclose:true
        }).on('changeDate', function(e) {
            var date = $(".birth_date").val();
            var year = date.split("/");
            var d = new Date();
            var age = d.getFullYear()-year[2];
            $("#age").val(age);
        }).on('clearDate', function(e) {
            $("#age").val("");
        });
        newClient.formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            locale: 'es_ES',
            excluded: ':disabled',
            fields: {
                name: {
                    validators: {notEmpty: {}}
                },
                last_name: {
                    validators: {notEmpty: {}}
                },
                sex: {
                    validators: {notEmpty: {}}
                },
                email: {
                    validators: {
                        validators: {notEmpty: {}},
                        emailAddress: {
                            message: 'Este campo no contiene una dirección de correo electrónico válida.'
                        },
                        remote: {
                            message: 'Esta cuenta de correo electrónico ya existe, pruebe con otra',
                            url: '/dashboard/client/validate-email',
                            data: {
                                type: 'email'
                            },
                            type: 'POST'
                        }
                    }
                },
                tel: {
                    validators: {notEmpty: {}}
                },
                iva: {
                    validators: {
                        notEmpty: {
                            message: 'El iva es un campo necesario y no puede estar vacío.'
                        }
                    }
                },
                rfc: {
                    validators: {
                        callback: {
                            message: 'El RFC es un campo necesario.',
                            callback: function(value, validator, $field) {
                                var channel = newClient.find('[name="iva"]:checked').val();
                                return (channel !== 'mx') ? true : (value !== '');
                            }
                        },
                        remote: {
                            message: 'Este RFC ya existe, pruebe con otro',
                            url: '/dashboard/client/validate-rfc',
                            data: {
                                type: 'rfc'
                            },
                            type: 'POST'
                        }
                    }
                },
                business_name: {
                    validators: {
                        callback: {
                            message: 'La Razón Social es un campo necesario.',
                            callback: function(value, validator, $field) {
                                var channel = newClient.find('[name="iva"]:checked').val();
                                return (channel !== 'mx') ? true : (value !== '');
                            }
                        }
                    }
                },
                street: {
                    validators: {
                        callback: {
                            message: 'La Dirección Fiscal es un campo necesario.',
                            callback: function(value, validator, $field) {
                                var channel = newClient.find('[name="iva"]:checked').val();
                                return (channel !== 'mx') ? true : (value !== '');
                            }
                        }
                    }
                },
                mpid_fiscal: {
                    validators: {
                        callback: {
                            message: 'El municipio es requerido',
                            callback: function(value, validator, $field) {
                                var channel = newClient.find('[name="iva"]:checked').val();
                                return (channel !== 'mx') ? true : (value !== '');
                            }
                        }
                    }
                },
                postal_code: {
                    validators: {
                        callback: {
                            message: 'El Código Postal es un campo necesario.',
                            callback: function(value, validator, $field) {
                                var channel = newClient.find('[name="iva"]:checked').val();
                                return (channel !== 'mx') ? true : (value !== '');
                            }
                        }
                    }
                },
                email_fiscal: {
                    validators: {
                        callback: {
                            message: 'El campo Email Facturación es un campo necesario.',
                            callback: function(value, validator, $field) {
                                var channel = newClient.find('[name="iva"]:checked').val();
                                return (channel !== 'mx') ? true : (value !== '');
                            }
                        }
                    }
                }
            }
        }).on('change', '[name="iva"]', function(e) {
            newClient.formValidation('revalidateField', 'rfc');
            newClient.formValidation('revalidateField', 'business_name');
            newClient.formValidation('revalidateField', 'street');
            newClient.formValidation('revalidateField', 'mpid_fiscal');
            newClient.formValidation('revalidateField', 'postal_code');
            newClient.formValidation('revalidateField', 'email_fiscal');
            if(count_fiscal_state==1){getMunicipios($("#state"),"mpid_fiscal",newClient);count_fiscal_state++}
        }).on('success.field.fv', function(e, data) {
            if (data.field === 'rfc' || data.field === 'business_name' || data.field === 'street' || data.field === 'mpid_fiscal' || data.field === 'postal_code' || data.field === 'email_fiscal') {
                var channel = newClient.find('[name="iva"]:checked').val();
                // User choose given channel
                if (channel !== 'mx') {
                    // Remove the success class from the container
                    data.element.closest('.form-group').removeClass('has-success');
                    // Hide the tick icon
                    data.element.data('fv.icon').hide();
                }
            }
        }).on('success.form.fv', function(e) {
            e.preventDefault();
            $("#message-box-info").toggleClass("open");
            $.ajax({
                type : "POST",
                url : "/dashboard/client/save",
                data :$(this).serialize(),
                dataType : "json",
                success : function(response){
                    if(response.code==200){
                        messages(1);
                        $(".billing").addClass("hide");
                        var channel = $('input[type="radio"][name="iva"]:checked').val();
                        if (channel=='mx'){$("#state").select2("val", "");$("#mpid_fiscal").select2("val", "");}

                        resetForm(newClient);
                        newClient.formValidation('resetForm', true);
                        setTimeout(function(){
                            $("#other_products").toggleClass("open");
                        },1000);
                    }else{
                        messages(2)
                    }
                },
                error : function(){
                    messages(3);
                }
            });
        });
        $("form#newClient input[name='iva']").click(function(){
            var radio = $(this).val();
            if(radio=="mx"){
                $(".billing").removeClass("hide");
            }else{
                $(".billing").addClass("hide");
            }
        });
        getMunicipios($("#state_dir"),"mpid",newClient);
    }

    /*********  Admitir estudiante de admisión  ***********/
    var editClientadmision = $("#editClientadmision");
    if(editClientadmision.length>=1){
        getSpanishD();
        $('.birth_date').datepicker({
            format: 'dd/mm/yyyy',
            language : "es",
            autoclose:true
        });
        $('.date-end-students').datepicker({
            format: 'dd/mm/yyyy',
            language : "es",
            autoclose:true
        });
        editClientadmision.formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            excluded: ':disabled',
            fields: {
                name: {
                    validators: {notEmpty: {}}
                },
                last_name: {
                    validators: {notEmpty: {}}
                },
                sex: {
                    validators: {notEmpty: {}}
                },
                posgrado: {
                    validators: {notEmpty: {}}
                },
                email: {
                    validators: {
                        validators: {notEmpty: {}},
                        emailAddress: {
                            message: 'Este campo no contiene una dirección de correo electrónico válida.'
                        },
                        remote: {
                            message: 'Esta cuenta de correo electrónico ya existe, pruebe con otra',
                            url: '/dashboard/client/validate-email',
                            data: function(validator, $field, value) {
                                return {
                                    email: validator.getFieldElements('email').val(),
                                    validation: validator.getFieldElements('email').attr("data-value")
                                };
                            },
                            type: 'POST'
                        }
                    }
                },
                tel: {
                    validators: {notEmpty: {}}
                }
            }
        }).on('success.form.fv', function(e) {
            console.log("hola");
            e.preventDefault();
            $("#message-box-info").toggleClass("open");
            $.ajax({
                type : "POST",
                url : "/dashboard/client/convertirestudiante",
                data :$(this).serialize(),
                dataType : "json",
                success : function(response){
                    if(response.code==200){
                        messages(1);
                        /*window.location.href = "/dashboard/client/admision";*/
                    }else if(response.code==404){
                        messages(2)
                    }
                },
                error : function(){
                    messages(3);
                }
            });
        });
    }


    /*********  Crear estudiante final  ***********/
    var addStudent = $("#addStudent");
    if(addStudent.length>=1){
        addStudent.formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            excluded: ':disabled',
            fields: {
                name: {
                    validators: {notEmpty: {}}
                },
                last_name: {
                    validators: {notEmpty: {}}
                },
                sex: {
                    validators: {notEmpty: {}}
                },
                email: {
                    validators: {
                        validators: {notEmpty: {}},
                        emailAddress: {
                            message: 'Este campo no contiene una dirección de correo electrónico válida.'
                        },
                        remote: {
                            message: 'Esta cuenta de correo electrónico ya existe, pruebe con otra',
                            url: '/dashboard/client/validate-email',
                            data: function(validator, $field, value) {
                                return {
                                    email: validator.getFieldElements('email').val(),
                                    validation: validator.getFieldElements('email').attr("data-value")
                                };
                            },
                            type: 'POST'
                        }
                    }
                },
                tel: {
                    validators: {notEmpty: {}}
                }
            }
        }).on('success.form.fv', function(e) {
            e.preventDefault();
            $("#message-box-info").toggleClass("open");
            $.ajax({
                type : "POST",
                url : "/dashboard/client/convertirestudiante",
                data :$(this).serialize(),
                dataType : "json",
                success : function(response){
                    if(response.code==200){
                        messages(1);
                        window.location.href = "/dashboard/client/admision";
                    }else if(response.code==404){
                        messages(2)
                    }
                },
                error : function(){
                    messages(3);
                }
            });
        });
    }

    var editClient = $("#editClient");
    if(editClient.length>=1){
        getSpanishD();
        $('.birth_date').datepicker({
            format: 'dd/mm/yyyy',
            language : "es",
            autoclose:true
        }).on('changeDate', function(e) {
            var date = $(".birth_date").val();
            var year = date.split("/");
            var d = new Date();
            var age = d.getFullYear()-year[2];
            $("#age").val(age);
        }).on('clearDate', function(e) {
            $("#age").val("");
        });
        editClient.formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            excluded: ':disabled',
            fields: {
                name: {
                    validators: {notEmpty: {}}
                },
                last_name: {
                    validators: {notEmpty: {}}
                },
                sex: {
                    validators: {notEmpty: {}}
                },
                email: {
                    validators: {
                        validators: {notEmpty: {}},
                        emailAddress: {
                            message: 'Este campo no contiene una dirección de correo electrónico válida.'
                        },
                        remote: {
                            message: 'Esta cuenta de correo electrónico ya existe, pruebe con otra',
                            url: '/dashboard/client/validate-email',
                            data: function(validator, $field, value) {
                                return {
                                    email: validator.getFieldElements('email').val(),
                                    validation: validator.getFieldElements('email').attr("data-value")
                                };
                            },
                            type: 'POST'
                        }
                    }
                },
                tel: {
                    validators: {notEmpty: {}}
                },
                iva: {
                    validators: {notEmpty: {}}
                },
                rfc: {
                    validators: {
                        callback: {
                            message: 'El RFC es un campo necesario.',
                            callback: function(value, validator, $field) {
                                var channel = newClient.find('[name="iva"]:checked').val();
                                return (channel !== 'mx') ? true : (value !== '');
                            }
                        },
                        remote: {
                            message: 'Este RFC ya existe, pruebe con otro',
                            url: '/dashboard/client/validate-rfc',
                            data: function(validator, $field, value) {
                                return {
                                    rfc: validator.getFieldElements('rfc').val(),
                                    validation: validator.getFieldElements('rfc').attr("data-value")
                                };
                            },
                            type: 'POST'
                        }
                    }
                },
                business_name: {
                    validators: {
                        callback: {
                            message: 'La Razón Social es un campo necesario.',
                            callback: function(value, validator, $field) {
                                var channel = newClient.find('[name="iva"]:checked').val();
                                return (channel !== 'mx') ? true : (value !== '');
                            }
                        }
                    }
                },
                street: {
                    validators: {
                        callback: {
                            message: 'La Dirección Fiscal es un campo necesario.',
                            callback: function(value, validator, $field) {
                                var channel = newClient.find('[name="iva"]:checked').val();
                                return (channel !== 'mx') ? true : (value !== '');
                            }
                        }
                    }
                },
                mpid_fiscal: {
                    validators: {
                        callback: {
                            message: 'El municipio es requerido',
                            callback: function(value, validator, $field) {
                                var channel = newClient.find('[name="iva"]:checked').val();
                                return (channel !== 'mx') ? true : (value !== '');
                            }
                        }
                    }
                },
                postal_code: {
                    validators: {
                        callback: {
                            message: 'El Código Postal es un campo necesario.',
                            callback: function(value, validator, $field) {
                                var channel = newClient.find('[name="iva"]:checked').val();
                                return (channel !== 'mx') ? true : (value !== '');
                            }
                        }
                    }
                },
                email_fiscal: {
                    validators: {
                        callback: {
                            message: 'El campo Email Facturación es un campo necesario.',
                            callback: function(value, validator, $field) {
                                var channel = newClient.find('[name="iva"]:checked').val();
                                return (channel !== 'mx') ? true : (value !== '');
                            }
                        }
                    }
                }
            }
        }).on('change', '[name="iva"]', function(e) {
            if($('input[name=iva]:checked', '#editClient').val()=="mx"){
                $(".billing").removeClass("hide");
                $('#editClient').formValidation('enableFieldValidators', 'rfc')
            }if($('input[name=iva]:checked', '#editClient').val()=="et"){
                $(".billing").addClass("hide");
            }
            billing();
        }).on('success.field.fv', function(e, data) {
            if (data.field === 'rfc' || data.field === 'business_name' || data.field === 'street' || data.field === 'mpid_fiscal' || data.field === 'postal_code' || data.field === 'email_fiscal') {
                var channel = newClient.find('[name="iva"]:checked').val();
                // User choose given channel
                if (channel !== 'mx') {
                    // Remove the success class from the container
                    data.element.closest('.form-group').removeClass('has-success');
                    // Hide the tick icon
                    data.element.data('fv.icon').hide();
                }
            }
        }).on('success.form.fv', function(e) {
            e.preventDefault();
            $("#message-box-info").toggleClass("open");
            $.ajax({
                type : "POST",
                url : "/dashboard/client/update",
                data :$(this).serialize(),
                dataType : "json",
                success : function(response){
                    if(response.code==200){
                        messages(1);
                    }else if(response.code==404){
                        messages(2)
                    }
                },
                error : function(){
                    messages(3);
                }
            });
        });
        if($('input[name=iva]:checked', '#editClient').val()=="mx"){
            $(".billing").removeClass("hide");
            $("#mpid_fiscal").parent().removeClass("hidden");
            editClient.formValidation('enableFieldValidators', 'rfc');
            billing();
        }
        getMunicipios($("#state_dir"),"mpid",newClient);
        getMunicipios($("#state"),"mpid_fiscal",newClient);
        function billing(){
            editClient.formValidation('revalidateField', 'rfc');
            editClient.formValidation('revalidateField', 'business_name');
            editClient.formValidation('revalidateField', 'fiscal_direction');
            editClient.formValidation('revalidateField', 'state');
            editClient.formValidation('revalidateField', 'postal_code');
            editClient.formValidation('revalidateField', 'email_fiscal');
        }
    }
    if($("#newInscription").length>=1){
        $("#inscriptionCourse").click(function(){
            $("#modal_inscription").modal("show");
        });
        $('#modal_inscription').on('shown.bs.modal', function() {
            getSection($("#searchUser"),"Identificador o nombre de usuario",$("#ap").val());
        });
        $('#modal_inscription').on('hidden.bs.modal', function() {
            $("#searchUser").select2('val',"");
            $("#searchUser").val('');
        });
        $('#newInscription').formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            excluded: ':disabled',
            fields: {
                searchUser: {
                    validators: {
                        notEmpty: {
                            message: 'Este campo es necesario y no puede estar vacío.'
                        },remote: {
                            message: 'Esta usuario ya esta registrado a este curso, pruebe con otro',
                            url: '/dashboard/process/validate-client',
                            data: {
                                type: 'client'
                            },
                            type: 'POST'
                        }
                    }
                }
            }
        }).on('success.form.fv', function(e) {
            e.preventDefault();
            $("#btn-submit").attr("disabled","disabled").val("Iniciando");
            $("#message-box-info").toggleClass("open");
            $.ajax({
                type : "POST",
                url : "/dashboard/process/save-inscription",
                data : $(this).serialize(),
                dataType : "json",
                success : function(response){
                    if(response.code==200){
                        $("#tbInscription tbody").append("<tr class='danger' id='"+response.content.identifier+"'><td>"+response.content.identifier+"</td><td class='nameClient'>"+response.content.name+" "+response.content.last_name+"</td><td>"+response.content.iva+"</td><td>$"+response.content.paid+"</td><td>$"+response.content.status_payed+"</td><td>"+response.content.date_paid+"</td><td class='optionsCtg'><a  href='/dashboard/annual-program/users/"+response.content.apid+"/manage/"+response.content.clid+"'><span class='fa fa-pencil' style='font-size: 17px;padding: 0%'></span></a></td>").fadeIn(1000);
                        messages(1)
                        setTimeout(function(){
                            $("#modal_inscription").modal("hide");
                        },1500)
                    }else if(response.code==300){
                        alert("Este usuario ya esta registrado en este curso");
                        $("#message-box-info").removeClass("open");
                    }else{
                        messages(2);
                    }
                },
                error : function(){
                    messages(3)
                }
            });
        });
    }
    if($("#adminPayments").length>=1){
        $("#addPayment").click(function(){
            $("#modal_payments").modal("show");
        });
        $('#modal_payments').on('hidden.bs.modal', function() {
            newPayment = $('#newPayment');
            $("#amount").val('');
            newPayment.formValidation('revalidateField', 'amount');
            $("#reason").val('');
            newPayment.formValidation('revalidateField', 'reason');
        });
        $('#newPayment').formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            excluded: ':disabled',
            fields: {
                amount: {
                    validators: {
                        notEmpty: {
                            message: 'El pago es requerido y no puede estar vacío.'
                        },
                        numeric: {
                            message: 'El pago debe de ser un entero.',
                        }
                    }
                },reason: {
                    validators: {
                        notEmpty: {
                            message: 'Este campo es requerido y no puede estar vacío.'
                        }
                    }
                }
            }
        }).on('success.form.fv', function(e) {
            e.preventDefault();
            $("#message-box-info").toggleClass("open");
            $.ajax({
                type : "POST",
                url : "/dashboard/process/save-payment",
                data : $(this).serialize(),
                dataType : "json",
                success : function(response){
                    if(response.code==200){
                        $("#tbPayment tbody#tbodyPayment").prepend("<tr id='"+response.content.pyid+"'><td><strong>Pago curso</strong><p>"+response.content.name_user+"</p></td><td class='text-center'>$"+response.content.amount+"</td><td class='text-center'>"+response.content.reason+"</td><td class='text-center'>"+response.content.date+"</td><td class='text-center'><span class='fa fa-remove fa-2x deletePayment pointer' data-amount='"+response.content.amount+"' data-payment='"+response.content.pyid+"' data-toggle='tooltip' data-placement='top' title='Eliminar pago'></span>&nbsp;<span style='margin-top: 3px' onclick=printPage('"+response.content.url+"') class='fa fa-print fa-2x pointer' data-toggle='tooltip' data-placement='top' title='Imprimir Reporte de pago'></span>&nbsp;<span style='margin-top: 3px' class='fa fa-upload fa-2x pointer upload-file "+response.content.pyid+"' data-url='' data-voucher='' data-pyid='"+response.content.pyid+"' data-toggle='tooltip' data-placement='top' title='Subir comprobante de pago firmado'></span></td></tr>").fadeIn(1000);
                        $("#plus").text(response.content.amount2);
                        $("#owed").text(response.content.owed);
                        messages(1);
                        setTimeout(function(){
                            $("#modal_payments").modal("hide");
                        },1500)
                    }else if(response.code==300){
                        alert("Este usuario ya ha superado el limite de pago");
                        $("#message-box-info").removeClass("open");
                    }else{
                        messages(2);
                    }
                },
                error : function(){
                    messages(3)
                }
            });
        });
        $(document.body).on("click","#tbPayment tbody#tbodyPayment td span.deletePayment",function(e){
            e.preventDefault();
            $this = $(this);
            payment = $this.attr("data-payment");
            amount = $this.attr("data-amount");
            $("#pyid").val(payment);
            $('#deletePaymentModal').modal();
            $("#cgValue").text(amount);
        });
        $('#deletePaymentModal').on('hidden.bs.modal', function() {
            $("#pyid").val('');
        });
        $('#deletePaymentForm').formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            excluded: ':disabled',
            fields: {
                reason_update: {
                    validators: {
                        notEmpty: {
                            message: 'Este campo es requerido y no puede estar vacío.'
                        }
                    }
                }
            }
        }).on('success.form.fv', function(e) {
            e.preventDefault();
            $("#message-box-info").toggleClass("open");
            $.ajax({
                type : "POST",
                url : "/dashboard/process/delete-payment",
                data : $(this).serialize(),
                dataType : "json",
                success : function(response){
                    if(response.code==200){
                        $("#tbPayment tbody#tbodyPayment tr#"+response.content.pyid+"").remove().fadeIn(1000);
                        $("#plus").text(response.content.amount2);
                        $("#owed").text(response.content.owed);
                        messages(1);
                        setTimeout(function(){
                            $("#deletePaymentModal").modal("hide");
                        },1500)
                    }else if(response.code==300){
                        alert("Este usuario ya ha superado el limite de pago");
                        $("#message-box-info").removeClass("open");
                    }else{
                        messages(2);
                    }
                },
                error : function(){
                    messages(3)
                }
            });
        });
        $(document.body).on("click","#tbPayment tbody#tbodyPayment td span.upload-file",function(e){
            e.preventDefault();
            $this = $(this);
            pyid = $this.attr("data-pyid");
            voucher = $this.attr("data-voucher");
            pdf_view = $("a#pdf_view");
            if(voucher){
                url = $this.attr("data-url");
                pdf_view.attr("href",url);
                pdf_view.parent().removeClass("hidden");
            }else{ pdf_view.parent().addClass("hidden"); pdf_view.attr("href","");}

            $("#pdfI").val(voucher);
            $('#upload_payments').modal();
            uploadPDF(".pdfPayment",pyid);
        });
    }

    /* General select2 */
    var select2M = $(".selectM");
    if(select2M.length>=1){
        select2M.select2({
            placeholder: select2M.data("placehorder")
        });
    }
    var select2U = $(".selectU");
    if(select2U.length>=1){
        select2U.select2({
            maximumSelectionLength:1,
            placeholder: select2U.data("placehorder")
        });
    }
    /* End */
});
function resetForm($form) {
    $form.find('input:text, input:password, input:file, input[type=email], select, textarea').val('');
    $form.find('input:radio, input:checkbox').removeAttr('checked').removeAttr('selected');
}
function getMunicipios(selector,name,form_municipios){
    selector.select2({
        maximumSelectionLength:1
    }).on("select2:select", function(response) {
        estado = response.target.value;
        $.ajax({
            data : {estado:estado},
            url : "/dashboard/client/getmunicipios",
            type : "POST",
            dataType : "json",
            success:function(response){
                if(response.code==200){
                    mpid = $("#"+name);
                    mpid.parent().removeClass("hidden");
                    var sel = $("select[name='"+name+"']");
                    $.each(response.result,function(key,value){
                        sel.append($("<option class='options'>").attr('value',value['id']).text(value['value']));
                    });
                    mpid.select2({
                        maximumSelectionLength:1
                    }).focus();
                }
            }
        });
    }).on("select2:unselect",function(){
        mpid = $("#"+name);
        mpid.select2('val',"");
        form_municipios.formValidation('revalidateField',name);
        mpid.find(".options").remove();
    });
    eid_estados = $("#eid_estados");
    if(eid_estados.length>=1 && eid_estados.data('value')==1){
        $("#"+name).select2({
            maximumSelectionLength:1
        }).focus();
    }
    eid_estados2 = $("#eid_estados2");
    if(eid_estados2.length>=1 && eid_estados2.data('value')==1){
        $("#"+name).select2({
            maximumSelectionLength:1
        }).focus();
    }
    return false;
}
function getSpanishD(){
    (function($){
        $.fn.datepicker.dates['es'] = {
            days: ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo"],
            daysShort: ["Dom", "Lun", "Mar", "Mié", "Jue", "Vie", "Sáb", "Dom"],
            daysMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa", "Do"],
            months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
            monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
            today: "Hoy",
            clear: "Borrar",
            weekStart: 1,
            format: "dd/mm/yyyy"
        };
    }(jQuery));
}
/* End Clients */
function messages($type){
    if($type==1){
        setTimeout(function(){
            $("#message-box-info").removeClass("open");
            $("#message-box-success").toggleClass("open");
        },1500);
        setTimeout(function(){
            $("#message-box-success").removeClass("open");
        },2000);
    }
    else if($type==2){
        setTimeout(function(){
            $("#message-box-info").removeClass("open");
            $("#message-box-warning").toggleClass("open");
        },1500);
        setTimeout(function(){
            $("#message-box-warning").removeClass("open");
        },3000);
    }else if($type==3){
        setTimeout(function(){
            $("#message-box-info").removeClass("open");
            $("#message-box-warning").toggleClass("open");
        },1500);
        setTimeout(function(){
            $("#message-box-warning").removeClass("open");
        },3000);
    }
}