$(document).ready(function () {
    $(".block-user").on("click", function(){
      Swal.fire({
        position: 'top-end',
        type: 'success',
        title: 'DEUDA',
        text: 'Comunicate al 5579131372 para regularizar tu situación',
        showConfirmButton: false,
        timer: 4000
      }).then((result)=>{
          window.location.replace("/dashboard/recibos/index");
      });
    });
    $("#paypal-notification").on("click", function(){
        Swal.fire({
          position: 'top-end',
          type: 'success',
          title: 'Usted será guiado a otra pestaña para procesar el pago',
          text: 'Recuerde contemplar la comisión del 5% del monto a depositar + $4.00, ejemplo: Monto a pagar $1,000.00, usted debe colocar $1,054.00',
          showConfirmButton: false,
          timer: 9500
        }).then((result)=>{
            window.location.replace("https://paypal.me/ideiberoamerica?locale.x=es_XC");
        });

    });
    $("#concepto").on("change",function(){
      if($(this).val()=="#"){
        $("#file-recibo").addClass("lock-div-file");
      }
      else{
        $("#file-recibo").removeClass("lock-div-file");
      }
    })

    if($("#recibo").length>=1){
      Dropzone.autoDiscover = false;
      var myDropzone = new Dropzone('#recibo', {
          url: "/dashboard/index/uploaddocumentsrecibo",
          uploadMultiple : false,
          paramName: "file", // The name that will be used to transfer the file
          maxFilesize: 15, // MB
          maxFiles: 1,
          parallelUploads : 1,
          addRemoveLinks : true,
          dictResponseError: "No se puede subir esta archivo!",
          autoProcessQueue: true,
          thumbnailWidth: 138,
          thumbnailHeight: 120,
          previewTemplate: '<div class="dz-preview dz-file-preview"><div class="dz-details"><div class="dz-filename"><span data-dz-name style:"font-weight: bold;"></span></div><div class="dz-size">File size: <span data-dz-size></span></div><div class="dz-thumbnail-wrapper"><div class="dz-thumbnail"><img data-dz-thumbnail><span class="dz-nopreview">No preview</span><div class="dz-success-mark"><i class="fa fa-check-circle-o"></i></div><div class="dz-error-mark"><i class="fas fa-times-circle"></i></div><div class="dz-error-message"><span data-dz-errormessage></span></div></div></div></div><div class="progress progress-striped active"><div class="progress-bar progress-bar-success" data-dz-uploadprogress></div></div></div>',

          sending : function(file, xhr, formData){
            formData.append("type",$("#recibo").attr("data-id"))
          },
          success: function(file, response){
            var values= response;
            $(".dz-preview").addClass("dz-success");
            $("div.progress").remove();
            $("#input-recibo-receipt").val(values.document); 
            Swal.fire({
              position: 'top-end',
              type: 'success',
              title: 'Recibo guardado, ¡Gracias!',
              showConfirmButton: true
            });
          },
          removedfile: function(file) {
                id = file.previewElement.accessKey;
                name = file.previewElement.id;
                $.ajax({
                    url: "/dashboard/index/deletedrecibo",
                    type: "post",
                    data: {name:name},
                    dataType:"json",
                    success : function(response){
                        if(response.code==200){
                            $(file.previewElement).remove();
                            $("#"+id).remove();
                        }else{
                            $(file.previewElement).remove();
                            $("#"+id).remove();
                        }
                    },
                    error : function(){
                        alert("Ha ocurrido un error");
                    }
                });
          }
          /*removedfile: function(file) {
              $(file.previewElement).remove();
          }*/
      });
      $('.deleteDocument').each(function(i,v){
          $(this).on("click",function(){
              if($(this).attr("data-docid")){
                $('.table tbody tr').eq(i).find("td").eq(1).text("No");
                $(this).parent().parent().addClass('hidden');
                $.ajax({
                  url:"/extrasoperations",
                  type:"post",
                  data:{deletedocument:$(this).attr("data-docid")},
                  dataType:"json",
                  success:function(resp){
                    $(this).removeAttr("data-docid");
                    $('select[name=documentload]').html(resp.html);
                  }
                });
              }else{
                console.log("no tiene ese atributo")
              }
          })
      });
      function selecttable(archivo,doctid){
          var num_trs = $('.table tbody tr').length;
          for(var i=0;i<num_trs;i++){
            name_trs = $('.table tbody tr').eq(i).find("td").eq(0).text();
            if(archivo.toLowerCase()==name_trs.toLowerCase()){
              $('.table tbody tr').eq(i).find("td").eq(1).text("Si");
              $('.table tbody tr.'+archivo).removeClass("hidden");
              $('.deleteDocument').eq(i).attr("data-docid",doctid);
            }
          }
      }
   }


   if($("#acta").length>=1){
      Dropzone.autoDiscover = false;
      var myDropzone = new Dropzone('#acta', {
              url: "/dashboard/index/uploaddocumentsrecibo",
              uploadMultiple : false,
              paramName: "file", // The name that will be used to transfer the file
              maxFilesize: 15, // MB
              maxFiles: 1,
              parallelUploads : 1,
              addRemoveLinks : true,
              dictResponseError: "No se puede subir esta archivo!",
              autoProcessQueue: true,
              thumbnailWidth: 138,
              thumbnailHeight: 120,
              previewTemplate: '<div class="dz-preview dz-file-preview"><div class="dz-details"><div class="dz-filename"><span data-dz-name style:"font-weight: bold;"></span></div><div class="dz-size">File size: <span data-dz-size></span></div><div class="dz-thumbnail-wrapper"><div class="dz-thumbnail"><img data-dz-thumbnail><span class="dz-nopreview">No preview</span><div class="dz-success-mark"><i class="fa fa-check-circle-o"></i></div><div class="dz-error-mark"><i class="fas fa-times-circle"></i></div><div class="dz-error-message"><span data-dz-errormessage></span></div></div></div></div><div class="progress progress-striped active"><div class="progress-bar progress-bar-success" data-dz-uploadprogress></div></div></div>',

              sending : function(file, xhr, formData){
                formData.append("type",$("#acta").data("id"))
              },
              success: function(file, response){
                var values= response;
                //$("#recibo").remove();
                $(".dz-preview").addClass("dz-success");
                $("div.progress").remove();
                $("#input-acta-receipt").val(values.document);
                Swal.fire({
                  position: 'top-end',
                  type: 'success',
                  title: 'Acta guardada, ¡Gracias!',
                  showConfirmButton: true
                });
              },
              removedfile: function(file) {
                    id = file.previewElement.accessKey;
                    name = file.previewElement.id;
                    $.ajax({
                        url: "/dashboard/index/deletedrecibo",
                        type: "post",
                        data: {name:name},
                        dataType:"json",
                        success : function(response){
                            if(response.code==200){
                                $(file.previewElement).remove();
                                $("#"+id).remove();
                            }else{
                                $(file.previewElement).remove();
                                $("#"+id).remove();
                            }
                        },
                        error : function(){
                            alert("Ha ocurrido un error");
                        }
                    });
              }
              /*removedfile: function(file) {
                  $(file.previewElement).remove();
              }*/
          });
          $('.deleteDocument').each(function(i,v){
              $(this).on("click",function(){
                  if($(this).attr("data-docid")){
                    $('.table tbody tr').eq(i).find("td").eq(1).text("No");
                    $(this).parent().parent().addClass('hidden');
                    $.ajax({
                      url:"/extrasoperations",
                      type:"post",
                      data:{deletedocument:$(this).attr("data-docid")},
                      dataType:"json",
                      success:function(resp){
                        $(this).removeAttr("data-docid");
                        $('select[name=documentload]').html(resp.html);
                      }
                    });
                  }else{
                    console.log("no tiene ese atributo")
                  }
              })
          });
          function selecttable(archivo,doctid){
              var num_trs = $('.table tbody tr').length;
              for(var i=0;i<num_trs;i++){
                name_trs = $('.table tbody tr').eq(i).find("td").eq(0).text();
                if(archivo.toLowerCase()==name_trs.toLowerCase()){
                  $('.table tbody tr').eq(i).find("td").eq(1).text("Si");
                  $('.table tbody tr.'+archivo).removeClass("hidden");
                  $('.deleteDocument').eq(i).attr("data-docid",doctid);
                }
              }
          }
   }
   if($("#titulo").length>=1){
      Dropzone.autoDiscover = false;
      var myDropzone = new Dropzone('#titulo', {
              url: "/dashboard/index/uploaddocumentsrecibo",
              uploadMultiple : false,
              paramName: "file", // The name that will be used to transfer the file
              maxFilesize: 15, // MB
              maxFiles: 1,
              parallelUploads : 1,
              addRemoveLinks : true,
              dictResponseError: "No se puede subir esta archivo!",
              autoProcessQueue: true,
              thumbnailWidth: 138,
              thumbnailHeight: 120,
              previewTemplate: '<div class="dz-preview dz-file-preview"><div class="dz-details"><div class="dz-filename"><span data-dz-name style:"font-weight: bold;"></span></div><div class="dz-size">File size: <span data-dz-size></span></div><div class="dz-thumbnail-wrapper"><div class="dz-thumbnail"><img data-dz-thumbnail><span class="dz-nopreview">No preview</span><div class="dz-success-mark"><i class="fa fa-check-circle-o"></i></div><div class="dz-error-mark"><i class="fas fa-times-circle"></i></div><div class="dz-error-message"><span data-dz-errormessage></span></div></div></div></div><div class="progress progress-striped active"><div class="progress-bar progress-bar-success" data-dz-uploadprogress></div></div></div>',

              sending : function(file, xhr, formData){
                formData.append("type",$("#titulo").data("id"))
              },
              success: function(file, response){
                var values= response;
                //$("#recibo").remove();
                $(".dz-preview").addClass("dz-success");
                $("div.progress").remove();
                $("#input-titulo-receipt").val(values.document);
                Swal.fire({
                  position: 'top-end',
                  type: 'success',
                  title: 'Titulo guardado, ¡Gracias!',
                  showConfirmButton: true
                });
              },
              removedfile: function(file) {
                    id = file.previewElement.accessKey;
                    name = file.previewElement.id;
                    $.ajax({
                        url: "/dashboard/index/deletedrecibo",
                        type: "post",
                        data: {name:name},
                        dataType:"json",
                        success : function(response){
                            if(response.code==200){
                                $(file.previewElement).remove();
                                $("#"+id).remove();
                            }else{
                                $(file.previewElement).remove();
                                $("#"+id).remove();
                            }
                        },
                        error : function(){
                            alert("Ha ocurrido un error");
                        }
                    });
              }
              /*removedfile: function(file) {
                  $(file.previewElement).remove();
              }*/
          });
          $('.deleteDocument').each(function(i,v){
              $(this).on("click",function(){
                  if($(this).attr("data-docid")){
                    $('.table tbody tr').eq(i).find("td").eq(1).text("No");
                    $(this).parent().parent().addClass('hidden');
                    $.ajax({
                      url:"/extrasoperations",
                      type:"post",
                      data:{deletedocument:$(this).attr("data-docid")},
                      dataType:"json",
                      success:function(resp){
                        $(this).removeAttr("data-docid");
                        $('select[name=documentload]').html(resp.html);
                      }
                    });
                  }else{
                    console.log("no tiene ese atributo")
                  }
              })
          });
          function selecttable(archivo,doctid){
              var num_trs = $('.table tbody tr').length;
              for(var i=0;i<num_trs;i++){
                name_trs = $('.table tbody tr').eq(i).find("td").eq(0).text();
                if(archivo.toLowerCase()==name_trs.toLowerCase()){
                  $('.table tbody tr').eq(i).find("td").eq(1).text("Si");
                  $('.table tbody tr.'+archivo).removeClass("hidden");
                  $('.deleteDocument').eq(i).attr("data-docid",doctid);
                }
              }
          }
   }
   if($("#identificacion").length>=1){
      Dropzone.autoDiscover = false;
      var myDropzone = new Dropzone('#identificacion', {
              url: "/dashboard/index/uploaddocumentsrecibo",
              uploadMultiple : false,
              paramName: "file", // The name that will be used to transfer the file
              maxFilesize: 15, // MB
              maxFiles: 1,
              parallelUploads : 1,
              addRemoveLinks : true,
              dictResponseError: "No se puede subir esta archivo!",
              autoProcessQueue: true,
              thumbnailWidth: 138,
              thumbnailHeight: 120,
              previewTemplate: '<div class="dz-preview dz-file-preview"><div class="dz-details"><div class="dz-filename"><span data-dz-name style:"font-weight: bold;"></span></div><div class="dz-size">File size: <span data-dz-size></span></div><div class="dz-thumbnail-wrapper"><div class="dz-thumbnail"><img data-dz-thumbnail><span class="dz-nopreview">No preview</span><div class="dz-success-mark"><i class="fa fa-check-circle-o"></i></div><div class="dz-error-mark"><i class="fas fa-times-circle"></i></div><div class="dz-error-message"><span data-dz-errormessage></span></div></div></div></div><div class="progress progress-striped active"><div class="progress-bar progress-bar-success" data-dz-uploadprogress></div></div></div>',

              sending : function(file, xhr, formData){
                formData.append("type",$("#identificacion").data("id"))
              },
              success: function(file, response){
                var values= response;
                //$("#recibo").remove();
                $(".dz-preview").addClass("dz-success");
                $("div.progress").remove();
                $("#input-identificacion-receipt").val(values.document);
                Swal.fire({
                  position: 'top-end',
                  type: 'success',
                  title: 'Identificación guardada, ¡Gracias!',
                  showConfirmButton: true
                });
              },
              removedfile: function(file) {
                    id = file.previewElement.accessKey;
                    name = file.previewElement.id;
                    $.ajax({
                        url: "/dashboard/index/deletedrecibo",
                        type: "post",
                        data: {name:name},
                        dataType:"json",
                        success : function(response){
                            if(response.code==200){
                                $(file.previewElement).remove();
                                $("#"+id).remove();
                            }else{
                                $(file.previewElement).remove();
                                $("#"+id).remove();
                            }
                        },
                        error : function(){
                            alert("Ha ocurrido un error");
                        }
                    });
              }
              /*removedfile: function(file) {
                  $(file.previewElement).remove();
              }*/
          });
          $('.deleteDocument').each(function(i,v){
              $(this).on("click",function(){
                  if($(this).attr("data-docid")){
                    $('.table tbody tr').eq(i).find("td").eq(1).text("No");
                    $(this).parent().parent().addClass('hidden');
                    $.ajax({
                      url:"/extrasoperations",
                      type:"post",
                      data:{deletedocument:$(this).attr("data-docid")},
                      dataType:"json",
                      success:function(resp){
                        $(this).removeAttr("data-docid");
                        $('select[name=documentload]').html(resp.html);
                      }
                    });
                  }else{
                    console.log("no tiene ese atributo")
                  }
              })
          });
          function selecttable(archivo,doctid){
              var num_trs = $('.table tbody tr').length;
              for(var i=0;i<num_trs;i++){
                name_trs = $('.table tbody tr').eq(i).find("td").eq(0).text();
                if(archivo.toLowerCase()==name_trs.toLowerCase()){
                  $('.table tbody tr').eq(i).find("td").eq(1).text("Si");
                  $('.table tbody tr.'+archivo).removeClass("hidden");
                  $('.deleteDocument').eq(i).attr("data-docid",doctid);
                }
              }
          }
   }
   if($("#curriculum").length>=1){
      Dropzone.autoDiscover = false;
      var myDropzone = new Dropzone('#curriculum', {
              url: "/dashboard/index/uploaddocumentsrecibo",
              uploadMultiple : false,
              paramName: "file", // The name that will be used to transfer the file
              maxFilesize: 15, // MB
              maxFiles: 1,
              parallelUploads : 1,
              addRemoveLinks : true,
              dictResponseError: "No se puede subir esta archivo!",
              autoProcessQueue: true,
              thumbnailWidth: 138,
              thumbnailHeight: 120,
              previewTemplate: '<div class="dz-preview dz-file-preview"><div class="dz-details"><div class="dz-filename"><span data-dz-name style:"font-weight: bold;"></span></div><div class="dz-size">File size: <span data-dz-size></span></div><div class="dz-thumbnail-wrapper"><div class="dz-thumbnail"><img data-dz-thumbnail><span class="dz-nopreview">No preview</span><div class="dz-success-mark"><i class="fa fa-check-circle-o"></i></div><div class="dz-error-mark"><i class="fas fa-times-circle"></i></div><div class="dz-error-message"><span data-dz-errormessage></span></div></div></div></div><div class="progress progress-striped active"><div class="progress-bar progress-bar-success" data-dz-uploadprogress></div></div></div>',

              sending : function(file, xhr, formData){
                formData.append("type",$("#curriculum").data("id"))
              },
              success: function(file, response){
                var values= response;
                //$("#recibo").remove();
                $(".dz-preview").addClass("dz-success");
                $("div.progress").remove();
                $("#input-curriculum-receipt").val(values.document);
                Swal.fire({
                  position: 'top-end',
                  type: 'success',
                  title: 'Curriculum guardada, ¡Gracias!',
                  showConfirmButton: true
                });
              },
              removedfile: function(file) {
                    id = file.previewElement.accessKey;
                    name = file.previewElement.id;
                    $.ajax({
                        url: "/dashboard/index/deletedrecibo",
                        type: "post",
                        data: {name:name},
                        dataType:"json",
                        success : function(response){
                            if(response.code==200){
                                $(file.previewElement).remove();
                                $("#"+id).remove();
                            }else{
                                $(file.previewElement).remove();
                                $("#"+id).remove();
                            }
                        },
                        error : function(){
                            alert("Ha ocurrido un error");
                        }
                    });
              }
              /*removedfile: function(file) {
                  $(file.previewElement).remove();
              }*/
          });
          $('.deleteDocument').each(function(i,v){
              $(this).on("click",function(){
                  if($(this).attr("data-docid")){
                    $('.table tbody tr').eq(i).find("td").eq(1).text("No");
                    $(this).parent().parent().addClass('hidden');
                    $.ajax({
                      url:"/extrasoperations",
                      type:"post",
                      data:{deletedocument:$(this).attr("data-docid")},
                      dataType:"json",
                      success:function(resp){
                        $(this).removeAttr("data-docid");
                        $('select[name=documentload]').html(resp.html);
                      }
                    });
                  }else{
                    console.log("no tiene ese atributo")
                  }
              })
          });
          function selecttable(archivo,doctid){
              var num_trs = $('.table tbody tr').length;
              for(var i=0;i<num_trs;i++){
                name_trs = $('.table tbody tr').eq(i).find("td").eq(0).text();
                if(archivo.toLowerCase()==name_trs.toLowerCase()){
                  $('.table tbody tr').eq(i).find("td").eq(1).text("Si");
                  $('.table tbody tr.'+archivo).removeClass("hidden");
                  $('.deleteDocument').eq(i).attr("data-docid",doctid);
                }
              }
          }
   }


   if($("#curp").length>=1){
      Dropzone.autoDiscover = false;
      var myDropzone = new Dropzone('#curp', {
              url: "/dashboard/index/uploaddocumentsrecibo",
              uploadMultiple : false,
              paramName: "file", // The name that will be used to transfer the file
              maxFilesize: 15, // MB
              maxFiles: 1,
              parallelUploads : 1,
              addRemoveLinks : true,
              dictResponseError: "No se puede subir esta archivo!",
              autoProcessQueue: true,
              thumbnailWidth: 138,
              thumbnailHeight: 120,
              previewTemplate: '<div class="dz-preview dz-file-preview"><div class="dz-details"><div class="dz-filename"><span data-dz-name style:"font-weight: bold;"></span></div><div class="dz-size">File size: <span data-dz-size></span></div><div class="dz-thumbnail-wrapper"><div class="dz-thumbnail"><img data-dz-thumbnail><span class="dz-nopreview">No preview</span><div class="dz-success-mark"><i class="fa fa-check-circle-o"></i></div><div class="dz-error-mark"><i class="fas fa-times-circle"></i></div><div class="dz-error-message"><span data-dz-errormessage></span></div></div></div></div><div class="progress progress-striped active"><div class="progress-bar progress-bar-success" data-dz-uploadprogress></div></div></div>',

              sending : function(file, xhr, formData){
                formData.append("type",$("#curp").data("id"))
              },
              success: function(file, response){
                var values= response;
                //$("#recibo").remove();
                $(".dz-preview").addClass("dz-success");
                $("div.progress").remove();
                $("#input-curp-receipt").val(values.document);
                Swal.fire({
                  position: 'top-end',
                  type: 'success',
                  title: 'CURP guardada, ¡Gracias!',
                  showConfirmButton: true
                });
              },
              removedfile: function(file) {
                    id = file.previewElement.accessKey;
                    name = file.previewElement.id;
                    $.ajax({
                        url: "/dashboard/index/deletedrecibo",
                        type: "post",
                        data: {name:name},
                        dataType:"json",
                        success : function(response){
                            if(response.code==200){
                                $(file.previewElement).remove();
                                $("#"+id).remove();
                            }else{
                                $(file.previewElement).remove();
                                $("#"+id).remove();
                            }
                        },
                        error : function(){
                            alert("Ha ocurrido un error");
                        }
                    });
              }
              /*removedfile: function(file) {
                  $(file.previewElement).remove();
              }*/
          });
          $('.deleteDocument').each(function(i,v){
              $(this).on("click",function(){
                  if($(this).attr("data-docid")){
                    $('.table tbody tr').eq(i).find("td").eq(1).text("No");
                    $(this).parent().parent().addClass('hidden');
                    $.ajax({
                      url:"/extrasoperations",
                      type:"post",
                      data:{deletedocument:$(this).attr("data-docid")},
                      dataType:"json",
                      success:function(resp){
                        $(this).removeAttr("data-docid");
                        $('select[name=documentload]').html(resp.html);
                      }
                    });
                  }else{
                    console.log("no tiene ese atributo")
                  }
              })
          });
          function selecttable(archivo,doctid){
              var num_trs = $('.table tbody tr').length;
              for(var i=0;i<num_trs;i++){
                name_trs = $('.table tbody tr').eq(i).find("td").eq(0).text();
                if(archivo.toLowerCase()==name_trs.toLowerCase()){
                  $('.table tbody tr').eq(i).find("td").eq(1).text("Si");
                  $('.table tbody tr.'+archivo).removeClass("hidden");
                  $('.deleteDocument').eq(i).attr("data-docid",doctid);
                }
              }
          }
   }

   if($("#certificado").length>=1){
      Dropzone.autoDiscover = false;
      var myDropzone = new Dropzone('#certificado', {
              url: "/dashboard/index/uploaddocumentsrecibo",
              uploadMultiple : false,
              paramName: "file", // The name that will be used to transfer the file
              maxFilesize: 15, // MB
              maxFiles: 1,
              parallelUploads : 1,
              addRemoveLinks : true,
              dictResponseError: "No se puede subir esta archivo!",
              autoProcessQueue: true,
              thumbnailWidth: 138,
              thumbnailHeight: 120,
              previewTemplate: '<div class="dz-preview dz-file-preview"><div class="dz-details"><div class="dz-filename"><span data-dz-name style:"font-weight: bold;"></span></div><div class="dz-size">File size: <span data-dz-size></span></div><div class="dz-thumbnail-wrapper"><div class="dz-thumbnail"><img data-dz-thumbnail><span class="dz-nopreview">No preview</span><div class="dz-success-mark"><i class="fa fa-check-circle-o"></i></div><div class="dz-error-mark"><i class="fas fa-times-circle"></i></div><div class="dz-error-message"><span data-dz-errormessage></span></div></div></div></div><div class="progress progress-striped active"><div class="progress-bar progress-bar-success" data-dz-uploadprogress></div></div></div>',

              sending : function(file, xhr, formData){
                formData.append("type",$("#certificado").data("id"))
              },
              success: function(file, response){
                var values= response;
                //$("#recibo").remove();
                $(".dz-preview").addClass("dz-success");
                $("div.progress").remove();
                $("#input-certificado-receipt").val(values.document);
                Swal.fire({
                  position: 'top-end',
                  type: 'success',
                  title: 'Certificado guardada, ¡Gracias!',
                  showConfirmButton: true
                });
              },
              removedfile: function(file) {
                    id = file.previewElement.accessKey;
                    name = file.previewElement.id;
                    $.ajax({
                        url: "/dashboard/index/deletedrecibo",
                        type: "post",
                        data: {name:name},
                        dataType:"json",
                        success : function(response){
                            if(response.code==200){
                                $(file.previewElement).remove();
                                $("#"+id).remove();
                            }else{
                                $(file.previewElement).remove();
                                $("#"+id).remove();
                            }
                        },
                        error : function(){
                            alert("Ha ocurrido un error");
                        }
                    });
              }
              /*removedfile: function(file) {
                  $(file.previewElement).remove();
              }*/
          });
          $('.deleteDocument').each(function(i,v){
              $(this).on("click",function(){
                  if($(this).attr("data-docid")){
                    $('.table tbody tr').eq(i).find("td").eq(1).text("No");
                    $(this).parent().parent().addClass('hidden');
                    $.ajax({
                      url:"/extrasoperations",
                      type:"post",
                      data:{deletedocument:$(this).attr("data-docid")},
                      dataType:"json",
                      success:function(resp){
                        $(this).removeAttr("data-docid");
                        $('select[name=documentload]').html(resp.html);
                      }
                    });
                  }else{
                    console.log("no tiene ese atributo")
                  }
              })
          });
          function selecttable(archivo,doctid){
              var num_trs = $('.table tbody tr').length;
              for(var i=0;i<num_trs;i++){
                name_trs = $('.table tbody tr').eq(i).find("td").eq(0).text();
                if(archivo.toLowerCase()==name_trs.toLowerCase()){
                  $('.table tbody tr').eq(i).find("td").eq(1).text("Si");
                  $('.table tbody tr.'+archivo).removeClass("hidden");
                  $('.deleteDocument').eq(i).attr("data-docid",doctid);
                }
              }
          }
   }
   if($("#cedula").length>=1){
      Dropzone.autoDiscover = false;
      var myDropzone = new Dropzone('#cedula', {
              url: "/dashboard/index/uploaddocumentsrecibo",
              uploadMultiple : false,
              paramName: "file", // The name that will be used to transfer the file
              maxFilesize: 15, // MB
              maxFiles: 1,
              parallelUploads : 1,
              addRemoveLinks : true,
              dictResponseError: "No se puede subir esta archivo!",
              autoProcessQueue: true,
              thumbnailWidth: 138,
              thumbnailHeight: 120,
              previewTemplate: '<div class="dz-preview dz-file-preview"><div class="dz-details"><div class="dz-filename"><span data-dz-name style:"font-weight: bold;"></span></div><div class="dz-size">File size: <span data-dz-size></span></div><div class="dz-thumbnail-wrapper"><div class="dz-thumbnail"><img data-dz-thumbnail><span class="dz-nopreview">No preview</span><div class="dz-success-mark"><i class="fa fa-check-circle-o"></i></div><div class="dz-error-mark"><i class="fas fa-times-circle"></i></div><div class="dz-error-message"><span data-dz-errormessage></span></div></div></div></div><div class="progress progress-striped active"><div class="progress-bar progress-bar-success" data-dz-uploadprogress></div></div></div>',

              sending : function(file, xhr, formData){
                formData.append("type",$("#cedula").data("id"))
              },
              success: function(file, response){
                var values= response;
                //$("#recibo").remove();
                $(".dz-preview").addClass("dz-success");
                $("div.progress").remove();
                $("#input-cedula-receipt").val(values.document);
                Swal.fire({
                  position: 'top-end',
                  type: 'success',
                  title: 'Cedula guardada, ¡Gracias!',
                  showConfirmButton: true
                });
              },
              removedfile: function(file) {
                    id = file.previewElement.accessKey;
                    name = file.previewElement.id;
                    $.ajax({
                        url: "/dashboard/index/deletedrecibo",
                        type: "post",
                        data: {name:name},
                        dataType:"json",
                        success : function(response){
                            if(response.code==200){
                                $(file.previewElement).remove();
                                $("#"+id).remove();
                            }else{
                                $(file.previewElement).remove();
                                $("#"+id).remove();
                            }
                        },
                        error : function(){
                            alert("Ha ocurrido un error");
                        }
                    });
              }
              /*removedfile: function(file) {
                  $(file.previewElement).remove();
              }*/
          });
          $('.deleteDocument').each(function(i,v){
              $(this).on("click",function(){
                  if($(this).attr("data-docid")){
                    $('.table tbody tr').eq(i).find("td").eq(1).text("No");
                    $(this).parent().parent().addClass('hidden');
                    $.ajax({
                      url:"/extrasoperations",
                      type:"post",
                      data:{deletedocument:$(this).attr("data-docid")},
                      dataType:"json",
                      success:function(resp){
                        $(this).removeAttr("data-docid");
                        $('select[name=documentload]').html(resp.html);
                      }
                    });
                  }else{
                    console.log("no tiene ese atributo")
                  }
              })
          });
          function selecttable(archivo,doctid){
              var num_trs = $('.table tbody tr').length;
              for(var i=0;i<num_trs;i++){
                name_trs = $('.table tbody tr').eq(i).find("td").eq(0).text();
                if(archivo.toLowerCase()==name_trs.toLowerCase()){
                  $('.table tbody tr').eq(i).find("td").eq(1).text("Si");
                  $('.table tbody tr.'+archivo).removeClass("hidden");
                  $('.deleteDocument').eq(i).attr("data-docid",doctid);
                }
              }
          }
   }

    if($("#photo").length>=1){
      Dropzone.autoDiscover = false;
      var myDropzone = new Dropzone('#photo', {
              url: "/dashboard/index/uploaddocumentsrecibo",
              uploadMultiple : false,
              paramName: "file", // The name that will be used to transfer the file
              maxFilesize: 15, // MB
              maxFiles: 1,
              parallelUploads : 1,
              addRemoveLinks : true,
              dictResponseError: "No se puede subir esta archivo!",
              autoProcessQueue: true,
              thumbnailWidth: 138,
              thumbnailHeight: 120,
              previewTemplate: '<div class="dz-preview dz-file-preview"><div class="dz-details"><div class="dz-filename"><span data-dz-name style:"font-weight: bold;"></span></div><div class="dz-size">File size: <span data-dz-size></span></div><div class="dz-thumbnail-wrapper"><div class="dz-thumbnail"><img data-dz-thumbnail><span class="dz-nopreview">No preview</span><div class="dz-success-mark"><i class="fa fa-check-circle-o"></i></div><div class="dz-error-mark"><i class="fas fa-times-circle"></i></div><div class="dz-error-message"><span data-dz-errormessage></span></div></div></div></div><div class="progress progress-striped active"><div class="progress-bar progress-bar-success" data-dz-uploadprogress></div></div></div>',

              sending : function(file, xhr, formData){
                formData.append("type",$("#photo").data("id"))
              },
              success: function(file, response){
                var values= response;
                //$("#recibo").remove();
                $(".dz-preview").addClass("dz-success");
                $("div.progress").remove();
                $("#input-photo-receipt").val(values.document);
                Swal.fire({
                  position: 'top-end',
                  type: 'success',
                  title: 'Foto guardada, ¡Gracias!',
                  showConfirmButton: true
                });
              },
              removedfile: function(file) {
                    id = file.previewElement.accessKey;
                    name = file.previewElement.id;
                    $.ajax({
                        url: "/dashboard/index/deletedrecibo",
                        type: "post",
                        data: {name:name},
                        dataType:"json",
                        success : function(response){
                            if(response.code==200){
                                $(file.previewElement).remove();
                                $("#"+id).remove();
                            }else{
                                $(file.previewElement).remove();
                                $("#"+id).remove();
                            }
                        },
                        error : function(){
                            alert("Ha ocurrido un error");
                        }
                    });
              }
              /*removedfile: function(file) {
                  $(file.previewElement).remove();
              }*/
          });
          $('.deleteDocument').each(function(i,v){
              $(this).on("click",function(){
                  if($(this).attr("data-docid")){
                    $('.table tbody tr').eq(i).find("td").eq(1).text("No");
                    $(this).parent().parent().addClass('hidden');
                    $.ajax({
                      url:"/extrasoperations",
                      type:"post",
                      data:{deletedocument:$(this).attr("data-docid")},
                      dataType:"json",
                      success:function(resp){
                        $(this).removeAttr("data-docid");
                        $('select[name=documentload]').html(resp.html);
                      }
                    });
                  }else{
                    console.log("no tiene ese atributo")
                  }
              })
          });
          function selecttable(archivo,doctid){
              var num_trs = $('.table tbody tr').length;
              for(var i=0;i<num_trs;i++){
                name_trs = $('.table tbody tr').eq(i).find("td").eq(0).text();
                if(archivo.toLowerCase()==name_trs.toLowerCase()){
                  $('.table tbody tr').eq(i).find("td").eq(1).text("Si");
                  $('.table tbody tr.'+archivo).removeClass("hidden");
                  $('.deleteDocument').eq(i).attr("data-docid",doctid);
                }
              }
          }
   }




   $("#mostrarrecibo").on("click",function(e){
    var email = $("#emailrecibo").val();
      swal({
        title: 'Buscando estatus de proceso de inscripción del correo: '+email,
        showConfirmButton: false
      });
      e.preventDefault();
      $.ajax({
        url:"/dashboard/index/validaterecibo",
        type:"post",
        data:{email:email},
        dataType:"json",
        success:function(resp){
          code = resp.code;
            switch (code){
              case 200:

                $("#recibo").removeClass("hidden");
                $("#emailrecibo").remove();
                $("#emailvalidate").val(email);


                swal({
                  title: 'Felicidades carga tu documento',
                  text: "a un lado aparecera un cuadro para cargar recibo da click",
                  showConfirmButton: false,
                  timer: 8000
                }).then((result)=>{
                  if(result.value){
                    window.location.replace("/cuenta");
                  }
                });


                break;
                case 500:
                  swal({
                    title: 'Ya cargaste tu recibo',
                    text: "tu recibo esta siendo validado.",
                    showConfirmButton: false,
                    timer: 8000
                  });
  
  
                  break;
              case 404:
                swal({
                  title: 'Sigue siendo evaluado tu inscripción',
                  showConfirmButton: false,
                  timer: 8000
                }).then((result)=>{
                  if(result.value){
                    window.location.replace("/cuenta");
                  }
                });
              break;
          }
        },
        error : function(response){
            code = response.code;
            /*console.log("error");*/
            swal({
              title: 'Sin registro',
              text: 'no existe registro con su correo',
              showConfirmButton: false,
              timer: 8000
            });
        },
    });
  });

   

 if($('#pago-paypal').length>=1){
      $('#pago-paypal').formValidation({
        framework:"bootstrap",
        icon:{
          valid: 'glyphicon glyphicon-ok',
          invalid: 'glyphicon glyphicon-remove',
          validating: 'glyphicon glyphicon-refresh'
        },
        fields:{
          cobrar:{
            validators:{
              notEmpty:{
                message:"Es necesario elegir que pagar"
              }
            }
          }
        }
      }).on("success.form.fv",function(e){
        e.preventDefault();
        swal({
          title: 'Procesando información',
          showConfirmButton: false
        });
        pago = 0;
        mensualidad = $("#mensualidad-paypal");
        inscripcion = $("#inscripcion-paypal");
        $(".inscripcion-paypal:checked").each(
          function(){
            valor=$(".inscripcion-paypal:checked").val();
            pago++;
          }
        );
        values = {"pago":pago,"valor":valor};
        data = $(this).serialize()+"&"+jQuery.param(values);
        $.ajax({
          url:"/dashboard/index/generarpago",
          type:"post",
          data:data,
          dataType:"json",
          success:function(resp){
            code = resp.code;
            switch (code){
              case 200:
                  if(resp.status=="no"){
                    swal({
                      title: 'Solicitud Incompleta',
                      text: 'Favor de terminar tu solicitud de inscripción',
                      showConfirmButton: false,
                      timer: 8000
                    }).then((result)=>{
                      if(result.value){
                        window.location.replace("/cuenta");
                      }
                    });
                  }
                  else{
                    $("#pago-paypal-hidden").removeClass("hidden");
                    /*$("#hola").attr("href", resp.url);*/
                    $("#pago-paypal").remove();
                    window.open(resp.url, '_blank');
                    swal({
                      title: 'Su registro ha sido exitoso',
                      text: 'Seleccione el postgrado solicitado y de click en el botón "Pagar Ahora!" que aparece a continuación',
                      showConfirmButton: false,
                      timer: 2000
                    });
                  }
              break;
              case 404:
                swal({
                  title: 'Error',
                  text: ''+resp.message,
                  showConfirmButton: false,
                  timer: 8000
                }).then((result)=>{
                  if(result.value){
                    window.location.replace("/cuenta");
                  }
                });
              break;
            };
          },
          error : function(resp){
              code = resp.code;
              /*console.log("error");*/
              swal({
                title: 'Error',
                text: 'Mensaje'+code,
                showConfirmButton: false,
                timer: 8000
              });
          },
        })
      })
    };
  });