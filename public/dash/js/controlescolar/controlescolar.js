$(document).ready(function(){
    var newForm = $("#newGrado");
    if(newForm.length>=1){

    	getSpanishD();
        $('.birth_date').datepicker({
            format: 'dd/mm/yyyy',
            language : "es",
            autoclose:true
        })
        newForm.formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            excluded: ':disabled',
            locale: 'es_ES',
            fields: {}
        }).on('success.form.fv', function(e) {
            e.preventDefault();
            $("#message-box-info").toggleClass("open");
            $.ajax({
                type : "POST",
                url : "/dashboard/controlescolar/savegrado",
                data : $(this).serialize(),
                dataType : "json",
                success : function(response){
                    if(response.message=="SUCCESS" && response.code==200){
                        messages(1);
                        setTimeout(function(){
                            $("#message-box-success").removeClass("open");
                            $("#other_products").toggleClass("open");
                        },2000);
                        resetForm(newForm);
                        newForm.formValidation('resetForm', true);
                    }else{
                        messages(2);
                    }
                },
                error : function(){
                    messages(3)
                }
            });
        });
    }


function getSpanishD(){
    (function($){
        $.fn.datepicker.dates['es'] = {
            days: ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo"],
            daysShort: ["Dom", "Lun", "Mar", "Mié", "Jue", "Vie", "Sáb", "Dom"],
            daysMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa", "Do"],
            months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
            monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
            today: "Hoy",
            clear: "Borrar",
            weekStart: 1,
            format: "dd/mm/yyyy"
        };
    }(jQuery));
}

});