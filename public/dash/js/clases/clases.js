$(document).ready(function(){
    /********************************************/
    /************  crear Conferencia  ***********/
    /********************************************/
    var newCatedra = $("#catedranew");
    if(newCatedra.length>=1){
        $('.datebegin').datepicker({
            format: 'dd/mm/yyyy',
            language : "es",
            autoclose:true
        });
        newCatedra.formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            locale: 'es_ES',
            excluded: ':disabled',
            fields: {
                brid: {
                    validators: {notEmpty: {}}
                },
                status: {
                    validators: {notEmpty: {}}
                },
                birth_date: {
                    validators: {notEmpty: {}}
                }
            }
        }).on('success.form.fv', function(e) {
            e.preventDefault();
            swal({
                title: 'Guardando conferencia',
                allowEscapeKey: false,
                allowOutsideClick: false,
                timer: 2000,
                onOpen: () => {
                    swal.showLoading();
                }
            })
            $.ajax({
                type : "POST",
                url : "/dashboard/clases/savecatedra",
                data :$(this).serialize(),
                dataType : "json",
                success : function(response){
                    if(response.code==200){
                        swal({
                          title: 'Conferencia guardada',
                          text: 'Gracias',
                          showConfirmButton: false,
                          type: "success",
                          timer: 3000
                        }).then((result)=>{
                          if(result.value){
                            window.location.replace("/dashboard/clases/catedras/"+$("#clasid").val());
                          }
                        });
                       
                    }else{

                    }
                },
                error : function(){

                }
            });
        });
    }

    /********************************************/
    /******  Fin de crear conferencias   ********/
    /********************************************/
    /********************************************/
    /********   Eliminar conferencias   *********/
    /********************************************/

    $(document.body).on("click","#table-conferencias tbody td a.deletecatedra",function(e){
        e.preventDefault();
        id=$(this).data("id");
        clasid = $(this).data("clasid");
        swal({
            title: 'Eliminando catedra',
            allowEscapeKey: false,
            allowOutsideClick: false,
            timer: 2000,
            onOpen: () => {
                swal.showLoading();
            }
        })
        $.ajax({
            type : "POST",
            url : "/dashboard/clases/deletecatedra",
            data :{id:id},
            dataType : "json",
            success : function(response){
                if(response.code==200){
                    swal({
                      title: 'Catedra eliminada',
                      text: 'Felicidades',
                      showConfirmButton: false,
                      type: "success",
                      timer: 5000
                    }).then((result)=>{
                      if(result.value){
                        window.location.replace("/dashboard/clases/catedras/"+clasid);
                      }
                    });
                }else{
                    Swal.fire({
                        type: 'error',
                        title: 'Oops...',
                        text: 'Algo salio mal!',
                        footer: '<a href="https://wa.me/521019931656476?text=Tengo problemas con mi recibo" target="_blank">Comunicarse con el área administrativa?</a>'
                    })
                }
            },
            error : function(){
                Swal.fire({
                    type: 'error',
                    title: 'Oops...',
                    text: 'Algo salio mal!',
                    footer: '<a href="https://wa.me/521019931656476?text=Tengo problemas con mi recibo" target="_blank">Comunicarse con el área administrativa?</a>'
                })
            }
        });
    });
    /********************************************/
    /******  Fin de eliminar conferencias  ******/
    /********************************************/
    /********************************************/
    /************  crear admisión  **************/
    /********************************************/
    var newModulo = $("#modulonew");
    if(newModulo.length>=1){
        $('.birth_date').datepicker({
            format: 'dd/mm/yyyy',
            language : "es",
            autoclose:true
        });
        newModulo.formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            locale: 'es_ES',
            excluded: ':disabled',
            fields: {
                brid: {
                    validators: {notEmpty: {}}
                },
                status: {
                    validators: {notEmpty: {}}
                },
                birth_date: {
                    validators: {notEmpty: {}}
                }
            }
        }).on('success.form.fv', function(e) {
            e.preventDefault();
            swal({
                title: 'Guardando clase',
                allowEscapeKey: false,
                allowOutsideClick: false,
                timer: 2000,
                onOpen: () => {
                    swal.showLoading();
                }
            })
            $.ajax({
                type : "POST",
                url : "/dashboard/clases/savemodulo",
                data :$(this).serialize(),
                dataType : "json",
                success : function(response){
                    if(response.code==200){
                        messages(1);
                        $(".billing").addClass("hide");
                        var channel = $('input[type="radio"][name="iva"]:checked').val();
                        if (channel=='mx'){$("#state").select2("val", "");$("#mpid_fiscal").select2("val", "");}

                        resetForm(newModulo);
                        newModulo.formValidation('resetForm', true);
                        setTimeout(function(){
                            $("#other_products").toggleClass("open");
                        },1000);
                    }else{
                        messages(2)
                    }
                },
                error : function(){
                    messages(3);
                }
            });
        });
    }

    /********************************************/
    /********* Fin de crear admisión  ***********/
    /********************************************/
    /********************************************/
    /*********  Cargar calendario ***************/
    /********************************************/
    if($("#calendario").length>=1){
      Dropzone.autoDiscover = false;
      var myDropzone = new Dropzone('#calendario', {
              url: "/dashboard/clases/uploaddocumentcalendario",
              uploadMultiple : false,
              paramName: "file", // The name that will be used to transfer the file
              maxFilesize: 15, // MB
              maxFiles: 1,
              parallelUploads : 1,
              addRemoveLinks : true,
              dictResponseError: "No se puede subir esta archivo!",
              autoProcessQueue: true,
              sending : function(file, xhr, formData){
                formData.append("type","RECIBO");
                formData.append("stuid",""+$("#uid").val());
                formData.append("concepto",""+$("#concepto option:selected").val())
              },
              success: function(file, response){
                var values= response;
                $("#input-calendario-receipt").val(values.document);
                swal({
                  title: 'Calendario guardado',
                  text: 'Gracias',
                  showConfirmButton: false,
                  type: "success",
                  timer: 5000
                }).then((result)=>{
                  if(result.value){
                    window.location.replace("/dashboard/student/pagos");
                  }
                });
              },
              removedfile: function(file) {
                    id = file.previewElement.accessKey;
                    name = file.previewElement.id;
                    $.ajax({
                        url: "/dashboard/index/deletedrecibo",
                        type: "post",
                        data: {name:name},
                        dataType:"json",
                        success : function(response){
                            if(response.code==200){
                                $(file.previewElement).remove();
                                $("#"+id).remove();
                            }else{
                                $(file.previewElement).remove();
                                $("#"+id).remove();
                            }
                        },
                        error : function(){
                            alert("Ha ocurrido un error");
                        }
                    });
              }
          })
    }
    /********************************************/
    /********* Fin Cargar calendario ************/
    /********************************************/
    /********************************************/
    /*******  Cargar plan de estudio ************/
    /********************************************/
    if($("#plan").length>=1){
      Dropzone.autoDiscover = false;
      var myDropzone = new Dropzone('#plan', {
              url: "/dashboard/clases/uploaddocumentplan",
              uploadMultiple : false,
              paramName: "file", // The name that will be used to transfer the file
              maxFilesize: 15, // MB
              maxFiles: 1,
              parallelUploads : 1,
              addRemoveLinks : true,
              dictResponseError: "No se puede subir esta archivo!",
              autoProcessQueue: true,
              previewTemplate: '<div class="dz-preview dz-file-preview"><div class="dz-details"><div class="dz-filename"><span data-dz-name style:"font-weight: bold;"></span></div><div class="dz-size">File size: <span data-dz-size></span></div><div class="dz-thumbnail-wrapper"><div class="dz-thumbnail"><img data-dz-thumbnail><span class="dz-nopreview">No preview</span><div class="dz-success-mark"><i class="fa fa-check-circle-o"></i></div><div class="dz-error-mark"><i class="fas fa-times-circle"></i></div><div class="dz-error-message"><span data-dz-errormessage></span></div></div></div></div><div class="progress progress-striped active"><div class="progress-bar progress-bar-success" data-dz-uploadprogress></div></div></div>',
              sending : function(file, xhr, formData){
                formData.append("type","RECIBO");
                formData.append("stuid",""+$("#uid").val());
                formData.append("concepto",""+$("#concepto option:selected").val())
              },
              success: function(file, response){
                var values= response;
                $(".dz-preview").addClass("dz-success");
                $("div.progress").remove();
                $("#input-plan-receipt").val(values.document);
                swal({
                  title: 'Plan guardado',
                  text: 'Gracias',
                  showConfirmButton: false,
                  type: "success",
                  timer: 5000
                }).then((result)=>{
                  if(result.value){
                    window.location.replace("/dashboard/student/pagos");
                  }
                });
              },
              removedfile: function(file) {
                    id = file.previewElement.accessKey;
                    name = file.previewElement.id;
                    $.ajax({
                        url: "/dashboard/index/deletedrecibo",
                        type: "post",
                        data: {name:name},
                        dataType:"json",
                        success : function(response){
                            if(response.code==200){
                                $(file.previewElement).remove();
                                $("#"+id).remove();
                            }else{
                                $(file.previewElement).remove();
                                $("#"+id).remove();
                            }
                        },
                        error : function(){
                            alert("Ha ocurrido un error");
                        }
                    });
              }
          })
    }
    /********************************************/
    /********* Fin Cargar calendario ************/
    /********************************************/

    $(document.body).on("click","#add-student",function(){
        $("#modalAddStudent").modal();
    });
    $(document.body).on("click",".delete-student-class",function(){
        clid = $(this).parents().parents("tr").attr("data-id");
        clasid = $(this).parents().parents("tr").attr("data-clasid");
        Swal.fire({
            title: '¿Estas seguro?',
            text: "Estas seguro de eliminar al alumno!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si,Eliminar!',
        }).then((result)=>{
            if(result.value){
                $.ajax({
                    type : "POST",
                    url : "/dashboard/clases/deleteuserclass",
                    data :  {clid:clid,clasid:clasid},
                    dataType : "json",
                    success : function(response){
                        var clase = response.clase;
                        if(response.message=="SUCCESS" && response.code==200){
                            Swal.fire(
                              'Eliminado!',
                              'El estudiante fue eliminado.',
                              'success'
                            ).then(function() {
                                window.location = "/dashboard/clases/list/"+clasid;
                            });
                        }else if(response.code==404){
                            
                        }
                    },
                    error : function(){
                    }
                });
            }  
        })
    });
        /*.then((result) => {
            if (result.value){
                Swal.fire(
                  'Eliminado!',
                  'Estudiante eliminado de la clase.',
                  'success'
            )}
        })*/
    $(document.body).on("click",".score",function(){
      clid = $(this).parents().parents("tr").attr("data-id");
      clasid = $(this).parents().parents("tr").attr("data-clasid");
      $("#clid-student-update-score").val(clid);
      $("#clasid-student-update-score").val(clasid);
      $("#updatescorestudent").toggleClass("open");

    });
    var updatescore = $("#updatescorestudentform");
    if(updatescore.length>=1){
        updatescore.formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                scorestudent: {
                    validators: {
                        notEmpty: {
                            message: 'La calificación es necesario y no puede estar vacío.'
                        }
                    }
                }
            }
        }).on('success.form.fv', function(e) {
            $("#updatescorestudent").toggleClass("open");
            e.preventDefault();
            swal({
                title: "Calificando",
                allowOutsideClick: false,
                button: false,
                showCancelButton: false,
                showConfirmButton: false,
                closeOnEsc: false,
            });
            $.ajax({
                type : "POST",
                url : "/dashboard/student/updatescorestudent",
                data :  $(this).serialize(),
                dataType : "json",
                success : function(response){
                    var clase = response.clase;
                    if(response.message=="SUCCESS" && response.code==200){
                        swal({
                            title: "calificación Actualizada",
                            icon: "success",
                            allowOutsideClick: false,
                            showCancelButton: false,
                            showConfirmButton: true,
                            closeOnEsc: false,
                        }).then(function() {
                            window.location = "/dashboard/clases/list/"+response.clasid;
                        });
                    }else if(response.code==404){
                        
                    }
                },
                error : function(){
                }
            });
        });
    }



	var newForm = $("#newProduct");
	if(newForm.length>=1){
	    console.log("entra el formulario");
	    getPermalink($("#name"),"/dashboard/products/permalink",$("#permalink"),newForm);
	    newForm.formValidation({
	        framework: 'bootstrap',
	        icon: {
	            valid: 'glyphicon glyphicon-ok',
	            invalid: 'glyphicon glyphicon-remove',
	            validating: 'glyphicon glyphicon-refresh'
	        },
	        excluded: ':disabled',
	        locale: 'es_ES',
	        fields: {}
	    }).on('success.form.fv', function(e) {
	        e.preventDefault();
	        $("#message-box-info").toggleClass("open");
	        $.ajax({
	            type : "POST",
	            url : "/dashboard/products/save",
	            data : $(this).serialize(),
	            dataType : "json",
	            success : function(response){
	                if(response.message=="SUCCESS" && response.code==200){
	                    messages(1);
	                    setTimeout(function(){
	                        $("#message-box-success").removeClass("open");
	                        $("#other_products").toggleClass("open");
	                    },2000);
	                    resetForm(newForm);
	                    newForm.formValidation('resetForm', true);
	                }else{
	                    messages(2);
	                }
	            },
	            error : function(){
	                messages(3)
	            }
	        });
	    });
	}
	function getPermalink(selector,url,selector_permalink,form){
	    selector.change(function(){
	        name_service = $(this).val();
	        id = $(this).attr("data-id");
	        $.ajax({
	            url : url,
	            type : "POST",
	            data : {name:name_service,id:id},
	            dataType : "json",
	            success : function(response){
	                if(response.message=="SUCCESS" && response.code==200){
	                    selector_permalink.val(response.permalink);
	                    form.formValidation('revalidateField', 'permalink');
	                }else{
	                    alert("Ha ocurrido un error intente nuevamente.");
	                }
	            },error : function(){
	                alert("Ha ocurrido un error intente nuevamente.");
	            }
	        });
	    });
	}
});

function resetForm($form) {
    $form.find('input:text, input:password, input:file, input[type=email], select, textarea').val('');
    $form.find('input:radio, input:checkbox').removeAttr('checked').removeAttr('selected');
}
function messages($type){
    if($type==1){
        setTimeout(function(){
            $("#message-box-info").removeClass("open");
            $("#message-box-success").toggleClass("open");
        },1500);
        setTimeout(function(){
            $("#message-box-success").removeClass("open");
        },2000);
    }
    else if($type==2){
        setTimeout(function(){
            $("#message-box-info").removeClass("open");
            $("#message-box-warning").toggleClass("open");
        },1500);
        setTimeout(function(){
            $("#message-box-warning").removeClass("open");
        },3000);
    }else if($type==3){
        setTimeout(function(){
            $("#message-box-info").removeClass("open");
            $("#message-box-warning").toggleClass("open");
        },1500);
        setTimeout(function(){
            $("#message-box-warning").removeClass("open");
        },3000);
    }
}