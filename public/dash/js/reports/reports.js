$(document).ready(function(){
    var formReports = $("#formReports");
    var t = null;
    if(formReports.length>=1){
        getDateTimePicker();
         t = getDataTable();

        formReports.submit(function (e) {
            submitSales = $("#submitSales");

            submitSales.addClass("disabled").attr("disabled",true);
            $("#exportReports").addClass("disabled").attr("disabled",true);
            t.row().remove().draw( false );
            e.preventDefault();

            $.ajax({
                data : $(this).serialize(),
                url : "/dashboard/reports/get-history",
                type : "POST",
                dataType : "json",
                success:function(response){
                    $("table#tableSales tbody tr").remove();
                    var class_tr = "";
                    if(response.code==200){
                        $.each(response.result,function(k,val){
                            discount = val.discount?val.discount:0;
                            var table = $("table#tableSales tbody");
                            status_service = val.status=="CANCELADO"?"cancelado":"activo";
                            class_tr  = t.row.add( [val.type+'<br>'+status_service,val.date,val.attend,val.name_service+" "+val.name_sub_service,"$"+val.price,val.quantity/*,val.subtotal*/,"$"+val.discount,"$"+val.total] ).draw( false).node();
                            switch (val.type){
                                case "SERVICIO":
                                    val.status=="CANCELADO"?$(class_tr).addClass('danger'):"";
                                    break;
                                case "VENTAS":
                                    $(class_tr).addClass('info');
                                    val.status=="CANCELADO"?$(class_tr).addClass('danger'):"";
                                    break;
                                case "CONSUMO":
                                    $(class_tr).addClass('warning');
                                    val.status=="CANCELADO"?$(class_tr).addClass('danger'):"";
                                    break;
                            }
                            t.columns.adjust().draw();
                        });
                        $("#subtotal").text("$ "+response.breakdown.subtotal);
                        $("#iva_general").text("$ "+response.breakdown.iva_general);
                        $("#amount_to_pay").text("$ "+response.breakdown.total);
                    }else if(response.code==300){
                        submitSales.popover('show');
                        setTimeout(function(){
                            submitSales.popover('destroy')
                        },2000);
                        $("#subtotal").text("$0");
                        $("#iva_general").text("$0");
                        $("#amount_to_pay").text("$0");
                    }else{
                        messages(2);
                    }
                },error:function(){
                    messages(3);
                },complete:function(){
                    submitSales.removeClass("disabled").attr("disabled",false);
                    $("#exportReports").removeClass("disabled").attr("disabled",false);
                }
            });
        });
        $("#exportReports").click(function(){
            user = $("#user").val();
            start_date = $("#start_date").val();
            cutoff_date = $("#cutoff_date").val();
            status = $("#status").val();
            window.open('/dashboard/reports/export-general?user='+user+'&start_date='+start_date+'&cutoff_date='+cutoff_date+'&status='+status+'','_blank');
        });
    }
    var formCommission = $("#formCommission");
    if(formCommission.length>=1){
        getDateTimePicker();
        t  = getDataTable();
        formCommission.submit(function (e) {
            submitSales = $("#submitSales");

            submitSales.addClass("disabled").attr("disabled",true);
            $("#exportReports").addClass("disabled").attr("disabled",true);
            t.row().remove().draw( false );
            e.preventDefault();

            $.ajax({
                data : $(this).serialize(),
                url : "/dashboard/reports/get-commission",
                type : "POST",
                dataType : "json",
                success:function(response){
                    $("table#tableSales tbody tr").remove();
                    var class_tr = "";
                    if(response.code==200){
                        $.each(response.result,function(k,val){
                            $.each(val,function(h,values){
                                class_tr  = t.row.add( [h,values.attend,values.quantity, "$"+parseFloat(values.commission).toFixed(2)] ).draw( false).node();
                                switch (h){
                                    case "SERVICIO":
                                        $(class_tr).addClass('success');
                                        break;
                                    case "VENTAS":
                                        $(class_tr).addClass('info');
                                        break;
                                }
                                t.columns.adjust().draw();
                            });
                        });
                    }else if(response.code==300){
                        submitSales.popover('show');
                        setTimeout(function(){
                            submitSales.popover('destroy')
                        },2000);
                    }else{
                        messages(2);
                    }
                },error:function(){
                    messages(3);
                },complete:function(){
                    submitSales.removeClass("disabled").attr("disabled",false);
                    $("#exportReports").removeClass("disabled").attr("disabled",false);
                }
            });
        });
        $("#exportReports").click(function(){
            user = $("#user").val();
            start_date = $("#start_date").val();
            cutoff_date = $("#cutoff_date").val();
            status = $("#status").val();
            window.open('/dashboard/reports/export-commission?user='+user+'&start_date='+start_date+'&cutoff_date='+cutoff_date+'&status='+status+'','_blank');
        });
    }

});
function getDataTable(){
    t = $(".dataTable").DataTable({"language": {
        "paginate": {"first":"Primero","last":"Ultimo","next":"Siguiente","previous":"Anterior"},
        "lengthMenu": "Mostrar _MENU_ registros por página",
        "zeroRecords": "Nada por el momento -Lo lamento",
        "info": "Mostrando del _START_ al _END_ de _TOTAL_",
        "infoEmpty": "No hay registros disponibles",
        "infoFiltered": "(Filtrado de _MAX_ registros en total)"
    },
        fnPreDrawCallback: function(oSettings, json) {
            $('.dataTables_filter input').attr('placeholder', 'Buscar.');
        }
    });
    return t;
}
function getDateTimePicker(){
    (function($){
        $.fn.datepicker.dates['es'] = {
            days: ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo"],
            daysShort: ["Dom", "Lun", "Mar", "Mié", "Jue", "Vie", "Sáb", "Dom"],
            daysMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa", "Do"],
            months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
            monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
            today: "Hoy",
            clear: "Borrar",
            weekStart: 1,
            format: "dd/mm/yyyy"
        };
    }(jQuery));
    $('.getDatepicker').datepicker({
        language : "es",
        autoclose:true
    }).on('changeDate', function(e) {
        $this = $(this);
        start_date = $("#start_date").val();
        cutoff_date_val = $("#cutoff_date");
        if(cutoff_date_val.val()<start_date && cutoff_date_val.val().length>0){
            cutoff_date_val.val("");
            cutoff_date_val.popover('show');
            setTimeout(function(){
                cutoff_date_val.popover('destroy')
            },2000);
        }
    });
}
function messages($type){
    if($type==1){
        setTimeout(function(){
            $("#message-box-info").removeClass("open");
            $("#message-box-success").toggleClass("open");
        },1500);
        setTimeout(function(){
            $("#message-box-success").removeClass("open");
        },2000);
    }
    else if($type==2){
        setTimeout(function(){
            $("#message-box-info").removeClass("open");
            $("#message-box-warning").toggleClass("open");
        },1500);
        setTimeout(function(){
            $("#message-box-warning").removeClass("open");
        },3000);
    }else if($type==3){
        setTimeout(function(){
            $("#message-box-info").removeClass("open");
            $("#message-box-warning").toggleClass("open");
        },1500);
        setTimeout(function(){
            $("#message-box-warning").removeClass("open");
        },3000);
    }
    else if($type==4){
        setTimeout(function(){
            $("#message-box-process").removeClass("open");
            $("#message-box-pay").toggleClass("open");
        },1500);
        setTimeout(function(){
            $("#message-box-pay").removeClass("open");
        },3000);
    }
}

/*table.append("<tr class='success'><td>"+val.salid+"</td><td class='text-right'>$ "+val.total+"</td><td class='text-right'>"+discount+"</td><td>"+val.user+"</td><td>"+val.date+"</td><td>"+val.status+"</td><td><a onclick="+"printPage('"+val.url_print+"')"+" data-toggle='tooltip' data-placement='top' title='Imprimir comprobante de venta'><span class='fa fa-print fa-2x'></span></a>&nbsp;<a target='_blank' href='"+val.url_list_print+"' data-toggle='tooltip' data-placement='top' title='Ver comprobante de venta'><span class='fa fa-eye fa-2x'></span></a>&nbsp;<a target='_blank' href='"+val.url_view+"' data-toggle='tooltip' data-placement='top' title='Información general de venta'><span class='fa fa-folder-open fa-2x'></span></a></td></tr>");*/
/*table.append("<tr class='"+status_color+"'><td>"+val.salid+"</td><td class='text-right'>$ "+val.total+"</td><td class='text-right'>"+discount+"</td><td>"+val.user+"</td><td>"+val.date+"</td><td>"+status_message+"</td><td><a target='_blank' href='"+val.url_list_print+"' data-toggle='tooltip' data-placement='top' title='Editar venta'><span class='fa fa-edit fa-2x'></span></a></td></tr>");*/
/*table.append("<tr class='"+status_color+"'><td>"+val.salid+"</td><td class='text-right'>$ "+val.total+"</td><td class='text-right'>"+discount+"</td><td>"+val.user+"</td><td>"+val.date+"</td><td>"+status_message+"</td><td><a target='_blank' href='"+val.url_view+"' data-toggle='tooltip' data-placement='top' title='Información general de venta'><span class='fa fa-folder-open fa-2x'></span></a></td></tr>");*/