
jQuery(document).ready(function($) {
  	$(document.body).on("click", ".validate-recibo", function(){
  		recid = $(this).attr("data-recid");
        Swal.fire('Validando recibo, con id:'+recid);
        $.ajax({
            type : "POST",
            url : "/dashboard/recibos/update",
            data :{id:recid},
            dataType : "json",
            success : function(response){
                if(response.code==200){
                    //messages(1);
                    Swal.fire({
                      position: 'top-end',
                      type: 'success',
                      title: 'Recibo Validado',
                      showConfirmButton: false,
                      timer: 1500
                    })
                    setTimeout(function(){
                        window.location.href = "/dashboard/recibos/index";
                    },500)
                }else{
                    swal ( "Oops" ,  "Hubo algún error de guardado!" ,  "error" );
                }
            },
            error : function(){
                swal ( "Oops" ,  "No pudo entrar al servidor!" ,  "error" );
            }
        });
  	});
 });