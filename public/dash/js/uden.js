$(document).ready(function(){

/* Session */
    if($("#session").length>=1){
        $('#session').formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                email: {
                    validators: {
                        notEmpty: {
                            message: 'Es necesaria la dirección de correo electrónico y esta no puede estar vacía'
                        },
                        emailAddress: {
                            message: 'Este campo no contiene una dirección de correo electrónico válida.'
                        }
                    }
                },
                password: {
                    validators: {
                        notEmpty: {
                            message: 'La contraseña es necesaria y no puede estar vacía.'
                        }
                    }
                }
            }
        }).on('success.form.fv', function(e) {
            e.preventDefault();
            $("#session-loading").removeClass("hide").addClass("in");
            $("#btn-submit").attr("disabled","disabled").val("Iniciando");
            $("#container-messages").removeClass("hide").addClass("in");
            var values = {
                key: $('#key-security').attr('data-key'),
                value: $('#value-security').attr('data-value')
            };
            var data = $(this).serialize()+"&"+jQuery.param(values);
            $.ajax({
                type : "POST",
                url : "/dashboard/login/session",
                data : data,
                dataType : "json",
                success : function(response){
                    if(response.message=="SUCCESS" && response.code==200){
                        window.location = response.url;
                    }else if(response.code==300){
                        $("#password-incorrect").removeClass("hide").addClass("in");
                    }else if(response.code==400){
                        $("#email-incorrect").removeClass("hide").addClass("in");
                    }
                    else if(response.code==404){
                        $("#all-incorrect").removeClass("hide").addClass("in");
                    }
                    $("#session-loading").removeClass("in").addClass("hide");
                    $("#btn-submit").removeAttr("disabled","disabled").val("Iniciar");
                    $("#key-security").attr("data-key",response.token.key);
                    $("#value-security").attr("data-value",response.token.value);
                },
                error : function(){

                },
                complete : function(){
                    setTimeout(function(){
                        $("#password-incorrect").removeClass("in").addClass("hide");
                        $("#email-incorrect").removeClass("in").addClass("hide");
                        $("#all-incorrect").removeClass("in").addClass("hide");
                        $("#container-messages").removeClass("in").addClass("hide");
                    },3000);
                }
            });
        });
    }
/* End */

/* Clients */
    var newClient = $("#newClient");
    if(newClient.length>=1){
        newClient.formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            excluded: ':disabled',
            fields: {
                name: {
                    validators: {
                        notEmpty: {
                            message: 'El nombre es un campo requerido y no puede estar vacío.'
                        }
                    }
                },
                last_name: {
                    validators: {
                        notEmpty: {
                            message: 'El apellido paterno es un campo requerido y no puede estar vacío.'
                        }
                    }
                },
                second_name: {
                    validators: {
                        notEmpty: {
                            message: 'El apellido materno es un campo requerido y no puede estar vacío.'
                        }
                    }
                },
                email: {
                    validators: {
                        notEmpty: {
                            message: 'El correo electrónico es un campo requerido y no puede estar vacío.'
                        },
                        emailAddress: {
                            message: 'Este campo no contiene una dirección de correo electrónico válida.'
                        },
                        remote: {
                            message: 'Esta cuenta de correo electrónico ya existe, pruebe con otra',
                            url: '/dashboard/client/validate-email',
                            data: {
                                type: 'email'
                            },
                            type: 'POST'
                        }
                    }
                },
                tel: {
                    validators: {
                        notEmpty: {
                            message: 'Su numero de telefono es necesario y no puede estar vacío.'
                        }
                    }
                },
                profession: {
                    validators: {
                        notEmpty: {
                            message: 'La profesión es un campo necesario y no puede estar vacío.'
                        }
                    }
                },
                company: {
                    validators: {
                        notEmpty: {
                            message: 'La empresa es un campo necesario y no puede estar vacío.'
                        }
                    }
                },
                street: {
                    validators: {
                        notEmpty: {
                            message: 'Este campo es necesario y no puede estar vacío.'
                        }
                    }
                },
                city_dir: {
                    validators: {
                        notEmpty: {
                            message: 'Este campo es necesario y no puede estar vacío.'
                        }
                    }
                },
                cp: {
                    validators: {
                        notEmpty: {
                            message: 'Este campo es necesario y no puede estar vacío.'
                        }
                    }
                },
                state_dir: {
                    validators: {
                        notEmpty: {
                            message: 'Este campo es necesario y no puede estar vacío.'
                        }
                    }
                },
                mpid: {
                    validators: {
                        notEmpty: {
                            message: 'Este campo es necesario y no puede estar vacío.'
                        }
                    }
                },
                nationality: {
                    validators: {
                        notEmpty: {
                            message: 'La nacionalidad es un campo necesario y no puede estar vacío.'
                        }
                    }
                },
                rfc: {
                    validators: {
                        callback: {
                            message: 'El RFC es un campo necesario.',
                            callback: function(value, validator, $field) {
                                var channel = newClient.find('[name="nationality"]:checked').val();
                                return (channel !== 'mx') ? true : (value !== '');
                            }
                        },
                        remote: {
                            message: 'Este RFC ya existe, pruebe con otro',
                            url: '/dashboard/client/validate-rfc',
                            data: {
                                type: 'rfc'
                            },
                            type: 'POST'
                        }
                    }
                },
                business_name: {
                    validators: {
                        callback: {
                            message: 'La Razón Social es un campo necesario.',
                            callback: function(value, validator, $field) {
                                var channel = newClient.find('[name="nationality"]:checked').val();
                                return (channel !== 'mx') ? true : (value !== '');
                            }
                        }
                    }
                },
                fiscal_direction: {
                    validators: {
                        callback: {
                            message: 'La Dirección Fiscal es un campo necesario.',
                            callback: function(value, validator, $field) {
                                var channel = newClient.find('[name="nationality"]:checked').val();
                                return (channel !== 'mx') ? true : (value !== '');
                            }
                        }
                    }
                },
                state: {
                    validators: {
                        callback: {
                            message: 'El Estado / Ciudad es un campo necesario.',
                            callback: function(value, validator, $field) {
                                var channel = newClient.find('[name="nationality"]:checked').val();
                                return (channel !== 'mx') ? true : (value !== '');
                            }
                        }
                    }
                },
                postal_code: {
                    validators: {
                        callback: {
                            message: 'El Código Postal es un campo necesario.',
                            callback: function(value, validator, $field) {
                                var channel = newClient.find('[name="nationality"]:checked').val();
                                return (channel !== 'mx') ? true : (value !== '');
                            }
                        }
                    }
                },
                email_fiscal: {
                    validators: {
                        callback: {
                            message: 'El campo Email Facturación es un campo necesario.',
                            callback: function(value, validator, $field) {
                                var channel = newClient.find('[name="nationality"]:checked').val();
                                return (channel !== 'mx') ? true : (value !== '');
                            }
                        }
                    }
                }
            }
        }).on('change', '[name="nationality"]', function(e) {
            newClient.formValidation('revalidateField', 'rfc');
            newClient.formValidation('revalidateField', 'business_name');
            newClient.formValidation('revalidateField', 'fiscal_direction');
            newClient.formValidation('revalidateField', 'state');
            newClient.formValidation('revalidateField', 'postal_code');
            newClient.formValidation('revalidateField', 'email_fiscal');
        }).on('success.field.fv', function(e, data) {
            if (data.field === 'rfc' || data.field === 'business_name' || data.field === 'fiscal_direction' || data.field === 'state' || data.field === 'postal_code' || data.field === 'email_fiscal') {
                var channel = newClient.find('[name="nationality"]:checked').val();
                // User choose given channel
                if (channel !== 'mx') {
                    // Remove the success class from the container
                    data.element.closest('.form-group').removeClass('has-success');
                    // Hide the tick icon
                    data.element.data('fv.icon').hide();
                }
            }
        }).on('success.form.fv', function(e) {
            e.preventDefault();
            $("#message-box-info").toggleClass("open");
            $.ajax({
                type : "POST",
                url : "/dashboard/client/save",
                data :$(this).serialize(),
                dataType : "json",
                success : function(response){
                    if(response.code==200){
                        messages(1);
                        setTimeout(function(){
                            window.location = "/dashboard/clients";
                        },2000);
                    }else if(response.code==404){
                        messages(2)
                    }
                },
                error : function(){
                    messages(3);
                }
            });
        });
        $("#newClient input[name='nationality']").click(function(){
            var radio = $(this).val();
            if(radio=="mx"){
                $(".billing").removeClass("hide");
            }else{
                $(".billing").addClass("hide");
            }
        });
        getMunicipios($("#state_dir"),"mpid",$("#newClient"));
    }
    if($("#editClient").length>=1){
        var email = $('#editClient').find('input[name="email"]').val();
        $('#editClient').formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            excluded: ':disabled',
            fields: {
                name: {
                    validators: {
                        notEmpty: {
                            message: 'El nombre es un campo requerido y no puede estar vacío.'
                        }
                    }
                },
                last_name: {
                    validators: {
                        notEmpty: {
                            message: 'El apellido paterno es un campo requerido y no puede estar vacío.'
                        }
                    }
                },
                second_name: {
                    validators: {
                        notEmpty: {
                            message: 'El apellido materno es un campo requerido y no puede estar vacío.'
                        }
                    }
                },
                email: {
                    enabled: false,
                    validators: {
                        notEmpty: {
                            message: 'El correo electrónico es un campo requerido y no puede estar vacío.'
                        },
                        emailAddress: {
                            message: 'Este campo no contiene una dirección de correo electrónico válida.'
                        },
                        remote: {
                            message: 'Esta cuenta de correo electrónico ya existe, pruebe con otra',
                            url: '/dashboard/client/validate-email',
                            data: {
                                type: 'email'
                            },
                            type: 'POST'
                        }
                    }
                },
                tel: {
                    validators: {
                        notEmpty: {
                            message: 'Su numero de telefono es necesario y no puede estar vacío.'
                        }
                    }
                },
                profession: {
                    validators: {
                        notEmpty: {
                            message: 'La profesión es un campo necesario y no puede estar vacío.'
                        }
                    }
                },
                company: {
                    validators: {
                        notEmpty: {
                            message: 'La empresa es un campo necesario y no puede estar vacío.'
                        }
                    }
                },
                street: {
                    validators: {
                        notEmpty: {
                            message: 'Este campo es necesario y no puede estar vacío.'
                        }
                    }
                },
                city_dir: {
                    validators: {
                        notEmpty: {
                            message: 'Este campo es necesario y no puede estar vacío.'
                        }
                    }
                },
                cp: {
                    validators: {
                        notEmpty: {
                            message: 'Este campo es necesario y no puede estar vacío.'
                        }
                    }
                },
                state_dir: {
                    validators: {
                        notEmpty: {
                            message: 'Este campo es necesario y no puede estar vacío.'
                        }
                    }
                },
                mpid: {
                    validators: {
                        notEmpty: {
                            message: 'Este campo es necesario y no puede estar vacío.'
                        }
                    }
                },
                nationality: {
                    validators: {
                        notEmpty: {
                            message: 'La nacionalidad es un campo necesario y no puede estar vacío.'
                        }
                    }
                },
                rfc: {
                    enabled: false,
                    validators: {
                        callback: {
                            message: 'El RFC es un campo necesario.',
                            callback: function(value, validator, $field) {
                                var channel = $('#editClient').find('[name="nationality"]:checked').val();
                                return (channel !== 'mx') ? true : (value !== '');
                            }
                        },
                        remote: {
                            message: 'Este RFC ya existe, pruebe con otro',
                            url: '/dashboard/client/validate-rfc',
                            data: {
                                type: 'rfc'
                            },
                            type: 'POST'
                        }
                    }
                },
                business_name: {
                    validators: {
                        callback: {
                            message: 'La Razón Social es un campo necesario.',
                            callback: function(value, validator, $field) {
                                var channel = $('#editClient').find('[name="nationality"]:checked').val();
                                return (channel !== 'mx') ? true : (value !== '');
                            }
                        }
                    }
                },
                fiscal_direction: {
                    validators: {
                        callback: {
                            message: 'La Dirección Fiscal es un campo necesario.',
                            callback: function(value, validator, $field) {
                                var channel = $('#editClient').find('[name="nationality"]:checked').val();
                                return (channel !== 'mx') ? true : (value !== '');
                            }
                        }
                    }
                },
                state: {
                    validators: {
                        callback: {
                            message: 'El Estado / Ciudad es un campo necesario.',
                            callback: function(value, validator, $field) {
                                var channel = $('#editClient').find('[name="nationality"]:checked').val();
                                return (channel !== 'mx') ? true : (value !== '');
                            }
                        }
                    }
                },
                postal_code: {
                    validators: {
                        callback: {
                            message: 'El Código Postal es un campo necesario.',
                            callback: function(value, validator, $field) {
                                var channel = $('#editClient').find('[name="nationality"]:checked').val();
                                return (channel !== 'mx') ? true : (value !== '');
                            }
                        }
                    }
                },
                email_fiscal: {
                    validators: {
                        callback: {
                            message: 'El campo Email Facturación es un campo necesario.',
                            callback: function(value, validator, $field) {
                                var channel = $('#editClient').find('[name="nationality"]:checked').val();
                                return (channel !== 'mx') ? true : (value !== '');
                            }
                        }
                    }
                }
            }
        }).on('keyup', '[name="email"],[name="rfc"]', function() {
            var e = $('#editClient').find('input[name="email"]').val()=='';
            var r = $('#editClient').find('input[name="rfc"]').val()=='';
            if(email!==$('#editClient').find('input[name="email"]').val()){
                $('#editClient').formValidation('enableFieldValidators', 'email', !e)
            }
            $('#editClient').formValidation('enableFieldValidators', 'rfc', !r)

            if (e.length == "true") {
                $('#editClient').formValidation('validateField', 'email')
            }
            if (r.length == "true") {
                $('#editClient').formValidation('validateField', 'rfc')
            }
        }).on('change', '[name="nationality"]', function(e) {
            if($('input[name=nationality]:checked', '#editClient').val()=="mx"){
                $('#editClient').formValidation('enableFieldValidators', 'rfc')
            }if($('input[name=nationality]:checked', '#editClient').val()=="et"){
                $("#rfc").val('');
                $("#business_name").val('');
                $("#fiscal_direction").val('');
                $("#state").val('');
                $("#postal_code").val('');
                $("#email_fiscal").val('');
                $("#town").val('');
                $("#colony").val('');
            }
            $('#editClient').formValidation('revalidateField', 'rfc');
            $('#editClient').formValidation('revalidateField', 'business_name');
            $('#editClient').formValidation('revalidateField', 'fiscal_direction');
            $('#editClient').formValidation('revalidateField', 'state');
            $('#editClient').formValidation('revalidateField', 'postal_code');
            $('#editClient').formValidation('revalidateField', 'email_fiscal');
        }).on('success.field.fv', function(e, data) {
            if (data.field === 'rfc' || data.field === 'business_name' || data.field === 'fiscal_direction' || data.field === 'state' || data.field === 'postal_code' || data.field === 'email_fiscal') {
                var channel = $('#editClient').find('[name="nationality"]:checked').val();
                // User choose given channel
                if (channel !== 'mx') {
                    // Remove the success class from the container
                    data.element.closest('.form-group').removeClass('has-success');
                    // Hide the tick icon
                    data.element.data('fv.icon').hide();
                }
            }
        }).on('success.form.fv', function(e) {
            e.preventDefault();
            $("#message-box-info").toggleClass("open");
            $.ajax({
                type : "POST",
                url : "/dashboard/client/update",
                data :$(this).serialize(),
                dataType : "json",
                success : function(response){
                    if(response.code==200){
                        messages(1);
                    }else if(response.code==404){
                        messages(2)
                    }
                },
                error : function(){
                    messages(3);
                }
            });
        })
        $("#editClient input[name='nationality']").click(function(){
            var radio = $(this).val();
            if(radio=="mx"){
                $(".billing").removeClass("hide");
            }else{
                $(".billing").addClass("hide");
            }
        });
        getMunicipios($("#state_dir"),"mpid",$("#newClient"));
    }
    if($("#newInscription").length>=1){
        $("#inscriptionCourse").click(function(){
            $("#modal_inscription").modal("show");
        });
        $('#modal_inscription').on('shown.bs.modal', function() {
            getSection($("#searchUser"),"Identificador o nombre de usuario",$("#ap").val());
        });
        $('#modal_inscription').on('hidden.bs.modal', function() {
            $("#searchUser").select2('val',"");
            $("#searchUser").val('');
        });
        $('#newInscription').formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            excluded: ':disabled',
            fields: {
                searchUser: {
                    validators: {
                        notEmpty: {
                            message: 'Este campo es necesario y no puede estar vacío.'
                        },remote: {
                            message: 'Esta usuario ya esta registrado a este curso, pruebe con otro',
                            url: '/dashboard/process/validate-client',
                            data: {
                                type: 'client'
                            },
                            type: 'POST'
                        }
                    }
                }
            }
        }).on('success.form.fv', function(e) {
            e.preventDefault();
            $("#btn-submit").attr("disabled","disabled").val("Iniciando");
            $("#message-box-info").toggleClass("open");
            $.ajax({
                type : "POST",
                url : "/dashboard/process/save-inscription",
                data : $(this).serialize(),
                dataType : "json",
                success : function(response){
                    if(response.code==200){
                        $("#tbInscription tbody").append("<tr class='danger' id='"+response.content.identifier+"'><td>"+response.content.identifier+"</td><td class='nameClient'>"+response.content.name+" "+response.content.last_name+"</td><td>"+response.content.iva+"</td><td>$"+response.content.paid+"</td><td>$"+response.content.status_payed+"</td><td>"+response.content.date_paid+"</td><td class='optionsCtg'><a  href='/dashboard/annual-program/users/"+response.content.apid+"/manage/"+response.content.clid+"'><span class='fa fa-pencil' style='font-size: 17px;padding: 0%'></span></a></td>").fadeIn(1000);
                        messages(1)
                        setTimeout(function(){
                            $("#modal_inscription").modal("hide");
                        },1500)
                    }else if(response.code==300){
                        alert("Este usuario ya esta registrado en este curso");
                        $("#message-box-info").removeClass("open");
                    }else{
                        messages(2);
                    }
                },
                error : function(){
                    messages(3)
                }
            });
        });
    }
    if($("#adminPayments").length>=1){
        $("#addPayment").click(function(){
            $("#modal_payments").modal("show");
        });
        $('#modal_payments').on('hidden.bs.modal', function() {
            newPayment = $('#newPayment');
            $("#amount").val('');
            newPayment.formValidation('revalidateField', 'amount');
            $("#reason").val('');
            newPayment.formValidation('revalidateField', 'reason');
        });
        $('#newPayment').formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            excluded: ':disabled',
            fields: {
                amount: {
                    validators: {
                        notEmpty: {
                            message: 'El pago es requerido y no puede estar vacío.'
                        },
                        numeric: {
                            message: 'El pago debe de ser un entero.',
                        }
                    }
                },reason: {
                    validators: {
                        notEmpty: {
                            message: 'Este campo es requerido y no puede estar vacío.'
                        }
                    }
                }
            }
        }).on('success.form.fv', function(e) {
            e.preventDefault();
            $("#message-box-info").toggleClass("open");
            $.ajax({
                type : "POST",
                url : "/dashboard/process/save-payment",
                data : $(this).serialize(),
                dataType : "json",
                success : function(response){
                    if(response.code==200){
                        $("#tbPayment tbody#tbodyPayment").prepend("<tr id='"+response.content.pyid+"'><td><strong>Pago curso</strong><p>"+response.content.name_user+"</p></td><td class='text-center'>$"+response.content.amount+"</td><td class='text-center'>"+response.content.reason+"</td><td class='text-center'>"+response.content.date+"</td><td class='text-center'><span class='fa fa-remove fa-2x deletePayment pointer' data-amount='"+response.content.amount+"' data-payment='"+response.content.pyid+"' data-toggle='tooltip' data-placement='top' title='Eliminar pago'></span>&nbsp;<span style='margin-top: 3px' onclick=printPage('"+response.content.url+"') class='fa fa-print fa-2x pointer' data-toggle='tooltip' data-placement='top' title='Imprimir Reporte de pago'></span>&nbsp;<span style='margin-top: 3px' class='fa fa-upload fa-2x pointer upload-file "+response.content.pyid+"' data-url='' data-voucher='' data-pyid='"+response.content.pyid+"' data-toggle='tooltip' data-placement='top' title='Subir comprobante de pago firmado'></span></td></tr>").fadeIn(1000);
                        $("#plus").text(response.content.amount2);
                        $("#owed").text(response.content.owed);
                        messages(1);
                        setTimeout(function(){
                            $("#modal_payments").modal("hide");
                        },1500)
                    }else if(response.code==300){
                        alert("Este usuario ya ha superado el limite de pago");
                        $("#message-box-info").removeClass("open");
                    }else{
                        messages(2);
                    }
                },
                error : function(){
                    messages(3)
                }
            });
        });
        $(document.body).on("click","#tbPayment tbody#tbodyPayment td span.deletePayment",function(e){
            e.preventDefault();
            $this = $(this);
            payment = $this.attr("data-payment");
            amount = $this.attr("data-amount");
            $("#pyid").val(payment);
            $('#deletePaymentModal').modal();
            $("#cgValue").text(amount);
        });
        $('#deletePaymentModal').on('hidden.bs.modal', function() {
            $("#pyid").val('');
        });
        $('#deletePaymentForm').formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            excluded: ':disabled',
            fields: {
                reason_update: {
                    validators: {
                        notEmpty: {
                            message: 'Este campo es requerido y no puede estar vacío.'
                        }
                    }
                }
            }
        }).on('success.form.fv', function(e) {
            e.preventDefault();
            $("#message-box-info").toggleClass("open");
            $.ajax({
                type : "POST",
                url : "/dashboard/process/delete-payment",
                data : $(this).serialize(),
                dataType : "json",
                success : function(response){
                    if(response.code==200){
                        $("#tbPayment tbody#tbodyPayment tr#"+response.content.pyid+"").remove().fadeIn(1000);
                        $("#plus").text(response.content.amount2);
                        $("#owed").text(response.content.owed);
                        messages(1)
                        setTimeout(function(){
                            $("#deletePaymentModal").modal("hide");
                        },1500)
                    }else if(response.code==300){
                        alert("Este usuario ya ha superado el limite de pago");
                        $("#message-box-info").removeClass("open");
                    }else{
                        messages(2);
                    }
                },
                error : function(){
                    messages(3)
                }
            });
        });
        $(document.body).on("click","#tbPayment tbody#tbodyPayment td span.upload-file",function(e){
            e.preventDefault();
            $this = $(this);
            pyid = $this.attr("data-pyid");
            voucher = $this.attr("data-voucher");
            pdf_view = $("a#pdf_view");
            if(voucher){
                url = $this.attr("data-url");
                pdf_view.attr("href",url);
                pdf_view.parent().removeClass("hidden");
            }else{ pdf_view.parent().addClass("hidden"); pdf_view.attr("href","");}

            $("#pdfI").val(voucher);
            $('#upload_payments').modal();
            uploadPDF(".pdfPayment",pyid);
        });
    }
/* End Clients */

/*Courses*/
    if($("#newCourse").length>=1){
        $('#newCourse').formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            excluded: ':disabled',
            fields: {
                name: {
                    validators: {
                        notEmpty: {
                            message: 'El Titulo es necesario y no puede estar vacío.'
                        },
                        stringLength: {
                            min: 10,
                            max: 200,
                            message: 'El titulo debe ser mayor de 10 y menor de 100 caracteres de longitud.'
                        }
                    }
                },
                permalink: {
                    validators: {
                        notEmpty: {
                            message: 'El Permalink o Url es necesario y no puede estar vacío.'
                        },
                        stringLength: {
                            min: 10,
                            max: 250,
                            message: 'El Permalink o Url  debe ser mayor de 10 y menor de 100 caracteres de longitud.'
                        }
                    }
                },
                description: {
                    validators: {
                        notEmpty: {
                            message: 'El sumario es necesario y no puede estar vacío.'
                        },
                        stringLength: {
                            min: 10,
                            max: 180,
                            message: 'La descripción debe ser mayor de 10 y menor de 180 caracteres de longitud.'
                        }
                    }
                },
                identifier: {
                    validators: {
                        notEmpty: {
                            message: 'El código del curso es campo necesario y no puede estar vacío.'
                        }
                    }
                },
                image: {
                    validators: {
                        notEmpty: {
                            message: 'La imagen principal es campo necesario y no puede estar vacío.'
                        }
                    }
                },
                "image-2": {
                    validators: {
                        notEmpty: {
                            message: 'La imagen principal es campo necesario y no puede estar vacío.'
                        }
                    }
                },
                objective: {
                    validators: {
                        notEmpty: {
                            message: 'El objetivo es requerida y no puede estar vacía.'
                        },
                        callback: {
                            message: 'El objetivo debe de tener mas de 100 caracteres.',
                            callback: function(value, validator, $field) {
                                if (value === '') {
                                    return true;
                                }
                                var div  = $('<div/>').html(value).get(0),
                                    text = div.textContent || div.innerText;
                                return text.length >= 0;
                            }
                        }
                    }
                },
                directed: {
                    validators: {
                        notEmpty: {
                            message: 'El campo "Dirigido a" es requerido y no puede estar vacío.'
                        },
                        callback: {
                            message: 'El campo "Dirigido a" debe de tener mas de 50 caracteres.',
                            callback: function(value, validator, $field) {
                                if (value === '') {
                                    return true;
                                }
                                var div  = $('<div/>').html(value).get(0),
                                    text = div.textContent || div.innerText;
                                return text.length >= 0;
                            }
                        }
                    }
                },
                content: {
                    validators: {
                        notEmpty: {
                            message: 'El contenido es requerido y no puede estar vacío.'
                        },
                        callback: {
                            message: 'El contenido debe de tener mas de 200 caracteres.',
                            callback: function(value, validator, $field) {
                                if (value === '') {
                                    return true;
                                }
                                var div  = $('<div/>').html(value).get(0),
                                    text = div.textContent || div.innerText;
                                return text.length >= 0;
                            }
                        }
                    }
                },
                price: {
                    validators: {
                        notEmpty: {
                            message: 'El precio es requerido y no puede estar vacío.'
                        },
                        numeric: {
                            message: 'El precio debe de ser un entero.',
                        }
                    }
                },
                status: {
                    validators: {
                        notEmpty: {
                            message: 'El status es requerida y no puede estar vacío.'
                        }
                    }
                },
                category: {
                    validators: {
                        notEmpty: {
                            message: 'La categoria es requerida y no puede estar vacía.'
                        }
                    }
                },
                duration: {
                    validators: {
                        notEmpty: {
                            message: 'La duración es requerida y no puede estar vacía.'
                        }
                    }
                }
            }
        }).on('success.form.fv', function(e) {
            e.preventDefault();
            $("#message-box-info").toggleClass("open");
            var values = {
                key: $('#key-security').attr('data-key'),
                value: $('#value-security').attr('data-value')
            };
            var data = $(this).serialize()+"&"+jQuery.param(values);
            $.ajax({
                type : "POST",
                url : "/dashboard/course/save",
                data : data,
                dataType : "json",
                success : function(response){
                    if(response.message=="SUCCESS" && response.code==200){
                        $("#key-security").attr("data-key",response.token.key);
                        $("#value-security").attr("data-value",response.token.value);
                        messages(1);
                        setTimeout(function(){
                            window.location = "/dashboard/courses";
                        },3000);
                    }else if(response.code==404){
                        messages(3);

                    }
                },
                error : function(){
                    messages(3);
                }
            });
        })
        $("#objective").ckeditor().editor.on('change', function(e) {
            $('#newCourse').formValidation('revalidateField', 'objective');
        });
        $("#directed").ckeditor().editor.on('change', function(e) {
            $('#newCourse').formValidation('revalidateField', 'directed');
        });
        $("#content").ckeditor().editor.on('change', function(e) {
            $('#newCourse').formValidation('revalidateField', 'content');
        });
        $("#cid").select2({
            maximumSelectionLength:1,
            placeholder: "Especialidades"
        });
    }
    if($("#editCourse").length>=1){
        $('#editCourse').formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            excluded: ':disabled',
            fields: {
                name: {
                    validators: {
                        notEmpty: {
                            message: 'El Titulo es necesario y no puede estar vacío.'
                        },
                        stringLength: {
                            min: 10,
                            max: 200,
                            message: 'El titulo debe ser mayor de 10 y menor de 100 caracteres de longitud.'
                        }
                    }
                },
                permalink: {
                    validators: {
                        notEmpty: {
                            message: 'El Permalink o Url es necesario y no puede estar vacío.'
                        },
                        stringLength: {
                            min: 10,
                            max: 250,
                            message: 'El Permalink o Url  debe ser mayor de 10 y menor de 100 caracteres de longitud.'
                        }
                    }
                },
                description: {
                    validators: {
                        notEmpty: {
                            message: 'El sumario es necesario y no puede estar vacío.'
                        },
                        stringLength: {
                            min: 10,
                            max: 180,
                            message: 'La descripción debe ser mayor de 10 y menor de 180 caracteres de longitud.'
                        }
                    }
                },
                identifier: {
                    validators: {
                        notEmpty: {
                            message: 'El código del curso es campo necesario y no puede estar vacío.'
                        }
                    }
                },
                image: {
                    validators: {
                        notEmpty: {
                            message: 'La imagen principal es campo necesario y no puede estar vacío.'
                        }
                    }
                },
                "image-2": {
                    validators: {
                        notEmpty: {
                            message: 'La imagen principal es campo necesario y no puede estar vacío.'
                        }
                    }
                },
                objective: {
                    validators: {
                        notEmpty: {
                            message: 'El objetivo es requerida y no puede estar vacía.'
                        },
                        callback: {
                            message: 'El objetivo debe de tener mas de 100 caracteres.',
                            callback: function(value, validator, $field) {
                                if (value === '') {
                                    return true;
                                }
                                var div  = $('<div/>').html(value).get(0),
                                    text = div.textContent || div.innerText;
                                return text.length >= 0;
                            }
                        }
                    }
                },
                directed: {
                    validators: {
                        notEmpty: {
                            message: 'El campo "Dirigido a" es requerido y no puede estar vacío.'
                        },
                        callback: {
                            message: 'El campo "Dirigido a" debe de tener mas de 100 caracteres.',
                            callback: function(value, validator, $field) {
                                if (value === '') {
                                    return true;
                                }
                                var div  = $('<div/>').html(value).get(0),
                                    text = div.textContent || div.innerText;
                                return text.length >= 0;
                            }
                        }
                    }
                },
                content: {
                    validators: {
                        notEmpty: {
                            message: 'El contenido es requerido y no puede estar vacío.'
                        },
                        callback: {
                            message: 'El contenido debe de tener mas de 200 caracteres.',
                            callback: function(value, validator, $field) {
                                if (value === '') {
                                    return true;
                                }
                                var div  = $('<div/>').html(value).get(0),
                                    text = div.textContent || div.innerText;
                                return text.length >= 0;
                            }
                        }
                    }
                },
                price: {
                    validators: {
                        notEmpty: {
                            message: 'El precio es requerido y no puede estar vacío.'
                        },
                        numeric: {
                            message: 'El precio debe de ser un entero.',
                        }
                    }
                },
                status: {
                    validators: {
                        notEmpty: {
                            message: 'El status es requerida y no puede estar vacío.'
                        }
                    }
                },
                category: {
                    validators: {
                        notEmpty: {
                            message: 'La categoria es requerida y no puede estar vacía.'
                        }
                    }
                },
                duration: {
                    validators: {
                        notEmpty: {
                            message: 'La duración es requerida y no puede estar vacía.'
                        }
                    }
                }
            }
        }).on('success.form.fv', function(e) {
            e.preventDefault();
            $("#message-box-info").toggleClass("open");
            var values = {
                key: $('#key-security').attr('data-key'),
                value: $('#value-security').attr('data-value')
            };
            var data = $(this).serialize()+"&"+jQuery.param(values);
            $.ajax({
                type : "POST",
                url : "/dashboard/course/update",
                data : data,
                dataType : "json",
                success : function(response){
                    if(response.message=="SUCCESS" && response.code==200){
                        $("#key-security").attr("data-key",response.token.key);
                        $("#value-security").attr("data-value",response.token.value);
                        messages(1);
                    }else if(response.code==404){
                        messages(2);
                    }
                },
                error : function(){
                    messages(3);
                }
            });
        })
        $("#objective").ckeditor().editor.on('change', function(e) {
            $('#editCourse').formValidation('revalidateField', 'objective');
        });
        $("#directed").ckeditor().editor.on('change', function(e) {
            $('#editCourse').formValidation('revalidateField', 'directed');
        });
        $("#content").ckeditor().editor.on('change', function(e) {
            $('#editCourse').formValidation('revalidateField', 'content');
        });
        $("#cid").select2({
            maximumSelectionLength:1,
            placeholder: "Especialidades"
        });
    }
    if($("#url-title").length>=1){
        $("#url-title").change(function(){
            var $input_loader = $("#input-loader");
            $input_loader.removeClass("hide fade");
            var title_note = $(this).val();
            $.ajax({
                url : "/dashboard/course/validateurl",
                type : "POST",
                data : {name:title_note,key: $('#key-security').attr('data-key'),value: $('#value-security').attr('data-value')},
                dataType : "json",
                success : function(response){
                    if(response.message=="SUCCESS" && response.code==200){
                        $("#note-permalink").val(response.new_url);
                        $('#newCourse').formValidation('revalidateField', 'permalink');
                        $("#key-security").attr("data-key",response.token.key);
                        $("#value-security").attr("data-value",response.token.value);
                        $input_loader.addClass("hide fade");
                    }else{
                        $input_loader.addClass("hide fade");
                        alert("Ha ocurrido un error intente nuevamente.");
                    }
                },
                complete: function(){
                    $input_loader.addClass("hide fade");
                },error : function(){
                    $input_loader.addClass("hide fade");
                    alert("Ha ocurrido un error intente nuevamente.");
                }
            });
        });
    }
/* End */

/* Annual Program */
    var annualCourse = $("#newAnnualCourse");
    if(annualCourse.length>=1){
        function createDatePicker(){
            $('.datepicker').datepicker({
                format: 'dd/mm/yyyy',
                language : "es",
                autoclose:true
            }).on('changeDate', function(e) {
                annualCourse.formValidation('revalidateField', 'start_date');
                annualCourse.formValidation('revalidateField', 'date_course');
            });
        }
        (function($){
            $.fn.datepicker.dates['es'] = {
                days: ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo"],
                daysShort: ["Dom", "Lun", "Mar", "Mié", "Jue", "Vie", "Sáb", "Dom"],
                daysMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa", "Do"],
                months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
                monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
                today: "Hoy",
                clear: "Borrar",
                weekStart: 1,
                format: "dd/mm/yyyy"
            };
        }(jQuery));
        createDatePicker();
        $("#add_payday").click(function(){
            if($("div#parent_payday div.elements_payday").length>5){
                alert("Llegaste al limite de Fechas de pago");
                return false
            }
            $("#parent_payday").append('<div class="col-sm-12 elements_payday"><div class="col-md-3"><input type="text" class="form-control datepicker" placeholder="Inscripción" name="payday[]" required/></div><div class="col-md-3"> <input type="text" class="form-control" placeholder="Descripción" name="description_payday[]" required value=""/></div><div class="col-md-3"><input  step="any" type="number" class="form-control" placeholder="Monto" name="amount_payday[]" required min="0"></div><div class="col-md-3"> <span class="fa fa-remove fa-2x btn btn-danger btn-small remove_payday"></span></div></div>');
            createDatePicker();
        });
        $(document.body).on("click","#parent_payday div span.remove_payday",function(e){
            e.preventDefault();
            $this = $(this);
            $this.parent().parent().remove();
        });
        annualCourse.formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            excluded: ':disabled',
            fields: {
                course: {
                    validators: {
                        notEmpty: {
                            message: 'El Curso es necesario y no puede estar vacío.'
                        }
                    }
                },
                city: {
                    validators: {
                        notEmpty: {
                            message: 'La ciudad es necesaria y no puede estar vacío.'
                        }
                    }
                },
                duration: {
                    validators: {
                        notEmpty: {
                            message: 'La duración en horas es necesaria y no puede estar vacía.'
                        }
                    }
                },
                start_date: {
                    validators: {
                        notEmpty: {
                            message: 'La fecha de inicio es necesaria y no puede estar vacía.'
                        }
                    }
                },
                date_course: {
                    validators: {
                        notEmpty: {
                            message: 'La fecha de fin de curso es necesaria y no puede estar vacía.'
                        }
                    }
                },
                instructor: {
                    validators: {
                        notEmpty: {
                            message: 'El instructor es necesario y no puede estar vacío.'
                        }
                    }
                },
                status: {
                    validators: {
                        notEmpty: {
                            message: 'El estatus es requerido y no puede estar vacío.'
                        }
                    }
                },
                final: {
                    validators: {
                        notEmpty: {
                            message: 'El estatus de curso finalizado es requerido y no puede estar vacío.'
                        }
                    }
                }
            }
        }).on('success.form.fv', function(e) {
            e.preventDefault();
            $("#message-box-info").toggleClass("open");
            $.ajax({
                type : "POST",
                url : "/dashboard/process/savecourse",
                data : $(this).serialize(),
                dataType : "json",
                success : function(response){
                    if(response.code==200){
                        messages(1);
                        setTimeout(function(){
                            window.location = "/dashboard/annual-program";
                        },3000);
                    }else if(response.code==404){
                        messages(2);
                    }
                },
                error : function(){
                    messages(3);
                }
            });
        });
    }
    var editAnnualCourse =$("#editAnnualCourse");
    if(editAnnualCourse.length>=1){
        function createDatePicker(){
            $('.datepicker').datepicker({
                format: 'dd/mm/yyyy',
                language : "es",
                autoclose:true
            }).on('changeDate', function(e) {
                editAnnualCourse.formValidation('revalidateField', 'start_date');
                editAnnualCourse.formValidation('revalidateField', 'date_course');
            });
        }
        (function($){
            $.fn.datepicker.dates['es'] = {
                days: ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo"],
                daysShort: ["Dom", "Lun", "Mar", "Mié", "Jue", "Vie", "Sáb", "Dom"],
                daysMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa", "Do"],
                months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
                monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
                today: "Hoy",
                clear: "Borrar",
                weekStart: 1,
                format: "dd/mm/yyyy"
            };
        }(jQuery));
        createDatePicker();

        $("#add_payday").click(function(){
            if($("div#parent_payday div.elements_payday").length>5){
                alert("Llegaste al limite de Fechas de pago");
                return false
            }
            $("#parent_payday").append('<div class="col-sm-12 elements_payday"><div class="col-md-3"><input type="text" class="form-control datepicker" placeholder="Inscripción" name="payday[]" required/></div><div class="col-md-3"> <input type="text" class="form-control" placeholder="Descripción" name="description_payday[]" required value=""/></div><div class="col-md-3"><input  step="any" type="number" class="form-control" placeholder="Monto" name="amount_payday[]" required min="0"></div><div class="col-md-1"><span data-paydid="" class="fa fa-exchange fa-2x btn btn-success btn-small update_payday" data-toggle="tooltip" data-placement="left" title="Actualizar fecha de pago."></span></div><div class="col-md-1"> <span data-paydid="" class="fa fa-remove fa-2x btn btn-danger btn-small remove_payday"></span></div></div>');
            createDatePicker();
        });
        $(document.body).on("click","#parent_payday div span.remove_payday",function(e){
            e.preventDefault();
            $this = $(this);
            $paydid = $this.attr("data-paydid");
            if($paydid){
                $.ajax({
                    url :"/dashboard/process/removepayday",
                    data:{id:$paydid},
                    type:"POST",
                    dataType:"Json",
                    success:function(response){
                        if(response.code!="200"){alert("No se ha podido eliminar, intente nuevamente");}
                        else{
                            $this.parent().parent().remove();
                        }
                    },error:function(){
                        alert("No se ha podido eliminar, intente nuevamente");
                    }
                })
            }else{
                $this.parent().parent().remove();
            }
        });
        $(document.body).on("click","#parent_payday div span.update_payday",function(e){
            e.preventDefault();
            $("#message-box-info").toggleClass("open");
            var $this = $(this);
            apid = $("#apid").val();
            var parent = $(this).parent().parent();
            $payday = parent.find("input[name='payday[]']").val();
            $description = parent.find("input[name='description_payday[]']").val();
            $amount = parent.find("input[name='amount_payday[]']").val();
            $paydid = $this.attr("data-paydid");
            $.ajax({
                url :"/dashboard/process/addpayday",
                data:{id:$paydid,payday:$payday,description:$description,amount:$amount,apid:apid},
                type:"POST",
                dataType:"Json",
                success:function(response){
                    if(response.code=="200")
                    {   switch (response.message){
                            case "UPDATE" :
                                break;
                            case "NEW" :
                                parent.find(".remove_payday").attr("data-paydid",response.payday);
                                $this.attr("data-paydid",response.payday);
                                break;
                            default : alert("No se ha podido eliminar, intente nuevamente");
                        }
                        messages(1);
                    }else{
                        messages(2);
                    }
                },error:function(){
                    messages(3);
                }
            })
        });
        editAnnualCourse.formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            excluded: ':disabled',
            fields: {
                course: {
                    validators: {
                        notEmpty: {
                            message: 'El Curso es necesario y no puede estar vacío.'
                        }
                    }
                },
                city: {
                    validators: {
                        notEmpty: {
                            message: 'La ciudad es necesaria y no puede estar vacío.'
                        }
                    }
                },
                duration: {
                    validators: {
                        notEmpty: {
                            message: 'La duración en horas es necesaria y no puede estar vacía.'
                        }
                    }
                },
                start_date: {
                    validators: {
                        notEmpty: {
                            message: 'La fecha de inicio es necesaria y no puede estar vacía.'
                        }
                    }
                },
                date_course: {
                    validators: {
                        notEmpty: {
                            message: 'La fecha de fin de curso es necesaria y no puede estar vacía.'
                        }
                    }
                },
                instructor: {
                    validators: {
                        notEmpty: {
                            message: 'El instructor es necesario y no puede estar vacío.'
                        }
                    }
                },
                status: {
                    validators: {
                        notEmpty: {
                            message: 'El estatus es requerido y no puede estar vacío.'
                        }
                    }
                },
                final: {
                    validators: {
                        notEmpty: {
                            message: 'El estatus de curso finalizado es requerido y no puede estar vacío.'
                        }
                    }
                }
            }
        }).on('success.form.fv', function(e) {
            e.preventDefault();
            $("#message-box-info").toggleClass("open");
            $.ajax({
                type : "POST",
                url : "/dashboard/process/update-course",
                data : $(this).serialize(),
                dataType : "json",
                success : function(response){
                    if(response.code==200){
                        messages(1);
                    }else if(response.code==404){
                        messages(2);
                    }
                },
                error : function(){
                    messages(3);
                }
            });
        });
    }
/* End */

/* Finished */
    if($("#newFinishedCourse").length>=1){
        (function($){
            $.fn.datepicker.dates['es'] = {
                days: ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo"],
                daysShort: ["Dom", "Lun", "Mar", "Mié", "Jue", "Vie", "Sáb", "Dom"],
                daysMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa", "Do"],
                months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
                monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
                today: "Hoy",
                clear: "Borrar",
                weekStart: 1,
                format: "dd/mm/yyyy"
            };
        }(jQuery));
        $('#datepicker').datepicker({
            format: 'dd/mm/yyyy',
            language : "es",
            autoclose:true
        }).on('changeDate', function(e) {
            $('#newFinishedCourse').formValidation('revalidateField', 'date_finished');
        });
        $('#newFinishedCourse').formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            excluded: ':disabled',
            fields: {
                name: {
                    validators: {
                        notEmpty: {
                            message: 'El Titulo es necesario y no puede estar vacío.'
                        },
                        stringLength: {
                            min: 10,
                            max: 100,
                            message: 'El titulo debe ser mayor de 10 y menor de 100 caracteres de longitud.'
                        }
                    }
                },
                permalink: {
                    validators: {
                        notEmpty: {
                            message: 'El Permalink o Url es necesario y no puede estar vacío.'
                        },
                        stringLength: {
                            min: 10,
                            max: 100,
                            message: 'El Permalink o Url  debe ser mayor de 10 y menor de 100 caracteres de longitud.'
                        }
                    }
                },
                image: {
                    validators: {
                        notEmpty: {
                            message: 'La imagen principal es campo necesario y no puede estar vacío.'
                        }
                    }
                },
                "image-1": {
                    validators: {
                        notEmpty: {
                            message: 'La imagen principal es campo necesario y no puede estar vacío.'
                        }
                    }
                },
                content: {
                    validators: {
                        notEmpty: {
                            message: 'El contenido es requerido y no puede estar vacío.'
                        },
                        callback: {
                            message: 'El contenido debe de tener mas de 100 caracteres.',
                            callback: function(value, validator, $field) {
                                if (value === '') {
                                    return true;
                                }
                                var div  = $('<div/>').html(value).get(0),
                                    text = div.textContent || div.innerText;
                                return text.length >= 100;
                            }
                        }
                    }
                },
                status: {
                    validators: {
                        notEmpty: {
                            message: 'El status es requerida y no puede estar vacío.'
                        }
                    }
                },
                category: {
                    validators: {
                        notEmpty: {
                            message: 'La categoria es requerida y no puede estar vacía.'
                        }
                    }
                },
                city: {
                    validators: {
                        notEmpty: {
                            message: 'La ciudad es requerida y no puede estar vacía.'
                        }
                    }
                },
                date_finished: {
                    validators: {
                        notEmpty: {
                            message: 'La fecha de finalización del curso     es requerida y no puede estar vacía.'
                        }
                    }
                }
            }
        }).on('success.form.fv', function(e) {
            e.preventDefault();
            $("#message-box-info").toggleClass("open");
            var values = {
                key: $('#key-security').attr('data-key'),
                value: $('#value-security').attr('data-value')
            };
            var data = $(this).serialize()+"&"+jQuery.param(values);
            $.ajax({
                type : "POST",
                url : "/dashboard/finished/save",
                data : data,
                dataType : "json",
                success : function(response){
                    if(response.message=="SUCCESS" && response.code==200){
                        $("#key-security").attr("data-key",response.token.key);
                        $("#value-security").attr("data-value",response.token.value);
                        setTimeout(function(){
                            $("#message-box-info").removeClass("open");
                            $("#message-box-success").toggleClass("open");
                        },1000);
                        setTimeout(function(){
                            window.location = "/dashboard/finished-courses";
                        },3000);
                    }else if(response.code==404){
                        $("#message-box-info").removeClass("open");
                        $("#message-box-warning").toggleClass("open");
                        setTimeout(function(){
                            $("#message-box-warning").removeClass("open");
                        },1500);

                    }
                },
                error : function(){
                    $("#message-box-info").removeClass("open");
                    $("#message-box-warning").toggleClass("open");
                    setTimeout(function(){
                        $("#message-box-warning").removeClass("open");
                    },1500);
                }
            });
        });
        $("#content").ckeditor().editor.on('change', function(e) {
            $('#newFinishedCourse').formValidation('revalidateField', 'content');
        });
        $("#cid").select2({
            maximumSelectionLength:1,
            placeholder: "Especialidades"
        });
    }
    if($("#editFinishedCourse").length>=1){
        (function($){
            $.fn.datepicker.dates['es'] = {
                days: ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo"],
                daysShort: ["Dom", "Lun", "Mar", "Mié", "Jue", "Vie", "Sáb", "Dom"],
                daysMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa", "Do"],
                months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
                monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
                today: "Hoy",
                clear: "Borrar",
                weekStart: 1,
                format: "dd/mm/yyyy"
            };
        }(jQuery));
        $('#datepicker').datepicker({
            format: 'dd/mm/yyyy',
            language : "es",
            autoclose:true
        }).on('changeDate', function(e) {
            $('#newFinishedCourse').formValidation('revalidateField', 'date_finished');
        });
        $('#editFinishedCourse').formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            excluded: ':disabled',
            fields: {
                name: {
                    validators: {
                        notEmpty: {
                            message: 'El Titulo es necesario y no puede estar vacío.'
                        },
                        stringLength: {
                            min: 10,
                            max: 100,
                            message: 'El titulo debe ser mayor de 10 y menor de 100 caracteres de longitud.'
                        }
                    }
                },
                permalink: {
                    validators: {
                        notEmpty: {
                            message: 'El Permalink o Url es necesario y no puede estar vacío.'
                        },
                        stringLength: {
                            min: 10,
                            max: 100,
                            message: 'El Permalink o Url  debe ser mayor de 10 y menor de 100 caracteres de longitud.'
                        }
                    }
                },
                image: {
                    validators: {
                        notEmpty: {
                            message: 'La imagen principal es campo necesario y no puede estar vacío.'
                        }
                    }
                },
                "image-1": {
                    validators: {
                        notEmpty: {
                            message: 'La imagen principal es campo necesario y no puede estar vacío.'
                        }
                    }
                },
                content: {
                    validators: {
                        notEmpty: {
                            message: 'El contenido es requerido y no puede estar vacío.'
                        },
                        callback: {
                            message: 'El contenido debe de tener mas de 100 caracteres.',
                            callback: function(value, validator, $field) {
                                if (value === '') {
                                    return true;
                                }
                                var div  = $('<div/>').html(value).get(0),
                                    text = div.textContent || div.innerText;
                                return text.length >= 100;
                            }
                        }
                    }
                },
                status: {
                    validators: {
                        notEmpty: {
                            message: 'El status es requerida y no puede estar vacío.'
                        }
                    }
                },
                category: {
                    validators: {
                        notEmpty: {
                            message: 'La categoria es requerida y no puede estar vacía.'
                        }
                    }
                },
                city: {
                    validators: {
                        notEmpty: {
                            message: 'La ciudad es requerida y no puede estar vacía.'
                        }
                    }
                },
                date_finished: {
                    validators: {
                        notEmpty: {
                            message: 'La fecha de finalización del curso     es requerida y no puede estar vacía.'
                        }
                    }
                }
            }
        }).on('success.form.fv', function(e) {
            e.preventDefault();
            $("#message-box-info").toggleClass("open");
            var values = {
                key: $('#key-security').attr('data-key'),
                value: $('#value-security').attr('data-value')
            };
            var data = $(this).serialize()+"&"+jQuery.param(values);
            $.ajax({
                type : "POST",
                url : "/dashboard/finished/update",
                data : data,
                dataType : "json",
                success : function(response){
                    if(response.message=="SUCCESS" && response.code==200){
                        $("#key-security").attr("data-key",response.token.key);
                        $("#value-security").attr("data-value",response.token.value);
                        messages(1);
                    }else if(response.code==404){
                        messages(2);

                    }
                },
                error : function(){
                    messages(3);
                }
            });
        })
        $("#content").ckeditor().editor.on('change', function(e) {
            $('#newFinishedCourse').formValidation('revalidateField', 'content');
        });
        $("#cid").select2({
            maximumSelectionLength:1,
            placeholder: "Especialidades"
        });
    }
    if($("#url-title-Fcourse").length>=1){
        $("#url-title-Fcourse").change(function(){
            var $input_loader = $("#input-loader");
            $input_loader.removeClass("hide fade");
            var title_note = $(this).val();
            $.ajax({
                url : "/dashboard/finished/validateurl",
                type : "POST",
                data : {name:title_note,key: $('#key-security').attr('data-key'),value: $('#value-security').attr('data-value')},
                dataType : "json",
                success : function(response){
                    if(response.message=="SUCCESS" && response.code==200){
                        $("#note-permalink").val(response.new_url);
                        $('#newCourse').formValidation('revalidateField', 'permalink');
                        $("#key-security").attr("data-key",response.token.key);
                        $("#value-security").attr("data-value",response.token.value);
                        $input_loader.addClass("hide fade");
                        $('#newFinishedCourse').formValidation('revalidateField', 'permalink');
                    }else{
                        $input_loader.addClass("hide fade");
                        alert("Ha ocurrido un error intente nuevamente.");
                        $('#newFinishedCourse').formValidation('revalidateField', 'permalink');
                    }
                },
                complete: function(){
                    $input_loader.addClass("hide fade");
                },error : function(){
                    $input_loader.addClass("hide fade");
                    alert("Ha ocurrido un error intente nuevamente.");
                }
            });
        });
    }
/* End */

/* Instructor */
    if($("#newInstructor").length>=1){
        (function($){
            $.fn.datepicker.dates['es'] = {
                days: ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo"],
                daysShort: ["Dom", "Lun", "Mar", "Mié", "Jue", "Vie", "Sáb", "Dom"],
                daysMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa", "Do"],
                months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
                monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
                today: "Hoy",
                clear: "Borrar",
                weekStart: 1,
                format: "dd/mm/yyyy"
            };
        }(jQuery));
        $('#datepicker').datepicker({
            format: 'dd/mm/yyyy',
            language : "es",
            autoclose:true
        }).on('changeDate', function(e) {
            $('#newInstructor').formValidation('revalidateField', 'beginning');
        });
        $('#newInstructor').formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            excluded: ':disabled',
            fields: {
                name: {
                    validators: {
                        notEmpty: {
                            message: 'El Nombre es un campo requerido y no puede estar vacío.'
                        },
                        stringLength: {
                            min: 4,
                            max: 45,
                            message: 'El nombre debe ser mayor de 4 y menor de 45 caracteres de longitud.'
                        }
                    }
                },
                last_name: {
                    validators: {
                        notEmpty: {
                            message: 'El Apellido Paterno es un campo requerido y no puede estar vacío.'
                        },
                        stringLength: {
                            min: 4,
                            max: 45,
                            message: 'El Apellido Paterno debe ser mayor de 4 y menor de 45 caracteres de longitud.'
                        }
                    }
                },
                second_name: {
                    validators: {
                        notEmpty: {
                            message: 'El Apellido Materno es un campo requerido y no puede estar vacío.'
                        },
                        stringLength: {
                            min: 4,
                            max: 45,
                            message: 'El Apellido Materno debe ser mayor de 4 y menor de 45 caracteres de longitud.'
                        }
                    }
                },
                sex: {
                    validators: {
                        notEmpty: {
                            message: 'El Sexo es un campo requerido y no puede estar vacío.'
                        }
                    }
                },
                title: {
                    validators: {
                        notEmpty: {
                            message: 'El Nivel educativo es un campo requerido y no puede estar vacío.'
                        }
                    }
                },
                image: {
                    validators: {
                        notEmpty: {
                            message: 'La imagen principal es necesaria y no puede estar vacía.'
                        }
                    }
                },
                curriculum: {
                    validators: {
                        notEmpty: {
                            message: 'El Currículum Vítae es necesario y no puede estar vacía.'
                        }
                    }
                },
                imageI: {
                    validators: {
                        notEmpty: {
                            message: 'La imagen principal es campo necesario y no puede estar vacío.'
                        }
                    }
                },
                pdfI: {
                    validators: {
                        notEmpty: {
                            message: 'El Currículum Vítae es archivo necesario y no puede estar vacío.'
                        }
                    }
                },
                description: {
                    validators: {
                        notEmpty: {
                            message: 'La descripción es un campo requerido y no puede estar vacía.'
                        },
                        callback: {
                            message: 'La descripción debe de tener mas de 100 caracteres.',
                            callback: function(value, validator, $field) {
                                if (value === '') {
                                    return true;
                                }
                                var div  = $('<div/>').html(value).get(0),
                                    text = div.textContent || div.innerText;
                                return text.length >= 100;
                            }
                        }
                    }
                },
                status: {
                    validators: {
                        notEmpty: {
                            message: 'El status es un campo requerido y no puede estar vacío.'
                        }
                    }
                },
                beginning: {
                    validators: {
                        notEmpty: {
                            message: 'La fecha de contratación es un campo requerido y no puede estar vacío.'
                        }
                    }
                },
                jurisdiction: {
                    validators: {
                        notEmpty: {
                            message: 'La Jurisdicción es un campo requerido y no puede estar vacío.'
                        }
                    }
                }
            }
        }).on('success.form.fv', function(e) {
            e.preventDefault();
            $("#message-box-info").toggleClass("open");
            var values = {
                key: $('#key-security').attr('data-key'),
                value: $('#value-security').attr('data-value')
            };
            var data = $(this).serialize()+"&"+jQuery.param(values);
            $.ajax({
                type : "POST",
                url : "/dashboard/instructor/save",
                data : data,
                dataType : "json",
                success : function(response){
                    if(response.message=="SUCCESS" && response.code==200){
                        messages(1);
                        setTimeout(function(){
                            window.location = "/dashboard/instructors";
                        },2000);
                    }else if(response.code==404){
                        messages(2)
                    }
                    $("#key-security").attr("data-key",response.token.key);
                    $("#value-security").attr("data-value",response.token.value);
                },
                error : function(){
                    messages(3);
                }
            });
        })
        $("#description").ckeditor().editor.on('change', function(e) {
            $('#newInstructor').formValidation('revalidateField', 'description');
        });
    }
    if($("#editInstructor").length>=1){
        (function($){
            $.fn.datepicker.dates['es'] = {
                days: ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo"],
                daysShort: ["Dom", "Lun", "Mar", "Mié", "Jue", "Vie", "Sáb", "Dom"],
                daysMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa", "Do"],
                months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
                monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
                today: "Hoy",
                clear: "Borrar",
                weekStart: 1,
                format: "dd/mm/yyyy"
            };
        }(jQuery));
        $('#datepicker').datepicker({
            format: 'dd/mm/yyyy',
            language : "es",
            autoclose:true
        }).on('changeDate', function(e) {
            $('#newInstructor').formValidation('revalidateField', 'beginning');
        });
        $('#editInstructor').formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            excluded: ':disabled',
            fields: {
                name: {
                    validators: {
                        notEmpty: {
                            message: 'El Nombre es un campo requerido y no puede estar vacío.'
                        },
                        stringLength: {
                            min: 4,
                            max: 45,
                            message: 'El nombre debe ser mayor de 4 y menor de 45 caracteres de longitud.'
                        }
                    }
                },
                last_name: {
                    validators: {
                        notEmpty: {
                            message: 'El Apellido Paterno es un campo requerido y no puede estar vacío.'
                        },
                        stringLength: {
                            min: 4,
                            max: 45,
                            message: 'El Apellido Paterno debe ser mayor de 4 y menor de 45 caracteres de longitud.'
                        }
                    }
                },
                second_name: {
                    validators: {
                        notEmpty: {
                            message: 'El Apellido Materno es un campo requerido y no puede estar vacío.'
                        },
                        stringLength: {
                            min: 4,
                            max: 45,
                            message: 'El Apellido Materno debe ser mayor de 4 y menor de 45 caracteres de longitud.'
                        }
                    }
                },
                sex: {
                    validators: {
                        notEmpty: {
                            message: 'El Sexo es un campo requerido y no puede estar vacío.'
                        }
                    }
                },
                title: {
                    validators: {
                        notEmpty: {
                            message: 'El Nivel educativo es un campo requerido y no puede estar vacío.'
                        }
                    }
                },
                image: {
                    validators: {
                        notEmpty: {
                            message: 'La imagen principal es necesaria y no puede estar vacía.'
                        }
                    }
                },
                curriculum: {
                    validators: {
                        notEmpty: {
                            message: 'El Currículum Vítae es necesario y no puede estar vacía.'
                        }
                    }
                },
                imageI: {
                    validators: {
                        notEmpty: {
                            message: 'La imagen principal es campo necesario y no puede estar vacío.'
                        }
                    }
                },
                pdfI: {
                    validators: {
                        notEmpty: {
                            message: 'El Currículum Vítae es archivo necesario y no puede estar vacío.'
                        }
                    }
                },
                description: {
                    validators: {
                        notEmpty: {
                            message: 'La descripción es un campo requerido y no puede estar vacía.'
                        },
                        callback: {
                            message: 'La descripción debe de tener mas de 100 caracteres.',
                            callback: function(value, validator, $field) {
                                if (value === '') {
                                    return true;
                                }
                                var div  = $('<div/>').html(value).get(0),
                                    text = div.textContent || div.innerText;
                                return text.length >= 100;
                            }
                        }
                    }
                },
                status: {
                    validators: {
                        notEmpty: {
                            message: 'El status es un campo requerido y no puede estar vacío.'
                        }
                    }
                },
                beginning: {
                    validators: {
                        notEmpty: {
                            message: 'La fecha de contratación es un campo requerido y no puede estar vacío.'
                        }
                    }
                },
                jurisdiction: {
                    validators: {
                        notEmpty: {
                            message: 'La Jurisdicción es un campo requerido y no puede estar vacío.'
                        }
                    }
                }
            }
        }).on('success.form.fv', function(e) {
            e.preventDefault();
            $("#message-box-info").toggleClass("open");
            var values = {
                key: $('#key-security').attr('data-key'),
                value: $('#value-security').attr('data-value')
            };
            var data = $(this).serialize()+"&"+jQuery.param(values);
            $.ajax({
                type : "POST",
                url : "/dashboard/instructor/update",
                data : data,
                dataType : "json",
                success : function(response){
                    if(response.message=="SUCCESS" && response.code=="200"){
                        messages(1);
                    }else if(response.code==404){
                        messages(2)
                    }
                    $("#key-security").attr("data-key",response.token.key);
                    $("#value-security").attr("data-value",response.token.value);
                },
                error : function(){
                    messages(3);
                }
            });
        })
        $("#description").ckeditor().editor.on('change', function(e) {
            $('#newInstructor').formValidation('revalidateField', 'description');
        });
    }
/* End */

/* Alliances */
    if($("#newAlliance").length>=1){
        (function($){
            $.fn.datepicker.dates['es'] = {
                days: ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo"],
                daysShort: ["Dom", "Lun", "Mar", "Mié", "Jue", "Vie", "Sáb", "Dom"],
                daysMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa", "Do"],
                months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
                monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
                today: "Hoy",
                clear: "Borrar",
                weekStart: 1,
                format: "dd/mm/yyyy"
            };
        }(jQuery));
        $('.datepicker').datepicker({
            format: 'dd/mm/yyyy',
            language : "es",
            autoclose:true
        }).on('changeDate', function(e) {
            $('#newAlliance').formValidation('revalidateField', 'beginning');
            $('#newAlliance').formValidation('revalidateField', 'expiration');
        });
        $('#newAlliance').formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            excluded: ':disabled',
            fields: {
                title: {
                    validators: {
                        notEmpty: {
                            message: 'El Nombre de la alianza es un campo requerido y no puede estar vacío.'
                        },
                        stringLength: {
                            min: 5,
                            max: 100,
                            message: 'El Nombre de la alianza debe ser mayor de 4 y menor de 45 caracteres de longitud.'
                        }
                    }
                },
                image: {
                    validators: {
                        notEmpty: {
                            message: 'La imagen principal es necesaria y no puede estar vacía.'
                        }
                    }
                },
                agreement: {
                    validators: {
                        notEmpty: {
                            message: 'El Archivo de la alianza es necesario y no puede estar vacía.'
                        }
                    }
                },
                imageI: {
                    validators: {
                        notEmpty: {
                            message: 'La imagen principal es campo necesario y no puede estar vacío.'
                        }
                    }
                },
                pdfI: {
                    validators: {
                        notEmpty: {
                            message: 'El Currículum Vítae es archivo necesario y no puede estar vacío.'
                        }
                    }
                },
                status: {
                    validators: {
                        notEmpty: {
                            message: 'El status es un campo requerido y no puede estar vacío.'
                        }
                    }
                },
                beginning: {
                    validators: {
                        notEmpty: {
                            message: 'La fecha de contratación es un campo requerido y no puede estar vacío.'
                        }
                    }
                },
                expiration: {
                    validators: {
                        notEmpty: {
                            message: 'La fecha de vencimiento es un campo requerido y no puede estar vacío.'
                        }
                    }
                }
            }
        }).on('success.form.fv', function(e) {
            e.preventDefault();
            $("#message-box-info").toggleClass("open");
            var values = {
                key: $('#key-security').attr('data-key'),
                value: $('#value-security').attr('data-value')
            };
            var data = $(this).serialize()+"&"+jQuery.param(values);
            $.ajax({
                type : "POST",
                url : "/dashboard/alliance/save",
                data : data,
                dataType : "json",
                success : function(response){
                    if(response.message=="SUCCESS" && response.code==200){
                        messages(1);
                        setTimeout(function(){
                            window.location = "/dashboard/alliances";
                        },2000);
                    }else if(response.code==404){
                        messages(2)
                    }
                    $("#key-security").attr("data-key",response.token.key);
                    $("#value-security").attr("data-value",response.token.value);
                },
                error : function(){
                    messages(3);
                }
            });
        });
    }
    if($("#newAllianceEdit").length>=1){
        (function($){
            $.fn.datepicker.dates['es'] = {
                days: ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo"],
                daysShort: ["Dom", "Lun", "Mar", "Mié", "Jue", "Vie", "Sáb", "Dom"],
                daysMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa", "Do"],
                months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
                monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
                today: "Hoy",
                clear: "Borrar",
                weekStart: 1,
                format: "dd/mm/yyyy"
            };
        }(jQuery));
        $('.datepicker').datepicker({
            format: 'dd/mm/yyyy',
            language : "es",
            autoclose:true
        }).on('changeDate', function(e) {
            $('#newAllianceEdit').formValidation('revalidateField', 'beginning');
            $('#newAllianceEdit').formValidation('revalidateField', 'expiration');
        });
        $('#newAllianceEdit').formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            excluded: ':disabled',
            fields: {
                title: {
                    validators: {
                        notEmpty: {
                            message: 'El Nombre de la alianza es un campo requerido y no puede estar vacío.'
                        },
                        stringLength: {
                            min: 5,
                            max: 100,
                            message: 'El Nombre de la alianza debe ser mayor de 4 y menor de 45 caracteres de longitud.'
                        }
                    }
                },
                image: {
                    validators: {
                        notEmpty: {
                            message: 'La imagen principal es necesaria y no puede estar vacía.'
                        }
                    }
                },
                agreement: {
                    validators: {
                        notEmpty: {
                            message: 'El Archivo de la alianza es necesario y no puede estar vacía.'
                        }
                    }
                },
                imageI: {
                    validators: {
                        notEmpty: {
                            message: 'La imagen principal es campo necesario y no puede estar vacío.'
                        }
                    }
                },
                pdfI: {
                    validators: {
                        notEmpty: {
                            message: 'El Currículum Vítae es archivo necesario y no puede estar vacío.'
                        }
                    }
                },
                status: {
                    validators: {
                        notEmpty: {
                            message: 'El status es un campo requerido y no puede estar vacío.'
                        }
                    }
                },
                beginning: {
                    validators: {
                        notEmpty: {
                            message: 'La fecha de contratación es un campo requerido y no puede estar vacío.'
                        }
                    }
                },
                expiration: {
                    validators: {
                        notEmpty: {
                            message: 'La fecha de vencimiento es un campo requerido y no puede estar vacío.'
                        }
                    }
                }
            }
        }).on('success.form.fv', function(e) {
            e.preventDefault();
            $("#message-box-info").toggleClass("open");
            var values = {
                key: $('#key-security').attr('data-key'),
                value: $('#value-security').attr('data-value')
            };
            var data = $(this).serialize()+"&"+jQuery.param(values);
            $.ajax({
                type : "POST",
                url : "/dashboard/alliance/update",
                data : data,
                dataType : "json",
                success : function(response){
                    if(response.message=="SUCCESS" && response.code==200){
                        messages(1);
                    }else if(response.code==404){
                        messages(2)
                    }
                    $("#key-security").attr("data-key",response.token.key);
                    $("#value-security").attr("data-value",response.token.value);
                },
                error : function(){
                    messages(3);
                }
            });
        });
    }
/* End */

/* Subscribers */
    if($("#editSubscriber").length>=1){
        $('#editSubscriber').formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            excluded: ':disabled',
            fields: {
                name: {
                    validators: {
                        notEmpty: {
                            message: 'El Nombre del suscriptor es un campo requerido y no puede estar vacío.'
                        },
                        stringLength: {
                            min: 5,
                            max: 65,
                            message: 'El Nombre del suscriptor debe ser mayor de 4 y menor de 65 caracteres de longitud.'
                        }
                    }
                },
                status: {
                    validators: {
                        notEmpty: {
                            message: 'El status es un campo requerido y no puede estar vacío.'
                        }
                    }
                },
                email: {
                    validators: {
                        notEmpty: {
                            message: 'Es necesaria la dirección de correo electrónico y esta no puede estar vacía'
                        }
                    }
                }
            }
        }).on('success.form.fv', function(e) {
            e.preventDefault();
            $("#message-box-info").toggleClass("open");
            var values = {
                key: $('#key-security').attr('data-key'),
                value: $('#value-security').attr('data-value')
            };
            var data = $(this).serialize()+"&"+jQuery.param(values);
            $.ajax({
                type : "POST",
                url : "/dashboard/mail/update",
                data : data,
                dataType : "json",
                success : function(response){
                    if(response.message=="SUCCESS" && response.code==200){
                        messages(1);
                    }else if(response.code==404){
                        messages(2)
                    }
                    $("#key-security").attr("data-key",response.token.key);
                    $("#value-security").attr("data-value",response.token.value);
                },
                error : function(){
                    messages(3);
                }
            });
        });
    }
/* End */

/* Slider */
    if($("#newSlider").length>=1){
        $('#newSlider').formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            excluded: ':disabled',
            fields: {
                title: {
                    validators: {
                        stringLength: {
                            min: 10,
                            max: 45,
                            message: 'El titulo debe ser mayor de 10 y menor de 45 caracteres de longitud.'
                        }
                    }
                },
                date: {
                    validators: {
                        stringLength: {
                            min: 10,
                            max: 30,
                            message: 'La fecha debe ser mayor de 10 y menor de 22 caracteres de longitud.'
                        }
                    }
                },
                description: {
                    validators: {
                        stringLength: {
                            min: 50,
                            max: 200,
                            message: 'La Descripción debe ser mayor de 50 y menor de 200 caracteres de longitud.'
                        }
                    }
                },
                url_direction: {
                    validators: {
                        stringLength: {
                            min: 10,
                            max: 150,
                            message: 'La Descripción debe ser mayor de 10 y menor de 150 caracteres de longitud.'
                        },
                        uri: {
                            message: 'La url no es valida'
                        }
                    }
                },
                image: {
                    validators: {
                        notEmpty: {
                            message: 'La imagen principal es necesaria y no puede estar vacía.'
                        }
                    }
                },
                imageI: {
                    validators: {
                        notEmpty: {
                            message: 'La imagen principal es campo necesario y no puede estar vacío.'
                        }
                    }
                },
                status: {
                    validators: {
                        notEmpty: {
                            message: 'El status es un campo requerido y no puede estar vacío.'
                        }
                    }
                }
            }
        }).on('success.form.fv', function(e) {
            e.preventDefault();
            $("#message-box-info").toggleClass("open");
            var values = {
                key: $('#key-security').attr('data-key'),
                value: $('#value-security').attr('data-value')
            };
            var data = $(this).serialize()+"&"+jQuery.param(values);
            $.ajax({
                type : "POST",
                url : "/dashboard/index/save",
                data : data,
                dataType : "json",
                success : function(response){
                    if(response.message=="SUCCESS" && response.code==200){
                        messages(1);
                        setTimeout(function(){
                            window.location = "/dashboard/index/slider";
                        },2000);
                    }else if(response.code==404){
                        messages(2)
                    }
                    $("#key-security").attr("data-key",response.token.key);
                    $("#value-security").attr("data-value",response.token.value);
                },
                error : function(){
                    messages(3);
                }
            });
        })
    }
    if($("#editSlider").length>=1){
        $('#editSlider').formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            excluded: ':disabled',
            fields: {
                title: {
                    validators: {
                        stringLength: {
                            min: 10,
                            max: 45,
                            message: 'El titulo debe ser mayor de 10 y menor de 45 caracteres de longitud.'
                        }
                    }
                },
                date: {
                    validators: {
                        stringLength: {
                            min: 10,
                            max: 30,
                            message: 'La fecha debe ser mayor de 10 y menor de 22 caracteres de longitud.'
                        }
                    }
                },
                description: {
                    validators: {
                        stringLength: {
                            min: 50,
                            max: 200,
                            message: 'La Descripción debe ser mayor de 50 y menor de 200 caracteres de longitud.'
                        }
                    }
                },
                url_direction: {
                    validators: {
                        stringLength: {
                            min: 10,
                            max: 150,
                            message: 'La Descripción debe ser mayor de 10 y menor de 150 caracteres de longitud.'
                        },
                        uri: {
                            message: 'La url no es valida'
                        }
                    }
                },
                image: {
                    validators: {
                        notEmpty: {
                            message: 'La imagen principal es necesaria y no puede estar vacía.'
                        }
                    }
                },
                imageI: {
                    validators: {
                        notEmpty: {
                            message: 'La imagen principal es campo necesario y no puede estar vacío.'
                        }
                    }
                },
                status: {
                    validators: {
                        notEmpty: {
                            message: 'El status es un campo requerido y no puede estar vacío.'
                        }
                    }
                }
            }
        }).on('success.form.fv', function(e) {
            e.preventDefault();
            $("#message-box-info").toggleClass("open");
            var values = {
                key: $('#key-security').attr('data-key'),
                value: $('#value-security').attr('data-value')
            };
            var data = $(this).serialize()+"&"+jQuery.param(values);
            $.ajax({
                type : "POST",
                url : "/dashboard/index/update",
                data : data,
                dataType : "json",
                success : function(response){
                    if(response.message=="SUCCESS" && response.code==200){
                        messages(1);
                    }else if(response.code==404){
                        messages(2)
                    }
                    $("#key-security").attr("data-key",response.token.key);
                    $("#value-security").attr("data-value",response.token.value);
                },
                error : function(){
                    messages(3);
                }
            });
        })
    }
/* End */

/*Categories*/
    if($("#newCategory").length>=1){
        $('#newCategory').formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                name_category: {
                    validators: {
                        notEmpty: {
                            message: 'Este campo es necesario y no puede estar vacío.'
                        },
                        remote: {
                            message: 'Esta categoría ya existe, pruebe con otra',
                            url: '/dashboard/category/validatecategory',
                            type: 'POST'
                        }
                    }
                },
                confirmCategory: {
                    validators: {
                        identical: {
                            field: 'name_category',
                            message: 'La categoría no coincide.'
                        }
                    }
                }
            }
        }).on('success.form.fv', function(e) {
            e.preventDefault();
            $("#message-box-info").toggleClass("open");
            type = $("#type").val();
            var values = {
                key: $('#key-security').attr('data-key'),
                value: $('#value-security').attr('data-value')
            };
            var data = $(this).serialize()+"&"+jQuery.param(values);
            $.ajax({
                type : "POST",
                url : "/dashboard/category/new",
                data : data,
                dataType : "json",
                success : function(response){
                    if(response.message=="SUCCESS" && response.code==200){
                        $("#key-security").attr("data-key",response.token.key);
                        $("#value-security").attr("data-value",response.token.value);
                        setTimeout(function(){
                            $("#message-box-info").removeClass("open");
                            $("#message-box-success").toggleClass("open");
                            $("#modal_basic").modal('hide');
                        },1500);
                        setTimeout(function(){
                            $("#message-box-success").removeClass("open");
                        },4000);
                        switch (type){
                            case "1":$("#tableCategory tbody").append("<tr id='"+response.result.id+"'><td class='nameCategory'>"+response.result.name+"</td><td class='optionsCtg'><a href='#' data-namec='"+response.result.name+"' data-cgid='"+response.result.id+"' class='btn btn-default btn-rounded btn-sm btn-edit'><span class='fa fa-pencil'></span></a><button class='btn btn-danger btn-rounded btn-sm deleteCg' data-namec='"+response.result.name+"' data-cgid='"+response.result.id+"'><span class='fa fa-times'></span></button></td>").fadeIn(1000);
                                break;
                            case "2": $("#cid").select2({maximumSelectionLength:1,data :[{ id : response.result.id, text: response.result.name }]});
                                break;
                            default : alert("No se ha encontrado el estatus correcto");
                                break;
                        }
                        resetForm($("#newCategory"));
                    }else if(response.code==404){
                        $("#message-box-info").removeClass("open");
                        $("#message-box-warning").toggleClass("open");
                        setTimeout(function(){
                            $("#message-box-warning").removeClass("open");
                        },1500);
                    }
                },
                error : function(){
                    alert("Ha ocurrido un error");
                    $("#message-box-info").removeClass("open");
                }
            });
        });
        $('#modal_basic').on('shown.bs.modal', function() {
            $('#newCategory').formValidation('resetForm', true);
        });
    }
    if($("#edit_category").length>=1){
        var $this = null;
        var cgid,nameCg;
        $(document.body).on("click","#tableCategory tbody a.btn-edit",function(e){
            e.preventDefault();
            $this = $(this);
            cgid = $this.attr("data-cgid");
            nameCg = $this.attr("data-namec");
            $('#edit_category').modal();
            $("#nameCtg").val(nameCg);
        });
        $('#editCategory').formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                name_category: {
                    validators: {
                        notEmpty: {
                            message: 'Este campo es necesario y no puede estar vacío.'
                        },
                        remote: {
                            message: 'Esta categoría ya existe, pruebe con otra',
                            url: '/dashboard/category/validatecategory',
                            type: 'POST'
                        }
                    }
                },
                confirmCategory: {
                    validators: {
                        identical: {
                            field: 'name_category',
                            message: 'La categoría no coincide.'
                        }
                    }
                }
            }
        }).on('success.form.fv', function(e) {
            e.preventDefault();
            $("#message-box-info").toggleClass("open");
            var values = {
                key: $('#key-security').attr('data-key'),
                value: $('#value-security').attr('data-value'),
                id :cgid
            };
            var data = $(this).serialize()+"&"+jQuery.param(values);
            $.ajax({
                type : "POST",
                url : "/dashboard/category/edit",
                data : data,
                dataType : "json",
                success : function(response){
                    if(response.message=="SUCCESS" && response.code==200){
                        $("#key-security").attr("data-key",response.token.key);
                        $("#value-security").attr("data-value",response.token.value);
                        setTimeout(function(){
                            $("#message-box-info").removeClass("open");
                            $("#message-box-success").toggleClass("open");
                            $("#edit_category").modal("hide");
                        },1500);
                        setTimeout(function(){
                            $("#message-box-success").removeClass("open");
                        },4000);
                        $("#tableCategory tbody tr#"+cgid+" .nameCategory").text(response.result.name);
                        $("#tableCategory tbody tr#"+cgid+" .optionsCtg a").attr('data-namec',response.result.name);
                        resetForm($("#editCategory"));
                    }else if(response.code==404){
                        $("#message-box-info").removeClass("open");
                        $("#message-box-warning").toggleClass("open");
                        setTimeout(function(){
                            $("#message-box-warning").removeClass("open");
                        },1500);
                    }
                },
                error : function(){
                    $("#message-box-warning").toggleClass("open");
                    setTimeout(function(){
                        $("#message-box-warning").removeClass("open");
                    },1500);
                }
            });
        });
        $('#edit_category').on('shown.bs.modal', function() {
            $('#editCategory').formValidation('resetForm', true);
        });
    }
    if($(".deleteCg").length>=1){
        var nameCg,cid;
        $(document.body).on("click","#tableCategory tbody button.deleteCg",function(e){
            e.preventDefault();
            $this = $(this);
            cid = $this.attr("data-cgid");
            nameCg = $this.attr("data-namec");
            $("#cgValue").text(nameCg);
            $("#deleteCg").modal("show");
        })
        $("#deleteCg").submit(function(){
            $("#message-box-info").toggleClass("open");
            $.ajax({
                type : "POST",
                url : "/dashboard/category/delete",
                data : {id:cid,key: $('#key-security').attr('data-key'),value: $('#value-security').attr('data-value')},
                dataType : "json",
                success : function(response){
                    if(response.message=="SUCCESS" && response.code==200){
                        $("#message-box-info").removeClass("open");
                        $("#message-box-success").toggleClass("open");
                        setTimeout(function(){
                            $("#message-box-success").removeClass("open");
                            $("#deleteCg").modal('hide');
                        },3000);
                        $("#"+cid).hide("slow",function(){
                            $(this).remove();
                        });
                        $("#key-security").attr("data-key",response.token.key);
                        $("#value-security").attr("data-value",response.token.value);
                    }else if(response.code==404){
                        $("#message-box-info").removeClass("open");
                        $("#message-box-warning").toggleClass("open");
                        setTimeout(function(){
                            $("#message-box-warning").removeClass("open");
                        },3000);
                    }
                },
                error : function(){
                    $("#message-box-info").removeClass("open");
                    $("#message-box-warning").toggleClass("open");
                    setTimeout(function(){
                        $("#message-box-warning").removeClass("open");
                    },3000);
                }
            });
            return false;
        })
    }
/* End */

/* control notes */
    if($("#controlClient").length>=1){
        $('#controlClient').formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            excluded: ':disabled',
            fields: {
                observations: {
                    validators: {
                        notEmpty: {
                            message: 'La descripción del seguimiento es un campo requerido y no puede estar vacío.'
                        },
                        stringLength: {
                            min: 5,
                            message: 'La procedencia del dato debe ser mayor de 5 caracteres de longitud.'
                        }
                    }
                },
                call_status: {
                    validators: {
                        notEmpty: {
                            message: 'El estatus es un campo requerido y no puede estar vacío.'
                        }
                    }
                },
                scholarship: {
                    validators: {
                        notEmpty: {
                            message: 'Seleccione una opción'
                        }
                    }
                },
                call: {
                    validators: {
                        notEmpty: {
                            message: 'El estatus de la llamada es un campo requerido y no puede estar vacío.'
                        }
                    }
                },
                percentage: {
                    enabled: false,
                    validators: {
                        notEmpty: {
                            message: 'El porcentaje es un campo requerido y no puede estar vacío.'
                        }
                    }
                },
                "observations-scho": {
                    enabled: false,
                    validators: {
                        notEmpty: {
                            message: 'El motivo de la beca es un campo requerido y no puede estar vacío.'
                        },
                        stringLength: {
                            min: 5,
                            message: 'El motivo de la beca debe ser mayor de 5 caracteres de longitud.'
                        }
                    }
                }
            }
        }).on('success.form.fv', function(e,data) {
            e.preventDefault();
            $("#infoA").removeClass("hidden").addClass("in");
            $("#submitNote").attr("disabled","disabled").text("Guardando nota");
            $.ajax({
                type : "POST",
                url : "/dashboard/client/save-control",
                data :$(this).serialize(),
                dataType : "json",
                success : function(response){
                    if(response.code==200){
                        setTimeout(function(){
                            $("#infoA").removeClass("in").addClass("hidden");
                            $("#successA").removeClass("hidden").addClass("in");
                            var status = getStatus(response.content.call_status);
                            var icon = getIcon(response.content.call_status);
                            var call = getCall(response.content.call);
                            $("#today").prepend('<div class="timeline-item timeline-item-right"> <div class="timeline-item-info">'+response.content.hour+'</div> <div class="timeline-item-icon"><span class="fa '+icon+'"></span></div> <div class="tasks ui-sortable"> <div class="task-item '+status+'"> <div class="task-text ui-sortable-handle">'+response.content.observations+'</div> <div class="task-footer"> <div class="pull-left"><span class="fa '+icon+'">&nbsp;'+response.content.call_status+'</span></div><div class="pull-right"><span class="fa fa-phone"></span> '+call+'</div></div> </div> </div> </div>');
                            $('#call_status.selectU').select2('val',"");
                            $("#controlClient").data('formValidation').resetForm(true);
                            $('#noMarco').prop('checked', true);
                            $('#noScholarship').prop('checked', true);
                            if(response.redirect==1){
                                window.location.href="";
                            }
                        },500);
                        setTimeout(function(){
                            $("#successA").removeClass("in").addClass("hidden");
                            $("#submitNote").removeAttr("disabled","disabled").text("Guardar nota");
                        },1500)
                    }else{
                        setTimeout(function(){
                            $("#infoA").removeClass("in").addClass("hidden");
                            $("#warningA").removeClass("hidden").addClass("in");
                        },500);
                        setTimeout(function(){
                            $("#warningA").removeClass("in").addClass("hidden");
                            $("#submitNote").removeAttr("disabled","disabled").text("Guardar nota");
                        },1500)
                    }
                },
                error : function(){
                    setTimeout(function(){
                        $("#infoA").removeClass("in").addClass("hidden");
                        $("#warningA").removeClass("hidden").addClass("in");
                    },500);
                    setTimeout(function(){
                        $("#warningA").removeClass("in").addClass("hidden");
                        $("#submitNote").removeAttr("disabled","disabled").text("Guardar nota");
                    },1500)
                }
            });
        });
        $('#call_status').on("select2:select", function(response) {
            var status = response.target.value;
            if(status=="INSCRITO"){
                $("#scholarshipDiv").removeClass("hidden");
            }else{
                $("#scholarshipDiv").addClass("hidden");
                $('#noScholarship').prop('checked', true);
            }
        });
        $('#call_status').on("change", function(response) {
            $("#scholarshipDiv").addClass("hidden");
            $(".scholarship-true").addClass("hidden");
        });
        $("#controlClient input[name='scholarship']").click(function(){
            var radio = $(this).val();
            if(radio==="si"){
                $(".scholarship-true").removeClass("hidden");

                $('#controlClient').formValidation('enableFieldValidators', 'observations-scho',true);
                $('#controlClient').formValidation('revalidateField', 'observations-scho');
                $('#controlClient').formValidation('enableFieldValidators', 'percentage',true);
                $('#controlClient').formValidation('revalidateField', 'percentage');
            }else{
                $(".scholarship-true").addClass("hidden");
                $('#controlClient').formValidation('enableFieldValidators', 'observations-scho',false);
                $('#controlClient').formValidation('enableFieldValidators', 'percentage',false);
                $('#noScholarship').prop('checked', true);
            }
        });
        function getStatus(value){
            switch (value){
                case "SEGUIMIENTO" : return "task-warning"; break;
                case "CITA" : return "task-primary"; break;
                case "ATENDIDO" : return "task-info"; break;
                case "PERDIDO" : return "task-danger"; break;
                case "INSCRITO" : return "task-success"; break;
                default :break;
            }
        }
        function getIcon(value){
            switch (value){
                case "SEGUIMIENTO" : return "fa-user"; break;
                case "CITA" : return "fa-clock-o"; break;
                case "ATENDIDO" : return "fa-users"; break;
                case "PERDIDO" : return "fa-user-times"; break;
                case "INSCRITO" : return "fa-trophy"; break;
                default :break;
            }
        }
        function getCall(value){
            switch (value){
                case "1" : return "EFECTIVA"; break;
                case "2" : return "NO EFECTIVA"; break;
                case "3" : return "NO MARCO"; break;
                default :break;
            }
        }
    }
    if($("#formScholarship").length>=1){
        $('#formScholarship').formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            excluded: ':disabled',
            fields: {
                percentage: {
                    validators: {
                        notEmpty: {
                            message: 'El porcentaje es un campo requerido y no puede estar vacío.'
                        }
                    }
                },
                "observations-scho": {
                    validators: {
                        notEmpty: {
                            message: 'El motivo de la beca es un campo requerido y no puede estar vacío.'
                        },
                        stringLength: {
                            min: 5,
                            message: 'El motivo de la beca debe ser mayor de 5 caracteres de longitud.'
                        }
                    }
                }
            }
        }).on('success.form.fv', function(e,data) {
            e.preventDefault();
            $("#message-box-info").toggleClass("open");
            $.ajax({
                type : "POST",
                url : "/dashboard/client/save-scholarship-control",
                data :$(this).serialize(),
                dataType : "json",
                success : function(response){
                    if(response.code==200){
                        setTimeout(function(){
                            messages(1);
                        },500);
                    }else{
                        messages(2);
                    }
                },
                error : function(){
                    messages(3);
                }
            });
        });
    }
    if($("#formMessage").length>=1){
        /* Image */
        if($("#sendMailPersonal").length>=1){
            Dropzone.autoDiscover = false;
            var myDropzone = new Dropzone('#sendMailPersonal', {
                url: "/dashboard/client/uploadfile",
                uploadMultiple : true,
                paramName: "file", // The name that will be used to transfer the file
                maxFilesize: 5, // MB
                maxFiles: 4 ,
                parallelUploads : 1,
                addRemoveLinks : true,
                dictResponseError: "No se puede subir esta archivo!",
                autoProcessQueue: true,
                thumbnailWidth: 138,
                thumbnailHeight: 120,
                previewTemplate: '<div class="dz-preview dz-file-preview"><div class="dz-details"><div class="dz-filename"><span data-dz-name style=font-weight: bold;"></span></div><div class="dz-size"><span data-dz-size></span></div></div><div class="dz-error-message"><span data-dz-errormessage></span></div><div class="progress progress-striped active"><div class="progress-bar progress-bar-success" data-dz-uploadprogress></div></div></div>',
                resize: function(file) {
                    var info = { srcX: 0, srcY: 0, srcWidth: file.width, srcHeight: file.height },
                        srcRatio = file.width / file.height;
                    if (file.height > this.options.thumbnailHeight || file.width > this.options.thumbnailWidth) {
                        info.trgHeight = this.options.thumbnailHeight;
                        info.trgWidth = info.trgHeight * srcRatio;
                        if (info.trgWidth > this.options.thumbnailWidth) {
                            info.trgWidth = this.options.thumbnailWidth;
                            info.trgHeight = info.trgWidth / srcRatio;
                        }
                    } else {
                        info.trgHeight = file.height;
                        info.trgWidth = file.width;
                    }
                    return info;
                },
                sending : function(file, xhr, formData){
                },
                success: function(file, response){
                    var values= jQuery.parseJSON(response);
                    $(".dz-preview").addClass("dz-success");
                    $("div.progress").remove();
                    file.previewElement.accessKey = values.id;
                    file.previewElement.id = values.name;
                    $("#sendMailPersonal").append('<input type="hidden" value="'+values.route+'" id="'+values.id+'" name="multiple-images[]">');
                },
                removedfile: function(file) {
                    id = file.previewElement.accessKey;
                    name = file.previewElement.id;
                    $.ajax({
                        url: "/dashboard/client/deleted-file",
                        type: "post",
                        data: {name:name},
                        dataType:"json",
                        success : function(response){
                            if(response.code==200){
                                $(file.previewElement).remove();
                                $("#"+id).remove();
                            }else{
                                $(file.previewElement).remove();
                                $("#"+id).remove();
                            }
                        },
                        error : function(){
                            alert("Ha ocurrido un error");
                        }
                    });
                }
            });
        }
        /* Image */
        /* Form */
        $('#formMessage').formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            excluded: ':disabled',
            fields: {
                email: {
                    validators: {
                        notEmpty: {
                            message: 'La cuenta de correo es un campo requerido y no puede estar vacío.'
                        }
                    }
                },
                subject: {
                    validators: {
                        notEmpty: {
                            message: 'El asunto es un campo requerido y no puede estar vacío.'
                        }
                    }
                },
                message: {
                    validators: {
                        notEmpty: {
                            message: 'El mensaje es un campo requerido y no puede estar vacía.'
                        },
                        callback: {
                            message: 'El mensaje debe de tener mas de 5 caracteres.',
                            callback: function(value, validator, $field) {
                                if (value === '') {
                                    return true;
                                }
                                var div  = $('<div/>').html(value).get(0),
                                    text = div.textContent || div.innerText;
                                return text.length >= 5;
                            }
                        }
                    }
                }
            }
        }).on('success.form.fv', function(e) {
            e.preventDefault();
            $("#message-box-info").toggleClass("open");
            $("#submitMessage").attr("disabled","disabled").text("Enviando mensaje");
            $.ajax({
                type : "POST",
                url : "/dashboard/client/send-mail",
                data :$(this).serialize(),
                dataType : "json",
                success : function(response){
                    if(response.code==200){
                        messages(1);
                        myDropzone.removeAllFiles();
                        $("#formMessage").data('formValidation').resetForm(true);
                        $("input[name='multiple-images[]']").remove();
                        $("#emailMessage").val(response.email);
                    }else{
                        messages(2);
                    }
                    $("#submitMessage").removeAttr("disabled","disabled").text("Enviar");
                },
                error : function(){
                    $("#submitMessage").removeAttr("disabled","disabled").text("Enviar");
                    messages(3);
                }
            });
        });
        $("#messageCK").ckeditor().editor.on('change', function(e) {
            $('#formMessage').formValidation('revalidateField', 'message');
        });
    }
/* End control notes */



/* Ckeditor */
    if($("#objective").length>=1){
        $( 'textarea.ckeditor' ).ckeditor();
    }

/* General select2 */
    if($(".selectM").length>=1){
        $(".selectM").select2({
            placeholder: $(".selectM").data("placehorder")
        });
    }
    if($(".selectU").length>=1){
        $(".selectU").select2({
            maximumSelectionLength:1,
            placeholder: $(".selectU").data("placehorder")
        });
    }
/* End */


});
/* Functions */
function resetForm($form) {
    $form.find('input:text, input:password, input:file, input[type=email], select, textarea').val('');
    $form.find('input:radio, input:checkbox').removeAttr('checked').removeAttr('selected');
}
function messages($type){
    if($type==1){
        setTimeout(function(){
            $("#message-box-info").removeClass("open");
            $("#message-box-success").toggleClass("open");
        },1500);
        setTimeout(function(){
            $("#message-box-success").removeClass("open");
        },2000);
    }
    else if($type==2){
        setTimeout(function(){
            $("#message-box-info").removeClass("open");
            $("#message-box-warning").toggleClass("open");
        },1500);
        setTimeout(function(){
            $("#message-box-warning").removeClass("open");
        },3000);
    }else if($type==3){
        setTimeout(function(){
            $("#message-box-info").removeClass("open");
            $("#message-box-warning").toggleClass("open");
        },1500);
        setTimeout(function(){
            $("#message-box-warning").removeClass("open");
        },3000);
    }
}
function getSection($selector,$placeholder,$ap){
    var result ;
    $selector.select2({
        ajax: {
            url: "/dashboard/process/feed",
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    q:params.term,
                    ap : $ap
                };
            },
            processResults: function (data, page) {
                return {
                    results: $.map(data, function (item,key) {
                        result = {"value":item,"pid":key}
                        return {
                            text: item,
                            id: key
                        }

                    })
                };
            }
        },
        escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
        minimumInputLength: 1,
        placeholder: $placeholder
    });
    $selector.on("change",function(e){
        console.log($selector.selector);
        console.log($selector.val());
        if($selector.selector!="#searchUser"){
            var theID = $selector.val();
            console.log(theID+" "+$selector.data("order")+" "+$selector.data("last")+" "+$selector.data("cgid"));
        }

    });
}
function getMunicipios(selector,name,form_municipios){
    selector.select2({
        maximumSelectionLength:1
    }).on("select2:select", function(response) {
        estado = response.target.value;
        $.ajax({
            data : {estado:estado},
            url : "/dashboard/client/getmunicipios",
            type : "POST",
            dataType : "json",
            success:function(response){
                if(response.code==200){
                    mpid = $("#"+name);
                    mpid.parent().removeClass("hidden");
                    var sel = $("select[name='"+name+"']");
                    $.each(response.result,function(key,value){
                        sel.append($("<option class='options'>").attr('value',value['id']).text(value['value']));
                    });
                    mpid.select2({
                        maximumSelectionLength:1
                    }).focus();
                }
            }
        });
    }).on("select2:unselect",function(){
        mpid = $("#"+name);
        mpid.select2('val',"");
        form_municipios.formValidation('revalidateField',name);
        mpid.find(".options").remove();
    });
    eid_estados = $("#eid_estados");
    if(eid_estados.length>=1 && eid_estados.data('value')==1){
        $("#"+name).select2({
            maximumSelectionLength:1
        }).focus();
    }
    eid_estados2 = $("#eid_estados2");
    if(eid_estados2.length>=1 && eid_estados2.data('value')==1){
        $("#"+name).select2({
            maximumSelectionLength:1
        }).focus();
    }
    return false;
}
function uploadPDF(selector,pyid){
    Dropzone.autoDiscover = false;
    var myDropzone;
    myDropzone = new Dropzone(selector, {
        url: "/dashboard/process/uploadfile",
        uploadMultiple : false,
        paramName: "file", // The name that will be used to transfer the file
        //maxFilesize: 5, // MB
        maxFiles: 2 ,
        parallelUploads : 1,
        addRemoveLinks : true,
        dictResponseError: "No se puede subir este archivo!",
        autoProcessQueue: true,
        thumbnailWidth: 138,
        thumbnailHeight: 120,
        acceptedFiles:".pdf",
        previewTemplate: '<div class="dz-preview dz-file-preview"><div class="dz-details"><div class="dz-filename"><span data-dz-name style:"font-weight: bold;"></span></div><div class="dz-size">File size: <span data-dz-size></span></div><div class="dz-thumbnail-wrapper"><div class="dz-thumbnail"><img data-dz-thumbnail><span class="dz-nopreview">No preview</span><div class="dz-success-mark"><i class="fa fa-check-circle-o"></i></div><div class="dz-error-mark"><i class="fa fa-times-circle-o"></i></div><div class="dz-error-message"><span data-dz-errormessage></span></div></div></div></div><div class="progress progress-striped active"><div class="progress-bar progress-bar-success" data-dz-uploadprogress></div></div></div>',
        resize: function(file) {
            var info = { srcX: 0, srcY: 0, srcWidth: file.width, srcHeight: file.height },
                srcRatio = file.width / file.height;
            if (file.height > this.options.thumbnailHeight || file.width > this.options.thumbnailWidth) {
                info.trgHeight = this.options.thumbnailHeight;
                info.trgWidth = info.trgHeight * srcRatio;
                if (info.trgWidth > this.options.thumbnailWidth) {
                    info.trgWidth = this.options.thumbnailWidth;
                    info.trgHeight = info.trgWidth / srcRatio;
                }
            } else {
                info.trgHeight = file.height;
                info.trgWidth = file.width;
            }
            return info;
        },
        sending : function(file, xhr, formData){
            formData.append("pyid",pyid);
        },
        success: function(file, response){
            var name_image = jQuery.parseJSON(response);
            $(".dz-preview").addClass("dz-success");
            $("div.progress").remove();
            $("#pdfI").val(name_image.name);
            pdf_view = $("a#pdf_view");
            pdf_view.attr("href",name_image.url);
            pdf_view.parent().removeClass("hidden");
            $("."+pyid).attr("data-href",name_image.url).attr("data-voucher",name_image.name)
        },
        removedfile: function(file) {
            $(file.previewElement).remove();
            $("#pdfI").val('');
        }
    });

    $("#upload_payments").on("hidden.bs.modal",function(){
        myDropzone.destroy();
    });
}