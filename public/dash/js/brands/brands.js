$(document).ready(function(){
    var formElement = $("#formElement");
    if(formElement.length>=1){
        $(document.body).on("click","table tbody tr td span.editElement",function(e){
            $("#name").val($(this).attr("data-name"));
            $("#permalink").val($(this).attr("data-permalink"));
            $("#brid").val($(this).parent().attr("data-id"));
            $("#defModalHead").text("Editar Servicio");
            $(".btn-submit-element").val("Actualizar");
            $("#modalElement").modal();
        });
        $("#newElement").on("click",function(e){
            $("#defModalHead").text("Agregar Marca");
            $(".btn-submit-element").val("Guardar");
            $("#brid").val("");
            $("#modalElement").modal();
        });
        getPermalink($("#name"),"/dashboard/brands/permalink",$("#permalink"),formElement);
        deleteRow("deleteElement","/dashboard/brands/delete");
        formElement.formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            excluded: ':disabled',
            fields: {
                name: {
                    validators: {
                        notEmpty: {
                            message: 'Este campo es requerido y no puede estar vacío'
                        },
                        stringLength: {
                            min: 2,
                            max: 100,
                            message: 'Este campo debe tener como mínimo 2 caracteres y como máximo 100.'
                        }
                    }
                },
                permalink: {
                    validators: {
                        notEmpty: {
                            message: 'Este campo es requerido y no puede estar vacío'
                        },
                        stringLength: {
                            min: 2,
                            max: 150,
                            message: 'Este campo debe tener como mínimo 2 caracteres y como máximo 150.'
                        }
                    }
                }
            }
        }).on('success.form.fv', function(e) {
            e.preventDefault();
            $("#message-box-info").toggleClass("open");
            $.ajax({
                type : "POST",
                url : "/dashboard/brands/save",
                data : $(this).serialize(),
                dataType : "json",
                success : function(response){
                    if(response.message=="SUCCESS" && response.code==200){
                        if(response.type==1){
                            $("#tableTbody").append("<tr class='"+response.result.id+"'><td>"+response.result.id+"</td><td class='name'>"+response.result.name+"</td><td data-id='"+response.result.id+"'><span data-name='"+response.result.name+"' data-permalink='"+response.result.permalink+"' class='editElement' data-toggle='tooltip' data-placement='top' data-original-title='Editar Marca'><i class='fa fa-pencil-square-o fa-2x'></i></span>&nbsp;<span class='deleteElement' data-toggle='tooltip' data-placement='top' data-original-title='Eliminar Marca'><i class='fa fa-remove fa-2x'></i></span>&nbsp;</td></tr>");
                        }else{
                            row_update =$("tbody#tableTbody tr."+response.result.id+"");
                            row_update.find(".name").text(response.result.name);
                            row_update.find(".editElement").attr("data-name",response.result.name).attr("data-permalink",response.result.permalink);
                        }
                        messages(1);
                        setTimeout(function(){
                            $("#message-box-success").removeClass("open");
                            $("#modalElement").modal("hide");
                        },2000);
                        resetForm(formElement);
                        formElement.formValidation('resetForm', true);
                    }else{
                        messages(2);
                    }
                },
                error : function(){
                    messages(3)
                }
            });
        });
        $("#modalElement").on("hidden.bs.modal",function(){
            resetForm(formElement);
            formElement.formValidation('resetForm', true);
        });
    }
});
function getPermalink(selector,url,selector_permalink,form){
    selector.change(function(){
        name_service = $(this).val();
        id = $(this).attr("data-id");
        $.ajax({
            url : url,
            type : "POST",
            data : {name:name_service,id:id},
            dataType : "json",
            success : function(response){
                if(response.message=="SUCCESS" && response.code==200){
                    selector_permalink.val(response.permalink);
                    form.formValidation('revalidateField', 'permalink');
                }else{
                    alert("Ha ocurrido un error intente nuevamente.");
                }
            },error : function(){
                alert("Ha ocurrido un error intente nuevamente.");
            }
        });
    });
}
function deleteRow(selector,url){
    var box = $("#mb-remove-row");
    var id = "0";
    $(document.body).on("click","table tbody tr td span."+selector,function(){
        id = $(this).parent().attr("data-id");
        box.addClass("open");
    });
    box.find(".mb-control-yes").on("click",function(){
        $.ajax({
            url : url,
            type : "POST",
            data : {id:id},
            dataType : "json",
            success : function(response){
                box.removeClass("open");
                if(response.code==200){
                    $("."+id).hide("slow",function(){
                        $(this).remove();
                    });
                }else{
                    alert("No se ha podido eliminar este fila por que esta ligado a otros registros.");
                }
            },error : function(){
                alert("Ha ocurrido un error intente nuevamente.");
            }
        });
    });
}
function resetForm($form) {
    $form.find('input:text, input:password, input:file, input[type=email], select, textarea').val('');
    $form.find('input:radio, input:checkbox').removeAttr('checked').removeAttr('selected');
}
function messages($type){
    if($type==1){
        setTimeout(function(){
            $("#message-box-info").removeClass("open");
            $("#message-box-success").toggleClass("open");
        },1500);
        setTimeout(function(){
            $("#message-box-success").removeClass("open");
        },2000);
    }
    else if($type==2){
        setTimeout(function(){
            $("#message-box-info").removeClass("open");
            $("#message-box-warning").toggleClass("open");
        },1500);
        setTimeout(function(){
            $("#message-box-warning").removeClass("open");
        },3000);
    }else if($type==3){
        setTimeout(function(){
            $("#message-box-info").removeClass("open");
            $("#message-box-warning").toggleClass("open");
        },1500);
        setTimeout(function(){
            $("#message-box-warning").removeClass("open");
        },3000);
    }
}