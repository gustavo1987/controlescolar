
/* Session */

  function mostrarContrasena(){
      var tipo = document.getElementById("password");
      if(tipo.type == "password"){
          tipo.type = "text";
      }else{
          tipo.type = "password";
      }
  }


$('#mostrar_contrasena').click(function () {
    if($('#mostrar_contrasena').is(':checked')){
        $('#contrasena').attr('type', 'text');
    } 
    else {
        $('#contrasena').attr('type', 'password');
    }
});
if($("#session").length>=1){
    $('#session').formValidation({
        framework: 'bootstrap',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            email: {
                validators: {
                    notEmpty: {
                        message: 'Es necesaria la dirección de correo electrónico y esta no puede estar vacía'
                    },
                    emailAddress: {
                        message: 'Este campo no contiene una dirección de correo electrónico válida.'
                    }
                }
            },
            password: {
                validators: {
                    notEmpty: {
                        message: 'La contraseña es necesaria y no puede estar vacía.'
                    }
                }
            }
        }
    }).on('success.form.fv', function(e) {
        e.preventDefault();
        $("#session-loading").removeClass("hide").addClass("in");
        $("#btn-submit").attr("disabled","disabled").val("Iniciando");
        $("#container-messages").removeClass("hide").addClass("in");
        var values = {
            key: $('#key-security').attr('data-key')
        };
        var data = $(this).serialize()+"&"+jQuery.param(values);
        $.ajax({
            type : "POST",
            url : "/dashboard/login/session",
            data : data,
            dataType : "JSON",
            success : function(response){
                if(response.message=="SUCCESS" && response.code==200){
                    window.location = response.url;
                }else if(response.code==300){
                    $("#password-incorrect").removeClass("hide").addClass("in");
                }else if(response.code==400){
                    $("#email-incorrect").removeClass("hide").addClass("in");
                }
                else if(response.code==404){
                    $("#all-incorrect").removeClass("hide").addClass("in");
                }
                $("#session-loading").removeClass("in").addClass("hide");
                $("#btn-submit").removeAttr("disabled","disabled").val("Iniciar");
            },
            error : function(){

            },
            complete : function(){
                setTimeout(function(){
                    $("#password-incorrect").removeClass("in").addClass("hide");
                    $("#email-incorrect").removeClass("in").addClass("hide");
                    $("#all-incorrect").removeClass("in").addClass("hide");
                    $("#container-messages").removeClass("in").addClass("hide");
                },3000);
            }
        });
    });
}
/* End */