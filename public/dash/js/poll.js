$(document).ready(function(){
    if($("#poll").length>=1){
        $('#poll').formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            excluded: ':disabled',
            fields: {
                p1: {
                    validators: {
                        notEmpty: {
                            message: 'Este campo es requerido y no puede estar vacío.'
                        }
                    }
                },p2: {
                    validators: {
                        notEmpty: {
                            message: 'Este campo es requerido y no puede estar vacío.'
                        }
                    }
                },p3: {
                    validators: {
                        notEmpty: {
                            message: 'Este campo es requerido y no puede estar vacío.'
                        }
                    }
                },p4: {
                    validators: {
                        notEmpty: {
                            message: 'Este campo es requerido y no puede estar vacío.'
                        }
                    }
                },p5: {
                    validators: {
                        notEmpty: {
                            message: 'Este campo es requerido y no puede estar vacío.'
                        }
                    }
                },p6: {
                    validators: {
                        notEmpty: {
                            message: 'Este campo es requerido y no puede estar vacío.'
                        }
                    }
                },p7: {
                    validators: {
                        notEmpty: {
                            message: 'Este campo es requerido y no puede estar vacío.'
                        }
                    }
                },p8: {
                    validators: {
                        notEmpty: {
                            message: 'Este campo es requerido y no puede estar vacío.'
                        }
                    }
                },p9: {
                    validators: {
                        notEmpty: {
                            message: 'Este campo es requerido y no puede estar vacío.'
                        }
                    }
                },p10: {
                    validators: {
                        notEmpty: {
                            message: 'Este campo es requerido y no puede estar vacío.'
                        }
                    }
                },p11: {
                    validators: {
                        notEmpty: {
                            message: 'Este campo es requerido y no puede estar vacío.'
                        }
                    }
                },p12: {
                    validators: {
                        notEmpty: {
                            message: 'Este campo es requerido y no puede estar vacío.'
                        }
                    }
                },p13: {
                    validators: {
                        notEmpty: {
                            message: 'Este campo es requerido y no puede estar vacío.'
                        }
                    }
                },p14: {
                    validators: {
                        notEmpty: {
                            message: 'Este campo es requerido y no puede estar vacío.'
                        }
                    }
                },p15: {
                    validators: {
                        notEmpty: {
                            message: 'Este campo es requerido y no puede estar vacío.'
                        }
                    }
                },p16: {
                    validators: {
                        notEmpty: {
                            message: 'Este campo es requerido y no puede estar vacío.'
                        }
                    }
                },p17: {
                    validators: {
                        notEmpty: {
                            message: 'Este campo es requerido y no puede estar vacío.'
                        }
                    }
                },p18: {
                    validators: {
                        notEmpty: {
                            message: 'Este campo es requerido y no puede estar vacío.'
                        }
                    }
                },p19: {
                    validators: {
                        notEmpty: {
                            message: 'Este campo es requerido y no puede estar vacío.'
                        }
                    }
                },p20: {
                    validators: {
                        notEmpty: {
                            message: 'Este campo es requerido y no puede estar vacío.'
                        }
                    }
                },p21: {
                    validators: {
                        notEmpty: {
                            message: 'Este campo es requerido y no puede estar vacío.'
                        }
                    }
                },p21: {
                    validators: {
                        notEmpty: {
                            message: 'Este campo es requerido y no puede estar vacío.'
                        }
                    }
                },p22: {
                    validators: {
                        notEmpty: {
                            message: 'Este campo es requerido y no puede estar vacío.'
                        }
                    }
                },p23: {
                    validators: {
                        notEmpty: {
                            message: 'Este campo es requerido y no puede estar vacío.'
                        }
                    }
                },p24: {
                    validators: {
                        notEmpty: {
                            message: 'Este campo es requerido y no puede estar vacío.'
                        }
                    }
                },p25: {
                    validators: {
                        notEmpty: {
                            message: 'Este campo es requerido y no puede estar vacío.'
                        }
                    }
                },p26: {
                    validators: {
                        notEmpty: {
                            message: 'Este campo es requerido y no puede estar vacío.'
                        }
                    }
                },p27: {
                    validators: {
                        notEmpty: {
                            message: 'Este campo es requerido y no puede estar vacío.'
                        }
                    }
                },p28: {
                    validators: {
                        notEmpty: {
                            message: 'Este campo es requerido y no puede estar vacío.'
                        }
                    }
                },p29: {
                    validators: {
                        notEmpty: {
                            message: 'Este campo es requerido y no puede estar vacío.'
                        }
                    }
                },p30: {
                    validators: {
                        notEmpty: {
                            message: 'Este campo es requerido y no puede estar vacío.'
                        }
                    }
                },p31: {
                    validators: {
                        notEmpty: {
                            message: 'Este campo es requerido y no puede estar vacío.'
                        }
                    }
                },p32: {
                    validators: {
                        notEmpty: {
                            message: 'Este campo es requerido y no puede estar vacío.'
                        }
                    }
                },p33: {
                    validators: {
                        notEmpty: {
                            message: 'Este campo es requerido y no puede estar vacío.'
                        }
                    }
                }
            }
        }).on('success.form.fv', function(e) {
            e.preventDefault();
            $("#info").removeClass("hidden").addClass("in");
            $("#submit").attr("disabled","disabled").text("Guardando");
            var values = {
                key: $('#key-security').attr('data-key'),
                value: $('#value-security').attr('data-value')
            };
            var data = $(this).serialize()+"&"+jQuery.param(values);
            $.ajax({
                type : "POST",
                url : "/dashboard/poll/save",
                data : data,
                dataType : "json",
                success : function(response){
                    if(response.code==200){
                        $("#info").removeClass("in").addClass("hidden");
                        $("#success").removeClass("hidden").addClass("in");
                        setTimeout(function(){
                            window.location.href="/";
                        },5000);
                    }else if(response.code==300){
                        $("#info").removeClass("in").addClass("hidden");
                        $("#danger").removeClass("hidden").addClass("in");
                        $("#submit").removeAttr("disabled","disabled").text("Reintentar");
                    }
                    $("#key-security").attr("data-key",response.key);
                    $("#value-security").attr("data-value",response.value);
                },
                error : function(){
                    $("#info").removeClass("in").addClass("hidden");
                    $("#danger").removeClass("hidden").addClass("in");
                    $("#submit").removeAttr("disabled","disabled").text("Reintentar");
                }
            });
        });
    }
})