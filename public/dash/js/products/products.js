window.onload = function() {
      $.ajax({
            type : "POST",
            url : "/dashboard/index/loadmessage",
            data : $(this).serialize(),
            dataType : "json",
            success : function(response){
                response.result.forEach(function(resul,index){
                    /*var source   = document.getElementById("hbs_participante").innerHTML;
                    var compileCode = Handlebars.compile(source);
                    var result = compileCode({namenoti: "hola"});
                    document.getElementById('append_pasticipantes').innerHTML = result;*/
                    Swal.fire({
                       position: 'top-end',
                       type: 'success',
                       title: resul.notification,
                       //background: '#fff url(/dash/images/ide.png)',
                       showConfirmButton: true,
                        showCloseButton: true
                    });
                })
                
                  
            },
            error : function(){
                messages(3)
            }
        });
    }

$(document).ready(function(){

    


    var buttonnotification = $("#loading_message").on('click',function(){
        $.ajax({
            type : "POST",
            url : "/dashboard/index/loadmessage",
            data : $(this).serialize(),
            dataType : "json",
            success : function(response){
                response.result.forEach(function(resul,index){
                    alert(resul.notification);
                    var source   = document.getElementById("hbs_participante").innerHTML;
                    //var source = $("#hbs_participante").html();
                    var compileCode = Handlebars.compile(source);
                    var result = compileCode({namenoti: "hola"});
                    document.getElementById('append_pasticipantes').innerHTML = result;
                    //$("#append_pasticipantes").html(result);
                    //$("#append_pasticipantes").append(result);
                })
                
                // Swal.fire({
                //   position: 'top-end',
                //   type: 'success',
                //   title: "response.result.notification",
                //   showConfirmButton: false,
                //   timer: 1500
                // }) 
            },
            error : function(){
                messages(3)
            }
        });
    });
    var newForm = $("#newProduct");
    if(newForm.length>=1){

        console.log("entra el formulario");
        getPermalink($("#name"),"/dashboard/products/permalink",$("#permalink"),newForm);
        newForm.formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            excluded: ':disabled',
            locale: 'es_ES',
            fields: {}
        }).on('success.form.fv', function(e) {
            e.preventDefault();
            $("#message-box-info").toggleClass("open");
            $.ajax({
                type : "POST",
                url : "/dashboard/products/save",
                data : $(this).serialize(),
                dataType : "json",
                success : function(response){
                    if(response.message=="SUCCESS" && response.code==200){
                        messages(1);
                        setTimeout(function(){
                            $("#message-box-success").removeClass("open");
                            $("#other_products").toggleClass("open");
                        },2000);
                        resetForm(newForm);
                        newForm.formValidation('resetForm', true);
                    }else{
                        messages(2);
                    }
                },
                error : function(){
                    messages(3)
                }
            });
        });
    }
    var formElement = $("#formElement");
    if(formElement.length>=1){
        $("#newElement").on("click",function(e){
            $("#defModalHead").text("Agregar Marca");
            $(".btn-submit-element").val("Guardar");
            $("#brid").val("");
            $("#modalElement").modal();
        });
        getPermalink($("#nameM"),"/dashboard/brands/permalink",$("#permalinkM"),formElement);
        formElement.formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            excluded: ':disabled',
            locale: 'es_ES',
            fields: {
                name: {
                    validators: {
                        notEmpty: {},
                        stringLength: {
                            min: 2,
                            max: 100,
                            message: 'Este campo debe tener como mínimo 2 caracteres y como máximo 100.'
                        }
                    }
                },
                permalink: {
                    validators: {
                        notEmpty: {},
                        stringLength: {
                            min: 2,
                            max: 150,
                            message: 'Este campo debe tener como mínimo 2 caracteres y como máximo 150.'
                        }
                    }
                }
            }
        }).on('success.form.fv', function(e) {
            e.preventDefault();
            $("#message-box-info").toggleClass("open");
            $.ajax({
                type : "POST",
                url : "/dashboard/brands/save",
                data : {"name":$("#nameM").val(),"permalink":$("#permalinkM").val()},
                dataType : "json",
                success : function(response){
                    if(response.message=="SUCCESS" && response.code==200){
                        if(response.type==1){
                            $('#brid').append($('<option>', {value: response.result.id, text: response.result.name}));
                        }
                        messages(1);
                        setTimeout(function(){
                            $("#message-box-success").removeClass("open");
                            $("#modalElement").modal("hide");
                        },2000);
                        resetForm(formElement);
                        formElement.formValidation('resetForm', true);
                    }else{
                        messages(2);
                    }
                },
                error : function(){
                    messages(3)
                }
            });
        });
        $("#modalElement").on("hidden.bs.modal",function(){
            resetForm(formElement);
            formElement.formValidation('resetForm', true);
        });
    }

    var editForm = $("#editProduct");
    if(editForm.length>=1){
        getPermalink($("#name"),"/dashboard/products/permalink",$("#permalink"),editForm);
        editForm.formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            excluded: ':disabled',
            locale: 'es_ES',
            fields: {
                name: {
                    validators: {
                        notEmpty: {},
                        stringLength: {
                            min: 2,
                            max: 100,
                            message: 'Este campo debe tener como mínimo 2 caracteres y como máximo 100.'
                        }
                    }
                },
                permalink: {
                    validators: {
                        notEmpty: {},
                        stringLength: {
                            min: 2,
                            max: 150,
                            message: 'Este campo debe tener como mínimo 2 caracteres y como máximo 150.'
                        }
                    }
                },
                sale_price: {
                    validators: {
                        notEmpty: {}
                    }
                },
                quantity: {
                    validators: {
                        notEmpty: {}
                    }
                },
                brid: {
                    validators: {
                        notEmpty: {}
                    }
                }
            }
        }).on('success.form.fv', function(e) {
            e.preventDefault();
            $("#message-box-info").toggleClass("open");
            $.ajax({
                type : "POST",
                url : "/dashboard/products/save",
                data : $(this).serialize(),
                dataType : "json",
                success : function(response){
                    if(response.message=="SUCCESS" && response.code==200){
                        messages(1);
                        setTimeout(function(){
                            $("#message-box-success").removeClass("open");
                        },2000);
                    }else{
                        messages(2);
                    }
                },
                error : function(){
                    messages(3)
                }
            });
        });
    }
    var tableProducts = $("#tableProducts");
    if(tableProducts.length>=1){
        deleteRow("deleteElement","/dashboard/products/delete");
        var id_product  = 0;
        $(document.body).on('click',"table#tableProducts tbody tr .inventoryElement",function (e) {
            e.preventDefault();
            id_product = $(this).parent().attr("data-id");
            if(id_product){
                $.ajax({
                    url : "/dashboard/products/get",
                    type : "POST",
                    data : {id:id_product},
                    dataType : "json",
                    success : function(response){
                        if(response.code==200){
                            $("#modalInventory").modal("show");
                            $("#trInventory").attr("data-id",response.result.stid).attr("data-pid",id_product);
                            $("#defModalHead").text(response.result.name+" '"+response.result.name_brand+"'");
                            $("#stock_max").val(response.result.stock_max);
                            $("#stock_min").val(response.result.stock_min);
                            $("#stock").val(response.result.stock);
                            $("#quantity").val(response.result.quantity_product);
                            $("#add").val(0);
                            $("#delete").val(0);
                        }else{
                            messages(2)
                        }
                    },error : function(){
                        messages(3)
                    }
                })
            }else{
                alert("No se ha encontrado el Id del producto, recargue la página y vuelva a intentarlo");
            }
        });
    }
    setStock($("#stock_max"),"stock_max");
    setStock($("#stock_min"),"stock_min");
    setStock($("#add"),"add");
    setStock($("#delete"),"delete");
});
function setStock(selector,param_type){
    var input = null;
    selector.dblclick(function(){
        input = $(this);
        input.removeAttr("readonly");
        input.focus();
    }).keypress(function(event){
        if ( event.which == 13 ) {
            event.preventDefault();
            if(input.val()!==0){
                $("#message-box-success").toggleClass("open");
                param_value = input.val();
                param_id = $("#trInventory").attr("data-id");
                pid = $("#trInventory").attr("data-pid");
                var data = {value:param_value,id:param_id,type:param_type,pid:pid};
                $.ajax({
                    type : "POST",
                    url : '/dashboard/products/stock',
                    data : data,
                    dataType : "json",
                    success : function(response){
                        $("#message-box-success").removeClass("open");
                        if(response.code==200){
                            $("#quantity").val(response.result.quantity_product);
                            $("#stock").val(response.result.stock);

                            //Div Percentage
                            div_percentage = $("tr."+response.result.pid+" td div").find(".percentage_stock");
                            $percentage = Math.round((response.result.stock / response.result.stock_max)*100);
                            div_percentage.css("width",$percentage+"%").text(response.result.stock);

                            if(response.result.stock==0) div_percentage.removeClass("progress-bar-warning progress-bar-success").addClass("progress-bar-danger status_danger");
                            else if(response.result.stock<=response.result.stock_min) div_percentage.removeClass("progress-bar-warning status_danger progress-bar-success").addClass("progress-bar-danger");
                            else if (response.result.stock>response.result.stock_min && response.result.stock<=response.result.stock_max) div_percentage.removeClass("progress-bar-warning progress-bar-danger").addClass("progress-bar-success");
                            else if (response.result.stock>response.result.stock_max) div_percentage.removeClass("progress-bar-success progress-bar-danger").addClass("progress-bar-warning");
                            //End Div Percentage
                            input.attr("readonly",true);
                            return false;

                        }else{
                            messages(2)
                        }
                    },
                    error : function(){
                        messages(3)
                    }
                });
            }else{
                alert("El valor debe ser mayor a 0")
            }
        }
    }).blur(function(){
        input.attr("readonly",true);
    });
}
function getPermalink(selector,url,selector_permalink,form){
    console.log("entra a la funcion");
    selector.change(function(){
        name_service = $(this).val();
        id = $(this).attr("data-id");
        $.ajax({
            url : url,
            type : "POST",
            data : {name:name_service,id:id},
            dataType : "json",
            success : function(response){
                if(response.message=="SUCCESS" && response.code==200){
                    selector_permalink.val(response.permalink);
                    form.formValidation('revalidateField', 'permalink');
                }else{
                    alert("Ha ocurrido un error intente nuevamente.");
                }
            },error : function(){
                alert("Ha ocurrido un error intente nuevamente.");
            }
        });
    });
}
function deleteRow(selector,url){
    var box = $("#mb-remove-row");
    var id = "0";
    $(document.body).on("click","table tbody tr td span."+selector,function(){
        id = $(this).parent().attr("data-id");
        box.addClass("open");
    });
    box.find(".mb-control-yes").on("click",function(){
        $.ajax({
            url : url,
            type : "POST",
            data : {id:id},
            dataType : "json",
            success : function(response){
                box.removeClass("open");
                if(response.code==200){
                    $("."+id).hide("slow",function(){
                        $(this).remove();
                    });
                }else{
                    alert("No se ha podido eliminar este fila por que esta ligado a otros registros.");
                }
            },error : function(){
                alert("Ha ocurrido un error intente nuevamente.");
            }
        });
    });
}
function resetForm($form) {
    $form.find('input:text, input:password, input:file, input[type=email], select, textarea').val('');
    $form.find('input:radio, input:checkbox').removeAttr('checked').removeAttr('selected');
}
function messages($type){
    if($type==1){
        setTimeout(function(){
            $("#message-box-info").removeClass("open");
            $("#message-box-success").toggleClass("open");
        },1500);
        setTimeout(function(){
            $("#message-box-success").removeClass("open");
        },2000);
    }
    else if($type==2){
        setTimeout(function(){
            $("#message-box-info").removeClass("open");
            $("#message-box-warning").toggleClass("open");
        },1500);
        setTimeout(function(){
            $("#message-box-warning").removeClass("open");
        },3000);
    }else if($type==3){
        setTimeout(function(){
            $("#message-box-info").removeClass("open");
            $("#message-box-warning").toggleClass("open");
        },1500);
        setTimeout(function(){
            $("#message-box-warning").removeClass("open");
        },3000);
    }
}