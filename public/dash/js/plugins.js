$(function() {
    var formElements = function(){
       //Bootstrap select
        var feSelect = function(){
            if($(".select").length > 0){
                $(".select").selectpicker();

                $(".select").on("change", function(){
                    if($(this).val() == "" || null === $(this).val()){
                        if(!$(this).attr("multiple"))
                            $(this).val("").find("option").removeAttr("selected").prop("selected",false);
                    }else{
                        $(this).find("option[value="+$(this).val()+"]").attr("selected",true);
                    }
                });
            }
        }
        var feTooltips = function(){
            $("body").tooltip({selector:'[data-toggle="tooltip"]',container:"body"});
        }

        var fePopover = function(){
            $("[data-toggle=popover]").popover();
            $(".popover-dismiss").popover({trigger: 'focus'});
        }
        var feTagsinput = function(){
            if($(".tagsinput").length > 0){

                $(".tagsinput").each(function(){

                    if($(this).data("placeholder") != ''){
                        var dt = $(this).data("placeholder");
                    }else
                        var dt = 'add a tag';

                    $(this).tagsInput({width: '100%',height:'auto',defaultText: dt});
                });

            }
        }
        var feiCheckbox = function(){
            if($(".icheckbox").length > 0){
                 $(".icheckbox,.iradio").iCheck({checkboxClass: 'icheckbox_minimal-grey',radioClass: 'iradio_minimal-grey'});
            }
        }

        var feBsFileInput = function(){

            if($("input.fileinput").length > 0)
                $("input.fileinput").bootstrapFileInput();

        }
        return {
		init: function(){
                    feSelect();
                    feTooltips();
                    fePopover();
                    feTagsinput();
                    feiCheckbox();
                    feBsFileInput();
                }
        }
    }();

    var uiElements = function(){

        var uiRangeSlider = function(){
            if($(".defaultSlider").length > 0){
                $(".defaultSlider").each(function(){
                    var rsMin = $(this).data("min");
                    var rsMax = $(this).data("max");

                    $(this).rangeSlider({
                        bounds: {min: 1, max: 200},
                        defaultValues: {min: rsMin, max: rsMax}
                    });
                });
            }
            if($(".dateSlider").length > 0){
                $(".dateSlider").each(function(){
                    $(this).dateRangeSlider({
                        bounds: {min: new Date(2012, 1, 1), max: new Date(2015, 12, 31)},
                        defaultValues:{min: new Date(2012, 10, 15),max: new Date(2014, 12, 15)}
                    });
                });
            }
            if($(".rangeSlider").length > 0){
                $(".rangeSlider").each(function(){
                    var rsMin = $(this).data("min");
                    var rsMax = $(this).data("max");

                    $(this).rangeSlider({
                        bounds: {min: 1, max: 200},
                        range: {min: 20, max: 40},
                        defaultValues: {min: rsMin, max: rsMax}
                    });
                });
            }
            if($(".stepSlider").length > 0){
                $(".stepSlider").each(function(){
                    var rsMin = $(this).data("min");
                    var rsMax = $(this).data("max");

                    $(this).rangeSlider({
                        bounds: {min: 1, max: 200},
                        defaultValues: {min: rsMin, max: rsMax},
                        step: 10
                    });
                });
            }
        }
        var uiKnob = function(){

            if($(".knob").length > 0){
                $(".knob").knob();
            }

        }
        var uiSmartWizard = function(){

            if($(".wizard").length > 0){

                $(".wizard > ul").each(function(){
                    $(this).addClass("steps_"+$(this).children("li").length);
                });
                if($("#wizard-validation").length > 0){

                    var validator = $("#wizard-validation").validate({
                            rules: {
                                login: {
                                    required: true,
                                    minlength: 2,
                                    maxlength: 8
                                },
                                password: {
                                    required: true,
                                    minlength: 5,
                                    maxlength: 10
                                },
                                repassword: {
                                    required: true,
                                    minlength: 5,
                                    maxlength: 10,
                                    equalTo: "#password"
                                },
                                email: {
                                    required: true,
                                    email: true
                                },
                                name: {
                                    required: true,
                                    maxlength: 10
                                },
                                adress: {
                                    required: true
                                }
                            }
                        });

                }
                $(".wizard").smartWizard({
                    onLeaveStep: function(obj){
                        var wizard = obj.parents(".wizard");

                        if(wizard.hasClass("wizard-validation")){

                            var valid = true;

                            $('input,textarea',$(obj.attr("href"))).each(function(i,v){
                                valid = validator.element(v) && valid;
                            });

                            if(!valid){
                                wizard.find(".stepContainer").removeAttr("style");
                                validator.focusInvalid();
                                return false;
                            }

                        }

                        return true;
                    },
                    onShowStep: function(obj){
                        var wizard = obj.parents(".wizard");

                        if(wizard.hasClass("show-submit")){

                            var step_num = obj.attr('rel');
                            var step_max = obj.parents(".anchor").find("li").length;

                            if(step_num == step_max){
                                obj.parents(".wizard").find(".actionBar .btn-primary").css("display","block");
                            }
                        }
                        return true;
                    }
                });
            }

        }
        var uiScroller = function(){

            if($(".scroll").length > 0){
                $(".scroll").mCustomScrollbar({axis:"y", autoHideScrollbar: true, scrollInertia: 20, advanced: {autoScrollOnFocus: false}});
            }

        }

        var uiSparkline = function(){

            if($(".sparkline").length > 0)
               $(".sparkline").sparkline('html', { enableTagOptions: true,disableHiddenCheck: true});

       }

        return {
            init: function(){
                uiRangeSlider();
                uiKnob();
                uiSmartWizard();
                uiScroller();
                uiSparkline();
            }
        }

    }();

    var templatePlugins = function(){

        var tp_clock = function(){

            function tp_clock_time(){
                var now     = new Date();
                var hour    = now.getHours();
                var minutes = now.getMinutes();

                hour = hour < 10 ? '0'+hour : hour;
                minutes = minutes < 10 ? '0'+minutes : minutes;

                $(".plugin-clock").html(hour+"<span>:</span>"+minutes);
            }
            if($(".plugin-clock").length > 0){

                tp_clock_time();

                window.setInterval(function(){
                    tp_clock_time();
                },10000);

            }
        }
        var tp_date = function(){

            if($(".plugin-date").length > 0){
                var days = ['Domingo','Lunes','Martes','Miercoles','Jueves','Viernes','Sábado'];
                var months = ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'];
                var now     = new Date();
                var day     = days[now.getDay()];
                var date    = now.getDate();
                var month   = months[now.getMonth()];
                var year    = now.getFullYear();
                $(".plugin-date").html(day+", "+month+" "+date+", "+year);
            }

        }
        return {init: function(){tp_clock();tp_date();}}
    }();

    formElements.init();
    uiElements.init();
    templatePlugins.init();

    $.mpb = function(action,options){

        var settings = $.extend({
            state: '',
            value: [0,0],
            position: '',
            speed: 20,
            complete: null
        },options);

        if(action == 'show' || action == 'update'){

            if(action == 'show'){
                $(".mpb").remove();
                var mpb = '<div class="mpb '+settings.position+'"><div class="mpb-progress'+(settings.state != '' ? ' mpb-'+settings.state: '')+'" style="width:'+settings.value[0]+'%;"></div></div>';
                $('body').append(mpb);
            }

            var i  = $.isArray(settings.value) ? settings.value[0] : $(".mpb .mpb-progress").width();
            var to = $.isArray(settings.value) ? settings.value[1] : settings.value;

            var timer = setInterval(function(){
                $(".mpb .mpb-progress").css('width',i+'%'); i++;

                if(i > to){
                    clearInterval(timer);
                    if($.isFunction(settings.complete)){
                        settings.complete.call(this);
                    }
                }
            }, settings.speed);

        }

        if(action == 'destroy'){
            $(".mpb").remove();
        }

    }
     $.expr[':'].containsi = function(a, i, m) {
         return jQuery(a).text().toUpperCase().indexOf(m[3].toUpperCase()) >= 0;
     };

});

Object.size = function(obj) {
    var size = 0, key;
    for (key in obj) {
        if (obj.hasOwnProperty(key)) size++;
    }
    return size;
};
$(document).ready(function(){
    if($("#manzory").length>=1){
        $('#manzory').masonry({
            itemSelector: '.col-md-3'
        });
    }
    if($("#mapCourse").length>=1){
        $('#mapCourse').locationpicker(
            {
                location: {latitude: 17.9933088, longitude: -92.9372959},
                radius: 400,
                inputBinding: {
                    locationNameInput: $('#city')
                },
                enableAutocomplete: true,
                onchanged: function(currentLocation, radius, isMarkerDropped) {
                    $("#long").val(currentLocation.longitude);
                    $("#lat").val(currentLocation.latitude);
                }
            }
        );
    }
    if($("#editMapCourse").length>=1){
        var lat = $("#lat").val();
        var long = $("#long").val();
        $('#editMapCourse').locationpicker(
            {
                location: {latitude: lat,longitude: long},
                radius: 400,
                inputBinding: {
                    locationNameInput: $('#city')
                },
                enableAutocomplete: true,
                onchanged: function(currentLocation, radius, isMarkerDropped) {
                    $("#long").val(currentLocation.longitude);
                    $("#lat").val(currentLocation.latitude);
                }
            }
        );
    }
})
$(document).ready(function(){
    var preview_sale =$(".preview_sale");
    if(preview_sale.length>=1){
        preview_sale.click(function(e){
            e.preventDefault();
            $.ajax({
                data : $(this).serialize(),
                url : "/dashboard/sales/get-clients",
                type : "POST",
                dataType : "json",
                success:function(response){
                    if(response.code==200){
                        data = [];
                        $.each(response.result,function(i,item){
                            /*append($("<option class='users_sales'>").attr('value',item.id).text(item.text));*/
                            data[i] = {id:item.id,text:item.text};
                        });
                        $("#user_sales").select2({maximumSelectionLength:1,data:data});
                    }
                },
                error : function(){
                    alert("Ha ocurrido un error intente nuevamente")
                }
            });
            $("#modal_sale").modal("show");
            return false;
        });
        $("#formSLS").submit(function(e){
            e.preventDefault();
            $(".button_new_sales").addClass("disabled").attr("disabled",true);
            $.ajax({
                data : $(this).serialize(),
                url : "/dashboard/sales/active",
                type : "POST",
                dataType : "json",
                success:function(response){
                    if(response.code==300){
                        alert("Ha ocurrido un error, prueba nuevamente");
                    }else{
                        window.open(response.url)
                        $("#modal_sale").modal("hide");
                    }
                },
                error : function(){
                    alert("Ha ocurrido un error intente nuevamente")
                },complete:function(){
                    $(".button_new_sales").removeClass("disabled").attr("disabled",false);
                }
            });
        });
        $("#modal_sale").on("hidden.bs.modal",function(){
            $("#user_sales option").remove();
            $("#user_sales").select2("destroy");
            $("#modal_sale").modal("hide");
            $(".button_new_sales").removeClass("disabled").attr("disabled",false);
        });
        return false;
    }
})

