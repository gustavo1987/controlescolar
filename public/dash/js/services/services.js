$(document).ready(function(){
    var formServices = $("#formServices");
    if(formServices.length>=1){
        $(document.body).on("click","table tbody tr td span.editService",function(e){
            $("#name").val($(this).attr("data-name"));
            $("#permalink").val($(this).attr("data-permalink"));
            $("#servid").val($(this).parent().attr("data-id"));
            $("#defModalHead").text("Editar Servicio");
            $(".btn-submit-services").val("Actualizar");
            $("#modalService").modal();
        });
        $("#newService").on("click",function(e){
            $("#defModalHead").text("Agregar Servicio");
            $(".btn-submit-services").val("Guardar");
            $("#servid").val("");
            $("#modalService").modal();
        });
        getPermalink($("#name"),"/dashboard/services/permalink",$("#permalink"),formServices);
        deleteRow("deleteService","/dashboard/services/delete");
        formServices.formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            excluded: ':disabled',
            fields: {
                name: {
                    validators: {
                        notEmpty: {
                            message: 'Este campo es requerido y no puede estar vacío'
                        },
                        stringLength: {
                            min: 5,
                            max: 100,
                            message: 'Este campo debe tener como mínimo 8 caracteres y como máximo 100.'
                        }
                    }
                },
                permalink: {
                    validators: {
                        notEmpty: {
                            message: 'Este campo es requerido y no puede estar vacío'
                        },
                        stringLength: {
                            min: 5,
                            max: 150,
                            message: 'Este campo debe tener como mínimo 8 caracteres y como máximo 100.'
                        }
                    }
                }
            }
        }).on('success.form.fv', function(e) {
            e.preventDefault();
            $("#message-box-info").toggleClass("open");
            $.ajax({
                type : "POST",
                url : "/dashboard/services/save",
                data : $(this).serialize(),
                dataType : "json",
                success : function(response){
                    if(response.message=="SUCCESS" && response.code==200){
                        if(response.type==1){
                            $("#tableTbody").append("<tr class='"+response.result.id+"'><td>"+response.result.id+"</td><td class='name'>"+response.result.name+"</td><td data-id='"+response.result.id+"'><span data-name='"+response.result.name+"' data-permalink='"+response.result.permalink+"' class='editService' data-toggle='tooltip' data-placement='top' data-original-title='Editar servicio'><i class='fa fa-pencil-square-o fa-2x'></i></span>&nbsp;<span class='deleteService' data-toggle='tooltip' data-placement='top' data-original-title='Eliminar servicio'><i class='fa fa-remove fa-2x'></i></span>&nbsp;<span class='newSubService' data-toggle='tooltip' data-placement='top' title='' data-original-title='Agregar sub-servicios'><i class='fa fa-plus-square fa-2x'></i></span>&nbsp;<a href='"+response.result.url+"' data-toggle='tooltip' data-placement='top' data-original-title='Lista de sub-servicios'><i class='fa fa-list-ol fa-2x'></i></a>&nbsp;</td></tr>");
                        }else{
                            row_update =$("tbody#tableTbody tr."+response.result.id+"");
                            row_update.find(".name").text(response.result.name);
                            row_update.find(".editService").attr("data-name",response.result.name).attr("data-permalink",response.result.permalink);
                        }
                        messages(1);
                        setTimeout(function(){
                            $("#message-box-success").removeClass("open");
                            $("#modalService").modal("hide");
                        },2000);
                        resetForm(formServices);
                        formServices.formValidation('resetForm', true);
                    }else{
                        messages(2);
                    }
                },
                error : function(){
                    messages(3)
                }
            });
        });

        $("#modalService").on("hidden.bs.modal",function(){
            resetForm(formServices);
            formServices.formValidation('resetForm', true);
        });
    }

    var formSubServices  = $("#formSubServices");
    if(formSubServices.length>=1){
        $(document.body).on("click","table tbody tr td span.newSubService",function(e){
            id = $(this).parent().attr("data-id");
            $("#servidSS").val(id);
            $("#nameSS").attr("data-id",id);
            $("#defModalHeadSS").text("Agregar Sub-Servicio");
            $(".btn-submit-sub-services").val("Guardar");
            $("#modalSubService").modal();
        });
        $("#newSubService").click(function(e){
            id = $("#dataServid").attr("data-id");
            $("#servidSS").val(id);
            $("#subsid").val("");
            $("#nameSS").attr("data-id",id);
            $("#defModalHeadSS").text("Agregar Sub-Servicio");
            $(".btn-submit-sub-services").val("Guardar");
            $("#modalSubService").modal();
        });
        $(document.body).on("click","table tbody tr td span.editSubService",function(e){
            id = $("#dataServid").attr("data-id");
            $("#status").removeClass('hidden');
            $("#nameSS").val($(this).attr("data-name")).attr("data-id",id);
            $("#permalinkSS").val($(this).attr("data-permalink"));
            $("#price").val($(this).attr("data-price"));
            $("#discount").val($(this).attr("data-discount"));
            $("#subsid").val($(this).parent().attr("data-id"));
            $("#defModalHeadSS").text("Editar Sub-Servicio");
            $(".btn-submit-sub-services").val("Actualizar");
            $("#modalSubService").modal();
        });
        getPermalink($("#nameSS"),"/dashboard/services/permalink-sub-service",$("#permalinkSS"),formSubServices);
        if($(".deleteSubService").length>=1){
            deleteRow("deleteSubService","/dashboard/services/delete-sub-service");
        }
        formSubServices.formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            excluded: ':disabled',
            fields: {
                nameSS: {
                    validators: {
                        notEmpty: {
                            message: 'Este campo es requerido y no puede estar vacío'
                        },
                        stringLength: {
                            min: 3,
                            max: 100,
                            message: 'Este campo debe tener como mínimo 3 caracteres y como máximo 100.'
                        }
                    }
                },
                permalinkSS: {
                    validators: {
                        notEmpty: {
                            message: 'Este campo es requerido y no puede estar vacío'
                        },
                        stringLength: {
                            min: 3,
                            max: 150,
                            message: 'Este campo debe tener como mínimo 3 caracteres y como máximo 100.'
                        }
                    }
                },
                price: {
                    validators: {
                        notEmpty: {
                            message: 'Este campo es requerido y no puede estar vacío'
                        }
                    }
                },
                discount: {
                    validators: {
                        notEmpty: {
                            message: 'Este campo es requerido y no puede estar vacío'
                        }
                    }
                }
            }
        }).on('success.form.fv', function(e) {
            e.preventDefault();
            $("#message-box-info").toggleClass("open");
            $.ajax({
                type : "POST",
                url : "/dashboard/services/save-sub-service",
                data : $(this).serialize(),
                dataType : "json",
                success : function(response){
                    if(response.message=="SUCCESS" && response.code==200){
                        if(response.type==1){
                            $("#tableTbodyS").append("<tr class='"+response.result.id+"'><td>"+response.result.id+"</td><td class='name'>"+response.result.name+"</td><td class='price'>"+response.result.price+"</td><td class='discount'>"+response.result.discount+"</td><td data-id='"+response.result.id+"'><span data-name='"+response.result.name+"' data-permalink='"+response.result.permalink+"' data-price='"+response.result.price+"' data-discount='"+response.result.discount+"' class='editSubService' data-toggle='tooltip' data-placement='top' data-original-title='Editar sub-servicio'><i class='fa fa-pencil-square-o fa-2x'></i></span>&nbsp;<span class='deleteSubService' data-toggle='tooltip' data-placement='top' title='' data-original-title='Eliminar sub-servicio'><i class='fa fa-remove fa-2x'></i></span></td></tr>");
                        }else{
                            status = response.result.status=="CANCEL"?"warning":"info";
                            row_update = $("tbody#tableTbodyS tr."+response.result.id+"");
                            row_update.find("td").removeClass("warning info").addClass(status);
                            row_update.find(".name").text(response.result.name);
                            row_update.find(".price").text(response.result.price);
                            row_update.find(".discount").text(response.result.discount);
                            row_update.find(".editSubService").attr("data-name",response.result.name).attr("data-permalink",response.result.permalink).attr("data-price",response.result.price).attr("data-discount",response.result.discount);
                        }
                        messages(1);
                        setTimeout(function(){
                            $("#message-box-success").removeClass("open");
                            $("#modalSubService").modal("hide");
                        },2000);
                        resetForm(formSubServices);
                        formSubServices.formValidation('resetForm', true);
                    }else{
                        messages(2);
                    }
                },
                error : function(){
                    messages(3)
                }
            });
        });
        $("#modalSubService").on("hidden.bs.modal",function(){
            resetForm(formSubServices);
            formSubServices.formValidation('resetForm', true);
            $("#status").addClass('hidden');
        });
    }
});
function getPermalink(selector,url,selector_permalink,form){
    selector.change(function(){
        name_service = $(this).val();
        id = $(this).attr("data-id");
        $.ajax({
            url : url,
            type : "POST",
            data : {name:name_service,id:id},
            dataType : "json",
            success : function(response){
                if(response.message=="SUCCESS" && response.code==200){
                    selector_permalink.val(response.permalink);
                    form.formValidation('revalidateField', 'permalink');
                }else{
                    alert("Ha ocurrido un error intente nuevamente.");
                }
            },error : function(){
                alert("Ha ocurrido un error intente nuevamente.");
            }
        });
    });
}
function deleteRow(selector,url){
    var box = $("#mb-remove-row");
    var id = "0";
    $(document.body).on("click","table tbody tr td span."+selector,function(){
        id = $(this).parent().attr("data-id");
        box.addClass("open");
    });
    box.find(".mb-control-yes").on("click",function(){
        $.ajax({
            url : url,
            type : "POST",
            data : {id:id},
            dataType : "json",
            success : function(response){
                box.removeClass("open");
                if(response.code==200){
                    $("."+id).hide("slow",function(){
                        $(this).remove();
                    });
                }else{
                    $("#message-box-danger-deleted").toggleClass("open");
                    setTimeout(function(){
                        $("#message-box-danger-deleted").removeClass("open");
                    },2000);
                }
            },error : function(){
                alert("Ha ocurrido un error intente nuevamente.");
            }
        });
    });
}
function resetForm($form) {
    $form.find('input:text, input:password, input:file, input[type=email], select, textarea').val('');
    $form.find('input:radio, input:checkbox').removeAttr('checked').removeAttr('selected');
}
function messages($type){
    if($type==1){
        setTimeout(function(){
            $("#message-box-info").removeClass("open");
            $("#message-box-success").toggleClass("open");
        },1500);
        setTimeout(function(){
            $("#message-box-success").removeClass("open");
        },2000);
    }
    else if($type==2){
        setTimeout(function(){
            $("#message-box-info").removeClass("open");
            $("#message-box-warning").toggleClass("open");
        },1500);
        setTimeout(function(){
            $("#message-box-warning").removeClass("open");
        },3000);
    }else if($type==3){
        setTimeout(function(){
            $("#message-box-info").removeClass("open");
            $("#message-box-warning").toggleClass("open");
        },1500);
        setTimeout(function(){
            $("#message-box-warning").removeClass("open");
        },3000);
    }
}