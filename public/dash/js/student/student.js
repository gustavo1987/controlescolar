$(document).ready(function () {


    $(document.body).on("click",".viewpay",function(){
        status=$(this).data("status");
        id=$(this).data("id");
        clid=$(this).data("clid");
        Swal.fire({
            title: "Actualizando vista pago",
            type: "warning",
            allowOutsideClick: false,
            showCancelButton: false,
            showConfirmButton: false,
            closeOnEsc: false,
            onOpen: () => {
                swal.showLoading();
            }
        });
        $.ajax({
         type : "POST",
         url : "/dashboard/student/hiddenpaystudent",
         data :  {id:id,status:status},
         dataType : "json",
         success : function(response){
            if(response.message=="SUCCESS" && response.code==200){
                Swal.fire({
                    position: 'top-end',
                    title: "Vista de pago Actualizado",
                    type: "success",
                    allowOutsideClick: false,
                    showCancelButton: false,
                    showConfirmButton: true,
                    closeOnEsc: false,
                }).then(function() {
                     window.location = "/dashboard/student/viewpay/"+clid;
                });
            }else if(response.code==404){
                 
            }
        },
        error : function(){}
        });
    });












    
    $(document.body).on("click",".block-student",function(){
        uid=$(this).attr("data-id");
        block=$(this).attr("data-status");
        if(status=1){
            Swal.fire({
                title: "bloqueando estudiante",
                type: "warning",
                allowOutsideClick: false,
                showCancelButton: false,
                showConfirmButton: false,
                closeOnEsc: false,
                onOpen: () => {
                    swal.showLoading();
                }
            });
        }
        else{
            Swal.fire({
                title: "desbloqueando estudiante",
                type: "warning",
                allowOutsideClick: false,
                showCancelButton: false,
                showConfirmButton: false,
                closeOnEsc: false,
                onOpen: () => {
                    swal.showLoading();
                }
            });
        }
        
        $.ajax({
         type : "POST",
         url : "/dashboard/student/blockstudent",
         data :  {uid:uid,block:block},
         dataType : "json",
         success : function(response){
             if(response.message=="SUCCESS" && response.code==200){
              Swal.fire({
                  position: 'top-end',
                  title: "Estudiante Actualizado",
                  type: "success",
                  allowOutsideClick: false,
                  showCancelButton: false,
                  showConfirmButton: true,
                  closeOnEsc: false,
              }).then(function() {
                     window.location = "/dashboard/student/all/"+response.braid;
                 });
             }else if(response.code==404){
                 
             }
        },
        error : function(){}
        });

    });
    $("#namepay").on("change",function(){
        if($("#namepay option:selected").val()=="MENSUALIDAD"){
            $(".div-others").addClass("hidden");
            $(".div-month").toggleClass("hidden");
        }
        else if($("#namepay option:selected").val()=="OTRO"){
            $(".div-month").addClass("hidden");
            $(".div-others").toggleClass("hidden");
        }
        else{
            $(".div-month").addClass("hidden");
            $(".div-others").addClass("hidden");
        }
    });
   $(document.body).on("click",".deletepay",function(){
      Swal.fire({
         title: "Eliminando Pago",
         type: "warning",
         allowOutsideClick: false,
         showCancelButton: false,
         showConfirmButton: false,
         closeOnEsc: false,
         onOpen: () => {
            swal.showLoading();
         }
     })
     pagid = $(this).parents().parents("tr").attr("data-id");
     clid = $(this).parents().parents("tr").attr("data-clid");
     $.ajax({
         type : "POST",
         url : "/dashboard/student/deletepay",
         data :  {pagid:pagid},
         dataType : "json",
         success : function(response){
             var clase = response.clase;
             if(response.message=="SUCCESS" && response.code==200){
              Swal.fire({
                  position: 'top-end',
                  title: "Pago eliminado",
                  type: "success",
                  allowOutsideClick: false,
                  showCancelButton: false,
                  showConfirmButton: true,
                  closeOnEsc: false,
              }).then(function() {
                     window.location = "/dashboard/student/viewpay/"+clid;
                 });
             }else if(response.code==404){
                 
             }
         },
         error : function(){
         }
     });
   });
   $(document.body).on("click",".score",function(){
      clid = $(this).parents().parents("tr").attr("data-id");
      clasid = $(this).parents().parents("tr").attr("data-clasid");
      $("#clid-student-update-score").val(clid);
      $("#clasid-student-update-score").val(clasid);
      $("#updatescorestudent").toggleClass("open");

   });
   var updatescore = $("#updatescorestudentform");
    if(updatescore.length>=1){
        updatescore.formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                scorestudent: {
                    validators: {
                        notEmpty: {
                            message: 'La calificación es necesario y no puede estar vacío.'
                        }
                    }
                }
            }
        }).on('success.form.fv', function(e) {
            $("#updatescorestudent").toggleClass("open");
            e.preventDefault();
            swal({
                title: "Calificando",
                allowOutsideClick: false,
                button: false,
                showCancelButton: false,
                showConfirmButton: false,
                closeOnEsc: false,
            });
            $.ajax({
                type : "POST",
                url : "/dashboard/student/updatescorestudent",
                data :  $(this).serialize(),
                dataType : "json",
                success : function(response){
                    var clase = response.clase;
                    if(response.message=="SUCCESS" && response.code==200){
                        swal({
                            title: "calificación Actualizada",
                            icon: "success",
                            allowOutsideClick: false,
                            showCancelButton: false,
                            showConfirmButton: true,
                            closeOnEsc: false,
                        }).then(function() {
                            window.location = "/dashboard/student/allstudentclass";
                        });
                    }else if(response.code==404){
                        
                    }
                },
                error : function(){
                }
            });
        });
    }







    $("#video").on('click',function(){
        var theModal = $(this).data("target");
        var videoSRC = $(this).data("video");
        videoSRCauto = videoSRC + "?modestbranding=1&rel=0&controls=0&showinfo=0&html5=1&autoplay=1";
        $(theModal+' iframe').attr('src', videoSRCauto);
        $(theModal + ' button.btn-primary').click(function(){
            $(theModal + ' iframe').attr('src', videoSRC);
        });
    });
    
    $("#ingresar-asistencia").on("click",function(){
        /*var id = $('#ingresar-asistencia').data("id");*/
        var values = {
            confid:confid = $(this).data("id")
        };
        var data = $(this).serialize()+"&"+jQuery.param(values);
        $.ajax({
            type : "POST",
            url : "/dashboard/student/asistenciavivo",
            data :  data,
            dataType : "json",
            success : function(response){
                if(response.message=="SUCCESS" && response.code==200){
                    swal({
                        title: "Su asistencia se actualizo a EN VIVO",
                        icon: "success",
                        allowOutsideClick: false,
                        showCancelButton: false,
                        showConfirmButton: true,
                        closeOnEsc: false,
                    }).then(function() {
                            window.open(response.url);
                            //window.location = response.url;
                        });
                }else if(response.code==404){
                    
                }
            },
            error : function(){
            }
        });
    });
    $(window).bind("load", function() {
        var name = $("#name-user-layout").text();
        var email = $("#email-user-layout").text();
        var others = $("#demio-calendar");

        $("input[name='name']").val(name).css({"display":"none"});
        $("input[name='email']").val(email).css({"display":"none"});
        $("input[name='notifications_allow']").prop('checked', true).css({"display":"none"});
        $(".demio-embed-checkbox").css({"display":"none"});
        $(":button").addClass("button-demio-go");
        /*$(":button").css({"background-image": "url(/dash/src/images/judge-1587300_1920.jpg)"}); 
        $(":button").css({"width": "26%"}); 
        $(":button").css({"margin": "auto"}); 
        $(":button").css({"display": "block"});*/
        others.css({"display":"none"});
    });
    $("#select-type").on("change", function(e){
            if($(this).val()=="document"){
                $("#document-hidden-modal").removeClass("hidden");
                $("#input-hidden-liga").addClass("hidden");
                //$("#document-hidden-modal").removeClass("hidden");
            }
            else if($(this).val()=="liga"){
                $("#input-hidden-liga").removeClass("hidden");
                $("#document-hidden-modal").addClass("hidden");
                //$("#input-hidden-liga").removeClass("hidden");
            }
    });

    $(document.body).on("click", ".direfido", function (e){
        e.preventDefault();
        var values = {
            confid:confid = $(this).data("id")
        };
        var data = $(this).serialize()+"&"+jQuery.param(values);
        $.ajax({
            type : "POST",
            url : "/dashboard/student/updatediferido",
            data :  data,
            dataType : "json",
            success : function(response){
                if(response.message=="SUCCESS" && response.code==200){
                    swal({
                        title: ""+response.mensaje,
                        icon: "success",
                        allowOutsideClick: false,
                        showCancelButton: false,
                        showConfirmButton: true,
                        closeOnEsc: false,
                    }).then(function() {
                            window.open(response.url);
                            //window.location = response.url;
                        });
                }else if(response.code==404){
                    
                }
            },
            error : function(){
            }
        });
    });


    var newpay = $("#newPay");
    if(newpay.length>=1){
        /*getSpanishD();*/
        $('.vencimiento').datepicker({
            format: 'dd/mm/yyyy',
            language : "es",
            autoclose:true
        })
        newpay.formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                name: {
                    validators: {
                        notEmpty: {
                            message: 'La descripción es necesario y no puede estar vacío.'
                        }
                    }
                },
                cantidad: {
                    validators: {
                        notEmpty: {
                            message: 'La cantidad es necesario y no puede estar vacío.'
                        }
                    }
                },
                status: {
                    validators: {
                        notEmpty: {
                            message: 'El estatus es necesario y no puede estar vacío.'
                        }
                    }
                }
            }
        }).on('success.form.fv', function(e) {
            e.preventDefault();
            swal({
                title: "Cargando pago",
                allowOutsideClick: false,
                button: false,
                showCancelButton: false,
                showConfirmButton: false,
                closeOnEsc: false,
            });
            $.ajax({
                type : "POST",
                url : "/dashboard/student/savepay",
                data :  $(this).serialize(),
                dataType : "json",
                success : function(response){
                    var clase = response.clase;
                    if(response.message=="SUCCESS" && response.code==200){
                        swal({
                            title: "Guardado exitoso",
                            icon: "success",
                            allowOutsideClick: false,
                            showCancelButton: false,
                            showConfirmButton: true,
                            closeOnEsc: false,
                        }).then(function() {
                            window.location = "/dashboard/student/viewpay/"+response.id;
                        });
                        /*setTimeout(function(){
                            window.location = "/dashboard/student/all";
                        },3000);*/
                    }else if(response.code==404){
                        
                    }
                },
                error : function(){
                }
            });
        });
    }
    var noteSaveTeacher = $("#save-note-teacher");
    if(noteSaveTeacher.length>=1){
        noteSaveTeacher.formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                confid: {
                    validators: {
                        notEmpty: {
                            message: 'Seleccione  la clase.'
                        }
                    }
                },
                selecttype: {
                    validators: {
                        notEmpty: {
                            message: 'Seleccione el tipo de nota.'
                        }
                    }
                }
            }
        }).on('success.form.fv', function(e) {
            e.preventDefault();
            swal({
                title: "Cargando archivo",
                allowOutsideClick: false,
                button: false,
                showCancelButton: false,
                showConfirmButton: false,
                closeOnEsc: false,
            });
            $.ajax({
                type : "POST",
                url : "/dashboard/maestro/savenote",
                data :  $(this).serialize(),
                dataType : "json",
                success : function(response){
                    var clase = response.clase;
                    if(response.message=="SUCCESS" && response.code==200){
                        swal({
                            title: "Guardado exitoso",
                            icon: "success",
                            allowOutsideClick: false,
                            showCancelButton: false,
                            showConfirmButton: true,
                            closeOnEsc: false,
                        }).then(function() {
                            window.location = "/dashboard/student/notas/"+clase;
                        });
                        /*setTimeout(function(){
                            window.location = "/dashboard/student/all";
                        },3000);*/
                    }else if(response.code==404){
                        
                    }
                },
                error : function(){
                }
            });
        });
    }



    if($("#note-teacher").length>=1){
        Dropzone.autoDiscover = false;
        var myDropzone = new Dropzone('#note-teacher', {
            url: "/dashboard/maestro/uploadfile",
            //acceptedFiles: "image/jpeg,image/jpg,image/png",
            uploadMultiple : false,
            paramName: "file", // The name that will be used to transfer the file
            maxFilesize: 20, // 10 MB maximo
            maxFiles: 1 ,
            parallelUploads : 1,
            addRemoveLinks : true,
            dictResponseError: "No se puede subir esta imagen!",
            autoProcessQueue: true,
            thumbnailWidth: 138,
            thumbnailHeight: 120,
            previewTemplate: '<div class="dz-preview dz-file-preview"><div class="dz-details"><div class="dz-filename"><span data-dz-name style:"font-weight: bold;"></span></div><div class="dz-size">File size: <span data-dz-size></span></div><div class="dz-thumbnail-wrapper"><div class="dz-thumbnail"><img data-dz-thumbnail><span class="dz-nopreview">No preview</span><div class="dz-success-mark"><i class="fa fa-check-circle-o"></i></div><div class="dz-error-mark"><i class="fas fa-times-circle"></i></div><div class="dz-error-message"><span data-dz-errormessage></span></div></div></div></div><div class="progress progress-striped active"><div class="progress-bar progress-bar-success" data-dz-uploadprogress></div></div></div>',

            resize: function(file) {
                var info = { srcX: 0, srcY: 0, srcWidth: file.width, srcHeight: file.height },
                    srcRatio = file.width / file.height;
                if (file.height > this.options.thumbnailHeight || file.width > this.options.thumbnailWidth) {
                    info.trgHeight = this.options.thumbnailHeight;
                    info.trgWidth = info.trgHeight * srcRatio;
                    if (info.trgWidth > this.options.thumbnailWidth) {
                        info.trgWidth = this.options.thumbnailWidth;
                        info.trgHeight = info.trgWidth / srcRatio;
                    }
                } else {
                    info.trgHeight = file.height;
                    info.trgWidth = file.width;
                }
                return info;
            },
            sending : function(file, xhr, formData){
                namework = $("#namework").val();
                formData.append("namework",namework);
            },
            success: function(file, response){
                var name_image = response;
                $(".dz-preview").addClass("dz-success");
                $("div.progress").remove();
                $("#input-image-principal").val(name_image.name);
                swal({
                  title: "Cargado exitosamente!",
                  text: "Da click en el botón aceptar!",
                  icon: "success",
                  button: "Aceptar",
                });
            },
            removedfile: function(file) {
                $(file.previewElement).remove();
            }
        });
    }


  /*$('#calendar').on("load", function(){
      alert("hola");
      $.ajax({
          type : "POST",
          url : "/dashboard/student/getcalendar",
          data :  student,
          dataType : "json",
          success : function(response){
              if(response.message=="SUCCESS" && response.code==200){

              }else if(response.code==404){
              }
          },
          error : function(){
          }
      });
  });*/
    /* Subir archivo de trabajo estudiante */
    if($("#image-principal-dz").length>=1){
        Dropzone.autoDiscover = false;
        var myDropzone = new Dropzone('#image-principal-dz', {
            url: "/dashboard/student/uploadfile",
            //acceptedFiles: "image/jpeg,image/jpg,image/png",
            uploadMultiple : false,
            paramName: "file", // The name that will be used to transfer the file
            maxFilesize: 20, // 10 MB maximo
            maxFiles: 1 ,
            parallelUploads : 1,
            addRemoveLinks : true,
            dictResponseError: "No se puede subir esta imagen!",
            autoProcessQueue: true,
            thumbnailWidth: 138,
            thumbnailHeight: 120,
            previewTemplate: '<div class="dz-preview dz-file-preview"><div class="dz-details"><div class="dz-filename"><span data-dz-name style:"font-weight: bold; color: black;"></span></div><div class="dz-size" style="color: black;">File size: <span data-dz-size style="color: black;"></span></div><div class="dz-thumbnail-wrapper"><div class="dz-thumbnail"><img data-dz-thumbnail><span class="dz-nopreview">No preview</span><div class="dz-success-mark"><i class="fa fa-check-circle-o"></i></div><div class="dz-error-mark"><i class="fas fa-times-circle"></i></div><div class="dz-error-message"><span data-dz-errormessage></span></div></div></div></div><div class="progress progress-striped active"><div class="progress-bar progress-bar-success" data-dz-uploadprogress></div></div></div>',

            resize: function(file) {
                var info = { srcX: 0, srcY: 0, srcWidth: file.width, srcHeight: file.height },
                    srcRatio = file.width / file.height;
                if (file.height > this.options.thumbnailHeight || file.width > this.options.thumbnailWidth) {
                    info.trgHeight = this.options.thumbnailHeight;
                    info.trgWidth = info.trgHeight * srcRatio;
                    if (info.trgWidth > this.options.thumbnailWidth) {
                        info.trgWidth = this.options.thumbnailWidth;
                        info.trgHeight = info.trgWidth / srcRatio;
                    }
                } else {
                    info.trgHeight = file.height;
                    info.trgWidth = file.width;
                }
                return info;
            },
            sending : function(file, xhr, formData){
                confid = $("#confid").val();
                formData.append("confid",confid);
            },
            success: function(file, response){
                var name_image = response;
                $(".dz-preview").addClass("dz-success");
                $("div.progress").remove();
                $("#input-image-principal").val(name_image.name);
                swal({
                  title: "Cargado exitosamente!",
                  text: "Da click en el botón aceptar!",
                  icon: "success",
                  button: "Aceptar",
                });
                setTimeout(function(){
                    window.location = "/dashboard/student/trabajos/"+$("#idclase").val();
                },3000);
            },
            removedfile: function(file) {
                $(file.previewElement).remove();
            }
        });
    }
    /* Subir trabajo con observaciones maestro */
    /* Users */
    if($("#image-principal-dz-teacher").length>=1){
        Dropzone.autoDiscover = false;
        var myDropzone = new Dropzone('#image-principal-dz-teacher', {
            url: "/dashboard/maestro/uploadfilecalificacion",
            //acceptedFiles: "image/jpeg,image/jpg,image/png",
            uploadMultiple : false,
            paramName: "file", // The name that will be used to transfer the file
            maxFilesize: 20, // 10 MB maximo
            maxFiles: 1 ,
            parallelUploads : 1,
            addRemoveLinks : true,
            dictResponseError: "No se puede subir esta imagen!",
            autoProcessQueue: true,
            thumbnailWidth: 138,
            thumbnailHeight: 120,
            previewTemplate: '<div class="dz-preview dz-file-preview"><div class="dz-details"><div class="dz-filename"><span data-dz-name style:"font-weight: bold; color: black;"></span></div><div class="dz-size" style="color: black;">File size: <span data-dz-size style="color: black;"></span></div><div class="dz-thumbnail-wrapper"><div class="dz-thumbnail"><img data-dz-thumbnail><span class="dz-nopreview">No preview</span><div class="dz-success-mark"><i class="fa fa-check-circle-o"></i></div><div class="dz-error-mark"><i class="fas fa-times-circle"></i></div><div class="dz-error-message"><span data-dz-errormessage></span></div></div></div></div><div class="progress progress-striped active"><div class="progress-bar progress-bar-success" data-dz-uploadprogress></div></div></div>',

            resize: function(file) {
                var info = { srcX: 0, srcY: 0, srcWidth: file.width, srcHeight: file.height },
                    srcRatio = file.width / file.height;
                if (file.height > this.options.thumbnailHeight || file.width > this.options.thumbnailWidth) {
                    info.trgHeight = this.options.thumbnailHeight;
                    info.trgWidth = info.trgHeight * srcRatio;
                    if (info.trgWidth > this.options.thumbnailWidth) {
                        info.trgWidth = this.options.thumbnailWidth;
                        info.trgHeight = info.trgWidth / srcRatio;
                    }
                } else {
                    info.trgHeight = file.height;
                    info.trgWidth = file.width;
                }
                return info;
            },
            sending : function(file, xhr, formData){
                var plant = document.getElementById('input-image-principal-teacher');
                var cali = $("#calificacion").val();

                formData.append("workid",plant.getAttribute('data-id'));
                formData.append("calificacion",cali);
            },
            success: function(file, response){
                var name_image = response;
                $(".dz-preview").addClass("dz-success");
                $("div.progress").remove();
                $("#input-image-principal-teacher").val(name_image.name);
                swal({
                  title: "Cargado exitosamente!",
                  text: "Da click en el botón aceptar!",
                  icon: "success",
                  button: "Aceptar",
                });
                setTimeout(function(){
                    window.location = "/dashboard/student/trabajos";
                },1000);
            },
            removedfile: function(file) {
                $(file.previewElement).remove();
            }
        });
    }


    /* click al cargar envía datos al modal maestro*/

    $(".upload-file").on("click",function(){

      var plant1 = document.getElementById('input-image-principal-teacher');

      id = $(this).data("id");
      //$("input[name='imageteacher']").date("id",id);

      plant1.setAttribute('data-id',id);
    });



    var newUser = $("#newUser");
    if(newUser.length>=1){
        newUser.formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                name: {
                    validators: {
                        notEmpty: {
                            message: 'El Nombre es necesario y no puede estar vacío.'
                        },
                        stringLength: {
                            min: 4,
                            max: 45,
                            message: 'El nombre debe ser mayor de 4 y menor de 45 caracteres de longitud.'
                        }
                    }
                },
                last_name: {
                    validators: {
                        notEmpty: {
                            message: 'El Apellido Paterno es necesario y no puede estar vacío.'
                        },
                        stringLength: {
                            min: 4,
                            max: 45,
                            message: 'El Apellido Paterno debe ser mayor de 4 y menor de 45 caracteres de longitud.'
                        }
                    }
                },
                sex: {
                    validators: {
                        notEmpty: {
                            message: 'El Sexo es un campo necesario y no puede estar vacío.'
                        }
                    }
                },
                phone: {
                    validators: {
                        notEmpty: {
                            message: 'Su numero de telefono es necesario y no puede estar vacío.'
                        }
                    }
                },
                email: {
                    validators: {
                        notEmpty: {
                            message: 'Es necesaria la dirección de correo electrónico y esta no puede estar vacía'
                        },
                        emailAddress: {
                            message: 'Este campo no contiene una dirección de correo electrónico válida.'
                        },
                        remote: {
                            message: 'Esta cuenta de correo electrónico ya existe, pruebe con otra',
                            url: '/dashboard/user/validateemail',
                            data: {
                                type: 'email'
                            },
                            type: 'POST'
                        }
                    }
                },
                username: {
                    message: 'El nombre de usuario no es válido.',
                    validators: {
                        notEmpty: {
                            message: 'Se requiere el nombre de usuario y no puede estar vacío.'
                        },
                        stringLength: {
                            min: 6,
                            max: 30,
                            message: 'El nombre de usuario debe ser mayor de 6 y menos de 30 caracteres de longitud.'
                        },
                        regexp: {
                            regexp: /^[a-zA-Z0-9_\.]+$/,
                            message: 'El nombre de usuario sólo puede consistir en alfabético, número, puntos o subrayados.'
                        },
                        remote: {
                            message: 'Estae nombre de usuario ya existe, pruebe con otro',
                            url: '/dashboard/user/validateusername',
                            data: {
                                type: 'username'
                            },
                            type: 'POST'
                        }
                    }
                },
                password: {
                    validators: {
                        notEmpty: {
                            message: 'El campo de contraseña no puede estar vacío'
                        },
                        stringLength: {
                            min: 8,
                            max: 30,
                            message: 'La contraseña debe tener como mínimo 8 caracteres.'
                        }
                    }
                },
                confirmPassword: {
                    validators: {
                        identical: {
                            field: 'password',
                            message: 'Las contraseñas no coinciden'
                        }
                    }
                },
                cargo: {
                    validators: {
                        notEmpty: {
                            message: 'El campo de Cargo es necesario y no puede estar vacío.'
                        },
                        stringLength: {
                            min: 4,
                            max: 45,
                            message: 'El campo de Cargo debe ser mayor de 4 y menor de 45 caracteres de longitud.'
                        }
                    }
                },
                rol: {
                    validators: {
                        notEmpty: {
                            message: 'El usuario necesita tener un rol.'
                        }
                    }
                },
                status: {
                    validators: {
                        notEmpty: {
                            message: 'El usuario necesita tener un status.'
                        }
                    }
                }
            }
        }).on('success.form.fv', function(e) {
            e.preventDefault();
            $("#btn-submit").attr("disabled","disabled").val("Iniciando");
            $("#message-box-info").toggleClass("open");
            $.ajax({
                type : "POST",
                url : "/dashboard/student/saveuser",
                data :  $(this).serialize(),
                dataType : "json",
                success : function(response){
                    if(response.message=="SUCCESS" && response.code==200){
                        $("#message-box-info").removeClass("open");
                        $("#message-box-success").toggleClass("open");
                        setTimeout(function(){
                            window.location = "/dashboard/student/all";
                        },3000);
                    }else if(response.code==404){
                        $("#message-box-info").removeClass("open");
                        $("#message-box-warning").toggleClass("open");
                        setTimeout(function(){
                            $("#message-box-warning").removeClass("open");
                        },1200);
                    }
                },
                error : function(){
                }
            });
        });
    }


    var editUser = $("#editUser");
    if(editUser.length>=1){
        editUser.formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                name: {
                    validators: {
                        notEmpty: {
                            message: 'El Nombre es necesario y no puede estar vacío.'
                        },
                        stringLength: {
                            min: 4,
                            max: 45,
                            message: 'El nombre debe ser mayor de 4 y menor de 45 caracteres de longitud.'
                        }
                    }
                },
                last_name: {
                    validators: {
                        notEmpty: {
                            message: 'El Apellido Paterno es necesario y no puede estar vacío.'
                        },
                        stringLength: {
                            min: 4,
                            max: 45,
                            message: 'El Apellido Paterno debe ser mayor de 4 y menor de 45 caracteres de longitud.'
                        }
                    }
                },
                sex: {
                    validators: {
                        notEmpty: {
                            message: 'El Sexo es un campo necesario y no puede estar vacío.'
                        }
                    }
                },
                phone: {
                    validators: {
                        notEmpty: {
                            message: 'Su numero de telefono es necesario y no puede estar vacío.'
                        }
                    }
                },
                email: {
                    validators: {
                        notEmpty: {
                            message: 'Es necesaria la dirección de correo electrónico y esta no puede estar vacía'
                        },
                        emailAddress: {
                            message: 'Este campo no contiene una dirección de correo electrónico válida.'
                        },
                        remote: {
                            message: 'Esta cuenta de correo electrónico ya existe, pruebe con otra',
                            url: '/dashboard/user/validateemail',
                            data: {
                                type: 'email'
                            },
                            type: 'POST'
                        }
                    }
                },
                status: {
                    validators: {
                        notEmpty: {
                            message: 'El usuario necesita tener un status.'
                        }
                    }
                }
            }
        }).on('success.form.fv', function(e) {
            e.preventDefault();
            $("#btn-submit").attr("disabled","disabled").val("Iniciando");
            $("#message-box-info").toggleClass("open");
            $.ajax({
                type : "POST",
                url : "/dashboard/student/updateuser",
                data :  $(this).serialize(),
                dataType : "json",
                success : function(response){
                    if(response.message=="SUCCESS" && response.code==200){
                        $("#message-box-info").removeClass("open");
                        $("#message-box-success").toggleClass("open");
                        setTimeout(function(){
                            window.location = "/dashboard/student/all/"+response.brid;
                        },3000);
                    }else if(response.code==404){
                        $("#message-box-info").removeClass("open");
                        $("#message-box-warning").toggleClass("open");
                        setTimeout(function(){
                            $("#message-box-warning").removeClass("open");
                        },1200);
                    }
                },
                error : function(){
                }
            });
        });
    }


    $('.customer-logos').slick({
        slidesToShow: 6,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 1500,
        arrows: false,
        dots: false,
        pauseOnHover: false,
        responsive: [{
            breakpoint: 768,
            settings: {
                slidesToShow: 4
            }
        }, {
            breakpoint: 520,
            settings: {
                slidesToShow: 3
            }
        }]
    });



	$('.star').on('click', function () {
      $(this).toggleClass('star-checked');
    });

    $('.ckbox label').on('click', function () {
      $(this).parents('tr').toggleClass('selected');
    });

    $('.btn-filter').on('click', function () {
      var $target = $(this).data('target');
      if ($target != 'all') {
        $('.table tr').css('display', 'none');
        $('.table tr[data-status="' + $target + '"]').fadeIn('slow');
      } else {
        $('.table tr').css('display', 'none').fadeIn('slow');
      }
    });







 var date = new Date();
    var d = date.getDate();
    var m = date.getMonth();
    var y = date.getFullYear();
    
    /*  className colors
    
    className: default(transparent), important(red), chill(pink), success(green), info(blue)
    
    */    
    
      
    /* initialize the external events
    -----------------------------------------------------------------*/
  
    $('#external-events div.external-event').each(function() {
    
      // create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
      // it doesn't need to have a start or end
      var eventObject = {
        title: $.trim($(this).text()) // use the element's text as the event title
      };
      
      // store the Event Object in the DOM element so we can get to it later
      $(this).data('eventObject', eventObject);
      
      // make the event draggable using jQuery UI
      $(this).draggable({
        zIndex: 999,
        revert: true,      // will cause the event to go back to its
        revertDuration: 0  //  original position after the drag
      });
      
    });
  
  
    /* initialize the calendar
    -----------------------------------------------------------------*/
    
    var calendar =  $('#calendar').fullCalendar({
      header: {
        left: 'title',
        center: 'agendaDay,agendaWeek,month',
        right: 'prev,next today'
      },
      editable: true,
      firstDay: 1, //  1(Monday) this can be changed to 0(Sunday) for the USA system
      selectable: true,
      defaultView: 'month',
      events: "/dashboard/student/getcalendar",  // request to load current events


      /*eventDrop: function(event, delta){ // event drag and drop
           $.ajax({
               url: '/dashboard/student/getcalendar',
               data: 1,
               type: 'POST',
               success: function(json) {
               alert("hola2");
               }
           });
       },
      events: function (start, end, timezone, callback) {
          $.ajax({
              url: '/dashboard/student/getcalendar',
              type: "GET",
              datatype: 'json',
              success: function (doc) {
                  alert("hola");
                  var events = [];
                  if ($(doc) != undefined && $(doc).Events.length > 0) {
                      $(doc).Events.forEach(function (entry) {
                          events.push({
                              title: entry.Title,
                              start: entry.Start,
                              className: entry.className
                          });

                      });
                  }
                  callback(events);
              },
              error: function (err) {
                  alert('Error in fetching data');
              }
          });
      },*/
      
      axisFormat: 'h:mm',
      columnFormat: {
                month: 'ddd',    // Mon
                week: 'ddd d', // Mon 7
                day: 'dddd M/d',  // Monday 9/7
                agendaDay: 'dddd d'
            },
            titleFormat: {
                month: 'MMMM yyyy', // September 2009
                week: "MMMM yyyy", // September 2009
                day: 'MMMM yyyy'                  // Tuesday, Sep 8, 2009
            },
      allDaySlot: false,
      selectHelper: true,
      /*select: function(start, end, allDay) {

        var title = prompt('Titulo del evento:');

        if (title) {
          calendar.fullCalendar('renderEvent',
            {
              title: title,
              start: start,
              end: end,
              allDay: allDay
            },
            true // make the event "stick"
          );
        }
        calendar.fullCalendar('unselect');
      },*/
      droppable: true, // this allows things to be dropped onto the calendar !!!
      drop: function(date, allDay) { // this function is called when something is dropped
        var student = $('#calendar').data("id");
       /* $.ajax({
            type : "POST",
            url : "/dashboard/student/getcalendar",
            data :  student,
            dataType : "json",
            success : function(response){
                if(response.message=="SUCCESS" && response.code==200){

                }else if(response.code==404){
                }
            },
            error : function(){
            }
        });*/
      
        // retrieve the dropped element's stored Event Object
        var originalEventObject = $(this).data('eventObject');

        
        // we need to copy it, so that multiple events don't have a reference to the same object
        var copiedEventObject = $.extend({}, originalEventObject);
        
        // assign it the date that was reported
        copiedEventObject.start = date;
        copiedEventObject.allDay = allDay;
        
        // render the event on the calendar
        // the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
        $('#calendar').fullCalendar('renderEvent', copiedEventObject, true);
        
        // is the "remove after drop" checkbox checked?
        if ($('#drop-remove').is(':checked')) {
          // if so, remove the element from the "Draggable Events" list
          $(this).remove();
        }
        
      },
      
      events: {

        url: '/dashboard/student/getcalendar'
      }
        /*{
          title: 'Inicios de clase',
          start: new Date(y, m, 17, 11, 0),
          allDay: false,
          className: 'success'
        },
        {
          id: 999,
          title: 'Segunda clase',
          start: new Date(y, m, 24, 11, 0),
          allDay: false,
          className: 'info'
        },
        {
          id: 999,
          title: 'Tercera clase',
          start: new Date(y, m, 25, 11, 0),
          allDay: false,
          className: 'info'
        },
        {
          title: 'Festivo',
          start: new Date(y, m, 19),
          allDay: true,
          className: 'important'
        },
        {
          title: 'Festivo',
          start: new Date(y, m, 2),
          allDay: true,
          className: 'important'
        }
      ],  */    
    });








/********************************************************/
/********************************************************/
/****** Inicio de calendario 1 "calendar2" **************/
/********************************************************/
/********************************************************/

    var calendar =  $('#calendar2').fullCalendar({
      header: {
        left: 'title',
        center: 'agendaDay,agendaWeek,month',
        right: 'prev,next today'
      },
      editable: true,
      firstDay: 1, //  1(Monday) this can be changed to 0(Sunday) for the USA system
      selectable: true,
      defaultView: 'month',
      events: "/dashboard/student/getcalendar",  // request to load current events


      eventDrop: function(event, delta){ // event drag and drop
           $.ajax({
               url: '/dashboard/student/getcalendar',
               data: 1,
               type: 'POST',
               success: function(json) {
               alert("hola2");
               }
           });
       },
      events: function (start, end, timezone, callback) {
          $.ajax({
              url: '/dashboard/student/getcalendar',
              type: "GET",
              datatype: 'json',
              success: function (doc) {
                  /*alert("hola es calendario 2");
                  alert(doc);*/
                  var events = [];
                    /*$(doc).Events.forEach(function (entry) {
                        alert("hola");
                          $("#calendar2").fullCalendar('renderEvent',
                           {
                                  title: entry.Title,
                                  start: entry.Start,
                                  className: entry.className

                           },
                           true);
                      });*/      
                    var len = doc.length;


                    doc.forEach(function(element) {
                      console.log(element);
                    });
                    for (i=0; i < len; i++) { 
                        alert(doc[i]);
                        events.push({
                            title: doc[i].Title,
                            start: doc[i].Start
                        });
                    } 
                  /*if ($(doc) != undefined && $(doc).Events.length > 0) {

                      $(doc).Events.forEach(function (entry) {
                        events.push({
                          title: entry.Title,
                          start: entry.Start,
                          className: entry.className
                        });

                      });
                  }*/
                  callback(events);
              },
              error: function (err) {
                  alert('Error in fetching data');
              }
          });
      },
      
      axisFormat: 'h:mm',
      columnFormat: {
                month: 'ddd',    // Mon
                week: 'ddd d', // Mon 7
                day: 'dddd M/d',  // Monday 9/7
                agendaDay: 'dddd d'
            },
            titleFormat: {
                month: 'MMMM yyyy', // September 2009
                week: "MMMM yyyy", // September 2009
                day: 'MMMM yyyy'                  // Tuesday, Sep 8, 2009
            },
      allDaySlot: false,
      selectHelper: true,
      /*select: function(start, end, allDay) {

        var title = prompt('Titulo del evento:');

        if (title) {
          calendar.fullCalendar('renderEvent',
            {
              title: title,
              start: start,
              end: end,
              allDay: allDay
            },
            true // make the event "stick"
          );
        }
        calendar.fullCalendar('unselect');
      },*/
      droppable: true, // this allows things to be dropped onto the calendar !!!
      drop: function(date, allDay) { // this function is called when something is dropped
        var student = $('#calendar2').data("id");
       /* $.ajax({
            type : "POST",
            url : "/dashboard/student/getcalendar",
            data :  student,
            dataType : "json",
            success : function(response){
                if(response.message=="SUCCESS" && response.code==200){

                }else if(response.code==404){
                }
            },
            error : function(){
            }
        });*/
        // recuperar el objeto de evento almacenado del elemento caído
        var originalEventObject = $(this).data('eventObject');
        // necesitamos copiarlo, para que varios eventos no tengan una referencia al mismo objeto
        var copiedEventObject = $.extend({}, originalEventObject);
        // Asígnele la fecha que se informó
        copiedEventObject.start = date;
        copiedEventObject.allDay = allDay;
        // render the event on the calendar
        // the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
        $('#calendar2').fullCalendar('renderEvent', copiedEventObject, true);
        // is the "remove after drop" checkbox checked?
        if ($('#drop-remove').is(':checked')) {
          // if so, remove the element from the "Draggable Events" list
          $(this).remove();
        }
      },
      /*events: [
        {
          title: 'Sesión inaugural',
          start: new Date(y, m, 17, 11, 0),
          allDay: false,
          className: 'success'
        },
        {
          title: 'Cátedra 9am-12pm',
          start: new Date(y, m, 25),
          end: new Date(y, m, 25),
          allDay: true,
          className: 'info'
        },
        {
          title: 'Cátedra 9am-12pm',
          start: new Date(y, m, 24),
          end: new Date(y, m, 24),
          allDay: true,
          className: 'info'
        },
        {
          title: 'Festivo',
          start: new Date(y, m, 19),
          allDay: true,
          className: 'important'
        },
        {
          title: 'Festivo',
          start: new Date(y, m, 2),
          allDay: true,
          className: 'important'
        }
      ], */     
    });
/*****************************************************/
/*****************************************************/
/****** Fin de calendario 1 "calendar2" **************/
/*****************************************************/
/*****************************************************/
































    var calendar =  $('#calendar3').fullCalendar({
      header: {
        left: 'title',
        center: 'agendaDay,agendaWeek,month',
        right: 'prev,next today'
      },
      editable: true,
      firstDay: 1, //  1(Monday) this can be changed to 0(Sunday) for the USA system
      selectable: true,
      defaultView: 'month',
      events: "/dashboard/student/getcalendar",  // request to load current events


      eventDrop: function(event, delta){ // event drag and drop
           $.ajax({
               url: '/dashboard/student/getcalendar',
               data: 1,
               type: 'POST',
               success: function(json) {
               alert("hola2");
               }
           });
       },
      events: function (start, end, timezone, callback) {
          $.ajax({
              url: '/dashboard/student/getcalendar',
              type: "GET",
              datatype: 'json',
              success: function (doc) {
                  alert("hola");
                  var events = [];
                  if ($(doc) != undefined && $(doc).Events.length > 0) {
                      $(doc).Events.forEach(function (entry) {
                          events.push({
                              title: entry.Title,
                              start: entry.Start,
                              className: entry.className
                          });

                      });
                  }
                  callback(events);
              },
              error: function (err) {
                  alert('Error in fetching data');
              }
          });
      },
      axisFormat: 'h:mm',
      columnFormat: {
                month: 'ddd',    // Mon
                week: 'ddd d', // Mon 7
                day: 'dddd M/d',  // Monday 9/7
                agendaDay: 'dddd d'
            },
            titleFormat: {
                month: 'MMMM yyyy', // September 2009
                week: "MMMM yyyy", // September 2009
                day: 'MMMM yyyy'                  // Tuesday, Sep 8, 2009
            },
      allDaySlot: false,
      selectHelper: true,
      droppable: true, 
      drop: function(date, allDay) { 
        var student = $('#calendar3').data("id");
        var originalEventObject = $(this).data('eventObject');
        var copiedEventObject = $.extend({}, originalEventObject);
        copiedEventObject.start = date;
        copiedEventObject.allDay = allDay;
        $('#calendar3').fullCalendar('renderEvent', copiedEventObject, true);
        if ($('#drop-remove').is(':checked')) {
          $(this).remove();
        }
      },
      events: [
        {
          title: 'Sesión inaugural',
          start: new Date(y, m, 17, 11, 0),
          allDay: false,
          className: 'success'
        },
        {
          id: 999,
          title: 'Cátedra 9am-12pm',
          start: new Date(y, m, 24),
          end: new Date(y, m, 24),
          allDay: true,
          className: 'info'
        },
        {
          id: 991,
          title: 'Cátedra 5pm-8pm',
          start: new Date(y, m, 23),
          end: new Date(y, m, 23),
          allDay: true,
          className: 'info'
        },
        {
          title: 'Festivo',
          start: new Date(y, m, 19),
          allDay: true,
          className: 'important'
        },
        {
          title: 'Festivo',
          start: new Date(y, m, 2),
          allDay: true,
          className: 'important'
        }
      ],      
    });





var calendar =  $('#calendar4').fullCalendar({
      header: {
        left: 'title',
        center: 'agendaDay,agendaWeek,month',
        right: 'prev,next today'
      },
      editable: true,
      firstDay: 1, //  1(Monday) this can be changed to 0(Sunday) for the USA system
      selectable: true,
      defaultView: 'month',
      events: "/dashboard/student/getcalendar",  // request to load current events


      eventDrop: function(event, delta){ // event drag and drop
           $.ajax({
               url: '/dashboard/student/getcalendar',
               data: 1,
               type: 'POST',
               success: function(json) {
               alert("hola2");
               }
           });
       },
      events: function (start, end, timezone, callback) {
          $.ajax({
              url: '/dashboard/student/getcalendar',
              type: "GET",
              datatype: 'json',
              success: function (doc) {
                  alert("hola");
                  var events = [];
                  if ($(doc) != undefined && $(doc).Events.length > 0) {
                      $(doc).Events.forEach(function (entry) {
                          events.push({
                              title: entry.Title,
                              start: entry.Start,
                              className: entry.className
                          });

                      });
                  }
                  callback(events);
              },
              error: function (err) {
                  alert('Error in fetching data');
              }
          });
      },
      
      axisFormat: 'h:mm',
      columnFormat: {
                month: 'ddd',    // Mon
                week: 'ddd d', // Mon 7
                day: 'dddd M/d',  // Monday 9/7
                agendaDay: 'dddd d'
            },
            titleFormat: {
                month: 'MMMM yyyy', // September 2009
                week: "MMMM yyyy", // September 2009
                day: 'MMMM yyyy'                  // Tuesday, Sep 8, 2009
            },
      allDaySlot: false,
      selectHelper: true,
      /*select: function(start, end, allDay) {

        var title = prompt('Titulo del evento:');

        if (title) {
          calendar.fullCalendar('renderEvent',
            {
              title: title,
              start: start,
              end: end,
              allDay: allDay
            },
            true // make the event "stick"
          );
        }
        calendar.fullCalendar('unselect');
      },*/
      droppable: true, // this allows things to be dropped onto the calendar !!!
      drop: function(date, allDay) { // this function is called when something is dropped
        var student = $('#calendar4').data("id");
       /* $.ajax({
            type : "POST",
            url : "/dashboard/student/getcalendar",
            data :  student,
            dataType : "json",
            success : function(response){
                if(response.message=="SUCCESS" && response.code==200){

                }else if(response.code==404){
                }
            },
            error : function(){
            }
        });*/
      
        // retrieve the dropped element's stored Event Object
        var originalEventObject = $(this).data('eventObject');

        
        // we need to copy it, so that multiple events don't have a reference to the same object
        var copiedEventObject = $.extend({}, originalEventObject);
        
        // assign it the date that was reported
        copiedEventObject.start = date;
        copiedEventObject.allDay = allDay;
        
        // render the event on the calendar
        // the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
        $('#calendar4').fullCalendar('renderEvent', copiedEventObject, true);
        
        // is the "remove after drop" checkbox checked?
        if ($('#drop-remove').is(':checked')) {
          // if so, remove the element from the "Draggable Events" list
          $(this).remove();
        }
        
      },
      
      events: [
        {
          title: 'Sesión inaugural',
          start: new Date(y, m, 17, 11, 0),
          allDay: false,
          className: 'success'
        },
        {
          title: 'Cátedra 9am-12pm',
          start: new Date(y, m, 24),
          end: new Date(y, m, 24),
          allDay: true,
          className: 'info'
        },
        {
          title: 'Cátedra 3pm-6:15pm',
          start: new Date(y, m, 23),
          end: new Date(y, m, 23),
          allDay: true,
          className: 'info'
        },
        {
          title: 'Festivo',
          start: new Date(y, m, 19),
          allDay: true,
          className: 'important'
        },
        {
          title: 'Festivo',
          start: new Date(y, m, 2),
          allDay: true,
          className: 'important'
        }/*,
        {
          title: 'Lunch',
          start: new Date(y, m, d, 12, 0),
          end: new Date(y, m, d, 14, 0),
          allDay: false,
          className: 'important'
        },
        {
          title: 'Birthday Party',
          start: new Date(y, m, d+1, 19, 0),
          end: new Date(y, m, d+1, 22, 30),
          allDay: false,
        },
        {
          title: 'Click for Google',
          start: new Date(y, m, 28),
          end: new Date(y, m, 29),
          url: 'https://ccp.cloudaccess.net/aff.php?aff=5188',
          className: 'success'
        }*/
      ],      
    });

    var calendar =  $('#calendar5').fullCalendar({
      header: {
        left: 'title',
        center: 'agendaDay,agendaWeek,month',
        right: 'prev,next today'
      },
      editable: true,
      firstDay: 1, //  1(Monday) this can be changed to 0(Sunday) for the USA system
      selectable: true,
      defaultView: 'month',
      events: "/dashboard/student/getcalendar",  // request to load current events


      eventDrop: function(event, delta){ // event drag and drop
           $.ajax({
               url: '/dashboard/student/getcalendar',
               data: 1,
               type: 'POST',
               success: function(json) {
               alert("hola2");
               }
           });
       },
      events: function (start, end, timezone, callback) {
          $.ajax({
              url: '/dashboard/student/getcalendar',
              type: "GET",
              datatype: 'json',
              success: function (doc) {
                  alert("hola");
                  var events = [];
                  if ($(doc) != undefined && $(doc).Events.length > 0) {
                      $(doc).Events.forEach(function (entry) {
                          events.push({
                              title: entry.Title,
                              start: entry.Start,
                              className: entry.className
                          });

                      });
                  }
                  callback(events);
              },
              error: function (err) {
                  alert('Error in fetching data');
              }
          });
      },
      
      axisFormat: 'h:mm',
      columnFormat: {
                month: 'ddd',    // Mon
                week: 'ddd d', // Mon 7
                day: 'dddd M/d',  // Monday 9/7
                agendaDay: 'dddd d'
            },
            titleFormat: {
                month: 'MMMM yyyy', // September 2009
                week: "MMMM yyyy", // September 2009
                day: 'MMMM yyyy'                  // Tuesday, Sep 8, 2009
            },
      allDaySlot: false,
      selectHelper: true,
      /*select: function(start, end, allDay) {

        var title = prompt('Titulo del evento:');

        if (title) {
          calendar.fullCalendar('renderEvent',
            {
              title: title,
              start: start,
              end: end,
              allDay: allDay
            },
            true // make the event "stick"
          );
        }
        calendar.fullCalendar('unselect');
      },*/
      droppable: true, // this allows things to be dropped onto the calendar !!!
      drop: function(date, allDay) { // this function is called when something is dropped
        var student = $('#calendar5').data("id");
       /* $.ajax({
            type : "POST",
            url : "/dashboard/student/getcalendar",
            data :  student,
            dataType : "json",
            success : function(response){
                if(response.message=="SUCCESS" && response.code==200){

                }else if(response.code==404){
                }
            },
            error : function(){
            }
        });*/
      
        // retrieve the dropped element's stored Event Object
        var originalEventObject = $(this).data('eventObject');

        
        // we need to copy it, so that multiple events don't have a reference to the same object
        var copiedEventObject = $.extend({}, originalEventObject);
        
        // assign it the date that was reported
        copiedEventObject.start = date;
        copiedEventObject.allDay = allDay;
        
        // render the event on the calendar
        // the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
        $('#calendar5').fullCalendar('renderEvent', copiedEventObject, true);
        
        // is the "remove after drop" checkbox checked?
        if ($('#drop-remove').is(':checked')) {
          // if so, remove the element from the "Draggable Events" list
          $(this).remove();
        }
        
      },
      
      events: [
        {
          title: 'Sesión inaugural',
          start: new Date(y, m, 17, 11, 0),
          allDay: false,
          className: 'success'
        },
        {
          id: 999,
          title: 'Cátedra 9am-12:15pm',
          start: new Date(y, m, 24),
          end: new Date(y, m, 24),
          allDay: true,
          className: 'info'
        },
        {
          id: 991,
          title: 'Cátedra 5pm-8:15pm',
          start: new Date(y, m, 23),
          end: new Date(y, m, 23),
          allDay: true,
          className: 'info'
        },
        {
          title: 'Festivo',
          start: new Date(y, m, 19),
          allDay: true,
          className: 'important'
        },
        {
          title: 'Festivo',
          start: new Date(y, m, 2),
          allDay: true,
          className: 'important'
        }/*,
        {
          title: 'Lunch',
          start: new Date(y, m, d, 12, 0),
          end: new Date(y, m, d, 14, 0),
          allDay: false,
          className: 'important'
        },
        {
          title: 'Birthday Party',
          start: new Date(y, m, d+1, 19, 0),
          end: new Date(y, m, d+1, 22, 30),
          allDay: false,
        },
        {
          title: 'Click for Google',
          start: new Date(y, m, 28),
          end: new Date(y, m, 29),
          url: 'https://ccp.cloudaccess.net/aff.php?aff=5188',
          className: 'success'
        }*/
      ],      
    });

});
function getSpanishD(){
    (function($){
        $.fn.datepicker.dates['es'] = {
            days: ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo"],
            daysShort: ["Dom", "Lun", "Mar", "Mié", "Jue", "Vie", "Sáb", "Dom"],
            daysMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa", "Do"],
            months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
            monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
            today: "Hoy",
            clear: "Borrar",
            weekStart: 1,
            format: "dd/mm/yyyy"
        };
    }(jQuery));
}