$(document).ready(function () {
    /* Users */
    var newUserclient = $("#newUser");
    if(newUserclient.length>=1){
        newUserclient.formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                name: {
                    validators: {
                        notEmpty: {
                            message: 'El Nombre es necesario y no puede estar vacío.'
                        },
                        stringLength: {
                            min: 4,
                            max: 45,
                            message: 'El nombre debe ser mayor de 4 y menor de 45 caracteres de longitud.'
                        }
                    }
                },
                last_name: {
                    validators: {
                        notEmpty: {
                            message: 'El Apellido Paterno es necesario y no puede estar vacío.'
                        },
                        stringLength: {
                            min: 4,
                            max: 45,
                            message: 'El Apellido Paterno debe ser mayor de 4 y menor de 45 caracteres de longitud.'
                        }
                    }
                },
                sex: {
                    validators: {
                        notEmpty: {
                            message: 'El Sexo es un campo necesario y no puede estar vacío.'
                        }
                    }
                },
                phone: {
                    validators: {
                        notEmpty: {
                            message: 'Su numero de telefono es necesario y no puede estar vacío.'
                        }
                    }
                },
                email: {
                    validators: {
                        notEmpty: {
                            message: 'Es necesaria la dirección de correo electrónico y esta no puede estar vacía'
                        },
                        emailAddress: {
                            message: 'Este campo no contiene una dirección de correo electrónico válida.'
                        },
                        remote: {
                            message: 'Esta cuenta de correo electrónico ya existe, pruebe con otra',
                            url: '/dashboard/user/validateemail',
                            data: {
                                type: 'email'
                            },
                            type: 'POST'
                        }
                    }
                },
                username: {
                    message: 'El nombre de usuario no es válido.',
                    validators: {
                        notEmpty: {
                            message: 'Se requiere el nombre de usuario y no puede estar vacío.'
                        },
                        stringLength: {
                            min: 6,
                            max: 30,
                            message: 'El nombre de usuario debe ser mayor de 6 y menos de 30 caracteres de longitud.'
                        },
                        regexp: {
                            regexp: /^[a-zA-Z0-9_\.]+$/,
                            message: 'El nombre de usuario sólo puede consistir en alfabético, número, puntos o subrayados.'
                        },
                        remote: {
                            message: 'Estae nombre de usuario ya existe, pruebe con otro',
                            url: '/dashboard/user/validateusername',
                            data: {
                                type: 'username'
                            },
                            type: 'POST'
                        }
                    }
                },
                password: {
                    validators: {
                        notEmpty: {
                            message: 'El campo de contraseña no puede estar vacío'
                        },
                        stringLength: {
                            min: 8,
                            max: 30,
                            message: 'La contraseña debe tener como mínimo 8 caracteres.'
                        }
                    }
                },
                confirmPassword: {
                    validators: {
                        identical: {
                            field: 'password',
                            message: 'Las contraseñas no coinciden'
                        }
                    }
                },
                cargo: {
                    validators: {
                        notEmpty: {
                            message: 'El campo de Cargo es necesario y no puede estar vacío.'
                        },
                        stringLength: {
                            min: 4,
                            max: 45,
                            message: 'El campo de Cargo debe ser mayor de 4 y menor de 45 caracteres de longitud.'
                        }
                    }
                },
                rol: {
                    validators: {
                        notEmpty: {
                            message: 'El usuario necesita tener un rol.'
                        }
                    }
                },
                status: {
                    validators: {
                        notEmpty: {
                            message: 'El usuario necesita tener un status.'
                        }
                    }
                }
            }
        }).on('success.form.fv', function(e) {
            e.preventDefault();
            $("#btn-submit").attr("disabled","disabled").val("Iniciando");
            $("#message-box-info").toggleClass("open");
            $.ajax({
                type : "POST",
                url : "/dashboard/user/saveuser",
                data :  $(this).serialize(),
                dataType : "json",
                success : function(response){
                    if(response.message=="SUCCESS" && response.code==200){
                        $("#message-box-info").removeClass("open");
                        $("#message-box-success").toggleClass("open");
                        setTimeout(function(){
                            window.location = "/dashboard/users";
                        },3000);
                    }else if(response.code==404){
                        $("#message-box-info").removeClass("open");
                        $("#message-box-warning").toggleClass("open");
                        setTimeout(function(){
                            $("#message-box-warning").removeClass("open");
                        },1200);
                    }
                },
                error : function(){
                }
            });
        });
    }
    var editUser = $("#updateProfile");
    if(editUser.length>=1){
        editUser.formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                name: {
                    validators: {
                        notEmpty: {
                            message: 'El Nombre es necesario y no puede estar vacío.'
                        },
                        stringLength: {
                            min: 4,
                            max: 45,
                            message: 'El nombre debe ser mayor de 4 y menor de 45 caracteres de longitud.'
                        }
                    }
                },
                last_name: {
                    validators: {
                        notEmpty: {
                            message: 'El Apellido Paterno es necesario y no puede estar vacío.'
                        },
                        stringLength: {
                            min: 4,
                            max: 45,
                            message: 'El Apellido Paterno debe ser mayor de 4 y menor de 45 caracteres de longitud.'
                        }
                    }
                },
                sex: {
                    validators: {
                        notEmpty: {
                            message: 'El Sexo es un campo necesario y no puede estar vacío.'
                        }
                    }
                },
                phone: {
                    validators: {
                        notEmpty: {
                            message: 'Su numero de telefono es necesario y no puede estar vacío.'
                        }
                    }
                },
                email: {
                    validators: {
                        notEmpty: {
                            message: 'Es necesaria la dirección de correo electrónico y esta no puede estar vacía'
                        },
                        emailAddress: {
                            message: 'Este campo no contiene una dirección de correo electrónico válida.'
                        },
                        remote: {
                            message: 'Esta cuenta de correo electrónico ya existe, pruebe con otra',
                            url: '/dashboard/user/validateemail',
                            data: function(validator, $field, value) {
                                return {
                                    email: validator.getFieldElements('email').val(),
                                    validation: validator.getFieldElements('email').attr("data-value")
                                };
                            },
                            type: 'POST'
                        }
                    }
                },
                username: {
                    message: 'El nombre de usuario no es válido.',
                    validators: {
                        notEmpty: {
                            message: 'Se requiere el nombre de usuario y no puede estar vacío.'
                        },
                        stringLength: {
                            min: 6,
                            max: 30,
                            message: 'El nombre de usuario debe ser mayor de 6 y menos de 30 caracteres de longitud.'
                        },
                        regexp: {
                            regexp: /^[a-zA-Z0-9_\.]+$/,
                            message: 'El nombre de usuario sólo puede consistir en alfabético, número, puntos o subrayados.'
                        }
                    }
                },
                password: {
                    validators: {
                        notEmpty: {
                            message: 'El campo de contraseña no puede estar vacío'
                        },
                        stringLength: {
                            min: 8,
                            max: 30,
                            message: 'La contraseña debe tener como mínimo 8 caracteres.'
                        }
                    }
                },
                confirmPassword: {
                    validators: {
                        identical: {
                            field: 'password',
                            message: 'Las contraseñas no coinciden'
                        }
                    }
                },
                cargo: {
                    validators: {
                        notEmpty: {
                            message: 'El campo de Cargo es necesario y no puede estar vacío.'
                        },
                        stringLength: {
                            min: 4,
                            max: 45,
                            message: 'El campo de Cargo debe ser mayor de 4 y menor de 45 caracteres de longitud.'
                        }
                    }
                },
                rol: {
                    validators: {
                        notEmpty: {
                            message: 'El usuario necesita tener un rol.'
                        }
                    }
                },
                status: {
                    validators: {
                        notEmpty: {
                            message: 'El usuario necesita tener un status.'
                        }
                    }
                }
            }
        }).on('success.form.fv', function(e) {
            e.preventDefault();
            $("#message-box-info").toggleClass("open");
            var values = {
                uid:$('#uid').attr('data-uid')
            };
            var data = $(this).serialize()+"&"+jQuery.param(values);
            $.ajax({
                type : "POST",
                url : "/dashboard/user/updateuser",
                data : data,
                dataType : "json",
                success : function(response){
                    if(response.message=="SUCCESS" && response.code==200){
                        messages(1);
                        if(response.redirect!="no"){
                            setTimeout(function(){
                                window.location = "profile";
                            },2000);
                        }else{
                            setTimeout(function(){
                                $("#message-box-success").removeClass("open");
                            },2000);
                        }
                    }else{messages(2);}
                },
                error : function(){
                    messages(3)
                }
            });
        });
    }
    var password = $("#updatePassword");
    if(password.length>=1){
        password.formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                password: {
                    validators: {
                        notEmpty: {
                            message: 'El campo de contraseña no puede estar vacío'
                        },
                        stringLength: {
                            min: 8,
                            max: 30,
                            message: 'La contraseña debe tener como mínimo 8 caracteres.'
                        }
                    }
                },
                confirmPassword: {
                    validators: {
                        notEmpty: {
                            message: 'El campo de contraseña no puede estar vacío'
                        },
                        identical: {
                            field: 'password',
                            message: 'Las contraseñas no coinciden'
                        }
                    }
                }
            }
        }).on('success.form.fv', function(e) {
            e.preventDefault();
            $("#message-box-info").toggleClass("open");
            var values = {
                uid:$('#uid').attr('data-uid')
            };
            var data = $(this).serialize()+"&"+jQuery.param(values);
            $.ajax({
                type : "POST",
                url : "/dashboard/user/updatepassword",
                data : data,
                dataType : "json",
                success : function(response){
                    if(response.message=="SUCCESS" && response.code==200){
                        messages(1);
                        setTimeout(function(){
                            $("#message-box-success").removeClass("open");
                            $("#modal_basic").modal('hide');
                        },1500);
                        resetForm(password);
                        password.formValidation('resetForm', true);
                    }else if(response.code==404){
                        messages(2);
                    }
                },
                error : function(){
                    messages(3)
                }
            });
        });
    }
    if($("#dropzone-user-edit").length>=1){
        $("#save-image").click(function(e){
            $("#btn-submit").attr("disabled","disabled").val("Iniciando");
            $("#message-box-info").toggleClass("open");
            var values = {
                uid:$('#uid').attr('data-uid')
            };
            var data = $("form[name=dropzone-user-edit]").serialize()+"&"+jQuery.param(values);
            $.ajax({
                type : "POST",
                url : "/dashboard/user/updateuserimage",
                data : data,
                dataType : "json",
                success : function(response){
                    if(response.message=="SUCCESS" && response.code==200){
                        setTimeout(function(){
                            $("#message-box-info").removeClass("open");
                            $("#message-box-success").toggleClass("open");
                        },1500);
                        setTimeout(function(){
                            window.location = "profile";
                        },4000);
                    }else if(response.message=="warning" && response.code==303){
                        $("#message-box-info").removeClass("open");
                        $("#message-box-danger").toggleClass("open");
                    }else if(response.code==404){
                        $("#message-box-warning").toggleClass("open");
                    }
                },
                error : function(){
                }
            });
        });
        Dropzone.autoDiscover = false;
        var myDropzone = new Dropzone('#dropzone-user-edit', {
            url: "/dashboard/user/uploadimage",
            paramName: "file", // The name that will be used to transfer the file
            maxFilesize: 5, // MB
            maxFiles: 2,
            addRemoveLinks : true,
            dictResponseError: "No se puede subir esta imagen!",
            autoProcessQueue: true,
            thumbnailWidth: 138,
            thumbnailHeight: 120,
            previewTemplate: '<div class="dz-preview dz-file-preview"><div class="dz-details"><div class="dz-filename"><span data-dz-name style:"font-weight: bold;"></span></div><div class="dz-size">File size: <span data-dz-size></span></div><div class="dz-thumbnail-wrapper"><div class="dz-thumbnail"><img data-dz-thumbnail><span class="dz-nopreview">No preview</span><div class="dz-success-mark"><i class="fa fa-check-circle-o"></i></div><div class="dz-error-mark"><i class="fa fa-times-circle-o"></i></div><div class="dz-error-message"><span data-dz-errormessage></span></div></div></div></div><div class="progress progress-striped active"><div class="progress-bar progress-bar-success" data-dz-uploadprogress></div></div></div>',

            resize: function(file) {
                var info = { srcX: 0, srcY: 0, srcWidth: file.width, srcHeight: file.height },
                    srcRatio = file.width / file.height;
                if (file.height > this.options.thumbnailHeight || file.width > this.options.thumbnailWidth) {
                    info.trgHeight = this.options.thumbnailHeight;
                    info.trgWidth = info.trgHeight * srcRatio;
                    if (info.trgWidth > this.options.thumbnailWidth) {
                        info.trgWidth = this.options.thumbnailWidth;
                        info.trgHeight = info.trgWidth / srcRatio;
                    }
                } else {
                    info.trgHeight = file.height;
                    info.trgWidth = file.width;
                }
                return info;
            },
            success: function(file, response){
                var name_image = response;
                $(".dz-preview").addClass("dz-success");
                $("div.progress").remove();
                $("#user-image").val(name_image.name);
            },
            removedfile: function(file) {
                $(file.previewElement).remove();
            }
        });
        var mockFile = { name: "Click en remove file", size: 12345};
        myDropzone.emit("addedfile", mockFile);
        $(".progress.progress-striped.active").addClass("hide");
        if($("#user-image").val()==""){
            image_load = "/dash/assets/images/users/thumbnail/no-image.jpg";
        }else{
            image_load = "/dash/assets/images/users/thumbnail/"+$("#user-image").val();
        }
        myDropzone.emit("thumbnail", mockFile,image_load);
        var existingFileCount = 1; // The number of files already uploaded
        myDropzone.options.maxFiles = myDropzone.options.maxFiles - existingFileCount;
    }
    var percentage = $("#percentage");
    if(percentage.length>=1){
        var input_percentage = $(".percentage");
        input_percentage.dblclick(function(){
            input = $(this);
            input.removeAttr("readonly");
            input.focus();
        }).keypress(function(event){
            if ( event.which == 13 ) {
                event.preventDefault();
                if(input.val()){
                    uid = $("#uid").attr("data-uid");
                    param_value = input.val();
                    param_usid = input.attr("data-usid");
                    var data = {value:param_value,id:param_usid,uid:uid};
                    $.ajax({
                        type : "POST",
                        url : '/dashboard/user/save-percentage',
                        data : data,
                        dataType : "json",
                        success : function(response){
                            if(response.code==200){
                                messages(1);
                                input.attr("readonly",true);
                                input.attr("data-value",response.val);
                                return false;
                            }else if(response.code==300){
                                messages(2);
                            }
                        },
                        error : function(){
                            messages(3);
                        }
                    });
                }else{
                    alert("Es necesario agregar un valor");
                }

            }
        }).blur(function(){
            input.val(input.attr("data-value"));
            input.attr("readonly",true);
        });
    }
    /* End */
});
function resetForm($form) {
    $form.find('input:text, input:password, input:file, input[type=email], select, textarea').val('');
    $form.find('input:radio, input:checkbox').removeAttr('checked').removeAttr('selected');
}
function messages($type){
    if($type==1){
        setTimeout(function(){
            $("#message-box-info").removeClass("open");
            $("#message-box-success").toggleClass("open");
        },1500);
        setTimeout(function(){
            $("#message-box-success").removeClass("open");
        },2000);
    }
    else if($type==2){
        setTimeout(function(){
            $("#message-box-info").removeClass("open");
            $("#message-box-warning").toggleClass("open");
        },1500);
        setTimeout(function(){
            $("#message-box-warning").removeClass("open");
        },3000);
    }else if($type==3){
        setTimeout(function(){
            $("#message-box-info").removeClass("open");
            $("#message-box-warning").toggleClass("open");
        },1500);
        setTimeout(function(){
            $("#message-box-warning").removeClass("open");
        },3000);
    }
}