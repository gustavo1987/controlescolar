<?php
date_default_timezone_set('America/Mexico_City');
error_reporting(E_ERROR | E_WARNING | E_PARSE);
error_reporting(E_ERROR);
ini_set('display_errors',true);
error_reporting(E_ALL);
//error_reporting(0);

$di = new \Phalcon\DI\FactoryDefault();
$di->set('url', function(){
    $url = new \Phalcon\Mvc\Url();
    $url->setBaseUri("https://".$_SERVER["SERVER_NAME"]."/");
    /*if($_SERVER['REQUEST_SCHEME']=='https' && $_SERVER['HTTP_HOST']=='servicioseducativos.ideiberoamerica.com'){
        $url->setBaseUri("https://".$_SERVER["SERVER_NAME"]."/");
    }else {
        $url->setBaseUri("http://".$_SERVER["SERVER_NAME"]."/");
    }*/
    return $url;
});
$di->set('router', function(){
    $router = new \Phalcon\Mvc\Router();
    $router->setDefaultModule("frontend");
    $router->add("/", array(
        'module'=>'frontend',
        'controller' => 'index',
        'action' => 'index',
    ));
/*Secciones*/
    $router->add('/([0-9-a-zA-Z\-]+)', array(
        'module'=>'frontend',
        'controller'=>'index',
        'action'=>'1'
    ))->setName("controllers")->convert('action', function($action) {
            return \Phalcon\Text::lower(\Phalcon\Text::camelize($action));
    });
    $router->add('/([a-zA-Z\-]+)/([0-9-a-zA-Z\-]+)', array(
        'module'=>'frontend',
        'controller'=>'1',
        'action'=>'2'
    ))->setName("controllers")->convert('action', function($action) {
            return \Phalcon\Text::lower(\Phalcon\Text::camelize($action));
    });
    $router->add('/([a-zA-Z\-]+)/([0-9-a-zA-Z\-]+)/([0-9]+)', array(
        'module'=>'frontend',
        'controller'=>'1',
        'action'=>'2',
        'id' => 3,
    ))->setName("controllers")->convert('action', function($action) {
        return \Phalcon\Text::lower(\Phalcon\Text::camelize($action));
    });
 /* Dashboard */
    $router->add("/dashboard", array(
        'module'=>'dashboard',
        'controller' => 'index',
        'action' => 'index',
    ));
    $router->add("/login", array(
        'module'=>'dashboard',
        'controller' => 'login',
        'action' => 'index',
    ));
    $router->add("/logout",array(
        'module'=>'dashboard',
        'controller' => 'login',
        'action' => 'logout',
    ));
    $router->add('/dashboard/([a-zA-Z\-]+)/([a-zA-Z\-]+)', array(
        'module'=>'dashboard',
        'controller' => 1,
        'action' => 2,
    ))->setName("controllers")->convert('action', function($action) {
            return \Phalcon\Text::lower(\Phalcon\Text::camelize($action));
        });
    $router->add('/dashboard/([a-zA-Z\-]+)', array(
        'module'=>'dashboard',
        'controller' => 1,
        'action' => "index",
    ))->setName("controllers")->convert('action', function($action) {
            return \Phalcon\Text::lower(\Phalcon\Text::camelize($action));
        });
    $router->add('/dashboard/([a-zA-Z\-]+)/([a-zA-Z\-]+)/([0-9]+)', array(
        'module'=>'dashboard',
        'controller' => 1,
        'action' => 2,
        'id' => 3,
    ))->setName("controllers")->convert('action', function($action) {
            return \Phalcon\Text::lower(\Phalcon\Text::camelize($action));
    });
    $router->add('/dashboard/user/percentage/([0-9]+)', array(
        'module'=>'dashboard',
        'controller' => "user",
        'action' => "percentage",
        'id' => 1,
    ))->setName("controllers")->convert('action', function($action) {
        return \Phalcon\Text::lower(\Phalcon\Text::camelize($action));
    });
    $router->add("/dashboard/users",array(
        'module'=>'dashboard',
        'controller' => 'user',
        'action' => 'index',
    ));
    $router->add("/dashboard/clients",array(
        'module'=>'dashboard',
        'controller' => 'client',
        'action' => 'index',
    ));
    $router->add("/dashboard/comunidad",array(
        'module'=>'dashboard',
        'controller' => 'comunidad',
        'action' => 'index',
    ));
    $router->add('/dashboard/estudiantes/([0-9]+)/([0-9]+)',array(
        'module'=>'dashboard',
        'controller' => 'client',
        'action' => 'aula',
        'salid' => 1,
        'subsalid' => 2,
    ));
    $router->add('/dashboard/sales/salestudentclase/([0-9]+)',array(
        'module'=>'dashboard',
        'controller' => 'sales',
        'action' => 'salestudentclase',
        'subsalid' => 1,
    ));
    $router->add("/dashboard/teachers",array(
        'module'=>'dashboard',
        'controller' => 'teacher',
        'action' => 'index',
    ));
    $router->add("/dashboard/user/edit/profile",array(
        'module'=>'dashboard',
        'controller' => 'user',
        'action' => 'profile',
    ));
    $router->add("/dashboard/category",array(
        'module'=>'dashboard',
        'controller' => 'category',
        'action' => 'index',
    ));
    $router->add("/dashboard/instructors",array(
        'module'=>'dashboard',
        'controller' => 'instructor',
        'action' => 'index',
    ));
    $router->add('/dashboard/estudiante/clase/([0-9]+)/([0-9]+)/([0-9]+)', array(
        'module'=>'dashboard',
        'controller' => 'client',
        'action' => 'clasestudiante',
        'clid' => 1,
        'salid' => 2,
        'subsalid' => 3,
    ));
    $router->add('/dashboard/clase/trabajos/([0-9]+)/([0-9]+)', array(
        'module'=>'dashboard',
        'controller' => 'client',
        'action' => 'work',
        'salid' => 1,
        'subsalid' => 2,
    ));
    $router->add("/dashboard/enviar-pago",array(
        'module'=>'dashboard',
        'controller' => 'client',
        'action' => 'emailaceptado22',
    ));
    $router->add("/dashboard/maestro/aula",array(
        'module'=>'dashboard',
        'controller' => 'maestro',
        'action' => 'aula',
    ));
    $router->add("/dashboard/calendario",array(
        'module'=>'dashboard',
        'controller' => 'index',
        'action' => 'calendar',
    ));
    $router->add("/dashboard/estudiante/catedras-anteriores",array(
        'module'=>'dashboard',
        'controller' => 'student',
        'action' => 'clasesanteriores',
    ));
    $router->add("/dashboard/estudiante/chat",array(
        'module'=>'dashboard',
        'controller' => 'student',
        'action' => 'chat',
    ));
    $router->add("/dashboard/estudiante/calendario",array(
        'module'=>'dashboard',
        'controller' => 'student',
        'action' => 'calendario',
    ));
    $router->add("/dashboard/estudiante/plan-de-estudio",array(
        'module'=>'dashboard',
        'controller' => 'student',
        'action' => 'planestudio',
    ));
    $router->add("/dashboard/sitios-de-interes",array(
        'module'=>'dashboard',
        'controller' => 'student',
        'action' => 'sitios',
    ));
    $router->add("/dashboard/servicios-escolares",array(
        'module'=>'dashboard',
        'controller' => 'student',
        'action' => 'servicios',
    ));
    $router->add("/dashboard/estudiantes",array(
        'module'=>'dashboard',
        'controller' => 'student',
        'action' => 'todos',
    ));
    $router->add("/dashboard/comunidad-general",array(
        'module'=>'dashboard',
        'controller' => 'index',
        'action' => 'comunidad',
    ));
    $router->add("/dashboard/clases/agregar/([0-9]+)",array(
        'module'=>'dashboard',
        'controller' => 'clases',
        'action' => 'modulonew',
        'id' => 1,
    ));
    return $router;
});
/**
 * Start the session the first time some component request the session service
 */
$di->set('dispatcher', function() use ($di){
    $dispatcher = new \Phalcon\Mvc\Dispatcher();
    $eventsManager = $di->getShared('eventsManager');
    $security = new Security($di);
    $security->setWorkFactor(50);
    $eventsManager->attach('dispatch', $security);
    $dispatcher->setEventsManager($eventsManager);
    return $dispatcher;
});

$di->set('session', function () {
    $session = new Phalcon\Session\Adapter\Files();
    $session->start();
    return $session;
});
$di->set('collectionManager', function(){
    return new Phalcon\Mvc\Collection\Manager();
}, true);

$application = new \Phalcon\Mvc\Application();
$di->set('cookies', function () {
    $cookies = new Phalcon\Http\Response\Cookies();
    $cookies->useEncryption(false);
    return $cookies;
});
//Pass the DI to the application
$application->setDI($di);

//Register the installed modules
$application->registerModules(array(
            'frontend' => array(
                'className' => 'Modules\Frontend\Module',
                'path' =>'../apps/modules/frontend/Module.php'
            ),
            'dashboard' => array(
                'className' => 'Modules\Dashboard\Module',
                'path' =>'../apps/modules/dashboard/Module.php'
            )
        ));
echo $application->handle()->getContent();
