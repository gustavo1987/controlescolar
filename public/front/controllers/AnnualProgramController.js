front.controller("AnnualProgramController",["$scope","$routeParams","$http","$location","$window","anchorSmoothScroll",function($scope,$routeParams,$http,$location,$window,anchorSmoothScroll){
    $scope.annualp = function(){
        var data = {};
        data["apid"] =$routeParams.id;
        data['course'] = $routeParams.course;
        $http({
            data : $.param(data),
            url : "/course/get-course-annual",
            method:"POST",
            headers : { 'Content-Type': 'application/x-www-form-urlencoded' }
        }).success(function (data) {
            if(data.code=="200"){
                $scope.course = data.course;
                $scope.result($scope.course.price.toString());
            }else{
                $window.location.href = '/';
            }
        }).error(function(data){
            console.log(data.message);
        }).finally(function(){
        });
    }
    if($routeParams.id && $routeParams.course && $routeParams.speciality){
        $scope.annualp();
        $scope.map={
            zoom : 15,
            options : {
                scrollwheel: false
            },
            style:[{"featureType":"all","elementType":"all","stylers":[{"invert_lightness":true},{"saturation":20},{"lightness":50},{"gamma":0.4},{"hue":"#00ffee"}]},{"featureType":"all","elementType":"geometry","stylers":[{"visibility":"simplified"}]},{"featureType":"all","elementType":"labels","stylers":[{"visibility":"on"}]},{"featureType":"administrative","elementType":"all","stylers":[{"color":"#ffffff"},{"visibility":"simplified"}]},{"featureType":"administrative.land_parcel","elementType":"geometry.stroke","stylers":[{"visibility":"simplified"}]},{"featureType":"landscape","elementType":"all","stylers":[{"color":"#405769"}]},{"featureType":"water","elementType":"geometry.fill","stylers":[{"color":"#232f3a"}]}]
        };
        $scope.isRegister=true;
        $scope.gotoElement = function (eID){
            $location.hash(eID);
            anchorSmoothScroll.scrollTo(eID);
        };
        $scope.registerData.course = $routeParams.id;

    }
    $scope.allcourses = function(){
        var data = {};
        $scope.year = $routeParams.yearProgram;
        data["year"] =$scope.year;
        $http({
            data : $.param(data),
            url : "/course/get-all-course-annual",
            method:"POST",
            headers : { 'Content-Type': 'application/x-www-form-urlencoded' }
        }).success(function (data) {
            if(data.code=="200"){
                $scope.courses = data.courses
            }else{
                $window.location.href = '/';
            }
        }).error(function(data){
            console.log(data.message);
        }).finally(function(){
        });
    }
    if($routeParams.yearProgram){
        $scope.allcourses();
    }
    $scope.result = function($price){
        var result = $price.split(".");
        var string = "";
        for(var i=0;i<result[0].length;i++){
            if(result[0].length==4){
                if(i==0){
                    string+=result[0][i]+",";
                }else{
                    string+=result[0][i];
                }
            }else if(result[0].length==5){
                if(i==1){
                    string+=result[0][i]+",";
                }else{
                    string+=result[0][i];
                }
            }else if(result[0].length>=6){
                if(i==2){
                    string+=result[0][i]+",";
                }else{
                    string+=result[0][i];
                }
            }else{
                string+=result[0][i];
            }

        }
        $scope.price = string+"."+result[1];
    }
}]);