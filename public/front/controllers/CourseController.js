front.controller("CourseController",["$scope","$location","$routeParams","$http","$window","Courses",function($scope,$location,$routeParams,$http,$window,Courses){
    $scope.infoCourse = function(){
        var data = {};
        data['speciality'] = $routeParams.speciality;
        data['course'] = $routeParams.course;
        $http({
            url:"/course/get-view",
            data : $.param(data),
            method : "POST",
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
        }).success(function (data) {
            if(data.code==200){
                $scope.course = data.course;
                $scope.result($scope.course.price.toString());
            }else{
                console.log(data);
                /*$window.location.href = '/';*/
            }
        }).error(function(data){
            alert("Ha ocurrido un error, intente nuevamente recargando la página. Gracias");
            console.log(data);
        })
    }
    if($routeParams.speciality && $routeParams.course){
        $scope.infoCourse();
    }
    $scope.getCourse = function(value){
        var course = Courses.listCourses(value);
        course.then(function(data){
            $scope.course = data.course;
        },function(error){
            console.log(error);
        })
    }
    if($location.path()=="/catalogo-de-cursos"){
        $http.get("/course/speciality").success(function(data){
            $scope.specialities = data.course;
            $scope.getCourse($scope.specialities[0]['cgid']);
        })
    }
    $scope.result = function($price){
        var result = $price.split(".");
        var string = "";
        for(var i=0;i<result[0].length;i++){
            if(result[0].length==4){
                if(i==0){
                    string+=result[0][i]+",";
                }else{
                    string+=result[0][i];
                }
            }else if(result[0].length==5){
                if(i==1){
                    string+=result[0][i]+",";
                }else{
                    string+=result[0][i];
                }
            }else if(result[0].length>=6){
                if(i==2){
                    string+=result[0][i]+",";
                }else{
                    string+=result[0][i];
                }
            }else{
                string+=result[0][i];
            }

        }
        $scope.price = string+"."+result[1];
    }
}]);