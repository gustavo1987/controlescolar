front.controller("FinishedController",["$scope","$routeParams","$http","Lightbox","$window",function($scope,$routeParams,$http,Lightbox,$window){
    $scope.getFCourse = function(){
        var page={};
        page['permalink'] = $routeParams.pagename;
        $http({
            method:"POST",
            data: $.param(page),
            url:"/course/finished-course",
            headers : { 'Content-Type': 'application/x-www-form-urlencoded' }
        }).success(function(data){
            if(data.code=="200"){
                $scope.info = data.result.RCourse;
                $scope.image = data.result.RGallery;
                $scope.content = $scope.info.content;
            }else{
                $window.location.href = '/';
            }
        }).error(function(data){
            console.log(data.message);
        }).finally(function(){
            $scope.openLightboxCourse = function (index) {
                Lightbox.openModal($scope.image, index);
            };
        });
    }
    if ($routeParams.pagename){
        $scope.getFCourse();
    }
    $scope.yearC = $routeParams.yearCourses;
    $scope.getAFCourse = function(){
        var year={};
        year['year'] = $scope.yearC;
        $http({
            method:"POST",
            data: $.param(year),
            url:"/course/get-all-finished-courses",
            headers : { 'Content-Type': 'application/x-www-form-urlencoded' }
        }).success(function(data){
            if(data.code=="200"){
                $scope.resultfc = data.fcourses;
            }else{
                $window.location.href = '/';
            }
        }).error(function(data){
            console.log(data.message);
        }).finally(function(){});
    }
    if ($scope.yearC){
        $scope.getAFCourse();
    }
    $scope.fix = function(key){
        key = key+1;
        console.log(key);
        var count = 1;
        if(key/3==1){
            count++;
            return 1;
        }else if(key/(count*3)==1){
            return 1;
        }else{
            return 2;
        }
    }
}]);