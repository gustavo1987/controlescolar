front.controller("IndexController",["$scope","$timeout","cfpLoadingBar","$http","Finished",function($scope,$timeout,cfpLoadingBar,$http,Finished){

/* Header */
    $scope.isCollapsedH=false;
/* End */

/* Slider */
    $http.get('/index/slider').success(function(data) {
        $scope.sliders =  data.sliders;
    });
    $scope.myInterval =4000;
/* End */

/* Count Values*/
    $scope.count ={
        "curses" : 214,
        "clients":1350,
        "experience":11,
        "instructors":70,
        "from":0,
        "duration":"10"
    };
/* End */

/* Loading Bar */
    $scope.start = function() {
        cfpLoadingBar.start();
        $scope.fakeIntro = true;
    };
    $scope.complete = function () {
        cfpLoadingBar.complete();
    }
    $scope.start();
    $timeout(function() {
        $scope.complete();
        $scope.fakeIntro = false;
    }, 1500);
/* End */

/* Bending accordion */
    $http.get('/json/accordionTraining.json').success(function(data) {
        $scope.groups =  data;
    });
      $scope.status = {
        isFirstOpen: true,
        isFirstDisabled: false
      };
/* End */

/* Courses select */
    $http.get('/index/get-annual-program').success(function(data) {
        $scope.courses =  data.courses;
    });
/* End */

/* Alliances */
    $http.get('/json/alliances.json').success(function(data) {
        $scope.alliances =  data.alliances;
    });
/* End */

/* Finished Courses */
    $scope.fcourse = function(){
        var result = Finished.finishedCourse();
        result.then(function(data){
            $scope.resultfc = data.fcourses;
        },function(error){console.log(error)});
    }
    $scope.fcourse();
/* End */

/* Map Controller */
    $scope.map={
        zoom : 15,
        coordinates : "17.9933088,-92.9372959",
        marker : "17.9933088,-92.9372959",
        options : {
            scrollwheel: false
        },
        style :[{"featureType":"water","elementType":"geometry","stylers":[{"color":"#193341"}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#2c5a71"}]},{"featureType":"road","elementType":"geometry","stylers":[{"color":"#29768a"},{"lightness":-37}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#406d80"}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#406d80"}]},{"elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#3e606f"},{"weight":2},{"gamma":0.84}]},{"elementType":"labels.text.fill","stylers":[{"color":"#ffffff"}]},{"featureType":"administrative","elementType":"geometry","stylers":[{"weight":0.6},{"color":"#1a3541"}]},{"elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#2c5a71"}]}],
        style2:[{"featureType":"water","elementType":"geometry","stylers":[{"color":"#004358"}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#1f8a70"}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#1f8a70"}]},{"featureType":"road.highway","elementType":"geometry","stylers":[{"color":"#f7cb17"}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#1f8a70"},{"lightness":-20}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#1f8a70"},{"lightness":-17}]},{"elementType":"labels.text.stroke","stylers":[{"color":"#ffffff"},{"visibility":"on"},{"weight":0.9}]},{"elementType":"labels.text.fill","stylers":[{"visibility":"on"},{"color":"#ffffff"}]},{"featureType":"poi","elementType":"labels","stylers":[{"visibility":"simplified"}]},{"elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#1f8a70"},{"lightness":-10}]},{},{"featureType":"administrative","elementType":"geometry","stylers":[{"color":"#1f8a70"},{"weight":0.7}]}],
        style3:[{"featureType":"all","elementType":"all","stylers":[{"invert_lightness":true},{"saturation":20},{"lightness":50},{"gamma":0.4},{"hue":"#00ffee"}]},{"featureType":"all","elementType":"geometry","stylers":[{"visibility":"simplified"}]},{"featureType":"all","elementType":"labels","stylers":[{"visibility":"on"}]},{"featureType":"administrative","elementType":"all","stylers":[{"color":"#ffffff"},{"visibility":"simplified"}]},{"featureType":"administrative.land_parcel","elementType":"geometry.stroke","stylers":[{"visibility":"simplified"}]},{"featureType":"landscape","elementType":"all","stylers":[{"color":"#405769"}]},{"featureType":"water","elementType":"geometry.fill","stylers":[{"color":"#232f3a"}]}],
        style4:[{"featureType":"all","elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#000000"},{"lightness":40}]},{"featureType":"all","elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#000000"},{"lightness":16}]},{"featureType":"all","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#000000"},{"lightness":20}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#000000"},{"lightness":17},{"weight":1.2}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":20}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":21}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#000000"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#000000"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":16}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":19}]},{"featureType":"water","elementType":"geometry","stylers":[{"color":"#191919"},{"lightness":17}]}]
    };
/* End */
}]);