front.controller("ServicesController",["$scope","$location","TokenService","$http","$timeout","$location","$routeParams",function($scope,$location,TokenService,$http,$timeout,$location,$routeParams){
/* Urls */
    $scope.$location = $location;
/* End */
    $scope.success= true;
    $scope.error = true;
    $scope.send = true;
    $scope.successSubscription= true;
    $scope.errorSubscription = true;
    $scope.sendSubscription = true;
/*Create token of the security*/
    $scope.createToken = function(){
        var $token =  TokenService.token();
        $token.then(function(data){
            $scope.key = data.key;
            $scope.value = data.value;
        },function(error){
            console.log(error);
        })
    }
    $scope.createToken();
/*End*/

/* Contact */
    $scope.formData = {};
    $scope.submitForm = function(isValid){
        var button  =  angular.element("#submitInput");
        if(isValid){
            $scope.send = false;
            $scope.formData['key'] = $scope.key;
            $scope.formData['value'] = $scope.value;
            button.attr("disabled",true);
            $http({
                method:"POST",
                url :"/contact/message-information",
                data : $.param($scope.formData),
                headers : { 'Content-Type': 'application/x-www-form-urlencoded' }
            }).success(function(data){
                $scope.send = true;
                if(data.code=="200"){
                    $scope.success = false;
                    $timeout(function(){$scope.success = true;},2000);
                    $scope.formData={};
                    $scope.createToken();
                }else{
                    $scope.send = true;
                    $scope.error = false;
                    $timeout(function(){$scope.error = true;},2000);
                    $scope.createToken();
                }

            }).finally(function(){});
        }
    }
/* EndContactRegister */

/* Subscription */
    $scope.subscriptionData = {};
    $scope.submitFormSubscription = function(isValid){
        var button  =  angular.element("#submitInput");
        if(isValid){
            $scope.sendSubscription = false;
            $scope.subscriptionData['key'] = $scope.key;
            $scope.subscriptionData['value'] = $scope.value;
            button.attr("disabled",true);
            $http({
                method:"POST",
                url :"/contact/subscription",
                data : $.param($scope.subscriptionData),
                headers : { 'Content-Type': 'application/x-www-form-urlencoded' }
            }).success(function(data){
                $scope.sendSubscription = true;
                if(data.code=="200"){
                    $scope.successSubscription = false;
                    $timeout(function(){$scope.successSubscription = true;},2000);
                    $scope.subscriptionData={};
                    $scope.subscriptionF.$setPristine();
                    $scope.createToken();
                }else{
                    $scope.errorSubscription = false;
                    $timeout(function(){$scope.errorSubscription = true;},2000);
                    $scope.createToken();
                }

            }).finally(function(){});
        }
    }

/* EndSubscription */

/* Company */
    $scope.companyData = {};;
    $scope.submitFormCompany = function(isValid){
        var button  =  angular.element("#submitInput");
        if(isValid){
            $scope.send = false;
            $scope.companyData['key'] = $scope.key;$scope.companyData['value'] = $scope.value;
            button.attr("disabled",true);
            $http({
                method:"POST",
                url :"/contact/message-company",
                data : $.param($scope.companyData),
                headers : { 'Content-Type': 'application/x-www-form-urlencoded' }
            }).success(function(data){
                $scope.send = true;
                if(data.code=="200"){
                    $scope.success = false;
                    $timeout(function(){$scope.success = true;},2000);
                    $scope.companyData={};
                    $scope.createToken();
                }else{
                    $scope.error = false;
                    $timeout(function(){$scope.error = true;},2000);
                    $scope.createToken();
                }

            }).finally(function(){});
        }
    }
    $scope.companyData.otro = false;
    $scope.otherCourse = true;
/* EndCompany */

/* Register */
    $scope.registerData = {};
    $scope.registerData.mx = true;
    $scope.submitFormRegister = function(isValid){
        var button  =  angular.element("#submitInput");
        if(isValid){
            $scope.send = false;
            $scope.registerData['key'] = $scope.key;$scope.registerData['value'] = $scope.value;
            button.attr("disabled",true);
            $http({
                method:"POST",
                url :"/course/register-user",
                data : $.param($scope.registerData),
                headers : { 'Content-Type': 'application/x-www-form-urlencoded' }
            }).success(function(data){
                $scope.send = true;
                if(data.code=="200"){
                    $scope.success = false;
                    $timeout(function(){
                        $scope.success = true;
                        $scope.registerData={};
                    },4000);
                    $scope.registerData.course = $routeParams.id;
                    $scope.createToken();
                }else{
                    $scope.error = false;
                    $timeout(function(){$scope.error = true;},3000);
                    $scope.createToken();
                }

            }).finally(function(){});
        }
    }
    $scope.resetForm = function (formName)
    {formName.$setPristine();}
/* Register */
}])