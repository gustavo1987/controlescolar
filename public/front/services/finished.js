front.service("Finished",["$http","$q",function($http,$q){
    this.finishedCourse = function(){
        var defer = $q.defer();
        $http.get("course/get-finished-courses").
            success(function(data){
                defer.resolve(data);
            }).error(function(data){
                defer.reject(data);
            });
        return defer.promise;
    }
}])