front.service("Courses",["$http","$q",function($http,$q){
    this.listCourses = function ($speciality) {
        var defer = $q.defer();
        var data = {};
        data['cgid'] = $speciality;
        $http({
            method:"POST",
            url:"/course/get-speciality-course",
            data : $.param(data),
            headers : { 'Content-Type': 'application/x-www-form-urlencoded' }
        }).success(function(data){
            defer.resolve(data);
        }).error(function(data){
            defer.reject(data);
        })
        return defer.promise;
    }
}]);