var front = angular.module("front",['ngRoute','angular-loading-bar','ngAnimate','ui.bootstrap','ngMap','countTo','bootstrapLightbox','remoteValidation','ngSanitize','angulartics', 'angulartics.google.analytics']);
front.config(['$routeProvider','$locationProvider','cfpLoadingBarProvider',"$analyticsProvider",function($routeProvider,$locationProvider,cfpLoadingBarProvider,$analyticsProvider){
    $routeProvider
        .when("/",{
            templateUrl:"/front/views/index/index.html",
            controller : "IndexController"
        }).when("/acerca-de-capacitaciones",{
            templateUrl:"/front/views/index/about.html",
            controller : "IndexController"
        }).when("/paypal/error",{
            templateUrl:"/front/views/index/paypal.html",
            controller : "IndexController"
        }).when("/paypal/pagado",{
            templateUrl:"/front/views/index/paid.html",
            controller : "IndexController"
        }).when("/paypal/cancelado",{
            templateUrl:"/front/views/index/cancel.html",
            controller : "IndexController"
        }).when('/contactanos',{
            templateUrl:"/front/views/index/contact.html",
            controller:"IndexController"
        }).when('/register-in-company',{
            templateUrl:"/front/views/index/registerIncompany.html",
            controller:"IndexController"
        })
        //Cursos Proximos del Año
        .when('/cursos/finalizados/:yearCourses',{
            templateUrl:"/front/views/finished/allFinishedCourse.html",
            controller:"FinishedController"
        })
        // Todos los cursos
        .when('/catalogo-de-cursos/:speciality/:course',{
            templateUrl:"/front/views/course/viewCourse.html",
            controller:"CourseController"
        })
        .when('/:speciality/:course/:id',{
            templateUrl:"/front/views/annualp/course.html",
            controller:"AnnualProgramController"
        })
        .when('/programa-anual/:yearProgram',{
            templateUrl:"/front/views/annualp/yearCourses.html",
            controller:"AnnualProgramController"
        })
        //Vista de todo los cursos que se pueden dar
        .when('/catalogo-de-cursos',{
            templateUrl:"/front/views/course/allCourses.html",
            controller:"CourseController"
        })
        //Cursos Finalizados
        .when('/curso/finalizado/:year/:pagename',{
            templateUrl:"/front/views/finished/finishedCourse.html",
            controller:"FinishedController"
        })
        .otherwise({redirectTo:"/"});
    $locationProvider.html5Mode(true);
    cfpLoadingBarProvider.includeSpinner = true;
}]).directive(
    "mAppLoading",
    function( $animate ) {
        return({
            link: link,
            restrict: "C"
        });
        function link( scope, element, attributes ) {
            $animate.leave( element.children().eq( 1 ) ).then(
                function cleanupAfterAnimation() {
                    element.remove();
                   scope = element = attributes = null;

                }
            );

        }

    }
).directive('carouselS',function($timeout){
    return function($scope,$el,$attrs){
        setTimeout((function(){
            $el.owlCarousel({
                loop:true,
                margin:0,
                nav:false,
                items:1
            })
        }),1000)
    }
}).directive('carouselC',function($timeout){
    return function($scope,$el,$attrs){
        setTimeout((function(){
            $el.owlCarousel({
                loop:true,
                responsiveClass:true,
                dots:false,
                nav:true,
                navText:['<i class="fa fa-angle-right"></i>','<i class="fa fa-angle-left"></i>'],
                responsive:{
                    0:{
                        items:2,
                        nav:true
                    },
                    500:{
                        items:3,
                        nav:true
                    },
                    992:{
                        items:4,
                        nav:false
                    },
                    1200:{
                        items:4,
                        nav:true,
                        loop:false
                    }
                }
            })
        }),1000)
    }
}).directive('focusMe', function($timeout) {
        return {
            scope: { trigger: '=focusMe' },
            link: function(scope, element) {
                scope.$watch('trigger', function(value) {
                    if(value === true) {
                        //console.log('trigger',value);
                        //$timeout(function() {
                        element[0].focus();
                        scope.trigger = false;
                        //});
                    }
                });
            }
        };
    });
